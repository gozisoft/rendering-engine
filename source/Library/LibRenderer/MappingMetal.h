//
// Created by Riad Gozim on 10/02/2016.
//

#ifndef MAPPING_METAL_H
#define MAPPING_METAL_H

#import "RendererTypes.h"
#import <Metal/Metal.h>

namespace engine {

    class ApiToMetal {
    public:
        // Input Format Mapping
        static MTLResourceOptions BufferUsage(CResourceAccess::Type usage);
        // static GLenum BufferMapMode(CResourceLock::Type lock);
        // static GLenum BufferBindFlags(size_t bufferType);

        // Texture Format Mapping
        static MTLPixelFormat TextureFormat(CTextureFormat::Type tFormat);

        // Render Mapping
        // static GLenum PrimitiveType(CPrimitiveType::Type primitiveType);
    };

    class MetalToApi {
    public:
        static CResourceAccess::Type BufferUsage(MTLResourceOptions options);

    };

}

#endif