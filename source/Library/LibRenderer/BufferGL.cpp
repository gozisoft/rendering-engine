#include "librendererfwd.h"
#include "BufferGL.h"
#include "MappingGL.h"
#include "Buffer.h"

using namespace engine;

CBufferGL::CBufferGL(const BufferPtr& pBuffer) : 
m_buffer(0), m_target(0)
{
	::glGenBuffers(1, &m_buffer);

	if (!m_buffer)
		assert( false && ieS("Cannot create GL index buffer\n") );

	// The opengl target ( index buffer, vertex buffer, uniform buffer object(UBO) )
	m_target = GLBindFlag( pBuffer->GetBufferType() );

	// generate a new VBO and get the associated ID
	::glBindBuffer(m_target, m_buffer);

	// bind VBO in order to use
	::glBufferData( m_target, pBuffer->GetBytesSize(), (GLvoid*)pBuffer->GetData(),
		GLBufferUsage( pBuffer->GetUsage() ) );

	// Error check for buffer
	GLenum errCode = ::glGetError();
	if (errCode != GL_NO_ERROR)
	{
		const char* errStr = 
			reinterpret_cast<const char*>(gluErrorString(errCode));

		OutputDebugStringA(errStr);
	}

	// disable buffer [its not being used yet]
	::glBindBuffer(m_target, 0);
}

CBufferGL::~CBufferGL()
{
	::glDeleteBuffers(1, &m_buffer);
}

// Data Access functions
void* CBufferGL::MapImpl(size_t offset, size_t length, Buffer::Locking mode)
{
	// Bind us, get a pointer into the buffer, then
	// offset it by vertexStart so we act like the D3D layer.
	::glBindBuffer(m_target, m_buffer);

	// Retrieve the buffer size
	GLint bufferSize = 0;
	::glGetBufferParameterivARB(m_target, GL_BUFFER_SIZE, &bufferSize);

	// Retrieve the buffer usage
	GLenum usage = 0;
	::glGetBufferParameterivARB(m_target, GL_BUFFER_USAGE, (GLint*)&usage);

	// Check for discard first
	if(mode == CIndexBuffer::RL_DISCARD && bufferSize != 0)
	{
		// Discard the buffer
		::glBufferData(m_target, bufferSize, 0, usage);
		
		// Get the new buffer size
		::glGetBufferParameterivARB(m_target, GL_BUFFER_SIZE, &bufferSize);
	}

	GLenum access = 0;
	switch (usage)
	{
	case GL_STATIC_DRAW: 
		// The data store contents will be modified once and used many times.
		// If static buffer already has data, cannot write to buffer
		// Else you can write to the buffer for the first time
		access = (bufferSize != 0) ? GL_READ_ONLY : GL_WRITE_ONLY;
		break;

	case GL_DYNAMIC_DRAW:
		switch (mode)
		{
		case Buffer::RL_WRITE_ONLY: 
			access = GL_WRITE_ONLY;
			break;

			// The application must ensure that it does not write over any data in use by the IA stage
		case Buffer::RL_NO_OVERWRITE: 
			if ( m_target & GL_ARRAY_BUFFER || m_target & GL_ELEMENT_ARRAY_BUFFER )
			{
				access = GL_WRITE_ONLY;
			}
			else
			{
				// This bufer is not the correct type to allow NO_OVERWRITE
				assert(false);
			}
			break;

		default:
			assert( false && ieS("Incorrect lock mode for use of dynamic or staging buffer") );
			return 0;
		};
		break;

	default:
		assert(false && ieS("Unkown buffer usage: MapImpl()"));
		return nullptr;
	};

	// Now time to get the pointer to memory
	GLvoid* pBuffer = ::glMapBuffer(m_target, access);
	if(pBuffer == nullptr)
	{
		GLenum errCode = glGetError();
		if (errCode != GL_NO_ERROR)
		{
			const char* errStr = 
				reinterpret_cast<const char*>(gluErrorString(errCode));

			OutputDebugStringA(errStr);
		}
	}

	// Reset gl state
	::glBindBuffer(m_target, 0);

	// return the offset data
	return static_cast<void*>(static_cast<uint8_t*>(pBuffer) + offset);
}

void CBufferGL::UnmapImpl()
{
	 ::glBindBuffer(m_target, m_buffer);

	 GLboolean result = ::glUnmapBuffer(m_target);
	 if (result == false)
	 {
		GLenum errCode = ::glGetError();
		if (errCode != GL_NO_ERROR)
		{
			const char* errStr = 
				reinterpret_cast<const char*>(gluErrorString(errCode));

			OutputDebugStringA(errStr);
		}
	 }

	 ::glBindBuffer(m_target, 0);
}
