#include "librendererafx.h"
#include "ViewD3D11.h"
#include "ResourceD3D11.h"
#include "ExceptionD3D.h"

using namespace engine;

// =========================================================================================
// CShaderResourceD3D11
// =========================================================================================
CShaderResourceD3D11::CShaderResourceD3D11(ID3D11ShaderResourceViewPtr view)
	:
	m_view(view),
	m_type(CViewType::DT_RESOURCE)
{

}

CShaderResourceD3D11::~CShaderResourceD3D11()
{
	//if (m_view)
	//	m_view->Release();
}

ShaderResourceInfo CShaderResourceD3D11::info() const
{
	D3D11_SHADER_RESOURCE_VIEW_DESC desc;
	m_view->GetDesc(&desc);

	switch (desc.ViewDimension)
	{
	case D3D11_RTV_DIMENSION_BUFFER:
		return CBufferSRI(desc.Buffer.ElementOffset, desc.Buffer.ElementWidth);
	case D3D11_RTV_DIMENSION_TEXTURE1D:
		return CTextureSRI(desc.Texture1D.MipLevels, desc.Texture1D.MostDetailedMip);
	case D3D11_RTV_DIMENSION_TEXTURE2D:
		return CTextureSRI(desc.Texture2D.MipLevels, desc.Texture2D.MostDetailedMip);
	case D3D11_RTV_DIMENSION_TEXTURE3D:
		return CTextureSRI(desc.Texture3D.MipLevels, desc.Texture3D.MostDetailedMip);
	default:
		throw std::exception("Unknown shader resource view");
	}
}

// =========================================================================================
// CRenderTargetD3D11
// =========================================================================================
CRenderTargetD3D11::CRenderTargetD3D11(ID3D11RenderTargetViewPtr view)
	:
	m_view(view),
	m_type(CViewType::DT_RENDERTARGET)
{

}

CRenderTargetD3D11::~CRenderTargetD3D11()
{
	//if (m_view)
	//	m_view->Release();
}

RenderTargetInfo CRenderTargetD3D11::info() const
{
	D3D11_RENDER_TARGET_VIEW_DESC desc;
	m_view->GetDesc(&desc);

	switch (desc.ViewDimension)
	{
	case D3D11_DSV_DIMENSION_TEXTURE1D:
		return CTextureRTI(desc.Texture1D.MipSlice);
	case D3D11_DSV_DIMENSION_TEXTURE2D:
		return CTextureRTI(desc.Texture2D.MipSlice);
	default:
		throw std::exception("Unknown render target view");
	}
}

// =========================================================================================
// CDepthStencilD3D11
// =========================================================================================
CDepthStencilD3D11::CDepthStencilD3D11(ID3D11DepthStencilViewPtr view)
	:
	m_view(view),
	m_type(CViewType::DT_DEPTHSTENCIL)
{

}

CDepthStencilD3D11::~CDepthStencilD3D11()
{

}

DepthStencilInfo CDepthStencilD3D11::info() const
{
	D3D11_DEPTH_STENCIL_VIEW_DESC desc;
	m_view->GetDesc(&desc);

	switch (desc.ViewDimension)
	{
	case D3D11_DSV_DIMENSION_TEXTURE1D:
		return CTextureDSI(desc.Texture1D.MipSlice);
	case D3D11_DSV_DIMENSION_TEXTURE2D:
		return CTextureDSI(desc.Texture2D.MipSlice);
	case D3D11_DSV_DIMENSION_UNKNOWN:
		// return CTextureRTI(desc.Texture3D.);
	default:
		throw std::exception("Unknown depth stencil view");
	}
}
