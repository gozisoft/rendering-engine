#include "librendererafx.h"
#include "CullState.h"

using namespace engine;


CCullState::CCullState() : m_cullEnabled(true), m_ccOrder(false)
{

}

CCullState::CCullState(bool enableCull, bool ccOrder) : 
m_cullEnabled(enableCull), m_ccOrder(ccOrder)
{

}

CCullState::~CCullState()
{

}