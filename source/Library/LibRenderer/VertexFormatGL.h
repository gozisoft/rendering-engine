#ifndef CVERTEX_FORMAT_GL_H
#define CVERTEX_FORMAT_GL_H

#include "FrameFwd.h"

_ENGINE_BEGIN

class CVertexFormatGL
{
public:
	CVertexFormatGL();
	~CVertexFormatGL();

	void Enable(const CVertexFormat* pVFromat);
	void Disable(const CVertexFormat* pVFromat);

private:
	uint8_t* BufferOffset(const Long offset);

};


_ENGINE_END


#endif