#include "librendererafx.h"
#include "InputFormatD3D11.h"
#include <algorithm>
#include <iterator>

using namespace engine;

InputFormatD3D11::InputFormatD3D11(const CInputElement* elements, size_t numElements, ID3D11InputLayoutPtr inputLayout) 
	:
m_elements(elements, elements + numElements),
m_inputLayout(inputLayout)
{
	
}
//-------------------------------------------------------------------------------------------------------------------
InputFormatD3D11::~InputFormatD3D11()
{

}
//-------------------------------------------------------------------------------------------------------------------
size_t InputFormatD3D11::GetElementSemanticCount(const std::string& semantic) const 
{
	ptrdiff_t size = std::count_if(std::begin(m_elements), std::end(m_elements), 
		[&semantic](const CInputElement& element)
	{
		return element.GetVertexSemantic() == semantic;
	});

	return static_cast<size_t>(size);
}
//-------------------------------------------------------------------------------------------------------------------
const CInputElement& InputFormatD3D11::GetElementBySemantic(const std::string& semantic, size_t semanticIndex) const
{
	auto itor = std::find_if(std::begin(m_elements), std::end(m_elements), 
		[&semantic, &semanticIndex](const CInputElement& pTemp)
	{
		return (pTemp.GetVertexSemantic() == semantic && pTemp.GetSemanticIndex() == semanticIndex);
	});

	if ( itor == std::end(m_elements) )
		throw  std::runtime_error("Couldn't find semantic [InputFormatD3D11::GetElementBySemantic]");

	return (*itor);
}
//-------------------------------------------------------------------------------------------------------------------