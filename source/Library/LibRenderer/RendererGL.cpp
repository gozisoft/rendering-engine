#include "librendererafx.h"
#include "RendererGL.h"
#include "ResourceGL.h"

using namespace engine;

CRendererGL::CRendererGL() {
}

CRendererGL::~CRendererGL() {

}

CRenderType::Type CRendererGL::GetRenderType() const {
    return CRenderType::Type::RT_OPENGL;
}

shared_ptr<IPipeline> CRendererGL::getPipeline() const {
    return std::shared_ptr<IPipeline>();
}

std::unique_ptr<IShader> CRendererGL::createShader(CShaderType::Type type, shader_handle handle) {
    return std::unique_ptr<IShader>();
}

std::unique_ptr<IInputFormat> CRendererGL::createInputFormat(const CInputElement *elements, size_t numElements,
                                                             shader_handle handle) {
    return std::unique_ptr<IInputFormat>();
}

unique_ptr<IBuffer> CRendererGL::createBuffer(size_t bindFlags, CResourceAccess::Type usage,
                                              size_t sizeInBytes, uint8_t const *data) {
    return std::unique_ptr<IBuffer>();
}

unique_ptr<ITexture1D> CRendererGL::createTexture(size_t bindFlags,
                                                  CResourceAccess::Type usage,
                                                  CTextureFormat::Type tformat,
                                                  const SMipMap1D *mips,
                                                  size_t numLevels,
                                                  uint8_t const *data,
                                                  size_t arraySize) {
    return std::unique_ptr<ITexture1D>();
}

unique_ptr<ITexture2D> CRendererGL::createTexture(size_t bindFlags,
                                                  CResourceAccess::Type usage,
                                                  CTextureFormat::Type tformat,
                                                  const SMipMap2D *mips,
                                                  size_t numLevels,
                                                  uint8_t const *data,
                                                  size_t arraySize) {
    return std::unique_ptr<ITexture2D>();
}

unique_ptr<CBufferGL> CRendererGL::_createBuffer(size_t bindFlags,
                                                 CResourceAccess::Type usage,
                                                 size_t sizeInBytes,
                                                 const uint8_t *data) {
    return std::make_unique<CBufferGL>(bindFlags, usage, sizeInBytes, data);
}

unique_ptr<CTexture1DGL> CRendererGL::_createTexture(size_t bindFlags,
                                                     CResourceAccess::Type usage,
                                                     CTextureFormat::Type tformat,
                                                     const SMipMap1D *mips,
                                                     size_t numLevels,
                                                     const uint8_t *data,
                                                     size_t arraySize) {
    return unique_ptr<CTexture1DGL>();
}



/*
bool CRendererGL::GetPickRay (int32_t x, int32_t y, Vector3f& origin, Vector3f& direction) const
{
	if (!m_pCamera)
	{
		return false;
	}

	// Get the current viewport and test whether (x,y) is in it.
	Viewporti view = GetViewport();
	if (x < view[0] || x > view[0] + view[2] || y < view[1] || y > view[1] + view[3])
	{
		return false;
	}

	// Get the [0,1]^2-normalized coordinates of (x,y).
	float r = ((float)(x - view[0]))/(float)view[2];
	float u = ((float)(y - view[1]))/(float)view[3];

	// Pointer to the frustum
	const float* pFrustum = m_pCamera->GetFrustum();

	float rBlend = (1.0f - r)*pFrustum[Camera::S_LEFT] + r*pFrustum[Camera::S_RIGHT];
	float uBlend = (1.0f - u)*pFrustum[Camera::S_BOTTOM] + u*pFrustum[Camera::S_TOP];
	float dBlend = pFrustum[Camera::S_NEAR];

	origin = m_pCamera->position();

	direction = dBlend*m_pCamera->forward() + rBlend * m_pCamera->right()
		+ uBlend*m_pCamera->up();

	direction.Normalise();
	return true;
}
*/

/*void CRendererGL::Draw(const Visual* pNode, const CEffect* pEffect)
{
	const IndexBufferPtr& pIBuffer = pNode->GetIndexBuffer();
	const VertexBufferPtr& pVBuffer = pNode->GetVertexBuffer();
	const CVertexFormatPtr& pVFormat = pNode->GetVertexFormat();

	// Enable geometry
	m_pResources->Enable(pVBuffer);
	m_pResources->Enable(pVFormat);
	if (pIBuffer)
	{
		m_pResources->Enable(pIBuffer);
	}

	// Set up projection matrix
	::glMatrixMode(GL_PROJECTION);
	::glLoadIdentity();

	Matrix4f proj = m_pCamera->GetProjectionMatrix();
	::glLoadMatrixf( static_cast<const GLfloat*>( proj.data() ) );

	// Set up view matrix
	::glMatrixMode(GL_MODELVIEW);
	::glLoadIdentity();

    // Set up world world view matrix
	const Matrix4f& view = m_pCamera->GetViewMatrix();
	const Matrix4f& world = pNode->GetWorldTransform().Matrix();
	const Matrix4f& myworldview = world * view;
	::glLoadMatrixf( static_cast<const GLfloat*>( myworldview.data() ) );

	// Draw the node
	DrawPrimitive(pNode);

	::glMatrixMode(GL_MODELVIEW);
	::glPopMatrix();
	::glMatrixMode(GL_PROJECTION);
	::glPopMatrix();

	// Disable geometry
	if (pIBuffer)
	{
		m_pResources->Disable(pIBuffer);
	}
	m_pResources->Disable(pVFormat);
	m_pResources->Disable(pVBuffer);

}
//----------------------------------------------------------------------------
void CRendererGL::DrawPrimitive(const Visual* pNode)
{
	Visual::PrimitiveType type = pNode->GetType();
	VertexBufferPtr pVBuffer = pNode->GetVertexBuffer();
	CVertexFormatPtr pVFormat = pNode->GetVertexFormat();
	IndexBufferPtr pIBuffer = pNode->GetIndexBuffer();

	switch (type)
	{
	case Visual::PT_TRISTRIP:
	case Visual::PT_TRIMESH:
	case Visual::PT_TRIFAN:
		{
			GLsizei numVerts = (GLsizei)pVBuffer->GetNumElements();
			GLsizei numIndices = (GLsizei)pIBuffer->GetNumElements();

			if (numVerts && numIndices)
			{
				GLenum indexType = 0;
				const GLvoid* pIndexData;
				switch ( pIBuffer->GetType() )
				{
				case Buffer::IT_USHORT:
					indexType = GL_UNSIGNED_SHORT;
					pIndexData = (UShort*)0 + pIBuffer->GetBytesStride();
					break;
				case Buffer::IT_size_t:
					indexType = GL_UNSIGNED_INT;
					pIndexData = (uint32_t*)0 + pIBuffer->GetBytesStride();
					break;
				}

				::glDrawRangeElements( GLPrimitiveType(type), 0, numVerts-1, numIndices, indexType, 0 );
			}
		}
		break;
	case Visual::PT_POLYLINE_OPEN:
		{
			const CPolyline* pPolyline = static_cast<const CPolyline*>(pNode);
			GLsizei numVerts = (GLsizei)pPolyline->GetNumLines();

			if (numVerts)
			{
				::glDrawArrays(GL_LINE_STRIP, 0, numVerts);
			}
		}
		break;
	case Visual::PT_POLYLINE_CLOSED:
		{
			const CPolyline* pPolyline = static_cast<const CPolyline*>(pNode);
			GLsizei numVerts = (GLsizei)pPolyline->GetNumLines();

			if (numVerts)
			{
				::glDrawArrays(GL_LINES, 0, numVerts);
			}
		}
		break;
	case Visual::PT_POLYPOINTS:
		{
			const CPolypoint* pPolypoint = static_cast<const CPolypoint*>(pNode);
			GLsizei numVerts = (GLsizei)pPolypoint->GetNumPoints();

			if (numVerts)
			{
				::glDrawArrays(GL_POINTS, 0, numVerts);
			}
		}
		break;
	default:
		assert( false && ieS("Invalid type\n") );
		break;

	} // switch

}*/
