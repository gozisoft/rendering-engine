#pragma once

#ifndef RENDERER_HEADERS_H
#define RENDERER_HEADERS_H

// Interfaces
#include "IInputFormat.h"
#include "IRenderer.h"
#include "IPipeline.h"
#include "IResource.h"
#include "IShader.h"
#include "IView.h"
#include "IRasterizerState.h"

// Device::states
#include "RendererTypes.h"
#include "AlphaState.h"
#include "CullState.h"
#include "DepthState.h"
#include "OffsetState.h"
#include "WireState.h"
#include "ZBufferState.h"

//// Device
//#include "ExceptionD3D.h"
//#include "ShaderCompilerHLSL.h"

//// Device::Direct3D 11
//#include "InputFormatD3D11.h"
//#include "ResourceD3D11.h"
//#include "ViewD3D11.h"
//#include "ShaderD3D11.h"
//#include "PipelineD3D11.h"
//#include "RendererD3D11.h"
//#include "MappingD3D11.h"






#endif