#pragma once

#ifndef D3D11_FORWARD_H
#define D3D11_FORWARD_H

#if (_MSC_VER < 1700)
#if defined(DEBUG) || defined(_DEBUG)
	#pragma comment( lib, "d3dx11d.lib" )
	#else
#pragma comment( lib, "d3dx11.lib" )
#endif
#else
	#pragma comment( lib, "d3d11.lib" )
#endif

// Enable extra D3D debugging in debug builds if using the debug DirectX runtime.
// This makes D3D objects work well in the debugger watch window, but slows down
// performance slightly.
#if defined(DEBUG) || defined(_DEBUG)
    #ifndef D3D_DEBUG_INFO
    #define D3D_DEBUG_INFO
    #endif
#endif

// Common includes and libs
#include "D3DFwd.h"

// Used for comptr
#include <atlbase.h>

// Direct3D11 includes
#include <d3d11_2.h>

// Shared pointer includes
#include <memory>


_ENGINE_BEGIN

// Device
class RendererD3D11;
class PipelineD3D11;

// Input stage
class InputFormatD3D11;

// Resources
class IResourceD3D11;
class CBufferD3D11;
class CTexture1DD3D11;
class CTexture2DD3D11;

// Views
class IViewD3D11;
class CShaderResourceD3D11;
class CRenderTargetD3D11;
class CDepthStencilD3D11;

// Shader
struct SD3DShaderVTable; // Used for shader function virtual table.
class CShaderD3D11;

// Rasterizer
class RasterizerStateD3D11;


// Internal pointer types 
// ---------------------------------------------------------------------------

//typedef std::shared_ptr<RendererD3D11> CRendererD3D11Ptr;
//typedef std::shared_ptr<PipelineD3D11> CPipelineD3D11Ptr;
//typedef std::shared_ptr<CBufferD3D11> CBufferD3D11Ptr;
//typedef std::shared_ptr<CTexture1DD3D11> CTexture1DD3D11Ptr;
//typedef std::shared_ptr<CTexture2DD3D11> CTexture2DD3D11Ptr;
//typedef std::shared_ptr<CShaderResourceD3D11> CShaderResourceD3D11Ptr;
//typedef std::shared_ptr<CRenderTargetD3D11> CRenderTargetD3D11Ptr;
//typedef std::shared_ptr<CDepthStencilD3D11> CDepthStencilD3D11Ptr;
//typedef std::shared_ptr<CShaderD3D11> CShaderD3D11Ptr;


// Com pointer types 
// ---------------------------------------------------------------------------

// D3D11 devices
typedef CComPtr<ID3D11Device> ID3D11DevicePtr;
typedef CComPtr<ID3D11DeviceContext> ID3D11DeviceContextPtr;

typedef CComPtr<ID3D11RasterizerState> ID3D11RasterizerStatePtr;
typedef CComPtr<ID3D11RasterizerState1> ID3D11RasterizerState1Ptr;
typedef CComPtr<ID3D11RenderTargetView> ID3D11RenderTargetViewPtr;
typedef CComPtr<ID3D11DepthStencilView> ID3D11DepthStencilViewPtr;

// Debug
typedef CComPtr<ID3D11Debug> ID3D11DebugPtr;
typedef CComPtr<ID3D11InfoQueue> ID3D11InfoQueuePtr;

// Resource
typedef CComPtr<ID3D11Resource> ID3D11ResourcePtr;
typedef CComPtr<ID3D11Buffer> ID3D11BufferPtr;
typedef CComPtr<ID3D11InputLayout> ID3D11InputLayoutPtr;
typedef CComPtr<ID3DBlob> ID3DBlobPtr;
typedef CComPtr<ID3D11View> ID3D11ViewPtr;
typedef CComPtr<ID3D11ShaderResourceView> ID3D11ShaderResourceViewPtr;

// Shader
typedef CComPtr<ID3D11DeviceChild> ID3D11DeviceChildPtr;
typedef CComPtr<ID3D11VertexShader> ID3D11VertexShaderPtr;
typedef CComPtr<ID3D11GeometryShader> ID3D11GeometryShaderPtr;
typedef CComPtr<ID3D11HullShader> ID3D11HullShaderPtr;
typedef CComPtr<ID3D11PixelShader> ID3D11PixelShaderPtr;

// Texture
typedef CComPtr<ID3D11Texture1D> ID3D11Texture1DPtr;
typedef CComPtr<ID3D11Texture2D> ID3D11Texture2DPtr;
typedef CComPtr<ID3D11Texture3D> ID3D11Texture3DPtr;

// DXGI
typedef CComPtr<IDXGIDevice1> IDXGIDevice1Ptr;
typedef CComPtr<IDXGIFactory1> IDXGIFactory1Ptr;
typedef CComPtr<IDXGIAdapter1> IDXGIAdapter1Ptr;
typedef CComPtr<IDXGIOutput> IDXGIOutputPtr;
typedef CComPtr<IDXGISwapChain> IDXGISwapChainPtr;


// Typedefs for resources
//typedef std::unordered_map< const CShader*, ID3D11DeviceChildPtr > ShaderMap;				// Device Child (Vertex, pixel, geometry shader)
//typedef std::unordered_map< const CBuffer*, ID3D11Resource* > ResourceMap;				// Resource
//typedef std::unordered_map< const CView*, ID3D11View* > ViewMap;							// Views
//typedef std::unordered_map< const CVertexFormat*, ID3D11InputLayoutPtr > InputLayoutMap;	// Device Child inputlayout specialisation for enable functions

//// Containers to map between API level types and D3D11 types
//typedef std::unordered_map<const IBuffer*, ID3D11Resource*> ResourceMap;
//typedef std::unordered_map<const IInputFormat*, ID3D11InputLayout*> InputLayoutMap;
//typedef std::unordered_map<const IShader*, ID3D11DeviceChildPtr> ShaderMap;

//namespace Detail
//{
//	// Specialistation of tbb_hash
//	template<typename Key>
//	class tbb::tbb_hash< std::shared_ptr<Key> >
//	{
//	public:
//		tbb_hash() {}
//
//		size_t operator()(const std::shared_ptr<Key>& key) const {
//			return tbb_hasher( key.get() );
//		}
//	};
//
//	// Types
//	typedef tbb::concurrent_unordered_map<
//		CShaderPtr,
//		CShaderD3D11Ptr
//	> ShaderMap;
//
//	typedef tbb::concurrent_unordered_map<
//		CVertexFormatPtr,
//		CVertexFormatD3D11Ptr
//	> VertexFormatMap;
//
//	typedef tbb::concurrent_unordered_map<
//		CConstantBufferPtr,
//		CConstantBufferD3D11Ptr
//	> ConstantBufferMap;
//
//	typedef tbb::concurrent_unordered_map<
//		CVertexBufferPtr,
//		CVertexBufferD3D11Ptr
//	> VertexBufferMap;	
//
//	typedef tbb::concurrent_unordered_map<
//		CIndexBufferPtr,
//		CIndexBufferD3D11Ptr
//	> IndexBufferMap;
//
//	typedef tbb::concurrent_unordered_map<
//		CTexture1DPtr,
//		CTexture1DD3D11Ptr
//	> Texture1DMap;	
//
//	typedef tbb::concurrent_unordered_map<
//		CTexture2DPtr,
//		CTexture2DD3D11Ptr
//	> Texture2DMap;
//}


_ENGINE_END


#endif