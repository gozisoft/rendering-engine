#include "librendererafx.h"
#include "AlphaState.h"

using namespace engine;

CAlphaState::CAlphaState() 
	: 
m_alphaBlendEnabled(false),
m_srcBlend(CSrcBlend::SB_SRC_ALPHA),
m_dstBlend(CDstBlend::DB_ONE_MINUS_SRC_ALPHA),
m_compareEnabled(false),
m_compareMode(CCompareMode::CM_ALWAYS),
m_ref(0)
{
	
}

CAlphaState::CAlphaState(bool blendEnable, CSrcBlend::Type srcBlend, CDstBlend::Type dstBlend,
					     bool compareEnable, CCompareMode::Type compareMode, float ref, const Colour4f& col) 
						 : 
m_alphaBlendEnabled(blendEnable),
m_srcBlend(srcBlend),
m_dstBlend(dstBlend),
m_compareEnabled(compareEnable),
m_compareMode(compareMode),
m_ref(ref),
m_constantColour(col)
{

}

CAlphaState::~CAlphaState()
{

}
