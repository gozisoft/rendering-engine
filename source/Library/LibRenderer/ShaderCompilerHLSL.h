#pragma once

#ifndef __SHADER_COMPILER_HLSL_H__
#define __SHADER_COMPILER_HLSL_H__

#include "librendererfwd.h"
#include "RendererTypes.h"

_ENGINE_BEGIN

class Shaderhlsl
{
public:
	static shader_handle compile(std::wstring const& filename);

	static shader_handle compile(CShaderVersion::Type version,
		CShaderType::Type shaderType,
		std::string const& name,
		std::string const& program);

	static shader_handle compileFromFile(CShaderVersion::Type version, 
		CShaderType::Type shaderType,
		std::string const& name,
		std::wstring const& filename);

private:
	static void D3DBlobRelease(PSHADERHANDLE handle);
};

_ENGINE_END

#endif