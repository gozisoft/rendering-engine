#pragma once

#ifndef __ISHADER_H__
#define __ISHADER_H__

#include "librendererfwd.h"
#include "RendererTypes.h"

_ENGINE_BEGIN

class IShader
{
public:
	// Pure virtual destructor
	virtual ~IShader() { /**/ } 

	// Enable shader constants
	virtual void enableConstantBuffers(const IPipeline* pipeline, IBuffer* const* cbuffers,
		size_t numbuffers, size_t startslot) = 0;

	// Enable shader resources
	virtual void enableShaderResource(const IPipeline* pipeline, IShaderResource* const* resources, 
		size_t numresources, size_t startslot) = 0;

	// Enable the shader itself.
	virtual void enableShader(const IPipeline* pipeline) = 0;

	// Function to get the shader type
	virtual CShaderType::Type GetShaderType() const = 0;
};

_ENGINE_END

#endif