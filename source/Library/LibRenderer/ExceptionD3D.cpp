#include "librendererafx.h"
#include "ExceptionD3D.h"

using namespace engine;

direct3d_error::direct3d_error(HRESULT hr) throw()
	:
exception(), mywhat(nullptr), doFree(false)
{
	LPVOID lpMsgBuf;
	DWORD numChars = FormatMessageA(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		nullptr,
		hr,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		reinterpret_cast<LPTSTR>(&lpMsgBuf),
		0,
		nullptr
		);

	if (numChars > 0)
	{
		// Point my what to the message managed by win32 runtime
		mywhat = static_cast<char*>(lpMsgBuf);
		doFree = true;
	}
}

direct3d_error::~direct3d_error()
{
	if (doFree)
	{
		LocalFree(static_cast<HLOCAL>(const_cast<char*>(mywhat)));
		mywhat = nullptr;
		doFree = false;
	}
}

const char* direct3d_error::what() const {
	// If error message not defined call parent class what()
	return mywhat ? mywhat : my_base::what();
}



