#pragma once

#ifndef __CALPHA_STATE_H__
#define __CALPHA_STATE_H__

#include "RendererTypes.h"
#include "Colour.h"

_ENGINE_BEGIN

class CAlphaState
{
public:
	CAlphaState();

	CAlphaState(bool blendEnable, CSrcBlend::Type srcBlend, CDstBlend::Type dstBlend, bool testEnable,
		CCompareMode::Type testval,	float ref, const Colour4f& col);

	~CAlphaState();

	bool				m_alphaBlendEnabled;	// default: false
	CSrcBlend::Type		m_srcBlend;				// default: SB_SRC_ALPHA
	CDstBlend::Type		m_dstBlend;				// default: DB_ONE_MINUS_SRC_ALPHA

	// Alpha Testing Related.
	bool				m_compareEnabled;	// default: false
	CCompareMode::Type	m_compareMode;		// default: TV_ALWAYS
	float				m_ref;				// default: 0 // 0 < ref < 1, lies between [0, 1]
	Colour4f			m_constantColour;
};

_ENGINE_END // namespace Engine

#endif