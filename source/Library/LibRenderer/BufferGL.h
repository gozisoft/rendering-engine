#ifndef __CBUFFER_GL_H__
#define __CBUFFER_GL_H__

#include "OpenGLFwd.h"
#include "framefwd.h"

_ENGINE_BEGIN

class CBufferGL 
{
public:
	CBufferGL(const BufferPtr& pBuffer);
	~CBufferGL();

	// Index buffer operations.
	void Enable();
	void Disable();

	// Data Access functions
	void* MapImpl(size_t offset, size_t length, Buffer::Locking mode);
	void UnmapImpl();

private:
	// handle to the opengl buffer
	GLsize_t m_buffer;

	// The type of buffer in use
	GLenum m_target;

};

#include "BufferGL.inl"


_ENGINE_END


#endif