#pragma once

#ifndef __EXCEPTION_D3D11_H__
#define __EXCEPTION_D3D11_H__

#include "Core.h"


#define WIN32_LEAN_AND_MEAN		
#include <Windows.h>
#undef WIN32_LEAN_AND_MEAN

#include <exception>

_ENGINE_BEGIN

class direct3d_error : public std::exception {
public:
	typedef std::exception my_base;

	explicit direct3d_error(HRESULT hr) throw();
	virtual ~direct3d_error();
	const char* what() const override;

private:
	const char* mywhat;
	bool doFree;
};

inline void ThrowIfFailed(HRESULT hr) {
	if (FAILED(hr)) {
		// Set a breakpoint on this line to catch DX API errors.
		throw direct3d_error(hr);
	}
}

_ENGINE_END


#endif






/*
	class HResult
	{
	public:
		// Test the HRESULT in the constructor.
		HResult(HRESULT hr = S_OK)
		{
			Assign(hr);
		}

		// Test failure of the received hr. If FAILED(hr), the function

		// AtlThrow() will be called.
		HResult &operator = (HRESULT hr)
		{
			Assign(hr);
			return *this;
		}

		// Extractor of the stored HRESULT.
		operator HRESULT ()
		{
			return m_hr;
		}

	private:
		void Assign(HRESULT hr) throw( CD3D11Exception )
		{
			if( FAILED(m_hr = hr) )
				CD3D11ThrowImpl(hr);
		}

		HRESULT m_hr; // the stored HRESULT

	};

	class CNoDirect3d : public std::exception
	{
	public:
		CNoDirect3d(const char *_Message = "No Direct3D avaialble")	throw()
			: exception(_Message)
		{	// construct from message string
		}

		virtual ~CNoDirect3d()
		{	// destroy the object
		}

	};

	class CNoCompatibleDevices : public std::exception
	{
	public:
		CNoCompatibleDevices(const char *_Message = "No Direct3D avaialble") throw()
			: exception(_Message)
		{	// construct from message string
		}

		virtual ~CNoCompatibleDevices()
		{	// destroy the object
		}

	};

	class CMediaNotFound : public std::exception
	{
	public:


	};

	class CNonZeroreFcount : public std::exception
	{
	public:


	};

	class CResettingDevice : public std::exception
	{
	public:


	};

	class CCreatingDeviceObjects : public std::exception
	{
	public:


	};

	class CResettingDeviceObjects : public std::exception
	{
	public:

	};

	class CDeviceRemoved : public std::exception
	{
	public:

	};

	class CNoDirect3d11 : public std::exception
	{
	public:
		CNoDirect3d11(const char *_Message = "Direct3D11 not avaialble : Check d3d11.dll") throw()
			: exception(_Message)
		{	// construct from message string
		}

		virtual ~CNoDirect3d11()
		{	// destroy the object
		}

	};
*/