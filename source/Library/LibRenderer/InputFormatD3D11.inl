#ifndef __CINPUT_FORMAT_D3D11_INL__
#define __CINPUT_FORMAT_D3D11_INL__

inline const CInputElement& InputFormatD3D11::GetElement(size_t index) const {
	assert(index < m_elements.size());
	return m_elements[index];
}

inline const CInputElement* InputFormatD3D11::GetElements() const {
	return &m_elements[0];
}

inline size_t InputFormatD3D11::GetElementCount() const {
	return m_elements.size();
}

inline size_t InputFormatD3D11::GetTotalOffset() const {
	size_t totalStride = 0;
	for (auto element : m_elements) {
		totalStride += element.GetOffset();
	}

	return totalStride;
}

inline size_t InputFormatD3D11::GetTotalSize() const {
	size_t totalSize = 0;
	for (auto element : m_elements) {
		totalSize += element.GetSize();
	}
	return totalSize;
}

inline ID3D11InputLayoutPtr InputFormatD3D11::get() const {
	return m_inputLayout;
}




#endif