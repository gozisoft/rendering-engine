#include "librendererafx.h"
#include "MappingD3D11.h"
#include <stdexcept>

using namespace engine;

// ----------------------------------------------------------------------------------------
DXGI_FORMAT CAPI_To_D3D11::DXGIFormat(CDataType::Type type, size_t numChannels) {
	const size_t index = numChannels - 1;

	DXGI_FORMAT byteArray[] = {
		DXGI_FORMAT_R8_SINT,
		DXGI_FORMAT_R8G8_SINT,
		DXGI_FORMAT_UNKNOWN,
		DXGI_FORMAT_R8G8B8A8_SINT
	};

	DXGI_FORMAT ubyteArray[] = {
		DXGI_FORMAT_R8_UINT,
		DXGI_FORMAT_R8G8_UINT,
		DXGI_FORMAT_UNKNOWN,
		DXGI_FORMAT_R8G8B8A8_UINT
	};

	DXGI_FORMAT shortArray[] = {
		DXGI_FORMAT_R16_SINT,
		DXGI_FORMAT_R16G16_SINT,
		DXGI_FORMAT_UNKNOWN,
		DXGI_FORMAT_R16G16B16A16_SINT,
	};

	DXGI_FORMAT ushortArray[] = {
		DXGI_FORMAT_R16_UINT,
		DXGI_FORMAT_R16G16_UINT,
		DXGI_FORMAT_UNKNOWN,
		DXGI_FORMAT_R16G16B16A16_UINT,
	};

	DXGI_FORMAT intArray[] = {
		DXGI_FORMAT_UNKNOWN,
		DXGI_FORMAT_R32G32_SINT,
		DXGI_FORMAT_R32G32B32_SINT,
		DXGI_FORMAT_R32G32B32A32_SINT,
	};

	DXGI_FORMAT uintArray[] = {
		DXGI_FORMAT_UNKNOWN,
		DXGI_FORMAT_R32G32_UINT,
		DXGI_FORMAT_R32G32B32_UINT,
		DXGI_FORMAT_R32G32B32A32_UINT,
	};

	DXGI_FORMAT floatArray[] = {
		DXGI_FORMAT_UNKNOWN,
		DXGI_FORMAT_R32G32_FLOAT,
		DXGI_FORMAT_R32G32B32_FLOAT,
		DXGI_FORMAT_R32G32B32A32_FLOAT,
	};

	switch (type) {
	case CDataType::DT_NONE: return DXGI_FORMAT_UNKNOWN;
	case CDataType::DT_BYTE: return byteArray[index];
	case CDataType::DT_UBYTE: return ubyteArray[index];
	case CDataType::DT_SHORT: return shortArray[index];
	case CDataType::DT_USHORT: return ushortArray[index];
	case CDataType::DT_INT: return intArray[index];
	case CDataType::DT_UINT: return uintArray[index];
	case CDataType::DT_FLOAT: return floatArray[index];
	default:
		throw std::runtime_error("Unknown data type [CAPI_To_D3D11::DXGIFormat]");
	};
}
// ----------------------------------------------------------------------------------------
D3D11_MAP CAPI_To_D3D11::BufferMapMode(CResourceLock::Type lock) {
	switch (lock) {
	case CResourceLock::RL_READ_ONLY: return D3D11_MAP_READ;
	case CResourceLock::RL_WRITE_ONLY: return D3D11_MAP_WRITE;
	case CResourceLock::RL_READ_WRITE: return D3D11_MAP_READ_WRITE;
	case CResourceLock::RL_DISCARD: return D3D11_MAP_WRITE_DISCARD;
	case CResourceLock::RL_NO_OVERWRITE: return D3D11_MAP_WRITE_NO_OVERWRITE;
	default:
		throw std::runtime_error("Unknown lock mode [CAPI_To_D3D11::BufferMapMode]");
	};
}
// ----------------------------------------------------------------------------------------
D3D11_USAGE CAPI_To_D3D11::BufferUsage(CResourceAccess::Type usage) {
	switch (usage) {
	case CResourceAccess::RA_GPU: return D3D11_USAGE_DEFAULT;	   // The CPU updates the resource less than once per frame
	case CResourceAccess::RA_STATIC: return D3D11_USAGE_IMMUTABLE; // The CPU does not update the resource
	case CResourceAccess::RA_DYNAMIC: return D3D11_USAGE_DYNAMIC;  // The CPU updates the resource more than once per frame
	case CResourceAccess::RA_STAGING: return D3D11_USAGE_STAGING;  // The CPU needs to read the resource
	default:
		throw std::runtime_error("Unknown buffer usage [CAPI_To_D3D11::BufferUsage]");
	};
}
// ----------------------------------------------------------------------------------------
size_t CAPI_To_D3D11::BufferBindFlags(size_t bufferType) {
	size_t bindFlags = 0;
	if (bufferType & CResourceUse::RU_VERTEX)
		bindFlags |= D3D11_BIND_VERTEX_BUFFER;

	if (bufferType & CResourceUse::RU_INDEX)
		bindFlags |= D3D11_BIND_INDEX_BUFFER;

	if (bufferType & CResourceUse::RU_CONSTANT)
		bindFlags |= D3D11_BIND_CONSTANT_BUFFER;

	if (bufferType & CResourceUse::RU_RESOURCE)
		bindFlags |= D3D11_BIND_SHADER_RESOURCE;

	if (bufferType & CResourceUse::RU_RENDERTARGET)
		bindFlags |= D3D11_BIND_RENDER_TARGET;

	if (bufferType & CResourceUse::RU_DEPTHSTENCIL)
		bindFlags |= D3D11_BIND_DEPTH_STENCIL;

	if (!bindFlags)
		throw std::runtime_error("Unknown buffer type or combination [CAPI_To_D3D11::BufferBindFlags]");

	return bindFlags;
}
// ----------------------------------------------------------------------------------------
DXGI_FORMAT CAPI_To_D3D11::DXGIFormat(CTextureFormat::Type tFormat) {
	switch (tFormat) {
	case CTextureFormat::TF_NONE: return DXGI_FORMAT_UNKNOWN;
	case CTextureFormat::TF_R32G32B32A32_TYPELESS:	return DXGI_FORMAT_R32G32B32A32_TYPELESS;
	case CTextureFormat::TF_R32G32B32A32_FLOAT: return DXGI_FORMAT_R32G32B32A32_FLOAT;
	case CTextureFormat::TF_R32G32B32A32_UINT: return DXGI_FORMAT_R32G32B32A32_UINT;
	case CTextureFormat::TF_R32G32B32A32_INT: return DXGI_FORMAT_R32G32B32A32_SINT;

	case CTextureFormat::TF_R32G32B32_TYPELESS: return DXGI_FORMAT_R32G32B32_TYPELESS;
	case CTextureFormat::TF_R32G32B32_FLOAT: return DXGI_FORMAT_R32G32B32_FLOAT;
	case CTextureFormat::TF_R32G32B32_UINT: return DXGI_FORMAT_R32G32B32_UINT;
	case CTextureFormat::TF_R32G32B32_INT: return DXGI_FORMAT_R32G32B32_SINT;

	case CTextureFormat::TF_R32G32_TYPELESS: return DXGI_FORMAT_R32G32_TYPELESS;
	case CTextureFormat::TF_R32G32_FLOAT: return DXGI_FORMAT_R32G32_FLOAT;
	case CTextureFormat::TF_R32G32_UINT: return DXGI_FORMAT_R32G32_UINT;
	case CTextureFormat::TF_R32G32_INT: return DXGI_FORMAT_R32G32_SINT;

	case CTextureFormat::TF_R32_TYPELESS: return DXGI_FORMAT_R32_TYPELESS;
	case CTextureFormat::TF_R32_FLOAT: return DXGI_FORMAT_R32_FLOAT;

	case CTextureFormat::TF_R8G8B8A8_TYPELESS: return DXGI_FORMAT_R8G8B8A8_TYPELESS;
	case CTextureFormat::TF_R8G8B8A8_UINT: return DXGI_FORMAT_R8G8B8A8_UINT;
	case CTextureFormat::TF_R8G8B8A8_INT: return DXGI_FORMAT_R8G8B8A8_SINT;
	case CTextureFormat::TF_R8G8B8A8_UNORM: return DXGI_FORMAT_R8G8B8A8_UNORM;
	case CTextureFormat::TF_R8G8B8A8_SNORM: return DXGI_FORMAT_R8G8B8A8_SNORM;

	case CTextureFormat::TF_D32_FLOAT_S8X24_UINT: return DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	case CTextureFormat::TF_D24_UNORM_S8_FLOAT: return DXGI_FORMAT_D24_UNORM_S8_UINT;
	case CTextureFormat::TF_D32_FLOAT: return DXGI_FORMAT_D32_FLOAT;

	default:
		throw std::runtime_error("Unknown texture format [CAPI_To_D3D11::DXGIFormat]");
	}
}
//----------------------------------------------------------------------------------------
D3D_PRIMITIVE_TOPOLOGY CAPI_To_D3D11::PrimitiveType(CPrimitiveType::Type primitiveType) {
	switch (primitiveType) {
	case CPrimitiveType::PT_NONE: return D3D_PRIMITIVE_TOPOLOGY_UNDEFINED;
	case CPrimitiveType::PT_POLYPOINTS:	return D3D_PRIMITIVE_TOPOLOGY_POINTLIST;
	case CPrimitiveType::PT_POLYLINE_CLOSED: return D3D_PRIMITIVE_TOPOLOGY_LINELIST;
	case CPrimitiveType::PT_POLYLINE_OPEN: return D3D_PRIMITIVE_TOPOLOGY_LINESTRIP;
		//case CPrimitiveType::PT_TRIANGLE: return D3D_PRIMITIVE_TOPOLOGY_UNDEFINED;
	case CPrimitiveType::PT_TRIMESH: return D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	case CPrimitiveType::PT_TRISTRIP: return D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
		//case CVisualNode::PT_TRIFAN: return D3D_PRIMITIVE_TOPOLOGY_UNDEFINED;
	default:
		throw std::runtime_error("Unknown primitive type [CAPI_To_D3D11::PrimitiveType]");
	};
}
//----------------------------------------------------------------------------------------
DXGI_SWAP_EFFECT CAPI_To_D3D11::SwapEffect(CBufferSwap::Type type) {
	switch (type) {
	case CBufferSwap::BS_DISCARD: return DXGI_SWAP_EFFECT_DISCARD;
	case CBufferSwap::BS_SEQUENTIAL: return DXGI_SWAP_EFFECT_SEQUENTIAL;
	case CBufferSwap::BS_FLIP: return DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
	default:
		throw std::runtime_error("Unknown swap effect type [CAPI_To_D3D11::SwapEffect]");
	};
}

D3D11_FILL_MODE CAPI_To_D3D11::FillMode(RasterizerState::FillMode fill_mode) {
	switch (fill_mode) {
	case RasterizerState::FILL_SOLID: return D3D11_FILL_SOLID;
	case RasterizerState::FILL_WIREFRAME: return D3D11_FILL_WIREFRAME;
	
	default:
		throw std::runtime_error("Unknown fille mode [CAPI_To_D3D11::FillMode]");
	};
}

D3D11_CULL_MODE CAPI_To_D3D11::CullMode(RasterizerState::CullMode cull_mode) {
	switch (cull_mode) {
	case RasterizerState::CULL_NONE: return D3D11_CULL_NONE;
	case RasterizerState::CULL_FRONT: return D3D11_CULL_FRONT;
	case RasterizerState::CULL_BACK: return D3D11_CULL_BACK;
	default:
		throw std::runtime_error("Unknown cull mode [CAPI_To_D3D11::CullMode]");
	};
}

// ----------------------------------------------------------------------------------------
//
// Back to API Mapping
//
// ----------------------------------------------------------------------------------------
CShaderVersion::Type CD3D11_To_API::FeatureLevel(D3D_FEATURE_LEVEL level) {
	switch (level) {
	case D3D_FEATURE_LEVEL_9_1: return CShaderVersion::SV_2_0;
	case D3D_FEATURE_LEVEL_9_2: return CShaderVersion::SV_2_0;
	case D3D_FEATURE_LEVEL_9_3: return CShaderVersion::SV_3_0;
	case D3D_FEATURE_LEVEL_10_0: return CShaderVersion::SV_4_0;
	case D3D_FEATURE_LEVEL_10_1: return CShaderVersion::SV_4_1;
	case D3D_FEATURE_LEVEL_11_0: return CShaderVersion::SV_5_0;
	default:
		throw std::runtime_error("Unknown Feature Level [CAPI_To_D3D11::FeatureLevel]");
	};
}
// ----------------------------------------------------------------------------------------
CDataType::Type CD3D11_To_API::VertexType(DXGI_FORMAT format) {
	switch (format) {
	case DXGI_FORMAT_R8_SINT:
	case DXGI_FORMAT_R8G8_SINT:
	case DXGI_FORMAT_R8G8B8A8_SINT:	return CDataType::DT_BYTE;
	case DXGI_FORMAT_R8_UINT:
	case DXGI_FORMAT_R8G8_UINT:
	case DXGI_FORMAT_R8G8B8A8_UINT:	return CDataType::DT_UBYTE;
	case DXGI_FORMAT_R16_SINT:
	case DXGI_FORMAT_R16G16_SINT:
	case DXGI_FORMAT_R16G16B16A16_SINT:	return CDataType::DT_SHORT;;
	case DXGI_FORMAT_R16_UINT:
	case DXGI_FORMAT_R16G16_UINT:
	case DXGI_FORMAT_R16G16B16A16_UINT:	return CDataType::DT_USHORT;
	case DXGI_FORMAT_R32_SINT:
	case DXGI_FORMAT_R32G32_SINT:
	case DXGI_FORMAT_R32G32B32_SINT:
	case DXGI_FORMAT_R32G32B32A32_SINT:	return CDataType::DT_INT;
	case DXGI_FORMAT_R32_UINT:
	case DXGI_FORMAT_R32G32_UINT:
	case DXGI_FORMAT_R32G32B32_UINT:
	case DXGI_FORMAT_R32G32B32A32_UINT:	return CDataType::DT_UINT;
	case DXGI_FORMAT_R32_FLOAT:
	case DXGI_FORMAT_R32G32_FLOAT:
	case DXGI_FORMAT_R32G32B32_FLOAT:
	case DXGI_FORMAT_R32G32B32A32_FLOAT: return CDataType::DT_FLOAT;
	default:
		throw std::runtime_error("Unknown DXGI_FORMAT [CD3D11_To_API::ChannelCount]");
	}
}
// ----------------------------------------------------------------------------------------
size_t CD3D11_To_API::ChannelCount(DXGI_FORMAT format) {
	switch (format) {
	case DXGI_FORMAT_R32_FLOAT:
	case DXGI_FORMAT_R32_UINT:
	case DXGI_FORMAT_R32_SINT:
	case DXGI_FORMAT_R16_UINT:
	case DXGI_FORMAT_R16_SINT:
	case DXGI_FORMAT_R8_UINT:
	case DXGI_FORMAT_R8_SINT: return 1;

	case DXGI_FORMAT_R32G32_FLOAT:
	case DXGI_FORMAT_R32G32_UINT:
	case DXGI_FORMAT_R32G32_SINT:
	case DXGI_FORMAT_R16G16_UINT:
	case DXGI_FORMAT_R16G16_SINT:
	case DXGI_FORMAT_R8G8_UINT:
	case DXGI_FORMAT_R8G8_SINT: return 2;

	case DXGI_FORMAT_R32G32B32_FLOAT:
	case DXGI_FORMAT_R32G32B32_UINT:
	case DXGI_FORMAT_R32G32B32_SINT: return 3;

	case DXGI_FORMAT_R32G32B32A32_FLOAT:
	case DXGI_FORMAT_R32G32B32A32_UINT:
	case DXGI_FORMAT_R32G32B32A32_SINT:
	case DXGI_FORMAT_R16G16B16A16_SINT:
	case DXGI_FORMAT_R16G16B16A16_UINT:
	case DXGI_FORMAT_R8G8B8A8_UINT:
	case DXGI_FORMAT_R8G8B8A8_SINT:	return 4;
	default:
		throw std::runtime_error("Unknown DXGI_FORMAT [CD3D11_To_API::ChannelCount]");
	}
}
// ----------------------------------------------------------------------------------------
size_t CD3D11_To_API::SizeOfType(DXGI_FORMAT format) {
	switch (format) {
	case DXGI_FORMAT_R8_SINT:
	case DXGI_FORMAT_R8G8_SINT:
	case DXGI_FORMAT_R8G8B8A8_SINT:	return sizeof(int8_t) * ChannelCount(format);
	case DXGI_FORMAT_R8_UINT:
	case DXGI_FORMAT_R8G8_UINT:
	case DXGI_FORMAT_R8G8B8A8_UINT:	return sizeof(uint8_t) * ChannelCount(format);
	case DXGI_FORMAT_R16_SINT:
	case DXGI_FORMAT_R16G16_SINT:
	case DXGI_FORMAT_R16G16B16A16_SINT:	return sizeof(int16_t) * ChannelCount(format);
	case DXGI_FORMAT_R16_UINT:
	case DXGI_FORMAT_R16G16_UINT:
	case DXGI_FORMAT_R16G16B16A16_UINT:	return sizeof(uint16_t) * ChannelCount(format);
	case DXGI_FORMAT_R32_SINT:
	case DXGI_FORMAT_R32G32_SINT:
	case DXGI_FORMAT_R32G32B32_SINT:
	case DXGI_FORMAT_R32G32B32A32_SINT:	return sizeof(int32_t) * ChannelCount(format);
	case DXGI_FORMAT_R32_UINT:
	case DXGI_FORMAT_R32G32_UINT:
	case DXGI_FORMAT_R32G32B32_UINT:
	case DXGI_FORMAT_R32G32B32A32_UINT:	return sizeof(uint32_t) * ChannelCount(format);
	case DXGI_FORMAT_R32_FLOAT:
	case DXGI_FORMAT_R32G32_FLOAT:
	case DXGI_FORMAT_R32G32B32_FLOAT:
	case DXGI_FORMAT_R32G32B32A32_FLOAT: return sizeof(float) * ChannelCount(format);
	default:
		throw std::runtime_error("Unknown DXGI_FORMAT [CD3D11_To_API::SizeOfType]");
	}
}
// ----------------------------------------------------------------------------------------
CResourceLock::Type CD3D11_To_API::BufferMapMode(D3D11_MAP lock) {
	switch (lock) {
	case D3D11_MAP_READ: return CResourceLock::RL_READ_ONLY;
	case D3D11_MAP_WRITE: return  CResourceLock::RL_WRITE_ONLY;
	case D3D11_MAP_READ_WRITE: return CResourceLock::RL_READ_WRITE;
	case D3D11_MAP_WRITE_DISCARD: return CResourceLock::RL_DISCARD;
	case D3D11_MAP_WRITE_NO_OVERWRITE: return CResourceLock::RL_NO_OVERWRITE;
	default:
		throw std::runtime_error("Unknown lock mode [CD3D11_To_API::BufferMapMode]");
	};
}
// ----------------------------------------------------------------------------------------
CResourceAccess::Type CD3D11_To_API::BufferUsage(D3D11_USAGE usage) {
	switch (usage) {
	case D3D11_USAGE_DEFAULT: return CResourceAccess::RA_GPU;	  // The CPU updates the resource less than once per frame
	case D3D11_USAGE_IMMUTABLE: return CResourceAccess::RA_STATIC; // The CPU does not update the resource
	case D3D11_USAGE_DYNAMIC: return CResourceAccess::RA_DYNAMIC;  // The CPU updates the resource more than once per frame
	case D3D11_USAGE_STAGING: return CResourceAccess::RA_STAGING;  // The CPU needs to read the resource
	default:
		throw std::runtime_error("Unknown buffer usage [CD3D11_To_API::BufferUsage]");
	};
}
// ----------------------------------------------------------------------------------------
size_t CD3D11_To_API::BufferBindFlags(size_t bufferType) {
	size_t bindFlags = 0;
	if (bufferType & D3D11_BIND_VERTEX_BUFFER)
		bindFlags |= CResourceUse::RU_VERTEX;

	if (bufferType & D3D11_BIND_INDEX_BUFFER)
		bindFlags |= CResourceUse::RU_INDEX;

	if (bufferType & D3D11_BIND_CONSTANT_BUFFER)
		bindFlags |= CResourceUse::RU_CONSTANT;

	if (bufferType & D3D11_BIND_SHADER_RESOURCE)
		bindFlags |= CResourceUse::RU_RESOURCE;

	if (!bindFlags)
		throw std::runtime_error("Unknown buffer type or combination [CD3D11_To_API::BufferBindFlags]");

	return bindFlags;
}
// ----------------------------------------------------------------------------------------
// http://msdn.microsoft.com/en-us/library/windows/desktop/ff471324(v=vs.85).aspx
CTextureFormat::Type CD3D11_To_API::TextureFormat(DXGI_FORMAT tFormat) {
	switch (tFormat) {
	case DXGI_FORMAT_UNKNOWN:				    return CTextureFormat::TF_NONE;
	case DXGI_FORMAT_R32G32B32A32_TYPELESS:     return CTextureFormat::TF_R32G32B32A32_TYPELESS;
	case DXGI_FORMAT_R32G32B32A32_FLOAT:		return CTextureFormat::TF_R32G32B32A32_FLOAT;
	case DXGI_FORMAT_R32G32B32A32_UINT:			return CTextureFormat::TF_R32G32B32A32_UINT;
	case DXGI_FORMAT_R32G32B32A32_SINT:			return CTextureFormat::TF_R32G32B32A32_INT;

	case DXGI_FORMAT_R32G32B32_TYPELESS:		return CTextureFormat::TF_R32G32B32_TYPELESS;
	case DXGI_FORMAT_R32G32B32_FLOAT:			return CTextureFormat::TF_R32G32B32_FLOAT;
	case DXGI_FORMAT_R32G32B32_UINT:			return CTextureFormat::TF_R32G32B32_UINT;
	case DXGI_FORMAT_R32G32B32_SINT:			return CTextureFormat::TF_R32G32B32_INT;

	case DXGI_FORMAT_R32G32_TYPELESS:		    return CTextureFormat::TF_R32G32_TYPELESS;
	case DXGI_FORMAT_R32G32_FLOAT:				return CTextureFormat::TF_R32G32_FLOAT;
	case DXGI_FORMAT_R32G32_UINT:				return CTextureFormat::TF_R32G32_UINT;
	case DXGI_FORMAT_R32G32_SINT:				return CTextureFormat::TF_R32G32_INT;

	case DXGI_FORMAT_R32_FLOAT:					return CTextureFormat::TF_R32_FLOAT;

	case DXGI_FORMAT_R16G16B16A16_TYPELESS:		return CTextureFormat::TF_R16G16B16A16_TYPELESS;
	case DXGI_FORMAT_R16G16B16A16_FLOAT:		return CTextureFormat::TF_R16G16B16A16_FLOAT;
	case DXGI_FORMAT_R16G16B16A16_UINT:			return CTextureFormat::TF_R16G16B16A16_UINT;
	case DXGI_FORMAT_R16G16B16A16_SINT:			return CTextureFormat::TF_R16G16B16A16_INT;

	case DXGI_FORMAT_R16G16_TYPELESS:			return CTextureFormat::TF_R16G16_TYPELESS;
	case DXGI_FORMAT_R16G16_FLOAT:				return CTextureFormat::TF_R16G16_FLOAT;
	case DXGI_FORMAT_R16G16_UINT:				return CTextureFormat::TF_R16G16_UINT;
	case DXGI_FORMAT_R16G16_SINT:				return CTextureFormat::TF_R16G16_INT;

		// DXGI_FORMAT_R16_UNORM
		// Note  Use.r swizzle in the pixel shader to duplicate red to other components
		// to get Direct3D 9 behavior.
	case DXGI_FORMAT_R16_UNORM:					return CTextureFormat::TF_L16;
	case DXGI_FORMAT_R16_FLOAT:					return CTextureFormat::TF_R16F;

	case DXGI_FORMAT_R8G8B8A8_TYPELESS:			return CTextureFormat::TF_R8G8B8A8_TYPELESS;
	case DXGI_FORMAT_R8G8B8A8_UNORM:			return CTextureFormat::TF_R8G8B8A8_UNORM;
	case DXGI_FORMAT_R8G8B8A8_SNORM:			return CTextureFormat::TF_R8G8B8A8_SNORM;
	case DXGI_FORMAT_R8G8B8A8_UINT:				return CTextureFormat::TF_R8G8B8A8_UINT;
	case DXGI_FORMAT_R8G8B8A8_SINT:				return CTextureFormat::TF_R8G8B8A8_INT;

	case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:		return CTextureFormat::TF_D32_FLOAT_S8X24_UINT;
	case DXGI_FORMAT_D32_FLOAT:					return CTextureFormat::TF_D32_FLOAT;
	case DXGI_FORMAT_D24_UNORM_S8_UINT:			return CTextureFormat::TF_D24_UNORM_S8_FLOAT;

	default:
		throw std::runtime_error("Unknown texture format [CD3D11_To_API::TextureFormat]");
	}
}
// ----------------------------------------------------------------------------------------
CTextureDimension::Type CD3D11_To_API::TextureDimension(D3D11_RESOURCE_DIMENSION dimension) {
	switch (dimension) {
	case D3D11_RESOURCE_DIMENSION_TEXTURE1D: return CTextureDimension::TD_1D;
	case D3D11_RESOURCE_DIMENSION_TEXTURE2D: return CTextureDimension::TD_2D;
	case D3D11_RESOURCE_DIMENSION_TEXTURE3D: return CTextureDimension::TD_3D;
	default:
		throw std::runtime_error("Unknown texture dimension [CD3D11_To_API::TextureDimension]");
	};
}
// ----------------------------------------------------------------------------------------



//LPCSTR VertexSemanticToName(Buffer::VertexSemantic semantic)
//{
//	switch (semantic)
//	{
//	case Buffer::VS_POSITION: return "POSITION";
//	case Buffer::VS_NORMAL: return "NORMAL";
//	case Buffer::VS_TANGENT: return "TANGENT";
//	case Buffer::VS_BINORMAL: return "BINORMAL";		
//	case Buffer::VS_COLOUR:	return "COLOR";	
//	case Buffer::VS_BLEND_INDICES: return "BLENDINDICES";
//	case Buffer::VS_BLEND_WEIGHTS: return "BLENDWEIGHT";
//	case Buffer::VS_TEXTURE_COORDINATES: return "TEXCOORD";
//	case Buffer::VS_FOG: return "FOG";
//	case Buffer::VS_PSIZE: return "PSIZE";
//	default:
//		assert( false && ieS("Unknown vertex semantic") );
//		return 0;
//	};
//}
//
//Buffer::VertexSemantic D3D11NameToSemantic(LPCSTR semantic)
//{
//	if( strcmp(semantic, "POSITION") == 0 )
//		return Buffer::VS_POSITION;
//	if( strcmp(semantic, "NORMAL") == 0 )
//		return Buffer::VS_NORMAL;
//	if( strcmp(semantic, "TANGENT") == 0 )
//		return Buffer::VS_TANGENT;
//	if( strcmp(semantic, "BINORMAL") == 0 )
//		return Buffer::VS_BINORMAL;
//	if( strcmp(semantic, "COLOR") == 0 )
//		return Buffer::VS_COLOUR;
//	if( strcmp(semantic, "BLENDINDICES") == 0 )
//		return Buffer::VS_BLEND_INDICES;
//	if( strcmp(semantic, "BLENDWEIGHT") == 0 )
//		return Buffer::VS_BLEND_WEIGHTS;
//	if( strcmp(semantic, "TEXCOORD") == 0 )
//		return Buffer::VS_TEXTURE_COORDINATES;
//	if( strcmp(semantic, "FOG") == 0 )
//		return Buffer::VS_FOG;
//	if( strcmp(semantic, "PSIZE") == 0 )
//		return Buffer::VS_PSIZE;
//
//	// Keep the compiler happy
//	return Buffer::VS_POSITION;
//}

//ImageFormat D3D11DXGIFormat(DXGI_FORMAT dxFormat)
//{
//	switch (dxFormat)
//	{
//	case DXGI_FORMAT_UNKNOWN: return TF_NONE;
//	case DXGI_FORMAT_B5G6R5_UNORM: return TF_R5G6B5;
//	case DXGI_FORMAT_B5G5R5A1_UNORM: return TF_A1R5G5B5;
//	case DXGI_FORMAT_A8_UNORM: return TF_A8;
//	case DXGI_FORMAT_R8_UNORM: return TF_L8;
//	case DXGI_FORMAT_R8G8B8A8_UNORM: return TF_A8B8G8R8;
//	case DXGI_FORMAT_R16_UNORM: return TF_L16;
//	case DXGI_FORMAT_R16G16_UNORM: return TF_G16R16;
//	case DXGI_FORMAT_R16G16B16A16_UNORM: return TF_A16B16G16R16;
//	case DXGI_FORMAT_R16_FLOAT: return TF_R16F;
//	case DXGI_FORMAT_R16G16_FLOAT: return TF_G16R16F;
//	case DXGI_FORMAT_R16G16B16A16_FLOAT: return TF_A16B16G16R16F;
//	case DXGI_FORMAT_R32_FLOAT: return TF_R32F;
//	case DXGI_FORMAT_R32G32_FLOAT: return TF_G32R32F;
//	case DXGI_FORMAT_R32G32B32A32_FLOAT: return TF_A32B32G32R32F;
//	case DXGI_FORMAT_BC1_UNORM:
//	case DXGI_FORMAT_BC1_UNORM_SRGB: return TF_DXT1;
//	case DXGI_FORMAT_BC2_UNORM:
//	case DXGI_FORMAT_BC2_UNORM_SRGB: return TF_DXT3;
//	case DXGI_FORMAT_BC3_UNORM:
//	case DXGI_FORMAT_BC3_UNORM_SRGB: return TF_DXT5;
//	default:
//		assert( false && ieS("Unknown texture format: GLTextureFormat()") );
//		return TF_NONE;
//	};
//}

////----------------------------------------------------------------------------
//// Shader Mapping
////----------------------------------------------------------------------------
//Shader::VariableType D3D11VariableType(D3D_SHADER_VARIABLE_TYPE type)
//{
//	switch (type)
//	{
//	case D3D_SDT_BOOL:
//		return Shader::DT_BOOL;
//	case D3D_SDT_INT:
//		return Shader::DT_INT;
//	case D3D_SDT_UINT:
//		return Shader::DT_UINT;
//	case D3D_SDT_FLOAT:
//		return Shader::DT_FLOAT;
//	case D3D_SDT_DOUBLE:
//		return Shader::DT_DOUBLE;
//	case D3D_SDT_TEXTURE1D: 
//		return Shader::DT_TEXTURE_1D;
//	case D3D_SDT_TEXTURE2D:
//		return Shader::DT_TEXTURE_2D;
//	case D3D_SDT_TEXTURE3D:
//		return Shader::DT_TEXTURE_3D;
//	default:
//		assert(false);
//		return Shader::DT_UNKNOWN;
//	};
//}
//
//Shader::ClassType D3D11ClassType(D3D_SHADER_VARIABLE_CLASS varClass)
//{
//	switch (varClass)
//	{
//	case D3D_SVC_SCALAR:
//		return Shader::CT_SCALAR;
//	case D3D_SVC_VECTOR:
//		return Shader::CT_VECTOR;
//	case D3D_SVC_MATRIX_ROWS:
//	case D3D_SVC_MATRIX_COLUMNS:	
//		return Shader::CT_MATRIX;
//	default:
//		assert(false);
//		return Shader::CT_UNKNOWN;
//	};
//}
//
//Shader::ShaderInputType D3D11ShaderInputType(D3D_SHADER_INPUT_TYPE inputType)
//{
//	switch (inputType)
//	{
//	case D3D_SIT_CBUFFER:
//		return Shader::SIT_CBUFFER;
//	case D3D_SIT_TBUFFER:
//		return Shader::SIT_TBUFFER;
//	case D3D_SIT_TEXTURE:
//		return Shader::SIT_TEXTURE;
//	case D3D_SIT_SAMPLER:		
//	default:
//		return Shader::SIT_UNKNOWN;
//	};
//}
//
//Shader::ShaderReturnType D3D11ShaderReturnType(D3D_RESOURCE_RETURN_TYPE returnType)
//{
//	switch (returnType)
//	{
//	case D3D_RETURN_TYPE_UNORM:
//		return Shader::SRT_UNORM;
//	case D3D_RETURN_TYPE_SNORM:
//		return Shader::SRT_SNORM;
//	case D3D_RETURN_TYPE_SINT:
//		return Shader::SRT_SINT;
//	case D3D_RETURN_TYPE_UINT:
//		return Shader::SRT_UINT;
//	case D3D_RETURN_TYPE_FLOAT:
//		return Shader::SRT_FLOAT;
//	case D3D_RETURN_TYPE_MIXED:
//		return Shader::SRT_MIXED;
//	case D3D_RETURN_TYPE_DOUBLE:
//		return Shader::SRT_DOUBLE;
//	case D3D_RETURN_TYPE_CONTINUED:
//		return Shader::SRT_CONTINUED;
//	default:
//		return Shader::SRT_NONE;
//	};
//}
//
//Shader::ShaderDimensions D3D11ShaderDimensions(D3D_SRV_DIMENSION dimension)
//{
//	switch (dimension)
//	{
//	case D3D_SRV_DIMENSION_UNKNOWN:	return Shader::SD_UNKNOWN;
//	case D3D_SRV_DIMENSION_BUFFER: return Shader::SD_BUFFER;
//	case D3D_SRV_DIMENSION_TEXTURE1D: return Shader::SD_TEXTURE1D;
//	case D3D_SRV_DIMENSION_TEXTURE1DARRAY: return Shader::SD_TEXTURE1DARRAY;
//	case D3D_SRV_DIMENSION_TEXTURE2D: return Shader::SD_TEXTURE2D;
//	case D3D_SRV_DIMENSION_TEXTURE2DARRAY:
//	case D3D_SRV_DIMENSION_TEXTURE2DMS:	return Shader::SD_TEXTURE2DARRAY;
//	case D3D_SRV_DIMENSION_TEXTURE2DMSARRAY: return Shader::SD_TEXTURE2DARRAY;
//	default:
//		return Shader::SD_UNKNOWN;
//	};
//}

//Buffer::VertexType D3D11RegisterTypeToFormatType(D3D_REGISTER_COMPONENT_TYPE type, BYTE mask)
//{
//	if (mask < 1)
//	{
//		assert( false && ieS("Unknown shader input mask value : D3D11RegisterTypeToFormatType()") );
//		return Buffer::DT_UNKNOWN;
//	}
//
//	// count the used bits within the mask
//	size_t maskValue = 0;
//	for (; mask; ++maskValue)
//	{
//		mask &= (mask-1);
//	}
//
//	size_t index = 0;
//	switch (type)
//	{
//	case D3D_REGISTER_COMPONENT_UNKNOWN:
//		return Buffer::DT_UNKNOWN;
//
//	case D3D_REGISTER_COMPONENT_UINT32:
//		{
//			index = static_cast<size_t>(Buffer::DT_UINT1)-1;
//			index += maskValue;		
//		}
//		break;
//
//	case D3D_REGISTER_COMPONENT_SINT32:
//		{
//			index = static_cast<size_t>(Buffer::DT_INT1)-1;
//			index += maskValue;		
//		}
//		break;
//
//	case D3D_REGISTER_COMPONENT_FLOAT32:
//		{
//			index = static_cast<size_t>(Buffer::DT_FLOAT1)-1;
//			index += maskValue;			
//		}
//		break;
//
//	default:
//		assert( false && ieS("Unknown register component type : D3D11RegisterTypeToFormatType()") );
//		return Buffer::DT_UNKNOWN;
//	};
//
//	Buffer::VertexType finalType = (Buffer::VertexType)index;
//	return finalType;
//}
//
//std::string D3D11CompilerTarget(D3D_FEATURE_LEVEL featureLevel, CShader::ShaderType shaderType)
//{
//	LPSTR pTarget = nullptr;
//	switch (featureLevel)
//	{
//	case D3D_FEATURE_LEVEL_9_1:
//	case D3D_FEATURE_LEVEL_9_2: pTarget = "2_0"; break;
//	case D3D_FEATURE_LEVEL_9_3: pTarget = "3_0"; break;
//	case D3D_FEATURE_LEVEL_10_0: pTarget = "4_0"; break;
//	case D3D_FEATURE_LEVEL_10_1: pTarget = "4_1"; break;
//	case D3D_FEATURE_LEVEL_11_0: pTarget = "5_0"; break;
//	default:
//		assert( false && ieS("Unknown Feature Level") );
//		break;
//	};
//
//	LPSTR pType = nullptr;
//	switch (shaderType)
//	{
//	case CShader::ST_VERTEX: pType = "vs_"; break;
//	case CShader::ST_GEOMETRY: pType = "gs_"; break; // only avaiable on shader model 4 or greater
//	case CShader::ST_PIXEL: pType = "ps_"; break;
//	default:
//		assert( false && ieS("Unknown shader type") );
//		break;
//	};
//
//	// Combine the strings
//	std::stringstream sstream;
//	sstream << pType << pTarget;
//
//	// add the target on to the type
//	return sstream.str();
//}