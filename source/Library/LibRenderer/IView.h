#pragma once

#ifndef __IVIEW_H__
#define __IVIEW_H__

#include "Core.h"
#include "RTTI.h"
#include "librendererfwd.h"
#include "RendererTypes.h"


_ENGINE_BEGIN

class IView
{
public:
	// Destructor
	virtual ~IView() { /**/ }

	// Access to the enum type
	virtual CViewType::Type type() const = 0;
};

class IShaderResource : public IView
{
public:
	// Destructor
	virtual ~IShaderResource() { /**/ }

	// Function to get shader resource creation info
	virtual ShaderResourceInfo info() const = 0;

	// Clear the view's state
};

class IRenderTarget : public IView
{
public:
	// Destructor
	virtual ~IRenderTarget() { /**/ }

	// Function to get render target view creation info
	virtual RenderTargetInfo info() const = 0;
};

class IDepthStencil : public IView
{
public:
	// Clear flag type.
	enum CLEAR_FLAG
	{
		CLEAR_DEPTH = 0x1L,
		CLEAR_STENCIL = 0x2L
	};

	// Destructor
	virtual ~IDepthStencil() { /**/ }

	// Function to get depth stencil view creation info
	virtual DepthStencilInfo info() const = 0;
};

_ENGINE_END

#endif