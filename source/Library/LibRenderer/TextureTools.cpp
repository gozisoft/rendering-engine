#include "librendererafx.h"
#include "TextureTools.h"

_ENGINE_BEGIN

namespace Texture
{
	//----------------------------------------------------------------------------
	// Convert from the specified format to 32-bit RGBA color (floating point).
	//----------------------------------------------------------------------------
	void ConvertFromR32G32B32A32_FLOAT(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const float* src = reinterpret_cast<const float*>(inTexels);
		Colour* trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = *src++;		// red		
			(*trg)[1] = *src++;		// green
			(*trg)[2] = *src++;		// blue
			(*trg)[3] = *src++;		// alpha											
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR32G32B32A32(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const uint32_t* src = reinterpret_cast<const uint32_t*>(inTexels);
		Colour* trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = static_cast<float>(*src++);	// red	
			(*trg)[1] = static_cast<float>(*src++);	// green
			(*trg)[2] = static_cast<float>(*src++);	// blue
			(*trg)[3] = static_cast<float>(*src++);	// alpha									
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR32G32B32_FLOAT(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const float* src = reinterpret_cast<const float*>(inTexels);
		Colour* trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = *src++;		// red	
			(*trg)[1] = *src++;		// green
			(*trg)[2] = *src++;		// blue
			(*trg)[3] = 0.0f;		// alpha												
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR32G32B32(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const uint32_t* src = reinterpret_cast<const uint32_t*>(inTexels);
		Colour* trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = static_cast<float>(*src++);		// red	
			(*trg)[1] = static_cast<float>(*src++);		// green
			(*trg)[2] = static_cast<float>(*src++);		// blue
			(*trg)[3] = 0.0f;							// alpha												
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR32G32_FLOAT(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const float* src = reinterpret_cast<const float*>(inTexels);
		Colour* trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = *src++;		// red	
			(*trg)[1] = *src++;		// green
			(*trg)[2] = 0.0f;		// blue
			(*trg)[3] = 0.0f;		// alpha												
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR32G32(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const float* src = reinterpret_cast<const float*>(inTexels);
		Colour* trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = static_cast<float>(*src++);		// red	
			(*trg)[1] = static_cast<float>(*src++);		// green
			(*trg)[2] = 0.0f;							// blue
			(*trg)[3] = 0.0f;							// alpha												
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR8G8B8A8_UNORM(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const uint8_t* src = inTexels;
		Colour* trg = outTexels;
		float invsize_t8Max = (float)1.0f / _size_t8_MAX;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = (*src++) * invsize_t8Max;
			(*trg)[1] = (*src++) * invsize_t8Max;
			(*trg)[2] = (*src++) * invsize_t8Max;
			(*trg)[3] = (*src++) * invsize_t8Max;
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR8G8B8A8_SNORM(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const int8_t* src = reinterpret_cast<const int8_t*>(inTexels);
		Colour* trg = outTexels;
		const float invInt8Max = (float)1.0f / (_INT8_MAX - _INT8_MIN); // Same as doing [ 127 - (-128) ]
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = ( 2 * ( ( (*src++) - _INT8_MIN ) * invInt8Max ) )-1;
			(*trg)[1] = ( 2 * ( ( (*src++) - _INT8_MIN ) * invInt8Max ) )-1;
			(*trg)[2] = ( 2 * ( ( (*src++) - _INT8_MIN ) * invInt8Max ) )-1;
			(*trg)[3] = ( 2 * ( ( (*src++) - _INT8_MIN ) * invInt8Max ) )-1;
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR8G8B8A8(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const uint8_t* src = reinterpret_cast<const uint8_t*>(inTexels);
		Colour* trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = static_cast<float>(*src++);
			(*trg)[1] = static_cast<float>(*src++);
			(*trg)[2] = static_cast<float>(*src++);
			(*trg)[3] = static_cast<float>(*src++);
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR8G8B8_UNORM(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const uint8_t* src = inTexels;
		Colour* trg = outTexels;
		float invsize_t8Max = (float)1.0f / _size_t8_MAX;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = (*src++) * invsize_t8Max;
			(*trg)[1] = (*src++) * invsize_t8Max;
			(*trg)[2] = (*src++) * invsize_t8Max;
			(*trg)[3] = 0.0f;
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR8G8B8_SNORM(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const int8_t* src = reinterpret_cast<const int8_t*>(inTexels);
		Colour* trg = outTexels;
		float invInt8Max = (float)1.0f / (_INT8_MAX - _INT8_MIN);
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = ( 2 * ( ( (*src++) - _INT8_MIN ) * invInt8Max ) )-1;
			(*trg)[1] = ( 2 * ( ( (*src++) - _INT8_MIN ) * invInt8Max ) )-1;
			(*trg)[2] = ( 2 * ( ( (*src++) - _INT8_MIN ) * invInt8Max ) )-1;
			(*trg)[3] = 0.0f;
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR8G8B8(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const uint8_t* src = reinterpret_cast<const uint8_t*>(inTexels);
		Colour* trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = static_cast<float>(*src++);	// red
			(*trg)[1] = static_cast<float>(*src++);	// green
			(*trg)[2] = static_cast<float>(*src++);	// blue
			(*trg)[3] = 0.0f;						// alpha
		}
	}
	//----------------------------------------------------------------------------
	void ConvertFromR8G8(size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
	{
		const uint8_t* src = reinterpret_cast<const uint8_t*>(inTexels);
		Colour* trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++trg)
		{
			(*trg)[0] = static_cast<float>(*src++);	// red
			(*trg)[1] = static_cast<float>(*src++);	// green
			(*trg)[2] = 0.0f;						// blue
			(*trg)[3] = 0.0f;						// alpha
		}
	}

	//----------------------------------------------------------------------------
	// Convert to the specified format from 32-bit RGBA color.
	//----------------------------------------------------------------------------
	void ConvertToR32G32B32A32_FLOAT(size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		float* trg = reinterpret_cast<float*>(outTexels);
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{
			*trg++ = (*src)[0];
			*trg++ = (*src)[1];
			*trg++ = (*src)[2];
			*trg++ = (*src)[3];
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR32G32B32A32(size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		uint32_t* trg = reinterpret_cast<uint32_t*>(outTexels);
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{
			*trg++ = static_cast<uint32_t>( (*src)[0] );
			*trg++ = static_cast<uint32_t>( (*src)[1] );
			*trg++ = static_cast<uint32_t>( (*src)[2] );
			*trg++ = static_cast<uint32_t>( (*src)[3] );
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR32G32B32_FLOAT(size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		float* trg = reinterpret_cast<float*>(outTexels);
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{
			*trg++ = (*src)[0];
			*trg++ = (*src)[1];
			*trg++ = (*src)[2];
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR32G32B32(size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		uint32_t* trg = reinterpret_cast<uint32_t*>(outTexels);
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{
			*trg++ = static_cast<uint32_t>( (*src)[0] );
			*trg++ = static_cast<uint32_t>( (*src)[1] );
			*trg++ = static_cast<uint32_t>( (*src)[2] );
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR32G32_FLOAT(size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		float* trg = reinterpret_cast<float*>(outTexels);
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{
			*trg++ = (*src)[0];
			*trg++ = (*src)[1];
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR32G32(size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		uint32_t* trg = reinterpret_cast<uint32_t*>(outTexels);
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{
			*trg++ = static_cast<uint32_t>( (*src)[0] );
			*trg++ = static_cast<uint32_t>( (*src)[1] );
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR8G8B8A8_UNORM(size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		uint8_t *trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{	
			*trg++ = static_cast<uint8_t>( (*src)[0] * _size_t8_MAX );
			*trg++ = static_cast<uint8_t>( (*src)[1] * _size_t8_MAX );		
			*trg++ = static_cast<uint8_t>( (*src)[2] * _size_t8_MAX );
			*trg++ = static_cast<uint8_t>( (*src)[3] * _size_t8_MAX );
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR8G8B8A8_SNORM(size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		const float valueRange = _INT8_MAX - _INT8_MIN;
		int8_t *trg = reinterpret_cast<int8_t*>(outTexels);		
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{	
			*trg++ = static_cast<int8_t>( ( ( ( (*src)[0]+1 )*0.5f )*valueRange )+_INT8_MIN );
			*trg++ = static_cast<int8_t>( ( ( ( (*src)[1]+1 )*0.5f )*valueRange )+_INT8_MIN );		
			*trg++ = static_cast<int8_t>( ( ( ( (*src)[2]+1 )*0.5f )*valueRange )+_INT8_MIN  );
			*trg++ = static_cast<int8_t>( ( ( ( (*src)[3]+1 )*0.5f )*valueRange )+_INT8_MIN  );
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR8G8B8A8 (size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		uint8_t *trg = reinterpret_cast<uint8_t*>(outTexels);
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{
			*trg++ = static_cast<uint8_t>( (*src)[0] );
			*trg++ = static_cast<uint8_t>( (*src)[1] );
			*trg++ = static_cast<uint8_t>( (*src)[2] );
			*trg++ = static_cast<uint8_t>( (*src)[3] );
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR8G8B8_UNORM(size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		uint8_t *trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{	
			*trg++ = static_cast<uint8_t>( (*src)[0] * _size_t8_MAX );
			*trg++ = static_cast<uint8_t>( (*src)[1] * _size_t8_MAX );		
			*trg++ = static_cast<uint8_t>( (*src)[2] * _size_t8_MAX );
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR8G8B8_SNORM(size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		int8_t *trg = reinterpret_cast<int8_t*>(outTexels);
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{	
			*trg++ = static_cast<int8_t>( (*src)[0] * _INT8_MAX );
			*trg++ = static_cast<int8_t>( (*src)[1] * _INT8_MAX );		
			*trg++ = static_cast<int8_t>( (*src)[2] * _INT8_MAX );
		}
	}
	//----------------------------------------------------------------------------
	void ConvertToR8G8B8 (size_t numTexels, const Colour* inTexels, uint8_t* outTexels)
	{
		const Colour* src = inTexels;
		uint8_t *trg = outTexels;
		for (size_t i = 0; i < numTexels; ++i, ++src)
		{	
			*trg++ = static_cast<uint8_t>( (*src)[0] );
			*trg++ = static_cast<uint8_t>( (*src)[1] );		
			*trg++ = static_cast<uint8_t>( (*src)[2] );
		}
	}

} // namespace Texture

_ENGINE_END