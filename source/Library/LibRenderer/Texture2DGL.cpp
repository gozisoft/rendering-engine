#include "librendererfwd.h"
#include "Texture2DGL.h"
#include "MappingGL.h"
#include "Texture2D.h"

using namespace engine;

CTexture2DGL::CTexture2DGL(CTexture2DPtr pTexture2D) : 
m_texture(0),
m_internalFormat(0),
m_format(0),
m_type(0),
m_usage(0)
{
	m_internalFormat = GLTextureInternalFormat( pTexture2D->GetTextureFormat() );
	m_format = GLTextureFormat( pTexture2D->GetTextureFormat() );
	m_type = GLTextureType( pTexture2D->GetTextureFormat() );
	m_usage = GLBufferUsage( pTexture2D->GetUsage() );

	// Create pixel buffer objects to store the texture data.
	const size_t numMipLevels = pTexture2D->GetNumMipLevels();

	// Assign the number of mipmaps
	m_mipMaps.resize(numMipLevels);

	// Texture is immutable
	if (m_usage == GL_STATIC_DRAW)
	{




	}

	for (size_t level = 0; level < numMipLevels; ++level)
	{
		const CTexture2D::SMipMap2D& mipMap = pTexture2D->GetMipMap(level);

		m_mipMaps[level].width = static_cast<GLsizei>(mipMap.width);
		m_mipMaps[level].height = static_cast<GLsizei>(mipMap.height);
		m_mipMaps[level].numBytes = static_cast<GLsizei>(mipMap.numBytes);

        ::glGenBuffers(1, &m_buffer[level]);
        ::glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, m_buffer[level]);
        ::glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, m_mipMaps[level].numBytes, 0, m_usage);
        ::glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
	}

	// Create a texture structure.
    ::glGenTextures(1, &m_texture);
    ::glBindTexture(GL_TEXTURE_2D, m_texture);

	// Create the mipmap level structures.  No image initialization occurs.
	m_isCompressed = pTexture2D->IsCompressed();
	if (m_isCompressed)
	{
		for (size_t level = 0; level < numMipLevels; ++level)
		{
			::glCompressedTexImage2D( GL_TEXTURE_2D, level, m_internalFormat, m_mipMaps[level].width, m_mipMaps[level].height, 0,
				m_mipMaps[level].numBytes, (GLvoid*)pTexture2D->GetData(level) );
		}
	}
	else
	{
		for (size_t level = 0; level < numMipLevels; ++level)
		{
			::glTexImage2D( GL_TEXTURE_2D, level, m_internalFormat, m_mipMaps[level].width, m_mipMaps[level].height, 0, m_format,
				m_type, (GLvoid*)pTexture2D->GetData(level) );
		}
	}

	::glBindTexture(GL_TEXTURE_2D, 0);

}

CTexture2DGL::~CTexture2DGL()
{
	for (size_t level = 0; level < m_mipMaps.size(); ++level)
	{
		::glDeleteBuffers(1, &m_buffer[level]);
	}
	::glDeleteTextures(1, &m_texture);
}

void CTexture2DGL::Enable(uint32_t sampler)
{
	::glActiveTexture(GL_TEXTURE0 + sampler);
	::glBindTexture(GL_TEXTURE_2D, m_texture);
}

void CTexture2DGL::Disable(uint32_t sampler)
{
	::glActiveTexture(GL_TEXTURE0 + sampler);
	::glBindTexture(GL_TEXTURE_2D, 0);
}

GLvoid* CTexture2DGL::Lock(Buffer::Locking mode)
{

	return 0;
}

void CTexture2DGL::Unlock()
{



}