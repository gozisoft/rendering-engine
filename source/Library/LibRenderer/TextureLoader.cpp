#include "librendererafx.h"
#include "TextureLoader.h"
#include "TextureTools.h"
#include "IImageLoader.h"
#include "IImage.h"
#include "AlignedAllocator.h" 

using namespace engine;


void CopyPadded(size_t padding, size_t lineWidth, size_t height, const uint8_t* dataBegin, uint8_t* dataOut)
{
	const uint8_t* rowBegin = dataBegin;
	uint8_t* dest = dataOut;
	size_t totalRow = lineWidth + padding;

	for (size_t i = 0; i < height; ++i)
	{
		// Copy a non padded row to the destination
		std::copy(rowBegin, rowBegin + lineWidth, dest);

		// Increment the src to point to the 
		// beginning of the next row
		rowBegin += lineWidth;

		// Increment the destenation pointer
		// across a padded row
		dest += totalRow;
	}
}

void ConvertA8R8G8B8ToR8G8B8A8(size_t padding, size_t lineWidth, size_t height, const uint8_t* dataBegin, uint8_t* dataOut)
{
	const uint8_t* src = reinterpret_cast<const uint8_t*>(dataBegin);
	uint32_t* dest = reinterpret_cast<uint32_t*>(dataOut);

	for (size_t i = 0; i < height; ++i)
	{
		// Copy a non padded row to the destination
		for (size_t j = 0; j < lineWidth; ++j)
		{
			*dest++ = ( (src[3] << 24) | (src[0] << 16) | (src[1] << 8) | src[2] );	
		}

		// Increment the destenation pointer
		// across a padded row
		dest += padding;
	}
}

void ConvertB8G8R8ToR8G8B8A8(size_t padding, size_t lineWidth, size_t height, const uint8_t* dataBegin, uint8_t* dataOut)
{
	const uint8_t* src = reinterpret_cast<const uint8_t*>(dataBegin);
	uint8_t* dest = dataOut;

	for (size_t i = 0; i < height; ++i)
	{
		// Copy a non padded row to the destination
		for (size_t j = 0; j < lineWidth; ++j)
		{
			*dest++ = *src++;	
			*dest++ = *src++;	
			*dest++ = *src++;
			*dest++ = 0xFF;
		}

		// Increment the destenation pointer
		// across a padded row
		dest += padding;
	}
}

void ConvertR8G8B8ToR8G8B8A8(size_t padding, size_t lineWidth, size_t height, const uint8_t* dataBegin, uint8_t* dataOut)
{
	const uint8_t* src = reinterpret_cast<const uint8_t*>(dataBegin);
	uint8_t* dest = dataOut;

	for (size_t i = 0; i < height; ++i)
	{
		// Copy a non padded row to the destination
		for (size_t j = 0; j < lineWidth; ++j)
		{
			*dest++ = src[2];	
			*dest++ = src[1];	
			*dest++ = src[0];
			*dest++ = 0xFF;
		}

		// Increment the destenation pointer
		// across a padded row
		dest += padding;
	}
}

void ConvertR5G6B5ToR8G8B8A8(size_t padding, size_t lineWidth, size_t height, const uint8_t* dataBegin, uint8_t* dataOut)
{
	const uint16_t* rowBegin = reinterpret_cast<const uint16_t*>(dataBegin);
	uint8_t* dest = dataOut;

	double fiveBitScale = 1 / 0x1F;
	double sixBitScale = 1 / 0x3F;

	for (size_t i = 0; i < height; ++i)
	{
		// Copy a non padded row to the destination
		for (size_t j = 0; j < lineWidth; ++j, ++rowBegin)
		{
			*dest++ = _size_t8_MAX * ( ( (*rowBegin) & 0xF800 ) * fiveBitScale );	// red src
			*dest++ = _size_t8_MAX * ( ( (*rowBegin) & 0x7E0 ) * sixBitScale );	// green src
			*dest++ = _size_t8_MAX * ( ( (*rowBegin) & 0x1F ) * fiveBitScale );	// blue src
			*dest++ = 0xFF;
		}

		// Increment the destenation pointer
		// across a padded row
		dest += padding;
	}
}

void ConvertA1R5G5B5ToR8G8B8A8(size_t padding, size_t lineWidth, size_t height, const uint8_t* dataBegin, uint8_t* dataOut)
{
	const uint16_t* rowBegin = reinterpret_cast<const uint16_t*>(dataBegin);
	uint8_t* dest = dataOut;

	double fiveBitScale = 1 / 0x1F;

	for (size_t i = 0; i < height; ++i)
	{
		// Copy a non padded row to the destination
		for (size_t j = 0; j < lineWidth; ++j, ++rowBegin)
		{
			*dest++ = _size_t8_MAX * ( ( (*rowBegin) & 0xF800 ) * fiveBitScale);	// red src
			*dest++ = _size_t8_MAX * ( ( (*rowBegin) & 0x3E0 ) * fiveBitScale);	// green src
			*dest++ = _size_t8_MAX * ( ( (*rowBegin) & 0x1F ) * fiveBitScale);	// blue src
			*dest++ = _size_t8_MAX * ( (*rowBegin) & 0x8000 );
		}

		// Increment the destenation pointer
		// across a padded row
		dest += padding;
	}
}

//ITexturePtr LoadTexture(IRenderer* device, const String& filename, const DesiredTexture& textureDesc)
//{
//	// Check for a a valid device instance
//	if (!device)
//	{
//		assert( false && ieS("Invalid device instance") );
//		return nullptr;
//	}
//
//	// Attempt to load the image without fullpath
//	IImagePtr pImage = Image::LoadImageData(filename);
//	if (pImage == nullptr)
//	{
//		// Find the image file as the ".xxx" is already on the string
//		String fullFile = FindMediaFile( filename.c_str() );
//		pImage = Image::LoadImageData(filename);
//		if (pImage == nullptr)
//		{
//			assert(false && ieS("Cannot find the image file") );
//			return nullptr;
//		}
//	}
//
//	// Calc rowbytes
//	size_t rowBytes = textureDesc.width * Texture::BytesPerPixel(textureDesc.format);
//	size_t alignedRowBytes = AlignToPowerOf2(rowBytes, 4U);
//	size_t padding = alignedRowBytes - rowBytes;
//
//	// Byte aligned array of data
//	// typedef AlignedAllocator<uint8_t, 4U> ByteAlignedAllocator;
//	std::vector<uint8_t> textureData;
//	switch ( pImage->GetImageFormat() )
//	{
//	case IImage::IF_A32B32G32R32F:
//		switch (textureDesc.format)
//		{
//		case ITexture::TF_R32G32B32A32_TYPELESS: // Performas a straight copy
//			CopyPadded(padding, pImage->GetBound(1), pImage->data(), pImage->data() + pImage->GetImageSize(), &textureData[0] );
//			break;
//		case ITexture::TF_R32G32B32A32_size_t:
//			
//			break;
//
//		case ITexture::TF_R32G32B32A32_INT:
//			
//			break;
//
//		case ITexture::TF_R32G32B32_TYPELESS:
//			break;
//
//		case ITexture::TF_R32G32B32_size_t:
//			break;
//
//		case ITexture::TF_R32G32B32_INT:
//		case ITexture::TF_R8G8B8A8_INT:
//		case ITexture::TF_R8G8B8A8_size_t:
//		case ITexture::TF_R8G8B8A8_SNORM:
//		case ITexture::TF_R8G8B8A8_UNORM:
//		}
//		break;
//	case IImage::IF_A16B16G16R16F:
//	case IImage::IF_A16B16G16R16:
//	case IImage::IF_A8B8G8R8:
//	case IImage::IF_A8R8G8B8:
//	case IImage::IF_B8G8R8:
//
//
//
//	}
//
//	// Convert image into a texture
//	switch (textureDesc.format)
//	{
//	case ITexture::TF_R8G8B8A8_INT:
//	case ITexture::TF_R8G8B8A8_size_t:
//		break;
//	case ITexture::TF_R8G8B8A8_SNORM:
//		break;
//	case ITexture::TF_R8G8B8A8_UNORM:
//		break;
//
//	};
//
//	CTexturePtr pTexture = nullptr;
//	switch (pImage->GetDimension())
//	{
//	case CImage::ID_1D:
//		{
//			// Bind the texture to the render pipeline
//			CTexture1DPtr pTex1D = std::make_shared<CTexture1D>(pImage);	
//			renderResources->Bind( pTex1D.get() );
//			pTexture = pTex1D;
//		}
//		break;
//	case CImage::ID_2D:
//		{
//			// Bind the texture to the render pipeline
//			CTexture2DPtr pTex2D = std::make_shared<CTexture2D>(pImage);
//			renderResources->Bind( pTex2D.get() );
//			pTexture = pTex2D;
//		}
//		break;
//	case CImage::ID_3D:
//		break;
//	default:
//		assert( false && ieS("Somthing has gone wrong here") );
//		break;
//	};
//	
//	return pTexture;
//}

ITexturePtr LoadTexture(IRenderer* device, const String& filename)
{
	// Check for a a valid device instance
	if (!device)
	{
		assert( false && ieS("Invalid device instance") );
		return nullptr;
	}

	// Attempt to load the image without fullpath
	IImagePtr pImage = Image::LoadImageData(filename);
	if (pImage == nullptr)
	{
		// Find the image file as the ".xxx" is already on the string
		String fullFile = FindMediaFile( filename.c_str() );
		pImage = Image::LoadImageData(filename);
		if (pImage == nullptr)
		{
			assert(false && ieS("Cannot find the image file") );
			return nullptr;
		}
	}

	// Determine closest match texture format to the image format
	ITexture::TextureFormat tFormat = ITexture::TF_NONE;
	switch ( pImage->GetImageFormat() )
	{
	case IImage::IF_A16B16G16R16: tFormat = ITexture::TextureFormat::TF_R16G16B16A16_size_t; break;
	case IImage::IF_A16B16G16R16F: tFormat = ITexture::TextureFormat::TF_R16G16B16A16_FLOAT; break;
	case IImage::IF_A32B32G32R32F: tFormat = ITexture::TextureFormat::TF_R32G32B32A32_FLOAT; break;

	case IImage::IF_A8B8G8R8:
	case IImage::IF_B8G8R8: ITexture::TextureFormat::TF_R8G8B8A8_size_t; break;

	case IImage::IF_A8R8G8B8:
	case IImage::IF_R5G6B5:
	case IImage::IF_R8G8B8:
	case IImage::IF_A1R5G5B5: 
	case IImage::IF_A4R4G4B4: tFormat = ITexture::TextureFormat::TF_R8G8B8A8_size_t; break;

	default:
		assert(false && ieS("Invalid image file") );
		return nullptr;
	}

	// Dimensions of the image
	size_t width = pImage->GetBound(0);
	size_t height = pImage->GetBound(1);

	// Determine destination (texture) size with added padding for byte alignment
	size_t rowBytes = width * Texture::BytesPerPixel(tFormat);
	size_t alignedRowBytes = AlignToPowerOf2(rowBytes, 4U);
	size_t padding = alignedRowBytes - rowBytes;

	// An array used to hold texture data (includes padding)
	std::vector<uint8_t> textureData(alignedRowBytes * height);

	// Convert image data to texture data
	switch ( pImage->GetImageFormat() )
	{
	// No real work required to convert
	case IImage::IF_A8B8G8R8:
	case IImage::IF_A16B16G16R16: 
	case IImage::IF_A16B16G16R16F: 
	case IImage::IF_A32B32G32R32F: 
		CopyPadded(padding, rowBytes, height, pImage->GetData(), &textureData[0]);
		break;

	// Needs a BGR to RGB swap and normalisation of data range
	case IImage::IF_R5G6B5:
		ConvertR5G6B5ToR8G8B8A8(padding, rowBytes, height, pImage->GetData(), &textureData[0]);
		break;

	case IImage::IF_A1R5G5B5:
		ConvertA1R5G5B5ToR8G8B8A8(padding, rowBytes, height, pImage->GetData(), &textureData[0]);
		break;

	case IImage::IF_A4R4G4B4:
		break;

	// Needs a BGR to RGB swap
	case IImage::IF_R8G8B8:
		ConvertR8G8B8ToR8G8B8A8(padding, rowBytes, height, pImage->GetData(), &textureData[0]);
		break;

	case IImage::IF_A8R8G8B8:
		ConvertA8R8G8B8ToR8G8B8A8(padding, rowBytes, height, pImage->GetData(), &textureData[0]);
		break;

	// Just add an alpha channel
	case IImage::IF_B8G8R8:
		ConvertB8G8R8ToR8G8B8A8(padding, rowBytes, height, pImage->GetData(), &textureData[0]);
		break;

	default:
		assert(false && ieS("Invalid image file") );
		return nullptr;
	}

	// Time to create a texture with out data
	STexture2DInfo texInfo;


	device->Create(

}



void ConvertFromIF_A32B32G32R32FToTF_IF_A32B32G32R32_size_t(size_t padding, size_t height, const uint8_t* dataBegin, uint8_t* textureData)
{
	const float* begin = reinterpret_cast<const float*>(dataBegin);
	uint32_t* dataOut = reinterpret_cast<uint32_t*>(textureData);

	for (size_t i = 0; i < height; ++i, dataOut += padding)
	{
		(dataOut)[0] = static_cast<uint32_t>( (int)begin[0] );
		(dataOut)[1] = static_cast<uint32_t>( (int)begin[1] );
		(dataOut)[2] = static_cast<uint32_t>( (int)begin[2] );
		(dataOut)[3] = static_cast<uint32_t>( (int)begin[3] );
	}
}

template < class SrcType, class ResultType >
void ConvertFromTo(const SrcType& src, ResultType& result)
{
	size_t valueMax = std::numeric_limits<SrcType>::max();
	size_t valueMin = std::numeric_limits<SrcType>::min();
	size_t scaleMin = std::numeric_limits<ResultType>::min();
	size_t scaleMax = std::numeric_limits<ResultType>::max();
	NormaliseValue(src, result, valueMin, valueMax, scaleMin, scaleMax);
}

// Conversion function used to maintain the source's type range in scale to that of result type
template < class SrcType, class ResultType >
void NormaliseValue(const SrcType& src, ResultType& result, size_t valueMin, size_t valueMax, size_t scaleMin, size_t scaleMax)
{
	double percentageVal = static_cast<double>(src - valueMin) / valueRange;
	result = (scaleMax * percentageVal) - scaleMin;
}

void ConvertFromIF_R8G8B8ToTF_R8G8B8A8( );			// 8-bit blue | 8-bit green | 8-bit red
void ConvertFromIF_B8G8R8ToTF_R8G8B8A8( ); 			// 8-bit red  | 8-bit green | 8-bit blue
void ConvertFromIF_A8R8G8B8ToTF_R8G8B8A8( );		// 8-bit alpha | 8-bit blue | 8-bit green | 8-bit red
void ConvertFromIF_A8B8G8R8ToTF_R8G8B8A8( );		// 8-bit alpha | 8-bit red  | 8-bit green | 8-bit blue


_ENGINE_END

