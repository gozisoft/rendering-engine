#pragma once

#ifndef __RESOURCE_GL_H__
#define __RESOURCE_GL_H__

#include "IResource.h"
#include "OpenGLFwd.h"

_ENGINE_BEGIN

class CBufferGL : public IBuffer {
public:
    CBufferGL(size_t bindFlags, CResourceAccess::Type usage, size_t sizeInBytes,
              const uint8_t *data);

    ~CBufferGL();

    // Create a resource view with specified values
    unique_ptr<IShaderResource> createSRView(CTextureFormat::Type tformat, size_t elementOffset,
                                             size_t elementWidth) override;

    unique_ptr<ShaderResourceGL> _createSRView(CTextureFormat::Type tformat, size_t elementOffset,
                                               size_t elementWidth);

    // Create a resource view using whole resource
    unique_ptr<IShaderResource> createSRView() override;

    unique_ptr<ShaderResourceGL> _createSRView();

    // Creates a render target view using the whole resource
    unique_ptr<IRenderTarget> createRTView() override;

    unique_ptr<RenderTargetGL> _createRTView();

    // Open access to the data of a buffer
    void *map(IPipeline *pipeline, CResourceLock::Type lock,
              size_t &alignedSize) const override;

    // Close access to the data of a resource
    void unMap(IPipeline *pipeline) const override;

    // The data total buffer's size in bytes
    size_t size() const override;

    // Buffer type determines if its a vertex or index buffer
    size_t bindFlags() const override;

    // Describes the buffers usage with relation to the hardware
    CResourceAccess::Type usage() const override;

    // Determine the type of resource thats been created
    CResourceType::Type type() const override;

    // Access to the underlying ID3D11Buffer
    GLuint get() const;

private:
    // handle to the opengl buffer
    GLuint m_buffer;

    // The type of buffer in use
    GLenum m_target;
};

_ENGINE_END

#endif