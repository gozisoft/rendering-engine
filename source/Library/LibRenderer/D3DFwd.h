#pragma once

#ifndef __D3D_FORWARD_H__
#define __D3D_FORWARD_H__

// #pragma comment( lib, "dxerr.lib" )
#pragma comment( lib, "dxguid.lib" )
#pragma comment( lib, "d3dcompiler.lib" )
#pragma comment( lib, "dxgi.lib" )

// Remove min and max defines from any windows header.
#define NOMINMAX

#include "Core.h"
#include <dxgiformat.h>

_ENGINE_BEGIN

size_t BitsPerPixel(DXGI_FORMAT dxgiFormat);
size_t BytesPerPixel(DXGI_FORMAT format);
bool IsBlockCompressed(DXGI_FORMAT fmt);

_ENGINE_END

#endif