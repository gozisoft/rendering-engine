#ifndef CRENDER_DATA_GL_H
#define CRENDER_DATA_GL_H

#include "OpenGLFwd.h"
#include "framefwd.h"
#include "Colour.h"

_ENGINE_BEGIN

class CRenderDataGL
{
public:
	CRenderDataGL();
	~CRenderDataGL();

	// Maintain current render states to avoid redundant state changes.
	class CRenderStateGL
	{
	public:
		void Initialise (const CAlphaState* astate, const CCullState* cstate,
			const CDepthState* dstate, const COffsetState* ostate,
			const CStencilState* sstate, const CWireState* wstate);

		// AlphaState
		bool	 m_alphaBlendEnabled;
		GLenum	 m_alphaSrcBlend;
		GLenum	 m_alphaDstBlend;
		bool	 m_alphaCompareEnabled;
		GLenum   m_compareFunction;
		float	 m_alphaReference;
		Colour m_blendColour;

		// CullState
		bool	 m_cullEnabled;
		bool	 m_CCWOrder; // counter clock wise oder

		// DepthState
		bool	 m_depthEnabled;
		bool	 m_depthWriteEnabled;
		GLenum	 m_depthCompareFunction;

		// OffsetState
		bool	 m_fillEnabled;
		bool	 m_lineEnabled;
		bool	 m_pointEnabled;
		float	 m_offsetScale;
		float	 m_offsetBias;

		// StencilState
		bool     m_stencilEnabled;
		GLenum   m_stencilCompareFunction;
		GLsize_t   m_stencilReference;
		GLsize_t   m_stencilMask;
		GLsize_t   m_stencilWriteMask;
		GLenum   m_stencilOnFail;
		GLenum   m_stencilOnZFail;
		GLenum   m_stencilOnZPass;

		// WireState
		bool	 m_wireEnabled;

	private:
		DECLARE_HEAP
	};

	// The current state of the renderer
	CRenderStateGL m_currentState;

	// Render window data
	HDC	m_HDc;						// Private GDI Device Context
	HGLRC m_HRc;					// Permanent Rendering Context
	HWND m_hWnd;
	HINSTANCE m_hInst;

};

_ENGINE_END


#endif