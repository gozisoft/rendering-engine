#include "librendererafx.h"
#include "ZBufferState.h"

using namespace engine;

CZBufferState::CZBufferState() 
	:
m_isEnabled(true),
m_isWritable(true),
m_compare(CCompareMode::CM_LEQUAL)
{

}

CZBufferState::~CZBufferState()
{


}