//
// Created by Riad Gozim on 08/02/2016.
//

#ifndef RENDERER_METAL_H
#define RENDERER_METAL_H

#import "IRenderer.h"
#import <Metal/Metal.h>

_ENGINE_BEGIN

class RendererMetal: public IRenderer
{
public:

    RendererMetal();

    virtual ~RendererMetal() override;

    virtual CRenderType::Type GetRenderType() const override;

    virtual shared_ptr<IPipeline> getPipeline() const override;

    virtual unique_ptr<IShader> createShader(CShaderType::Type type, shader_handle handle) override;

    virtual unique_ptr<IInputFormat> createInputFormat(const CInputElement *elements,
                                                       size_t numElements, shader_handle handle) override;

    virtual unique_ptr<IBuffer> createBuffer(size_t bindFlags, CResourceAccess::Type usage,
                                             size_t sizeInBytes, const uint8_t *data) override;

    virtual unique_ptr<ITexture1D> createTexture(size_t bindFlags,
                                                 CResourceAccess::Type usage,
                                                 CTextureFormat::Type tformat,
                                                 const SMipMap1D *mips,
                                                 size_t numLevels,
                                                 const uint8_t *data,
                                                 size_t arraySize) override;

    virtual unique_ptr<ITexture2D> createTexture(size_t bindFlags,
                                                 CResourceAccess::Type usage,
                                                 CTextureFormat::Type tformat,
                                                 const SMipMap2D *mips,
                                                 size_t numLevels,
                                                 const uint8_t *data,
                                                 size_t arraySize) override;

private:
    id<MTLDevice> m_device;
    id<MTLCommandQueue> m_queue;

};

_ENGINE_END


#endif //ENGINE_RENDERERMETAL_H
