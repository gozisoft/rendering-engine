#include "librendererafx.h"
#include "RasterizerStateD3D11.h"

using namespace engine;

RasterizerStateD3D11::RasterizerStateD3D11(ID3D11RasterizerStatePtr const& state) : m_state(state)
{
}

RasterizerStateD3D11::~RasterizerStateD3D11()
{
}