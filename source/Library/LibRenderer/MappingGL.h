#pragma once

#ifndef MAPPING_GL_H
#define MAPPING_GL_H

#include "OpenGLFwd.h"
#include "RendererTypes.h"

_ENGINE_BEGIN

class ApiToGL
{
public:
    // Input Format Mapping
    static GLenum BufferUsage(CResourceAccess::Type usage);
    static GLenum BufferMapMode(CResourceLock::Type lock);
    static GLenum BufferBindFlags(size_t bufferType);

    // Texture Format Mapping
    static GLenum TextureFormat(CTextureFormat::Type tFormat);

    // Render Mapping
    static GLenum PrimitiveType(CPrimitiveType::Type primitiveType);

    // Swap chain mode mapping
    // static DXGI_SWAP_EFFECT SwapEffect(CBufferSwap::Type type);
};

class GlToApi
{
public:
    static size_t BufferBindFlags(GLenum bufferType);


};


//----------------------------------------------------------------------------
// Texture Mapping
//----------------------------------------------------------------------------
/*GLenum GLTextureInternalFormat(ImageFormat tFormat);
GLenum GLTextureFormat(ImageFormat tFormat);
GLenum GLTextureTarget(CTexture::TextureType textureType);
GLenum GLTextureType(ImageFormat tFormat);*/


_ENGINE_END

#endif