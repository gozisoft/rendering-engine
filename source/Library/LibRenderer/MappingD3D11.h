#pragma once

#ifndef __MAPPING_D3D11_H__
#define __MAPPING_D3D11_H__

#include "D3D11Fwd.h"
#include "RendererTypes.h"

_ENGINE_BEGIN

class CAPI_To_D3D11
{
public:
	// Input Format Mapping
	static DXGI_FORMAT DXGIFormat(CDataType::Type type, size_t numChannels);

	// Buffer Mapping
	static D3D11_MAP BufferMapMode(CResourceLock::Type lock);
	static D3D11_USAGE BufferUsage(CResourceAccess::Type usage);
	static size_t BufferBindFlags(size_t bufferType);

	// Texture Format Mapping
	static DXGI_FORMAT DXGIFormat(CTextureFormat::Type tFormat);

	// Render Mapping
	static D3D_PRIMITIVE_TOPOLOGY PrimitiveType(CPrimitiveType::Type primitiveType);

	// Swap chain mode mapping
	static DXGI_SWAP_EFFECT SwapEffect(CBufferSwap::Type type);

	// RasterizerState
	static D3D11_FILL_MODE FillMode(RasterizerState::FillMode fill_mode);
	static D3D11_CULL_MODE CullMode(RasterizerState::CullMode cull_mode);	
};

//
// Mapping back to API
//
class CD3D11_To_API
{
public:
	// Hardware Feature Mapping
	static CShaderVersion::Type FeatureLevel(D3D_FEATURE_LEVEL level);

	// Inputformat
	static CDataType::Type VertexType(DXGI_FORMAT format);
	static size_t ChannelCount(DXGI_FORMAT format);
	static size_t SizeOfType(DXGI_FORMAT format);

	// Buffer Mapping
	static CResourceLock::Type BufferMapMode(D3D11_MAP lock);
	static CResourceAccess::Type BufferUsage(D3D11_USAGE usage);
	static size_t BufferBindFlags(size_t bufferType);

	// Texture Format Mapping
	static CTextureFormat::Type TextureFormat(DXGI_FORMAT tFormat);
	static CTextureDimension::Type TextureDimension(D3D11_RESOURCE_DIMENSION dimension);
};


_ENGINE_END

#endif

////----------------------------------------------------------------------------
//// Shader Mapping
////----------------------------------------------------------------------------
//Shader::VariableType D3D11VariableType(D3D_SHADER_VARIABLE_TYPE type);
//Shader::ClassType D3D11ClassType(D3D_SHADER_VARIABLE_CLASS varClass);
//Shader::ShaderInputType D3D11ShaderInputType(D3D_SHADER_INPUT_TYPE inputType);
//Shader::ShaderReturnType D3D11ShaderReturnType(D3D_RESOURCE_RETURN_TYPE returnType);
//Shader::ShaderDimensions D3D11ShaderDimensions(D3D_SRV_DIMENSION dimension);

//Buffer::VertexType D3D11RegisterTypeToFormatType(D3D_REGISTER_COMPONENT_TYPE type, BYTE mask);
//std::string D3D11CompilerTarget(D3D_FEATURE_LEVEL featureLevel, CShader::ShaderType shaderType);