//
// Created by Riad Gozim on 21/12/2015.
//
#include "librendererafx.h"
#include "ExceptionGL.h"

using namespace engine;

opengl_error::opengl_error(GLenum error) throw()
        : std::runtime_error(nullptr), mywhat(getErrorString(error)) {

}

opengl_error::opengl_error(const std::string &error) throw()
        : std::runtime_error(error), mywhat(error) {

}

opengl_error::~opengl_error() throw() {

}

const char *opengl_error::what() const throw() {
    return mywhat.c_str();
}

const char *opengl_error::getErrorString(GLenum error) const {
    switch (error) {
        case GL_NO_ERROR:
            return "No error has been recorded. The value of this symbolic constant is guaranteed to be 0.";
        case GL_INVALID_ENUM:
            return "An unacceptable value is specified for an enumerated argument. "
                    "The offending command is ignored and has no other side effect than to set the error flag.";
        case GL_INVALID_VALUE:
            return "A numeric argument is out of range. "
                    "The offending command is ignored and has no other side effect than to set the error flag.";
        case GL_INVALID_OPERATION:
            return "The specified operation is not allowed in the current state. "
                    "The offending command is ignored and has no other side effect than to set the error flag.";
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            return "The framebuffer object is not complete. "
                    "The offending command is ignored and has no other side effect than to set the error flag.";
        case GL_OUT_OF_MEMORY:
            return "There is not enough memory left to execute the command. "
                    "The state of the GL is undefined, except for the state of the error flags, "
                    "after this error is recorded.";
        default:
            return "";
    }
}


