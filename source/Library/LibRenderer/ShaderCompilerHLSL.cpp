#include "librendererafx.h"
#include "ShaderCompilerHLSL.h"
#include "ExceptionD3D.h"
#include <sstream>
#include <D3Dcompiler.h>

using namespace engine;

DWORD getShaderFlag()
{
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

#if defined(RE_USE_ROW_MAJOR)
	dwShaderFlags |= D3DCOMPILE_PACK_MATRIX_ROW_MAJOR;
#else
	dwShaderFlags |= D3DCOMPILE_PACK_MATRIX_COLUMN_MAJOR;
#endif
	return dwShaderFlags;
}

std::string getCompileTarget(CShaderVersion::Type version, CShaderType::Type shaderType)
{
	char* pTarget = nullptr;
	switch (version)
	{
	case CShaderVersion::SV_2_0: pTarget = "2_0";
		break;
	case CShaderVersion::SV_3_0: pTarget = "3_0";
		break;
	case CShaderVersion::SV_4_0: pTarget = "4_0";
		break;
	case CShaderVersion::SV_4_1: pTarget = "4_1";
		break;
	case CShaderVersion::SV_5_0: pTarget = "5_0";
		break;
	default:
		assert(false && ieS("Unknown Feature Level"));
		break;
	};

	char* pType = nullptr;
	switch (shaderType)
	{
	case CShaderType::ST_VERTEX: pType = "vs_";
		break;
	case CShaderType::ST_GEOMETRY: pType = "gs_";
		break; // only available on shader model 4 or greater
	case CShaderType::ST_PIXEL: pType = "ps_";
		break;
	default:
		assert(false && ieS("Unknown shader type"));
		break;
	};

	// Combine the strings
	std::stringstream sstream;
	sstream << pType << pTarget;

	// add the target on to the type
	return sstream.str();
}

shader_handle Shaderhlsl::compile(std::wstring const& filename)
{
	ID3DBlob* pShaderBlob = nullptr;
	ThrowIfFailed(D3DReadFileToBlob(filename.c_str(), &pShaderBlob));

	// Convert the type
	auto handle = reinterpret_cast<PSHADERHANDLE>(pShaderBlob);

	// Return the handle, unfortunately you lose type info (HANDLE could you boost any instead)
	return shader_handle(handle, D3DBlobRelease);
}

shader_handle Shaderhlsl::compile(CShaderVersion::Type version, CShaderType::Type shaderType,
	const std::string& name, const std::string& program)
{
	// Compile the shader with Direct3D
	auto hlslTarget = getCompileTarget(version, shaderType);
	auto shaderFlags = getShaderFlag();

	ID3DBlob* pShaderBlob = nullptr;
	ID3DBlob* pErrorBlob = nullptr;
	HRESULT hr = ::D3DCompile(program.c_str(),
		program.size(),
		nullptr,
		nullptr,
		nullptr,
		name.c_str(),
		hlslTarget.c_str(),
		shaderFlags,
		0,
		&pShaderBlob,
		&pErrorBlob);

	// Error check
	if (FAILED(hr))
	{
		if (pErrorBlob != nullptr)
		{
			OutputDebugStringA((LPCSTR)pErrorBlob->GetBufferPointer());
			pErrorBlob->Release(); // No longer used
			throw direct3d_error(hr);
		}
	}

	// Convert the type
	auto handle = reinterpret_cast<PSHADERHANDLE>(pShaderBlob);

	// Return the handle, unfortunately you lose type info (HANDLE could you boost any instead)
	return shader_handle(handle, D3DBlobRelease);
}

shader_handle Shaderhlsl::compileFromFile(CShaderVersion::Type version,
	CShaderType::Type shaderType,
	std::string const& name,
	std::wstring const& filename)
{
	std::string hlslTarget = getCompileTarget(version, shaderType);
	DWORD shaderFlags = getShaderFlag();

	ID3DBlob* pShaderBlob = nullptr;
	ID3DBlob* pErrorBlob = nullptr;
	HRESULT hr = D3DCompileFromFile(
		filename.c_str(), // filename
		nullptr, // defines
		nullptr, // includes
		name.c_str(), // entry point
		hlslTarget.c_str(), // target
		shaderFlags, // shader compile flags
		0, // effect compile flags
		&pShaderBlob,
		&pErrorBlob
	);

	// Error check
	if (FAILED(hr))
	{
		if (pErrorBlob != nullptr)
		{
			OutputDebugStringA((LPCSTR)pErrorBlob->GetBufferPointer());
			pErrorBlob->Release(); // No longer used
		}

		throw direct3d_error(hr);
	}

	// Convert the type
	auto handle = reinterpret_cast<PSHADERHANDLE>(pShaderBlob);

	// Return the handle, unfortunately you lose type info (HANDLE could you boost any instead)
	return shader_handle(handle, D3DBlobRelease);
}

// Function for the shared_ptr handle
void Shaderhlsl::D3DBlobRelease(PSHADERHANDLE handle)
{
	ID3DBlob* blob = reinterpret_cast<ID3DBlob*>(handle);
	if (!blob)
		throw std::runtime_error("Bad cast in deletion of D3DBlobRelease");

	blob->Release();
}

