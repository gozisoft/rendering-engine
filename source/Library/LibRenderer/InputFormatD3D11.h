#pragma once

#ifndef __CINPUT_FORMAT_D3D11_H__
#define __CINPUT_FORMAT_D3D11_H__

#include "D3D11Fwd.h"
#include "IInputFormat.h"
#include <vector>

_ENGINE_BEGIN

class InputFormatD3D11 : public IInputFormat
{
public:
	// Element container
	typedef std::vector<CInputElement> ElementContainer;

	// Constructor
	InputFormatD3D11(const CInputElement* elements, size_t numElements,
		ID3D11InputLayoutPtr inputLayout);

	// Destructor
	~InputFormatD3D11();

	// Get an element by its semantic type
	const CInputElement& GetElementBySemantic(const std::string& semantic,
		size_t semanticIndex = 0) const override;

	// Get an element based on its position in the array
	const CInputElement& GetElement(size_t pos) const override;

	// Get all the elements
	const CInputElement* GetElements() const override;

	// Number of elements within the vertex format, best used with GetElements()
	// for copying the list of elements
	size_t GetElementCount() const override;

	// Lookup the number of active element semantics. Use when looking for the number
	// of textures or colours semenatics within the vertex format.
	size_t GetElementSemanticCount(const std::string& semantic) const override;
	
	// The total length of all the element types combined
	size_t GetTotalOffset() const override;

	// The total size of all the elements combined
	size_t GetTotalSize() const override;

	// Lookup and element list by its stream index
	// void GetElementsByStream(size_t streamindex, ElementList& elements);

	// Get access to the resource
	ID3D11InputLayoutPtr get() const;

private:
	// An array of elements that make up the input layout
	ElementContainer m_elements;

	// Direct3d resource pointer
	ID3D11InputLayoutPtr m_inputLayout;
};

#include "InputFormatD3D11.inl"

_ENGINE_END

#endif