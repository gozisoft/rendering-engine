#pragma once

#ifndef IPIPELINE_H
#define IPIPELINE_H

#include "librendererfwd.h"
#include "RendererTypes.h"
#include <vector>
#include <array>

_ENGINE_BEGIN

// Controls the state of the graphics pipeline
class IPipeline {
public:
	// Clear stencil state types
	enum ClearState {
		CS_DEPTH = 0,
		CS_STENCIL = 1
	};

	class BufferDesc {
	public:
		std::shared_ptr<IBuffer> bufferPtr;
		size_t stride;
		size_t offset;
	};

	typedef std::vector<BufferDesc> buffer_descs;

	// Virtual destructor
	virtual ~IPipeline() { /**/ }

	// Flush the resources contained in the pipeline.
	virtual void flush() = 0;

	// Clear the state of the pipeline
	virtual void ClearState() = 0;

	// Clear a render target
	virtual void Clear(const IRenderTarget* renderTarget, const float* rgba) = 0;

	virtual void Clear(shared_ptr<IRenderTarget> renderTarget, const float* rgba) = 0;

	// Clears a depth stencil
	virtual void Clear(const IDepthStencil* depthStencil, uint32_t stateFlags, float depth,
		uint8_t stencil) = 0;

	// Enable a shader input layout.
	virtual void enableInputLayout(const IInputFormat* inputformat) = 0;

	virtual void enableInputLayout(std::shared_ptr<IInputFormat> const& inputformat) = 0;

	// Enable a bunch of vertex buffers (Input assembler stage)
	virtual void enableVertexBuffers(const IBuffer** vbuffers,
		size_t numbuffers,
		size_t startslot,
		size_t* strides,
		size_t* offsets) = 0;

	virtual void enableVertexBuffers(std::vector<BufferDesc> const& descriptors,
		size_t startslot) = 0;

	// Enable an index buffer (Input assembler stage)
	virtual void enableIndexBuffer(const IBuffer* ibuffer,
		size_t stide, size_t offset) = 0;

	virtual void enableIndexBuffer(std::shared_ptr<IBuffer> const& ibuffer,
		size_t stide, size_t offset) = 0;

	// Enable a constant buffer (vertex shader pipeline stage)
	virtual void enableConstantBuffer(const IBuffer** cbuffers, size_t numbuffers,
		size_t startslot) = 0;

	virtual void enableConstantBuffer(std::vector<shared_ptr<IBuffer>> const& cbuffers,
		size_t startslot) = 0;

	// Enable a vertex shader
	virtual void enableShader(const IShader* vshader) = 0;

	virtual void enableShader(shared_ptr<IShader> const& vshader) = 0;

	// Input assembler drawing mode set function. Triangles, lines, points, etc.
	virtual void setInputDrawingMode() = 0;

	// Output merger stage
	virtual void setRenderTargets(const IRenderTarget** renderTargets, size_t num,
		const IDepthStencil* depthStencil) = 0;

	virtual void setRenderTargets(std::vector<shared_ptr<IRenderTarget>> const& renderTargets,
		shared_ptr<IDepthStencil> const& depthStencil) = 0;

	// Set the rendering portion of the screen
	virtual void SetViewports(const ViewPort* viewports, size_t numViewPorts) = 0;

	// Drawing function.
	virtual void drawIndexd(size_t indexCount, size_t startIndex) = 0;

	// Draw vertex function.
	virtual void draw(size_t count, size_t startVertex) = 0;
};

_ENGINE_END

#endif