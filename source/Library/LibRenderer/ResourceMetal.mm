//
// Created by Riad Gozim on 04/02/2016.
//

#include "ResourceMetal.h"

using namespace engine;

BufferMetal::BufferMetal(id<MTLBuffer> buffer, size_t bindFlags)
    : m_buffer(buffer), m_bindFlags(bindFlags)
{

}

BufferMetal::~BufferMetal()
{
}

std::unique_ptr<IShaderResource> BufferMetal::createSRView()
{
    return unique_ptr<IShaderResource>();
}

std::unique_ptr<IRenderTarget> BufferMetal::createRTView()
{
    return unique_ptr<IRenderTarget>();
}

std::unique_ptr<IShaderResource> BufferMetal::createSRView(CTextureFormat::Type tformat,
                                              size_t elementOffset,
                                              size_t elementWidth)
{
    return unique_ptr<IShaderResource>();
}

void *BufferMetal::map(IPipeline *pipeline, CResourceLock::Type lock, size_t &alignedSize) const
{
    return nullptr;
}

void BufferMetal::unMap(IPipeline *pipeline) const
{

}
