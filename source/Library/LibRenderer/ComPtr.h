/*
+---------------------------------------------------------------------------+
| Descriptions:                                                             |
|                                                                           |
|    ATL has already provided such Smart Pointer from version 2.0. |
|    I just borrow its concept and implement it in PURE C++ so that it can  |
|    be compiled under any ANSI C/C++ Compliant compiler.                   |
|                                                                           |
+---------------------------------------------------------------------------+
| Note(s):                                                                  |
|                                                                           |
|    CComPtr does not support IUnknown interface. That is, you CAN'T use    |
|    CComPtr<IUnknown> to wrap an IUnknown interface pointer. Under most    |
|    conditions, IUnknown pointer will be provided by OS or HIGH layer code.|
|    So this is a reasonable LIMITATION.                                    |
|                                                                           |
+---------------------------------------------------------------------------+
*/
#pragma once
#ifndef __CCOMPTR_H___
#define __CCOMPTR_H___

#include "Core.h"

// Windows Header Files:
#include <Unknwn.h>

_ENGINE_BEGIN

// Cool little template that will invalidate the AddRef and
// Release functions of a IUnkown type.
template <class T>
class NoAddRefReleaseOnCComPtr : public T
{
private:
	virtual ULONG __stdcall AddRef() throw() = 0;
	virtual ULONG __stdcall Release() throw() = 0;
};

IUnknown* ComPtrAssign(IUnknown** pp, IUnknown* lp);
IUnknown* ComQIPtrAssign(IUnknown** pp, IUnknown* lp, REFIID riid);


template <class T>
class CComPtr
{
public:
	typedef T PtrType;
	PtrType* m_Ptr;

	CComPtr() throw() : 
	m_Ptr(nullptr)
	{

	}

	CComPtr(PtrType* lPtr) throw() : 
	m_Ptr(lPtr)
	{
		if (m_Ptr != nullptr)
			m_Ptr->AddRef();
	}

	CComPtr(const CComPtr<T>& RefComPtr) throw() :
	m_Ptr(RefComPtr.m_Ptr)
	{
		if (m_Ptr != nullptr)
			m_Ptr->AddRef();
	}

	CComPtr(::IUnknown* pIUnknown, IID iid) throw() : 
	m_Ptr(nullptr)
	{
		if (pIUnknown != nullptr)
		{
			pIUnknown->QueryInterface(iid, (void**)&m_Ptr);
		}
	}

	~CComPtr() throw()
	{
		if (m_Ptr)
			m_Ptr->Release();
	}

public:
	operator PtrType* () const throw()
    {
        return m_Ptr;
    }

	PtrType& operator * () const throw()
    {
        assert(m_Ptr != nullptr);
		return *m_Ptr;
    }

	PtrType** operator & () throw()
    {
        assert(m_Ptr == nullptr);
        return &m_Ptr;
    }

	NoAddRefReleaseOnCComPtr<T>* operator -> () const throw()
	{
		assert(m_Ptr != nullptr);
		return static_cast< NoAddRefReleaseOnCComPtr<T>* >(m_Ptr);
	}

	PtrType* operator = (PtrType* lPtr) throw()
	{
        if(*this != lPtr)
        {
    		return static_cast<PtrType*>(ComPtrAssign((IUnknown**)&m_Ptr, lPtr));
        }
        return *this;
	}

	PtrType* operator = (const CComPtr<PtrType>& lPtr) throw()
	{
		if(*this != lPtr)
        {
    		return static_cast<PtrType*>(ComPtrAssign((IUnknown**)&m_Ptr, lPtr));
        }
        return *this;
	}

	template <typename Q>
	PtrType* operator = (const CComPtr<Q>& lPtr) throw()
	{
        if( !IsEqualObject(lPtr) )
        {
    		return static_cast<PtrType*>(ComQIPtrAssign((IUnknown**)&m_Ptr, lPtr, __uuidof(PtrType)));
        }
        return *this;
	}

	// Attach to an existing interface (does not AddRef)
	void Attach(PtrType* lPtr) throw()
    {
		if (m_Ptr)
			m_Ptr->Release();
			
		m_Ptr = lPtr;	
    }

    PtrType* Detach() throw()
    {
        PtrType* lPtr = m_Ptr;
        m_Ptr = nullptr;
        return lPtr;
    }

	void Release() throw()
	{
		PtrType* pTemp = m_Ptr;
		if (pTemp)
		{
			m_Ptr = nullptr;
			pTemp->Release();
		}
	}

	// Compare two objects for equivalence
	bool IsEqualObject(::IUnknown* pOther) throw()
	{
		if (m_Ptr == nullptr && pOther == nullptr)
			return true;	// They are both NULL objects

		if (m_Ptr == nullptr || pOther == nullptr)
			return false;	// One is NULL the other is not

		CComPtr<IUnknown> punk1;
		CComPtr<IUnknown> punk2;
		p->QueryInterface(__uuidof(IUnknown), (void**)&punk1);
		pOther->QueryInterface(__uuidof(IUnknown), (void**)&punk2);
		return punk1 == punk2;
	}

};


_ENGINE_END

#endif // __CCOMPTR_H___
