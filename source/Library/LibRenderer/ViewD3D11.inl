#pragma once

#ifndef __CVIEW_D3D11_INL__
#define __CVIEW_D3D11_INL__

//----------------------------------------------------------------------------------------
inline CViewType::Type CShaderResourceD3D11::type() const
{
	return m_type;
}

inline ID3D11ShaderResourceViewPtr CShaderResourceD3D11::get() const
{
	return m_view;
}


//----------------------------------------------------------------------------------------
inline CViewType::Type CRenderTargetD3D11::type() const
{
	return m_type;
}

inline ID3D11RenderTargetViewPtr CRenderTargetD3D11::get() const
{
	return m_view;
}

//----------------------------------------------------------------------------------------
inline CViewType::Type CDepthStencilD3D11::type() const
{
	return m_type;
}

inline ID3D11DepthStencilViewPtr CDepthStencilD3D11::get() const
{
	return m_view;
}

#endif 