//
// Created by Riad Gozim on 27/12/2015.
//

#ifndef ENGINE_SHADERGL_H
#define ENGINE_SHADERGL_H

#include "IShader.h"

_ENGINE_BEGIN

class ShaderGL : public IShader {
public:
    ShaderGL();

    ~ShaderGL();

};

_ENGINE_END

#endif //ENGINE_SHADERGL_H
