#pragma once

#ifndef __IRENDERER_H__
#define __IRENDERER_H__

#include "librendererfwd.h"
#include "RendererTypes.h"

_ENGINE_BEGIN

class IRenderer {
public:
    // Virtual destructor
    virtual ~IRenderer() { /**/ }

    // Determine the type of the render used
    virtual CRenderType::Type GetRenderType() const = 0;

    // Get access to the rendering pipeline object
    virtual shared_ptr<IPipeline> getPipeline() const = 0;

    //////////////////////////////////////////////////////////////////////////////////////
    // Swap chain and back buffer access

    //// Function to set multiple swap chain buffers
    //virtual void createSwapChain(size_t* index, size_t numBackBuffers, void* hwnd,
    //	CTextureFormat::Type format=CTextureFormat::TF_R8G8B8A8_UNORM,
    //	CBufferSwap::Type swapType=CBufferSwap::BS_DISCARD) = 0;

    //// Obtain a back buffer, which can then be used to create a rendertarget
    //virtual ITexture2D* GetBackBuffer(size_t swapChainIndex, size_t bufferIndex) = 0;

    //// Flip the back buffers
    //virtual void Flush(size_t swapChainIndex, size_t syncInterval) = 0;

    // Shader creation
    virtual unique_ptr<IShader> createShader(CShaderType::Type type, shader_handle handle) = 0;

    // Input layout creation.
    virtual unique_ptr<IInputFormat> createInputFormat(const CInputElement *elements, size_t numElements,
            shader_handle handle) = 0;

    //************************************
    // Method:    createBuffer
    // FullName:  Engine::IRenderer::createBuffer
    // Access:    virtual public
    // Returns:   Engine::IBufferPtr
    // Qualifier:
    // Parameter: size_t bindFlags Used to set the type of buffer [constant, vertex, index, etc.].
    // Parameter: CResourceAccess::Type usage GPU and CPU usage for buffer access.
    // Parameter: size_t sizeInBytes Size of buffer in bytes.
    // Parameter: const uint8_t * data data to be copied into the buffer on creation.
    //************************************
    virtual unique_ptr<IBuffer> createBuffer(size_t bindFlags, CResourceAccess::Type usage,
            size_t sizeInBytes, const uint8_t *data = nullptr) = 0;

    //
    // Structured [Texture1D, Texture2D, Texture3D] buffer creation
    //

    // Texture1D creation.
    virtual unique_ptr<ITexture1D> createTexture(size_t bindFlags, CResourceAccess::Type usage,
            CTextureFormat::Type tformat, const SMipMap1D *mips, size_t numLevels,
            const uint8_t *data = nullptr, size_t arraySize = 1) = 0;

    // Texture2D creation.
    virtual unique_ptr<ITexture2D> createTexture(size_t bindFlags, CResourceAccess::Type usage,
            CTextureFormat::Type tformat, const SMipMap2D *mips, size_t numLevels,
            const uint8_t *data = nullptr, size_t arraySize = 1) = 0;
};


_ENGINE_END // namespace Engine

#endif


//// Open access to the data of a buffer
//virtual void* Map(IBuffer *pResource, CResourceLock::Type lock,
//	size_t& alignedSize) const = 0;

//// Close access to the data of a buffer
//virtual void* unMap(IBuffer *pResource) const = 0;

//// Open access to the data of a texture
//virtual void* Map(ITexture *pResource, CResourceLock::Type lock, size_t& rowPitch,
//	size_t& depthSlice) const = 0;

//// Close access to the data of a texture
//virtual void* unMap(ITexture *pResource, size_t subresource) const = 0;