#ifndef __CRESOURCE_D3D11_INL__
#define __CRESOURCE_D3D11_INL__

//---------------------------------------------------------------------------------
inline unique_ptr<IShaderResource> CBufferD3D11::createSRView(CTextureFormat::Type tformat, size_t elementOffset,
	size_t elementWidth)
{
	return _createSRView(tformat, elementOffset, elementWidth);
}

inline unique_ptr<IShaderResource> CBufferD3D11::createSRView()
{
	return _createSRView();
}

inline unique_ptr<IRenderTarget> CBufferD3D11::createRTView()
{
	return _createRTView();
}

inline size_t CBufferD3D11::size() const
{
	return static_cast<size_t>(m_desc.ByteWidth);
}

inline size_t CBufferD3D11::bindFlags() const
{
	return CD3D11_To_API::BufferBindFlags(m_desc.BindFlags);
}

inline CResourceAccess::Type CBufferD3D11::usage() const
{
	return CD3D11_To_API::BufferUsage(m_desc.Usage);
}
	
inline CResourceType::Type CBufferD3D11::type() const
{
	return CResourceType::RT_BUFFER;
}

inline ID3D11BufferPtr CBufferD3D11::get() const
{
	return m_resource;
}
//---------------------------------------------------------------------------------
inline unique_ptr<IShaderResource> CTexture1DD3D11::createSRView(CTextureFormat::Type tformat, size_t highestMip,
	size_t maxlevels)
{
	return _createSRView(tformat, highestMip, maxlevels);
}

inline unique_ptr<IShaderResource> CTexture1DD3D11::createSRView()
{
	return _createSRView();
}

inline unique_ptr<IRenderTarget> CTexture1DD3D11::createRTView()
{
	return _createRTView();
}

inline unique_ptr<IDepthStencil> CTexture1DD3D11::createDSView()
{
	return _createDSView();
}

inline size_t CTexture1DD3D11::numMips() const
{
	return static_cast<size_t>(m_desc.MipLevels);
}

inline size_t CTexture1DD3D11::width() const
{
	return static_cast<size_t>(m_desc.Width);
}

inline size_t CTexture1DD3D11::size() const
{
	size_t totalsize = 0;
	auto width = m_desc.Width;
	for (size_t i = 0; i < m_desc.MipLevels; ++i)
	{
		// Accumulate the total size
		totalsize += BitsPerPixel(m_desc.Format) * m_desc.Width;

		// shift width down a power of 2
		if (width > 1)
			width >>= 1;
	}
	
	return totalsize;
}

inline size_t CTexture1DD3D11::bindFlags() const
{
	return CD3D11_To_API::BufferBindFlags(m_desc.BindFlags);
}

inline CResourceAccess::Type CTexture1DD3D11::usage() const
{
	return CD3D11_To_API::BufferUsage(m_desc.Usage);
}

inline CResourceType::Type CTexture1DD3D11::type() const
{
	return CResourceType::RT_TEXTURE1D;
}

inline CTextureFormat::Type CTexture1DD3D11::textureFormat() const
{
	return CD3D11_To_API::TextureFormat(m_desc.Format);
}

inline CTextureDimension::Type CTexture1DD3D11::textureDimension() const
{
	return CTextureDimension::TD_1D;
}

inline bool CTexture1DD3D11::isCompressed() const
{
	return IsBlockCompressed(m_desc.Format);
}

inline ID3D11Texture1DPtr CTexture1DD3D11::get() const
{
	return m_texture;
}
//---------------------------------------------------------------------------------
inline unique_ptr<IShaderResource> CTexture2DD3D11::createSRView(CTextureFormat::Type tformat, size_t highestMip,
	size_t maxlevels)
{
	return _createSRView(tformat, highestMip, maxlevels);
}

inline unique_ptr<IShaderResource> CTexture2DD3D11::createSRView()
{
	return _createSRView();
}

inline unique_ptr<IRenderTarget> CTexture2DD3D11::createRTView()
{
	return _createRTView();
}

inline unique_ptr<IDepthStencil> CTexture2DD3D11::createDSView()
{
	return _createDSView();
}

inline size_t CTexture2DD3D11::numMips() const
{
	return static_cast<size_t>(m_desc.MipLevels);
}

inline size_t CTexture2DD3D11::width() const
{
	return static_cast<size_t>(m_desc.Width);
}

inline size_t CTexture2DD3D11::height() const
{
	return static_cast<size_t>(m_desc.Height);
}

inline size_t CTexture2DD3D11::size() const
{
	size_t totalsize = 0;
	auto width = m_desc.Width;
	auto height = m_desc.Height;
	for (size_t i = 0; i < m_desc.MipLevels; ++i)
	{
		// Accumulate the total size
		totalsize += BitsPerPixel(m_desc.Format) * m_desc.Width * m_desc.Height;

		// shift width down a power of 2
		if (width > 1)
			width >>= 1;

		// shift height down a power of 2
		if (height > 1)
			height >>= 1;
	}
	
	return totalsize;
}

inline size_t CTexture2DD3D11::bindFlags() const
{
	return CD3D11_To_API::BufferBindFlags(m_desc.BindFlags);
}

inline CResourceAccess::Type CTexture2DD3D11::usage() const
{
	return CD3D11_To_API::BufferUsage(m_desc.Usage);
}

inline CResourceType::Type CTexture2DD3D11::type() const
{
	return CResourceType::RT_TEXTURE2D;
}

inline CTextureFormat::Type CTexture2DD3D11::textureFormat() const
{
	return CD3D11_To_API::TextureFormat(m_desc.Format);
}

inline CTextureDimension::Type CTexture2DD3D11::textureDimension() const
{
	return CTextureDimension::TD_2D;
}
	
inline bool CTexture2DD3D11::isCompressed() const
{
	return IsBlockCompressed(m_desc.Format);
}

inline ID3D11Texture2DPtr CTexture2DD3D11::get() const
{
	return m_texture;
}

#endif