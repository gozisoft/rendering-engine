#pragma once

#ifndef __IINPUT_FORMAT_H__
#define __IINPUT_FORMAT_H__

#include "RendererTypes.h"
#include <string>

_ENGINE_BEGIN

class CInputElement
{
public:
	// Constructor
	CInputElement(size_t sourceIndex, size_t offset, CDataType::Type type, size_t numChannels,
		const std::string& semantic, size_t semanticIndex = 0);

	// Destructor
	~CInputElement();

	// Get the size of this input element
	size_t GetSize() const;

	// Steam index
	size_t GetStreamIndex() const;

	// Stride between elements
	size_t GetOffset() const;

	// Used to distinguish if there are more than one type of semantic 
	// i.e.
	// semantic = Texcoord, semantic index = 0
	// semantic = Texcoord, semantic index = 1
	size_t GetSemanticIndex() const;

	// Returns the number of vertex channels. These range from 1 - 4.
	size_t GetVertexChannels() const;

	// Returns the type of vertex, V_UNKNOWN = ~0, V_BYTE = 0, V_UBYTE,
	// V_SHORT, V_USHORT, V_INT, V_size_t, V_FLOAT, V_DOUBLE.
	CDataType::Type GetVertexType() const;

	// Vertex semantic is a text string to match the input assembler of 
	// a vertex shader.
	const std::string& GetVertexSemantic() const;

private:
	size_t				m_streamIndex;
	size_t				m_offset;
	size_t				m_semanticIndex;
	size_t				m_vtypeChannels;
	CDataType::Type		m_vertexType;
	std::string			m_semantic;
};

class IInputFormat
{
public:
	// default destructor
	virtual ~IInputFormat() { }

	// Get an element by its semantic type
	virtual const CInputElement& GetElementBySemantic(const std::string& semantic,
		size_t semanticIndex) const = 0;

	// Get an element based on its position in the array
	virtual const CInputElement& GetElement(size_t pos) const = 0;

	// Get all the elements
	virtual const CInputElement* GetElements() const = 0;

	// Number of elements within the vertex format, best used with GetElements()
	// for copying the list of elements
	virtual size_t GetElementCount() const = 0;

	// Lookup the number of active element semantics. Use when looking for the number
	// of textures or colours semantics within the vertex format.
	virtual size_t GetElementSemanticCount(const std::string& semantic) const = 0;
	
	// The total offset of all the element types combined
	virtual size_t GetTotalOffset() const = 0;

	// The total size of all the elements combined
	virtual size_t GetTotalSize() const = 0;

	// Lookup and element list by its stream index
	// void GetElementsByStream(size_t streamindex, ElementList& elements);
};


_ENGINE_END

#endif