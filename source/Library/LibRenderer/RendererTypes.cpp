#include "librendererafx.h"
#include "RendererTypes.h"

using namespace engine;

ViewPort::ViewPort() {
    std::fill(&m_data[0], &m_data[5], 0.0f);
}

ViewPort::ViewPort(float posX, float posY, float width,
                   float height, float minZ, float maxZ) {
    m_data[0] = posX;
    m_data[1] = posY;
    m_data[2] = width;
    m_data[3] = height;
    m_data[4] = minZ;
    m_data[5] = maxZ;
}

