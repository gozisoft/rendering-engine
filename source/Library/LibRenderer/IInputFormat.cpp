#include "librendererafx.h"
#include "IInputFormat.h"
#include <cassert>

using namespace engine;

// -----------------------------------------------------------------------------------------------------
// CInputElement
// -----------------------------------------------------------------------------------------------------
CInputElement::CInputElement(size_t sourceIndex, size_t offset, CDataType::Type type, size_t numChannels,
		const std::string& semantic, size_t semanticIndex) :
m_streamIndex(sourceIndex),
m_offset(offset),
m_vtypeChannels(numChannels),
m_vertexType(type),
m_semantic(semantic),
m_semanticIndex(semanticIndex)
{
	// Error check for the number of channels (must be between 1 and 4)
	assert( (numChannels > 0 && numChannels < 5) && ieS("Invalid number of channels") );
}

CInputElement::~CInputElement()
{

}