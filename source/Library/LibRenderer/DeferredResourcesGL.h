#ifndef __CDEFERRED_RESOURCES_GL_H__
#define __CDEFERRED_RESOURCES_GL_H__

#include "OpenGLFwd.h"
#include "IRenderResources.h"

_ENGINE_BEGIN

class CDeferredResourcesGL : public IRenderResources
{
private:
	typedef std::map< BufferPtr, CBufferGLPtr > BufferMap;
	typedef std::map< CVertexFormatPtr, CVertexFormatGLPtr > VertexFormatMap;
	typedef std::map< CTexture1DPtr, CTexture1DGLPtr > Texture1DMap;
	typedef std::map< CTexture2DPtr, CTexture2DGLPtr > Texture2DMap;

	// Resources (references for reading only)
	BufferMap& m_buffers;
	VertexFormatMap& m_vertexFormats;
	Texture1DMap& m_1DTextures;
	Texture2DMap& m_2DTextures;

	// Containts all the render commands built up within ths resource
	// CRenderCommandPtr m_renderCommands; 
	CCommandList m_renderCommands;

public:
	CDeferredResourcesGL(BufferMap& buffers, VertexFormatMap& formats,
		Texture1DMap& tex1D, Texture2DMap& tex2D);

	~CDeferredResourcesGL();

	// Command list functions
	void FinishRenderCommands(commandList& pRC); 

	// General shader
	void Bind(const CShaderPtr& pShader) {};
	void UnBind(const CShaderPtr& pShader) {};
	void Enable(const CShaderPtr& pShader);
	void Disable(const CShaderPtr& pShader);

	// General buffer (vertex, index, constant)
	void Bind(const BufferPtr& pBuffer) {};
	void UnBind(const BufferPtr& pBuffer) {};
	void* Map(const BufferPtr& pBuffer);
	void Unmap(const BufferPtr& pBuffer);
	void Enable(const BufferPtr& pBuffer);
	void Disable(const BufferPtr& pBuffer);

	// Vertex format
	void Bind(const CVertexFormatPtr& pVFormat) {};
	void UnBind(const CVertexFormatPtr& pVFormat) {};
	void Enable(const CVertexFormatPtr& pVFormat);
	void Disable(const CVertexFormatPtr& pVFormat);

/*	// Texture1D
	void Enable(CTexture1DPtr pTexture1D);
	void Disable(CTexture1DPtr pTexture1D);

	// Texture2D
	void Enable(CTexture2DPtr pTexture2D);
	void Disable(CTexture2DPtr pTexture2D); */

	// Render calls
	void Draw(const VertexBufferPtr& pVBuffer, Visual::PrimitiveType primitiveType);
	void DrawIndexd(const IndexBufferPtr& pIBuffer, Visual::PrimitiveType primitiveType);
	void DrawNode(const Visual* pNode);

};


_ENGINE_END

#endif



	/*
	
class CDeferredResourcesGL : public IRenderResources
{
private:
	typedef std::map< CBufferPtr, CBufferGLPtr > BufferMap;
	typedef std::map< CVertexFormatPtr, CVertexFormatGLPtr > VertexFormatMap;
	typedef std::map< CTexture1DPtr, CTexture1DGLPtr > Texture1DMap;
	typedef std::map< CTexture2DPtr, CTexture2DGLPtr > Texture2DMap;

	// Resources (references for reading only)
	BufferMap& m_buffers;
	VertexFormatMap& m_vertexFormats;
	Texture1DMap& m_1DTextures;
	Texture2DMap& m_2DTextures;

	// Containts all the render commands built up within ths resource
	CRenderCommandPtr m_renderCommands; 

	// Pointer the the immediate context
	CRenderResourcesGLPtr m_pImmediate;

public:
	CDeferredResourcesGL(const CRenderResourcesGLPtr& pImmediate, BufferMap& buffers,
		VertexFormatMap& formats, Texture1DMap& tex1D, Texture2DMap& tex2D);

	~CDeferredResourcesGL();

	// Command list functions
	void FinishRenderCommands(CRenderCommandPtr& pRC); 

	// Vertex shader
	void Bind(const CVertexShaderPtr& pVShader);
	void UnBind(const CVertexShaderPtr& pVShader);
	
	// Pixel Shader
	void Bind(const CPixelShaderPtr& pPShader);
	void UnBind(const CPixelShaderPtr& pPShader);

	// General buffer
	void Enable(const CBufferPtr& pBuffer);
	void Disable(const CBufferPtr& pBuffer);
	void Bind(const CBufferPtr& pBuffer);
	void UnBind(const CBufferPtr& pBuffer);
	void* Map(const CBufferPtr& pBuffer);
	void Unmap(const CBufferPtr& pBuffer);

	// Vertex format
	void Enable(const CVertexFormatPtr& pVFormat);
	void Disable(const CVertexFormatPtr& pVFormat);
	void Bind(const CVertexFormatPtr& pVFormat);
	void UnBind(const CVertexFormatPtr& pVFormat);

	// Node data
	void SetPrimitiveType(CVisualNode::PrimitiveType primitiveType);
	void SetIndexData();

};
*/