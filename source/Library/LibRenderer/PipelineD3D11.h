#pragma once

#ifndef PIPELINE_D3D11_H
#define PIPELINE_D3D11_H

#include "D3D11Fwd.h"
#include "IPipeline.h"
#include "RendererTypes.h"

_ENGINE_BEGIN

// Controls the state of the graphics pipeline
class PipelineD3D11 : public IPipeline {
public:
	// Constructor
	PipelineD3D11(ID3D11DeviceContextPtr context);

	// Destructor
	~PipelineD3D11();

	// Used to flush resources
	void flush() override;

	// Clear all pipeline states
	void ClearState() override;

	// Clear a render target
	void Clear(const IRenderTarget* renderTarget, const float* rgba) override;
	void Clear(IRenderTargetPtr renderTarget, const float* rgba) override;

	// Clears a depth stencil
	void Clear(const IDepthStencil* depthStencil, uint32_t stateFlags, float depth,
		uint8_t stencil) override;

	// Enables pipeline rasteriazation state.
	void enableRasterState(const IRasterizerState* state);

	// Enable an input format
	void enableInputLayout(const IInputFormat* inputformat) override;

	void enableInputLayout(shared_ptr<IInputFormat> const& inputformat) override;

	// Enable a bunch of vertex buffers (Input assembler stage)
	void enableVertexBuffers(const IBuffer** vbuffers, size_t numbuffers, size_t startslot,
		size_t* strides, size_t* offsets) override;

	void enableVertexBuffers(std::vector<BufferDesc> const& descriptors,
		size_t startslot) override;

	// Enable an index buffer (Input assembler stage)
	void enableIndexBuffer(const IBuffer* ibuffer, size_t stide,
		size_t offset) override;

	void enableIndexBuffer(std::shared_ptr<IBuffer> const& ibuffer,
		size_t stride, size_t offset) override;

	// Enable a constant buffer (vertex shader pipeline stage)
	void enableConstantBuffer(const IBuffer**cbuffers, size_t numbuffers, 
		size_t startslot) override;

	void enableConstantBuffer(std::vector<shared_ptr<IBuffer>> const& cbuffers,
		size_t startslot) override;

	// Enable a shader
	void enableShader(const IShader* vshader) override;

	void enableShader(shared_ptr<IShader> const& vshader) override;

	// Output merger stage
	void setRenderTargets(const IRenderTarget** renderTargets, size_t num,
		const IDepthStencil* depthStencil) override;

	void setRenderTargets(std::vector<shared_ptr<IRenderTarget>> const& renderTargets,
		shared_ptr<IDepthStencil> const& depthStencil) override;

	// Set the rendering portion of the screen
	void SetViewports(const ViewPort* viewports, size_t numViewPorts) override;

	// Sets the drawing mode to triangles, lines, points, etc.
	void setInputDrawingMode() override;

	void drawIndexd(size_t indexCount, size_t startIndex) override;

	void draw(size_t count, size_t startVertex) override;

	// Access to the device context
	ID3D11DeviceContextPtr Get() const;

private:
	ID3D11DeviceContextPtr m_context;
};

#include "PipelineD3D11.inl"	


_ENGINE_END

#endif