#pragma once

#ifndef __TYPES_API_H__
#define __TYPES_API_H__

#include "Core.h"
#include "boost/variant.hpp"

_ENGINE_BEGIN

#pragma warning( push, 1 )
#pragma warning( disable : 4482 )

// =================================================================================================================
// Rasterization state
// =================================================================================================================
class RasterizerState {
public:
	enum FillMode {
		FILL_SOLID,
		FILL_WIREFRAME
	};

	enum CullMode {
		CULL_NONE,
		CULL_FRONT,
		CULL_BACK
	};

	// Construction.
	RasterizerState();

	// Member access. The members are intended to be write-once before
	// you create an associated graphics state.
	FillMode fillMode;              // default: FILL_SOLID
	CullMode cullMode;              // default: CULL_BACK
	bool frontCCW;                  // default: true
	int depthBias;                  // default: 0
	float depthBiasClamp;           // default: 0
	float slopeScaledDepthBias;     // default: 0
	bool enableDepthClip;           // default: true
	bool enableScissor;             // default: false
	bool enableMultisample;         // default: false
	bool enableAntialiasedLine;     // default: false
};


// =================================================================================================================
// Vertex types
// =================================================================================================================
// Vertex type describes the variable type used
// within input and output format descriptors
class CDataType
{
public:
	enum Type
	{
		DT_NONE = 0,
		DT_BYTE,
		DT_UBYTE,
		DT_SHORT,
		DT_USHORT,
		DT_INT,
		DT_UINT,
		DT_BOOL,
		DT_FLOAT,
		DT_DOUBLE,
		DT_COUNT
	};

	// Size functions for variable types
	static size_t SizeOfType(CDataType::Type type);

private:
	CDataType();
};

// Cool enum type functions
template < typename T > struct Type2EnumType	  { static const CDataType::Type type = CDataType::DT_NONE; };
template <> struct Type2EnumType<int8_t>		  { static const CDataType::Type type = CDataType::DT_BYTE; };
template <> struct Type2EnumType<uint8_t>		  { static const CDataType::Type type = CDataType::DT_UBYTE; };
template <> struct Type2EnumType<int16_t>		  { static const CDataType::Type type = CDataType::DT_SHORT; };
template <> struct Type2EnumType<uint16_t>		  { static const CDataType::Type type = CDataType::DT_USHORT; };
template <> struct Type2EnumType<int32_t>		  { static const CDataType::Type type = CDataType::DT_INT; };
template <> struct Type2EnumType<uint32_t>		  { static const CDataType::Type type = CDataType::DT_UINT; };
template <> struct Type2EnumType<float>			  { static const CDataType::Type type = CDataType::DT_FLOAT; };
template <> struct Type2EnumType<double>		  { static const CDataType::Type type = CDataType::DT_DOUBLE; };
template <> struct Type2EnumType<bool>			  { static const CDataType::Type type = CDataType::DT_BOOL; };



// =================================================================================================================
// Rendering device types
// =================================================================================================================
class CRenderType
{
public:
	// Describers the renderer type, used with interface
	// factory creation method
	enum Type
	{
		RT_UNKNOWN = 0,
		RT_OPENGL,
		RT_DIRECT3D9,
		RT_DIRECT3D11
	};
private:
	CRenderType() {};
};

class CBufferSwap
{
public:
	// Specifies the back buffer swapping
	// model.
	enum Type
	{
		BS_DISCARD,
		BS_SEQUENTIAL,
		BS_FLIP
	};
private:
	CBufferSwap() {};
};

// ViewPort uses a left handed coordinate system.
// posX and posY should be the bottom left of the
// window or screen.
class ViewPort  {
public:
	// Default constructor
	ViewPort();

	ViewPort(float posX, float posY, float width,
		float height, float minZ = 0.0f, float maxZ = 1.0f);

	/*
	 * Array size should not be greater than 4.
	 * Order should be: posX, posY, width, height.
	 */
	template <typename T>
	ViewPort(const T* data, float minZ = 0.0f, float maxZ = 1.0f);

	// Copy constructor
	ViewPort(ViewPort&& viewport);

	// Assignment operator
	ViewPort& operator = (ViewPort&& viewport);

	// Calculate the aspect ratio of the viewport
	float aspect_ratio() const;

	// Public accessors
	float posX() const;
	float posY() const;
	float width() const;
	float height() const;
	float minZ() const;
	float maxZ() const;

private:
	float m_data[6];
};

// =================================================================================================================
// Resource types
// =================================================================================================================
class CResourceType
{
public:
	enum Type
	{
		RT_BUFFER,
		RT_TEXTURE1D,
		RT_TEXTURE2D,
		RT_TEXTURE3D
	};

private:
	CResourceType();
};

// Type of buffer in use (these can now be combined)
class CResourceUse
{
public:
	enum Type : size_t
	{
		RU_NONE = 0,
		RU_VERTEX = 0x1U,		// contains vertex data
		RU_INDEX = 0x2U,		// contains index data
		RU_CONSTANT = 0x4U,		// acts as a global constant
		RU_RENDERTARGET = 0x8U,
		RU_DEPTHSTENCIL = 0x10U,
		RU_RESOURCE = 0x20U		// acts as a Shader resource
	};

private:
	CResourceUse();
};

/************************************************************************/
/* Locking modes for accessing video memory for a buffer or texture.    */
/************************************************************************/
class CResourceLock
{
public:
	enum Type : size_t
	{
		RL_NONE = 0,
		RL_READ_ONLY = 0x1U,
		RL_WRITE_ONLY = 0x2U,
		RL_READ_WRITE = 0x4U,
		RL_DISCARD = 0x8U,
		RL_NO_OVERWRITE = 0x10U,
		NUM_LOCKING_STATES
	};

private:
	CResourceLock() {};
};

/************************************************************************/
/* Usage flags for vertex buffers, index buffers, and textures.			*/
/************************************************************************/
class CResourceAccess
{
public:
	enum Type : size_t
	{
		RA_NONE = 0,
		RA_GPU = 0x1U,					// A resource that requires read and write access by the GPU.
		RA_STATIC = 0x2U,				// CPU can write to GPU on creation. Can only be read by the GPU.
		RA_DYNAMIC = 0x4U,				// accessible by both the GPU and the CPU (write only)
		RA_STAGING = 0x8U				// data transfer (copy) from the GPU to the CPU.
	};

private:
	CResourceAccess() {};
};
// =================================================================================================================
// Texture resource types
// =================================================================================================================
// Texture Dimensions
class CTextureDimension
{
public:
	enum Type
	{
		TD_NONE,
		TD_1D,
		TD_2D,
		TD_3D,
		TD_CUBE
	};
private:
	CTextureDimension() {};
};

class CTextureFormat
{
public:
	// Currently supported formats. More may be added as needed.
	enum Type
	{
		TF_NONE = 0,

		// four dword (4 x 32bit = 128bit) sized components
		TF_R32G32B32A32_TYPELESS,
		TF_R32G32B32A32_FLOAT,
		TF_R32G32B32A32_UINT,
		TF_R32G32B32A32_INT,

		// three dword (3 x 32bit = 96bit) sized components
		TF_R32G32B32_TYPELESS,
		TF_R32G32B32_FLOAT,
		TF_R32G32B32_UINT,
		TF_R32G32B32_INT,

		// two dword sized components
		TF_R32G32_TYPELESS,
		TF_R32G32_FLOAT,
		TF_R32G32_UINT,
		TF_R32G32_INT,

		// single dword sized components
		TF_R32_TYPELESS,
		TF_R32_FLOAT,

		// four word sized components
		TF_R16G16B16A16_TYPELESS,
		TF_R16G16B16A16_FLOAT,
		TF_R16G16B16A16_UINT,
		TF_R16G16B16A16_INT,

		// two word sized components
		TF_R16G16_TYPELESS,
		TF_R16G16_FLOAT,
		TF_R16G16_UINT,
		TF_R16G16_INT,

		// 16-bit integer formats.
		TF_L16,
		TF_R16F,

		// four byte sized components
		TF_R8G8B8A8_TYPELESS,
		TF_R8G8B8A8_UNORM,
		TF_R8G8B8A8_SNORM,
		TF_R8G8B8A8_UINT,
		TF_R8G8B8A8_INT,

		// two byte sized components
		TF_R8G8_TYPELESS,
		TF_R8G8_UINT,
		TF_R8G8_INT,

		// single byte sized components.
		TF_A8,
		TF_L8,

		// Block compressed formats
		TF_BC1_TYPELESS,
		TF_BC1_UNORM,
		TF_BC1_UNORM_SRGB,
		TF_BC2_TYPELESS,
		TF_BC2_UNORM,
		TF_BC2_UNORM_SRGB,
		TF_BC3_TYPELESS,
		TF_BC3_UNORM,
		TF_BC3_UNORM_SRGB,
		TF_BC4_TYPELESS,
		TF_BC4_UNORM,
		TF_BC4_SNORM,
		TF_BC5_TYPELESS,
		TF_BC5_UNORM,
		TF_BC5_SNORM,
		TF_BC6_TYPELESS,
		TF_BC6_UF16,
		TF_BC6_SF16,
		TF_BC7_TYPELESS,
		TF_BC7_UNORM,
		TF_BC7_UNORM_SRGB,

		//// DXT compressed formats.
		//TF_DXT1, // supports 3 colour channels and only a 1-bit (on/off) alpha channel
		//TF_DXT3, // supports 3 colour channels and only a 8-bit alpha channel
		//TF_DXT5, // supports 2 colour channels

		// Depth-stencil format.

		// A 32-bit floating-point component, and two unsigned-integer components (with an additional 32 bits).
		// This format supports 32-bit depth, 8-bit stencil, and 24 bits are unused.
		TF_D32_FLOAT_S8X24_UINT,

		// A single-component, 32-bit floating-point format that supports 32 bits for depth.
		TF_D32_FLOAT,

		// Depth 24 bit unsigned int and stencil 8 bit float.
		TF_D24_UNORM_S8_FLOAT,

		// The number of formats in this enum.
		TF_COUNT,
	};

	// Function to determine the bytes per pixel of a format and bits per pixel
	static size_t BytesPerPixel(CTextureFormat::Type format);
	static size_t BitsPerPixel(CTextureFormat::Type format);
	static bool IsCompressed(CTextureFormat::Type format);

private:
	CTextureFormat() {};
};

// MipMap structure that will represent
// mip'd versions of the texture.
struct SMipMap1D
{
	size_t width;
	size_t numBytes;
	size_t offset;
};

// MipMap structure that will represent
// mip'd versions of a texture.
struct SMipMap2D
{
	size_t width;
	size_t height;
	size_t numBytes;
	size_t offset;
};
// =================================================================================================================
// View types
// =================================================================================================================
class CViewType
{
public:
	// The type of view created
	enum Type
	{
		DT_RESOURCE = 0,
		DT_RENDERTARGET,
		DT_DEPTHSTENCIL
	};
private:
	CViewType() {};
};

// Buffer shader resouce info
class CBufferSRI
{
public:
	CBufferSRI(size_t elementOffset, size_t elementWidth)
		:
		elementOffset(elementOffset), elementWidth(elementWidth)
	{ /**/
	}

	size_t elementOffset;
	size_t elementWidth;
};

// Texture shader resouce info
class CTextureSRI
{
public:
	CTextureSRI(size_t highestMip, size_t maxlevels)
		:
		highestMip(highestMip), maxlevels(maxlevels)
	{ /**/
	}

	size_t highestMip;
	size_t maxlevels;
};

// Boost variant used for shader resource view info
// Programmer must determine the resource type
typedef boost::variant<
	CBufferSRI,
	CTextureSRI
> ShaderResourceInfo;

// Buffer shader resouce type info
class CBufferRTI
{
public:
	CBufferRTI(size_t elementOffset, size_t elementWidth)
		:
		elementOffset(elementOffset), elementWidth(elementWidth)
	{ /**/
	}

	size_t elementOffset;
	size_t elementWidth;
};

// Texture shader resouce type info
class CTextureRTI
{
public:
	CTextureRTI(size_t mipSlice)
		:
		mipSlice(mipSlice)
	{ /**/
	}

	size_t mipSlice;
};

// Boost variant used for render target info
// Programmer must determine the resource type
typedef boost::variant<
	CBufferRTI,
	CTextureRTI
> RenderTargetInfo;

class CTextureDSI
{
public:
	CTextureDSI(size_t mipSlice)
		:
		mipSlice(mipSlice)
	{ /**/
	}

	size_t mipSlice;
};

typedef boost::variant<
	CTextureDSI
> DepthStencilInfo;
// =================================================================================================================
// Render primitive types
// =================================================================================================================
class CPrimitiveType
{
public:
	// The primitive type that sets the rendering mode of
	// vertexs or indices
	enum Type
	{
		PT_NONE = 0,
		PT_POLYPOINTS,		// Renders the vertices as a collection of isolated points
		PT_POLYLINE_CLOSED,	// Renders the vertices as a list of isolated straight line segments
		PT_POLYLINE_OPEN,	// Renders the vertices as a single polyline.
		PT_TRIANGLE,
		PT_TRIMESH,
		PT_TRISTRIP,
		PT_TRIFAN,
		PT_COUNT
	};
private:
	CPrimitiveType() {};
};
// =================================================================================================================
// Shader types
// =================================================================================================================
class CShaderType
{
public:
	// Types of shader function supported
	enum Type
	{
		ST_NONE = 0,
		ST_VERTEX,
		ST_GEOMETRY,
		ST_PIXEL,
		ST_COUNT
	};
private:
	CShaderType() {};
};

class CShaderVersion
{
public:
	// Shader profile information.
	enum Type
	{
		SV_NONE = 0,
		SV_2_0,
		SV_3_0,
		SV_4_0,
		SV_4_1,
		SV_5_0,
		SV_ARBVP1
	};
private:
	CShaderVersion() {};
};

class CFXTypes
{
public:
	// Support for .fx in HLSL and in GLSL
	enum FXTypes
	{
		FT_NONE = 0,
		FT_HLSL,
		FT_GLSL,
		FT_COUNT
	};
private:
	CFXTypes() {};
};
// =================================================================================================================
// Rasterization types
// =================================================================================================================
class CSrcBlend // Alpha state
{
public:
	enum Type
	{
		SB_ZERO,
		SB_ONE,
		SB_DST_COLOUR,
		SB_ONE_MINUS_DST_COLOUR,
		SB_SRC_ALPHA,
		SB_ONE_MINUS_SRC_ALPHA,
		SB_DST_ALPHA,
		SB_ONE_MINUS_DST_ALPHA,
		SB_SRC_ALPHA_SATURATE,
		SB_CONSTANT_COLOUR,
		SB_ONE_MINUS_CONSTANT_COLOUR,
		SB_CONSTANT_ALPHA,
		SB_ONE_MINUS_CONSTANT_ALPHA,
		NUM_SRC_BLENDS
	};
private:
	CSrcBlend() {};
};

class CDstBlend // Alpha state
{
public:
	enum Type
	{
		DB_ZERO,
		DB_ONE,
		DB_DST_COLOUR,
		DB_ONE_MINUS_DST_COLOUR,
		DB_SRC_ALPHA,
		DB_ONE_MINUS_SRC_ALPHA,
		DB_DST_ALPHA,
		DB_ONE_MINUS_DST_ALPHA,
		DB_CONSTANT_COLOUR,
		DB_ONE_MINUS_CONSTANT_COLOUR,
		DB_CONSTANT_ALPHA,
		DB_ONE_MINUS_CONSTANT_ALPHA,
		NUM_DST_BLENDS
	};
private:
	CDstBlend() {};
};

class CCompareMode // Alpha/Depth state
{
public:
	enum Type
	{
		CM_NEVER,
		CM_LESS,
		CM_EQUAL,
		CM_LEQUAL,
		CM_GREATER,
		CM_NOTEQUAL,
		CM_GEQUAL,
		CM_ALWAYS,
		NUM_CMP_MODES
	};
private:
	CCompareMode() {};
};


#pragma warning( pop ) // Pop warning on [enum : size_t]

_ENGINE_END

#endif