#ifndef CTEXTURE1D_GL_H
#define CTEXTURE1D_GL_H

#include "OpenGLFwd.h"
#include "IResource.h"

_ENGINE_BEGIN

class CTexture1DGL : public ITexture1D {
public:
    // Construction and destruction.
    CTexture1DGL(size_t bindFlags, CResourceAccess::Type usage,
                  CTextureFormat::Type tformat, const SMipMap1D *mips, size_t numLevels,
                  const uint8_t *data, size_t arraySize);

    // Releases the D3DResource
    ~CTexture1DGL();

    // Create a resource view with specified values
    unique_ptr<IShaderResource> createSRView(CTextureFormat::Type tformat, size_t highestMip,
                                             size_t maxlevels) override;

    // Create a resource view using whole resource
    unique_ptr<IShaderResource> createSRView() override;

    // Creates a render target view using the whole resource
    unique_ptr<IRenderTarget> createRTView() override;

    // Create a depth stencil view buffer.
    unique_ptr<IDepthStencil> createDSView() override;

    // Open access to the data of a texture
    void *Map(IPipeline *pipeline, CResourceLock::Type lock, size_t &rowPitch,
              size_t &depthSlice) const override;

    // Close access to the data of a texture
    void UnMap(IPipeline *pipeline, size_t subresource) const override;

    // Get The mipmaps via a pointer
    // #Note: they must be copied into a container
    const SMipMap1D *info() const override;

    // Return the number of mip map levels
    size_t numMips() const override;

    // Width of the texture
    size_t width() const override;

    // The data total buffer's size in bytes
    size_t size() const override;

    // Buffer type determines if its a vertex or index buffer
    size_t bindFlags() const override;

    // Describes the buffers usage with relation to the hardware
    CResourceAccess::Type usage() const override;

    // Determine the type of resource thats been created
    CResourceType::Type type() const override;

    // The textures format
    CTextureFormat::Type textureFormat() const override;

    // The number of dimensions of the texture
    CTextureDimension::Type textureDimension() const override;

    // Check if the texture format is compressed
    bool isCompressed() const override;

    // MipMap structure that will represent
    // mip'd versions of the texture.
    struct SMipMap1DGL {
        GLsizei width;
        GLsizei numBytes;
    };
    typedef std::vector<SMipMap1DGL> MipMap1DArray;
    typedef std::vector<GLuint> GLsize_tArray;

private:
    // HANDLEs to texture details
    GLuint m_texture;
    GLuint m_usage;
    GLenum m_internalFormat;
    GLenum m_format;
    GLenum m_type;
    GLsize_tArray m_buffer;

    // The type of buffer in use
    GLenum m_target;

    // compressed flag
    GLboolean m_isCompressed;

    // Array of mipmaps used for the texture
    MipMap1DArray m_mipMaps;
};


_ENGINE_END


#endif