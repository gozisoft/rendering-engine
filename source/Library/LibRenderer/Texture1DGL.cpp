#include "librendererfwd.h"
#include "Texture1DGL.h"
#include "MappingGL.h"

using namespace engine;

CTexture1DGL::CTexture1DGL(size_t bindFlags, CResourceAccess::Type usage, CTextureFormat::Type tformat,
                           const SMipMap1D *mips, size_t numLevels, const uint8_t *data, size_t arraySize)
    :
    m_target(ApiToGL::BufferBindFlags(bindFlags)),
    m_usage(ApiToGL::BufferUsage(usage)),
    m_internalFormat(ApiToGL::TextureFormat(tformat))
{
    // Assign the number of mipmaps
    m_mipMaps.resize(numLevels);

    for (size_t level = 0; level < numLevels; ++level)
    {
        const SMipMap1D &mip = mips[i];

        m_mipMaps[level].width = static_cast<GLsizei>(mip.width);
        m_mipMaps[level].numBytes = static_cast<GLsizei>(mip.numBytes);

        glGenBuffers(1, &m_buffer[level]);
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_buffer[level]);
        glBufferData(GL_PIXEL_UNPACK_BUFFER, m_mipMaps[level].numBytes, 0, m_usage);
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    }

    switch (usage)
    {
        case CResourceAccess::RA_GPU: // The CPU updates the resource less than once per frame
            break;
        case CResourceAccess::RA_STATIC:  // The CPU does not update the resource IMMUTABLE
            if (arraySize > 0)
            {
                for (size_t level = 0; level < numLevels; ++level)
                {
                    const SMipMap1D &mip = mips[level];
                    glTexStorage2D(GL_TEXTURE_1D_ARRAY, (GLsizei) level,
                                   m_internalFormat, (GLsizei) mip.width, (GLsizei) arraySize);
                }
            }
            else
            {
                // Create the mipmap level structures.  No image initialization occurs.
                for (size_t level = 0; level < numLevels; ++level)
                {
                    glTexStorage1D(
                        (GLint) level,
                        m_internalFormat,
                        m_mipMaps[level].width,
                        0,
                        m_format,
                        m_type,
                        static_cast<const GLvoid *>(data + mips[i].offset));
                }
            }
            break;

        case CResourceAccess::RA_DYNAMIC: // The CPU updates the resource more than once per frame DYNAMIC
            // Create the mipmap level structures.  No image initialization occurs.
            for (size_t level = 0; level < numLevels; ++level)
            {
                glTexImage1D(GL_TEXTURE_1D,
                             (GLint) level,
                             m_internalFormat,
                             m_mipMaps[level].width,
                             0,
                             m_format,
                             m_type,
                             static_cast<const GLvoid *>(data + mips[i].offset));
            }
            break;
        case CResourceAccess::RA_STAGING: // The CPU needs to read the resource
            break;
        default:
            throw std::runtime_error("Unknown buffer usage [CAPI_To_D3D11::BufferUsage]");
    };

    // Create one OpenGL texture
    glGenTextures((GLsizei) arraySize, &m_texture);

    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_1D, m_texture);


    glBindTexture(GL_TEXTURE_1D, 0);
}

CTexture1DGL::~CTexture1DGL()
{
    for (size_t level = 0; level < m_mipMaps.size(); ++level)
    {
        glDeleteBuffers(1, &m_buffer[level]);
    }
    glDeleteTextures(1, &m_texture);
}

void CTexture1DGL::Enable(uint32_t sampler)
{
    ::glActiveTexture(GL_TEXTURE0 + sampler);
    ::glBindTexture(GL_TEXTURE_1D, m_texture);
}

void CTexture1DGL::Disable(uint32_t sampler)
{
    ::glActiveTexture(GL_TEXTURE0 + sampler);
    ::glBindTexture(GL_TEXTURE_1D, 0);
}

GLvoid *CTexture1DGL::Lock(Buffer::Locking mode)
{

    return 0;
}

void CTexture1DGL::Unlock()
{

}

unique_ptr<IDepthStencil> CTexture1DGL::createDSView()
{
    return unique_ptr<IDepthStencil>();
}

CTextureFormat::Type CTexture1DGL::textureFormat() const
{
    return GlToApi::BufferBindFlags();
}

CTextureDimension::Type CTexture1DGL::textureDimension() const
{
    return TD_CUBE;
}

bool CTexture1DGL::isCompressed() const
{
    return false;
}

void *CTexture1DGL::Map(IPipeline *pipeline, CResourceLock::Type lock, size_t &rowPitch, size_t &depthSlice) const
{
    return nullptr;
}

void CTexture1DGL::UnMap(IPipeline *pipeline, size_t subresource) const
{

}

unique_ptr<IShaderResource> CTexture1DGL::createSRView(CTextureFormat::Type tformat, size_t highestMip,
                                                       size_t maxlevels)
{
    return unique_ptr<IShaderResource>();
}

const SMipMap1D *CTexture1DGL::info() const
{
    return nullptr;
}

size_t CTexture1DGL::numMips() const
{
    return 0;
}

size_t CTexture1DGL::width() const
{
    return 0;
}

size_t CTexture1DGL::size() const
{
    return 0;
}
size_t CTexture1DGL::bindFlags() const
{
    return 0;
}
CResourceAccess::Type CTexture1DGL::usage() const
{
    return RA_STAGING;
}
CResourceType::Type CTexture1DGL::type() const
{
    return RT_TEXTURE2D;
}
