//
// Created by Riad Gozim on 22/12/2015.
//

#include "librendererafx.h"
#include "PipelineGL.h"

engine::PipelineGL::PipelineGL()
{

}

engine::PipelineGL::~PipelineGL()
{

}

void engine::PipelineGL::flush()
{

}
void engine::PipelineGL::ClearState()
{

}
void engine::PipelineGL::Clear(const IRenderTarget *renderTarget, const float *rgba)
{

}
void engine::PipelineGL::Clear(shared_ptr<IRenderTarget> renderTarget, const float *rgba)
{

}
void engine::PipelineGL::Clear(const engine::IDepthStencil *depthStencil,
                               uint32_t stateFlags,
                               float depth,
                               uint8_t stencil)
{

}
void engine::PipelineGL::enableInputLayout(const engine::IInputFormat *inputformat)
{

}
void engine::PipelineGL::enableInputLayout(std::shared_ptr<IInputFormat> const &inputformat)
{

}
void engine::PipelineGL::enableVertexBuffers(const engine::IBuffer **vbuffers,
                                             size_t numbuffers,
                                             size_t startslot,
                                             size_t *strides,
                                             size_t *offsets)
{

}
void engine::PipelineGL::enableVertexBuffers(const std::vector<engine::IPipeline::BufferDesc> &descriptors,
                                             size_t startslot)
{

}
void engine::PipelineGL::enableIndexBuffer(const engine::IBuffer *ibuffer, size_t stide, size_t offset)
{

}
void engine::PipelineGL::enableIndexBuffer(std::shared_ptr<IBuffer> const &ibuffer, size_t stide, size_t offset)
{

}
void engine::PipelineGL::enableConstantBuffer(const engine::IBuffer **cbuffers, size_t numbuffers, size_t startslot)
{

}
void engine::PipelineGL::enableConstantBuffer(std::vector<shared_ptr<IBuffer>> const &cbuffers, size_t startslot)
{

}
void engine::PipelineGL::enableShader(const engine::IShader *vshader)
{

}
void engine::PipelineGL::enableShader(shared_ptr<IShader> const &vshader)
{

}
void engine::PipelineGL::setInputDrawingMode()
{

}
void engine::PipelineGL::setRenderTargets(const engine::IRenderTarget **renderTargets,
                                          size_t num,
                                          const engine::IDepthStencil *depthStencil)
{

}
void engine::PipelineGL::setRenderTargets(std::vector<shared_ptr<IRenderTarget>> const &renderTargets,
                                          shared_ptr<IDepthStencil> const &depthStencil)
{

}
void engine::PipelineGL::SetViewports(const engine::ViewPort *viewports, size_t numViewPorts)
{
    for (size_t i = 0; i < numViewPorts; ++i) {
        const ViewPort &viewPort = viewports[i];
        glViewport((GLint) viewPort.posX(), (GLint) viewPort.posY(), (GLsizei) viewPort.width(),
                   (GLsizei) viewPort.height());
    }
}
void engine::PipelineGL::drawIndexd(size_t indexCount, size_t startIndex)
{

}
void engine::PipelineGL::draw(size_t count, size_t startVertex)
{

}

