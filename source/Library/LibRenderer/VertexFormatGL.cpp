#include "Frameafx.h"
#include "VertexFormatGL.h"
#include "MappingGL.h"
#include "VertexFormat.h"
#include "VertexElement.h"

using namespace engine;

CVertexFormatGL::CVertexFormatGL()
{

}

CVertexFormatGL::~CVertexFormatGL()
{

}

void CVertexFormatGL::Enable(const CVertexFormat* pVFromat)
{
	for (size_t i = 0; i < pVFromat->GetElementCount(); ++i)
	{
		const CVertexElement* pElement = pVFromat->GetElement(i);
		::glVertexAttribPointer( i,
			pElement->GetVertexChannels(),
			GLVertexType( pElement->GetVertexType() ),
			GL_FALSE,
			m_pVFormat->GetTotalStride(),
			BufferOffset( pElement->GetStride() ) );	
	}
}

void CVertexFormatGL::Disable(const CVertexFormat* pVFromat)
{
	for (size_t i = 0; i < pVFromat->GetElementCount(); ++i)
		::glDisableVertexAttribArray(i);
}

uint8_t* CVertexFormatGL::BufferOffset(Long offset)
{ 
	return ( (uint8_t*)0 + offset );
}