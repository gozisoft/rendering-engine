#ifndef __TEXTURE_TOOLS_H__
#define __TEXTURE_TOOLS_H__

#include "MathsFwd.h"
#include "IResource.h"

_ENGINE_BEGIN

// How to normalise a range of numbers
// http://stackoverflow.com/questions/1226587/how-to-normalize-a-list-of-int-values
// http://people.revoledu.com/kardi/tutorial/Similarity/Normalization.html

// To normalise a value to the range [-1, 1] you need to use the equation
// percentageVal = (i - valuemin) / (valuemax - valuemin)
// scaledval = ( percentageVal * (scalemax - scalemin) ) + scalemin

// To normalise
// double valueRange = valueMax - valueMin; 
// double scaleRange = scaleMax - scaleMin; 
// val = (scaleRange * (i - valueMin)) / valueRange) + scaleMin); 

// (val - scaleMin) / scaleRange
// ( ((val - scaleMin) / scaleRange ) * valueRange ) + valuemin

namespace Texture
{
	// Convert from the specified format to 32-bit RGBA color (floating point).
	void ConvertFromR32G32B32A32_FLOAT(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR32G32B32A32(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR32G32B32_FLOAT(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR32G32B32(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR32G32_FLOAT(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR32G32(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR8G8B8A8_UNORM(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR8G8B8A8_SNORM(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR8G8B8A8(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR8G8B8_UNORM(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR8G8B8_SNORM(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR8G8B8(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertFromR8G8(size_t numTexels, const uint8_t* inTexels, Colour* outTexels);

	// Convert to the specified format from 32-bit RGBA color.
	void ConvertToR32G32B32A32_FLOAT(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR32G32B32A32(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR32G32B32_FLOAT(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR32G32B32(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR32G32_FLOAT(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR32G32(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR8G8B8A8_UNORM(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR8G8B8A8_SNORM(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR8G8B8A8(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR8G8B8_UNORM(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR8G8B8_SNORM(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);
	void ConvertToR8G8B8(size_t numTexels, const Colour* inTexels, uint8_t* outTexels);

	// Conversion functions
	void ConvertFrom(CTextureFormat::Type tFormat, size_t numTexels, const uint8_t* inTexels, Colour* outTexels);
	void ConvertTo(CTextureFormat::Type tFormat, size_t numTexels, const Colour* inTexels, uint8_t* outTexels);

#include "TextureTools.inl"

}

_ENGINE_END

#endif