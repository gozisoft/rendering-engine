#ifndef CTEXTURE2D_GL_H
#define CTEXTURE2D_GL_H

#include "OpenGLFwd.h"
#include "framefwd.h"


_ENGINE_BEGIN


class CTexture2DGL
{
public:
	CTexture2DGL(CTexture2DPtr pTexture2D);
	~CTexture2DGL();

	GLsize_t GetTexture();

	// Index buffer operations.
	void Enable(uint32_t sampler);
	void Disable(uint32_t sampler);
	GLvoid* Lock(Buffer::Locking mode);
	void Unlock();

	// MipMap structure that will represent
	// mip'd versions of the texture.
	struct SMipMap2DGL
	{
		GLsizei width;
		GLsizei height;
		GLsizei numBytes;
	};
	typedef std::vector<SMipMap2DGL> MipMap2DArray;
	typedef std::vector<GLsize_t> GLsize_tArray;

private:
	// HANDLEs to texture details
	GLsize_t m_texture;
	GLenum m_internalFormat;
	GLenum m_format;
	GLenum m_type;
	GLsize_t m_usage;
	GLsize_tArray m_buffer;

	// compressed flag
	GLboolean m_isCompressed;

	// Array of mipmaps used for the texture
	MipMap2DArray m_mipMaps;

};


_ENGINE_END


#endif