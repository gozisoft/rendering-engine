#ifndef __OPENGL_FORWARD_H__
#define __OPENGL_FORWARD_H__

#pragma comment( lib, "OpenGL32.lib" )
#pragma comment( lib, "glu32.lib" )

#include "Core.h"

#if defined (WIN32) || defined (_WIN32) || defined (WIN64) || defined (_WIN64)
#define GLEW_NO_GLU
#define GLEW_STATIC
#include "GL/wglew.h" // windows glew header
#else
#include <OpenGL/gl3.h>
#include <OpenGL/gl3ext.h>
#endif

#include <memory>

_ENGINE_BEGIN

// OpenGL
class CRendererGL;
class CRenderDataGL;
class CRenderResourcesGL;
class CDeferredResourcesGL;
class CBufferGL;
class CVertexFormatGL;
class CTexture1DGL;
class CTexture2DGL;

class ShaderResourceGL;
class RenderTargetGL;

_ENGINE_END

#endif