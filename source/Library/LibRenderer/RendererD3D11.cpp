#include "librendererafx.h"
#include "RendererD3D11.h"
#include "PipelineD3D11.h"
#include "MappingD3D11.h"
#include "ExceptionD3D.h"
#include "AlignedAllocator.h"

#include <memory>

using namespace engine;

//-------------------------------------------------------------------------------------------------------------------
RendererD3D11::RendererD3D11() {
    D3D_FEATURE_LEVEL featureLevels[] =
            {
                    D3D_FEATURE_LEVEL_11_1,
                    D3D_FEATURE_LEVEL_11_0,
                    D3D_FEATURE_LEVEL_10_1,
                    D3D_FEATURE_LEVEL_10_0
            };

    D3D_DRIVER_TYPE driverTypes[] =
            {
                    D3D_DRIVER_TYPE_HARDWARE,
                    D3D_DRIVER_TYPE_WARP,
                    D3D_DRIVER_TYPE_SOFTWARE
            };

    size_t createDeviceFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    // HRESULT error checking
    HRESULT hr = S_OK;

    // Resultant device context
    ID3D11DeviceContextPtr pImmediate = nullptr;

    // The chosen feature level
    D3D_FEATURE_LEVEL chosenlevel;

    // Attempt to create the device manually [hardware device first]
    for (size_t j = 0; j < ARRAYSIZE(driverTypes); ++j) {
        if (driverTypes[j] == D3D_DRIVER_TYPE_HARDWARE) {
            hr = D3D11CreateDevice(nullptr,
                                   driverTypes[j],
                                   nullptr,
                                   createDeviceFlags,
                                   &featureLevels[0],
                                   ARRAYSIZE(featureLevels), // same as sizeof(array) / sizeof(array[0])
                                   D3D11_SDK_VERSION,
                                   &m_device,
                                   &chosenlevel,
                                   &pImmediate);

            if (SUCCEEDED(hr))
                break;
        }
    }

    // Check for failure
    ThrowIfFailed(hr);

#if defined(DEBUG) || defined(_DEBUG)
    // Enable debugging
    enableDebug();
#endif

    // Create the main pipeline device
    m_pipeline = make_shared<PipelineD3D11>(pImmediate);
}

//-------------------------------------------------------------------------------------------------------------------
RendererD3D11::~RendererD3D11() {
    if (m_debug)
        m_debug->ReportLiveDeviceObjects(D3D11_RLDO_SUMMARY | D3D11_RLDO_DETAIL);
}

shared_ptr<IPipeline> RendererD3D11::getPipeline() const {
    return m_pipeline;
}

unique_ptr<RasterizerStateD3D11> RendererD3D11::createRasterState(std::shared_ptr<RasterizerState> const &state) {
    // Return the raster state object.
    return std::make_unique<RasterizerStateD3D11>(_createRasterState(state));
}

ID3D11RasterizerStatePtr RendererD3D11::_createRasterState(std::shared_ptr<RasterizerState> const &state) {
    // https://anteru.net/2011/12/27/1830/
    // Specify the rasterizer state description.
    D3D11_RASTERIZER_DESC desc;
    desc.FillMode = CAPI_To_D3D11::FillMode(state->fillMode);
    desc.CullMode = CAPI_To_D3D11::CullMode(state->cullMode);
    desc.FrontCounterClockwise = (state->frontCCW ? TRUE : FALSE);
    desc.DepthBias = state->depthBias;
    desc.DepthBiasClamp = state->depthBiasClamp;
    desc.SlopeScaledDepthBias = state->slopeScaledDepthBias;
    desc.DepthClipEnable = (state->enableDepthClip ? TRUE : FALSE);
    desc.ScissorEnable = (state->enableScissor ? TRUE : FALSE);
    desc.MultisampleEnable =
            (state->enableMultisample ? TRUE : FALSE);
    desc.AntialiasedLineEnable =
            (state->enableAntialiasedLine ? TRUE : FALSE);

    // Create the rasterizer state.
    ID3D11RasterizerStatePtr stated3d = nullptr;
    ThrowIfFailed(m_device->CreateRasterizerState(&desc, &stated3d));

    // Return the raster state object.
    return stated3d;
}

//-------------------------------------------------------------------------------------------------------------------
IDXGISwapChainPtr RendererD3D11::createSwapChain(size_t numBackBuffers,
                                                 void *hwnd,
                                                 CTextureFormat::Type format,
                                                 CBufferSwap::Type swapType) {
    // Determine the refresh rate of the device
    IDXGIDevice1 *pDXGIDevice = nullptr;
    ThrowIfFailed(m_device->QueryInterface(IID_IDXGIDevice1, (void **) &pDXGIDevice));

    // Get the DXGIDevices adapter
    IDXGIAdapter1 *pDXGIAdapter = nullptr;
    ThrowIfFailed(pDXGIDevice->GetParent(__uuidof(IDXGIAdapter1), (void **) &pDXGIAdapter));

    // Get the first output
    IDXGIOutput *pOutput = nullptr;
    ThrowIfFailed(pDXGIAdapter->EnumOutputs(0, &pOutput));

    // Get the number of display modes that fit the back buffer format
    DXGI_FORMAT backFormat = CAPI_To_D3D11::DXGIFormat(format);
    size_t numModes = 0;
    ThrowIfFailed(pOutput->GetDisplayModeList(backFormat, 0, &numModes, nullptr));

    // Create a list to hold all the possible display modes for this monitor/video card combination.
    std::vector<DXGI_MODE_DESC> displayModeArray(numModes);

    // Now fill the display mode list structures.
    ThrowIfFailed(pOutput->GetDisplayModeList(backFormat, 0, &numModes, &displayModeArray[0]));

    // Determine window size
    RECT rc;
    GetClientRect(static_cast<HWND>(hwnd), &rc);
    size_t width = rc.right - rc.left;
    size_t height = rc.bottom - rc.top;

    // Now go through all the display modes and find the one that matches the screen width and height.
    // When a match is found store the numerator and denominator of the refresh rate for that monitor.
    size_t numerator = 60;
    size_t denominator = 1;
    for (size_t i = 0; i < numModes; ++i) {
        if (displayModeArray[i].Width == width && displayModeArray[i].Height == height) {
            numerator = displayModeArray[i].RefreshRate.Numerator;
            denominator = displayModeArray[i].RefreshRate.Denominator;
        }
    }

    // Create the swap chain for the device
    DXGI_SWAP_CHAIN_DESC sd;
    ::SecureZeroMemory(&sd, sizeof(DXGI_SWAP_CHAIN_DESC));
    sd.BufferDesc.Width = width;
    sd.BufferDesc.Height = height;
    sd.BufferDesc.Format = backFormat;
    sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    sd.BufferDesc.RefreshRate.Numerator = numerator;
    sd.BufferDesc.RefreshRate.Denominator = denominator;

    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;

    sd.BufferCount = static_cast<UINT>(numBackBuffers);
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.SwapEffect = CAPI_To_D3D11::SwapEffect(swapType);
    sd.OutputWindow = static_cast<HWND>(hwnd);
    sd.Windowed = TRUE;
    sd.Flags = 0;

    // Get the DXGI factory to create the swap chain
    IDXGIFactory1Ptr pIDXGIFactory = nullptr;
    ThrowIfFailed(pDXGIAdapter->GetParent(IID_IDXGIFactory1, (void **) &pIDXGIFactory));

    // Create the swap chain
    IDXGISwapChainPtr pSwapChain = nullptr;
    ThrowIfFailed(pIDXGIFactory->CreateSwapChain(m_device, &sd, &pSwapChain));

    // Return the swap chain
    return pSwapChain;
}

//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CTexture2DD3D11> RendererD3D11::getBackBuffer(IDXGISwapChainPtr swapChain, size_t bufferIndex) {
    // Get a pointer to the index swap chain
    IDXGISwapChainPtr pSwapChain = swapChain;

    // Get back buffer and create a render-target-view.
    ID3D11Texture2DPtr pBuffer = nullptr;
    ThrowIfFailed(pSwapChain->GetBuffer((UINT) bufferIndex, __uuidof(ID3D11Texture2D),
                                        (void **) &pBuffer));

    // Get the swap chain description and insure check the swap chains
    // presentation mode supports the access of more than one back buffer.
    // BS_DISCARD: One buffer only, the zero index (bit blit).
    // BS_SEQUENTIAL: Sequential access and traversal only (bit blit).
    // BS_FLIP: Same as BS_SEQUENTIAL (flip mode).
    DXGI_SWAP_CHAIN_DESC sd;
    pSwapChain->GetDesc(&sd);
    if ((bufferIndex > 0) && (sd.SwapEffect == DXGI_SWAP_EFFECT_DISCARD)) {
        throw std::exception("Swap chain created with BS_DISCARD, does not support access \
			of more than one back buffer [RendererD3D11::GetBackBuffer]");
    }

    // Return the abstract render target view
    return std::make_unique<CTexture2DD3D11>(pBuffer);
}

//-------------------------------------------------------------------------------------------------------------------
ID3D11DeviceChildPtr RendererD3D11::create_shader(SD3DShaderVTable *table, shader_handle handle) {
    // D3DBlob for creation
    ID3DBlob *pBlob = reinterpret_cast<ID3DBlob *>(handle.get());

    // Get a raw pointer to the D3DDevice
    ID3D11Device *device = m_device;

    // Create the shader using the device [VertexShader, GeometryShader, PixelShader]
    ID3D11DeviceChildPtr pObjectD3D = nullptr;
    ThrowIfFailed(
            (device->*(table->pCreateShader))(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &pObjectD3D));

    // Return the pointer
    return pObjectD3D;
}

//-------------------------------------------------------------------------------------------------------------------
ID3D11InputLayoutPtr RendererD3D11::create_input_format(const CInputElement *elements, size_t numElements,
                                                        shader_handle handle) {
    // Array of elements that make up the input layout
    std::vector<D3D11_INPUT_ELEMENT_DESC> elementsD3D(numElements);
    for (size_t i = 0; i < numElements; ++i) {
        // Set the DX11 attribute.
        D3D11_INPUT_ELEMENT_DESC newElement;
        newElement.SemanticName = elements[i].GetVertexSemantic().c_str();
        newElement.SemanticIndex = static_cast<size_t>(elements[i].GetSemanticIndex());
        newElement.Format = CAPI_To_D3D11::DXGIFormat(elements[i].GetVertexType(), elements[i].GetVertexChannels());
        newElement.InputSlot = static_cast<size_t>(elements[i].GetStreamIndex());
        newElement.AlignedByteOffset = static_cast<size_t>(elements[i].GetOffset());
        newElement.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA; // This could be changed for instancing
        newElement.InstanceDataStepRate = 0; // This could be changed for instancing

        // Push the D3D11 element back on the list
        elementsD3D[i] = newElement;
    }

    // Get the shader blob
    ID3DBlob *pShaderBlob = reinterpret_cast<ID3DBlob *>(handle.get());

    // Create the input layout
    ID3D11InputLayoutPtr pInputLayout = nullptr;
    ThrowIfFailed(m_device->CreateInputLayout(&elementsD3D[0], elementsD3D.size(), pShaderBlob->GetBufferPointer(),
                                              pShaderBlob->GetBufferSize(), &pInputLayout));

    // Create the D3D11 inputlayout
    return pInputLayout; // std::make_shared<InputFormatD3D11>(elements, numElements, pInputLayout);
}

//-------------------------------------------------------------------------------------------------------------------
ID3D11BufferPtr RendererD3D11::create_buffer(size_t bindFlags, CResourceAccess::Type usage, size_t sizeInBytes,
                                             const uint8_t *data) {
    // Fill in a buffer description.
    D3D11_BUFFER_DESC buff_desc;
    SecureZeroMemory(&buff_desc, sizeof(D3D11_BUFFER_DESC));
    buff_desc.ByteWidth = sizeInBytes;
    buff_desc.Usage = CAPI_To_D3D11::BufferUsage(usage);
    buff_desc.BindFlags = CAPI_To_D3D11::BufferBindFlags(bindFlags);

    // If the bind flag is D3D11_BIND_CONSTANT_BUFFER, you must set the ByteWidth value
    // in multiples of 16, and less than or equal to D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT.
    if (buff_desc.BindFlags & D3D11_BIND_CONSTANT_BUFFER) {
        if (sizeInBytes == 0) {
            throw std::runtime_error("Resources with constant buffer binding must have a sizeInBytes specified.");
        }

        // Same 0x0F = 15.
        if ((sizeInBytes & 0x0F) != 0) {
            throw std::runtime_error("Resources with constant buffer binding must have \
									 a sizeInBytes aligned to 16 bytes.");
        }

        if (sizeInBytes >= D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT) {
            throw std::overflow_error("Resources with constant buffer binding must have a sizeInBytes \
									  less than or equal to D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT.");
        }

        if (data != nullptr && !IsAligned(data, 16)) {
            throw std::runtime_error("Resources with constant buffer binding must have data aligned to 16 bytes.");
        }
    }

    // Dynamic buffers can write to a resource.
    // Staging buffers can read from or write to a resource.
    switch (buff_desc.Usage) {
        case D3D11_USAGE_DYNAMIC:
            buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
            break;
        case D3D11_USAGE_STAGING:
            buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
            break;
        case D3D11_USAGE_IMMUTABLE: // static resource can only be written to upon creation
            if (data == nullptr) {
                throw std::runtime_error(
                        "Resource is static and has no intialisation data. [RendererD3D11::CreateBuffer]");
            }
        default:
            buff_desc.CPUAccessFlags = 0;
            break;
    };

    buff_desc.MiscFlags = 0; // D3D11_RESOURCE_MISC_FLAG (for textures and such)
    buff_desc.StructureByteStride = 0;

    std::unique_ptr<D3D11_SUBRESOURCE_DATA> InitData;
    if (data != nullptr) {
        // Fill in the subresource data if the data is not null. If you don't do this you
        // get an error about pSysMem being null. Stupid, I know...
        InitData = std::make_unique<D3D11_SUBRESOURCE_DATA>();
        InitData->pSysMem = reinterpret_cast<const void *>(data); // Pointer to the initialization data.
        InitData->SysMemPitch = 0;
        InitData->SysMemSlicePitch = 0;
    }

    // Create the buffer from the D3D11 device
    ID3D11BufferPtr d3dBuffer = nullptr;
    ThrowIfFailed(m_device->CreateBuffer(&buff_desc, InitData.get(), &d3dBuffer));

    // Return the resource
    return d3dBuffer;
}

//-------------------------------------------------------------------------------------------------------------------
ID3D11Texture1DPtr RendererD3D11::create_texture(size_t bindFlags, CResourceAccess::Type usage,
                                                 CTextureFormat::Type tformat, const SMipMap1D *mips, size_t numLevels,
                                                 const uint8_t *data, size_t arraySize) {
    // TextureDesc
    D3D11_TEXTURE1D_DESC texture_desc;
    texture_desc.Width = static_cast<UINT>(mips[0].width);
    texture_desc.MipLevels = static_cast<UINT>(numLevels);
    texture_desc.Format = CAPI_To_D3D11::DXGIFormat(tformat);
    texture_desc.ArraySize = static_cast<UINT>(arraySize);
    texture_desc.Usage = CAPI_To_D3D11::BufferUsage(usage);
    texture_desc.BindFlags = CAPI_To_D3D11::BufferBindFlags(bindFlags);
    texture_desc.MiscFlags = 0;

    // Dynamic buffers can write to a resource.
    // Staging buffers can read from or write to a resource.
    switch (texture_desc.Usage) {
        case D3D11_USAGE_DYNAMIC:
            texture_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
            break;
        case D3D11_USAGE_STAGING:
            texture_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
            break;
        case D3D11_USAGE_IMMUTABLE: // static resource can only be written to upon creation
            if (!data) {
                throw std::runtime_error(
                        "Resource is static and has no intialisation data. [RendererD3D11::CreateBuffer]");
            }
        default:
            texture_desc.CPUAccessFlags = 0;
            break;
    };

    ID3D11Texture1DPtr d3dTextures = nullptr;

    // Applications cannot specify nullptr for pInitData when creating IMMUTABLE resources
    if (texture_desc.Usage == D3D11_USAGE_IMMUTABLE || data != nullptr) {
        // Create the sub resources
        std::vector<D3D11_SUBRESOURCE_DATA> initData(texture_desc.MipLevels * texture_desc.ArraySize);

        // Loop over all the textures
        for (size_t i = 0; i < texture_desc.ArraySize; ++i) {
            // Loop over all the mips and copy their data to the sub resource
            for (size_t j = 0; j < texture_desc.MipLevels; ++j) {
                // Fill in the sub resource data. If not multi sampled
                initData[i].pSysMem = static_cast<const void *>(data + mips[i].offset);

                // You set SysMemPitch to the length of the 1D surface in bytes.
                initData[i].SysMemPitch = static_cast<UINT>(mips[i].width * CTextureFormat::BytesPerPixel(tformat));

                // You don't need to set SysMemSlicePitch
                initData[i].SysMemSlicePitch = 0;
            }
        }

        // Create the textures from the D3D11 device
        ThrowIfFailed(m_device->CreateTexture1D(&texture_desc, &initData[0], &d3dTextures));
    }
    else {
        ThrowIfFailed(m_device->CreateTexture1D(&texture_desc, nullptr, &d3dTextures));
    }

    // Return the successful texture(s)
    return d3dTextures;
}

//-------------------------------------------------------------------------------------------------------------------
ID3D11Texture2DPtr RendererD3D11::create_texture(
        size_t bindFlags,
        CResourceAccess::Type usage,
        CTextureFormat::Type tformat,
        const SMipMap2D *mips,
        size_t numLevels,
        const uint8_t *data,
        size_t num_textures
) {
    DXGI_SAMPLE_DESC sample_desc;
    sample_desc.Count = 1;
    sample_desc.Quality = 0;

    // TextureDesc
    D3D11_TEXTURE2D_DESC texture_desc;
    texture_desc.Width = static_cast<UINT>(mips[0].width);
    texture_desc.Height = static_cast<UINT>(mips[0].height);
    texture_desc.MipLevels = static_cast<UINT>(numLevels);
    texture_desc.Format = CAPI_To_D3D11::DXGIFormat(tformat);
    texture_desc.ArraySize = static_cast<UINT>(num_textures);
    texture_desc.SampleDesc = sample_desc;
    texture_desc.Usage = CAPI_To_D3D11::BufferUsage(usage);
    texture_desc.BindFlags = CAPI_To_D3D11::BufferBindFlags(bindFlags);
    texture_desc.CPUAccessFlags = 0;
    texture_desc.MiscFlags = 0;

    // Dynamic buffers can write to a resource.
    // Staging buffers can read from or write to a resource.
    switch (texture_desc.Usage) {
        case D3D11_USAGE_DYNAMIC:
            texture_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
            break;
        case D3D11_USAGE_STAGING:
            texture_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
            break;
        case D3D11_USAGE_IMMUTABLE: // static resource can only be written to upon creation
            // A resource that can only be read by the GPU.It cannot be written by the GPU,
            // and cannot be accessed at all by the CPU.This type of resource must be
            // initialized when it is created, since it cannot be changed after creation.
            if (!data) {
                throw std::runtime_error(R"(Resource is static and has no
				intialisation data. [RendererD3D11::CreateBuffer])");
            }
        default:
            texture_desc.CPUAccessFlags = 0;
            break;
    };

    // Create the texture from the D3D11 device
    ID3D11Texture2DPtr d3dTexture;

    // ToDo: Add loop for texture array to fill D3D11_SUBRESOURCE_DATA.

    // Note: Applications cannot specify nullptr for pInitData when creating IMMUTABLE resources
    if (texture_desc.Usage == D3D11_USAGE_IMMUTABLE || data != nullptr) {
        // Create the sub resources
        std::vector<D3D11_SUBRESOURCE_DATA> initData(numLevels);

        // Loop over all the mips and copy their data to the sub resource
        for (size_t i = 0; i < texture_desc.MipLevels; ++i) {
            // Fill in the sub resource data. If not multi sampled
            initData[i].pSysMem = static_cast<const void *>(data +
                                                            mips[i].offset); // Pointer to the initialization data.

            // The distance (in bytes) from the beginning of one line of a texture to the next line.
            initData[i].SysMemPitch = static_cast<UINT>(CTextureFormat::BytesPerPixel(tformat) * mips[i].width);

            // You set SysMemSlicePitch to the size of the entire 2D surface in bytes
            initData[i].SysMemSlicePitch = static_cast<UINT>(mips[i].numBytes);
        }
        ThrowIfFailed(m_device->CreateTexture2D(&texture_desc, &initData[0], &d3dTexture));
    }
    else {
        ThrowIfFailed(m_device->CreateTexture2D(&texture_desc, nullptr, &d3dTexture));
    }

    // Return the successful texture(s)
    return d3dTexture;
}

//-------------------------------------------------------------------------------------------------------------------
void RendererD3D11::enableDebug() {
    // Enable debugging
    if (SUCCEEDED(m_device->QueryInterface(__uuidof(ID3D11Debug), (void **) &m_debug))) {
        ID3D11InfoQueuePtr d3dInfoQueue;
        if (SUCCEEDED(m_debug->QueryInterface(__uuidof(ID3D11InfoQueue), (void **) &d3dInfoQueue))) {
#ifdef _DEBUG
            d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_CORRUPTION, true);
            d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_ERROR, true);
#endif

            D3D11_MESSAGE_ID hide[] =
                    {
                            D3D11_MESSAGE_ID_SETPRIVATEDATA_CHANGINGPARAMS,
                            // Add more message IDs here as needed
                    };

            D3D11_INFO_QUEUE_FILTER filter;
            SecureZeroMemory(&filter, sizeof(filter));
            filter.DenyList.NumIDs = _countof(hide);
            filter.DenyList.pIDList = hide;
            d3dInfoQueue->AddStorageFilterEntries(&filter);
        }
    }
}




//// Switch case statement to mape vertex type enums to HLSL attribute arguments.
//// Not all types are supported in HLSL. 
//const char* D3D11VertexType(VertexType type)
//{
//	switch (type)
//	{
//	case DT_NONE: return nullptr;
//	case DT_INT1: return "int1";
//	case DT_INT2: return "int2";
//	case DT_INT3: return "int3";
//	case DT_INT4: return "int4";
//	case DT_size_t1: return "size_t1";
//	case DT_size_t2: return "size_t1";
//	case DT_size_t3: return "size_t1";
//	case DT_size_t4: return "size_t1";
//	case DT_BOOL1: return "bool1";
//	case DT_BOOL2: return "bool2";
//	case DT_BOOL3: return "bool3";
//	case DT_BOOL4: return "bool4";
//	case DT_FLOAT1: return "float1";
//	case DT_FLOAT2: return "float2";
//	case DT_FLOAT3: return "float3";
//	case DT_FLOAT4: return "float4";
//	default:
//		assert( false && ieS("VertexElement does not match D3D11 input type") );
//		return nullptr;
//	};
//}


//// Vertex buffer creation
//void RendererD3D11::Create(size_t numVertices, size_t stride, CResourceAccess::Type usage, IVertexBuffer** buffers)
//{
//	// #To do:
//	// If the bind flag is D3D11_BIND_CONSTANT_BUFFER then the ByteWidth value must be
//	// in multiples of 16, and less than or equal to D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT.
//
//	// Fill in a buffer description.
//	D3D11_BUFFER_DESC buff_desc;
//	buff_desc.ByteWidth = static_cast<size_t>(numVertices*stride);
//	buff_desc.Usage = D3D11BufferUsage(usage);
//	buff_desc.BindFlags = D3D11BindFlag(bufferInfo.bufferType);
//
//	// Dynamic buffers can write to a resource.
//	// Staging buffers can read from or write to a resource.
//	switch (buff_desc.Usage)
//	{
//	case D3D11_USAGE_DYNAMIC:
//		buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//		break;
//	case D3D11_USAGE_STAGING:
//		buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
//		break;
//	default:
//		buff_desc.CPUAccessFlags = 0;
//		break;
//	};
//
//	buff_desc.MiscFlags = 0; // D3D11_RESOURCE_MISC_FLAG (for textures and such)
//	buff_desc.StructureByteStride = 0;
//	
//	// Fill in the subresource data.
//	D3D11_SUBRESOURCE_DATA InitData;
//	InitData.pSysMem = reinterpret_cast<const void*>(bufferInfo.data); // Pointer to the initialization data.
//
//	// The distance (in bytes) from the beginning of one line of a texture to the next line.
//	// System-memory pitch is used only for 2D and 3D texture data as it is has no meaning
//	// for the other resource types.
//	InitData.SysMemPitch = 0;
//
//	// The distance (in bytes) from the beginning of one depth level to the next.
//	// System-memory-slice pitch is only used for 3D texture data as it has no
//	// meaning for the other resource types.
//	InitData.SysMemSlicePitch = 0;
//
//	// Create the buffer from the D3D11 device
//	ID3D11BufferPtr d3dBuffer = nullptr;
//	HRESULT hr = m_pDevice->CreateBuffer(&buff_desc, &InitData, &d3dBuffer);
//	if ( FAILED(hr) )
//		throw CD3D11Exception(hr);
//
//	// Create the API buffer and assign it to output variable
//	buffers[0] = new CVertexBufferD3D11(d3dBuffer);
//}
//
//// Index buffer creation
//void RendererD3D11::Create(size_t reserve, size_t stride, CResourceAccess::Type usage, IIndexBuffer** buffers)
//{
//	// Fill in a buffer description.
//	D3D11_BUFFER_DESC buff_desc;
//	buff_desc.ByteWidth = static_cast<size_t>(bufferInfo.sizeInBytes);
//	buff_desc.Usage = D3D11BufferUsage(bufferInfo.usage);
//	buff_desc.BindFlags = D3D11BindFlag(bufferInfo.bufferType);
//
//	// Dynamic buffers can write to a resource.
//	// Staging buffers can read from or write to a resource.
//	switch (buff_desc.Usage)
//	{
//	case D3D11_USAGE_DYNAMIC:
//		buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//		break;
//	case D3D11_USAGE_STAGING:
//		buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
//		break;
//	default:
//		buff_desc.CPUAccessFlags = 0;
//		break;
//	};
//
//	buff_desc.MiscFlags = 0; // D3D11_RESOURCE_MISC_FLAG (for textures and such)
//	buff_desc.StructureByteStride = 0;
//	
//	// Fill in the subresource data.
//	D3D11_SUBRESOURCE_DATA InitData;
//	InitData.pSysMem = reinterpret_cast<const void*>(bufferInfo.data); // Pointer to the initialization data.
//
//	// The distance (in bytes) from the beginning of one line of a texture to the next line.
//	// System-memory pitch is used only for 2D and 3D texture data as it is has no meaning
//	// for the other resource types.
//	InitData.SysMemPitch = 0;
//
//	// The distance (in bytes) from the beginning of one depth level to the next.
//	// System-memory-slice pitch is only used for 3D texture data as it has no
//	// meaning for the other resource types.
//	InitData.SysMemSlicePitch = 0;
//
//	// Create the buffer from the D3D11 device
//	ID3D11BufferPtr d3dBuffer = nullptr;
//	HRESULT hr = m_pDevice->CreateBuffer(&buff_desc, &InitData, &d3dBuffer);
//	if ( FAILED(hr) )
//		throw CD3D11Exception(hr);
//
//	// Create the API buffer and assign it to output variable
//	buffers[0] = new CIndexBufferD3D11(d3dBuffer);
//}
//
//// Constant buffer creation
//void RendererD3D11::Create(size_t sizeInBytes, CResourceAccess usage, IConstantBuffer** buffers)
//{
//	// Fill in a buffer description.
//	D3D11_BUFFER_DESC buff_desc;
//	buff_desc.ByteWidth = static_cast<size_t>(bufferInfo.sizeInBytes);
//	buff_desc.Usage = D3D11BufferUsage(bufferInfo.usage);
//	buff_desc.BindFlags = D3D11BindFlag(bufferInfo.bufferType);
//
//	// Dynamic buffers can write to a resource.
//	// Staging buffers can read from or write to a resource.
//	switch (buff_desc.Usage)
//	{
//	case D3D11_USAGE_DYNAMIC:
//		buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//		break;
//	case D3D11_USAGE_STAGING:
//		buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
//		break;
//	default:
//		buff_desc.CPUAccessFlags = 0;
//		break;
//	};
//
//	buff_desc.MiscFlags = 0; // D3D11_RESOURCE_MISC_FLAG (for textures and such)
//	buff_desc.StructureByteStride = 0;
//	
//	// Fill in the subresource data.
//	D3D11_SUBRESOURCE_DATA InitData;
//	InitData.pSysMem = reinterpret_cast<const void*>(bufferInfo.data); // Pointer to the initialization data.
//
//	// The distance (in bytes) from the beginning of one line of a texture to the next line.
//	// System-memory pitch is used only for 2D and 3D texture data as it is has no meaning
//	// for the other resource types.
//	InitData.SysMemPitch = 0;
//
//	// The distance (in bytes) from the beginning of one depth level to the next.
//	// System-memory-slice pitch is only used for 3D texture data as it has no
//	// meaning for the other resource types.
//	InitData.SysMemSlicePitch = 0;
//
//	// Create the buffer from the D3D11 device
//	ID3D11BufferPtr d3dBuffer = nullptr;
//	HRESULT hr = m_pDevice->CreateBuffer(&buff_desc, &InitData, &d3dBuffer);
//	if ( FAILED(hr) )
//		throw CD3D11Exception(hr);
//
//	// Create the API buffer and assign it to output variable
//	buffers[0] = new CConstantBufferD3D11(d3dBuffer);
//}
//


//ID3D11DeviceChildPtr RendererD3D11::Create(const CShaderData& shaderData, CShader::ShaderVersion version)
//{
//
//
//}
//
//ID3D11InputLayoutPtr RendererD3D11::Create(CVertexElement** elements, size_t numElements, size_t totalStride)
//{
//
//
//}
//
//ID3D11BufferPtr RendererD3D11::Create(const SBufferInfo& bufferInfo)
//{
//	// #To do:
//	// If the bind flag is D3D11_BIND_CONSTANT_BUFFER then the ByteWidth value must be
//	// in multiples of 16, and less than or equal to D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT.
//
//	// Fill in a buffer description.
//	D3D11_BUFFER_DESC buff_desc;
//	buff_desc.ByteWidth = static_cast<size_t>(bufferInfo.sizeInBytes);
//	buff_desc.Usage = D3D11BufferUsage(bufferInfo.usage);
//	buff_desc.BindFlags = D3D11BindFlag(bufferInfo.bufferType);
//
//	// Dynamic buffers can write to a resource.
//	// Staging buffers can read from or write to a resource.
//	switch (buff_desc.Usage)
//	{
//	case D3D11_USAGE_DYNAMIC:
//		buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//		break;
//	case D3D11_USAGE_STAGING:
//		buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
//		break;
//	default:
//		buff_desc.CPUAccessFlags = 0;
//		break;
//	};
//
//	buff_desc.MiscFlags = 0; // D3D11_RESOURCE_MISC_FLAG (for textures and such)
//	buff_desc.StructureByteStride = 0;
//	
//	// Fill in the subresource data.
//	D3D11_SUBRESOURCE_DATA InitData;
//	InitData.pSysMem = static_cast<const void*>(bufferInfo.data); // Pointer to the initialization data.
//
//	// The distance (in bytes) from the beginning of one line of a texture to the next line.
//	// System-memory pitch is used only for 2D and 3D texture data as it is has no meaning
//	// for the other resource types.
//	InitData.SysMemPitch = 0;
//
//	// The distance (in bytes) from the beginning of one depth level to the next.
//	// System-memory-slice pitch is only used for 3D texture data as it has no
//	// meaning for the other resource types.
//	InitData.SysMemSlicePitch = 0;
//
//	// Create the buffer from the D3D11 device
//	ID3D11BufferPtr d3dBuffer = nullptr;
//	HRESULT hr = m_pDevice->CreateBuffer(&buff_desc, &InitData, &d3dBuffer);
//	if ( FAILED(hr) )
//		throw CD3D11Exception(hr);
//
//	// return the created resource
//	return d3dBuffer;
//}
//
//void RendererD3D11::Create(const STexture1DInfo& texInfo, ID3D11Texture1DPtr* textures)
//{
//	// Build API independant texture information
//	CTexture1D texture1DInfo(texInfo.tformat, texInfo.width, texInfo.numLevels,
//		texInfo.imageSize, texInfo.usage);
//
//	// Multisample desc
//	DXGI_SAMPLE_DESC sample_desc;
//	sample_desc.Count = 1;
//	sample_desc.Quality = 0;
//
//	// TextureDesc
//	D3D11_TEXTURE1D_DESC texture_desc;
//	texture_desc.Width = static_cast<size_t>(texInfo.width);
//	texture_desc.MipLevels = static_cast<size_t>(texInfo.numLevels);
//	texture_desc.Format = D3D11DXGIFormat(texInfo.tformat);
//	texture_desc.ArraySize = 1;
//	texture_desc.Usage = D3D11BufferUsage(texInfo.usage);
//	texture_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
//	texture_desc.CPUAccessFlags = 0;
//	texture_desc.MiscFlags = 0;
//
//	// Applications cannot specify nullptr for pInitData when creating IMMUTABLE resources 
//	std::vector<D3D11_SUBRESOURCE_DATA> initData;
//	if (texture_desc.Usage == D3D11_USAGE_IMMUTABLE)
//	{
//		// Create the sub resource
//		initData.resize(texture_desc.MipLevels);
//
//		// Loop over all the mips and copy their data to the subresource
//		for (size_t i = 0; i < texture_desc.MipLevels; ++i)
//		{
//			// Fill in the subresource data. If not multisampled
//			if (sample_desc.Count > 1 && sample_desc.Quality != 0)
//			{
//				initData[i].pSysMem = static_cast<const void*>(texInfo.pData); // Pointer to the initialization data.
//			}
//
//			// Both have no meaning for 1D Textures
//			initData[i].SysMemPitch = 0;
//			initData[i].SysMemSlicePitch = 0;
//		}
//	}
//
//	// Create the textures from the D3D11 device
//	std::vector<ID3D11Texture1DPtr> d3dTextures( texture_desc.ArraySize );
//	HRESULT hr = m_pDevice->CreateTexture1D( &texture_desc, &initData[0], &d3dTextures[0] );
//	if ( FAILED(hr) ) // Check result of hersult
//		throw CD3D11Exception(hr);
//
//	// Create Texture objects for the engine
//	for (size_t i = 0; i < texture_desc.ArraySize; ++i)
//	{
//		textures[i] = new CTexture1DD3D11(texture1DInfo, d3dTextures[i]);
//	}
//}
//
//void RendererD3D11::Create(const STexture2DInfo& texInfo, ITexture2D** textures)
//{
//	// Multisample desc
//	DXGI_SAMPLE_DESC sample_desc;
//	sample_desc.Count = 1;
//	sample_desc.Quality = 0;
//
//	// TextureDesc
//	D3D11_TEXTURE2D_DESC texture_desc;
//	texture_desc.Width = static_cast<size_t>(texInfo.width);
//	texture_desc.Height = static_cast<size_t>(texInfo.height);
//	texture_desc.MipLevels = static_cast<size_t>(texInfo.numLevels);
//	texture_desc.Format = D3D11DXGIFormat(texInfo.tformat);
//	texture_desc.ArraySize = 1;
//	texture_desc.SampleDesc = sample_desc;
//	texture_desc.Usage = D3D11BufferUsage(texInfo.usage);
//	texture_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
//	texture_desc.CPUAccessFlags = 0;
//	texture_desc.MiscFlags = 0;
//
//	// Applications cannot specify nullptr for pInitData when creating IMMUTABLE resources 
//	std::vector<D3D11_SUBRESOURCE_DATA> initData;
//	if (texture_desc.Usage == D3D11_USAGE_IMMUTABLE)
//	{
//		// Create the sub resources
//		initData.resize(texInfo.numLevels);
//
//		// Temp width and height
//		size_t width = texture_desc.Width;
//		size_t height = texture_desc.Height;
//
//		// Check for compression outside of loop
//		bool isCompressed = IsBlockCompressed(texture_desc.Format);
//		switch (isCompressed)
//		{
//		case true:
//			{
//				// Block comression bytes per block
//				size_t dxtBytesPerBlock = 16;
//				switch (texture_desc.Format)
//				{
//				case DXGI_FORMAT_BC1_TYPELESS:
//				case DXGI_FORMAT_BC1_UNORM:
//				case DXGI_FORMAT_BC1_UNORM_SRGB:
//				case DXGI_FORMAT_BC4_TYPELESS:
//				case DXGI_FORMAT_BC4_UNORM:
//				case DXGI_FORMAT_BC4_SNORM:
//					dxtBytesPerBlock = 8;
//					break;
//
//				default:
//					break;
//				};
//
//				// Loop over all the mips and copy their data to the subresource
//				for (size_t i = 0, offset = 0; i < texture_desc.MipLevels; ++i)
//				{
//					size_t numBlocksWide = 0;
//					if (width > 0)
//						numBlocksWide = std::max(1U, width/4);
//					
//					size_t numBlocksHigh = 0;
//					if (height > 0)
//						numBlocksHigh = std::max(1U, height/4);
//					
//					size_t rowBytes = numBlocksWide*numBlocksHigh;
//					size_t numBytes = rowBytes*numBlocksHigh;
//
//					// Fill in the subresource data. If not multisampled
//					if (sample_desc.Count > 1 && sample_desc.Quality != 0)
//					{
//						initData[i].pSysMem = static_cast<const void*>(texInfo.pData + offset); // Pointer to the initialization data.
//						offset += numBytes;
//					}
//
//					// Set the mem pitch to point to the next block
//					initData[i].SysMemPitch = rowBytes;
//
//					// Bit shift the width to the next mip map
//					if (width > 1)
//						width >>= 1;
//
//					// Bit shift the height to the next mip map
//					if (height > 1)
//						height >>= 1;
//				} 
//			}
//			break;
//
//		case false:
//			{
//				// Loop over all the mips and copy their data to the subresource
//				for (size_t i = 0, offset = 0; i < texture_desc.MipLevels; ++i)
//				{
//					size_t bpp = BitsPerPixel(texture_desc.Format);
//					size_t rowBytes = (width * bpp + 7) / 8; // round up to nearest byte
//					size_t numBytes = rowBytes * height;
//
//					// Fill in the subresource data. If not multisampled
//					if (sample_desc.Count > 1 && sample_desc.Quality != 0)
//					{
//						initData[i].pSysMem = static_cast<const void*>(texInfo.pData + offset); // Pointer to the initialization data.
//						offset += numBytes;
//					}
//
//					// Set the mem pitch to point to the next block
//					initData[i].SysMemPitch = rowBytes;
//				}
//			} 
//			break;
//		}; // switch
//	} // if 
//
//	// Create the texture from the D3D11 device
//	ID3D11Texture2DPtr d3dTexture = nullptr;
//	HRESULT hr = m_pDevice->CreateTexture2D(&texture_desc, &initData[0], &d3dTexture);		
//	if ( FAILED(hr) ) // Check result of hersult
//		throw CD3D11Exception(hr);
//
//	// Return the sucessful texture
//	return d3dTexture;
//}