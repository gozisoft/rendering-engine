#include "librendererfwd.h"
#include "DeferredResourcesGL.h"

// opengl
#include "RendererGL.h"
#include "RenderDataGL.h"
#include "RenderResourcesGL.h"
#include "BufferGL.h"
#include "VertexFormatGL.h"
#include "Texture1DGL.h"
#include "Texture2DGL.h"
#include "MappingGL.h"

// Generic
#include "RenderCommand.h"
#include "SceneNode.h"

using namespace std::placeholders; 

_ENGINE_BEGIN

namespace Video
{
	void CreateDeferredGL(const IRenderResourcesPtr& in, IRenderResourcesPtr& out)
	{
		CRenderResourcesGLPtr pResource = std::static_pointer_cast<CRenderResourcesGL>(in);

		out = std::make_shared<CDeferredResourcesGL>(pResource->m_buffers, pResource->m_vertexFormats,
			pResource->m_1DTextures, pResource->m_2DTextures);
	}
}


CDeferredResourcesGL::CDeferredResourcesGL(BufferMap& buffers, VertexFormatMap& formats,
	Texture1DMap& tex1D, Texture2DMap& tex2D) :
m_buffers(buffers),
m_vertexFormats(formats),
m_1DTextures(tex1D),
m_2DTextures(tex2D)
{

}

CDeferredResourcesGL::~CDeferredResourcesGL()
{

}

// -------------------------------------------------------------------------------------------
// Command list functions
// -------------------------------------------------------------------------------------------
void CDeferredResourcesGL::FinishRenderCommands(commandList& pRC)
{
	// Render command is not empty
	assert( pRC.empty() );

	// copy over the render command
	pRC = m_renderCommands;

	// reset the original render command
	m_renderCommands.clear();
}

// -------------------------------------------------------------------------------------------
// General Shader
// -------------------------------------------------------------------------------------------
void CDeferredResourcesGL::Enable(const CShaderPtr& pShader)
{


}

void CDeferredResourcesGL::Disable(const CShaderPtr& pShader)
{


}
// -------------------------------------------------------------------------------------------
// General Buffer
// -------------------------------------------------------------------------------------------
void CDeferredResourcesGL::Enable(const BufferPtr& pBuffer)
{
	// search to find if buffer is active
	auto itor = m_buffers.find( pBuffer );
	CBufferGLPtr pBufferGL = CBufferGLPtr();
	if ( itor != m_buffers.end() )
	{
		pBufferGL = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pBufferGL = std::make_shared<CBufferGL>(pBuffer);
		m_buffers.insert( std::make_pair(pBuffer, pBufferGL) );
	}

	std::function<void()> zeroArg = [&pBufferGL] { pBufferGL->Enable(); };
	m_renderCommands.push_back(zeroArg); 
}

void CDeferredResourcesGL::Disable(const BufferPtr& pBuffer)
{
	// search to find if buffer is active
	auto itor = m_buffers.find(pBuffer);
	if ( itor != m_buffers.end() )
	{
		CBufferGLPtr pBufferGL = (*itor).second;

		// ensure the next command has been created
		std::function<void()> zeroArg = [&pBufferGL] { pBufferGL->Disable(); };
		m_renderCommands.push_back(zeroArg);
	}
}

void* CDeferredResourcesGL::Map(const BufferPtr& pBuffer)
{
	return nullptr;
}

void CDeferredResourcesGL::Unmap(const BufferPtr& pBuffer)
{


}
// -------------------------------------------------------------------------------------------
// VertexFormat
// -------------------------------------------------------------------------------------------
void CDeferredResourcesGL::Enable(const CVertexFormatPtr& pVFormat)
{
	// search to find if vertex buffer is active
	auto itor = m_vertexFormats.find(pVFormat);
	CVertexFormatGLPtr pVFormatGL = CVertexFormatGLPtr();
	if ( itor != m_vertexFormats.end() )
	{
		pVFormatGL = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pVFormatGL = std::make_shared<CVertexFormatGL>(pVFormat);
		m_vertexFormats.insert( std::make_pair(pVFormat, pVFormatGL) );
	}

	std::function<void()> zeroArg = [&pVFormatGL] { pVFormatGL->Enable(); };
	m_renderCommands.push_back(zeroArg); 
}

void CDeferredResourcesGL::Disable(const CVertexFormatPtr& pVFormat)
{
	// search to find if buffer is active
	auto itor = m_vertexFormats.find(pVFormat);
	if ( itor != m_vertexFormats.end() )
	{
		const CVertexFormatGLPtr& pVFormatGL = (*itor).second;

		std::function<void()> zeroArg = [&pVFormatGL] { pVFormatGL->Disable(); };
		m_renderCommands.push_back(zeroArg);
	}
}
// -------------------------------------------------------------------------------------------
// Draw Functions
// -------------------------------------------------------------------------------------------
void CDeferredResourcesGL::Draw(const VertexBufferPtr& pVBuffer, Visual::PrimitiveType primitiveType)
{
/*	
	GLenum mode = 0;
	GLsizei numVertices = 0;
	switch (primitiveType)
	{
	case CVisualNode::PT_POLYPOINTS: 
		mode = GL_POINTS;
		break;

	case CVisualNode::PT_POLYLINE_CLOSED:
		return GL_LINES;

	case CVisualNode::PT_POLYLINE_OPEN: 
		return GL_LINE_STRIP;

	case CVisualNode::PT_TRIMESH: 
		return GL_TRIANGLES;

	case CVisualNode::PT_TRISTRIP:
		return GL_TRIANGLE_STRIP;

	case CVisualNode::PT_TRIFAN:
		return GL_TRIANGLE_FAN;

	default:
		assert( false && ieS("Unkown primitive type: GLPrimitiveType()") );
		return 0;
	};
*/
}

void CDeferredResourcesGL::DrawIndexd(const IndexBufferPtr& pIBuffer, Visual::PrimitiveType primitiveType)
{
	// check for invalid pointer
	assert( pIBuffer.get() );

	GLenum indexType = 0;
	GLvoid* pIndexData = nullptr;
	switch ( pIBuffer->GetType() )
	{
	case Buffer::IT_USHORT:
		indexType = GL_UNSIGNED_SHORT;
		pIndexData = reinterpret_cast<UShort*>( pIBuffer->GetData() ) + 0; // pIBuffer->GetIndexOffset();
		break;
	case Buffer::IT_size_t:
		indexType = GL_UNSIGNED_INT;
		pIndexData = reinterpret_cast<uint32_t*>( pIBuffer->GetData() ) + 0; // pIBuffer->GetIndexOffset();
		break;
	}

	// Determine the opengl rendering mode (triangles, triangestrip etc)
	GLenum mode = GLPrimitiveType(primitiveType);

	// Store either a glDrawRangeElements or glDrawElements function call.
	// This is dependant on the index buffer having its min and max values calculated.
	std::function<void()> zeroArg;
	GLsizei numIndices = static_cast<GLsizei>( pIBuffer->GetNumElements() );
	if ( GLsize_t maxIndex = static_cast<GLsize_t>( pIBuffer->GetMaxRange() ) )
	{
		GLsize_t minIndex = static_cast<GLsize_t>( pIBuffer->GetMinRange() );

		zeroArg = [mode, minIndex, maxIndex, numIndices, indexType, pIndexData]()
		{		
			glDrawRangeElements(mode, minIndex, maxIndex, numIndices, indexType, pIndexData); 
		};
	}
	else
	{
		zeroArg = [mode, numIndices, indexType, pIndexData]()
		{
			glDrawElements(mode, numIndices, indexType, pIndexData);
		};
	}

	m_renderCommands.push_back(zeroArg);
}

void CDeferredResourcesGL::DrawNode(const Visual* pNode)
{
	Visual::PrimitiveType type = pNode->GetType();
	const VertexBufferPtr& pVBuffer = pNode->GetVertexBuffer();
	const CVertexFormatPtr& pVFormat = pNode->GetVertexFormat();
	const IndexBufferPtr& pIBuffer = pNode->GetIndexBuffer();

	switch (type)
	{
	case Visual::PT_TRISTRIP:
	case Visual::PT_TRIMESH:
	case Visual::PT_TRIFAN:
		{
			GLenum indexType = 0;
			GLvoid* pIndexData = nullptr;
			switch ( pIBuffer->GetType() )
			{
			case Buffer::IT_USHORT:
				indexType = GL_UNSIGNED_SHORT;
				pIndexData = reinterpret_cast<UShort*>( pIBuffer->GetData() ) + 0; // pIBuffer->GetIndexOffset();
				break;
			case Buffer::IT_size_t:
				indexType = GL_UNSIGNED_INT;
				pIndexData = reinterpret_cast<uint32_t*>( pIBuffer->GetData() ) + 0; // pIBuffer->GetIndexOffset();
				break;
			}

			// Store either a glDrawRangeElements or glDrawElements function call.
			// This is dependant on the index buffer having its min and max values calculated.
			std::function<void()> zeroArg;
			GLsizei numIndices = static_cast<GLsizei>( pIBuffer->GetNumElements() );
			if ( GLsize_t maxIndex = static_cast<GLsize_t>( pIBuffer->GetMaxRange() ) )
			{
				GLsize_t minIndex = static_cast<GLsize_t>( pIBuffer->GetMinRange() );

				zeroArg = [type, minIndex, maxIndex, numIndices, indexType, pIndexData]()
				{ 
					GLenum primitiveType = GLPrimitiveType(type);
					glDrawRangeElements(primitiveType, minIndex, maxIndex, numIndices, indexType, pIndexData); 
				};
			}
			else
			{
				zeroArg = [type, numIndices, indexType, pIndexData]()
				{
					GLenum primitiveType = GLPrimitiveType(type);
					glDrawElements(primitiveType, numIndices, indexType, pIndexData);
				};
			}

			m_renderCommands.push_back(zeroArg);	
		}
		break;
	case Visual::PT_POLYLINE_OPEN:
		{
			const CPolyline* pPolyline = static_cast<const CPolyline*>(pNode);
			GLsizei numVerts = (GLsizei)pPolyline->GetNumLines();

			if (numVerts)
			{
				::glDrawArrays(GL_LINE_STRIP, 0, numVerts);
			}
		}
		break;
	case Visual::PT_POLYLINE_CLOSED:
		{
			const CPolyline* pPolyline = static_cast<const CPolyline*>(pNode);
			GLsizei numVerts = (GLsizei)pPolyline->GetNumLines();

			if (numVerts)
			{
				::glDrawArrays(GL_LINES, 0, numVerts);
			}
		}
		break;
	case Visual::PT_POLYPOINTS:
		{
			const CPolypoint* pPolypoint = static_cast<const CPolypoint*>(pNode);
			GLsizei numVerts = (GLsizei)pPolypoint->GetNumPoints();

			if (numVerts)
			{
				::glDrawArrays(GL_POINTS, 0, numVerts);
			}
		}
		break;
	default:
		assert( false && ieS("Invalid type\n") );
		break;

	} // switch
}

_ENGINE_END

/*
void CDeferredResourcesGL::FinishRenderCommands(CRenderCommandPtr& pRC)
{
	// this will assign the render command data so it better be 
	// clear before hand
	// assert( pRC.get() != nullptr );

	// create the render command
	pRC = std::make_shared<CRenderCommand>();

	// copy over the render command
	pRC->enableFunctions = m_renderCommands->enableFunctions;
	pRC->disableFunctions = m_renderCommands->disableFunctions;
	pRC->primitiveType = m_renderCommands->primitiveType;

	// reset the original render command
	pRC->enableFunctions.clear();
	pRC->disableFunctions.clear();
	pRC->primitiveType = CVisualNode::PT_NONE;
}
*/


/*CDeferredResourcesGL::CDeferredResourcesGL(const CRenderResourcesGLPtr& pImmediate,
	BufferMap& buffers,	VertexFormatMap& formats, Texture1DMap& tex1D,
	Texture2DMap& tex2D) :
m_pImmediate(pImmediate),
m_buffers(buffers),
m_vertexFormats(formats),
m_1DTextures(tex1D),
m_2DTextures(tex2D),
m_renderCommands( std::make_shared<CRenderCommand>() )
{

}

CDeferredResourcesGL::~CDeferredResourcesGL()
{

}

// -------------------------------------------------------------------------------------------
// Command list functions
// -------------------------------------------------------------------------------------------
void CDeferredResourcesGL::FinishRenderCommands(CRenderCommandPtr& pRC)
{
	// this will assign the render command data so it better be 
	// clear before hand
	// assert( pRC.get() != nullptr );

	// create the render command
	pRC = std::make_shared<CRenderCommand>();

	// copy over the render command
	pRC->vertexBuffer = m_renderCommands->vertexBuffer;
	pRC->indexBuffer = m_renderCommands->indexBuffer;
	pRC->vertexFormat = m_renderCommands->vertexFormat;
	pRC->primitiveType = m_renderCommands->primitiveType;

	// reset the original render command
	pRC->vertexBuffer.reset();
	pRC->indexBuffer.reset();
	pRC->vertexFormat.reset();
	pRC->primitiveType = CVisualNode::PT_NONE;
}

// -------------------------------------------------------------------------------------------
// VertexShader
// -------------------------------------------------------------------------------------------
void CDeferredResourcesGL::Bind(const CVertexShaderPtr& pVShader)
{


}

void CDeferredResourcesGL::UnBind(const CVertexShaderPtr& pVShader)
{


}

// -------------------------------------------------------------------------------------------
// PixelShader
// -------------------------------------------------------------------------------------------
void CDeferredResourcesGL::Bind(const CPixelShaderPtr& pPShader)
{


}

void CDeferredResourcesGL::UnBind(const CPixelShaderPtr& pPShader)
{


}

// -------------------------------------------------------------------------------------------
// VertexFormat
// -------------------------------------------------------------------------------------------
void CDeferredResourcesGL::Enable(const CVertexFormatPtr& pVFormat)
{
	// search to find if vertex buffer is active
	auto itor = m_vertexFormats.find(pVFormat);
	CVertexFormatGLPtr pVFormatGL = CVertexFormatGLPtr();
	if ( itor != m_vertexFormats.end() )
	{
		pVFormatGL = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pVFormatGL = std::make_shared<CVertexFormatGL>(pVFormat);
		m_vertexFormats.insert( std::make_pair(pVFormat, pVFormatGL) );
	}

	m_renderCommands->vertexFormats.push_back(pVFormatGL);
}

void CDeferredResourcesGL::Disable(const CVertexFormatPtr& pVFormat)
{
	auto itor = m_vertexFormats.find( pVFormat );
	if ( itor != m_vertexFormats.end() )
	{
		CVertexFormatGLPtr pVFormatGL = (*itor).second;
//		m_renderCommands->disableFunctions.push_back( std::bind(&CVertexFormatGL::Disable, pVFormatGL) );
	}
}

void CDeferredResourcesGL::Bind(const CVertexFormatPtr& pVFormat)
{

}

void CDeferredResourcesGL::UnBind(const CVertexFormatPtr& pVFormat)
{


}

// -------------------------------------------------------------------------------------------
// General Buffer
// -------------------------------------------------------------------------------------------
void CDeferredResourcesGL::Enable(const CBufferPtr& pBuffer)
{
	// search to find if buffer is active
	auto itor = m_buffers.find( pBuffer );
	CBufferGLPtr pBufferGL = CBufferGLPtr();
	if ( itor != m_buffers.end() )
	{
		pBufferGL = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pBufferGL = std::make_shared<CBufferGL>(pBuffer);
		m_buffers.insert( std::make_pair(pBuffer, pBufferGL) );
	}

	m_renderCommands->enableFunctions.push_back( std::bind(&CBufferGL::Enable, pBufferGL) );
}

void CDeferredResourcesGL::Disable(const CBufferPtr& pBuffer)
{
	// search to find if buffer is active
	auto itor = m_buffers.find(pBuffer);
	if ( itor != m_buffers.end() )
	{
		CBufferGLPtr pBufferGL = (*itor).second;

		// ensure the next command has been created
		m_renderCommands->disableFunctions.push_back( std::bind(&CBufferGL::Disable, pBufferGL) );
	}
}


void CDeferredResourcesGL::Bind(const CBufferPtr& pBuffer)
{


}

void CDeferredResourcesGL::UnBind(const CBufferPtr& pBuffer)
{


}

void* CDeferredResourcesGL::Map(const CBufferPtr& pBuffer)
{
	return nullptr;
}

void CDeferredResourcesGL::Unmap(const CBufferPtr& pBuffer)
{


}
*/