#pragma once

#ifndef RENDERER_FORWARD_H
#define RENDERER_FORWARD_H

#include "Core.h"
#include <memory>

_ENGINE_BEGIN

// Rendering device
class IRenderer;

// Rendering pipline
class IPipeline;

// Rendering Resource
class IResource;
class IBuffer;
class ITexture;
class ITexture1D;
class ITexture2D;

// Shader Resource
class IView;
class IShaderResource;
class IRenderTarget;
class IDepthStencil;

// Shader
class IShader;
class IInputFormat;
class CInputElement;

// RasterizerState
class IRasterizerState;
class RasterizerState;

//----------------------------------------------------------------------------
// Special pointer type (HANDLEs)
//----------------------------------------------------------------------------
struct SHADERHANDLE {};
typedef SHADERHANDLE* PSHADERHANDLE;
typedef shared_ptr<SHADERHANDLE> shader_handle;

_ENGINE_END

#endif