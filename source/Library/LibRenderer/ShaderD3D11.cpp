#include "librendererafx.h"
#include "ShaderD3D11.h"
#include "PipelineD3D11.h"
#include "ResourceD3D11.h"
#include "ViewD3D11.h"
#include <vector>

using namespace engine;

//-------------------------------------------------------------------------------------------------------------------
// these VTables must be setup in the proper order:
// 1) SetShader
// 2) SetConstantBuffers
// 3) SetSamplers
// 4) SetShaderResources
// 5) CreateShader
SD3DShaderVTable CShaderD3D11::s_vertexShaderTable =
{
	(void(__stdcall ID3D11DeviceContext::*)(ID3D11DeviceChildPtr, ID3D11ClassInstance*const*, size_t)) &ID3D11DeviceContext::VSSetShader,
	&ID3D11DeviceContext::VSSetConstantBuffers,
	&ID3D11DeviceContext::VSSetSamplers,
	&ID3D11DeviceContext::VSSetShaderResources,
	(HRESULT(__stdcall ID3D11Device::*)(const void *, SIZE_T, ID3D11ClassLinkage*, ID3D11DeviceChild **)) &ID3D11Device::CreateVertexShader
};
//-------------------------------------------------------------------------------------------------------------------
SD3DShaderVTable CShaderD3D11::s_geometryShaderTable =
{
	(void(__stdcall ID3D11DeviceContext::*)(ID3D11DeviceChildPtr, ID3D11ClassInstance*const*, size_t)) &ID3D11DeviceContext::GSSetShader,
	&ID3D11DeviceContext::GSSetConstantBuffers,
	&ID3D11DeviceContext::GSSetSamplers,
	&ID3D11DeviceContext::GSSetShaderResources,
	(HRESULT(__stdcall ID3D11Device::*)(const void *, SIZE_T, ID3D11ClassLinkage*, ID3D11DeviceChild **)) &ID3D11Device::CreateGeometryShader
};
//-------------------------------------------------------------------------------------------------------------------
SD3DShaderVTable CShaderD3D11::s_pixelShaderTable =
{
	(void(__stdcall ID3D11DeviceContext::*)(ID3D11DeviceChildPtr, ID3D11ClassInstance*const*, size_t)) &ID3D11DeviceContext::PSSetShader,
	&ID3D11DeviceContext::PSSetConstantBuffers,
	&ID3D11DeviceContext::PSSetSamplers,
	&ID3D11DeviceContext::PSSetShaderResources,
	(HRESULT(__stdcall ID3D11Device::*)(const void *, SIZE_T, ID3D11ClassLinkage*, ID3D11DeviceChild **)) &ID3D11Device::CreatePixelShader
};

//-------------------------------------------------------------------------------------------------------------------
CShaderD3D11::CShaderD3D11(ID3D11DeviceChildPtr pShader, SD3DShaderVTable* vtable)
	:
	m_shader(pShader), m_vtable(vtable)
{
}

//-------------------------------------------------------------------------------------------------------------------
CShaderD3D11::~CShaderD3D11()
{
	//if (m_shader)
	//	m_shader.Release();
}

//-------------------------------------------------------------------------------------------------------------------
void CShaderD3D11::enableConstantBuffers(const IPipeline* pipeline, IBuffer* const* cbuffers, size_t numbuffers, size_t startslot)
{
	// Verify the type of the pipeline
	const PipelineD3D11* pipelineD3D = PolymorphicDowncast<const PipelineD3D11*>(pipeline);
	if (!pipelineD3D)
		throw std::runtime_error("Invalid IPipeline type [CShaderD3D11::Enable]");

	// Verify the buffers are there
	if (!cbuffers)
		throw std::runtime_error("Invalid constant buffers [CShaderD3D11::Enable]");

	// Check start slot is less than cap
	if (!(startslot < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT))
		throw std::runtime_error("startslot is higher than D3D11 cbuffer slot count [CShaderD3D11::Enable]");

	// Check the number of buffers is not higher than the max buffer cap
	size_t maxBufferCount = D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT - startslot;
	if (numbuffers > maxBufferCount)
		throw std::runtime_error("numbuffers is higher than D3D11 max cbuffer count [CShaderD3D11::Enable]");

	// Fill an array of D3D11 buffers
	std::vector<ID3D11BufferPtr> buffers(numbuffers);
	for (size_t i = 0; i < numbuffers; ++i)
	{
		const CBufferD3D11* pBufferD3D = PolymorphicDowncast<const CBufferD3D11*>(cbuffers[i]);
#if defined DEBUG || _DEBUG
		if (!pBufferD3D)
			throw std::runtime_error("A member IBuffer** cbuffers is of incorrect type [CShaderD3D11::Enable]");
#endif

		// Assign one buffer at a time :(
		buffers[i] = pBufferD3D->get();
	}

	// Get the d3ddevice context
	ID3D11DeviceContextPtr context = pipelineD3D->Get();

	// Set the contants
	(context->*(m_vtable->pSetConstantBuffers))(startslot, numbuffers, &buffers[0]);
}

//-------------------------------------------------------------------------------------------------------------------
void CShaderD3D11::enableShaderResource(const IPipeline* pipeline, IShaderResource* const* resources, size_t numresources, size_t startslot)
{
	// Verify the type of the pipeline
	const PipelineD3D11* pipelineD3D = PolymorphicDowncast<const PipelineD3D11*>(pipeline);
	if (!pipelineD3D)
		throw std::runtime_error("Invalid IPipeline type [CShaderD3D11::Enable]");

	// Verify the buffers are there
	if (!resources)
		throw std::runtime_error("Invalid shader resources [CShaderD3D11::Enable]");

	// Check start slot is less than cap
	if (!(startslot < 128))
		throw std::runtime_error("Startslot is higher than D3D11 shader resources slot count [CShaderD3D11::Enable]");

	// Check the number of buffers is not higher than the max buffer cap
	size_t maxBufferCount = 128 - startslot;
	if (numresources > maxBufferCount)
		throw std::runtime_error("Numbuffers is higher than D3D11 shader resources count [CShaderD3D11::Enable]");

	// Fill an array of D3D11 buffers
	std::vector<ID3D11ShaderResourceViewPtr> resViews(numresources);
	for (size_t i = 0; i < numresources; ++i) // Assign one buffer at a time :(
	{
		resViews[i] = PolymorphicDowncast<CShaderResourceD3D11*>(resources[i])->get();
	}

	// Get the d3ddevice context
	ID3D11DeviceContextPtr context = pipelineD3D->Get();

	// Set the constants
	(context ->* (m_vtable->pSetShaderResources))(startslot, numresources, &resViews[0]);
}

//-------------------------------------------------------------------------------------------------------------------
void CShaderD3D11::enableShader(const IPipeline* pipeline)
{
	// Verify the type of the pipeline
	const PipelineD3D11* pipelineD3D = PolymorphicDowncast<const PipelineD3D11*>(pipeline);
	if (!pipelineD3D)
		throw std::runtime_error("Invalid IPipeline type [CShaderD3D11::Enable]");

	// Get the d3ddevice context
	ID3D11DeviceContextPtr context = pipelineD3D->Get();

	// Set the shader via the vtable
	(context ->* (m_vtable->pSetShader))(m_shader, nullptr, 0);
}

//-------------------------------------------------------------------------------------------------------------------
CShaderType::Type CShaderD3D11::GetShaderType() const
{
	if (m_vtable == &s_vertexShaderTable)
		return CShaderType::ST_VERTEX;
	if (m_vtable == &s_geometryShaderTable)
		return CShaderType::ST_GEOMETRY;
	if (m_vtable == &s_pixelShaderTable)
		return CShaderType::ST_PIXEL;


	throw std::runtime_error("Unknown or unsupported shader type [CShaderD3D11::GetShaderType]");
}


//ID3D11DeviceChildPtr CreateShader(const CShader* pShader, ID3D11DevicePtr& pDevice, SD3DShaderVTable** vtable)
//{
//	// Vtable reference
//	SD3DShaderVTable* temp = *vtable;
//
//    // VertexShader, GeometryShader, PixelShader
//	ID3D11DeviceChildPtr pObjectD3D = nullptr;
//
//	// D3DBlob for creation
//	const handle& marker = pShader->GetShaderHANDLE();
//	ID3DBlob* pBlob = reinterpret_cast<ID3DBlob*>( marker.get() );
//
//	// Create the shader
//	switch ( pShader->GetShaderType() )
//	{
//	case CShader::ST_VERTEX: temp = &g_vertexShaderTable; break;
//	case CShader::ST_GEOMETRY: temp = &g_geometryShaderTable; break;
//	case CShader::ST_PIXEL: temp = &g_pixelShaderTable; break;
//	default:
//		assert( false && ieS("Unknown shader type") );
//		break;
//	};
//
//	// Create the shader using the device
//	HRESULT hr = ( pDevice->*(temp->pCreateShader) )
//		(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &pObjectD3D);
//
//	if ( FAILED(hr) )
//	{
//		Video::CD3D11Exception(hr);
//		return nullptr;
//	}
//
//	return pObjectD3D;
//}

