#ifndef __TEXTURE_TOOLS_INL__
#define __TEXTURE_TOOLS_INL__

//----------------------------------------------------------------------------------------------------------------------------
inline void ConvertFrom(CTextureFormat::Type tFormat, size_t numTexels, const uint8_t* inTexels, Colour* outTexels)
{
	switch (tFormat)
	{
	case CTextureFormat::TF_R32G32B32A32_FLOAT: ConvertFromR32G32B32A32_FLOAT(numTexels, inTexels, outTexels); break;
	case CTextureFormat::TF_R32G32B32A32_size_t: // Unit and int the same
	case CTextureFormat::TF_R32G32B32A32_INT: ConvertFromR32G32B32A32(numTexels, inTexels, outTexels); break;

	case CTextureFormat::TF_R32G32B32_FLOAT: ConvertFromR32G32B32_FLOAT(numTexels, inTexels, outTexels); break;
	case CTextureFormat::TF_R32G32B32_size_t: // Unit and int the same
	case CTextureFormat::TF_R32G32B32_INT: ConvertFromR32G32B32(numTexels, inTexels, outTexels); break;

	case CTextureFormat::TF_R32G32_FLOAT: ConvertFromR32G32_FLOAT(numTexels, inTexels, outTexels); break;
	case CTextureFormat::TF_R32G32_size_t: // Unit and int the same
	case CTextureFormat::TF_R32G32_INT: ConvertFromR32G32(numTexels, inTexels, outTexels); break;

	case CTextureFormat::TF_R8G8B8A8_UNORM: ConvertFromR8G8B8_UNORM(numTexels, inTexels, outTexels); break;
	case CTextureFormat::TF_R8G8B8A8_SNORM: ConvertFromR8G8B8_SNORM(numTexels, inTexels, outTexels); break;
	case CTextureFormat::TF_R8G8B8A8_size_t:
	case CTextureFormat::TF_R8G8B8A8_INT: ConvertFromR8G8B8A8(numTexels, inTexels, outTexels); break;

	default:
		assert( false && ieS("Format unsupported") );
		break;
	}
}
//----------------------------------------------------------------------------------------------------------------------------
inline void ConvertTo(CTextureFormat::Type tFormat, size_t  numTexels, const Colour* inTexels, uint8_t* outTexels)
{
	switch (tFormat)
	{
	case CTextureFormat::TF_R32G32B32A32_FLOAT: ConvertToR32G32B32A32_FLOAT(numTexels, inTexels, outTexels); break;
	case CTextureFormat::TF_R32G32B32A32_size_t: // Unit and int the same
	case CTextureFormat::TF_R32G32B32A32_INT: ConvertToR32G32B32A32(numTexels, inTexels, outTexels); break;

	case CTextureFormat::TF_R32G32B32_FLOAT: ConvertToR32G32B32_FLOAT(numTexels, inTexels, outTexels); break;
	case CTextureFormat::TF_R32G32B32_size_t: // Unit and int the same
	case CTextureFormat::TF_R32G32B32_INT: ConvertToR32G32B32(numTexels, inTexels, outTexels); break;

	case CTextureFormat::TF_R32G32_FLOAT: ConvertToR32G32_FLOAT(numTexels, inTexels, outTexels); break;
	case CTextureFormat::TF_R32G32_size_t: // Unit and int the same
	case CTextureFormat::TF_R32G32_INT:	ConvertToR32G32(numTexels, inTexels, outTexels); break;

	case CTextureFormat::TF_R8G8B8A8_UNORM: break;
	case CTextureFormat::TF_R8G8B8A8_SNORM: break;
	case CTextureFormat::TF_R8G8B8A8_size_t: break;
	case CTextureFormat::TF_R8G8B8A8_INT: break;

	default:
		assert( false && ieS("Format unsupported") );
		break;
	}
}

#endif