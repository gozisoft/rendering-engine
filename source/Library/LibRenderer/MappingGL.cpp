#include "librendererfwd.h"
#include "MappingGL.h"
#include <stdexcept>

using namespace engine;

GLenum ApiToGL::BufferUsage(CResourceAccess::Type usage)
{
    switch (usage)
    {
        case CResourceAccess::RA_GPU:
            return GL_STATIC_DRAW;       // The CPU updates the resource less than once per frame
        case CResourceAccess::RA_STATIC:
            return GL_STATIC_DRAW; // The CPU does not update the resource
        case CResourceAccess::RA_DYNAMIC:
            return GL_DYNAMIC_DRAW;  // The CPU updates the resource more than once per frame
        case CResourceAccess::RA_STAGING:
            return GL_STREAM_DRAW;  // The CPU needs to read the resource
        default:
            throw std::runtime_error("Unknown buffer usage [ApiToGL::BufferUsage]");
    };
}

GLenum ApiToGL::BufferMapMode(CResourceLock::Type lock)
{
    switch (lock)
    {
        case CResourceLock::RL_READ_ONLY:
            return GL_READ_ONLY;
        case CResourceLock::RL_WRITE_ONLY:
            return GL_WRITE_ONLY;
        case CResourceLock::RL_READ_WRITE:
            return GL_READ_WRITE;
        case CResourceLock::RL_DISCARD:
            return GL_READ_WRITE;
        case CResourceLock::RL_NO_OVERWRITE:
            return GL_READ_WRITE;
        default:
            throw std::runtime_error("Unknown lock mode [ApiToGL::BufferMapMode]");
    };
}

GLenum ApiToGL::BufferBindFlags(size_t bufferType)
{
    switch (bufferType)
    {
        case CResourceUse::RU_VERTEX:
            return GL_ARRAY_BUFFER;
        case CResourceUse::RU_INDEX:
            return GL_ELEMENT_ARRAY_BUFFER;
        case CResourceUse::RU_CONSTANT:
            return GL_UNIFORM_BUFFER_BINDING;
        default:
            throw std::runtime_error("Unknown buffer type or combination [ApiToGL::BufferBindFlags]");
    };
}

GLenum ApiToGL::TextureFormat(CTextureFormat::Type tFormat)
{
    switch (tFormat)
    {
        case CTextureFormat::TF_NONE:
            return GL_NONE;

        case CTextureFormat::TF_R32G32B32A32_TYPELESS:
            return GL_RGBA32UI;
        case CTextureFormat::TF_R32G32B32A32_FLOAT:
            return GL_RGBA32F;
        case CTextureFormat::TF_R32G32B32A32_UINT:
            return GL_RGBA32UI;
        case CTextureFormat::TF_R32G32B32A32_INT:
            return GL_RGBA32I;

        case CTextureFormat::TF_R32G32B32_TYPELESS:
            return GL_RGB32UI;
        case CTextureFormat::TF_R32G32B32_FLOAT:
            return GL_RGB32F;
        case CTextureFormat::TF_R32G32B32_UINT:
            return GL_RGB32UI;
        case CTextureFormat::TF_R32G32B32_INT:
            return GL_RGB32I;

        case CTextureFormat::TF_R32G32_TYPELESS:
            return GL_RG32UI;
        case CTextureFormat::TF_R32G32_FLOAT:
            return GL_RG32F;
        case CTextureFormat::TF_R32G32_UINT:
            return GL_RG32UI;
        case CTextureFormat::TF_R32G32_INT:
            return GL_RG32I;

        case CTextureFormat::TF_R32_TYPELESS:
            return GL_R32UI;
        case CTextureFormat::TF_R32_FLOAT:
            return GL_R32F;

        case CTextureFormat::TF_R8G8B8A8_TYPELESS:
            return GL_RGBA8UI;
        case CTextureFormat::TF_R8G8B8A8_UINT:
            return GL_RGBA8UI;
        case CTextureFormat::TF_R8G8B8A8_INT:
            return GL_RGBA8I;
        case CTextureFormat::TF_R8G8B8A8_UNORM:
            return GL_RGBA8;
        case CTextureFormat::TF_R8G8B8A8_SNORM:
            return GL_RGBA8_SNORM;

        case CTextureFormat::TF_D32_FLOAT_S8X24_UINT:
            return GL_DEPTH32F_STENCIL8;
        case CTextureFormat::TF_D24_UNORM_S8_FLOAT:
            return GL_DEPTH24_STENCIL8;
        case CTextureFormat::TF_D32_FLOAT:
            return GL_DEPTH_COMPONENT32F;

        default:
            throw std::runtime_error("Unknown texture format [ApiToGL::TextureFormat]");
    }
}

size_t GlToApi::BufferBindFlags(GLenum type)
{
    switch (type)
    {
        case GL_ARRAY_BUFFER:
            return CResourceUse::RU_VERTEX;
        case GL_ELEMENT_ARRAY_BUFFER:
            return CResourceUse::RU_INDEX;
        case GL_UNIFORM_BUFFER_BINDING :
            return CResourceUse::RU_CONSTANT;
        default:
            throw std::runtime_error("Unknown buffer type or combination [GlToApi::BufferBindFlags]");
    };
}



//GLenum GLPrimitiveType(Visual::PrimitiveType primitiveType)
//{
//	// PT_NONE (not used)
//	// PT_POLYPOINT
//	// PT_POLYSEGMENTS_DISJOINT
//	// PT_POLYSEGMENTS_CONTIGUOUS
//	// PT_TRIANGLES (not used)
//	// PT_TRIMESH
//	// PT_TRISTRIP
//	// PT_TRIFAN
//	switch (primitiveType)
//	{
//	case Visual::PT_POLYPOINTS: return GL_POINTS;
//	case Visual::PT_POLYLINE_CLOSED: return GL_LINES;
//	case Visual::PT_POLYLINE_OPEN: return GL_LINE_STRIP;
//	case Visual::PT_TRIMESH: return GL_TRIANGLES;
//	case Visual::PT_TRISTRIP: return GL_TRIANGLE_STRIP;
//	case Visual::PT_TRIFAN: return GL_TRIANGLE_FAN;
//	default:
//		assert( false && ieS("Unkown primitive type: GLPrimitiveType()") );
//		return 0;
//	};
//}
//
//GLenum GLVertexType(Buffer::VertexType vtype)
//{
//	switch (vtype)
//	{
//	case DT_BYTE1:
//	case DT_BYTE2:
//	case DT_BYTE4:
//		return GL_BYTE;
//	case DT_UBYTE1:
//	case DT_UBYTE2:
//	case DT_UBYTE4:
//		return GL_UNSIGNED_BYTE;
//	case DT_SHORT1:
//	case DT_SHORT2:
//	case DT_SHORT4:
//		return GL_SHORT;
//	case DT_USHORT1:
//	case DT_USHORT2:
//	case DT_USHORT4:
//		return GL_UNSIGNED_SHORT;
//	case DT_INT1:
//	case DT_INT2:
//	case DT_INT3:
//	case DT_INT4:
//		return GL_INT;
//	case DT_UINT1:
//	case DT_UINT2:
//	case DT_UINT3:
//	case DT_UINT4:
//		return GL_UNSIGNED_INT;
//	case DT_FLOAT1:
//	case DT_FLOAT2:
//	case DT_FLOAT3:
//	case DT_FLOAT4:
//		return GL_FLOAT;
//	default:
//		assert( false && ieS("Unkown vertex type: GLVertexType()") );
//		return 0;
//	};
//}
//
//GLenum GLIndexType(Buffer::IndexType itype)
//{
//	switch (itype)
//	{
//	case Buffer::IT_UINT: return GL_UNSIGNED_INT;
//	case Buffer::IT_USHORT: return GL_UNSIGNED_SHORT;
//	default:
//		assert( false && ieS("Unkown index type") );
//		return 0;
//	};
//}
//
//
//
//
//// States Mapping
//GLenum GLAlphaSrcBlend[CAlphaState::NUM_SRC_BLENDS] =
//{
//	GL_ZERO,                        // SB_ZERO
//	GL_ONE,                         // SB_ONE
//	GL_DST_COLOR,                   // SB_DST_COLOR
//	GL_ONE_MINUS_DST_COLOR,         // SB_ONE_MINUS_DST_COLOR
//	GL_SRC_ALPHA,                   // SB_SRC_ALPHA
//	GL_ONE_MINUS_SRC_ALPHA,         // SB_ONE_MINUS_SRC_ALPHA
//	GL_DST_ALPHA,                   // SB_DST_ALPHA
//	GL_ONE_MINUS_DST_ALPHA,         // SB_ONE_MINUS_DST_ALPHA
//	GL_SRC_ALPHA_SATURATE,          // SB_SRC_ALPHA_SATURATE
//	GL_CONSTANT_COLOR,              // SB_CONSTANT_COLOR
//	GL_ONE_MINUS_CONSTANT_COLOR,    // SB_ONE_MINUS_CONSTANT_COLOR
//	GL_CONSTANT_ALPHA,              // SB_CONSTANT_ALPHA
//	GL_ONE_MINUS_CONSTANT_ALPHA     // SB_ONE_MINUS_CONSTANT_ALPHA
//};
//
//GLenum GLAlphaDstBlend[CAlphaState::NUM_DST_BLENDS] =
//{
//	GL_ZERO,                        // DB_ZERO
//	GL_ONE,                         // DB_ONE
//	GL_SRC_COLOR,                   // DB_SRC_COLOR
//	GL_ONE_MINUS_SRC_COLOR,         // DB_ONE_MINUS_SRC_COLOR
//	GL_SRC_ALPHA,                   // DB_SRC_ALPHA
//	GL_ONE_MINUS_SRC_ALPHA,         // DB_ONE_MINUS_SRC_ALPHA
//	GL_DST_ALPHA,                   // DB_DST_ALPHA
//	GL_ONE_MINUS_DST_ALPHA,         // DB_ONE_MINUS_DST_ALPHA
//	GL_CONSTANT_COLOR,              // DB_CONSTANT_COLOR
//	GL_ONE_MINUS_CONSTANT_COLOR,    // DB_ONE_MINUS_CONSTANT_COLOR
//	GL_CONSTANT_ALPHA,              // DB_CONSTANT_ALPHA
//	GL_ONE_MINUS_CONSTANT_ALPHA     // DB_ONE_MINUS_CONSTANT_ALPHA
//};
//
//GLenum GLAlphaCompare[CAlphaState::NUM_CMP_MODES] =
//{
//	GL_NEVER,       // CM_NEVER
//	GL_LESS,        // CM_LESS
//	GL_EQUAL,       // CM_EQUAL
//	GL_LEQUAL,      // CM_LEQUAL
//	GL_GREATER,     // CM_GREATER
//	GL_NOTEQUAL,    // CM_NOTEQUAL
//	GL_GEQUAL,      // CM_GEQUAL
//	GL_ALWAYS       // CM_ALWAYS
//};
//
//GLenum GLDepthCompare[CDepthState::NUM_CMP_MODES] =
//{
//	GL_NEVER,       // CM_NEVER
//	GL_LESS,        // CM_LESS
//	GL_EQUAL,       // CM_EQUAL
//	GL_LEQUAL,      // CM_LEQUAL
//	GL_GREATER,     // CM_GREATER
//	GL_NOTEQUAL,    // CM_NOTEQUAL
//	GL_GEQUAL,      // CM_GEQUAL
//	GL_ALWAYS       // CM_ALWAYS
//};
//
//GLenum GLStencilCompare[CStencilState::NUM_CMP_MODES] =
//{
//	GL_NEVER,       // CM_NEVER
//	GL_LESS,        // CM_LESS
//	GL_EQUAL,       // CM_EQUAL
//	GL_LEQUAL,      // CM_LEQUAL
//	GL_GREATER,     // CM_GREATER
//	GL_NOTEQUAL,    // CM_NOTEQUAL
//	GL_GEQUAL,      // CM_GEQUAL
//	GL_ALWAYS       // CM_ALWAYS
//};
//
//GLenum GLStencilOperation[CStencilState::NUM_OP_MODES] =
//{
//	GL_KEEP,    // OT_KEEP
//	GL_ZERO,    // OT_ZERO
//	GL_REPLACE, // OT_REPLACE
//	GL_INCR,    // OT_INCREMENT
//	GL_DECR,    // OT_DECREMENT
//	GL_INVERT   // OT_INVERT
//};
//
//

//
//GLenum GLTextureFormat(ImageFormat tFormat)
//{
//	switch (tFormat)
//	{
//	case TF_NONE:
//		return 0;
//	case TF_R5G6B5:
//		return GL_BGR;
//	case TF_A1R5G5B5:
//		return GL_RGBA;
//	case TF_A4R4G4B4:
//		return GL_RGBA;
//	case TF_A8:
//		return GL_ALPHA;
//	case TF_L8:
//		return GL_LUMINANCE;
//	case TF_A8L8:
//		return GL_LUMINANCE_ALPHA;
//	case TF_R8G8B8:
//		return GL_BGR;
//	case TF_A8R8G8B8:
//		return GL_BGRA;
//	case TF_A8B8G8R8:
//		return GL_RGBA;
//	case TF_L16:
//		return GL_LUMINANCE;
//	case TF_G16R16:
//		return GL_RG;
//	case TF_A16B16G16R16:
//		return GL_RGBA;
//	case TF_R16F:
//		return GL_RED;
//	case TF_G16R16F:
//		return GL_RG;
//	case TF_A16B16G16R16F:
//		return GL_RGBA;
//	case TF_R32F:
//		return GL_RED;
//	case TF_G32R32F:
//		return GL_RG;
//	case TF_A32B32G32R32F:
//		return GL_RGBA;
//	case TF_DXT1:
//		return GL_COMPRESSED_RGRA_S3TC_DXT1_EXT;
//	case TF_DXT3:
//		return GL_COMPRESSED_RGRA_S3TC_DXT3_EXT;
//	case TF_DXT5:
//		return GL_COMPRESSED_RGRA_S3TC_DXT5_EXT;
//	case TF_D24S8:
//		return GL_DEPTH_STENCIL_EXT;
//	default:
//		assert( false && ieS("Unknown texture format: GLTextureFormat()") );
//		return 0;
//	}
//}
//
//// The sampler type for interpreting the texture assigned to the sampler.
//inline GLenum GLTextureTarget(CTexture::TextureType textureType)
//{
//	switch (textureType)
//	{
//	case CTexture::TT_NONE:
//		return 0;
//	case CTexture::TT_1D:
//		return GL_TEXTURE_1D;
//	case CTexture::TT_2D:
//		return GL_TEXTURE_2D;
//	case CTexture::TT_3D:
//		return GL_TEXTURE_3D;
//	case CTexture::TT_CUBE:
//		return GL_TEXTURE_CUBE_MAP;
//	default:
//		assert( false && ieS("Unknown texture type: GLTextureTarget()") );
//		return 0;
//	}
//}
//
//inline GLenum GLTextureType(ImageFormat tFormat)
//{
//	switch (tFormat)
//	{
//	case TF_NONE:
//		return 0;
//	case TF_R5G6B5:
//		return GL_UNSIGNED_SHORT_5_6_5_REV;
//	case TF_A1R5G5B5:
//		return GL_UNSIGNED_SHORT_1_5_5_5_REV;
//	case TF_A4R4G4B4:
//		return GL_UNSIGNED_SHORT_4_4_4_4_REV;
//	case TF_A8:
//		return GL_UNSIGNED_BYTE;
//	case TF_L8:
//		return GL_UNSIGNED_BYTE;
//	case TF_A8L8:
//		return GL_UNSIGNED_BYTE;
//	case TF_R8G8B8:
//		return GL_UNSIGNED_BYTE;
//	case TF_A8R8G8B8:
//		return GL_UNSIGNED_BYTE;
//	case TF_A8B8G8R8:
//		return GL_UNSIGNED_BYTE;
//	case TF_L16:
//		return GL_UNSIGNED_SHORT;
//	case TF_G16R16:
//		return GL_UNSIGNED_SHORT;
//	case TF_A16B16G16R16:
//		return GL_UNSIGNED_SHORT;
//	case TF_R16F:
//		return GL_HALF_FLOAT_ARB;
//	case TF_G16R16F:
//		return GL_HALF_FLOAT_ARB;
//	case TF_A16B16G16R16F:
//		return GL_HALF_FLOAT_ARB;
//	case TF_R32F:
//		return GL_FLOAT;
//	case TF_G32R32F:
//		return GL_FLOAT;
//	case TF_A32B32G32R32F:
//		return GL_FLOAT;
//	case TF_DXT1:
//		return GL_NONE;
//	case TF_DXT3:
//		return GL_NONE;
//	case TF_DXT5:
//		return GL_NONE;
//	case TF_D24S8:
//		return GL_UNSIGNED_INT_24_8_EXT;
//	default:
//		assert( false && ieS("Unknown texture format: GLTextureType()") );
//		return 0;
//	}
//}

