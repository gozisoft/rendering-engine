#ifndef __CSTENCIL_STATE_H__
#define __CSTENCIL_STATE_H__

#include "Core.h"

_ENGINE_BEGIN

class CStencilState
{
public:
	// Stenticl comparision.
	enum CompareMode
	{
		CM_NEVER,
		CM_LESS,
		CM_EQUAL,
		CM_LEQUAL,
		CM_GREATER,
		CM_NOTEQUAL,
		CM_GEQUAL,
		CM_ALWAYS,
		NUM_CMP_MODES
	};

	enum OperationMode
	{
		OM_KEEP,
		OM_ZERO,
		OM_REPLACE,
		OM_INCREMENT,
		OM_DECREMENT,
		OM_INVERT,
		NUM_OP_MODES
	};

public:
	CStencilState();
	CStencilState(bool enable,
		CompareMode compare,
		uint32_t ref,
		uint32_t mask,
		uint32_t writeMask,
		OperationMode onFail,
		OperationMode onZFail,
		OperationMode onZPass);

	~CStencilState();

	bool m_enabled;            // default: false
	CompareMode m_compare;     // default: CM_NEVER
	uint32_t m_reference;	   // default: 0
	uint32_t m_mask;		   // default: UINT_MAX (0xFFFFFFFF)
	uint32_t m_writeMask;	   // default: UINT_MAX (0xFFFFFFFF)
	OperationMode m_onFail;    // default: OT_KEEP
	OperationMode m_onZFail;   // default: OT_KEEP
	OperationMode m_onZPass;   // default: OT_KEEP

};

_ENGINE_END // namespace Engine

#endif