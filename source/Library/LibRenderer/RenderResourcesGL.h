#ifndef __CRENDER_RESOURCES_GL_H__
#define __CRENDER_RESOURCES_GL_H__

#include "OpenGLFwd.h"
#include "IRenderResources.h"

_ENGINE_BEGIN

class CRenderResourcesGL : public IRenderResources
{
private:
	friend void Video::CreateDeferredGL(const IRenderResourcesPtr&, IRenderResourcesPtr&);

	typedef std::map< BufferPtr, CBufferGLPtr > BufferMap;
	typedef std::map< CVertexFormatPtr, CVertexFormatGLPtr > VertexFormatMap;
	typedef std::map< CTexture1DPtr, CTexture1DGLPtr > Texture1DMap;
	typedef std::map< CTexture2DPtr, CTexture2DGLPtr > Texture2DMap;
	typedef std::function<void ()> command;
	typedef std::list<command> commandList;

	// Resources
	BufferMap m_buffers;
	VertexFormatMap	m_vertexFormats;
	Texture1DMap m_1DTextures;
	Texture2DMap m_2DTextures;

public:
	CRenderResourcesGL();
	~CRenderResourcesGL();

	// Function for executing the command buffer
	void ExecuteCommands(commandList& pRC);

	// Vertex shader
	void Bind(const CShaderPtr& pShader);
	void UnBind(const CShaderPtr& pVShader);

	// General buffer
	void Enable(const BufferPtr& pBuffer);
	void Disable(const BufferPtr& pBuffer);
	void Bind(const BufferPtr& pBuffer);
	void UnBind(const BufferPtr& pBuffer);
	void* Map(const BufferPtr& pBuffer);
	void Unmap(const BufferPtr& pBuffer);

	// Vertex format
	void Enable(const CVertexFormatPtr& pVFormat);
	void Disable(const CVertexFormatPtr& pVFormat);
	void Bind(const CVertexFormatPtr& pVFormat);
	void UnBind(const CVertexFormatPtr& pVFormat);

/*	// Texture1D
	void Enable(CTexture1DPtr pTexture1D);
	void Disable(CTexture1DPtr pTexture1D);

	// Texture2D
	void Enable(CTexture2DPtr pTexture2D);
	void Disable(CTexture2DPtr pTexture2D); */

	// Render calls
	void Draw(const VertexBufferPtr& pVBuffer, Visual::PrimitiveType primitiveType);
	void DrawIndexd(const IndexBufferPtr& pIBuffer, Visual::PrimitiveType primitiveType);
	void DrawNode(const Visual* pNode);



};

_ENGINE_END

#endif