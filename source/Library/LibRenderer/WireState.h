#pragma once

#ifndef __CWIRE_STATE_H__
#define __CWIRE_STATE_H__

#include "Core.h"

_ENGINE_BEGIN

class CWireState
{
public:
	CWireState();
	~CWireState();

	bool m_enable; // default : false
};

_ENGINE_END // namespace Engine

#endif