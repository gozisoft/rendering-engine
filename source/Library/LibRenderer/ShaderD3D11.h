#pragma once

#ifndef __CSHADER_D3D11_H__
#define __CSHADER_D3D11_H__

#include "D3D11Fwd.h"
#include "IShader.h"

_ENGINE_BEGIN

// Shader VTables are used to eliminate branching in ApplyShaderBlock.
// The effect owns three D3DShaderVTables, one for PS, one for VS, and one for GS.
struct SD3DShaderVTable
{
	void ( __stdcall ID3D11DeviceContext::*pSetShader)(ID3D11DeviceChildPtr pShader, ID3D11ClassInstance*const* ppClassInstances, size_t NumClassInstances);
	void ( __stdcall ID3D11DeviceContext::*pSetConstantBuffers)(size_t StartConstantSlot, size_t NumBuffers, ID3D11Buffer *const *pBuffers);
	void ( __stdcall ID3D11DeviceContext::*pSetSamplers)(size_t Offset, size_t NumSamplers, ID3D11SamplerState*const* pSamplers);
	void ( __stdcall ID3D11DeviceContext::*pSetShaderResources)(size_t Offset, size_t NumResources, ID3D11ShaderResourceView *const *pResources);
	HRESULT ( __stdcall ID3D11Device::*pCreateShader)(const void *pShaderBlob, SIZE_T ShaderBlobSize, ID3D11ClassLinkage* pClassLinkage, ID3D11DeviceChild **ppShader);
};

class CShaderD3D11 : public IShader
{
public:
	// Basic constructor
	CShaderD3D11(ID3D11DeviceChildPtr pShader, SD3DShaderVTable* vtable);

	// Releases the device child
	~CShaderD3D11();

	// Enable shader constants
	void enableConstantBuffers(const IPipeline* pipeline, IBuffer* const* cbuffers, 
		size_t numbuffers, size_t startslot) override;

	// Enable shader resources
	void enableShaderResource(const IPipeline* pipeline, IShaderResource* const* resources,
		size_t numresources, size_t startslot) override;

	// Enable the shader itself
	void enableShader(const IPipeline* pipeline) override;

	// Function to get the shader type
	CShaderType::Type GetShaderType() const;

	// Access the underlying d3d type
	ID3D11DeviceChildPtr Get() const;

	// Access to the vtable that allows state settings without
	// pointless type stricting
	SD3DShaderVTable* GetTable() const;

	// Static shader vtables
	static SD3DShaderVTable s_vertexShaderTable;
	static SD3DShaderVTable s_geometryShaderTable;
	static SD3DShaderVTable s_pixelShaderTable;

private:
	ID3D11DeviceChildPtr m_shader;
	SD3DShaderVTable* m_vtable;
};

#include "ShaderD3D11.inl"

_ENGINE_END

#endif