#pragma once

#ifndef RENDERER_D3D11_H__
#define RENDERER_D3D11_H__

#include "D3D11Fwd.h"
#include "IRenderer.h"
#include "ShaderD3D11.h"
#include "ResourceD3D11.h"
#include "InputFormatD3D11.h"
#include "RasterizerStateD3D11.h"
#include <vector>

_ENGINE_BEGIN


class RendererD3D11: public IRenderer
{
public:
    // Constructor to create a rendering device, user manually
    // has to get the pipeline object.
    RendererD3D11();

    // Device will be inherited
    ~RendererD3D11();

    // Determine the type of the render used [RT_DIRECT3D11]
    //-------------------------------------------------------------------------------------------------------------------
    CRenderType::Type GetRenderType() const override
    {
        return CRenderType::RT_DIRECT3D11;
    }

    // Access the rendering pipeline.
    //-------------------------------------------------------------------------------------------------------------------
    shared_ptr<IPipeline> getPipeline() const override;

    // Access the rendering pipeline.
    shared_ptr<PipelineD3D11> _getPipeline() const
    {
        return m_pipeline;
    }

    // https://anteru.net/2011/12/27/1830/
    unique_ptr<RasterizerStateD3D11> createRasterState(
        std::shared_ptr<RasterizerState> const &state);

    ID3D11RasterizerStatePtr _createRasterState(
        std::shared_ptr<RasterizerState> const &state);

    //
    // Swap chain and back buffer access
    //

    // Function to set multiple swap chain buffers
    IDXGISwapChainPtr createSwapChain(size_t numBackBuffers, void *hwnd,
                                      CTextureFormat::Type format, CBufferSwap::Type swapType);

    // Obtain a back buffer, which can then be used to create a rendertarget
    unique_ptr<CTexture2DD3D11> getBackBuffer(IDXGISwapChainPtr swapChain, size_t bufferIndex = 0);

    //
    // Resource creation
    //

    // Function to create a shader
    unique_ptr<IShader> createShader(CShaderType::Type type, shader_handle handle) override;

    // Function to create an input layout
    unique_ptr<IInputFormat> createInputFormat(const CInputElement *elements, size_t numElements,
                                               shader_handle handle) override;

    /**
     * Method:    createBuffer
     * FullName:  Engine::RendererD3D11::createBuffer
     * Access:    public
     * Returns:   Engine::IBufferPtr
     * Qualifier:
     * Parameter: size_t bindFlags the CResourceUse type (RU_VERTEX, RU_INDEX, etc)
     * Parameter: CResourceAccess::Type usage the CResourceAccess type (RA_GPU, RA_STATIC, etc)
     * Parameter: size_t sizeInBytes size of buffer
     * Parameter: const uint8_t * data pointer to data
     **/
    unique_ptr<IBuffer> createBuffer(size_t bindFlags, CResourceAccess::Type usage,
                                     size_t sizeInBytes, const uint8_t *data) override;

    //
    // Structured [Texture1D, Texture2D, Texture3D] buffer creation
    //

    // Texture1D creation.
    unique_ptr<ITexture1D> createTexture(size_t bindFlags, CResourceAccess::Type usage,
                                         CTextureFormat::Type tformat, const SMipMap1D *mips, size_t numLevels,
                                         const uint8_t *data, size_t arraySize) override;

    // Texture2D creation.
    unique_ptr<ITexture2D> createTexture(size_t bindFlags, CResourceAccess::Type usage,
                                         CTextureFormat::Type tformat, const SMipMap2D *mips, size_t numLevels,
                                         const uint8_t *data, size_t arraySize) override;

    //
    // Covariant methods!
    //

    // Buffer creation, index, vertex, constant
    unique_ptr<CBufferD3D11> _createBuffer(size_t bindFlags, CResourceAccess::Type usage,
                                           size_t sizeInBytes, const uint8_t *data);

    /// <summary>
    /// Texture 1D creation.
    /// </summary>
    /// <param name="bindFlags">Texture bind flag, depthstencil, rendertarget, etc.</param>
    /// <param name="usage">Texture resource usage by GPU and CPU</param>
    /// <param name="tformat">Memory format of the texture.</param>
    /// <param name="mips">Mips describe the layout of texture layers.</param>
    /// <param name="numLevels">Number of mip levels. Set 0 for auto generation.</param>
    /// <param name="data">flags used to bind this texture. changes.</param>
    /// <param name="arraySize">flags used to bind this texture. changes.</param>
    unique_ptr<CTexture1DD3D11> _createTexture(size_t bindFlags, CResourceAccess::Type usage,
                                               CTextureFormat::Type tformat, const SMipMap1D *mips, size_t numLevels,
                                               const uint8_t *data, size_t arraySize);

    unique_ptr<CTexture1DD3D11> _createTexture(size_t bindFlags,
                                               CResourceAccess::Type usage,
                                               CTextureFormat::Type tformat,
                                               const SMipMap1D *mips,
                                               size_t numLevels,
                                               const uint8_t *data)
    {
        return _createTexture(bindFlags, usage, tformat, mips, numLevels, data, 1);
    }

    /// <summary>
    /// Texture 2D creation.
    /// </summary>
    /// <param name="bindFlags">Texture bind flag, depthstencil, rendertarget, etc.</param>
    /// <param name="usage">Texture resource usage by GPU and CPU</param>
    /// <param name="tformat">Memory format of the texture.</param>
    /// <param name="mips">Mips describe the layout of texture layers.</param>
    /// <param name="numLevels">Number of mip levels. Set 0 for auto generation.</param>
    /// <param name="data">flags used to bind this texture. changes.</param>
    /// <param name="arraySize">flags used to bind this texture. changes.</param>
    unique_ptr<CTexture2DD3D11> _createTexture(size_t bindFlags, CResourceAccess::Type usage,
                                               CTextureFormat::Type tformat, const SMipMap2D *mips, size_t numLevels,
                                               const uint8_t *data, size_t numTextures);

    unique_ptr<CTexture2DD3D11> _createTexture(size_t bindFlags,
                                               CResourceAccess::Type usage,
                                               CTextureFormat::Type tformat,
                                               const SMipMap2D *mips,
                                               size_t numLevels,
                                               const uint8_t *data)
    {
        return _createTexture(bindFlags, usage, tformat, mips, numLevels, data, 1);
    }

private:
    typedef std::vector<IDXGISwapChainPtr> SwapChains;

    void enableDebug();

    //
    // Internal builders
    //

    // Create a shader using a v table (saves code).
    ID3D11DeviceChildPtr create_shader(SD3DShaderVTable *table, shader_handle handle);

    // Function to create an input layout
    ID3D11InputLayoutPtr create_input_format(const CInputElement *elements, size_t numElements,
                                             shader_handle handle);

    ID3D11BufferPtr create_buffer(size_t bindFlags, CResourceAccess::Type usage, size_t sizeInBytes,
                                  const uint8_t *data);

    ID3D11Texture1DPtr create_texture(size_t bindFlags, CResourceAccess::Type usage,
                                      CTextureFormat::Type tformat, const SMipMap1D *mips, size_t numLevels,
                                      const uint8_t *data, size_t arraySize);

    ID3D11Texture2DPtr create_texture(size_t bindFlags, CResourceAccess::Type usage,
                                      CTextureFormat::Type tformat, const SMipMap2D *mips, size_t numLevels,
                                      const uint8_t *data, size_t arraySize);

    //
    // Data members
    //

    // An array of swap
    SwapChains m_swapChains;

    // Instance of the D3D11 device
    ID3D11DevicePtr m_device;

    // Debug layer reporter
    ID3D11DebugPtr m_debug;

    // Owns an instance of the pipeline
    shared_ptr<PipelineD3D11> m_pipeline;
};

#include "RendererD3D11.inl"

_ENGINE_END


#endif


//// Struct used for creating a buffer via interface
//struct SBufferInfo
//{
//	size_t numVertices;
//	size_t stride;
//	size_t sizeInBytes;
//	IBuffer::BufferUse usage;	
//	const uint8_t* data;
//};
//
//// Struct used for creating a buffer via interface
//struct SIndexBufferInfo
//{
//	size_t numIndices;
//	size_t stride;
//	IIndexBuffer::BufferUse usage;	
//	const uint8_t* data;
//};
//
//// Struct used for creating a buffer via interface
//struct SConstantBufferInfo
//{
//	size_t sizeInBytes;
//	IConstantBuffer::BufferUse usage;	
//	const uint8_t* data;
//};
//
//// Structure used to create textures via interface
//struct STexture1DInfo
//{
//	ITexture::TextureFormat tformat;
//	ITexture::BufferUse usage;
//	size_t imageSize;
//	size_t width;
//	size_t numLevels;
//	const uint8_t* pData;
//};
//
//// Structure used to create textures via interface
//struct STexture2DInfo
//{
//	ITexture::TextureFormat tformat;
//	ITexture::BufferUse usage;
//	size_t imageSize;
//	size_t width;
//	size_t height;
//	size_t numLevels;
//	const uint8_t* pData;
//};


//class CCreateResourceEvent : public IEventData
//{
//public:
//	explicit CCreateResourceEvent(const CBuffer& buffer) : m_bufferData(buffer)
//	{
//
//	}
//
//	~CCreateResourceEvent() {
//
//	}
//
//	void Serialise(std::ostream &out) const	{
//
//	}
//
//	const EventType& GetEventType() const	{
//		return sk_EventType;
//	}
//
//	static const EventType sk_EventType;
//
//protected:
//	CBuffer m_bufferData;
//};
//
//class CCreateInputLayoutEvent : public IEventData
//{
//public:
//
//	const EventType& GetEventType() const	{
//		return sk_EventType;
//	}
//
//	static const EventType sk_EventType;
//
//protected:
//
//
//};
//
//class IDeviceController : public IEventListener
//{
//public:
//	virtual ~IDeviceController() {}
//
//
//};


//class CRenderDevice
//{
//public:
//	// Constructor to create a rendering device
//	CRenderDevice();
//
//	// Device will be inherited
//	virtual ~CRenderDevice();
//
//	// Creation is enforced to the device
//	CShaderPtr Create(const CShaderData& shaderData, CShader::ShaderVersion version);
//
//	// Specifies the input layout for a vertex shader (or the output layout)
//	CVertexFormatPtr Create(CVertexElement** elements, size_t numElements, size_t totalStride);
//
//	// Buffer creation (vertex, index, constant)
//	CBufferPtr Create(const SBufferInfo& bufferInfo);
//
//	// Texture 1D creation
//	CTexture1DPtr Create(const STexture1DInfo& tex1DInfo);
//
//	// Texture 2D creation
//	CTexture2DPtr Create(const STexture2DInfo& tex1DInfo);
//
//private:
//	ID3D11DevicePtr m_pDevice;
//
//};

