#ifndef __IINPUT_FORMAT_INL__
#define __IINPUT_FORMAT_INL__

#include "IInputFormat.h"

_ENGINE_BEGIN

inline size_t CInputElement::GetSize() const
{
	return CDataType::SizeOfType(m_vertexType) * m_vtypeChannels;
}

inline size_t CInputElement::GetStreamIndex() const
{
	return m_streamIndex;
}

inline size_t CInputElement::GetOffset() const
{
	return m_offset;
}

inline CDataType::Type CInputElement::GetVertexType() const
{
	return m_vertexType;
}

inline const std::string& CInputElement::GetVertexSemantic() const
{
	return m_semantic;
}

inline size_t CInputElement::GetVertexChannels() const
{
	return m_vtypeChannels;
}

inline size_t CInputElement::GetSemanticIndex() const
{
	return m_semanticIndex;
}

_ENGINE_END

#endif



//inline void CInputFormat::SetEnabled(Buffer::VertexSemantic semantic, bool enable)
//{
//	assert(semantic >= Buffer::VS_POSITION && semantic <= Buffer::VS_PSIZE);
//    m_semanticFlags &= ~(1<<semantic); // Clearing a bit (must invert the bit string with the bitwise NOT operator (~), then AND it)
//    m_semanticFlags |= ( (!!enable)<<semantic ); // Setting a bit
//}
//
//inline bool CInputFormat::IsEnabled(Buffer::VertexSemantic semantic) const
//{
//	assert(semantic >= Buffer::VS_POSITION && semantic <= Buffer::VS_PSIZE);
//	return !!( m_semanticFlags & (1<<semantic) );
//}

//inline void CInputFormat::SetEnabled(Buffer::VertexSemantic semantic, bool enable)
//{
//	assert(semantic >= Buffer::VS_POSITION && semantic <= Buffer::VS_PSIZE);
//	m_semanticFlags[semantic] = enable;
//}
//
//inline bool CInputFormat::IsEnabled(Buffer::VertexSemantic semantic) const
//{
//	assert(semantic >= Buffer::VS_POSITION && semantic <= Buffer::VS_PSIZE);
//	return m_semanticFlags[semantic];
//}


//inline CInputElementPtr CInputFormat::GetElement(size_t index) const
//{
//	//sequenced_index.
//	typedef ElementContainer::index<linear>::type element_by_linear;
//	const element_by_linear& index_type = m_elements.get<linear>();
//
//	size_t size = index_type.size();
//	if ( index < size )
//	{
//		auto itor = index_type.begin();
//		for (size_t i = 0; i < index; ++i)
//			++itor;
//
//		const CInputElementPtr& pElement = (*itor);		
//		return pElement;
//	}
//	else
//	{
//		assert( false && ieS("Invalid index called for GetElement") );
//		return CInputElementPtr();
//	}
//}
