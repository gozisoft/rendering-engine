#ifndef METAL_FORWARD_H
#define METAL_FORWARD_H

#import "Core.h"
#import <Metal/Metal.h>

_ENGINE_BEGIN

// OpenGL
class RendererMetal;
class ResourceMetal;
class BufferMetal;
class VertexFormatMetal;
class Texture1DMetal;
class Texture2DMetal;
class ShaderResourceMetal;
class RenderTargetMetal;

_ENGINE_END

#endif