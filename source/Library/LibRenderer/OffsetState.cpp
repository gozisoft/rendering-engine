#include "librendererafx.h"
#include "OffsetState.h"

using namespace engine;

COffsetState::COffsetState()
	: 
m_fillEnabled(false),
m_lineEnabled(false),
m_pointEnabled(false),
m_offsetScale(0),
m_offsetBias(0)
{

}

COffsetState::~COffsetState()
{

}