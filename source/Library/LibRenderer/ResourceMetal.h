#ifndef RESOURCE_METAL_H
#define RESOURCE_METAL_H

#import "MetalFwd.h"
#import "MappingMetal.h"
#import "IResource.h"
#import "IView.h"

_ENGINE_BEGIN

class BufferMetal: public IBuffer
{
public:
    BufferMetal(id<MTLBuffer> buffer, size_t bindFlags);

    ~BufferMetal() override;

    std::unique_ptr<IShaderResource> createSRView() override;

    std::unique_ptr<IRenderTarget> createRTView() override;

    std::unique_ptr<IShaderResource> createSRView(CTextureFormat::Type tformat,
                                                          size_t elementOffset,
                                                          size_t elementWidth) override;

    void *map(IPipeline *pipeline, CResourceLock::Type lock, size_t &alignedSize) const override;

    void unMap(IPipeline *pipeline) const override;

    size_t size() const override;

    size_t bindFlags() const override;

    CResourceAccess::Type usage() const override;

    CResourceType::Type type() const override;

    id<MTLBuffer> get();

private:
    id<MTLBuffer> m_buffer;
    size_t m_bindFlags;
};

inline size_t BufferMetal::size() const
{
    return static_cast<size_t>(m_buffer.length);
}

inline size_t BufferMetal::bindFlags() const
{
    return m_bindFlags;
}

inline CResourceAccess::Type BufferMetal::usage() const
{
    return MetalToApi::BufferUsage(m_buffer.cpuCacheMode);
}

inline CResourceType::Type BufferMetal::type() const
{
    return CResourceType::RT_BUFFER;
}

inline id<MTLBuffer> BufferMetal::get()
{
    return m_buffer;
}


#include "ResourceMetal.inl"

_ENGINE_END

#endif
