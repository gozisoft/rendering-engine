#include "librendererfwd.h"
#include "RenderResourcesGL.h"

// opengl
#include "RendererGL.h"
#include "RenderDataGL.h"
#include "BufferGL.h"
#include "VertexFormatGL.h"
#include "Texture1DGL.h"
#include "Texture2DGL.h"
#include "MappingGL.h"

// Generic
#include "RenderCommand.h"

using namespace engine;

CRenderResourcesGL::CRenderResourcesGL()
{

}

CRenderResourcesGL::~CRenderResourcesGL()
{

}

void CRenderResourcesGL::ExecuteCommands(commandList& clist)
{
	// Process the enable function (FIFO - first in first our)
	std::for_each(clist.begin(), clist.end(), [] (command& cmd) 
	{
		cmd(); // execute the command
	} );
}
// -------------------------------------------------------------------------------------------
// VertexShader
// -------------------------------------------------------------------------------------------
void CRenderResourcesGL::Bind(const CShaderPtr& pShader)
{


}

void CRenderResourcesGL::UnBind(const CShaderPtr& pShader)
{


}
// -------------------------------------------------------------------------------------------
// General Buffer
// -------------------------------------------------------------------------------------------
void CRenderResourcesGL::Enable(const BufferPtr& pBuffer)
{
	// search to find if buffer is active
	auto itor = m_buffers.find( pBuffer );
	CBufferGLPtr pBufferGL = CBufferGLPtr();
	if ( itor != m_buffers.end() )
	{
		pBufferGL = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pBufferGL = std::make_shared<CBufferGL>(pBuffer);
		m_buffers.insert( std::make_pair(pBuffer, pBufferGL) );
	}

	pBufferGL->Enable();
}

void CRenderResourcesGL::Disable(const BufferPtr& pBuffer)
{
	// search to find if buffer is active
	auto itor = m_buffers.find(pBuffer);
	if ( itor != m_buffers.end() )
	{
		CBufferGLPtr pVBufferGL = (*itor).second;
		pVBufferGL->Disable();
	}
}


void CRenderResourcesGL::Bind(const BufferPtr& pBuffer)
{
	if ( m_buffers.find(pBuffer) == m_buffers.end() )
	{
		// Create the API Buffer
		CBufferGLPtr pBufferGL = std::make_shared<CBufferGL>(pBuffer);

		// insert the buffers to the map
		m_buffers.insert( std::make_pair(pBuffer, pBufferGL) );
	}
}

void CRenderResourcesGL::UnBind(const BufferPtr& pBuffer)
{
	auto itor = m_buffers.find(pBuffer);
	if (itor != m_buffers.end())
	{
		m_buffers.erase(itor);
	}
}

void* CRenderResourcesGL::Map(const BufferPtr& pBuffer)
{
	return nullptr;
}

void CRenderResourcesGL::Unmap(const BufferPtr& pBuffer)
{


}
// -------------------------------------------------------------------------------------------
// VertexFormat
// -------------------------------------------------------------------------------------------
void CRenderResourcesGL::Enable(const CVertexFormatPtr& pVFormat)
{
	// search to find if vertex buffer is active
	auto itor = m_vertexFormats.find(pVFormat);
	CVertexFormatGLPtr pVFormatGL = CVertexFormatGLPtr();
	if ( itor != m_vertexFormats.end() )
	{
		pVFormatGL = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pVFormatGL = std::make_shared<CVertexFormatGL>(pVFormat);
		m_vertexFormats.insert( std::make_pair(pVFormat, pVFormatGL) );
	}

	pVFormatGL->Enable();
}

void CRenderResourcesGL::Disable(const CVertexFormatPtr& pVFormat)
{
	auto itor = m_vertexFormats.find( pVFormat );
	if ( itor != m_vertexFormats.end() )
	{
		CVertexFormatGLPtr pVFormatGL = (*itor).second;
		pVFormatGL->Disable();
	}
}

void CRenderResourcesGL::Bind(const CVertexFormatPtr& pVFormat)
{
	if ( m_vertexFormats.find(pVFormat) == m_vertexFormats.end() )
	{
		// Create the API Buffer
		CVertexFormatGLPtr pVFormatGL = std::make_shared<CVertexFormatGL>(pVFormat);

		// insert the buffers to the map
		m_vertexFormats.insert( std::make_pair(pVFormat, pVFormatGL) );
	}
}

void CRenderResourcesGL::UnBind(const CVertexFormatPtr& pVFormat)
{
	auto itor = m_vertexFormats.find(pVFormat);
	if (itor != m_vertexFormats.end())
	{
		m_vertexFormats.erase(itor);
	}
}
// -------------------------------------------------------------------------------------------
// Draw Functions
// -------------------------------------------------------------------------------------------
void CRenderResourcesGL::Draw(const VertexBufferPtr& pVBuffer, Visual::PrimitiveType primitiveType)
{
/*	
	GLenum mode = 0;
	GLsizei numVertices = 0;
	switch (primitiveType)
	{
	case CVisualNode::PT_POLYPOINTS: 
		mode = GL_POINTS;
		break;

	case CVisualNode::PT_POLYLINE_CLOSED:
		return GL_LINES;

	case CVisualNode::PT_POLYLINE_OPEN: 
		return GL_LINE_STRIP;

	case CVisualNode::PT_TRIMESH: 
		return GL_TRIANGLES;

	case CVisualNode::PT_TRISTRIP:
		return GL_TRIANGLE_STRIP;

	case CVisualNode::PT_TRIFAN:
		return GL_TRIANGLE_FAN;

	default:
		assert( false && ieS("Unkown primitive type: GLPrimitiveType()") );
		return 0;
	};
*/
}

void CRenderResourcesGL::DrawIndexd(const IndexBufferPtr& pIBuffer, Visual::PrimitiveType primitiveType)
{
	// check for invalid pointer
	assert( pIBuffer.get() );

	GLenum indexType = 0;
	GLvoid* pIndexData = nullptr;
	switch ( pIBuffer->GetType() )
	{
	case Buffer::IT_USHORT:
		indexType = GL_UNSIGNED_SHORT;
		pIndexData = reinterpret_cast<UShort*>( pIBuffer->GetData() ) + 0; // pIBuffer->GetIndexOffset();
		break;
	case Buffer::IT_size_t:
		indexType = GL_UNSIGNED_INT;
		pIndexData = reinterpret_cast<uint32_t*>( pIBuffer->GetData() ) + 0; // pIBuffer->GetIndexOffset();
		break;
	}

	// Determine the opengl rendering mode (triangles, triangestrip etc)
	GLenum mode = GLPrimitiveType(primitiveType);

	// Store either a glDrawRangeElements or glDrawElements function call.
	// This is dependant on the index buffer having its min and max values calculated.
	GLsizei numIndices = static_cast<GLsizei>( pIBuffer->GetNumElements() );
	if ( GLsize_t maxIndex = static_cast<GLsize_t>( pIBuffer->GetMaxRange() ) )
	{
		GLsize_t minIndex = static_cast<GLsize_t>( pIBuffer->GetMinRange() );
		glDrawRangeElements(mode, minIndex, maxIndex, numIndices, indexType, pIndexData); 
	}
	else
	{
		glDrawElements(mode, numIndices, indexType, pIndexData);
	}
}

void CRenderResourcesGL::DrawNode(const Visual* pNode)
{
	Visual::PrimitiveType type = pNode->GetType();
	const VertexBufferPtr& pVBuffer = pNode->GetVertexBuffer();
	const CVertexFormatPtr& pVFormat = pNode->GetVertexFormat();
	const IndexBufferPtr& pIBuffer = pNode->GetIndexBuffer();

	switch (type)
	{
	case Visual::PT_TRISTRIP:
	case Visual::PT_TRIMESH:
	case Visual::PT_TRIFAN:
		{
			GLenum indexType = 0;
			GLvoid* pIndexData = nullptr;
			switch ( pIBuffer->GetType() )
			{
			case Buffer::IT_USHORT:
				indexType = GL_UNSIGNED_SHORT;
				pIndexData = reinterpret_cast<UShort*>( pIBuffer->GetData() ) + 0; // pIBuffer->GetIndexOffset();
				break;
			case Buffer::IT_size_t:
				indexType = GL_UNSIGNED_INT;
				pIndexData = reinterpret_cast<uint32_t*>( pIBuffer->GetData() ) + 0; // pIBuffer->GetIndexOffset();
				break;
			}

			// Store either a glDrawRangeElements or glDrawElements function call.
			// This is dependant on the index buffer having its min and max values calculated.
			std::function<void()> zeroArg;
			GLsizei numIndices = static_cast<GLsizei>( pIBuffer->GetNumElements() );
			if ( GLsize_t maxIndex = static_cast<GLsize_t>( pIBuffer->GetMaxRange() ) )
			{
				GLsize_t minIndex = static_cast<GLsize_t>( pIBuffer->GetMinRange() );
				GLenum primitiveType = GLPrimitiveType(type);
				glDrawRangeElements(primitiveType, minIndex, maxIndex, numIndices, indexType, pIndexData); 
			}
			else
			{
				GLenum primitiveType = GLPrimitiveType(type);
				glDrawElements(primitiveType, numIndices, indexType, pIndexData);
			}
		}
		break;
	case Visual::PT_POLYLINE_OPEN:
		{
			const CPolyline* pPolyline = static_cast<const CPolyline*>(pNode);
			GLsizei numVerts = (GLsizei)pPolyline->GetNumLines();

			if (numVerts)
			{
				::glDrawArrays(GL_LINE_STRIP, 0, numVerts);
			}
		}
		break;
	case Visual::PT_POLYLINE_CLOSED:
		{
			const CPolyline* pPolyline = static_cast<const CPolyline*>(pNode);
			GLsizei numVerts = (GLsizei)pPolyline->GetNumLines();

			if (numVerts)
			{
				::glDrawArrays(GL_LINES, 0, numVerts);
			}
		}
		break;
	case Visual::PT_POLYPOINTS:
		{
			const CPolypoint* pPolypoint = static_cast<const CPolypoint*>(pNode);
			GLsizei numVerts = (GLsizei)pPolypoint->GetNumPoints();

			if (numVerts)
			{
				::glDrawArrays(GL_POINTS, 0, numVerts);
			}
		}
		break;
	default:
		assert( false && ieS("Invalid type\n") );
		break;

	} // switch
}

/*
void CRenderResourcesGL::ExecuteCommands(const CRenderCommandPtr& pRC)
{
// Process the enable function
std::for_each(pRC->enableFunctions.begin(), pRC->enableFunctions.end(), 
[] (CRenderCommand::ZeroArgFunctions::reference enableFunction) {
enableFunction();
} ); 

GLsizei numVerts = (GLsizei)pRC->numVerts;
GLsizei numIndices = (GLsizei)pRC->numIndices; 
GLenum mode = GLPrimitiveType(pRC->primitiveType);
GLenum indexType = GLIndexType(pRC->indexType);

// Process the commands for execution
glDrawElements(mode, numIndices, indexType, GL_BUFFER_MAP_OFFSET( ) );
// glDrawRangeElements(type, 0, numIndices, numIndices, indexType, 0);

// Process the disable functions
std::for_each(pRC->disableFunctions.begin(), pRC->disableFunctions.end(), 
[] (CRenderCommand::ZeroArgFunctions::reference disableFunction) {
disableFunction();
} ); 
}
*/

/*
// -------------------------------------------------------------------------------------------
// Texture 1D
// -------------------------------------------------------------------------------------------
void CRendererGL::Enable(CTexture1DPtr pTexture1D)
{
// search to find if vertex buffer is active
Texture1DMap::iterator itor = m_1DTextures.find(pTexture1D);
CTexture1DGLPtr pTexture1DGL = CTexture1DGLPtr();
if ( itor != m_1DTextures.end() )
{
pTexture1DGL = (*itor).second;
}
else // if not found, lazy create it
{
pTexture1DGL = CTexture1DGLPtr( new CTexture1DGL(pTexture1D) );
m_1DTextures[pTexture1D] = pTexture1DGL;
}

pTexture1DGL->Enable(0);
}

void CRendererGL::Disable(CTexture1DPtr pTexture1D)
{
Texture1DMap::iterator itor = m_1DTextures.find(pTexture1D);
if ( itor != m_1DTextures.end() )
{
CTexture1DGLPtr pTexture1DGL = (*itor).second;
pTexture1DGL->Disable(0);
}
}

// -------------------------------------------------------------------------------------------
// Texture 2D
// -------------------------------------------------------------------------------------------
void CRendererGL::Enable(CTexture2DPtr pTexture2D)
{
// search to find if vertex buffer is active
Texture2DMap::iterator itor = m_2DTextures.find(pTexture2D);
CTexture2DGLPtr pTexture2DGL = CTexture2DGLPtr();
if ( itor != m_2DTextures.end() )
{
pTexture2DGL = (*itor).second;
}
else // if not found, lazy create it
{
pTexture2DGL = CTexture2DGLPtr( new CTexture2DGL(pTexture2D) );
m_2DTextures[pTexture2D] = pTexture2DGL;
}

pTexture2DGL->Enable(0);
}

void CRendererGL::Disable(CTexture2DPtr pTexture2D)
{
Texture2DMap::iterator itor = m_2DTextures.find(pTexture2D);
if ( itor != m_2DTextures.end() )
{
CTexture2DGLPtr pTexture2DGL = (*itor).second;
pTexture2DGL->Disable(0);
}
}
*/

/*
void CRenderResourcesGL::ExecuteCommands(CRenderCommandPtr& pRC)
{
// Process the enable function
if (!pRC->vertexBuffers.empty())
{
std::for_each(pRC->vertexBuffers.begin(), pRC->vertexBuffers.end(), 
[] (const CRenderCommand::HANDLEType& handle) 
{
CBufferGLPtr& pVBufferGL = std::static_pointer_cast<CBufferGL>(handle);
pVBufferGL->Enable();
} );	
}

if (!pRC->vertexFormats.empty())
{
std::for_each(pRC->vertexFormats.begin(), pRC->vertexFormats.end(),
[] (const CRenderCommand::HANDLEType& handle) 
{
CVertexFormatGLPtr& pFormatGL = std::static_pointer_cast<CVertexFormatGL>(handle);
pFormatGL->Enable();
} );			
}

if (!pRC->indexBuffers.empty())
{
std::for_each(pRC->vertexFormats.begin(), pRC->vertexFormats.end(),
[] (const CRenderCommand::HANDLEType& handle) 
{
CBufferGLPtr& pIBufferGL = std::static_pointer_cast<CBufferGL>(handle);
pIBufferGL->Enable();
} );			
}

GLsizei numVerts = static_cast<GLsizei>(pRC->numVerts);
GLsizei numIndices = static_cast<GLsizei>(pRC->numIndices); 
GLenum mode = GLPrimitiveType(pRC->primitiveType);
GLenum indexType = GLIndexType(pRC->indexType);
GLvoid* pIndices = static_cast<GLvoid*>(pRC->pIndexData);

// Process the commands for execution
glDrawElements(mode, numIndices, indexType, pIndices);
// glDrawRangeElements(type, 0, numIndices, numIndices, indexType, 0);

// Process the disable functions
if (!pRC->vertexBuffers.empty())
{
std::for_each(pRC->vertexBuffers.begin(), pRC->vertexBuffers.end(), 
[] (const CRenderCommand::HANDLEType& handle) 
{
CBufferGLPtr& pVBufferGL = std::static_pointer_cast<CBufferGL>(handle);
pVBufferGL->Disable();
} );	
}

if (!pRC->vertexFormats.empty())
{
std::for_each(pRC->vertexFormats.begin(), pRC->vertexFormats.end(),
[] (const CRenderCommand::HANDLEType& handle) 
{
CVertexFormatGLPtr& pFormatGL = std::static_pointer_cast<CVertexFormatGL>(handle);
pFormatGL->Disable();
} );			
}

if (!pRC->indexBuffers.empty())
{
std::for_each(pRC->vertexFormats.begin(), pRC->vertexFormats.end(),
[] (const CRenderCommand::HANDLEType& handle) 
{
CBufferGLPtr& pIBufferGL = std::static_pointer_cast<CBufferGL>(handle);
pIBufferGL->Disable();
} );			
}
}
*/