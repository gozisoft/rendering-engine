#pragma once

#ifndef __CZBUFFER_STATE_H__
#define __CZBUFFER_STATE_H__

#include "RendererTypes.h"

_ENGINE_BEGIN

class CZBufferState
{
public:
	CZBufferState();
	~CZBufferState();

	bool				m_isEnabled;	// default: true
	bool				m_isWritable;	// default: true
	CCompareMode::Type	m_compare;		// default: CF_LEQUAL
};

_ENGINE_END // namespace Engine

#endif