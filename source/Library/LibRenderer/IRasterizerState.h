#pragma once
#ifndef IRASTERIZER_STATE_H
#define IRASTERIZER_STATE_H

#include "Core.h"

_ENGINE_BEGIN

class IRasterizerState {
public:
	virtual ~IRasterizerState() {}
};

_ENGINE_END

#endif