#ifndef CRENDERER_GL_H
#define CRENDERER_GL_H

#include "OpenGLFwd.h"
#include "IRenderer.h"
#include "Colour.h"

_ENGINE_BEGIN

class CRendererGL : public IRenderer {
public:
    CRendererGL();

    ~CRendererGL();

    // Determine the type of the render used
    CRenderType::Type GetRenderType() const override;

    // Get access to the rendering pipeline object
    shared_ptr<IPipeline> getPipeline() const override;

    //////////////////////////////////////////////////////////////////////////////////////
    // Swap chain and back buffer access

    //// Function to set multiple swap chain buffers
    //virtual void createSwapChain(size_t* index, size_t numBackBuffers, void* hwnd,
    //	CTextureFormat::Type format=CTextureFormat::TF_R8G8B8A8_UNORM,
    //	CBufferSwap::Type swapType=CBufferSwap::BS_DISCARD) = 0;

    //// Obtain a back buffer, which can then be used to create a rendertarget
    //virtual ITexture2D* GetBackBuffer(size_t swapChainIndex, size_t bufferIndex) = 0;

    //// Flip the back buffers
    //virtual void Flush(size_t swapChainIndex, size_t syncInterval) = 0;

    // Shader creation
    unique_ptr<IShader> createShader(CShaderType::Type type, shader_handle handle) override;

    // Input layout creation.
    unique_ptr<IInputFormat> createInputFormat(const CInputElement *elements, size_t numElements,
                                               shader_handle handle) override;

    //************************************
    // Method:    createBuffer
    // FullName:  Engine::IRenderer::createBuffer
    // Access:    virtual public
    // Returns:   Engine::IBufferPtr
    // Qualifier:
    // Parameter: size_t bindFlags Used to set the type of buffer [constant, vertex, index, etc.].
    // Parameter: CResourceAccess::Type usage GPU and CPU usage for buffer access.
    // Parameter: size_t sizeInBytes Size of buffer in bytes.
    // Parameter: const uint8_t * data data to be copied into the buffer on creation.
    //************************************
    unique_ptr<IBuffer> createBuffer(size_t bindFlags, CResourceAccess::Type usage,
                                     size_t sizeInBytes, const uint8_t *data) override;

    //
    // Structured [Texture1D, Texture2D, Texture3D] buffer creation
    //

    // Texture1D creation.
    unique_ptr<ITexture1D> createTexture(size_t bindFlags, CResourceAccess::Type usage,
                                         CTextureFormat::Type tformat, const SMipMap1D *mips, size_t numLevels,
                                         const uint8_t *data, size_t arraySize) override;

    // Texture2D creation.
    unique_ptr<ITexture2D> createTexture(size_t bindFlags, CResourceAccess::Type usage,
                                         CTextureFormat::Type tformat, const SMipMap2D *mips, size_t numLevels,
                                         const uint8_t *data, size_t arraySize) override;

    unique_ptr<CBufferGL> _createBuffer(size_t bindFlags, CResourceAccess::Type usage,
                                        size_t sizeInBytes, const uint8_t *data);

    unique_ptr<CTexture1DGL> _createTexture(size_t bindFlags, CResourceAccess::Type usage,
                                            CTextureFormat::Type tformat, const SMipMap1D *mips, size_t numLevels,
                                            const uint8_t *data, size_t arraySize);

    unique_ptr<CTexture1DGL> _createTexture(size_t bindFlags, CResourceAccess::Type usage,
                                            CTextureFormat::Type tformat, const SMipMap1D *mips, size_t numLevels,
                                            const uint8_t *data) {
        return _createTexture(bindFlags, usage, tformat, mips, numLevels, data, 1);
    }

    unique_ptr<CTexture2DGL> _createTexture(size_t bindFlags, CResourceAccess::Type usage,
                                            CTextureFormat::Type tformat, const SMipMap2D *mips, size_t numLevels,
                                            const uint8_t *data, size_t numTextures);

    unique_ptr<CTexture2DGL> _createTexture(size_t bindFlags, CResourceAccess::Type usage,
                                            CTextureFormat::Type tformat, const SMipMap2D *mips, size_t numLevels,
                                            const uint8_t *data) {
        return _createTexture(bindFlags, usage, tformat, mips, numLevels, data, 1);
    }
};


_ENGINE_END

#endif