if (NOT MSVC)
    message(FATAL_ERROR "Not a windows system")
endif ()

# Direct3D
set(RENDER_HEADER
        D3DFwd.h
        D3D11Fwd.h
        RendererD3D11.h
        ResourceD3D11.h)

set(RENDER_SOURCE
        D3DFwd.cpp
        RendererD3D11.cpp
        ResourceD3D11.cpp)