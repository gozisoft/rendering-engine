if (NOT APPLE)
    message(FATAL_ERROR "Not an XCode system")
endif ()

# OpenGL
set(PLATFORM_HEADER
        MetalFwd.h
        MappingMetal.h
        RendererMetal.h
        ResourceMetal.h)

set(PLATFORM_INLINE
        ResourceMetal.inl)

set(PLATFORM_SOURCE
        MappingMetal.mm
        RendererMetal.mm
        ResourceMetal.mm)