#pragma once

#ifndef __CDEPTH_STATE_H__
#define __CDEPTH_STATE_H__

#include "RendererTypes.h"

_ENGINE_BEGIN

class CDepthState
{
public:
	CDepthState();
	CDepthState(bool enable, bool writable, CCompareMode::Type compareMode);
	~CDepthState();

	bool m_enabled;				// default: true
	bool m_writable;            // default: true
	CCompareMode::Type m_compareMode;  // default: CM_LEQUAL
};

_ENGINE_END // namespace Engine

#endif