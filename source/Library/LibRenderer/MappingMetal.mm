//
// Created by Riad Gozim on 10/02/2016.
//

#include "librendererafx.h"
#include "MappingMetal.h"

using namespace engine;

MTLResourceOptions ApiToMetal::BufferUsage(engine::CResourceAccess::Type usage) {
    switch (usage) {
        case CResourceAccess::RA_GPU: // The CPU updates the resource less than once per frame
            return MTLResourceCPUCacheModeDefaultCache;
        case CResourceAccess::RA_STATIC: // The CPU does not update the resource
            return MTLResourceStorageModePrivate;
        case CResourceAccess::RA_DYNAMIC: // The CPU updates the resource more than once per frame
            return MTLResourceCPUCacheModeWriteCombined;
        case CResourceAccess::RA_STAGING: // The CPU needs to read the resource
            return MTLResourceStorageModeShared;
        default:
            throw std::runtime_error("Unknown buffer usage [ApiToGL::BufferUsage]");
    };
}

MTLPixelFormat ApiToMetal::TextureFormat(CTextureFormat::Type tFormat) {
    return MTLPixelFormat::MTLPixelFormatA8Unorm;
}

CResourceAccess::Type MetalToApi::BufferUsage(MTLResourceOptions options) {
    return engine::CResourceAccess::RA_NONE;
}
