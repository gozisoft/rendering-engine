#pragma once

#ifndef __IRESOURCE_H__
#define __IRESOURCE_H__

#include "librendererfwd.h"
#include "RendererTypes.h"
#include <vector>

_ENGINE_BEGIN

class IResource
{
public:
	// Destructor
	virtual ~IResource() { /**/ }

	// Function to create a resource view
	virtual std::unique_ptr<IShaderResource> createSRView() = 0;

	// Function to create a resource view
	virtual std::unique_ptr<IRenderTarget> createRTView() = 0;

	// The data total buffer's size in bytes
	virtual size_t size() const = 0;

	// Buffer type determines if its a vertex or index buffer
	virtual size_t bindFlags() const = 0;

	// Describes the buffers usage with relation to the hardware
	virtual CResourceAccess::Type usage() const = 0;

	// Determine the type of resource thats been created
	virtual CResourceType::Type type() const = 0;
};

class IBuffer : public IResource
{
public:
	// Typedef for data array
	typedef std::vector<uint8_t> byte_array_t;

	// Destructor
	virtual ~IBuffer() { /**/ }

	// Function to create a resource view
	virtual std::unique_ptr<IShaderResource> createSRView(CTextureFormat::Type tformat,
			size_t elementOffset, size_t elementWidth) = 0;

	// Open access to the data of a buffer
	virtual void* map(IPipeline *pipeline, CResourceLock::Type lock, size_t &alignedSize) const = 0;

	// Close access to the data of a resource
	virtual void unMap(IPipeline *pipeline) const = 0;
};

class ITexture : public IResource
{
public:
	// Destructor
	virtual ~ITexture() { /**/ }

	// Create a depth stencil view from a texture.
	virtual std::unique_ptr<IDepthStencil> createDSView() = 0;

	// The textures format
	virtual CTextureFormat::Type textureFormat() const = 0;

	// The number of dimensions of the texture
	virtual CTextureDimension::Type textureDimension() const = 0;

	// Check if the texture format is compressed
	virtual bool isCompressed() const = 0;

	// Open access to the data of a texture
	virtual void* Map(IPipeline *pipeline, CResourceLock::Type lock, size_t& rowPitch,
		size_t& depthSlice) const = 0;

	// Close access to the data of a texture
	virtual void UnMap(IPipeline *pipeline, size_t subresource) const = 0;
};

class ITexture1D : public ITexture
{
public:
	// Virtual destructor
	virtual ~ITexture1D() { }

	// Function to create a shader resource view
	// ToDo: Move this function to ITexture.
	virtual std::unique_ptr<IShaderResource> createSRView(CTextureFormat::Type tformat, size_t highestMip,
		size_t maxlevels) = 0;

	// Get The mipmaps via a pointer 
	// #Note: they must be copied into a container
	virtual const SMipMap1D* info() const = 0;

	// Return the number of mip map levels
	virtual size_t numMips() const = 0;

	// Width of the texture
	virtual size_t width() const = 0;
};

class ITexture2D : public ITexture
{
public:
	// Virtual destructor
	virtual ~ITexture2D() { }

	// Function to create a resource view
	// ToDo: Move this function to ITexture.
	virtual std::unique_ptr<IShaderResource> createSRView(CTextureFormat::Type tformat, size_t highestMip,
		size_t maxlevels) = 0;

	// Get The mipmaps via a pointer 
	// #Note: they must be copied into a container
	virtual const SMipMap2D* info() const = 0;

	// Return the number of mip map levels
	virtual size_t numMips() const = 0;

	// Width of the texture
	virtual size_t width() const = 0;

	// Height of a texture
	virtual size_t height() const = 0;
};


/************************************************************************/
/* Descriptors used for creating resources within rendering device.		*/
/************************************************************************/
//class ResourceDesc
//{
//public:
//	typedef std::vector<uint8_t> data_type;
//
//	ResourceDesc(size_t bind_flags, CResourceAccess::Type usage, size_t buffer_capacity,
//		const uint8_t* data);
//
//	virtual ~ResourceDesc();
//
//	size_t bind_flags() const { return m_bindFlags; }
//	CResourceAccess::Type resource_usage() const { return m_resourceUsage; }
//	const uint8_t* data() const { return &m_data[0]; }
//	size_t buffer_capacity() const { return m_bufferCapacity; }
//
//protected:
//	size_t m_bindFlags;
//	size_t m_bufferCapacity;
//	CResourceAccess::Type m_resourceUsage;
//	uint8_t* m_data;
//};
//
//class TextureDesc : public ResourceDesc
//{
//public:
//	typedef TextureDesc::data_type data_type;
//
//	//************************************
//	// Method:    TextureDesc
//	// FullName:  Engine::TextureDesc::TextureDesc
//	// Access:    public  
//	// Qualifier:
//	// Parameter: size_t resource_usage sets GPU and CPU access rules for resource.
//	// Parameter: size_t bind_flags set resource interpretation, i.e. RenderTarget or ShaderResource etc.
//	// Parameter: CTextureFormat::Type tformat the data format of the texture.
//	// Parameter: const data_type & data texture data.
//	//************************************
//	TextureDesc(size_t bind_flags, CResourceAccess::Type usage,
//		CTextureFormat::Type tformat, const data_type& data);
//
//	//************************************
//	// Method:    TextureDesc
//	// FullName:  Engine::TextureDesc::TextureDesc
//	// Access:    public 
//	// Qualifier:
//	// Parameter: size_t resource_usage sets GPU and CPU access rules for resource.
//	// Parameter: size_t bind_flags set resource interpretation, i.e. RenderTarget or ShaderResource etc.
//	// Parameter: CTextureFormat::Type tformat the data format of the texture.
//	//************************************
//	TextureDesc(size_t bind_flags, CResourceAccess::Type usage,
//		CTextureFormat::Type tformat);
//	virtual ~TextureDesc();
//
//	CTextureFormat::Type format() const { return m_tFormat; }
//
//
//protected:
//	CTextureFormat::Type m_tFormat;
//};
//
//class Texture1DDesc : public TextureDesc
//{
//public:
//	typedef TextureDesc::data_type data_type;
//	typedef std::vector<SMipMap1D> mip_type;
//
//	//************************************
//	// Method:    Texture1DDesc
//	// FullName:  Engine::Texture1DDesc::Texture1DDesc
//	// Access:    public 
//	// Qualifier:
//	// Parameter: size_t resource_usage sets GPU and CPU access rules for resource.
//	// Parameter: size_t bind_flags set resource interpretation, i.e. RenderTarget or ShaderResource etc.
//	// Parameter: CTextureFormat::Type tformat the data format of the texture.
//	// Parameter: const data_type & data
//	// Parameter: const mip_type & mips
//	//************************************
//	Texture1DDesc(size_t bind_flags, CResourceAccess::Type usage,
//		CTextureFormat::Type tformat, const data_type& data, const mip_type& mips);
//
//	// Create a 1D texture resource with no mip-maps. This resource is not yet
//	// defined within the pipeline, but it's basics are defined.
//	Texture1DDesc(size_t bind_flags, CResourceAccess::Type usage,
//		CTextureFormat::Type tformat, size_t width);
//
//	~Texture1DDesc();
//
//protected:
//	mip_type m_mips;
//};
//
//class Texture2DDesc : public TextureDesc
//{
//public:
//	typedef TextureDesc::data_type data_type;
//	typedef std::vector<SMipMap2D> mip_type;
//
//
//	Texture2DDesc(size_t bind_flags, CResourceAccess::Type usage, CTextureFormat::Type tformat,
//		const data_type& data, const mip_type& mips);
//
//	// Create a 2D texture resource with no mip-maps. This resource is not yet
//	// defined within the pipeline, but it's basics are defined.
//	Texture2DDesc(size_t bind_flags, CResourceAccess::Type usage, CTextureFormat::Type tformat,
//		size_t width, size_t height);
//
//	~Texture2DDesc();
//
//protected:
//	mip_type m_mips;
//};

_ENGINE_END

#endif