#pragma once

#ifndef __CRESOURCE_D3D11_H__
#define __CRESOURCE_D3D11_H__

#include "D3D11Fwd.h"
#include "IResource.h"
#include "MappingD3D11.h"
#include "ViewD3D11.h"
#include "ShaderD3D11.h"


_ENGINE_BEGIN

// Classes
class CBufferD3D11 : public IBuffer
{
public:
	SMART_PTR_TYPES(CBufferD3D11)

	// Construction and destruction.
	CBufferD3D11(ID3D11BufferPtr buffer);

	// Releases the D3DResource
	~CBufferD3D11();

	// Create a resource view with specified values
	unique_ptr<IShaderResource> createSRView(CTextureFormat::Type tformat, size_t elementOffset,
		size_t elementWidth) override;

	unique_ptr<CShaderResourceD3D11> _createSRView(CTextureFormat::Type tformat, size_t elementOffset,
		size_t elementWidth);

	// Create a resource view using whole resource
	unique_ptr<IShaderResource> createSRView() override;

	unique_ptr<CShaderResourceD3D11> _createSRView();

	// Creates a render target view using the whole resource
	unique_ptr<IRenderTarget> createRTView() override;

	unique_ptr<CRenderTargetD3D11> _createRTView();

	// Open access to the data of a buffer
	void *map(IPipeline *pipeline, CResourceLock::Type lock,
			 size_t &alignedSize) const override;

	// Close access to the data of a resource
	void unMap(IPipeline *pipeline) const override;

	// The data total buffer's size in bytes
	size_t size() const override;

	// Buffer type determines if its a vertex or index buffer
	size_t bindFlags() const override;

	// Describes the buffers usage with relation to the hardware
	CResourceAccess::Type usage() const override;

	// Determine the type of resource thats been created
	CResourceType::Type type() const override;

	// Access to the underlying ID3D11Buffer
	ID3D11BufferPtr get() const;

private:
	// Direct3D resource
	ID3D11BufferPtr m_resource;

	// Resource desc
	D3D11_BUFFER_DESC m_desc;
};

class CTexture1DD3D11 : public ITexture1D
{
public:
	SMART_PTR_TYPES(CTexture1DD3D11)

	// Construction and destruction.
	CTexture1DD3D11(ID3D11Texture1DPtr res);

	// Releases the D3DResource
	~CTexture1DD3D11();

	// Create a resource view with specified values
	unique_ptr<IShaderResource> createSRView(CTextureFormat::Type tformat, size_t highestMip,
		size_t maxlevels) override;

	unique_ptr<CShaderResourceD3D11> _createSRView(CTextureFormat::Type tformat, size_t highestMip,
		size_t maxlevels);

	// Create a resource view using whole resource
	unique_ptr<IShaderResource> createSRView() override;

	// Creates a render target view using the whole resource
	unique_ptr<IRenderTarget> createRTView() override;

	// Create a depth stencil view buffer.
	unique_ptr<IDepthStencil> createDSView() override;

	//
	// Internal creation methods, not visible at interface
	// level.
	//

	unique_ptr<CShaderResourceD3D11> _createSRView();

	unique_ptr<CRenderTargetD3D11> _createRTView();

	unique_ptr<CDepthStencilD3D11> _createDSView();

	// Open access to the data of a texture
	void* Map(IPipeline *pipeline, CResourceLock::Type lock, size_t& rowPitch,
		size_t& depthSlice) const override;

	// Close access to the data of a texture
	void UnMap(IPipeline *pipeline, size_t subresource) const override;

	// Get The mipmaps via a pointer
	// #Note: they must be copied into a container
	const SMipMap1D* info() const override;

	// Return the number of mip map levels
	size_t numMips() const override;

	// Width of the texture
	size_t width() const override;

	// The data total buffer's size in bytes
	size_t size() const override;

	// Buffer type determines if its a vertex or index buffer
	size_t bindFlags() const override;

	// Describes the buffers usage with relation to the hardware
	CResourceAccess::Type usage() const override;

	// Determine the type of resource thats been created
	CResourceType::Type type() const override;

	// The textures format
	CTextureFormat::Type textureFormat() const override;

	// The number of dimensions of the texture
	CTextureDimension::Type textureDimension() const override;

	// Check if the texture format is compressed
	bool isCompressed() const override;

	// Access to the underlying ID3D11Texture1D
	ID3D11Texture1DPtr get() const;

private:
	// Direct3D resource
	ID3D11Texture1DPtr m_texture;

	// Resource desc
	D3D11_TEXTURE1D_DESC m_desc;
};

class CTexture2DD3D11 : public ITexture2D
{
public:
	SMART_PTR_TYPES(CTexture2DD3D11)

	// Construction and destruction.
	CTexture2DD3D11(ID3D11Texture2DPtr res);

	// Releases the D3DResource
	~CTexture2DD3D11();

	// Create a resource view with specified values
	unique_ptr<IShaderResource> createSRView(CTextureFormat::Type tformat, size_t highestMip,
		size_t maxlevels) override;

	unique_ptr<CShaderResourceD3D11> _createSRView(CTextureFormat::Type tformat, size_t highestMip,
		size_t maxlevels);

	// Create a resource view using whole resource
	unique_ptr<IShaderResource> createSRView() override;

	unique_ptr<CShaderResourceD3D11> _createSRView();

	// Creates a render target view using the whole resource
	unique_ptr<IRenderTarget> createRTView() override;

	unique_ptr<CRenderTargetD3D11> _createRTView();

	// Create a depth stencil view buffer.
	unique_ptr<IDepthStencil> createDSView() override;

	unique_ptr<CDepthStencilD3D11> _createDSView();

	// Open access to the data of a texture
	void* Map(IPipeline *pipeline, CResourceLock::Type lock, size_t& rowPitch,
		size_t& depthSlice) const override;

	// Close access to the data of a texture
	void UnMap(IPipeline *pipeline, size_t subresource) const override;

	// Get The mipmaps via a pointer
	// #Note: they must be copied into a container
	const SMipMap2D* info() const override;

	// Width of the texture
	size_t width() const override;

	// Height of a texture
	size_t height() const override;

	// Return the number of mip map levels
	size_t numMips() const override;

	// The data total buffer's size in bytes
	size_t size() const override;

	// Buffer type determines if its a vertex or index buffer
	size_t bindFlags() const override;

	// Describes the buffers usage with relation to the hardware
	CResourceAccess::Type usage() const override;

	// Determine the type of resource thats been created
	CResourceType::Type type() const override;

	// The textures format
	CTextureFormat::Type textureFormat() const override;

	// The number of dimensions of the texture
	CTextureDimension::Type textureDimension() const override;

	// Check if the texture format is compressed
	bool isCompressed() const override;

	// Access to the underlying ID3D11Texture2D
	ID3D11Texture2DPtr get() const;

private:
	// Direct3D resource
	ID3D11Texture2DPtr m_texture;

	// Resource desc
	D3D11_TEXTURE2D_DESC m_desc;
};

#include "ResourceD3D11.inl"

_ENGINE_END


#endif



//template < class Parent >
//class TResource : public Parent
//{
//public:
//	// Typedef for the parent type
//	typedef Parent parent_type;
//
//	// Construction and destruction.
//	TResource(size_t sizeInBytes, size_t type, CResourceAccess::Type usage);
//
//	// Virtual destructor
//	virtual ~TResource();
//
//	// The data total buffer's size in bytes
//	size_t GetBytesSize() const;
//
//	// Buffer type determines if its a vertex or index buffer
//	size_t GetBufferType() const;
//
//	// Describes the buffers usage with relation to the hardware
//	CResourceAccess::Type GetUsage() const;
//
//protected:
//	// Data members
//	size_t					m_sizeInBytes;	 // The number of bytes within the buffer
//	size_t					m_bufferType;	 // The buffer type. Multiple types can be combined.
//	CResourceAccess::Type		m_usage;		 // The buffer use on the GPU side of things
//};
//
//template < class Parent >
//class TTexture : public TResource<Parent>
//{
//public:
//	// Typedef for parent class
//	typedef TResource<Parent> parent_type;
//
//	// Constructors
//	TTexture(size_t type, CResourceAccess::Type use, CTextureFormat::Type format,
//		CTextureDimension::Type diemension);
//
//	// Virtual destructor
//	virtual ~TTexture();
//
//	// The textures format
//	CTextureFormat::Type GetTextureFormat() const;
//
//	// The number of dimensions of the texture
//	CTextureDimension::Type GetTextureDimension() const;
//
//	// Check if the texture format is compressed
//	bool IsCompressed() const;
//
//protected:
//	// Texture information
//	CTextureFormat::Type		m_textureFormat;
//	CTextureDimension::Type		m_textureDimensions;
//};
//
//template < class Parent >
//TResource<Parent>::TResource(size_t sizeInBytes, size_t type,
//	CResourceAccess::Type usage) :
//m_sizeInBytes(sizeInBytes),
//m_bufferType(type),
//m_usage(usage)
//{
//
//}
//
//template < class Parent >
//TResource<Parent>::~TResource()
//{
//
//}
//
//template < class Parent >
//size_t TResource<Parent>::GetBytesSize() const
//{
//	return m_sizeInBytes;
//}
//
//template < class Parent >	
//size_t TResource<Parent>::GetBufferType() const
//{
//	return m_bufferType;
//}
//
//template < class Parent >	
//CResourceAccess::Type TResource<Parent>::GetUsage() const
//{
//	return m_usage;
//}

////---------------------------------------------------------------------------------
//template < class Parent >
//TTexture<Parent>::TTexture(size_t type, CResourceAccess::Type use, CTextureFormat::Type format,
//		CTextureDimension::Type diemension) :
//parent_type(0, type, use),
//m_textureFormat(format),
//m_textureDimensions(diemension)
//{
//
//}
//
//template < class Parent >	
//CTextureFormat::Type TTexture<Parent>::GetTextureFormat() const
//{
//	return m_textureFormat;
//}
//
//template < class Parent >	
//CTextureDimension::Type TTexture<Parent>::GetTextureDimension() const
//{
//	return m_textureDimensions;
//}
//
//template < class Parent >	
//bool TTexture<Parent>::IsCompressed() const
//{
//
//}