#ifndef __CPIPELINE_D3D11_INL__
#define __CPIPELINE_D3D11_INL__

inline void PipelineD3D11::flush() {
	m_context->Flush();
}

inline void PipelineD3D11::ClearState() {
	m_context->ClearState();
}

inline ID3D11DeviceContextPtr PipelineD3D11::Get() const {
	return m_context;
}

inline 	void PipelineD3D11::enableInputLayout(shared_ptr<IInputFormat> const& inputformat) {
	enableInputLayout(inputformat.get());
}

inline void PipelineD3D11::enableIndexBuffer(std::shared_ptr<IBuffer> const& ibuffer, size_t stride, size_t offset) {
	enableIndexBuffer(ibuffer.get(), stride, offset);
}

inline void PipelineD3D11::enableShader(shared_ptr<IShader> const& vshader) {
	enableShader(vshader.get());
}


#endif