#ifndef __CTEXTURE_LOADER_H__
#define __CTEXTURE_LOADER_H__

#include "IResource.h"

_ENGINE_BEGIN

struct DesiredTexture
{
	size_t width;
	size_t height;
	size_t depth;
	size_t firstMipLevel;
	size_t mipLevels;
	size_t bindFlags;
	size_t miscFlags;
	size_t filter;
	size_t mipFilter;
	ITexture::TextureFormat format;
	ITexture::BufferUse usage;
};

// Load a texture from disk
ITexturePtr LoadTexture(IRenderer* device, const String& filename, const DesiredTexture& textureDesc);
ITexturePtr LoadTexture(IRenderer* device, const String& filename);

ITexturePtr LoadTexture(IRenderer* device, const IImage* pImage, const DesiredTexture& textureDesc);
ITexturePtr LoadTexture(IRenderer* device, const IImage* pImage);


_ENGINE_END

#endif