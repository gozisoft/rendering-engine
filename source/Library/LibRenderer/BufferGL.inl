#ifndef __CBUFFER_GL_INL__
#define __CBUFFER_GL_INL__

// Vertex buffer operations.
inline void CBufferGL::Enable()
{
	glBindBuffer(m_target, m_buffer);
}

inline void CBufferGL::Disable()
{
	glBindBuffer(m_target, 0);
}



#endif