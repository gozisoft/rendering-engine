//
// Created by Riad Gozim on 08/02/2016.
//

#import "librendererafx.h"
#import "RendererMetal.h"
#import "ResourceMetal.h"

using namespace engine;

// Init the rendering device in constructor
RendererMetal::RendererMetal()
    : m_device(MTLCreateSystemDefaultDevice())
{
    // Create a new command queue.
    m_queue = [m_device newCommandQueue];
}

RendererMetal::~RendererMetal()
{
    // [m_queue release];
}

CRenderType::Type RendererMetal::GetRenderType() const
{
    return CRenderType::RT_UNKNOWN;
}

shared_ptr<IPipeline> RendererMetal::getPipeline() const
{
    return shared_ptr<IPipeline>();
}

unique_ptr<IShader> RendererMetal::createShader(CShaderType::Type type, shader_handle handle)
{
    return unique_ptr<IShader>();
}

unique_ptr<IInputFormat> RendererMetal::createInputFormat(const CInputElement *elements,
                                                          size_t numElements,
                                                          shader_handle handle)
{
    return unique_ptr<IInputFormat>();
}

unique_ptr<IBuffer> RendererMetal::createBuffer(size_t bindFlags,
                                                CResourceAccess::Type usage,
                                                size_t sizeInBytes,
                                                const uint8_t *data)
{
    id<MTLBuffer> buffer = nil;
    if (data != nullptr) {
        // method creates a MTLBuffer object with a new storage allocation.
        buffer = [m_device newBufferWithBytes:&data[0] length:(NSUInteger) sizeInBytes
                                       options:ApiToMetal::BufferUsage(usage)];
    }
    else {
        // method creates a MTLBuffer object by copying data from existing storage
        buffer = [m_device newBufferWithLength:(NSUInteger) sizeInBytes
                                        options:ApiToMetal::BufferUsage(usage)];
    }
    return std::make_unique<BufferMetal>(buffer, bindFlags);
}

unique_ptr<ITexture1D> RendererMetal::createTexture(size_t bindFlags,
                                                    CResourceAccess::Type usage,
                                                    CTextureFormat::Type tformat,
                                                    const SMipMap1D *mips,
                                                    size_t numLevels,
                                                    const uint8_t *data,
                                                    size_t arraySize)
{
    return unique_ptr<ITexture1D>();
}

unique_ptr<ITexture2D> RendererMetal::createTexture(size_t bindFlags,
                                                    CResourceAccess::Type usage,
                                                    CTextureFormat::Type tformat,
                                                    const SMipMap2D *mips,
                                                    size_t numLevels,
                                                    const uint8_t *data,
                                                    size_t arraySize)
{
    return unique_ptr<ITexture2D>();
}

