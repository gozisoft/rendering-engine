#ifndef __CSHADER_D3D11_INL__
#define __CSHADER_D3D11_INL__

inline ID3D11DeviceChildPtr CShaderD3D11::Get() const
{
	return m_shader;
}

inline SD3DShaderVTable* CShaderD3D11::GetTable() const
{
	return m_vtable;
}

#endif

//inline void CShaderD3D11::Enable(const ID3D11DeviceContextPtr& pContext)
//{
//	 // Now set the shader
//    (pContext->*(m_pVTable->pSetShader))(m_pObjectD3D, nullptr, 0);
//}
//
//inline void CShaderD3D11::Disable(const ID3D11DeviceContextPtr& pContext)
//{
//	(pContext->*(m_pVTable->pSetShader))(nullptr, nullptr, 0);
//}