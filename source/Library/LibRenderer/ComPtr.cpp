#include "librendererafx.h"
#include "ComPtr.h"

_ENGINE_BEGIN

inline IUnknown* ComPtrAssign(IUnknown** pp, IUnknown* lp)
{
	if (pp == nullptr)
		return nullptr;
		
	if (lp != nullptr)
		lp->AddRef();

	if (*pp)
		(*pp)->Release();

	*pp = lp;
	return lp;
}

inline IUnknown* ComQIPtrAssign(IUnknown** pp, IUnknown* lp, REFIID riid)
{
	if (pp == nullptr)
		return nullptr;

	IUnknown* pTemp = *pp;
	*pp = nullptr;
	if (lp != nullptr)
		lp->QueryInterface(riid, (void**)pp);

	if (pTemp)
		pTemp->Release();

	return *pp;
}

_ENGINE_END