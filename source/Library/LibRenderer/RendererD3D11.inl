#ifndef RENDERER_MVC_D3D11_INL
#define RENDERER_MVC_D3D11_INL

//
// Covariant methods!
//

inline unique_ptr<IShader> RendererD3D11::createShader(CShaderType::Type type, shader_handle handle)
{
	// Determine the shader vtable
	SD3DShaderVTable* table = nullptr;
	switch (type)
	{
	case CShaderType::ST_VERTEX: table = &CShaderD3D11::s_vertexShaderTable;
		break;
	case CShaderType::ST_GEOMETRY: table = &CShaderD3D11::s_geometryShaderTable;
		break;
	case CShaderType::ST_PIXEL: table = &CShaderD3D11::s_pixelShaderTable;
		break;
	default:
		throw std::runtime_error("Unknown or unsupported shader type [RendererD3D11::GetShaderType]");
	};

	// Assign the shader pointer pointer
	return std::make_unique<CShaderD3D11>(create_shader(table, handle), table);
}

inline unique_ptr<IInputFormat> RendererD3D11::createInputFormat(const CInputElement* elements, size_t numElements,
	shader_handle handle)
{
	return std::make_unique<InputFormatD3D11>(elements, numElements,
		create_input_format(elements, numElements, handle));
}

inline unique_ptr<IBuffer> RendererD3D11::createBuffer(size_t bindFlags, CResourceAccess::Type usage,
	size_t sizeInBytes, const uint8_t* data)
{
	return _createBuffer(bindFlags, usage, sizeInBytes, data);
}

inline unique_ptr<ITexture1D> RendererD3D11::createTexture(size_t bindFlags, CResourceAccess::Type usage,
	CTextureFormat::Type tformat, const SMipMap1D* mips, size_t numLevels,
	const uint8_t* data, size_t arraySize)
{
	return _createTexture(bindFlags, usage, tformat, mips, numLevels, data, arraySize);
}

inline unique_ptr<ITexture2D> RendererD3D11::createTexture(size_t bindFlags, CResourceAccess::Type usage,
	CTextureFormat::Type tformat, const SMipMap2D* mips, size_t numLevels,
	const uint8_t* data, size_t arraySize)
{
	return _createTexture(bindFlags, usage, tformat, mips, numLevels, data, arraySize);
}

// Buffer creation, index, vertex, constant
inline unique_ptr<CBufferD3D11> RendererD3D11::_createBuffer(size_t bindFlags, CResourceAccess::Type usage,
	size_t sizeInBytes, const uint8_t* data)
{
	return std::make_unique<CBufferD3D11>(create_buffer(bindFlags, usage, sizeInBytes, data));
}

// Texture1D creation.
inline unique_ptr<CTexture1DD3D11> RendererD3D11::_createTexture(size_t bindFlags, CResourceAccess::Type usage,
	CTextureFormat::Type tformat, const SMipMap1D* mips, size_t numLevels,
	const uint8_t* data, size_t arraySize)
{
	return std::make_unique<CTexture1DD3D11>(create_texture(bindFlags, usage, tformat,
		mips, numLevels, data, arraySize));
}

// Texture2D creation.
inline unique_ptr<CTexture2DD3D11> RendererD3D11::_createTexture(size_t bindFlags, CResourceAccess::Type usage,
	CTextureFormat::Type tformat, const SMipMap2D* mips, size_t numLevels,
	const uint8_t* data, size_t arraySize)
{
	return std::make_unique<CTexture2DD3D11>(create_texture(bindFlags, usage, tformat,
		mips, numLevels, data, arraySize));
}


#endif