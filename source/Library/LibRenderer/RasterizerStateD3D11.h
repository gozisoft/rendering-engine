#pragma once
#ifndef RASTERIZER_STATE_D3D11_H
#define RASTERIZER_STATE_D3D11_H

#include "D3D11Fwd.h"
#include "IRasterizerState.h"

_ENGINE_BEGIN

class RasterizerStateD3D11 : public IRasterizerState
{
public:
	RasterizerStateD3D11(ID3D11RasterizerStatePtr const& state);
	~RasterizerStateD3D11();

	const ID3D11RasterizerStatePtr& get() const {
		return m_state;
	}

private:
	ID3D11RasterizerStatePtr m_state;
};

_ENGINE_END

#endif