#include "librendererafx.h"
#include "StencilState.h"

using namespace engine;

CStencilState::CStencilState() :
m_enabled(false),
m_compare(CM_NEVER),
m_reference(0),
m_mask(UINT_MAX),
m_writeMask(UINT_MAX),
m_onFail(OM_KEEP),
m_onZFail(OM_KEEP),
m_onZPass(OM_KEEP)
{


}

CStencilState::CStencilState(bool enable,
	CompareMode compare,
	uint32_t ref,
	uint32_t mask,
	uint32_t writeMask,
	OperationMode onFail,
	OperationMode onZFail,
	OperationMode onZPass) :
m_enabled(enable),
m_compare(compare),
m_reference(ref),
m_mask(mask),
m_writeMask(writeMask),
m_onFail(onFail),
m_onZFail(onZFail),
m_onZPass(onZPass)
{

}

CStencilState::~CStencilState()
{

}