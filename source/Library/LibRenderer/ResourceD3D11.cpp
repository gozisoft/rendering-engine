#include "librendererafx.h"
#include "ResourceD3D11.h"
#include "PipelineD3D11.h"
#include "MappingD3D11.h"
#include "ExceptionD3D.h"
#include <vector>
#include <stdexcept>

using namespace engine;

// =========================================================================================
// CBufferD3D11
// =========================================================================================
CBufferD3D11::CBufferD3D11(ID3D11BufferPtr buffer)
	:
	m_resource(buffer) {
	// Fill in the buffer description
	m_resource->GetDesc(&m_desc);
}
//-------------------------------------------------------------------------------------------------------------------
CBufferD3D11::~CBufferD3D11() {
}

//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CShaderResourceD3D11> CBufferD3D11::_createSRView(CTextureFormat::Type tformat, size_t elementOffset,
	size_t elementWidth) {
	// Check the validity of the resource
	if (!m_resource)
		throw std::runtime_error("Invalid D3D11 resource [CBufferD3D11::CreateView]");

	// Get the buffer description
	D3D11_BUFFER_DESC buffDesc;
	m_resource->GetDesc(&buffDesc);

	// Check the buffer has the resource flag
	if (!(buffDesc.BindFlags & D3D11_BIND_SHADER_RESOURCE))
		throw std::runtime_error("Buffer not created with shader resource flag [CBufferD3D11::CreateSRView]");

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_resource->GetDevice(&pDevice);

	// Resource view creation
	D3D11_SHADER_RESOURCE_VIEW_DESC resource_desc;
	resource_desc.Format = CAPI_To_D3D11::DXGIFormat(tformat);
	resource_desc.ViewDimension = D3D_SRV_DIMENSION_BUFFER;
	resource_desc.Buffer.ElementOffset = static_cast<UINT>(elementOffset);
	resource_desc.Buffer.ElementWidth = static_cast<UINT>(elementWidth);

	// Create the d3d view
	ID3D11ShaderResourceViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateShaderResourceView(m_resource, &resource_desc, &pViewD3D));

	// return the proxy
	return std::make_unique<CShaderResourceD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CShaderResourceD3D11> CBufferD3D11::_createSRView() {
	// Check the validity of the resource
	if (!m_resource)
		throw std::runtime_error("Invalid D3D11 resource [CBufferD3D11::CreateView]");

	// Get the buffer description
	D3D11_BUFFER_DESC buffDesc;
	m_resource->GetDesc(&buffDesc);

	// Check the buffer has the resource flag
	if (!(buffDesc.BindFlags & D3D11_BIND_SHADER_RESOURCE))
		throw std::runtime_error("Buffer not created with shader resource flag [CBufferD3D11::CreateSRView]");

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_resource->GetDevice(&pDevice);

	// Create the d3d view
	ID3D11ShaderResourceViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateShaderResourceView(m_resource, nullptr, &pViewD3D));

	// return the proxy
	return std::make_unique<CShaderResourceD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CRenderTargetD3D11> CBufferD3D11::_createRTView() {
	// Check the validity of the resource
	if (!m_resource)
		throw std::runtime_error("Invalid D3D11 resource [CBufferD3D11::CreateView]");

	// Get the buffer description
	D3D11_BUFFER_DESC buffDesc;
	m_resource->GetDesc(&buffDesc);

	// Check the buffer has the resource flag
	if (!(buffDesc.BindFlags & D3D11_BIND_RENDER_TARGET))
		throw std::runtime_error("Buffer not created with render target flag [CBufferD3D11::CreateRTView]");

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_resource->GetDevice(&pDevice);

	// Create the d3d view
	ID3D11RenderTargetViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateRenderTargetView(m_resource, nullptr, &pViewD3D));

	// return the proxy
	return std::make_unique<CRenderTargetD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
void* CBufferD3D11::map(IPipeline *pipeline, CResourceLock::Type lock, size_t &alignedSize) const {
	// Ensure pResource is valid
	if (!pipeline)
		throw std::runtime_error("IPipeline is an invalid pointer [CBufferD3D11::Map]");

	// map directly
	D3D11_MAP mapType = D3D11_MAP_WRITE;

	// Check if is either staging or dyanmic
	if (usage() & CResourceAccess::RA_DYNAMIC) {
		// Dynamic resource can write only
		switch (lock) {
		case CResourceLock::RL_DISCARD: mapType = D3D11_MAP_WRITE_DISCARD; break;
		case CResourceLock::RL_WRITE_ONLY: mapType = D3D11_MAP_WRITE; break;
		case CResourceLock::RL_NO_OVERWRITE: mapType = D3D11_MAP_WRITE_NO_OVERWRITE; break;
		default:
			throw std::runtime_error("Incorrect lock mode for use of dynamic buffer [CBufferD3D11::Map]");
		}
	}
	else if (usage() & CResourceAccess::RA_STAGING) {
		switch (lock) {
		case CResourceLock::RL_READ_ONLY: mapType = D3D11_MAP_READ; break;
		case CResourceLock::RL_READ_WRITE: mapType = D3D11_MAP_READ_WRITE; break;
		case CResourceLock::RL_DISCARD: mapType = D3D11_MAP_WRITE_DISCARD; break;
		case CResourceLock::RL_WRITE_ONLY: mapType = D3D11_MAP_WRITE; break;
		case CResourceLock::RL_NO_OVERWRITE: mapType = D3D11_MAP_WRITE_NO_OVERWRITE; break;
		default:
			throw std::runtime_error("Incorrect lock mode for use of dynamic or staging buffer [CBufferD3D11::Map]");
		}
	}

	// Find the resource type
	auto pPipelineD3D = PolymorphicDowncast<PipelineD3D11*>(pipeline);
	if (!pPipelineD3D)
		throw std::runtime_error("IResource is unknown type [CBufferD3D11::Map]");

	// Get the resource, whichever it may be...
	ID3D11DeviceContextPtr pContext = pPipelineD3D->Get();

	// Map the resource
	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
	ThrowIfFailed(pContext->Map(m_resource, 0, mapType, 0, &mappedSubResource));

	// Asign the aligned size
	alignedSize = static_cast<size_t>(mappedSubResource.RowPitch);

	// return the data
	return mappedSubResource.pData;
}
//-------------------------------------------------------------------------------------------------------------------
void CBufferD3D11::unMap(IPipeline *pipeline) const {
	// Ensure pResource is valid
	if (!pipeline)
		throw std::runtime_error("IPipeline is an invalid pointer [CBufferD3D11::UnMap]");

	// Find the resource type
	PipelineD3D11* pPipelineD3D = PolymorphicDowncast<PipelineD3D11*>(pipeline);
	if (!pPipelineD3D)
		throw std::runtime_error("IResource is unknown type [CBufferD3D11::UnMap]");

	// Call unmap from the immediate context
	pPipelineD3D->Get()->Unmap(m_resource, 0);
}



// =========================================================================================
// CTexture1DD3D11
// =========================================================================================
CTexture1DD3D11::CTexture1DD3D11(ID3D11Texture1DPtr res)
	:
	m_texture(res) {
	// Fill in the buffer description
	m_texture->GetDesc(&m_desc);
}
//-------------------------------------------------------------------------------------------------------------------
CTexture1DD3D11::~CTexture1DD3D11() {
	//if (m_texture)
	//	m_texture->Release();
}
//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CShaderResourceD3D11> CTexture1DD3D11::_createSRView(CTextureFormat::Type tformat, size_t highestMip,
	size_t maxlevels) {
	// Check the validity of the resource
	if (!m_texture) {
		throw std::runtime_error("Invalid D3D11 texture1d [CTexture1DD3D11::CreateView]");
	}

	// Get the buffer description
	D3D11_TEXTURE1D_DESC desc;
	m_texture->GetDesc(&desc);

	// Check the buffer has the resource flag
	if (!(desc.BindFlags & D3D11_BIND_SHADER_RESOURCE)) {
		throw std::runtime_error("Buffer not created with shader resource flag");
	}

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_texture->GetDevice(&pDevice);

	// Resource view creation
	D3D11_SHADER_RESOURCE_VIEW_DESC resource_desc;
	resource_desc.Format = CAPI_To_D3D11::DXGIFormat(tformat);
	resource_desc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE1D;
	resource_desc.Texture1D.MipLevels = static_cast<UINT>(highestMip);
	resource_desc.Texture1D.MostDetailedMip = static_cast<UINT>(maxlevels);

	// Create the d3d view
	ID3D11ShaderResourceViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateShaderResourceView(m_texture, &resource_desc, &pViewD3D));

	// Create the proxy class
	// An instance of this to be passed to the resource view
	return std::make_unique<CShaderResourceD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CShaderResourceD3D11> CTexture1DD3D11::_createSRView() {
	// Check the validity of the resource
	if (!m_texture) {
		throw std::runtime_error("Invalid D3D11 resource [CBufferD3D11::CreateView]");
	}


	// Get the buffer description
	D3D11_TEXTURE1D_DESC buffDesc;
	m_texture->GetDesc(&buffDesc);

	// Check the buffer has the resource flag
	if (!(buffDesc.BindFlags & D3D11_BIND_SHADER_RESOURCE)) {
		throw std::runtime_error("Buffer not created with shader resource flag [CBufferD3D11::CreateSRView]");
	}

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_texture->GetDevice(&pDevice);

	// Create the d3d view
	ID3D11ShaderResourceViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateShaderResourceView(m_texture, nullptr, &pViewD3D));

	// return the proxy
	return std::make_unique<CShaderResourceD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CRenderTargetD3D11> CTexture1DD3D11::_createRTView() {
	// Check the validity of the resource
	if (!m_texture) {
		throw std::runtime_error("Invalid D3D11 resource [CTexture1DD3D11::CreateRTView]");
	}

	// Get the buffer description
	D3D11_TEXTURE1D_DESC buffDesc;
	m_texture->GetDesc(&buffDesc);

	// Check the buffer has the resource flag
	if (!(buffDesc.BindFlags & D3D11_BIND_RENDER_TARGET))
		throw std::runtime_error("Buffer not created with render target flag [CTexture1DD3D11::CreateRTView]");

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_texture->GetDevice(&pDevice);

	// Create the d3d view
	ID3D11RenderTargetViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateRenderTargetView(m_texture, nullptr, &pViewD3D));

	// return the proxy
	return std::make_unique<CRenderTargetD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CDepthStencilD3D11> CTexture1DD3D11::_createDSView() {
	// Check the validity of the resource
	if (!m_texture) {
		throw std::runtime_error("Invalid D3D11 resource [CTexture1DD3D11::createDSView]");
	}

	// Get the texture description
	D3D11_TEXTURE1D_DESC buffDesc;
	m_texture->GetDesc(&buffDesc);

	// Check the buffer has the resource flag
	if (!(buffDesc.BindFlags & D3D11_BIND_DEPTH_STENCIL)) {
		throw std::runtime_error("DepthStencil not created, invalid bind texture flag. [CTexture1DD3D11::CTexture1DD3D11]");
	}

	// Valid depth stencil formats!
	switch (buffDesc.Format) {
	case DXGI_FORMAT_D16_UNORM:
	case DXGI_FORMAT_D24_UNORM_S8_UINT:
	case DXGI_FORMAT_D32_FLOAT:
	case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
	case DXGI_FORMAT_UNKNOWN:
		break;

	default:
		throw std::runtime_error("DepthStencil not created due to invalid texture format. [CTexture1DD3D11::CTexture1DD3D11]");
	}

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_texture->GetDevice(&pDevice);

	// Create the d3d view
	ID3D11DepthStencilViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateDepthStencilView(m_texture, nullptr, &pViewD3D));

	// return the proxy
	return std::make_unique<CDepthStencilD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
const SMipMap1D* CTexture1DD3D11::info() const {
	// Allocate array size for number of mipmaps.
	std::vector<SMipMap1D> mipInfo((size_t)m_desc.MipLevels);
	size_t width = static_cast<size_t>(m_desc.Width);
	size_t sizeInBytes = 0;

	// Setup mip description values
	for (size_t level = 0; level < mipInfo.size(); ++level) {
		size_t numBytes = static_cast<size_t>(BytesPerPixel(m_desc.Format)) * width;

		mipInfo[level].numBytes = numBytes;
		mipInfo[level].width = width;
		mipInfo[level].offset = sizeInBytes;

		// shift width down a power of 2
		if (width > 1)
			width >>= 1;

		// Increase the number of bytes
		sizeInBytes += numBytes;
	}

	// Return the mip info
	return &mipInfo[0];
}
//-------------------------------------------------------------------------------------------------------------------
void* CTexture1DD3D11::Map(IPipeline *pipeline, CResourceLock::Type lock, size_t& rowPitch,
	size_t& depthSlice) const {


	return nullptr;
}
//-------------------------------------------------------------------------------------------------------------------
void CTexture1DD3D11::UnMap(IPipeline *pipeline, size_t subresource) const {
	// Ensure pResource is valid
	if (!pipeline) {
		throw std::runtime_error("IPipeline is an invalid pointer [CBufferD3D11::Map]");
	}

	// Find the resource type
	PipelineD3D11* pPipelineD3D = PolymorphicDowncast<PipelineD3D11*>(pipeline);
	if (!pPipelineD3D) {
		throw std::runtime_error("IResource is unknown type [CBufferD3D11::UnMap]");
	}

	// Call unmap from the immediate context
	pPipelineD3D->Get()->Unmap(m_texture, subresource);
}



// =========================================================================================
// CTexture2DD3D11
// =========================================================================================
CTexture2DD3D11::CTexture2DD3D11(ID3D11Texture2DPtr res)
	:
	m_texture(res) {
	// Fill in the buffer description
	m_texture->GetDesc(&m_desc);
}
//-------------------------------------------------------------------------------------------------------------------
CTexture2DD3D11::~CTexture2DD3D11() {
	//if (m_texture)
	//	m_texture->Release();
}
//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CShaderResourceD3D11> CTexture2DD3D11::_createSRView(CTextureFormat::Type tformat, size_t highestMip,
	size_t maxlevels) {
	// Check the validity of the resource
	if (!m_texture) {
		throw std::runtime_error("Invalid D3D11 texture2d");
	}

	// Get the buffer description
	D3D11_TEXTURE2D_DESC desc;
	m_texture->GetDesc(&desc);

	// Check the buffer has the resource flag
	if (!(desc.BindFlags & D3D11_BIND_SHADER_RESOURCE)) {
		assert(false && ieS("Buffer not created with shader resource flag"));
	}

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_texture->GetDevice(&pDevice);

	// Resource view creation
	D3D11_SHADER_RESOURCE_VIEW_DESC resource_desc;
	resource_desc.Format = CAPI_To_D3D11::DXGIFormat(tformat);
	resource_desc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE2D;
	resource_desc.Texture2D.MipLevels = static_cast<UINT>(highestMip);
	resource_desc.Texture2D.MostDetailedMip = static_cast<UINT>(maxlevels);

	// Create the d3d view
	ID3D11ShaderResourceViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateShaderResourceView(m_texture, &resource_desc, &pViewD3D));

	// Create the proxy class
	// An instance of this to be passed to the resource view
	return std::make_unique<CShaderResourceD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CShaderResourceD3D11> CTexture2DD3D11::_createSRView() {
	// Check the validity of the resource
	if (!m_texture)
		throw std::runtime_error("Invalid D3D11 texture2d");

	// Get the buffer description
	D3D11_TEXTURE2D_DESC desc;
	m_texture->GetDesc(&desc);

	// Check the buffer has the resource flag
	if (!(desc.BindFlags & D3D11_BIND_SHADER_RESOURCE))
		assert(false && ieS("Buffer not created with shader resource flag"));

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_texture->GetDevice(&pDevice);

	// Create the d3d view
	ID3D11ShaderResourceViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateShaderResourceView(m_texture, nullptr, &pViewD3D));

	// Create the proxy class
	// An instance of this to be passed to the resource view
	return std::make_unique<CShaderResourceD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CRenderTargetD3D11> CTexture2DD3D11::_createRTView() {
	// Check the validity of the resource
	if (!m_texture) {
		throw std::runtime_error("Invalid D3D11 resource [CBufferD3D11::CreateView]");
	}

	// Get the buffer description
	D3D11_TEXTURE2D_DESC buffDesc;
	m_texture->GetDesc(&buffDesc);

	// Check the buffer has the resource flag
	if (!(buffDesc.BindFlags & D3D11_BIND_RENDER_TARGET)) {
		throw std::runtime_error("Buffer not created with render target flag [CBufferD3D11::CreateRTView]");
	}

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_texture->GetDevice(&pDevice);

	// Create the d3d view
	ID3D11RenderTargetViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateRenderTargetView(m_texture, nullptr, &pViewD3D));

	// return the proxy
	return std::make_unique<CRenderTargetD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
unique_ptr<CDepthStencilD3D11> CTexture2DD3D11::_createDSView() {
	// Check the validity of the resource
	if (!m_texture) {
		throw std::runtime_error("Invalid D3D11 resource [CTexture2DD3D11::createDSView]");
	}

	// Get the texture description
	D3D11_TEXTURE2D_DESC buffDesc;
	m_texture->GetDesc(&buffDesc);

	// Check the buffer has the resource flag
	if (!(buffDesc.BindFlags & D3D11_BIND_DEPTH_STENCIL)) {
		throw std::runtime_error("DepthStencil not created, invalid bind texture flag. [CTexture1DD3D11::CTexture1DD3D11]");
	}

	// Valid depth stencil formats!
	switch (buffDesc.Format) {
	case DXGI_FORMAT_D16_UNORM:
	case DXGI_FORMAT_D24_UNORM_S8_UINT:
	case DXGI_FORMAT_D32_FLOAT:
	case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
	case DXGI_FORMAT_UNKNOWN:
		break;

	default:
		throw std::runtime_error("DepthStencil not created due to invalid texture format. [CTexture1DD3D11::CTexture1DD3D11]");
	}

	// Get the D3D11 device
	ID3D11DevicePtr pDevice = nullptr;
	m_texture->GetDevice(&pDevice);

	// Create the d3d view
	ID3D11DepthStencilViewPtr pViewD3D = nullptr;
	ThrowIfFailed(pDevice->CreateDepthStencilView(m_texture, nullptr, &pViewD3D));

	// return the proxy
	return std::make_unique<CDepthStencilD3D11>(pViewD3D);
}
//-------------------------------------------------------------------------------------------------------------------
const SMipMap2D* CTexture2DD3D11::info() const {
	// Allocate array size for number of mipmaps.
	std::vector<SMipMap2D> mipInfo((size_t)m_desc.MipLevels);
	size_t width = static_cast<size_t>(m_desc.Width);
	size_t height = static_cast<size_t>(m_desc.Height);
	size_t sizeInBytes = 0;

	// Is using block compression
	if (isCompressed()) {
		// Determine if using compression the fast way
		size_t dxtBytesPerBlock = 16;
		switch (m_desc.Format) {
		case DXGI_FORMAT_BC1_TYPELESS:
		case DXGI_FORMAT_BC1_UNORM:
		case DXGI_FORMAT_BC1_UNORM_SRGB:
		case DXGI_FORMAT_BC4_TYPELESS:
		case DXGI_FORMAT_BC4_UNORM:
		case DXGI_FORMAT_BC4_SNORM:
			dxtBytesPerBlock = 8;
			break;
		};

		// Setup mip description values
		for (size_t level = 0; level < mipInfo.size(); ++level) {
			size_t numBlocksWide = 0;
			if (width > 0) {
				numBlocksWide = std::max(1U, width / 4);
			}

			size_t numBlocksHigh = 0;
			if (height > 0) {
				numBlocksHigh = std::max(1U, height / 4);
			}

			size_t numBytes = dxtBytesPerBlock*numBlocksWide*numBlocksHigh;
			mipInfo[level].numBytes = numBytes;
			mipInfo[level].width = width;
			mipInfo[level].height = height;
			mipInfo[level].offset = sizeInBytes;

			if (width > 1)
				width >>= 1;

			if (height > 1)
				height >>= 1;

			// Increment the number of bytes
			sizeInBytes += numBytes;
		}
	}
	else // Not using DXT
	{
		// Setup mip description values
		for (size_t level = 0; level < mipInfo.size(); ++level) {
			size_t numBytes = BytesPerPixel(m_desc.Format) * width * height;

			mipInfo[level].numBytes = numBytes;
			mipInfo[level].width = width;
			mipInfo[level].height = height;
			mipInfo[level].offset = sizeInBytes;

			// shift width down a power of 2
			if (width > 1)
				width >>= 1;

			if (height > 1)
				height >>= 1;

			// Increase the number of bytes
			sizeInBytes += numBytes;
		}
	}

	// Return the mip info
	return &mipInfo[0];
}
//-------------------------------------------------------------------------------------------------------------------
void* CTexture2DD3D11::Map(IPipeline *pipeline, CResourceLock::Type lock, size_t& rowPitch,
	size_t& depthSlice) const {


	return nullptr;
}
//-------------------------------------------------------------------------------------------------------------------
void CTexture2DD3D11::UnMap(IPipeline *pipeline, size_t subresource) const {
	// Ensure pResource is valid
	if (!pipeline)
		throw std::runtime_error("IPipeline is an invalid pointer [CBufferD3D11::Map]");

	// Find the resource type
	auto pPipelineD3D = PolymorphicDowncast<PipelineD3D11*>(pipeline);
	if (!pPipelineD3D)
		throw std::runtime_error("IResource is unknown type [CBufferD3D11::UnMap]");

	// Call unmap from the immediate context
	pPipelineD3D->Get()->Unmap(m_texture, subresource);
}













//CVertexBufferD3D11::CVertexBufferD3D11(const CVertexBufferD3D11& info, ID3D11BufferPtr buffer) :
//CBufferD3D11(info, 
//{
//
//}
//
//CVertexBufferD3D11::~CVertexBufferD3D11()
//{
//	
//}
//
//CIndexBufferD3D11::CIndexBufferD3D11(const CIndexBufferD3D11& info, ID3D11BufferPtr buffer) : m_buffer(buffer)
//{
//
//}
//
//CIndexBufferD3D11::~CIndexBufferD3D11()
//{
//	m_buffer->Release();
//}
//
//CConstantBufferD3D11::CConstantBufferD3D11(const CConstantBufferD3D11& info, ID3D11BufferPtr buffer)
//{
//
//
//}
//
//CConstantBufferD3D11::~CConstantBufferD3D11()
//{
//
//}
//
//CTexture1DD3D11::CTexture1DD3D11(const CImage* pImage)
//{
//
//
//}
//
//CTexture1DD3D11::CTexture1DD3D11(const CTexture1D& info, ID3D11Texture1DPtr res)
//{
//
//}
//
//CTexture1DD3D11::~CTexture1DD3D11()
//{
//
//}
//
//// Special constructor for direct creation from an image file
//CTexture2DD3D11::CTexture2DD3D11(const CImage* pImage)
//{
//
//
//}
//
//// Construction and destruction.
//CTexture2DD3D11::CTexture2DD3D11(const CTexture2D& info, ID3D11Texture2DPtr res)
//{
//
//
//}
//
//CTexture2DD3D11::~CTexture2DD3D11()
//{
//
//}



//// Buffer creation methods
//ID3D11BufferPtr CreateBufferD3D11(const CBuffer* pBuffer, ID3D11DevicePtr& pDevice);
//
//// Texture creation methods
//ID3D11Texture1DPtr CreateTextureD3D11(const CTexture1D* pTexture, ID3D11DevicePtr& pDevice);
//ID3D11Texture2DPtr CreateTextureD3D11(const CTexture2D* pTexture, ID3D11DevicePtr& pDevice);
//
//// Map and unmap implementations
//void* MapImpl(size_t offset, size_t length, CBuffer::Locking mode, ID3D11ResourcePtr& resource);
//void UnmapImpl(ID3D11ResourcePtr& resource);


//ID3D11BufferPtr CreateBufferD3D11(const CBuffer* pBuffer, ID3D11DevicePtr& pDevice)
//{
//	// This is sed for the HR_THROW macro
//	HRESULT hr = S_OK;
//
//	// Fill in a buffer description.
//	D3D11_BUFFER_DESC buff_desc;
//	buff_desc.ByteWidth = static_cast<size_t>( pBuffer->GetBytesSize() );
//	buff_desc.Usage = D3D11BufferUsage( pBuffer->GetUsage() );
//	buff_desc.BindFlags = D3D11BindFlag( pBuffer->GetBufferType() );
//
//	// Dynamic buffers can write to a resource.
//	// Staging buffers can read from or write to a resource.
//	switch (buff_desc.Usage)
//	{
//	case D3D11_USAGE_DYNAMIC:
//		buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//		break;
//	case D3D11_USAGE_STAGING:
//		buff_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
//		break;
//	default:
//		buff_desc.CPUAccessFlags = 0;
//		break;
//	};
//
//	buff_desc.MiscFlags = 0; // D3D11_RESOURCE_MISC_FLAG (for textures and such)
//	buff_desc.StructureByteStride = 0;
//	
//	// Fill in the subresource data.
//	D3D11_SUBRESOURCE_DATA InitData;
//	InitData.pSysMem = static_cast<const void*>( pBuffer->data() ); // Pointer to the initialization data.
//
//	// The distance (in bytes) from the beginning of one line of a texture to the next line.
//	// System-memory pitch is used only for 2D and 3D texture data as it is has no meaning
//	// for the other resource types.
//	InitData.SysMemPitch = 0;
//
//	// The distance (in bytes) from the beginning of one depth level to the next.
//	// System-memory-slice pitch is only used for 3D texture data as it has no
//	// meaning for the other resource types.
//	InitData.SysMemSlicePitch = 0;
//
//	// Create the buffer from the D3D11 device
//	ID3D11BufferPtr d3dBuffer = nullptr;
//	HR_THROW( pDevice->CreateBuffer(&buff_desc, &InitData, &d3dBuffer) );
//
//	// Return the created buffer
//	return d3dBuffer;
//}

//// Texture creation methods
//ID3D11Texture1DPtr CreateTextureD3D11(const CTexture1D* pTexture, ID3D11DevicePtr& pDevice)
//{
//	DXGI_SAMPLE_DESC sample_desc;
//	sample_desc.Count = 1;
//	sample_desc.Quality = 0;
//
//	D3D11_TEXTURE1D_DESC texture_desc;
//	texture_desc.Width = pTexture->GetWidth();
//	texture_desc.MipLevels = pTexture->GetNumMipLevels();
//	texture_desc.Format = D3D11DXGIFormat( pTexture->GetTextureFormat() );
//	texture_desc.ArraySize = 1;
//	texture_desc.Usage = D3D11BufferUsage( pTexture->GetUsage() );
//	texture_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
//	texture_desc.CPUAccessFlags = 0;
//	texture_desc.MiscFlags = 0;
//
//	D3D11_SUBRESOURCE_DATA *pInitData = nullptr;
//	if (D3D11_USAGE_DEFAULT == texture_desc.Usage)
//	{
//		// Create the sub resource
//		pInitData = new D3D11_SUBRESOURCE_DATA[pTexture->GetNumMipLevels()];
//
//		// Loopp over all the mips and copy their data to the subresource
//		for (size_t i = 0; i < pTexture->GetNumMipLevels(); ++i)
//		{
//			// Fill in the subresource data.
//			pInitData->pSysMem = static_cast<const void*>( pTexture->data(i) ); // Pointer to the initialization data.
//
//			// Get the texture infromation with DWORD alignment
//			ImageFormat tFormat = pTexture->GetTextureFormat();
//			uint32_t bytesPerPixel = BytesPerPixel(tFormat);
//			uint32_t bitsPerPixel = bytesPerPixel * 8;
//			uint32_t rowBytes = ImageConversion::CalculateLine(pTexture->GetMipMap(i).width, bitsPerPixel);
//
//			// The distance (in bytes) from the beginning of one line of a texture to the next line.
//			pInitData->SysMemPitch = bytesPerPixel * rowBytes;
//
//			// The distance (in bytes) from the beginning of one depth level to the next.
//			pInitData->SysMemSlicePitch = 0;
//		}
//	}
//
//	// Create the texture from the D3D11 device
//	ID3D11Texture1DPtr d3dTexture = nullptr;
//	HRESULT hr = pDevice->CreateTexture1D(&texture_desc, pInitData, &d3dTexture);
//
//	// Delete the pInitData
//	boost::checked_array_delete(pInitData);
//
//	// Check result of hersult
//	if ( FAILED(hr) )
//		Video::CD3D11ThrowImpl(hr);
//
//	// All passed, return the texture
//	return d3dTexture;
//}
//
//ID3D11Texture2DPtr CreateTextureD3D11(const CTexture2D* pTexture, ID3D11DevicePtr& pDevice)
//{
//	HRESULT hr = S_OK;
//
//	DXGI_SAMPLE_DESC sample_desc;
//	sample_desc.Count = 1;
//	sample_desc.Quality = 0;
//
//	D3D11_TEXTURE2D_DESC texture_desc;
//	texture_desc.Width = pTexture->GetWidth();
//	texture_desc.Height = pTexture->GetHeight();
//	texture_desc.MipLevels = pTexture->GetNumMipLevels();
//	texture_desc.Format = D3D11DXGIFormat( pTexture->GetTextureFormat() );
//	texture_desc.ArraySize = 1;
//	texture_desc.SampleDesc = sample_desc;
//	texture_desc.Usage = D3D11BufferUsage( pTexture->GetUsage() );
//	texture_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
//	texture_desc.CPUAccessFlags = 0;
//	texture_desc.MiscFlags = 0;
//
//	D3D11_SUBRESOURCE_DATA *pInitData = nullptr;
//	if (D3D11_USAGE_DEFAULT == texture_desc.Usage)
//	{
//		// Create the sub resource
//		pInitData = new D3D11_SUBRESOURCE_DATA[pTexture->GetNumMipLevels()];
//
//		// Loopp over all the mips and copy their data to the subresource
//		for (size_t i = 0; i < pTexture->GetNumMipLevels(); ++i)
//		{
//			// Fill in the subresource data.
//			pInitData[i].pSysMem = static_cast<const void*>( pTexture->data(i) ); // Pointer to the initialization data.
//
//			uint32_t width = pTexture->GetMipMap(i).width;
//			uint32_t height = pTexture->GetMipMap(i).height;
//			ImageFormat imageFormat = pTexture->GetTextureFormat();
//
//			// Get the texture infromation with DWORD alignment
//			if (imageFormat == TF_DXT1 || imageFormat == TF_DXT3 || imageFormat  == TF_DXT5)
//			{
//				uint32_t numBlocksWide = 0;
//				if (width > 0)
//					numBlocksWide = max(1, width / 4);
//
//				pInitData[i].SysMemPitch  = numBlocksWide * BytesPerPixel(imageFormat);
//			}
//			else
//			{
//				uint32_t bpp = BitsPerPixel( imageFormat );
//				pInitData[i].SysMemPitch = (width * bpp + 7) / 8; // round up to nearest byte
//			}
//		}
//	}
//
//	// Create the texture from the D3D11 device
//	ID3D11Texture2DPtr d3dTexture = nullptr;
//	hr = pDevice->CreateTexture2D(&texture_desc, pInitData, &d3dTexture);		
//
//	// Delete the pInitData
//	boost::checked_array_delete(pInitData);
//
//	// Check result of hersult
//	if ( FAILED(hr) )
//		Video::CD3D11ThrowImpl(hr);
//
//	// All passed, return the texture
//	return d3dTexture;
//}
//
//// http://msdn.microsoft.com/en-us/library/ff476181(v=vs.85).aspx
//void* MapImpl(size_t offset, size_t length, CBuffer::Locking mode, ID3D11ResourcePtr& resource)
//{
//	// USed for the error checking macro HR_THROW
//	HRESULT hr = S_OK;
//	
//	// Get the device that created the buffer
//	ID3D11DevicePtr pDevice;
//	m_pBuffer->GetDevice(&pDevice);
//
//	// Retrieve the buffer description
//	D3D11_BUFFER_DESC buff_desc;
//	m_pBuffer->GetDesc(&buff_desc);
//
//	if (static_cast<size_t>(length) > buff_desc.ByteWidth)
//	{
//		// need to realloc
//		buff_desc.ByteWidth = static_cast<size_t>(length);
//		HR_THROW( pDevice->CreateBuffer(&buff_desc, 0, &m_pBuffer) );
//	}
//
//	// map directly
//	D3D11_MAP mapType;
//
//	// Check if is either staging or dyanmic
//	if (buff_desc.CPUAccessFlags & D3D11_CPU_ACCESS_WRITE)
//	{
//		// This will be a D3D11_USAGE_DYNAMIC or a D3D11_USAGE_STAGING resource
//		switch (mode)
//		{
//		case CBuffer::RL_DISCARD: // Does not work with textures 
//			mapType = D3D11_MAP_WRITE_DISCARD;
//			break;
//		case CBuffer::RL_WRITE_ONLY: 
//			mapType = D3D11_MAP_WRITE;
//			break;
//		case CBuffer::RL_NO_OVERWRITE: // Only useable on a vertex or index buffer
//			if ( buff_desc.BindFlags & D3D11_BIND_VERTEX_BUFFER || 
//				 buff_desc.BindFlags & D3D11_BIND_INDEX_BUFFER )
//			{
//				mapType = D3D11_MAP_WRITE_NO_OVERWRITE;
//			}
//			break;
//		default:
//			assert( false && ieS("Incorrect lock mode for use of dynamic or staging buffer") );
//			return 0;
//		}
//	}
//	else if (buff_desc.CPUAccessFlags & D3D11_CPU_ACCESS_READ)
//	{
//		// This will be a D3D11_USAGE_STAGING resource
//		switch (mode)
//		{
//		case CBuffer::RL_READ_ONLY: // This will be a D3D11_USAGE_DYNAMIC resource
//			mapType = D3D11_MAP_READ;
//			break;
//		case CBuffer::RL_READ_WRITE: // This will be a D3D11_USAGE_STAGING resource
//			mapType = D3D11_MAP_READ_WRITE;
//			break;
//		default:
//			assert( false && ieS("Incorrect lock mode for use of dynamic or staging buffer") );
//			return 0;
//		}
//	}
//
//	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
//	mappedSubResource.pData = 0;
//	HR_THROW( pContext->Map(m_pBuffer, 0, mapType, 0, &mappedSubResource) );
//
//	// return the data
//	return static_cast<void*>(static_cast<uint8_t*>(mappedSubResource.pData) + offset);
//}
//
//void UnmapImpl(ID3D11ResourcePtr& resource)
//{
//	// Unmap the buffer
//	pRes->m_pContext->Unmap(resource, 0);
//}