//
// Created by Riad Gozim on 22/12/2015.
//

#ifndef ENGINE_PIPELINEGL_H
#define ENGINE_PIPELINEGL_H

#include "OpenGLFwd.h"
#include "IPipeline.h"
#include "RendererTypes.h"

_ENGINE_BEGIN

class PipelineGL: public IPipeline
{
public:
    PipelineGL();
    ~PipelineGL();

    virtual void flush() override;
    virtual void ClearState() override;
    virtual void Clear(const IRenderTarget *renderTarget, const float *rgba) override;
    virtual void Clear(shared_ptr<IRenderTarget> renderTarget, const float *rgba) override;
    virtual void Clear(const IDepthStencil *depthStencil, uint32_t stateFlags, float depth, uint8_t stencil) override;
    virtual void enableInputLayout(const IInputFormat *inputformat) override;
    virtual void enableInputLayout(std::shared_ptr<IInputFormat> const &inputformat) override;
    virtual void enableVertexBuffers
        (const IBuffer **vbuffers, size_t numbuffers, size_t startslot, size_t *strides, size_t *offsets) override;
    virtual void enableVertexBuffers(std::vector<BufferDesc> const &descriptors, size_t startslot) override;
    virtual void enableIndexBuffer(const IBuffer *ibuffer, size_t stide, size_t offset) override;
    virtual void enableIndexBuffer(std::shared_ptr<IBuffer> const &ibuffer, size_t stide, size_t offset) override;
    virtual void enableConstantBuffer(const IBuffer **cbuffers, size_t numbuffers, size_t startslot) override;
    virtual void enableConstantBuffer(std::vector<shared_ptr<IBuffer>> const &cbuffers, size_t startslot) override;
    virtual void enableShader(const IShader *vshader) override;
    virtual void enableShader(shared_ptr<IShader> const &vshader) override;
    virtual void setInputDrawingMode() override;
    virtual void
        setRenderTargets(const IRenderTarget **renderTargets, size_t num, const IDepthStencil *depthStencil) override;
    virtual void setRenderTargets
        (std::vector<shared_ptr<IRenderTarget>> const &renderTargets, shared_ptr<IDepthStencil> const &depthStencil)
        override;
    virtual void SetViewports(const ViewPort *viewports, size_t numViewPorts) override;
    virtual void drawIndexd(size_t indexCount, size_t startIndex) override;
    virtual void draw(size_t count, size_t startVertex) override;
};

_ENGINE_END


#endif //ENGINE_PIPELINEGL_H
