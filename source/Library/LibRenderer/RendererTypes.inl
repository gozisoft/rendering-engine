#ifndef __TYPES_API_INL__
#define __TYPES_API_INL__

#include "RendererTypes.h"
#include "ExceptionGL.h"

_ENGINE_BEGIN

// =================================================================================================================
// Rasterization types
// =================================================================================================================
inline RasterizerState::RasterizerState()
	:
	fillMode(FILL_SOLID),
	cullMode(CULL_BACK),
	frontCCW(true),
	depthBias(0),
	depthBiasClamp(0.0f),
	slopeScaledDepthBias(0.0f),
	enableDepthClip(true),
	enableScissor(false),
	enableMultisample(false),
	enableAntialiasedLine(false) {
}

// =================================================================================================================
// Rendering device types
// =================================================================================================================
template <typename T>
ViewPort::ViewPort(const T* data, float minZ, float maxZ)
{
	m_data[0] = static_cast<float>(data[0]); // Set x coord to left.
	m_data[1] = static_cast<float>(data[1]); // Set y coord to bottom.
	m_data[2] = static_cast<float>(data[2]);
	m_data[3] = static_cast<float>(data[3]);
	m_data[4] = minZ;
	m_data[5] = maxZ;
}

inline ViewPort::ViewPort(ViewPort&& other) {
	*this = std::move(other);
}

inline ViewPort& ViewPort::operator=(ViewPort&& other) {
	std::swap(m_data, other.m_data);
	return *this;
}

inline float ViewPort::aspect_ratio() const {
	return width() / height();
}

inline float ViewPort::posX() const {
	return m_data[0];
}

inline float ViewPort::posY() const {
	return m_data[1];
}

inline float ViewPort::width() const {
	return m_data[2];
}

inline float ViewPort::height() const {
	return m_data[3];
}

inline float ViewPort::minZ() const {
	return m_data[4];
}

inline float ViewPort::maxZ() const {
	return m_data[5];
}

// =======================================================================================
// Vertex types
// =======================================================================================
inline size_t CDataType::SizeOfType(CDataType::Type type)
{ 
	// Find the size of an input variable type
	switch (type)
	{
	case CDataType::DT_BYTE:
	case CDataType::DT_UBYTE:
		return sizeof(int8_t);
	case CDataType::DT_SHORT:
	case CDataType::DT_USHORT:
		return sizeof(int16_t);
	case CDataType::DT_INT:
	case CDataType::DT_UINT:
		return sizeof(int32_t);
	case CDataType::DT_FLOAT:
		return sizeof(float);
	case CDataType::DT_DOUBLE:
		return sizeof(double);
	default:
		throw opengl_error("Unknown variable type: SizeOfType()");
		return 0;
	};
}
// =======================================================================================
// Texture types
// =======================================================================================
inline size_t CTextureFormat::BytesPerPixel(CTextureFormat::Type format)
{
	switch (format)
	{
		// four dword sized components
	case CTextureFormat::TF_R32G32B32A32_TYPELESS:
	case CTextureFormat::TF_R32G32B32A32_FLOAT:
	case CTextureFormat::TF_R32G32B32A32_UINT:
	case CTextureFormat::TF_R32G32B32A32_INT:
		return 16;

		// three dword sized components
	case CTextureFormat::TF_R32G32B32_TYPELESS:
	case CTextureFormat::TF_R32G32B32_FLOAT:
	case CTextureFormat::TF_R32G32B32_UINT:
	case CTextureFormat::TF_R32G32B32_INT:
		return 12;

		// two dword sized components
	case CTextureFormat::TF_R32G32_TYPELESS:
	case CTextureFormat::TF_R32G32_FLOAT:
	case CTextureFormat::TF_R32G32_UINT:
	case CTextureFormat::TF_R32G32_INT:
		return 8;

		// single dword sized components
	case CTextureFormat::TF_R32_TYPELESS:
	case CTextureFormat::TF_R32_FLOAT: 
	case CTextureFormat::TF_D32_FLOAT:
		return 4;

		// four word sized components
	case CTextureFormat::TF_R16G16B16A16_TYPELESS:
	case CTextureFormat::TF_R16G16B16A16_FLOAT:
	case CTextureFormat::TF_R16G16B16A16_UINT:
	case CTextureFormat::TF_R16G16B16A16_INT:
		return 8;

		// two word sized components
	case CTextureFormat::TF_R16G16_TYPELESS:
	case CTextureFormat::TF_R16G16_FLOAT:
	case CTextureFormat::TF_R16G16_UINT:
	case CTextureFormat::TF_R16G16_INT:
		return 4;

		// 16-bit integer formats.
	case CTextureFormat::TF_L16: 
	case CTextureFormat::TF_R16F:
		return 2;

		// four byte sized components
	case CTextureFormat::TF_R8G8B8A8_TYPELESS:
	case CTextureFormat::TF_R8G8B8A8_UNORM:
	case CTextureFormat::TF_R8G8B8A8_SNORM:
	case CTextureFormat::TF_R8G8B8A8_UINT:
	case CTextureFormat::TF_R8G8B8A8_INT:
		return 4;

		// two byte sized components
	case CTextureFormat::TF_R8G8_TYPELESS:
	case CTextureFormat::TF_R8G8_UINT:
	case CTextureFormat::TF_R8G8_INT:
		return 2;

		// single byte sized components.
	case CTextureFormat::TF_A8:
	case CTextureFormat::TF_L8:

		// Compressed formats
	//case CTextureFormat::TF_DXT1: return 8;	
	//case CTextureFormat::TF_DXT3: return 16;
	//case CTextureFormat::TF_DXT5: return 16;

	default:
		throw opengl_error("Unknown CTextureFormat");
		return 0;
	};
}
//----------------------------------------------------------------------------------------------------------------------------
inline size_t CTextureFormat::BitsPerPixel(CTextureFormat::Type format)
{
	return BytesPerPixel(format) * 8;
}
//----------------------------------------------------------------------------------------------------------------------------
inline bool CTextureFormat::IsCompressed(CTextureFormat::Type format)
{
    switch (format)
    {
    case CTextureFormat::TF_BC1_TYPELESS:
    case CTextureFormat::TF_BC1_UNORM:
    case CTextureFormat::TF_BC1_UNORM_SRGB:
    case CTextureFormat::TF_BC4_TYPELESS:
    case CTextureFormat::TF_BC4_UNORM:
    case CTextureFormat::TF_BC4_SNORM:
    case CTextureFormat::TF_BC2_TYPELESS:
    case CTextureFormat::TF_BC2_UNORM:
    case CTextureFormat::TF_BC2_UNORM_SRGB:
    case CTextureFormat::TF_BC3_TYPELESS:
    case CTextureFormat::TF_BC3_UNORM:
    case CTextureFormat::TF_BC3_UNORM_SRGB:
    case CTextureFormat::TF_BC5_TYPELESS:
    case CTextureFormat::TF_BC5_UNORM:
    case CTextureFormat::TF_BC5_SNORM:
    case CTextureFormat::TF_BC6_TYPELESS:
    case CTextureFormat::TF_BC6_UF16:
    case CTextureFormat::TF_BC6_SF16:
    case CTextureFormat::TF_BC7_TYPELESS:
    case CTextureFormat::TF_BC7_UNORM:
    case CTextureFormat::TF_BC7_UNORM_SRGB:
		return true;        
    }

	return false;
}

_ENGINE_END

#endif