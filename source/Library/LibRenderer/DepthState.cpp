#include "librendererafx.h"
#include "DepthState.h"

using namespace engine;

CDepthState::CDepthState() 
	: 
m_enabled(true),
m_writable(true),
m_compareMode(CCompareMode::CM_LEQUAL)
{


}

CDepthState::CDepthState(bool enable, bool writable, CCompareMode::Type compareMode)
	:
m_enabled(enable),
m_writable(writable),
m_compareMode(compareMode)
{


}

CDepthState::~CDepthState()
{

}