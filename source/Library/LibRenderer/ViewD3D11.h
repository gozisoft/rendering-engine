#pragma once

#ifndef __CVIEW_D3D11_H__
#define __CVIEW_D3D11_H__

#include "D3D11Fwd.h"
#include "IView.h"

_ENGINE_BEGIN

class CShaderResourceD3D11 : public IShaderResource
{
public:
	// Constructor
	CShaderResourceD3D11(ID3D11ShaderResourceViewPtr view);

	// Destructor to auto release the object
	~CShaderResourceD3D11();

	// Access to the type flags
	CViewType::Type type() const override;

	// Function to get shader resource creation info
	ShaderResourceInfo info() const override;

	// Access to the underlying ID3D11ShaderResourceView
	ID3D11ShaderResourceViewPtr get() const;

private:
	// Direct3D view
	ID3D11ShaderResourceViewPtr m_view;

	// Type of view
	CViewType::Type m_type;
};

class CRenderTargetD3D11 : public IRenderTarget
{
public:
	// Constructor for a back buffer
	CRenderTargetD3D11(ID3D11RenderTargetViewPtr view);

	// Destructor to auto release the object
	~CRenderTargetD3D11();

	// Access to the type flags
	CViewType::Type type() const override;

	// Function to get render target view creation info
	RenderTargetInfo info() const override;

	// Access to the underlying ID3D11ShaderResourceView
	ID3D11RenderTargetViewPtr get() const;

private:
	// Direct3D view
	ID3D11RenderTargetViewPtr m_view;

	// Type of view
	CViewType::Type m_type;
};

class CDepthStencilD3D11 : public IDepthStencil
{
public:
	// Constructor for a back buffer
	CDepthStencilD3D11(ID3D11DepthStencilViewPtr view);

	// Destructor to auto release the object
	~CDepthStencilD3D11();

	// Access to the type flags
	CViewType::Type type() const override;

	// Function to get render target view creation info
	DepthStencilInfo info() const override;

	// Access to the underlying ID3D11ShaderResourceView
	ID3D11DepthStencilViewPtr get() const;

private:
	ID3D11DepthStencilViewPtr m_view;
	CViewType::Type m_type;
};

#include "ViewD3D11.inl"

_ENGINE_END

#endif


//template < class Parent >
//class TView : public Parent
//{	
//public:
//	// Typedef for this types RTTI
//	typedef TView<Parent> this_type;
//
//	// Constructor
//	TView(CViewType::Type type, IResource* resource);
//
//	// Destructor
//	virtual ~TView();
//
//	// Access to the type flags
//	CViewType::Type type() const;
//
//	// Access the resource
//	IResource* GetResource() const;
//
//protected:
//	// Type of buffer
//	CViewType::Type m_type;
//
//	// Pointer to the resource the view
//	// represents.
//	IResource* m_resource;
//};
//
//template < class Parent >
//TView<Parent>::TView(CViewType::Type type, IResource* resource) :
//m_type(type),
//m_resource(resource)
//{
//	assert(m_resource != nullptr);
//}
//
//template < class Parent >
//TView<Parent>::~TView()
//{
//
//}
//
//template < class Parent >
//CViewType::Type TView<Parent>::type() const
//{
//	return m_type;
//}
//
//template < class Parent >
//IResource* TView<Parent>::GetResource() const
//{
//	return m_resource;
//}