#include "librendererafx.h"
#include "PipelineD3D11.h"
#include "ResourceD3D11.h"
#include "ViewD3D11.h"
#include "ShaderD3D11.h"
#include "InputFormatD3D11.h"
#include "RasterizerStateD3D11.h"
#include <vector>
#include <stdexcept>

using namespace engine;

//-------------------------------------------------------------------------------------------------------------------
PipelineD3D11::PipelineD3D11(ID3D11DeviceContextPtr context) :
m_context(context)
{

}
//-------------------------------------------------------------------------------------------------------------------
PipelineD3D11::~PipelineD3D11()
{

}
//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::Clear(const IRenderTarget* renderTarget, const float* rgba)
{
	// Down cast the pointer
	auto pRenderTargetD3D = PolymorphicDowncast<const CRenderTargetD3D11*>(renderTarget);
	if (!pRenderTargetD3D)
		throw std::runtime_error("Invalid renderTarget type PipelineD3D11::Clear"); 

	// Clear the render target view
	m_context->ClearRenderTargetView(pRenderTargetD3D->get(), (FLOAT*)rgba);
}
//-------------------------------------------------------------------------------------------------------------------
inline void PipelineD3D11::Clear(IRenderTargetPtr renderTarget, float const* rgba)
{
	// Down cast the pointer
	auto pRenderTargetD3D
		= PolymorphicPointerDowncast<CRenderTargetD3D11>(renderTarget);
	if (!pRenderTargetD3D)
		throw std::runtime_error("Invalid renderTarget type PipelineD3D11::Clear");

	// Clear the render target view
	m_context->ClearRenderTargetView(pRenderTargetD3D->get(), (FLOAT*)rgba);
}
//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::Clear(const IDepthStencil* depthStencil, uint32_t stateFlags, float depth,
		uint8_t stencil)
{
	auto pDepthStencilD3D
		= PolymorphicDowncast<const CDepthStencilD3D11*>(depthStencil);

	m_context->ClearDepthStencilView(pDepthStencilD3D->get(), stateFlags, depth, stencil);
}

void PipelineD3D11::enableRasterState(const IRasterizerState* state)
{
	auto pStateD3D
		= PolymorphicDowncast<const RasterizerStateD3D11*>(state);
	m_context->RSSetState(pStateD3D->get());
}

//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::enableInputLayout(IInputFormat const* inputformat)
{
	auto inputFormatD3D = PolymorphicDowncast<const InputFormatD3D11*>(inputformat);
	if (inputFormatD3D == nullptr)
		throw std::runtime_error("A member IInputFormat* inputformat is of incorrect type [IPipeline::Enable]");

	m_context->IASetInputLayout(inputFormatD3D->get());
}
//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::enableVertexBuffers(const IBuffer** vbuffers, size_t numbuffers, size_t startslot, 
	size_t* strides, size_t* offsets)
{
	// Verify the buffers are there
	if (!vbuffers)
		throw std::runtime_error("Invalid vertex buffers PipelineD3D11::enableVertexBuffers"); 

	// Check start slot is less than cap
	if ( !(startslot < D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT) )
		throw std::runtime_error("startslot is higher than D3D11 slot count [PipelineD3D11::enableVertexBuffers]");

	// Check the number of buffers is not higher than the max buffer cap
	size_t maxBufferCount = D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT - startslot;
	if (numbuffers > maxBufferCount)
		throw std::runtime_error("numbuffers is higher than D3D11 max buffer count [PipelineD3D11::enableVertexBuffers]");

	// Fill an array of D3D11 buffers
	std::vector<ID3D11Buffer*> buffers(numbuffers);
	for (size_t i = 0; i < numbuffers; ++i)
	{
		auto pBufferD3D = PolymorphicDowncast<const CBufferD3D11*>(vbuffers[i]);
#if defined DEBUG || _DEBUG
		if (!pBufferD3D)
			throw std::runtime_error("A member IBuffer** vbuffers is of incorrect type [IPipeline::Enable]");
#endif

		// Assign one buffer at a time :(
		buffers[i] = pBufferD3D->get();
	}

	m_context->IASetVertexBuffers(startslot, numbuffers, &buffers[0], strides, offsets);
}

void PipelineD3D11::enableVertexBuffers(std::vector<BufferDesc> const& descriptors, size_t startslot)
{
	// Check start slot is less than cap
	if (!(startslot < D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT))
		throw std::runtime_error("startslot is higher than D3D11 slot count [PipelineD3D11::enableVertexBuffers]");

	// Get the number of descriptors	
	auto numBuffers = descriptors.size();

	// Check the number of buffers is not higher than the max buffer cap
	auto maxBufferCount = D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT - startslot;
	if (numBuffers > maxBufferCount)
		throw std::runtime_error("numbuffers is higher than D3D11 max buffer count [PipelineD3D11::enableVertexBuffers]");

	// Fill an array of D3D11 buffers
	std::vector<ID3D11Buffer*> buffers(numBuffers);
	std::vector<UINT> strides(numBuffers);
	std::vector<UINT> offsets(numBuffers);
	for (size_t i = 0; i < numBuffers; ++i) {
		auto& desc = descriptors[i];
		auto pBufferD3D = PolymorphicDowncast<const CBufferD3D11*>(desc.bufferPtr.get());
#if defined DEBUG || _DEBUG
		if (!pBufferD3D) {
			throw std::runtime_error(R"(A member IBuffer** vbuffers
					is of incorrect type [IPipeline::Enable])");
		}
#endif
		// Assign one buffer at a time :(
		buffers[i] = pBufferD3D->get();
		strides[i] = desc.stride;
		offsets[i] = desc.offset;
	}

	m_context->IASetVertexBuffers(startslot, numBuffers, &buffers[0], &strides[0], &offsets[0]);
}

//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::enableIndexBuffer(const IBuffer* ibuffer, size_t stide, size_t offset)
{
	auto pBufferD3D = PolymorphicDowncast<const CBufferD3D11*>(ibuffer);
	if (!pBufferD3D)
		throw std::runtime_error("A member IBuffer** vbuffers is of incorrect type [IPipeline::Enable]");

	DXGI_FORMAT format;
	switch (stide)
	{
	case sizeof(uint16_t): format = DXGI_FORMAT_R16_UINT; break;
	case sizeof(uint32_t): format = DXGI_FORMAT_R32_UINT; break;
	default:
		throw std::runtime_error("Index buffer has unknown stride [IPipeline::Enable]");
		break;
	}

	m_context->IASetIndexBuffer(pBufferD3D->get(), format, offset);
}
//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::enableConstantBuffer(const IBuffer**cbuffers, size_t numbuffers, size_t startslot)
{
	// Verify the buffers are there
	if (!cbuffers)
		throw std::runtime_error("Invalid constant buffers IPipeline::Enable");

	// Check start slot is less than cap
	if (!(startslot < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT))
		throw std::runtime_error("startslot is higher than D3D11 slot count [IPipeline::Enable]");

	// Check the number of buffers is not higher than the max buffer cap
	size_t maxBufferCount = D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT - startslot;
	if (numbuffers > maxBufferCount)
		throw std::runtime_error("numbuffers is higher than D3D11 max buffer count [IPipeline::Enable]");

	// Fill an array of D3D11 buffers
	std::vector<ID3D11Buffer*> buffers(numbuffers);
	for (size_t i = 0; i < numbuffers; ++i)
	{
		auto pBufferD3D = PolymorphicDowncast<const CBufferD3D11*>(cbuffers[i]);
		if (!pBufferD3D)
			throw std::runtime_error("A member IBuffer** vbuffers is of incorrect type [IPipeline::Enable]");

		// Assign one buffer at a time :(
		buffers[i] = pBufferD3D->get();
	}

	m_context->VSSetConstantBuffers(startslot, numbuffers, &buffers[0]);
}

void PipelineD3D11::enableConstantBuffer(std::vector<shared_ptr<IBuffer>> const& cbuffers, size_t startslot) {
	
	// Check start slot is less than cap
	if (!(startslot < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT))
		throw std::runtime_error("startslot is higher than D3D11 slot count [IPipeline::Enable]");

	// Get the number of constatn buffers.
	auto numbuffers =  cbuffers.size();

	// Check the number of buffers is not higher than the max buffer cap
	size_t maxBufferCount = D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT - startslot;
	if (numbuffers > maxBufferCount) {
		throw std::runtime_error(R"(numbuffers is higher than D3D11 
			max buffer count [IPipeline::Enable])");
	}

	// Fill an array of D3D11 buffers
	std::vector<ID3D11Buffer*> buffers(numbuffers);
	for (size_t i = 0; i < numbuffers; ++i) {
		auto pBufferD3D = PolymorphicDowncast<const CBufferD3D11*>(cbuffers[i].get());
		if (!pBufferD3D) {
			throw std::runtime_error(R"(A member IBuffer** vbuffers is of incorrect type
				 [IPipeline::Enable])");
		}

		// Assign one buffer at a time :(
		buffers[i] = pBufferD3D->get();
	}

	m_context->VSSetConstantBuffers(startslot, numbuffers, &buffers[0]);
}

//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::enableShader(const IShader* vshader)
{
	auto pShader = PolymorphicDowncast<const CShaderD3D11*>(vshader);
	if (!pShader)
		throw std::runtime_error("IShader* is of incorrect type [IPipeline::Enable]");

	// Access to the vtable
	auto table = pShader->GetTable();

	// Set the shader via the vtable
	ID3D11DeviceContext* pContext = m_context;
	(pContext->*(table->pSetShader))(pShader->Get(), nullptr, 0);
}
//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::setInputDrawingMode()
{
	m_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}
//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::SetViewports(const ViewPort* viewports, size_t numViewPorts) {
	// DirectX viewports are in left-handed screen coordinates.  The origin
	// is the upper-left corner, the y-axis points downward, and the x-axis
	// points upward.  We need a transformation in y to meet the Wild Magic
	// convention of a right-handed view port with origin in the lower-left
	// corner.
	std::vector<D3D11_VIEWPORT> viewportsD3D(numViewPorts);
	for (size_t i = 0; i < numViewPorts; ++i) {
		const ViewPort& viewPort = viewports[i];
		D3D11_VIEWPORT& viewPortD3D = viewportsD3D[i];
		viewPortD3D.TopLeftX = viewPort.posX();
		viewPortD3D.TopLeftY = viewPort.posY();
		viewPortD3D.Width = viewPort.width();
		viewPortD3D.Height = viewPort.height();
		viewPortD3D.MinDepth = viewPort.minZ();
		viewPortD3D.MaxDepth = viewPort.maxZ();
	}

	m_context->RSSetViewports(viewportsD3D.size(), &viewportsD3D[0]);
}
//-------------------------------------------------------------------------------------------------------------------
void engine::PipelineD3D11::setRenderTargets(const IRenderTarget** renderTargets, size_t num, const IDepthStencil* depthStencil)
{
	// Fill an array render targets, if there are any...
	std::vector<ID3D11RenderTargetView*> renderTargetsD3D11(num);
	for (size_t i = 0; i < num; ++i)
	{
		auto renderTargetD3D11
			= PolymorphicDowncast<const CRenderTargetD3D11*>(renderTargets[i]);		
		renderTargetsD3D11[i] = renderTargetD3D11->get();
	}

	ID3D11DepthStencilView* depthStencilView = nullptr;
	if (depthStencil) {
		auto depthStencilD3D11 = PolymorphicDowncast<const CDepthStencilD3D11*>(depthStencil);
		depthStencilView = depthStencilD3D11->get();
	}
	
	m_context->OMSetRenderTargets(static_cast<UINT>(num), &renderTargetsD3D11[0], depthStencilView);
}

void PipelineD3D11::setRenderTargets(std::vector<shared_ptr<IRenderTarget>> const& renderTargets, 
	shared_ptr<IDepthStencil> const& depthStencil) {
	// Fill an array render targets, if there are any...
	auto numRenderTargets = renderTargets.size();
	std::vector<ID3D11RenderTargetView*> renderTargetsD3D11(numRenderTargets);
	for each (const auto& renderTarget in renderTargets)
	{
		auto renderTargetD3D11
			= PolymorphicDowncast<const CRenderTargetD3D11*>(renderTarget.get());		
		renderTargetsD3D11.push_back(renderTargetD3D11->get());
	}

	ID3D11DepthStencilView* depthStencilView = nullptr;
	if (depthStencil != nullptr) {
		auto depthStencilD3D11 = PolymorphicDowncast<const CDepthStencilD3D11*>(depthStencil.get());
		depthStencilView = depthStencilD3D11->get();
	}
	
	m_context->OMSetRenderTargets(static_cast<UINT>(numRenderTargets),
		&renderTargetsD3D11[0], depthStencilView);
}

//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::drawIndexd(size_t indexCount, size_t startIndex)
{
	m_context->DrawIndexed(indexCount, startIndex, 0);
}

//-------------------------------------------------------------------------------------------------------------------
void PipelineD3D11::draw(size_t count, size_t startVertex)
{
	m_context->Draw(count, startVertex);
}







//void* PipelineD3D11::Map(IBuffer *pResource, CResourceLock::Type lock, size_t& alignedSize) const
//{
//	// Ensure pResource is valid
//	if (!pResource)
//		throw std::runtime_error("IResource is an invalid pointer [IPipeline::Map]");
//	
//	// map directly
//	D3D11_MAP mapType;
//
//	// Check if is either staging or dyanmic
//	switch ( pResource->GetUsage() )
//	{
//	case CResourceAccess::RA_DYNAMIC: // Dynamic resource can write only
//		switch (lock)
//		{
//		case CResourceLock::RL_DISCARD: mapType = D3D11_MAP_WRITE_DISCARD; break;
//		case CResourceLock::RL_WRITE_ONLY: mapType = D3D11_MAP_WRITE; break;
//		case CResourceLock::RL_NO_OVERWRITE: mapType = D3D11_MAP_WRITE_NO_OVERWRITE; break;
//		default:
//			throw std::runtime_error("Incorrect lock mode for use of dynamic buffer");
//			return nullptr;
//		} 
//		break;
//	case CResourceAccess::RA_STAGING: // Staging resource can read or write	
//		switch (lock)
//		{
//		case CResourceLock::RL_READ_ONLY: mapType = D3D11_MAP_READ; break;
//		case CResourceLock::RL_READ_WRITE: mapType = D3D11_MAP_READ_WRITE; break;
//		case CResourceLock::RL_DISCARD: mapType = D3D11_MAP_WRITE_DISCARD; break;
//		case CResourceLock::RL_WRITE_ONLY: mapType = D3D11_MAP_WRITE; break;
//		case CResourceLock::RL_NO_OVERWRITE: mapType = D3D11_MAP_WRITE_NO_OVERWRITE; break;
//		default:
//			throw std::runtime_error("Incorrect lock mode for use of dynamic or staging buffer");
//			return nullptr;
//		} 
//		break;
//	}; // switch
//
//	// Find the resource type
//	CBufferD3D11* pBufferD3D = PolymorphicDowncast<CBufferD3D11*>(pResource);
//	if (!pBufferD3D)
//		throw std::runtime_error("IResource is unknown type [IPipeline::Map]");
//
//	// Get the resource, whichever it may be...
//	ID3D11Resource* pRes = pBufferD3D->Get();
//
//	// Map the resource
//	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
//	HRESULT hr = m_context->Map(pRes, 0, mapType, 0, &mappedSubResource);
//	if ( FAILED(hr) )
//		throw CD3D11Exception(hr);
//
//	// Asign the aligned size
//	alignedSize = static_cast<size_t>(mappedSubResource.RowPitch);
//
//	// return the data
//	return mappedSubResource.pData;
//}
//
//void* PipelineD3D11::UnMap(IBuffer *pResource) const
//{
//	// Ensure pResource is valid
//	if (!pResource)
//		throw std::runtime_error("IResource is an invalid pointer [IPipeline::Map]");
//
//	CBufferD3D11* pBufferD3D = PolymorphicDowncast<CBufferD3D11*>(pResource);
//	if (!pBufferD3D)
//		throw std::runtime_error("A member IBuffer** vbuffers is of incorrect type [IPipeline::Enable]");
//
//	// Call unmap from the immediate context
//	m_context->Unmap(pBufferD3D->Get(), 0);
//}
//
//void* PipelineD3D11::Map(ITexture *pResource, CResourceLock::Type lock, size_t& rowPitch, size_t& depthSlice) const
//{
//
//
//
//}
//
//// Close access to the data of a texture
//void* PipelineD3D11::UnMap(ITexture *pResource, size_t subresource) const
//{
//	ID3D11Resource* pRes = nullptr;
//	switch ( pResource->type() )
//	{
//	case CResourceType::RT_TEXTURE1D:
//		pRes = PolymorphicDowncast<CTexture1DD3D11*>(pResource)->Get(); 
//		break;
//	case CResourceType::RT_TEXTURE2D:
//		pRes = PolymorphicDowncast<CTexture2DD3D11*>(pResource)->Get(); 
//		break;
//	};
//
//
//
//
//
//}