//
// Created by Riad Gozim on 21/12/2015.
//

#pragma once

#ifndef EXCEPTIONGL_H
#define EXCEPTIONGL_H

#include "Core.h"
#include "OpenGLFwd.h"
#include <string>
#include <exception>

_ENGINE_BEGIN

class opengl_error: public std::runtime_error
{
public:
    explicit opengl_error(GLenum error) throw();
    explicit opengl_error(const std::string& error) throw();

    virtual ~opengl_error() throw();

    virtual const char *what() const throw();

protected:
    const char *getErrorString(GLenum error) const;

private:
    const std::string mywhat;
};

_ENGINE_END

#endif //ENGINE_EXCEPTIONGL_H
