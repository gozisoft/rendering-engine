#pragma once
#ifndef __CBUFFER_IO_H__
#define __CBUFFER_IO_H__

#include "Core.h"

_ENGINE_BEGIN

class CBufferIO
{
public:
	enum BufferMode
	{
		BM_NONE = 0,
		BM_READ,
		BM_WRITE,
		BM_READ_AND_SWAP,
		BM_WRITE_AND_SWAP,

		// All data files are in little endian format, because most platforms
		// these days are little endian.  The BufferIO class is designed for a
		// block load of an entire file followed by parsing the buffer.
#ifdef LITTLE_ENDIAN
		BM_DEFAULT_READ = BM_READ,
		BM_DEFAULT_WRITE = BM_WRITE
#else
		BM_DEFAULT_READ = BM_READ_AND_SWAP,
		BM_DEFAULT_WRITE = BM_WRITE_AND_SWAP
#endif
	};

	// Construction and destruction.  The caller provides the memory for the
	// buffer, whether reading or writing, and is responsible for managing
	// that memory.  The buffer does not have to be dynamically allocated.
	// TODO:  Support writing where the total number of bytes to be written is
	// unknown.  This requires dynamic reallocation of the input buffer, and
	// accessors must be added, say, GetBuffer() and GetNumBytes().
	CBufferIO ();
	CBufferIO (BufferMode mode);
	CBufferIO (size_t numBytesTotal, char* buffer, BufferMode mode);
	~CBufferIO ();

	bool Open (size_t numBytesTotal, char* buffer, BufferMode mode);
	bool Close ();

	// Implicit conversion to allow testing for successful construction.
	operator bool () const;

	// Member access.
	const char* GetBuffer () const;
	size_t GetNumBytesTotal () const;
	size_t GetNumBytesProcessed () const;
	BufferMode GetMode () const;
	bool IncrementNumBytesProcessed (size_t numBytes);

	// The return value is 'true' if and only if the operation was
	// successful, in which case the number of bytes read or written is
	// the item size times number of items.
	bool Read (size_t itemSize, void* datum);
	bool Read (size_t itemSize, size_t numItems, void* data);
	bool Write (size_t itemSize, const void* datum);
	bool Write (size_t itemSize, size_t numItems, const void* data);

private:
	char* m_pBuffer;
	size_t m_numBytesTotal;
	size_t m_numBytesProcessed;
	BufferMode m_mode;
};

#include "BufferIO.inl"

_ENGINE_END

#endif