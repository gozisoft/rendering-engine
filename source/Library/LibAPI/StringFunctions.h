#ifndef __STRING_FUNCTIONS_H__
#define __STRING_FUNCTIONS_H__

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)

#include "Core.h"
#include <string>
#include <sstream>

_ENGINE_BEGIN

class StringUtils
{
public:
	static std::string GetExeDir();
	static std::string GetFullPath(const char* path);
	static std::string GetWorkingDirectory(const char* path);

	static std::wstring Widen(const std::string& str);
	static std::string Narrow(const std::wstring& str);

	static int AnsiToWide(wchar_t* dest, const char* src, int inputLen, int outputLen);
	static int WideToAnsi(char* dest, const wchar_t* src, int inputLen, int outputLen);
	static int GenericToAnsi(char* dest, const Char* src, int inputLen, int outputLen);
	static int AnsiToGeneric(Char* dest, const char* src, int inputLen, int outputLen);
};



//int GenericToWide(wchar_t* dest, const Char* src, int inputLen, int outputLen); 
//int WideToGeneric(Char* dest, const wchar_t* src, int charCount);



#include "StringFunctions.inl"

_ENGINE_END

#endif

#endif



	/*
	// AProgrammer response to widen a char to wchar_t
	// http://stackoverflow.com/questions/1791578/how-do-i-convert-a-char-string-to-a-wchar-t-string
	std::wstring Widen(const std::string& s, std::locale loc);
	*/