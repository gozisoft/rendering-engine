#pragma once
#ifndef __MEMORY_MANAGER_H__
#define __MEMORY_MANAGER_H__

#include "HeapFactory.h"

#ifdef USING_MEMORY_MGR

//using Engine::CHeapFactory;
//using Engine::CHeap;

void * operator new (size_t size);
void * operator new (size_t size, engine::CHeap * pHeap);
void operator delete (void * pMem); 

namespace engine
{

#define DECLARE_HEAP \
public: \
	static void * operator new(size_t size); \
	static void operator delete(void * p); \
private: \
	static CHeap * s_pHeap; 


#define DEFINE_HEAP(className,heapName) \
	CHeap * className::s_pHeap = NULL; \
	void * className::operator new(size_t size) { \
	if (s_pHeap==NULL)  \
	s_pHeap = CHeapFactory::CreateHeap(heapName); \
	return ::operator new(size, s_pHeap); \
} \
	void className::operator delete(void * p) { \
	::operator delete(p); \
}


#define DEFINE_HIERARCHICALHEAP(className,heapName,parentName) \
	CHeap * className::s_pHeap = NULL; \
	void * className::operator new(size_t size) { \
	if (s_pHeap==NULL)  \
	s_pHeap = CHeapFactory::CreateHeap(heapName,parentName); \
	return ::operator new(size, s_pHeap); \
} \
	void className::operator delete(void * p) { \
	::operator delete(p); \
}


}

#else

#define DECLARE_HEAP
#define DEFINE_HEAP(className,heapName)
#define DEFINE_HIERARCHICALHEAP(className,heapName,parentName)

#endif // debug


#endif // __MEMORYMGR_H__