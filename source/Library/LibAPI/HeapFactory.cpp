#include "libapiafx.h"
#include "HeapFactory.h"
#include <cassert>
#include <cstring>
#include <cstdio>


#ifdef USING_MEMORY_MGR

using namespace engine;

CHeap * CHeapFactory::s_pRootHeap    = NULL;
CHeap * CHeapFactory::s_pDefaultHeap = NULL;
CHeap CHeapFactory::s_heaps[];


void CHeapFactory::Initialize()
{   
	for (int i=0; i < MAXHEAPS; ++i)
		s_heaps[i].Initialize ();

	s_pRootHeap    = CreateNewHeap("Root");
	s_pDefaultHeap = CreateHeap("Default");
}



CHeap * CHeapFactory::GetRootHeap ()
{
	if (s_pRootHeap == NULL)
		Initialize();
	return s_pRootHeap;
}


CHeap * CHeapFactory::GetDefaultHeap ()
{
	if (s_pDefaultHeap == NULL)
		Initialize();
	return s_pDefaultHeap;
}



CHeap * CHeapFactory::CreateHeap (const char * name, const char * parent)
{
	if (s_pRootHeap == NULL)
		Initialize();

	CHeap * pParent = FindHeap(parent);
	if (pParent == NULL)
	{
		pParent = CreateNewHeap(parent);
		pParent->AttachTo (s_pRootHeap);
	}

	CHeap * pHeap = FindHeap(name);
	if (pHeap == NULL)
		pHeap = CreateNewHeap(name);

	pHeap->AttachTo (pParent);
	return pHeap;
}



CHeap * CHeapFactory::CreateHeap (const char * name)
{
	return CreateHeap(name, "Root");
}


CHeap * CHeapFactory::FindHeap (const char * name)
{
	for (int i=0; i < MAXHEAPS; ++i)
	{
		CHeap * pHeap = &s_heaps[i];
		if (pHeap->IsActive() && !_stricmp(name, pHeap->GetName()))
			return pHeap;
	}

	return NULL;
}


CHeap * CHeapFactory::CreateNewHeap (const char * name)
{
	for (int i=0; i < MAXHEAPS; ++i)
	{
		CHeap * pHeap = &s_heaps[i];
		if (!pHeap->IsActive())
		{
			pHeap->Activate (name);
			return pHeap;
		}
	}

	return NULL;
}



void CHeapFactory::PrintInfo ()
{
	printf ("MEMORY INFORMATION\n");
	printf ("                            Local                 Total\n");
	printf ("Name                  Memory  Peak  Inst   Memory  Peak  Inst\n");
	GetRootHeap()->PrintTreeInfo(0);
	/*
	for (int i=0; i < MAXHEAPS; ++i)
	{
	if (s_heaps[i].IsActive())
	s_heaps[i].PrintInfo();
	}
	*/
	printf ("\n");
}



int CHeapFactory::GetMemoryBookmark () 
{
	return CHeap::GetMemoryBookmark();
}


void CHeapFactory::ReportMemoryLeaks (int nBookmark) 
{
	ReportMemoryLeaks (nBookmark, GetMemoryBookmark ());
}


void CHeapFactory::ReportMemoryLeaks (int nBookmark1, int nBookmark2) 
{
	int nLeaks = 0;
	for (int i=0; i < MAXHEAPS; ++i)
		if (s_heaps[i].IsActive())
			nLeaks += s_heaps[i].ReportMemoryLeaks(nBookmark1, nBookmark2);

	if (nLeaks > 0)
		printf ("%d memory leaks found\n", nLeaks);
	else
		printf ("No memory leaks detected.\n");
}





#endif