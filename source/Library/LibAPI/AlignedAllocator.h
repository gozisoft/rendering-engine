#pragma once
#ifndef __ALIGNED_ALLOCATOR_H__
#define __ALIGNED_ALLOCATOR_H__

#include "Core.h"
#include <cassert>

#if defined(__GNUG__)
    #include <mm_malloc.h>
#else
    #include <malloc.h>
#endif

_ENGINE_BEGIN

// Computing padding 
// http://en.wikipedia.org/wiki/Data_structure_alignment
inline size_t AlignToPowerOf2(size_t value, size_t alignment) {
    // to align to 2^N, add 2^N - 1 and AND with all but lowest N bits set
    assert((alignment & (alignment - 1)) == 0);
    return (value + alignment - 1) & (~(alignment - 1));
}

// Function to check alignment
inline bool IsAligned(const void* data, size_t alignment) {
    // check that the alignment is a power of two
    assert((alignment & (alignment - 1)) == 0);
    return ((uintptr_t) data & (alignment - 1)) == 0;
}

#if ( (_MSC_VER >= 1500) || (__STDC_VERSION__ >= 201112L) )
// alligned_malloc and alligned_free
// The "alignment" must be a power of two!

inline void* aligned_malloc(size_t size, size_t alignment) {
    return ::_aligned_malloc(size, alignment);
}

inline void aligned_free(void* aligned) {
    ::_aligned_free(aligned);
}

inline void* aligned_realloc(void* memory, size_t size, size_t alignment) {
    return ::_aligned_realloc(memory, size, alignment);
}
#elif (__GNUG__) 
// The "alignment" must be a power of 2!
inline void* aligned_malloc(size_t size, size_t alignment) {
    return _mm_malloc(size, alignment);
}

inline void* aligned_realloc(void* memory, size_t size, size_t alignment) {
    return realloc(memory, size);
}

void aligned_free(void* aligned) {
	void* raw = *(void**)((char*)aligned - sizeof (void*));
	free(raw);
}
#else
// alligned_malloc and alligned_free
// The "alignment" must be a power of two!
void* aligned_malloc(size_t size, size_t alignment) {
    // check that the alignment is a power of two
    assert((alignment & (alignment - 1)) == 0);

    // - We don't know what alignment malloc will return, so we need to account for the worst case.
    // That means that our block has to be large enough to hold the requested 
    // allocation size, one pointer, and up to the full alignment size (minus one). 
    const size_t worstCase = alignment - 1;
    const size_t pointerSize = sizeof (uintptr_t);
    const size_t requestedSize = size + worstCase + pointerSize;

    // Allocate the memory
    void* raw = malloc(requestedSize);

    // Pointer adjust to start
    void* start = static_cast<void*> (static_cast<char*> (raw) + pointerSize);

    // Align the pointer
    void* aligned = reinterpret_cast<void*> (reinterpret_cast<size_t> (static_cast<char*> (start) + worstCase) & ~(worstCase));
    *(void**) ((char*) aligned - pointerSize) = raw;
    return aligned;
}

void aligned_free(void* aligned) {
	void* raw = *(void**)((char*)aligned - sizeof (void*));
	free(raw);
}
#endif



template<class T, size_t TALIGN>
class AlignedAllocator {
public:
    typedef T value_type;

    typedef value_type* pointer;
    typedef value_type& reference;
    typedef const value_type* const_pointer;
    typedef const value_type& const_reference;

    typedef size_t size_type;
    typedef ptrdiff_t difference_type;

    // convert an AlignedAllocator<T> to an AlignedAllocator <_Other>

    template<class _Other>
    struct rebind {
        typedef AlignedAllocator<_Other, TALIGN> other;
    };

    // return address of mutable _Val

    pointer address(reference _val) const {
        return ((pointer) &(char&) _val);
    }

    // return address of nonmutable _Val

    const_pointer address(const_reference _val) const {
        return ((const_pointer) &(char&) _val);
    }

    // construct default AlignedAllocator (do nothing)

    AlignedAllocator() throw () {
    }

    /* construct by copying (do nothing) */
    AlignedAllocator(const AlignedAllocator<T, TALIGN>&) throw () {
    }

    template<class _Other>
    AlignedAllocator(const AlignedAllocator<_Other, TALIGN>&) throw () {
        // construct from a related AlignedAllocator (do nothing)
    }

    /* assign from a related AlignedAllocator (do nothing) */
    template<class _Other>
    AlignedAllocator<T, TALIGN>& operator=(const AlignedAllocator<_Other, TALIGN>&) {
        return (*this);
    }

    // deallocate object at _Ptr, ignore size

    void deallocate(pointer _ptr, size_type) {
        if (_ptr) aligned_free(_ptr);
    }

    // allocate array of _Count elements

    pointer allocate(size_type _count) {
        return allocate(_count, const_pointer(0));
    }

    pointer allocate(size_type _count, const_pointer /* hint */) {
        if (max_size() < _count)
            throw std::bad_alloc("bad allocation in the custom aligned allocator");

        // allocate array of _Count elements, ignore hint
        if (_count > 0) {
            size_t byteCount = _count * sizeof (T);
            void* p = aligned_malloc(byteCount, TALIGN);
            if (p == NULL)
                throw std::bad_alloc("bad allocation in the custom aligned allocator");
            
            return p;
        }

        return NULL;
    }

    // Construct object at _ptr with value _val
    void construct(pointer _ptr, const T& _val) {
        new(_ptr) value_type(_val);
    }

    // Destroy object at _ptr
    void destroy(pointer _ptr) {
        _ptr->~value_type();
    }

    // Estimate maximum array size
    size_type max_size() const {
        size_type _count = (size_type) (-1) / sizeof (value_type);
        return (0 < _count ? _count : 1);
    }
};

_ENGINE_END


#endif