#ifndef __CRTTI_INL__
#define __CRTTI_INL__

namespace detail {

	template< class T > 
	struct has_member_function_getRtti
	{
		template<const CRTTI& (T::*)() const> struct tester; 
		template<class U> static small_type has_func(tester<&U::getRtti> *); 
		template<class U> static large_type has_func(...);
		static const bool value = (sizeof(has_func<T>(0)) == sizeof(small_type));
	};

	template< class T > 
	struct has_member_variable_rtti
	{
		template<const CRTTI*> struct tester; 
		template<class U> static small_type has_func(tester<&U::rtti> *); 
		template<class U> static large_type has_func(...);
		static const bool value = (sizeof(has_func<T>(0)) == sizeof(small_type));
	};

	// Enabled if the type is an object that contains CRTTI
	template < class T >
	const CRTTI& typeid_engine(const T& object)
	{
		return object.getRtti();
	};

	// Enabled if the type is an object that does not contain CRTTI
	template < class T >
	const std::type_info& typeid_default(const T& object)
	{
		return typeid(object);
	};

	template< class Target, class Source >
	typename std::enable_if<std::is_pointer<Target>::value, Target>::type 
		dynamic_cast_impl (Source src, typename std::enable_if<std::is_pointer<Source>::value, Source>::type* dummy = 0)
	{
		UNUSED_PARAMETER(dummy);

		// Typedef to get the original type
		typedef typename std::remove_pointer<Target>::type target_type;

		// Perform the operation
		return src->getRtti().derives_from(target_type::rtti)  ? static_cast<Target>(src) : nullptr;
	}

	template< class Target, class Source >
	typename std::enable_if<std::is_reference<Target>::value, Target>::type 
		dynamic_cast_impl (Source src, typename std::enable_if<std::is_reference<Source>::value, Source>::type* dummy = 0)
	{
		UNUSED_PARAMETER(dummy);

		// Typedef to get the original type
		typedef typename std::remove_reference<Target>::type target_type;

		// Perform the operation
		return src.getRtti().derives_from(target_type::rtti)  ? static_cast<Target>(src) : nullptr;
	}

	template< bool b > 
	struct dynamic_cast_selector
	{
		template< class Target, class Source >
		static Target implementation(Source src)
		{
			// Perform the operation
			return dynamic_cast<Target>(src);
		}
	};

	template<> 
	struct dynamic_cast_selector<true>
	{
		template< class Target, class Source >
		static Target implementation(Source src)
		{
			//return dynamic_cast<Target>(src);
			return dynamic_cast_impl<Target>(src);
		}
	};

} // namespace Detail

template < class T >
const CRTTI& Typeid(T&& object, typename std::enable_if<detail::has_member_function_getRtti<typename std::decay<T>::type>::value>::type* dummy = 0)
{
	UNUSED_PARAMETER(dummy);
	return detail::typeid_engine< typename std::decay<T>::type >( std::forward<T>(object) );
}

template < class T >
const std::type_info& Typeid(T&& object, typename std::enable_if<!detail::has_member_function_getRtti<typename std::decay<T>::type>::value>::type* dummy = 0)
{
	UNUSED_PARAMETER(dummy); 
	return detail::typeid_default< typename std::decay<T>::type >( std::forward<T>(object) );
}

template < class Target, class Source>
Target StaticCast (Source* src)
{
    return static_cast<Target>(src);
}

template < class Target, class Source >
const Target StaticCast (const Source* src)
{
    return static_cast<const Target>(src);
}

template< class Target, class Source >
Target DynamicCast(Source src)
{
//	return detail::dynamic_cast_selector<
//		detail::has_member_variable_rtti<typename decay_ref_ptr<Target>::type>::value
//		&& detail::has_member_function_getRtti<typename decay_ref_ptr<Source>::type>::value
//	>::implementation(src);

	return dynamic_cast<Target>(src);
}

template < class Target, class Source >
Target PolymorphicDowncast(Source x)
{
	assert( DynamicCast<Target>(x) == x );  // detect logic error
	return static_cast<Target>(x);
}

template< class Target, class Source > 
std::shared_ptr<Target> StaticPointerCast(std::shared_ptr<Source> const & r)
{
	typedef typename std::shared_ptr<Target>::element_type E;

	E * p = StaticCast<E*>(r.get());
	return std::shared_ptr<Target>(r, p);
}

template< class Target, class Source >
std::shared_ptr<Target> DynamicPointerCast(std::shared_ptr<Source> const & r)
{
//	typedef typename std::shared_ptr<Target>::element_type E;
//
//	E * p = DynamicCast<E*>(r.get());
//	return std::shared_ptr<Target>(r, p);
	return std::dynamic_pointer_cast<Target>(r);
}

template < class Target, class Source >
std::shared_ptr<Target> PolymorphicPointerDowncast(std::shared_ptr<Source> const & r)
{
	assert(DynamicPointerCast<Target>(r) == r);  // detect logic error
	return StaticPointerCast<Target>(r);
}

#endif



//// Solution here: http://stackoverflow.com/questions/3076206/enable-if-and-conversion-operator
//template< class Target >
//struct DynamicCast 
//{
//	template<class Source>
//	Target To (Source& src, typename std::enable_if< std::is_reference<Target>::value >::type = 0)
//	{
//		// Remove the pointer type of target
//		typedef typename std::remove_reference<Target>::type type;
//
//		// Perform the operation
//		return ( src && src->GetRtti().DerivesFrom(type::rtti) ) ? static_cast<Target>(src) : nullptr;
//	}
//
//	template<class Source>
//	Target To (Source* src, typename std::enable_if< std::is_pointer<Target>::value >::type = 0)
//	{
//		// Remove the pointer type of target
//		typedef typename std::remove_pointer<Target>::type type;
//
//		// Perform the operation
//		return ( src && src->GetRtti().DerivesFrom(type::rtti) ) ? static_cast<Target>(src) : nullptr;
//	}
//
//	template<class Source>
//	Target operator () (Source* src)
//	{
//		return To<Target>(src); 
//	}
//
//	//template<class Target, class Source>
//	//Target operator () (Source& src)
//	//{
//	//	return To<Target>(src); 
//	//}
//};

