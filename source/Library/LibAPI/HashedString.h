#pragma once
#ifndef __CHASHED_STRING_H__
#define __CHASHED_STRING_H__

#include "Core.h"
#include <string>

_ENGINE_BEGIN

#pragma warning(push)
#pragma warning(disable : 4311)

class CHashedString
{
public:
	explicit CHashedString(const char* pIdentString);

	// Copy constructor
	CHashedString(const CHashedString& other);

	// Assignment operator
	CHashedString& operator = (const CHashedString& other);

	uint32_t GetHashValue() const;

	const std::string & GetStr() const;

	static void* hash_name(const char* pIdentStr);

	bool operator < (const CHashedString& o) const;

	bool operator == (const CHashedString& o) const;

private:
	// note: m_ident is stored as a void* not an int, so that in
	// the debugger it will show up as hex-values instead of
	// integer values. This is a bit more representative of what
	// we're doing here and makes it easy to allow external code
	// to assign event types as desired.
	void* m_ident;
	std::string m_identStr;
};
//Remove the warning for warning #4311...

#pragma warning(pop)

_ENGINE_END

#endif