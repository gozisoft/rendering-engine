#pragma once
#ifndef __CTHUNK_H__
#define __CTHUNK_H__

#include "Core.h"
#include <mutex>

_ENGINE_BEGIN

#if defined(_M_IX86)

class CThunkBase
{
protected:
	CThunkBase();
	~CThunkBase();

	struct Bytecode
	{
#pragma pack(push, 1)
		unsigned short stub1;      // lea ecx, 
		unsigned long  nThisPtr;   // this
		unsigned char  stub2;      // mov eax,
		unsigned long  nJumpProc;  // pointer to destination function
		unsigned short stub3;      // jmp eax
#pragma pack(pop)
		Bytecode() :
			stub1	  (0x0D8D),
			nThisPtr  (0),
			stub2	  (0xB8),
			nJumpProc (0),
			stub3	  (0xE0FF)
		{}
	};

	Bytecode* bytecode;
	static void* volatile bytecodeHeap;
	
private:
	// Thunks will not be copyable
	CThunkBase(const CThunkBase&); // not implemented
	const CThunkBase& operator=(const CThunkBase&); // not implemented

	// Cleanup function
	static void __cdecl cleanupHeap();

	// Class uses a mutex to limit memory access
	std::mutex heapMutex;
};

#endif // _M_IX86

_ENGINE_END

#endif