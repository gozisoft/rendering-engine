#pragma once

#ifndef __IS_SMART_PTR_H__
#define __IS_SMART_PTR_H__

#include "api_traits.h"
#include "is_shared_ptr.h"
#include "is_unique_ptr.h"
#include "is_weak_ptr.h"

_ENGINE_BEGIN

template < class T >
class is_smart_ptr 
	:
	cat_base< is_std_shared_ptr<T>::value
	|| is_std_weak_ptr<T>::value
	|| is_std_weak_ptr<T>::value
	|| is_std_unique_ptr<T>::value >
{
};

_ENGINE_END

#endif