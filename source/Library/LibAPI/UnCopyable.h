#pragma once
#ifndef __UNCOPYABLE_H__
#define __UNCOPYABLE_H__

#include "Core.h"

_ENGINE_BEGIN

// Make a class non copyable
class UnCopyable
{
protected:
	UnCopyable () {}
	~UnCopyable () {} /// Protected non-virtual destructor

private: 
    UnCopyable(const UnCopyable&);
    const UnCopyable& operator = (const UnCopyable&);
};

_ENGINE_END

#endif