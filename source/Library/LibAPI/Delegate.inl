template< typename ArgsType >
Delegate< ArgsType >::Delegate()
{

}

template< typename ArgsType >
Delegate< ArgsType >::Delegate( const Delegate& rhs )
	: m_Impl( rhs.m_Impl )
{

}

template< typename ArgsType >
template < typename FunctionType >
Delegate< ArgsType >::Delegate( FunctionType function )
{
	m_Impl = new Function (function);
}

template< typename ArgsType >
template < class ClassType, typename MethodType >
Delegate< ArgsType >::Delegate( ClassType* instance, MethodType method )
{
	m_Impl = new Method<ClassType> (instance, method);
}

template< typename ArgsType >
template < typename FunctionType >
Delegate< ArgsType > Delegate< ArgsType >::Create( FunctionType function )
{
	return Delegate(function);
}

template< typename ArgsType >
template < class ClassType, typename MethodType >
Delegate< ArgsType > Delegate< ArgsType >::Create( ClassType* instance, MethodType method )
{
	return Delegate (instance, method);
}

template< typename ArgsType >
void Delegate< ArgsType >::Clear()
{
	m_Impl = NULL;
}

template< typename ArgsType >
bool Delegate< ArgsType >::Valid() const
{
	return m_Impl.ReferencesObject();
}

template< typename ArgsType >
void Delegate< ArgsType >::Set( const Delegate& delegate )
{
	m_Impl = delegate.m_Impl;
}

template< typename ArgsType >
template < typename FunctionType >
void Delegate< ArgsType >::Set( FunctionType function )
{
	m_Impl = new Function (function);
}

template< typename ArgsType >
template < class ClassType, typename MethodType >
void Delegate< ArgsType >::Set( ClassType* instance, MethodType method )
{
	m_Impl = new Method<ClassType> (instance, method);
}

template< typename ArgsType >
bool Delegate< ArgsType >::Equals( const Delegate& rhs ) const
{
	if (m_Impl.ReferencesObject() != rhs.m_Impl.ReferencesObject())
	{
		return false;
	}

	if (!m_Impl.ReferencesObject())
	{
		return false;
	}

	return m_Impl->Equals( rhs.m_Impl );
}

template< typename ArgsType >
template <typename FunctionType>
bool Delegate< ArgsType >::Equals( FunctionType function ) const
{
	if (m_Impl.ReferencesObject() && m_Impl->GetType() == DelegateTypes::Function)
	{
		Function* func = static_cast<Function*>(m_Impl.Ptr());

		return func->m_Function == function;
	}
	else
	{
		return false;
	}
}

template< typename ArgsType >
template< class ClassType, typename MethodType >
bool Delegate< ArgsType >::Equals( const ClassType* instance, MethodType method ) const
{
	if (m_Impl.ReferencesObject() && m_Impl->GetType() == DelegateTypes::Method)
	{
		Method<ClassType>* meth = static_cast<Method<ClassType>*>(m_Impl.Ptr());

		return meth->m_Instance == instance && meth->m_Method == method;
	}
	else
	{
		return false;
	}
}

template< typename ArgsType >
void Delegate< ArgsType >::Invoke( ArgsType parameter ) const
{
	if (m_Impl.ReferencesObject())
	{
		m_Impl->Invoke(parameter);
	}
}

template< typename ArgsType >
Delegate< ArgsType >::Function::Function( FunctionType function )
	: m_Function( function )
{
	HELIUM_ASSERT( function );
}

template< typename ArgsType >
DelegateType Delegate< ArgsType >::Function::GetType() const
{
	return DelegateTypes::Function;
}

template< typename ArgsType >
bool Delegate< ArgsType >::Function::Equals( const DelegateImpl* rhs ) const
{
	if ( GetType() != rhs->GetType() )
	{
		return false;
	}

	const Function* f = static_cast<const Function*>(rhs);

	return m_Function == f->m_Function;
}

template< typename ArgsType >
void Delegate< ArgsType >::Function::Invoke( ArgsType parameter ) const
{
	m_Function( parameter );
}

template< typename ArgsType >
template< class ClassType >
Delegate< ArgsType >::Method< ClassType >::Method( ClassType* instance, MethodType method )
	: m_Instance( instance )
	, m_Method( method )
{
	assert( instance );
	assert( method );
}

template< typename ArgsType >
template< class ClassType >
DelegateType Delegate< ArgsType >::Method< ClassType >::GetType() const
{
	return DelegateTypes::Method;
}

template< typename ArgsType >
template< class ClassType >
bool Delegate< ArgsType >::Method< ClassType >::Equals( const DelegateImpl* rhs ) const
{
	if ( GetType() != rhs->GetType() )
	{
		return false;
	}

	const Method* m = static_cast<const Method*>(rhs);

	return m_Instance == m->m_Instance && m_Method == m->m_Method;
}

template< typename ArgsType >
template< class ClassType >
void Delegate< ArgsType >::Method< ClassType >::Invoke( ArgsType parameter ) const
{
	(m_Instance->*m_Method)( parameter );
}

template< typename ArgsType >
uint32_t Event< ArgsType >::Count() const
{
	return m_Impl.ReferencesObject() ? (uint32_t)m_Impl->Count() : 0;
}

template< typename ArgsType >
bool Event< ArgsType >::Valid() const
{
	return Count() > 0;
}

template< typename ArgsType >
void Event< ArgsType >::Add( const Delegate& delegate )
{
	if ( !m_Impl.ReferencesObject() )
	{
		m_Impl = new EventImpl;
	}

	m_Impl->Add( delegate );
}

template< typename ArgsType >
template< typename FunctionType >
void Event< ArgsType >::AddFunction( FunctionType function )
{
	if ( !m_Impl.ReferencesObject() )
	{
		m_Impl = new EventImpl;
	}

	m_Impl->AddFunction( function );
}

template< typename ArgsType >
template< class ClassType, typename MethodType >
void Event< ArgsType >::AddMethod( ClassType* instance, MethodType method )
{
	if ( !m_Impl.ReferencesObject() )
	{
		m_Impl = new EventImpl;
	}

	m_Impl->AddMethod( instance, method );
}

template< typename ArgsType >
void Event< ArgsType >::Remove( const Delegate& delegate )
{
	if ( m_Impl.ReferencesObject() )
	{
		m_Impl->Remove( delegate );

		if (m_Impl->Count() == 0)
		{
			m_Impl = NULL;
		}
	}
}

template< typename ArgsType >
template< typename FunctionType >
void Event< ArgsType >::RemoveFunction( FunctionType function )
{
	if ( m_Impl.ReferencesObject() )
	{
		m_Impl->RemoveFunction( function );

		if (m_Impl->Count() == 0)
		{
			m_Impl = NULL;
		}
	}
}

template< typename ArgsType >
template< class ClassType, typename MethodType >
void Event< ArgsType >::RemoveMethod( const ClassType* instance, MethodType method )
{
	if ( m_Impl.ReferencesObject() )
	{
		m_Impl->RemoveMethod( instance, method );

		if (m_Impl->Count() == 0)
		{
			m_Impl = NULL;
		}
	}
}

template< typename ArgsType >
void Event< ArgsType >::Raise( ArgsType parameter )
{
	if ( m_Impl != nullptr )
	{
		// hold a pointer on the stack in case the object we are aggregated into deletes inside this function
		// use impl and not m_Impl in case _we_ are deleted and m_Impl is trashed
		std::shared_ptr<EventImpl> impl = m_Impl;

		return impl->Raise( parameter, Delegate () );
	}

	return void ();
}

template< typename ArgsType >
void Event< ArgsType >::RaiseWithEmitter( ArgsType parameter, const Delegate& emitter )
{
	if ( m_Impl != nullptr )
	{
		// hold a pointer on the stack in case the object we are aggregated into deletes inside this function
		// use impl and not m_Impl in case _we_ are deleted and m_Impl is trashed
		std::shared_ptr<EventImpl> impl = m_Impl;

		return impl->Raise( parameter, emitter );
	}

	return void ();
}

template< typename ArgsType >
Event< ArgsType >::EventImpl::EventImpl()
	: m_EntryCount (0)
	, m_EmptySlots (0)
{

}

template< typename ArgsType >
uint32_t Event< ArgsType >::EventImpl::Count() const
{
	return (uint32_t)m_Delegates.size();
}

template< typename ArgsType >
void Event< ArgsType >::EventImpl::Compact()
{
	if (m_EmptySlots)
	{
		typename std::vector<Delegate>::iterator itr = m_Delegates.begin();
		typename std::vector<Delegate>::iterator end = m_Delegates.end();
		for ( uint32_t slotsLeft = m_EmptySlots; itr != end && slotsLeft; ++itr )
		{
			if ( !itr->Valid() )
			{
				typename std::vector<Delegate>::iterator next = itr + 1;
				for ( ; next != end; ++next )
				{
					if (next->Valid())
					{
						*itr = *next;
						next->Clear();
						--slotsLeft;
						break;
					}
				}
			}
		}

		m_Delegates.resize( m_Delegates.size() - m_EmptySlots );
		m_EmptySlots = 0;
	}
}

template< typename ArgsType >
void Event< ArgsType >::EventImpl::Add( const Delegate& delegate )
{
	typename std::vector<Delegate>::const_iterator itr = m_Delegates.begin();
	typename std::vector<Delegate>::const_iterator end = m_Delegates.end();
	for ( ; itr != end; ++itr )
	{
		if ( itr->Valid() && itr->Equals(delegate) )
		{
			return;
		}
	}

	m_Delegates.push_back( delegate );
}

template< typename ArgsType > template< typename FunctionType >
void Event< ArgsType >::EventImpl::AddFunction( FunctionType function )
{
	typename std::vector<Delegate>::const_iterator itr = m_Delegates.begin();
	typename std::vector<Delegate>::const_iterator end = m_Delegates.end();
	for ( ; itr != end; ++itr )
	{
		if ( itr->Valid() && itr->Equals(function) )
		{
			return;
		}
	}

	m_Delegates.push_back( Delegate (function) );
}

template< typename ArgsType > template< class ClassType, typename MethodType >
void Event< ArgsType >::EventImpl::AddMethod( ClassType* instance, MethodType method )
{
	typename std::vector<Delegate>::const_iterator itr = m_Delegates.begin();
	typename std::vector<Delegate>::const_iterator end = m_Delegates.end();
	for ( ; itr != end; ++itr )
	{
		if ( itr->Valid() && itr->Equals(instance, method ))
		{
			return;
		}
	}

	m_Delegates.push_back( Delegate (instance, method) );
}

template< typename ArgsType >
void Event< ArgsType >::EventImpl::Remove( const Delegate& delegate )
{
	typename std::vector<Delegate>::iterator itr = m_Delegates.begin();
	typename std::vector<Delegate>::iterator end = m_Delegates.end();
	for ( ; itr != end; ++itr )
	{
		if ( itr->Valid() && itr->Equals(delegate) )
		{
			if ( GetRefCount() == 1 )
			{
				m_Delegates.erase( itr );
			}
			else
			{
				m_EmptySlots++;
				itr->Clear();
			}
			break;
		}
	}
}

template< typename ArgsType >
template< typename FunctionType >
void Event< ArgsType >::EventImpl::RemoveFunction( FunctionType function )
{
	typename std::vector<Delegate>::iterator itr = m_Delegates.begin();
	typename std::vector<Delegate>::iterator end = m_Delegates.end();
	for ( ; itr != end; ++itr )
	{
		if ( itr->Valid() && itr->Equals(function) )
		{
			if ( GetRefCount() == 1 )
			{
				m_Delegates.erase( itr );
			}
			else
			{
				m_EmptySlots++;
				itr->Clear();
			}
			break;
		}
	}
}

template< typename ArgsType >
template< class ClassType, typename MethodType >
void Event< ArgsType >::EventImpl::RemoveMethod( const ClassType* instance, MethodType method )
{
	typename std::vector<Delegate>::iterator itr = m_Delegates.begin();
	typename std::vector<Delegate>::iterator end = m_Delegates.end();
	for ( ; itr != end; ++itr )
	{
		if ( itr->Valid() && itr->Equals(instance, method) )
		{
			if ( GetRefCount() == 1 )
			{
				m_Delegates.erase( itr );
			}
			else
			{
				m_EmptySlots++;
				itr->Clear();
			}
			break;
		}
	}
}

template< typename ArgsType >
void Event< ArgsType >::EventImpl::Raise( ArgsType parameter, const Delegate& emitter )
{
	++m_EntryCount;

	for ( size_t i=0; i<m_Delegates.size(); ++i )
	{
		Delegate& d ( m_Delegates[i] );

		if ( !d.Valid() || ( emitter.Valid() && emitter.Equals( d ) ) )
		{
			continue;
		}

		d.Invoke(parameter); 
	}

	if ( --m_EntryCount == 0 )
	{
		Compact();
	}
}