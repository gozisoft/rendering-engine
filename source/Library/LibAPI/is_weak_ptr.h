#pragma once

#ifndef __IS_WEAK_PTR_H__
#define __IS_WEAK_PTR_H__

#include "api_traits.h"
#include <memory>

_ENGINE_BEGIN

template < class T >
class is_std_weak_ptr
{
private:
	template <typename U> static small_type isSmartPtr(std::weak_ptr<U>);
	static large_type isSmartPtr(...);
	static T t;
public:
	static const bool value = sizeof(isSmartPtr<T>(t) == sizeof(small_type));

	typedef typename std::conditional<is_std_weak_ptr<T>::value, 
		std::true_type, 
		std::false_type>::type type;
};

_ENGINE_END

#endif