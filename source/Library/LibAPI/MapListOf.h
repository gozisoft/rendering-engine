#ifndef __MAP_LIST_OF_H__
#define __MAP_LIST_OF_H__

#include "Core.h"
#include <map>

_ENGINE_BEGIN

// Map list of implementation similar to that of boost::assign::map_list_of
template < class Key, class Value >
class map_list_of_type 
{ 
private:
	typedef std::map<Key, Value> Map; 
	Map data; 

public:
	map_list_of_type(const Key& k, const Value& v) 
	{
		data[k] = v;
	} 

	map_list_of_type& operator()(const Key& k, const Value& v) 
	{ 
		data[k] = v;
		return *this;
	} 

	operator Map const&() const 
	{ 
		return data;
	} 
}; 

template < class Key, class Value >
map_list_of_type<Key, Value> map_list_of(const Key& k, const Value& v)
{
	return map_list_of_type<Key, Value>(k, v);
}

_ENGINE_END

#endif