#ifndef __DATA_FUNCTIONS_INL__
#define __DATA_FUNCTIONS_INL__

//-------------------------------------------------------------------------------------
inline uint16_t LowerDoubleWord(uint32_t dword)
{
	return ( dword & 0xFFFF );
}

inline uint16_t UpperDoubleWord(uint32_t dword)
{
	return ( (dword >> 16) & 0xFFFF );
}

//-------------------------------------------------------------------------------------
inline uint32_t UpperHalf(uint64_t qword)
{
	return static_cast<uint16_t>( (qword >> 32) & 0xFFFFFFFF );
}

inline uint32_t LowerHalf(uint64_t qword)
{
	return static_cast<uint16_t>(qword & 0xFFFFFFFF);
}
//-------------------------------------------------------------------------------------
inline uint16_t UpperHalf(uint32_t dword)
{
	return static_cast<uint16_t>( (dword >> 16) & 0xFFFF );
}

inline uint16_t LowerHalf(uint32_t dword)
{
	return static_cast<uint16_t>(dword & 0xFFFF);
}
//-------------------------------------------------------------------------------------
inline uint8_t UpperHalf(uint16_t word)
{
	return static_cast<uint8_t>( (word >> 8) & 0xFF );
}

inline uint8_t LowerHalf(uint16_t word)
{
	return static_cast<uint8_t>( word & 0xFF );
}
//-------------------------------------------------------------------------------------
inline uint16_t CombineHalves(uint8_t upperbyte, uint8_t lowerbyte)
{
	return ( (upperbyte << 8) | (lowerbyte) );
}

inline uint32_t CombineHalves(uint16_t upperbyte, uint16_t lowerbyte)
{
	return ( (upperbyte << 16) | (lowerbyte) );
}
//-------------------------------------------------------------------------------------
inline size_t Popcnt(size_t i) 
{
	size_t c = 0;
	for(; i; c++) 
	{
		i &= i - 1;
	}
	return c;
}


#endif