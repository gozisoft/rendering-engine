#include "libapiafx.h"
#include "Thunk.h"

#if defined(_M_IX86)

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#ifdef WIN32_LEAN_AND_MEAN
#undef WIN32_LEAN_AND_MEAN
#endif

using namespace Engine;

void* volatile CThunkBase::bytecodeHeap = NULL;

CThunkBase::CThunkBase()
{
	// Double checked locking, guaranteed by Acquire/Release-semantics in Microsoft's
	// volatile-implementation.
	if(bytecodeHeap == NULL)
	{
		heapMutex.lock();
		if(bytecodeHeap == NULL)
		{
			bytecodeHeap = ::HeapCreate(HEAP_CREATE_ENABLE_EXECUTE, 0, 0);
			if(bytecodeHeap == NULL)
			{
				throw std::exception("Heap creation failed!");
			}

			// Schedule the heap to be destroyed when the application terminates.
			// Until then, it will manage its own size.
			atexit(cleanupHeap);
		}
		heapMutex.unlock();
	}

	bytecode = reinterpret_cast<Bytecode*>(::HeapAlloc(bytecodeHeap, 0, sizeof(Bytecode)));
	if(bytecode == NULL) 
	{ 
		throw std::exception("Bytecode allocation failed!");
	}
	new (bytecode) Bytecode;
}

CThunkBase::~CThunkBase()
{
	if(bytecode)
	{
		bytecode->~Bytecode();
		::HeapFree(bytecodeHeap, 0, bytecode);
	}
}

void CThunkBase::cleanupHeap()
{
	::HeapDestroy(CThunkBase::bytecodeHeap);
}

#endif