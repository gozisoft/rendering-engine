#pragma once
#ifndef __SAFE_DELETE_H__
#define __SAFE_DELETE_H__

#include "Core.h"
#include <functional>

_ENGINE_BEGIN

// Same as boost checked_delete
template<class T> 
inline void SafeDelete(T * x)
{
	typedef char type_must_be_complete[ sizeof(T)? 1: -1 ];
	(void) sizeof(type_must_be_complete);
	delete x;
}

// Same as boost checked_deleter
template<class T> 
struct SafeDeleter : std::unary_function <T *, void>
{
    void operator()(T * x) const
    {
        SafeDelete(x);
    }
};

// Checks if pointer is not 0 before deleting it.
template <class T>
void SafeDeleteArray(T& iface)
{
	if (iface)
	{
		delete[] (iface);
		iface = 0;
	}
}

template < typename T >
T **Allocate2DArray(size_t nRows, size_t nCols)
{
    // (step 1) allocate memory for array of elements of column
    T **ppi = new T*[nRows];

    // (step 2) allocate memory for array of elements of each row
    T *pool = new T [nRows * nCols];

    // Now point the pointers in the right place
    T *curPtr = pool;
    for(size_t i = 0; i < nRows; i++)
    {
        *(ppi + i) = curPtr;
         curPtr += nCols;
    }
    return ppi;
}

//template < typename T >
//void SafeDelete2DArray(T data[][])
//{
//    SafeDeleteArray(*data);
//    SafeDeleteArray(data);
//}

_ENGINE_END


#endif