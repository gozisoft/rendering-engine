#pragma once
#ifndef __CORE_H__
#define __CORE_H__

#if defined(_MSC_VER) || defined(MSC_VER)
//// If you wish to build your application for a previous Windows platform, include WinSDKVer.h and
//// set the _WIN32_WINNT macro to the platform you wish to support before including SDKDDKVer.h.
//#	include <SDKDDKVer.h>
#	if (_MSC_VER >= 1600) // Define to indicate the compiler is MS Visual Studio Version 10.                                                       
#		include <stdint.h>
#	endif

#	if defined (WIN64) || defined(_WIN64)
#		define USE_X64
#	else
#		define USE_X86
#	endif

#	if defined(WIN32) || defined(_WIN32)
#		define LITTLE_ENDIAN
#	elif defined(WIN64) || defined(_WIN64)
#		define LITTLE_ENDIAN
#	endif
#else
    #include <stdint.h>
#endif

#if defined (USE_X64)
	typedef uint64_t uint_t;
	typedef int64_t int_t;
#else
	typedef uint32_t uint_t;
	typedef int32_t int_t;
#endif

// Use unicode versions of Microsoft API
#if defined (UNICODE) || defined(_UNICODE)
	typedef wchar_t Char;
	#define ieS__(s) L##s		// string macro - used in conjunction with UNICODE and USING_WIDE_CHAR.  
	#define ieS(s) ieS__(s)		
#else
	typedef char Char;
	#define ieS(s) (s) 			// string macro - used in conjunction with ASCII and NOT_USING_WIDE_CHAR.    							
#endif // UNICODE

// Main char typedef (wchar_t, char)
typedef Char* pChar;
typedef const pChar Const_pChar;

// namespace Engine defines
#define _ENGINE_BEGIN namespace engine {
#define _ENGINE_END	  }
#define _ENGINE		  ::engine::

#define _DETAIL_BEGIN namespace detail {
#define _DETAIL_END	  }
#define _DETAIL		  ::detail::

// Removes warnings for unused parameters
#define UNUSED_PARAMETER(P) (P)

// Enables extra asserts for the BufferIO class
#if defined(DEBUG) || defined(_DEBUG)
#define BUFFERIO_VALIDATE_OPERATION
#endif

#if defined (USING_MEMORY_MGR) || defined (_USING_MEMORY_MGR)
#include "MemoryMgr.h"
#endif

#include <memory>

using std::unique_ptr;
using std::shared_ptr;
using std::weak_ptr;
using std::make_shared;

// Macro for adding typedefs for smart pointers.
#define SMART_PTR_TYPES(T) \
	typedef std::shared_ptr<T> shared_ptr_t; \
	typedef std::unique_ptr<T> unique_ptr_t; \
	typedef std::weak_ptr<T> weak_ptr_t; \

// Expose exactly one of these.
#define RE_USE_ROW_MAJOR
//#define RE_USE_COL_MAJOR

#if (defined(RE_USE_ROW_MAJOR) && defined(RE_USE_COL_MAJOR)) || (!defined(RE_USE_ROW_MAJOR) && !defined(RE_USE_COL_MAJOR))
#error Exactly one storage order must be specified.
#endif


#endif


//// Integer types capable of holding object pointers
//#if defined (WIN64) || defined(_WIN64) 
//typedef __int64	 intptr_t;
//typedef unsigned __int64  uintptr_t;
//#else 
//typedef _W64 int intptr_t;
//typedef _W64 unsigned int uintptr_t;
//#endif // WIN64

// #define USING_BOOST
// Place boost headers that you want to use in here, this will disable any TR1 use
//#ifdef USING_BOOST
//
//	#if defined(HAS_TR1) || defined(_HAS_TR1)
//		#define BOOST_HAS_TR1
//	#endif
//
//	#include <boost/config.hpp>
//
	//// This will include the multi index capabability of boost
	//#include <boost/multi_index_container.hpp>
	//#include <boost/multi_index/mem_fun.hpp>
	//#include <boost/multi_index/composite_key.hpp>
	//#include <boost/multi_index/member.hpp>

	//// These do have fwd declares, but I have no idea how to use them
	//#include <boost/multi_index/ordered_index.hpp>
	//#include <boost/multi_index/hashed_index.hpp>
	//#include <boost/multi_index/sequenced_index.hpp>
	//#include <boost/multi_index/random_access_index.hpp>

	//// The assign list of functionality
	//#include <boost/assign/list_of.hpp>

	//// The boost varient
	//#include <boost/variant.hpp>

	//// Safe casting ability
	//#include <boost/cast.hpp>

	//// Safe deltetion
	//#include <boost/checked_delete.hpp>
//
//#endif