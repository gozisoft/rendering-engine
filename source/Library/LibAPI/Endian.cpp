#include "libapiafx.h"
#include "Endian.h"
#include <cassert>

using namespace engine;


static int gsOne = 1;
bool CEndian::m_sIsLittle = *( reinterpret_cast<char*>(&gsOne) )!= 0;

//----------------------------------------------------------------------------
bool CEndian::IsBig ()
{
    return !m_sIsLittle;
}
//----------------------------------------------------------------------------
bool CEndian::IsLittle ()
{
    return m_sIsLittle;
}
//----------------------------------------------------------------------------
void CEndian::Swap2 (void* datum)
{
    char* bytes = reinterpret_cast<char*>(datum);
    char save = bytes[0];
    bytes[0] = bytes[1];
    bytes[1] = save;
}
//----------------------------------------------------------------------------
void CEndian::Swap2 (size_t numItems, void* data)
{
    char* bytes = reinterpret_cast<char*>(data);
    for (size_t i = 0; i < numItems; ++i, bytes += 2)
    {
        char save = bytes[0];
        bytes[0] = bytes[1];
        bytes[1] = save;
    }
}
//----------------------------------------------------------------------------
void CEndian::Swap4 (void* datum)
{
    char* bytes = reinterpret_cast<char*>(datum);
    char save = bytes[0];
    bytes[0] = bytes[3];
    bytes[3] = save;
    save = bytes[1];
    bytes[1] = bytes[2];
    bytes[2] = save;
}
//----------------------------------------------------------------------------
void CEndian::Swap4 (size_t numItems, void* data)
{
    char* bytes = reinterpret_cast<char*>(data);
    for (size_t i = 0; i < numItems; ++i, bytes += 4)
    {
        char save = bytes[0];
        bytes[0] = bytes[3];
        bytes[3] = save;
        save = bytes[1];
        bytes[1] = bytes[2];
        bytes[2] = save;
    }
}
//----------------------------------------------------------------------------
void CEndian::Swap8 (void* datum)
{
    char* bytes = reinterpret_cast<char*>(datum);
    char save = bytes[0];
    bytes[0] = bytes[7];
    bytes[7] = save;
    save = bytes[1];
    bytes[1] = bytes[6];
    bytes[6] = save;
    save = bytes[2];
    bytes[2] = bytes[5];
    bytes[5] = save;
    save = bytes[3];
    bytes[3] = bytes[4];
    bytes[4] = save;
}
//----------------------------------------------------------------------------
void CEndian::Swap8 (size_t numItems, void* data)
{
    char* bytes = reinterpret_cast<char*>(data);
    for (size_t i = 0; i < numItems; ++i, bytes += 8)
    {
        char save = bytes[0];
        bytes[0] = bytes[7];
        bytes[7] = save;
        save = bytes[1];
        bytes[1] = bytes[6];
        bytes[6] = save;
        save = bytes[2];
        bytes[2] = bytes[5];
        bytes[5] = save;
        save = bytes[3];
        bytes[3] = bytes[4];
        bytes[4] = save;
    }
}
//----------------------------------------------------------------------------
void CEndian::Swap (size_t itemSize, void* data)
{
    assert( itemSize == 2 || itemSize == 4 || itemSize == 8 && ieS("Size must be 2, 4, or 8\n") );

    int size = static_cast<int>(itemSize);
    char* bytes = reinterpret_cast<char*>(data);
    for (int i0 = 0, i1 = size - 1; i0 < size/2; ++i0, --i1)
    {
        char save = bytes[i0];
        bytes[i0] = bytes[i1];
        bytes[i1] = save;
    }
}
//----------------------------------------------------------------------------
void CEndian::Swap (size_t itemSize, size_t numItems, void* data)
{
    assert( itemSize == 2 || itemSize == 4 || itemSize == 8 && ieS("Size must be 2, 4, or 8\n") );

    int size = (int)itemSize;
    char* bytes = (char*) data;
    for (int i = 0; i < (int)numItems; ++i, bytes += itemSize)
    {
        for (int i0 = 0, i1 = size - 1; i0 < size/2; ++i0, --i1)
        {
            char save = bytes[i0];
            bytes[i0] = bytes[i1];
            bytes[i1] = save;
        }
    }
}
//----------------------------------------------------------------------------


