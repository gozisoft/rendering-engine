#pragma once
#ifndef __CENDIAN_H__
#define __CENDIAN_H__

#include "Core.h"

_ENGINE_BEGIN

// Geometric Tools:: This class is from geometric tools.
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
class CEndian
{
public:
	// Test whether the byte order of the machine is big endian.
	static bool IsBig ();

	// Test whether the byte order of the machine is little endian.
	static bool IsLittle ();

	// Swap the byte order.  The functions with itemSize input are designed
	// to be used with template classes, where the item size depends on the
	// template parameter.
	static void Swap2 (void* datum);
	static void Swap2 (size_t numItems, void* data);
	static void Swap4 (void* datum);
	static void Swap4 (size_t numItems, void* data);
	static void Swap8 (void* datum);
	static void Swap8 (size_t numItems, void* data);
	static void Swap (size_t itemSize, void* datum);
	static void Swap (size_t itemSize, size_t numItems, void* data);

private:
	static bool m_sIsLittle;
};

_ENGINE_END



#endif