#pragma once

#ifndef __API_TRAITS_H__
#define __API_TRAITS_H__

#include "Core.h"
#include <type_traits>

_ENGINE_BEGIN

// -- null_type ---------------------------------------------------------------------------------------
struct null_type {};

struct small_type { char dummy; };

struct large_type { char dummy[2]; };

template<bool>
struct cat_base
	: std::false_type
{	// base class for type predicates
};

template<>
struct cat_base<true>
	: std::true_type
{	// base class for type predicates
};

template< class T >
struct decay_ref_ptr
{	// removes reference and pointers to type T
	typedef typename std::conditional<std::is_reference<T>::value,
		typename std::remove_reference<T>::type,
		typename std::conditional<std::is_pointer<T>::value,
		typename std::remove_pointer<T>::type,
		typename std::remove_cv<T>::type>::type>::type type;
};

//-----------------------------------------------------------------------------------------------------
// Bit size checking for built in types.
//-----------------------------------------------------------------------------------------------------
template< class T > 
struct is_32bit 
	: cat_base< sizeof(T) == sizeof(int32_t) > 
{ // determines if the T is 4 bytes in size
};

template< class T > 
struct is_64bit
	: cat_base< sizeof(T) == sizeof(int64_t) >
{ // determines if the T is 8 bytes in size
};

template < class T >
inline int64_t ToInt64(const T& x)
{
	return *(int64_t*)&x;
}

template < class T >
inline uint64_t ToUInt64(const T& x)
{
	return *(uint64_t*)&x;
}

template < class T >
inline int32_t ToInt32(const T& x)
{
	return *(int32_t*)&x;
}

template < class T >
inline uint32_t ToUInt32(const T& x)
{
	return *(uint32_t*)&x;
}

//-----------------------------------------------------------------------------------------------------
// Function to determine size of an array.
//-----------------------------------------------------------------------------------------------------
template < std::size_t N >
struct type_of_size
{
	typedef char type[N];
};

template < typename T, std::size_t N >
typename type_of_size<N>::type& sizeof_array_helper(T(&)[N]);

// Determines the size of an array using templates.
template < typename T >
size_t sizeof_array(T& pArray, typename std::enable_if<std::is_array<T>::value>::type* dummy = nullptr)
{
	return sizeof(sizeof_array_helper(pArray));
}

// #define sizeof_array(pArray) sizeof(sizeof_array_helper(pArray))

_ENGINE_END

#endif // __API_TRAITS_H__




//template< typename T > 
//struct has_member 
//{ 
//private:
//	typedef T type;
//
//	struct yes{ char m; };
//	struct no { yes array[2]; };  
//
//	template< typename C > 
//	static yes test(typename C::rtti*);  
//
//	template< typename C > 
//	static no test(...); 
//
//public:
//	typedef integral_constant< 
//		bool,
//		sizeof(test<type>(0)) == sizeof(yes)
//	>::value value;
//}; 

//#define CREATE_MEMBER_CHECK(member_name) \
//template< class T, class Sign > \
//struct has_member_## member_name \
//{ \
//	template<Sign> struct tester; \
//	template<class U> static small_type has_foo(tester<&U::## member_name> *); \
//	template<class U> static large_type has_foo(...); \
//	static const bool value = (sizeof(has_foo<T>(0)) == sizeof(small_type)); \
//}

//#define CREATE_MEMBER_FUNC_CHECK(member_name) \
//	template<class T> \
//	struct has_member_function_##member_name \
//	{ \
//		template<int (T::*)()> struct tester; \
//		template<class U> static small_type has_foo(tester<&U::foo> *); \
//		template<class U> static large_type has_foo(...); \
//		static const bool value = (sizeof(has_foo<T>(0)) == sizeof(small_type)); \
//	}


//#define CREATE_STATIC_MEMBER_FUNC_CHECK(member_name) \
//	template<class T> \
//	struct has_static_member_function_##member_name \ 
//	{ \
//		template<int (*)()> struct tester; \
//		template<class U> static small_type has_foo(tester<&U::foo> *); \
//		template<class U> static large_type has_foo(...); \
//		static const bool value = (sizeof(has_foo<T>(0)) == sizeof(small_type)); \
//	}


//#define CREATE_MEMBER_VAR_CHECK(member_name) \
//template<class T> \
//struct has_member_variable_member_name_##member_name \
//{ \
//    template<int T::*> struct tester; \
//    template<class U> static small_type has_foo(tester<&U::foo> *); \
//    template<class U> static large_type has_foo(...); \
//    static const bool value = (sizeof(has_foo<T>(0)) == sizeof(small_type)); \
//}


//#define CREATE_STATIC_MEMBER_VAR_CHECK(member_name) \
//	template< class T > \
//	struct has_static_member_variable_##member_name \
//	{ \
//		template<int *> struct tester; \
//		template<class U> static small_type has_foo(tester<&U::member_name> *); \
//		template<class U> static large_type has_foo(...); \ 
//		static const bool value = (sizeof(has_foo<T>(0)) == sizeof(small_type)); \
//	};


//-----------------------------------------------------------------------------------------------------
// Check for member x in a given class. Could be var, func, class, union, or enum:
// http://stackoverflow.com/questions/1005476/how-to-detect-whether-there-is-a-specific-member-variable-in-class
//-----------------------------------------------------------------------------------------------------
// http://stackoverflow.com/questions/4007179/concept-checking-of-static-member-variables-compile-error-on-gcc
// based on this
// http://cplusplus.co.il/2009/09/11/substitution-failure-is-not-an-error-1/
//-----------------------------------------------------------------------------------------------------