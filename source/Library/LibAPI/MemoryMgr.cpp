#include "libapiafx.h"
#include "MemoryMgr.h"
#include <cassert>

#ifdef USING_MEMORY_MGR

using namespace engine;

void * operator new (size_t size) 
{
	return operator new (size, CHeapFactory::GetDefaultHeap() );
}


void * operator new (size_t size, CHeap * pHeap) 
{
	assert(pHeap != NULL);
	return pHeap->Allocate(size);
}


void operator delete (void * pMem) 
{
	if (pMem != NULL)
		CHeap::Deallocate (pMem);    
}



#endif