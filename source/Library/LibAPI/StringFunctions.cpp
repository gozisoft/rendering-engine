#include "libapiafx.h"

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)

#include "StringFunctions.h"

#include <direct.h>
#include <tchar.h>

#if (!defined(WIN32_LEAN_AND_MEAN))
#define WIN32_LEAN_AND_MEAN
	#include <Windows.h>
#undef WIN32_LEAN_AND_MEAN
#endif


_ENGINE_BEGIN

std::string StringUtils::GetExeDir()
{
	char path[MAX_PATH];
	::GetModuleFileName(nullptr, &path[0], MAX_PATH);
	return path;
}


std::string StringUtils::GetFullPath(const char* path)
{
	std::string result;

	// Get absolute path
	char* pathName = _fullpath(NULL, path, _MAX_PATH);
	if (pathName != NULL) {
		size_t stringLength = strlen(pathName);
		for (size_t i = 0; i < stringLength; ++i)
		{
			if (pathName[i] == '\\')
				pathName[i] = '/';
		}

		// Copy the path name over.
		result.append(pathName);

		// Delete the old string
		delete pathName;
	}

	return result;
}

std::string StringUtils::GetWorkingDirectory(const char* path)
{
	// Passing NULL as the buffer forces getcwd to allocate
	// memory for the path, which allows the code to support file paths
	// longer than _MAX_PATH, which are supported by NTFS.

	// The result to return
	std::string result;

	// Get the current working directory: 
	const char* pBuffer = _getcwd(NULL, 0);
	if (pBuffer != nullptr)
	{
		// Add the buffer to the string and 
		// append a forward slash.
		result.append(pBuffer);
		result.append("/");

		// Delete the old string
		delete pBuffer;

		// Swap slashes.
		for (size_t i = 0; i < result.size(); ++i)
		{
			if (result[i] == '\\')
				result[i] = '/';
		}
	}

	return result;
}

//std::string StringUtils::GetFullPath(const std::string& path);
//std::string StringUtils::GetWorkingDirectory(const std::string& path);

std::wstring StringUtils::Widen(const std::string& str)
{
	using std::wostringstream;
	wostringstream wstm;

	using std::ctype;
	using std::use_facet;
	const ctype<wchar_t>& ctfacet = use_facet< ctype<wchar_t> >( wstm.getloc() );

	for( size_t i=0 ; i<str.size() ; ++i ) 
	{
		wstm << ctfacet.widen( str[i] );
	}

	return wstm.str();
}

std::string StringUtils::Narrow(const std::wstring& str)
{
	using std::ostringstream;
	ostringstream stm;

	using std::ctype;
	using std::use_facet;
	const ctype<char>& ctfacet = use_facet< ctype<char> >( stm.getloc() ) ;

	for( size_t i=0 ; i<str.size() ; ++i ) 
	{
		stm << ctfacet.narrow( str[i], 0 ) ;
	}

	return stm.str() ;
}

int StringUtils::AnsiToWide(wchar_t* dest, const char* src, int inputLen, int outputLen)
{
	if( dest == 0 || src == 0 || outputLen < 1 )
		return 1;

	int result = MultiByteToWideChar(CP_ACP, 0, src, inputLen, dest, outputLen);

	if(result == 0)
		return 1;

	return 0;
}

int StringUtils::WideToAnsi(char* dest, const wchar_t* src, int inputLen, int outputLen)
{
	if( dest == 0 || src == 0 || outputLen < 1 )
		return 1;

	int result = WideCharToMultiByte(CP_UTF8, 0, src, inputLen, dest, outputLen, 0, 0);

	if( result == 0 )
		return 1;

	return 0;
}

int StringUtils::GenericToAnsi(char* dest, const Char* src, int inputLen, int outputLen)
{
	if( dest == 0 || src == 0 || outputLen < 1 )
		return 1;

#if defined(_UNICODE) | defined(UNICODE)
	return WideToAnsi(dest, src, inputLen, outputLen);
#else
	strncpy(dest, src, outputLen);

	if (inputLen < 1)
		dest[outputLen-1] = '\0';

	return 0;
#endif   
}

int StringUtils::AnsiToGeneric(Char* dest, const char* src, int inputLen, int outputLen)
{
	if( dest == 0  || src == 0 || outputLen < 1 )
		return 1;

#if defined(_UNICODE) | defined(UNICODE)
	return AnsiToWide( dest, src, inputLen, outputLen );
#else
	strncpy(dest, src, outputLen);

	if (inputLen < 1)
		dest[outputLen-1] = '\0';

	return 0;
#endif    
}


_ENGINE_END


#endif







	/*
	std::wstring widen(const std::string& s, std::locale loc)
	{
	std::char_traits<wchar_t>::state_type state = { 0 };

	typedef std::codecvt<wchar_t, char, std::char_traits<wchar_t>::state_type >
	ConverterFacet;

	ConverterFacet const& converter(std::use_facet<ConverterFacet>(loc));

	char const* nextToRead = s.data();
	wchar_t buffer[BUFSIZ];
	wchar_t* nextToWrite;
	std::codecvt_base::result result;
	std::wstring wresult;

	while ((result
	= converter.in
	(state,
	nextToRead, s.data()+s.size(), nextToRead,
	buffer, buffer+sizeof(buffer)/sizeof(*buffer), nextToWrite))
	== std::codecvt_base::partial)
	{
	wresult.append(buffer, nextToWrite);
	}

	if (result == std::codecvt_base::error)
	{
	throw std::runtime_error("Encoding error");
	}

	wresult.append(buffer, nextToWrite);
	return wresult;
	}

	*/
