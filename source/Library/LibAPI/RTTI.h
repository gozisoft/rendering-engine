#pragma once
#ifndef __CRTTI_H__
#define __CRTTI_H__

#include "api_traits.h"
#include <cassert>
#include <vector>
#include <string>
#include <functional>
#include <memory>

_ENGINE_BEGIN

// Class based Run Time Type Information.
// This method is simple and fast/effcient for classes that don't
// need multiple inheritance based information.
class CRTTI {
public:
	CRTTI(const std::string& name);
	CRTTI(const std::string& name, const CRTTI* base);
	CRTTI(const std::string& name, size_t numParents, ...);
	~CRTTI();

	const char* name() const {
		return m_name.c_str();
	}

	size_t hash_code() const {
		return m_hashcode;
	}

	bool is_exactly(const CRTTI& rtti) const {
		return (this == &rtti);
	}

	bool derives_from(const CRTTI & rtti) const {
		if (is_exactly(rtti))
			return true;

		for (size_t i = 0; i < m_parents.size(); ++i) {
			if (m_parents[i]->derives_from(rtti))
				return true;
		}

		return false;
	}

	bool before(const CRTTI & rtti) const {
		// If classes are the same, return false.
		if (is_exactly(rtti))
			return false;

		// If the rtti has this class as a parent
		// then this is before.
		return rtti.derives_from(*this);
	}

	bool operator ==(const CRTTI& right) const {
		return (*this).is_exactly(right);
	}

	bool operator !=(const CRTTI& right) const {
		return !(*this == right);
	}

private:
	// Typedef for a vector of parents
	typedef std::vector<const CRTTI*> ParentArray;

	// Prevent copying
	CRTTI(const CRTTI& rtti);
	CRTTI& operator =(const CRTTI& rtti);

	// An Array of Parents
	ParentArray m_parents;

	// Name of the class
	std::string m_name;

	// Hash code of the name of the class
	size_t m_hashcode;
};

class CTypeIndex {
public:

	CTypeIndex(const CRTTI& rtti)
		: pRtti(&rtti) {
	}

	const char* name() const {
		return pRtti->name();
	}

	size_t hash_code() const {
		return pRtti->hash_code();
	}

	bool operator ==(const CTypeIndex& right) const {
		return (*pRtti == *right.pRtti);
	}

	bool operator !=(const CTypeIndex& right) const {
		return !(*this == right);
	}

	bool operator<(const CTypeIndex& right) const {
		return (pRtti->before(*right.pRtti));
	}

	bool operator >=(const CTypeIndex& right) const {
		return !(*this < right);
	}

	bool operator>(const CTypeIndex& right) const {
		return (right < *this);
	}

	bool operator <=(const CTypeIndex& right) const {
		return !(right < *this);
	}

private:
	const CRTTI* pRtti;
};

//----------------------------------------------------------------------------
template <typename T>
class base_rtti
{
public:
	// types
	typedef CRTTI rtti_type;

	static const rtti_type rtti;

	virtual const rtti_type& getRtti() const
	{
		return rtti;
	}
};


#define RTTI_DECL \
public: \
	typedef CRTTI rtti_type; \
	\
    static const rtti_type rtti; \
    \
    virtual const rtti_type& getRtti() const \
    { \
        return rtti; \
    }

#define RTTI_ROOT_IMPL(nsname, name) \
    const CRTTI name::rtti(#nsname"."#name);

#define RTTI_IMPL_1(nsname, name, parent) \
    const CRTTI name::rtti(#nsname"."#name, &parent::rtti);

#define RTTI_IMPL_2(nsname, name, parent0, parent1) \
    const CRTTI name::rtti(#nsname"."#name, 2, &parent0::rtti, &parent1::rtti);

#define RTTI_IMPL_3(nsname, name, parent0, parent1, parent2) \
    const CRTTI name::rtti(#nsname"."#name, 3, &parent0::rtti, &parent1::rtti, &parent2::rtti);

#define RTTI_IMPL_4(nsname, name, parent0, parent1, parent2, parent3) \
    const CRTTI name::rtti(#nsname"."#name, 4, &parent0::rtti, &parent1::rtti, &parent2::rtti, &parent3::rtti);

#include "RTTI.inl"

_ENGINE_END

namespace std {

	// TEMPLATE STRUCT SPECIALIZATION hash
	// hash functor for CTypeIndex

	template<>
	struct hash<engine::CTypeIndex> : public unary_function < engine::CTypeIndex, size_t > {

		size_t operator() (engine::CTypeIndex keyval) const { // hash _Keyval to size_t value by pseudorandomizing transform
			return (keyval.hash_code());
		}
	};

}


#endif

