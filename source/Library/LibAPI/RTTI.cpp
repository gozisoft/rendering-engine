#include "libapiafx.h"
#include "RTTI.h"
#include <cstdarg>

using namespace engine;

CRTTI::CRTTI(const std::string& name) 
	:
m_name(name),
m_hashcode( std::hash<std::string>()(name) )
{

}

CRTTI::CRTTI(const std::string& name, const CRTTI* base) 
	: 
m_name(name),
m_parents(1, base),
m_hashcode( std::hash<std::string>()(name) )
{
 
}

CRTTI::CRTTI(const std::string& name, size_t numParents, ...) 
	: 
m_name(name),
m_parents(numParents),
m_hashcode( std::hash<std::string>()(name) )
{   
	va_list v = nullptr;
	va_start(v, numParents);
	for (size_t i = 0; i < m_parents.size(); ++i) 
	{
		m_parents[i] = va_arg(v, const CRTTI*);
	}
	va_end(v);
}

CRTTI::~CRTTI()
{
	m_name.clear();
	m_parents.clear();
}
