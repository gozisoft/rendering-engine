#pragma once
#ifndef API_FORWARD_H
#define API_FORWARD_H

#include "Core.h"

_ENGINE_BEGIN

// Memory manager
class CHeap;
class CHeapFactory;

// String functions
class CHashedString;

// Input/Output
class CEndian;
class CBufferIO;

// Run time type info
class CRTTI;

_ENGINE_END

#endif