#pragma once

#ifndef __IS_SHARED_PTR_H__
#define __IS_SHARED_PTR_H__

#include "api_traits.h"
#include <memory>

_ENGINE_BEGIN

template < class T >
class is_std_shared_ptr
{
private:
	template <typename U> static small_type isSmartPtr(std::shared_ptr<U>);
	static large_type isSmartPtr(...);
	static T t;
public:
	static const bool value = sizeof(isSmartPtr<T>(t) == sizeof(small_type);

	typedef typename std::conditional<is_std_shared_ptr<T>::value, 
		std::true_type, 
		std::false_type>::type type;
};

_ENGINE_END

#endif