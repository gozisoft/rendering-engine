#pragma once
#ifndef __MAP_SELECT_H__
#define __MAP_SELECT_H__

#include "Core.h"
#include <functional>

_ENGINE_BEGIN

// Similar to those functions found in:
// http://www.sgi.com/tech/stl/select1st.html

template <typename PairT>
struct select1st : public std::unary_function<PairT, typename PairT::first_type>
{
    typename PairT::first_type operator()(const PairT& a) { return a.first; }
};

template <typename PairT>
struct select2nd : public std::unary_function<PairT, typename PairT::second_type>
{
    typename PairT::second_type operator()(const PairT& a) { return a.second; }
};

_ENGINE_END

#endif