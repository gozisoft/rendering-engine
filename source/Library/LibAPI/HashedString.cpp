#include "libapiafx.h"
#include "HashedString.h"
#include <locale> // used for tolower
#include <cstring>

using namespace engine;

CHashedString::CHashedString(const char* pIdentString) 
	: 
m_ident( hash_name( pIdentString ) ),
m_identStr(pIdentString)
{

}

CHashedString::CHashedString(const CHashedString& other) 
	:
m_ident(other.m_ident),
m_identStr( other.GetStr() )
{


}

CHashedString& CHashedString::operator = (const CHashedString& other)
{
	m_ident = (void*)other.GetHashValue();
	m_identStr = other.GetStr();
	return *this;
}

void* CHashedString::hash_name(const char* pIdentStr)
{
	// Relatively simple hash of arbitrary text string into a
	// 32-bit identifier Output value is
	// input-valid-deterministic, but no guarantees are made
	// about the uniqueness of the output per-input
	//
	// Input value is treated as lower-case to cut down on false
	// separations cause by human mistypes. Sure, it could be
	// construed as a programming error to mix up your cases, and
	// it cuts down on permutations, but in Real World Usage
	// making this text case-sensitive will likely just lead to
	// Pain and Suffering.
	//
	// This code lossely based upon the adler32 checksum by Mark
	// Adler and published as part of the zlib compression
	// library sources.

	// largest prime smaller than 65536
	uint32_t BASE = 65521L;

	// NMAX is the largest n such that 255n(n+1)/2 +
	// (n+1)(BASE-1) <= 2^32-1
	uint32_t NMAX = 5552;

	// Locale to be used 
	std::locale loc;

#define DO1(buf,i)  { s1 += std::tolower(buf[i], loc); s2 += s1; }
#define DO2(buf,i)  DO1(buf,i); DO1(buf,i+1);
#define DO4(buf,i)  DO2(buf,i); DO2(buf,i+2);
#define DO8(buf,i)  DO4(buf,i); DO4(buf,i+4);
#define DO16(buf)   DO8(buf,0); DO8(buf,8);

	if (pIdentStr == NULL)
		return NULL;

	uint32_t s1 = 0;
	uint32_t s2 = 0;

	for ( size_t len = strlen( pIdentStr ); len > 0 ; )
	{
		uint32_t k = len < NMAX ? len : NMAX;

		len -= k;

		while (k >= 16)
		{
			DO16(pIdentStr);
			pIdentStr += 16;
			k -= 16;
		}

		if (k != 0) do
		{
			s1 += std::tolower( *pIdentStr++, loc );
			s2 += s1;
		} while (--k);

		s1 %= BASE;
		s2 %= BASE;
	}

#pragma warning(push)
#pragma warning(disable : 4312)

	return reinterpret_cast<void*>( (s2 << 16) | s1 );

#pragma warning(pop)
#undef DO1
#undef DO2
#undef DO4
#undef DO8
#undef DO16

}