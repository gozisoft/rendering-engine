#pragma once
#ifndef __DATA_FUNCTIONS_H__
#define __DATA_FUNCTIONS_H__

#include "Core.h"

_ENGINE_BEGIN

// These can be used recursively for halving values
// Paramater names are based off of x86 architectures from AMD and Intel
uint32_t UpperHalf(uint64_t qword);
uint32_t LowerHalf(uint64_t qword);
uint16_t UpperHalf(uint32_t dword);
uint16_t LowerHalf(uint32_t dword);
uint8_t UpperHalf(uint16_t word);
uint8_t LowerHalf(uint16_t word);

// These functions can be used to combine integer types to form
// larger integer types.
uint16_t CombineHalves(uint8_t upperbyte, uint8_t lowerbyte);
uint32_t CombineHalves(uint16_t upperbyte, uint16_t lowerbyte);

// Method for counting used bits
size_t Popcnt(size_t val);

#include "DataFunctions.inl"

_ENGINE_END

#endif