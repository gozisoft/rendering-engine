#pragma once
#ifndef FIXED_VERTEX_FORMAT_H
#define FIXED_VERTEX_FORMAT_H

#include "libgeometryfwd.h"
#include "GeometryTypes.h"
#include "RendererTypes.h"

#include <map>
#include <vector>

_ENGINE_BEGIN

// A fixed vertex format element
class FVFElement
{
public:
    // Constructor with default arguements
    FVFElement(size_t offset, size_t numChannels, CDataType::Type dataType,
               CVertexSemantic::Type semantic, size_t semanticIndex = 0);

    // Stride between elements
    size_t offset() const;

    // Used to distinguish if there are more than one type of semantic
    // i.e.
    // semantic = Texcoord, semantic index = 0
    // semantic = Texcoord, semantic index = 1
    size_t index() const;

    // Returns the number of vertex channels. These range from 1 - 4.
    size_t channels() const;

    // Return the size of this element
    size_t size() const;

    // Returns the type of vertex, V_UNKNOWN = ~0, V_BYTE = 0, V_UBYTE,
    // V_SHORT, V_USHORT, V_INT, V_UINT, V_FLOAT, V_DOUBLE.
    CDataType::Type dataType() const;

    // Vertex semantic is a text string to match the input assembler of
    // a vertex shader.
    CVertexSemantic::Type semantic() const;

private:
    size_t m_offset;                    // Offset from the previous element
    size_t m_numChannels;                // The number of channels the data type consumes
    size_t m_semanticIndex;                // The semantic index
    CDataType::Type m_dataType;            // Type element represents in shader
    CVertexSemantic::Type m_semantic;    // A fixed semantic the element represents of a vbuffer
};

class CFVFormat
{
public:
    // Element container
    typedef std::vector<FVFElement *> ElementArray;
    typedef std::map<CVertexSemantic::Type, ElementArray> ElementsMap;

    // Default CTor
    CFVFormat();

    // Default destructor
    ~CFVFormat();

    // Chained method for adding elements
    CFVFormat &operator()(size_t numChannels, CDataType::Type dataType,
                          CVertexSemantic::Type semantic);

    // Creates an element and adds it to the back of the element container
    const FVFElement *AddElement(size_t numChannels, CDataType::Type dataType,
                                 CVertexSemantic::Type semantic);

    // Remove element by semantic and sematic index
    void RemoveElement(CVertexSemantic::Type semantic, size_t semanticIndex = 0);

    // Remove an element by comparison
    void RemoveElement(const FVFElement *element);

    // Return an element by its semantic
    const FVFElement *GetElement(CVertexSemantic::Type semantic,
                                 size_t semanticIndex = 0) const;

    // Return an array of elements by semantic, C++ style method
    ElementArray GetElements(CVertexSemantic::Type semantic) const;

    // Return an array of elements by semantic, C style method
    void GetElements(CVertexSemantic::Type semantic, FVFElement *const *elemnts,
                     size_t *count) const;

    // Determine the num of elements belonging to a semantic
    size_t GetElementCount(CVertexSemantic::Type semantic) const;

    // The total stride (all offsets added together to make this format)
    size_t totalStride() const;

    // Returns the map
    const ElementsMap &GetElementMap() const;

    // Returns all the elements in an array - sorted by offset value
    // # This function is not recommended
    void getAllELements(ElementArray &elements) const;

    // Returns all the elements in an array - sorted by offset value
    // # This function is not recommended
    void getAllELements(FVFElement **elementsOut, size_t count);

private:
    ElementsMap m_elementsMap;
    size_t m_totalStride;
};

// Public builder class
class CFVFBuilder
{
public:
    CFVFBuilder(size_t numChannels, CDataType::Type dataType,
                CVertexSemantic::Type semantic)
    {
        m_format.AddElement(numChannels, dataType, semantic);
    }

    CFVFBuilder &operator()(size_t numChannels, CDataType::Type dataType,
                            CVertexSemantic::Type semantic)
    {
        m_format.AddElement(numChannels, dataType, semantic);
        return *this;
    }

    operator CFVFormat const &() const
    {
        return m_format;
    }

private:
    CFVFormat m_format;
};

#include "FVFormat.inl"

_ENGINE_END

#endif

//template < class Container >
//class container_back_insert_interator
//{
//public:
//	typedef container_back_insert_interator<Container> this_type;
//	typedef Container container_type;
//	typedef typename container_type::const_reference const_reference;
//	typedef typename container_type::value_type valty;
//
//	explicit container_back_insert_interator (container_type& cont) 
//		: container(&cont)
//	{ }
//
//	template < class Other_Container >
//	this_type& operator = (const Other_Container& value)
//	{ 
//		container->insert( container->end(), std::begin(value), std::end(value) );
//		return *this;
//	}
//
//	this_type& operator* ()
//	{ 
//		return *this;
//	}
//
//	this_type& operator++ ()
//	{ 
//		return *this;
//	}
//
//	this_type operator++ (int)
//	{
//		return *this;
//	}
//
//protected:
//	container_type* container; // pointer to container
//};
//
//template < class Container >
//inline container_back_insert_interator<Container> container_back_inserter(Container& cont)
//{
//	return container_back_insert_interator<Container>(cont);
//}
//
//template < class InputIterator, class OutputIterator, class UnaryOperator >
//OutputIterator transform ( InputIterator first1, InputIterator last1,
//	OutputIterator result, UnaryOperator op )
//{
//	while (first1 != last1)
//		*result++ = op(*first1++);  // or: *result++=binary_op(*first1++,*first2++);
//	return result;
//}

//class FVFElement
//{
//public:
//	// Constructor with default arguements
//	FVFElement(size_t offset, size_t numChannels, CDataType::Type dataType, CVertexSemantic::Type semantic,
//		size_t semanticIndex = 0, size_t streamIndex = 0);
//
//	// The stream this format element belongs to
//	size_t GetStreamIndex() const;
//
//	// Stride between elements
//	size_t offset() const;
//
//	// Used to distinguish if there are more than one type of semantic 
//	// i.e.
//	// semantic = Texcoord, semantic index = 0
//	// semantic = Texcoord, semantic index = 1
//	size_t index() const;
//
//	// Returns the number of vertex channels. These range from 1 - 4.
//	size_t channels() const;
//
//	// Returns the type of vertex, V_UNKNOWN = ~0, V_BYTE = 0, V_UBYTE,
//	// V_SHORT, V_USHORT, V_INT, V_UINT, V_FLOAT, V_DOUBLE.
//	CDataType::Type dataType() const;
//
//	// Vertex semantic is a text string to match the input assembler of 
//	// a vertex shader.
//	CVertexSemantic::Type GetSemantic() const;
//
//private:
//	size_t m_offset;
//	size_t m_numChannels;
//	size_t m_semanticIndex;
//	size_t m_streamIndex;
//	CDataType::Type m_dataType;
//	CVertexSemantic::Type m_semantic;
//};

//class CFVFormat
//{
//public:
//	// Element container
//	typedef std::vector<FVFElement> Elements;
//
//	// Default ctor
//	CFVFormat(const FVFElement* elements, size_t numElements);
//
//	// C++ style ctor
//	CFVFormat(const Elements& elements);
//
//	// Default destructor
//	~CFVFormat();
//
//	// Creates an element and adds it to the back of the element container
//	const FVFElement& AddElement(size_t numChannels, CDataType::Type dataType,
//		CVertexSemantic::Type semantic,	size_t semanticIndex = 0,
//		size_t streamIndex = 0);
//
//	// Return an element by its semantic
//	const FVFElement& GetElement(CVertexSemantic::Type semantic, size_t index = 0);
//
//	// Determine the num of elements belonging to a semantic
//	size_t GetElementCount(CVertexSemantic::Type semantic) const;
//
//	// The total stride (all offsets added together to make this format)
//	size_t totalStride() const;
//
//	// Check if a field is enabled
//	bool IsEnabled(CVertexSemantic::Type semantic) const;
//
//private:
//	// Bit field setting functions for setting a semantic as on or off.
//	void SetEnabled(CVertexSemantic::Type semantic, bool enable);
//
//	Elements m_elements;
//	size_t m_totalStride;
//	size_t m_semanticFlags;
//};