#pragma once
#ifndef __GEOMETRY_HEADERS_H__
#define __GEOMETRY_HEADERS_H__

// Resources
#include "Effect.h"
#include "Buffer.h"
#include "FVFormat.h"
#include "ElementView.h"
#include "Material.h"
#include "Transform.h"

// NTree
#include "Spatial.h"
#include "VisualGroup.h"
#include "Visual.h"
#include "Triangle.h"
#include "TriangleMesh.h"
#include "TriangleFan.h"
#include "TriangleStrip.h"
#include "Polyline.h"
#include "Polypoint.h"

// Mesh tools
#include "StandardMesh.h"



#endif