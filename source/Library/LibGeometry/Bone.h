#ifndef __CBONE_H__
#define __CBONE_H__

#include "libgeometryfwd.h"
#include "Spatial.h"

_ENGINE_BEGIN

class CBone : public Spatial
{
public:
	// Skeleton has access to private members
	friend class CSkeleton;

	// Access the handle
	size_t GetHandle() const;

private:
	// The owner of this bone
	CSkeletonPtr m_skeleton; 

	// Index handle to the skeletal array
	size_t m_index;
};

_ENGINE_END

#endif