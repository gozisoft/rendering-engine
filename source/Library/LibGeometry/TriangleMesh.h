#pragma once
#ifndef __CTRIANGLE_MESH_H__
#define __CTRIANGLE_MESH_H__

#include "Triangle.h"

_ENGINE_BEGIN

class TriMesh : public Triangle
{
public:
	// Constructor and destructor
	TriMesh(shared_ptr<VertexBuffer> vbuffer, shared_ptr<IndexBuffer> ibuffer,
		shared_ptr<Effect> effect = nullptr);
	~TriMesh();

	// Retreive the number of trianges from within this mesh
	size_t GetTriangleCount() const;

	// Get the index's of the vertices that make up a single triangle 
	bool GetTriangleIndex(size_t pos, uint32_t &i0, uint32_t &i1, uint32_t &i2) const;

protected:
	// protected ctor
	TriMesh();

};

#include "TriangleMesh.inl"

_ENGINE_END

#endif