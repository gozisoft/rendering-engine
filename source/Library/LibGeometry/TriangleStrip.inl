#ifndef __CTRIANGLE_STRIP_INL__
#define __CTRIANGLE_STRIP_INL__

inline size_t engine::TriStrip::GetTriangleCount() const
{
	return m_ibuffer->index_count() - 2;
}

#endif