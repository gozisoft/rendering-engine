#include "libgeometryafx.h"
#include "Polyline.h"
#include "Buffer.h"
#include <stdexcept>

using namespace engine;

CPolyline::CPolyline(VertexBufferPtr vbuffer, shared_ptr<Effect> effect, bool contiguous)
	:
	Visual(contiguous ? CPrimitiveType::PT_POLYLINE_OPEN : CPrimitiveType::PT_POLYLINE_CLOSED, vbuffer, nullptr, effect),
	m_numLines(0),
	m_contiguous(contiguous)
{
	size_t numelements = vbuffer->vertex_count();
	if ( numelements < 2  )
		throw std::runtime_error("Polylines must have at least 2 vertex points [CPolyline::CPolyline]");

	if (m_contiguous)
	{
		m_numLines = numelements - 1;
	}
	else
	{
		// If number of vertices is uneven
		if (numelements & 1)
			throw std::runtime_error("Disconnected lines require an even number of vertices [CPolyline::CPolyline]");

		m_numLines = numelements / 2;
	}
}

void CPolyline::SetNumLines(size_t numLines)
{
	size_t numVertices = m_vbuffer->vertex_count();
	if (m_contiguous)
	{
		size_t numVertices1 = numVertices - 1;
		// Number of lines greator than 0
		// Number of lines <= number of vertices
		if (numLines > 0 && numVertices1 > numLines) 
			m_numLines = numLines;
		else
			m_numLines = numVertices1;
	}
	else
	{
		size_t numVertices2 = numVertices / 2;
		// Number of lines greator than 0
		// Number of lines <= number of vertices
		if (numLines > 0 && numVertices2 > numLines) 
			m_numLines = numLines;
		else
			m_numLines = numVertices2;
	}
}