#include "libgeometryafx.h"
#include "Triangle.h"
#include "Buffer.h"
#include "Vector.h"
#include <stdexcept>

using namespace engine;

Triangle::Triangle() 
	: 
Visual()
{}

Triangle::Triangle(CPrimitiveType::Type primativeType, VertexBufferPtr vbuffer,
		IndexBufferPtr ibuffer, shared_ptr<Effect> effect)
		: 
Visual(primativeType, vbuffer, ibuffer, effect)
{

}

Triangle::~Triangle()
{

}

void Triangle::CalculateNormals()
{
	// The memory format of the vertex buffer
	auto format = m_vbuffer->format();

	// Get the vertex buffers memory format
	auto normalElement = format->GetElement(CVertexSemantic::VS_NORMAL);
	if (normalElement == nullptr) // If no normal data, just return.
		return;

	// Get the vertex buffers memory format
	auto posElement = format->GetElement(CVertexSemantic::VS_POSITION);
	if (posElement == nullptr)
		throw std::runtime_error("Invalid position data within this triangle node [Triangle::CalculateNormals]");

	// This will work for [x,y,z] or [x,y,z,w] based data
	element_view<Vector3f> normal_view = CreateElementView<Vector3f>(m_vbuffer.get(), normalElement);

	// This will work for [x,y,z] or [x,y,z,w] based data
	element_view<Vector3f> pos_view = CreateElementView<Vector3f>(m_vbuffer.get(), posElement);

	// Reset normal values
	size_t numVertices = m_vbuffer->vertex_count();
	for (auto i = normal_view.begin(); i != normal_view.end(); ++i)
	{
		(*i) = Vector3f();
	}

	for (size_t i = 0; i < GetTriangleCount(); ++i)
	{
		uint32_t I0 = 0;
		uint32_t I1 = 0;
		uint32_t I2 = 0;

		if ( !GetTriangleIndex(i, I0, I1, I2) )
		{
			continue;
		}

		Vector3f E0 = pos_view[I1] - pos_view[I0];
		Vector3f E1 = pos_view[I2] - pos_view[I0];
		Vector3f Normal = cml::cross(E0, E1);

		normal_view[I0] += Normal;
		normal_view[I1] += Normal;			
		normal_view[I2] += Normal;
	}

	// Normalise all normal vectors.
	for (size_t i = 0; i < numVertices; ++i)
	{
		cml::normalize( normal_view[i] );
	}
}



//void Triangle::UpdateModelSpace(GeoUpdate type)
//{
//	CVisualNode::UpdateModelBound();
//
//	if (type == GU_MODEL_BOUND_ONLY)
//	{
//		return;
//	}
//	
//	CVertexBufferAccess vba(m_pVFormat, m_pVBuffer);
//	if (vba.HasNormal())
//	{
//		CalculateNormals(vba);
//	}
//
//	if (type != GU_NORMALS)
//	{
//		if (vba.HasTangent() || vba.HasBinormal())
//		{
//			if (type == GU_USE_GEOMETRY)
//			{
//				UpdateModelTangentsUseGeometry(vba);
//			}
//			else
//			{
//				UpdateModelTangentsUseTCoords(vba);
//			}
//		}
//	}
//}