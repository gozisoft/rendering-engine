#pragma once
#ifndef __CMATERIAL_H__
#define __CMATERIAL_H__

#include "Core.h"
#include "Colour.h"

_ENGINE_BEGIN

class CMaterial
{
public:
    // Construction and destruction.
    CMaterial ();
    ~CMaterial ();

	Colour4f emissive;  // default: (0,0,0,1)
    Colour4f ambient;   // default: (0,0,0,1)

    // The material alpha is the alpha channel of mDiffuse.
    Colour4f diffuse;   // default: (0,0,0,1)

    // The material specular exponent is in the alpha channel of mSpecular.
    Colour4f specular;  // default: (0,0,0,0)
};

_ENGINE_END

#endif