#include "libgeometryafx.h"
#include "Skeleton.h"
#include "Bone.h"

using namespace engine;

size_t CSkeleton::AddBone(CBonePtr other)
{
	// does the pointer exist
	if ( other->GetParent() ) 
		throw std::runtime_error("Bone has already got a skeleton [CSkeleton::AddBone]");

	// search for empty slot and insert child to that pos.
	auto it = std::find_if(std::begin(m_bones), std::end(m_bones),
		[](const CBonePtr& bone)
	{
		return bone == nullptr;
	});

	// all slot used, so add child to end of array.
	it = m_bones.insert(it, other);

	// Set the child as having this root as a parent
	other->m_skeleton = shared_from_this();

	// Determine the index location of the new iterator
	size_t index = std::distance(std::begin(m_bones), it);
	other->m_index = index; // Set the handle of the bone so it knows where it lives

	// return the index
	return index;
}

void CSkeleton::RemoveBone(size_t index)
{
	if ( index >= m_bones.size() )
		throw std::out_of_range("Index out of bounds [CSkeleton::RemoveBone]");

	size_t i = 0;
	auto itor = std::begin(m_bones);
	for (/**/; itor != std::end(m_bones); ++itor, ++i)
	{
		if (i == index)
			break;
	}

	// Found the bone, lets erase it
	m_bones.erase(itor);
}

void CSkeleton::RemoveBone(CBonePtr other)
{
	// search for empty slot and insert child to that pos.
	auto it = std::find_if(std::begin(m_bones), std::end(m_bones),
		[&other](const CBonePtr& bone)
	{
		return bone == other;
	});

	if (it == std::end(m_bones))
		throw std::runtime_error("Bone not found within the skeleton [CSkeleton::RemoveBone]");
		
	// Found the bone, lets erase it
	m_bones.erase(it);
}