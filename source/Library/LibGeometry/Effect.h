#pragma once
#ifndef __CEFFECT_H__
#define __CEFFECT_H__

#include "Core.h"
#include "librendererfwd.h"

_ENGINE_BEGIN

class Effect
{
public:
	virtual ~Effect() { }

	virtual void apply(std::shared_ptr<IPipeline> const&  pipeline) = 0;

	/*// Update the constants when apply is called.
	virtual void updateConstants(void* data) = 0;

	// Update the constants right now and set 
	// them later when apply is called.
	virtual void updateConstantsImmediate(IPipelinePtr pipeline, void* dataIn) = 0;*/
};

_ENGINE_END

#endif