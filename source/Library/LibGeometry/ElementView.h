#pragma once

#ifndef __ELEMENT_VIEW_H__
#define __ELEMENT_VIEW_H__

#include "Core.h"
#include <iterator>
#include <cassert>

_ENGINE_BEGIN

// Custom iterator
template < class Type >
class interleaved_iterater : public std::iterator<std::bidirectional_iterator_tag, Type>
{
public:
	typedef interleaved_iterater<Type> my_iter;
	typedef std::iterator<std::bidirectional_iterator_tag, Type> my_base;

	typedef typename my_base::iterator_category iterator_category;
	typedef typename my_base::value_type value_type;
	typedef typename my_base::difference_type difference_type;
	// typedef typename my_base::distance_type distance_type;	// retained
	typedef typename my_base::pointer pointer;
	typedef typename my_base::reference reference;

	// Stride is in bytes
	interleaved_iterater (pointer data, size_t stride_bytes) 
		:
	ptr(data), stride(stride_bytes)
	{
	}

	interleaved_iterater (const my_iter& other) 
		: 
	ptr(other.ptr), stride(other.stride)
	{
	}

	// preincrement
	my_iter& operator++ () 
	{ 
		// ptr = (pointer)( (uint8_t*)ptr + stride );
		((uint8_t*&)ptr) += stride;
		return *this;
	}

	// postincrement
	my_iter operator++ (int)
	{
		my_iter tmp(*this);
		++(*this);
		return tmp;
	}

	// predecrement
	my_iter& operator-- () 
	{ 
		((uint8_t*&)ptr) -= stride;
		return *this;
	}

	// postdecrement
	my_iter operator-- (int)
	{
		my_iter tmp(*this);
		--*this;
		return tmp;
	}

	// check equal
	bool operator == (const my_iter& rhs) const
	{
		return (ptr==rhs.ptr && stride == rhs.stride);
	}

	// check not equal
	bool operator != (const my_iter& rhs) const
	{
		return !(*this == rhs);
	}

	reference operator* () 
	{
		return *ptr;
	}

	// Pointer to an interleaved data memeber.
	pointer ptr;

	// The stide is in bytes, so whatever type the pointer is in
	// needs to cast to byte form in order to increment etc.
	size_t stride; 
};

// The element access class is capable of reading or writing the correct
// data format for a vertex buffer element. This is well suited for
// interleaved data as offset and data types can be checked.
template < class Type >
class element_view
{
public:
	typedef element_view<Type> my_type;
	typedef Type  value_type;
	typedef Type* pointer;
	typedef Type& reference;
	typedef const pointer const_pointer;
	typedef const reference const_reference;
	typedef std::ptrdiff_t difference_type;
	typedef std::ptrdiff_t distance_type;	// retained

	// Iterator typedefs
	typedef interleaved_iterater<Type> iterator;
	typedef const iterator const_iterator;
	
	/// Requires the element to determine data offsets and various other details.
	/// Data pointer is required for reading and writing.
	/// <stride_bytes> 
	///		Total stride in bytes from one interleaved array block to the next.
	/// </stride_bytes>
	/// <offset_bytes> 
	///		Offset in bytes to the first member of the array block a view
	///		will represent.
	/// </offset_bytes>
	element_view(uint8_t* first, size_t count, size_t stride_bytes, size_t offset_bytes = 0) 
		:
	m_first((pointer)(first+offset_bytes)),
	m_last((pointer)((first+offset_bytes) + (count*stride_bytes))),
	m_count(count),
	m_stride(stride_bytes)
	{
	}

	// Constructor for actual data type
	element_view(pointer first, size_t count, size_t stride_bytes) 
		:
	m_first(first),
	m_last((pointer)((uint8_t*)first + (count*stride_bytes))),
	m_count(count),
	m_stride(stride_bytes)
	{
	}

	// Set's raw data, type is checked but size is not
	const_reference operator[] (size_t index) const
	{
		assert(index < m_count);
		return *reinterpret_cast<const_pointer>(reinterpret_cast<uint8_t*>(m_first) + (index * m_stride));
	}

	// Set's raw data, type is checked but size is not
	reference operator[] (size_t index)
	{
		return const_cast<reference>( static_cast<const my_type&>(*this)[index] );
	}

	// Const access to the first element
	const_iterator begin() const
	{
		return ( const_iterator(m_first, m_stride) );
	}

	// Access the first element
	iterator begin()
	{
		return ( iterator(m_first, m_stride) );
	}

	// return iterator for end of nonmutable sequence
	const_iterator end() const
	{	
		return ( const_iterator(m_last, m_stride) );
	}

	// return iterator for end of mutable sequence
	iterator end()
	{	
		return ( iterator(m_last, m_stride) );
	}

	// return first element of nonmutable sequence
	const_reference front() const
	{	
		return *m_first;
	}

	// return first element of mutable sequence
	reference front()
	{	
		return const_cast<reference>( static_cast<const my_type&>(*this).front() );
	}

	// return last element of nonmutable sequence
	const_reference back() const
	{	
		return ( static_cast<const my_type&>(*this)[m_count - 1] );
	}

	// return last element of mutable sequence
	reference back()
	{	
		return const_cast<reference>( static_cast<const my_type&>(*this).back() );
	}

	//// Insert elements, first to last, into the element view
	//template < class InputIterator >
	//void insert(const_iterator position, InputIterator first, InputIterator last)
	//{
	//	difference_type distance = std::distance(begin(), position);
	//	for (first != last)
	//	{
	//	}
	//	size_t offset = position - begin();
	//	_Insert(_Where, _First, _Last, _Iter_cat(_First));
	//	return (begin() + _Off);
	//}

private:
	pointer m_first;					// Points to the first element of it's type within the vbuffer
	pointer m_last;						// Points to the last element of it's type within the vbuffer
	size_t m_stride;					// The stride between elements (in bytes)
	size_t m_count;						// The number of elements
};


_ENGINE_END


#endif

//// The element access class is capable of reading or writing the correct
//// data format for a vertex buffer element. This is well suited for
//// interleaved data as offset and data types can be checked.
//template < typename Type >
//class CElementAccess
//{
//public:
//	// Iterator typedefs
//	typedef InterleavedIterator<Type> iterator;
//	typedef const iterator const_iterator;
//
//	// Requires the element to determine data offsets and various other details.
//	// Data pointer is required for reading and writing
//	CElementAccess(size_t stride, size_t count, size_t channels, CDataType::Type type,
//		uint8_t* data);
//
//	// Set's raw data, type is checked but size is not
//	void push_back(const Type& input);
//
//	// Set's raw data, type is checked but size is not
//	template < class Type >
//	void SetData(size_t index, Type input);
//
//	// Set's raw data, type is and size is checked
//	template < class Type >
//	void SetData(size_t index, T elementfirst, T elementlast);
//
//	// Set's raw data, type is and size is checked
//	template < class Array, class Function >
//	void SetArray(Array first, Array last, Function func);
//
//	// Sets a single variable using a maths library
//	template < typename T >
//	T& Variable(size_t index);
//
//
//private:
//	size_t m_stride;						// The stride between elements (in bytes)
//	size_t m_count;						// The number of instances of this element
//	size_t m_channels;					// The number of channels the element occupies
//	CDataType::Type m_type;				// The elements data type
//	uint8_t* m_data;						// Points to the first element of it's type within the vbuffer
//	uint8_t* m_end;
//};