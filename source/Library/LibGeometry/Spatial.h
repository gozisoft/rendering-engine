#pragma once

#ifndef SPATIAL_H
#define SPATIAL_H

#include "libgeometryfwd.h"
#include "Transform.h"
#include "Sphere.h"
#include <vector>

_ENGINE_BEGIN

class Spatial : public std::enable_shared_from_this<Spatial>
{
public:
	// Spatial cull modes
	enum CullMode
	{
		// Determine visibility state by comparing the world bounding volume
		// to culling planes.
		CM_DYNAMIC,
		// Force the object to be culled.  If a Node is culled, its entire
		// subtree is culled.
		CM_ALWAYS,
		// Never cull the object.  If a Node is never culled, its entire
		// subtree is never culled.  To accomplish this, the first time such
		// a Node is encountered, the bNoCull parameter is set to 'true' in
		// the recursive chain GetVisibleSet/OnGetVisibleSet.
		CM_NEVER,
		CM_COUNT
	};
	
	// Public typedefs
	typedef std::vector< shared_ptr<Visual> > VisualNodes;

	// This class is a friend of the root
	friend class VisualGroup;

	// Virtual destructor
	virtual ~Spatial();

	// Updates only controllers and transforms
	virtual void UpdateWorldTransform() = 0;

	// Updates the models world bounding volume
	virtual void UpdateWorldBound() = 0;

	// Used to add children to a list.
	virtual void getAll(VisualNodes& nodes) = 0;

	// Updates the layout of the spatial object.
	// If this is a root object, it will update the layout
	// of it's children. If it is a visual node, it will
	// update itself.
	virtual void UpdateLayout();

	// Returns the absolute root node of this 
	// spatial hierarchy.
	shared_ptr<VisualGroup> getRoot() const;

	// Access to the parent
	shared_ptr<VisualGroup> getParent() const;

	// Const access to the world transform
	const Transform& worldTransform() const;

	// Non const access to the world transform
	Transform& worldTransform();

	// Const access to the world transform
	const Transform& localTransform() const;

	// Non const access to the world transform
	Transform& localTransform();

	// Const access to the world transform
	const Sphere3f& worldBound() const;

	// Non const access the world transform
	Sphere3f& worldBound();

	// Access the cull mode
	CullMode getCullMode() const;

	// Access to the handle
	size_t getHandle() const;

protected:
	// Protected constructor to prevent construction
	// of this class
	Spatial();

	// Transformation data
	Transform m_worldTransform;
	Transform m_localTransform;
	
	// World bounding volume
	Sphere3f m_worldBound;

	// Pointer to parent
	weak_ptr<VisualGroup> m_parent;

	// Cull mode will determine spatial visibility
	CullMode m_cullmode;

	// Index to place in root
	size_t m_handle;
};

#include "Spatial.inl"

_ENGINE_END


#endif