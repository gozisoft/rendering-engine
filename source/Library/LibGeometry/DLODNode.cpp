#include "frameafx.h"
#include "DLODNode.h"
#include "Camera.h"

using namespace engine;

CDlodNode::CDlodNode(UInt numLevelsOfDetail) : 
m_nodeMinMax(numLevelsOfDetail)
{

}

CDlodNode::~CDlodNode()
{
	m_nodeMinMax.clear();
}

void CDlodNode::SetModelDistance(UInt index, float minDist, float maxDist)
{
	assert( index >= 0 && index < m_nodeMinMax.size() && ieS("Invalid index to DLOD mesh") );
	m_nodeMinMax[index].m_localMinDist = minDist;
	m_nodeMinMax[index].m_localMaxDist = maxDist;
	m_nodeMinMax[index].m_worldMinDist = minDist;
	m_nodeMinMax[index].m_worldMaxDist = maxDist;
}

void CDlodNode::SelectLOD(const CCameraPtr& pCamera)
{
	// transform the nodes worldcenter position by world transform
	m_worldLodCenter = transform_point(GetWorldTransform(), m_worldLodCenter);

    // Compute the world squared-distance intervals.
	auto worldSqrdDistInverals = [this](NodeMinMax& minMax)
	{
		minMax.m_worldMinDist = GetWorldTransform().GetUniformScale()*minMax.m_localMinDist;
		minMax.m_worldMaxDist = GetWorldTransform().GetUniformScale()*minMax.m_localMaxDist;
	};
	std::for_each(m_nodeMinMax.begin(), m_nodeMinMax.end(), worldSqrdDistInverals);

	// Select the LOD child.
    m_activeChildIndex = INACTIVE_CHILD;
	float dist = cml::length( m_worldLodCenter - pCamera->GetPosition() );
    for (UInt i = 0; i < m_nodeMinMax.size(); ++i) 
    {
		if (m_nodeMinMax[i].m_worldMinDist <= dist && dist < m_nodeMinMax[i].m_worldMaxDist)
        {
			assert( i < GetChildCount() && ieS("Invalid active child specified") );  
            m_activeChildIndex = i;
            break;
        }
    }
}