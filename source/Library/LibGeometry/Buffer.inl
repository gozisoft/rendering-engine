#ifndef __CBUFFER_INL__
#define __CBUFFER_INL__

_ENGINE_BEGIN




_ENGINE_END

#endif

// ------------------------------------------------------------------------------------------
//// BASE TEXTURE
//// ------------------------------------------------------------------------------------------
//inline size_t CTexture::GetBytesSize() const
//{
//	return m_sizeInBytes;
//}
//
//inline size_t CTexture::GetBufferType() const
//{
//	return m_bufferType;
//}
//
//inline CResourceAccess::Type CTexture::GetUsage() const
//{
//	return m_usage;
//}
//
//inline CTextureFormat::Type CTexture::GetTextureFormat() const
//{
//	return m_textureFormat;
//}
//
//inline CTextureDimension::Type CTexture::GetTextureDimension() const
//{
//	return m_textureDimensions;
//}
//// -----------------------------------------------------------------------------------------------
//// TEXTURE 1D
//// -----------------------------------------------------------------------------------------------
//inline size_t CTexture1D::GetNumMipLevels() const
//{
//	return static_cast<size_t>( m_mipMaps.size() );
//}
//
//inline size_t CTexture1D::GetWidth(size_t level) const
//{
//	return m_mipMaps[level].width;
//}
//
//inline size_t CTexture1D::size(size_t level) const
//{
//	return m_mipMaps[level].numBytes;
//}
//
//inline size_t CTexture1D::offset(size_t level) const
//{
//	return m_mipMaps[level].offset;
//}
//
//inline const SMipMap1D& CTexture1D::GetMipMap(size_t level) const
//{
//	return m_mipMaps[level];
//}
//// -----------------------------------------------------------------------------------------------
//// TEXTURE 2D
//// -----------------------------------------------------------------------------------------------
//inline size_t CTexture2D::GetNumMipLevels() const
//{
//	return static_cast<size_t>( m_mipMaps.size() );
//}
//
//inline size_t CTexture2D::GetWidth(size_t level) const
//{
//	return m_mipMaps[level].width;
//}
//
//inline size_t CTexture2D::GetHeight(size_t level) const
//{
//	return m_mipMaps[level].height;
//}
//
//inline size_t CTexture2D::size(size_t level) const
//{
//	return m_mipMaps[level].numBytes;
//}
//
//inline size_t CTexture2D::offset(size_t level) const
//{
//	return m_mipMaps[level].offset;
//}
//
//inline const SMipMap2D& CTexture2D::GetMipMap(size_t level) const
//{
//	return m_mipMaps[level];
//}



//inline uint8_t* CTexture1D::data(size_t level)
//{
//	if ( !m_data.empty() && level && level < m_mipMaps.size() )
//	{
//		return &m_data[0] + m_mipMaps[level].offset;
//	}
//
//	assert( false &&  ieS("Null pointer or invalid level in data") );
//	return 0;
//}

//inline uint8_t* CTexture2D::data(size_t level) 
//{
//	assert( level < m_mipMaps.size() &&  ieS("Null pointer or invalid level in data") );
//	return &m_data[0] + m_mipMaps[level].offset;
//}


//// ------------------------------------------------------------------------------------------
//// Buffer
//// ------------------------------------------------------------------------------------------
//
//// Used to set the buffer size in advance
//template < class Interface >
//CBuffer<Interface>::CBuffer(size_t sizeInBytes, BufferUse usage, BufferType bufferType, const uint8_t* data) :
//m_bufferType(bufferType),
//m_usage(usage),
//m_data(data, data+sizeInBytes)
//{
//
//}
//
//template < class Interface >
//CBuffer::~CBuffer()
//{
//	m_data.clear();
//}
//
//template < class Interface >
//inline size_t CBuffer<Interface>::GetBytesSize() const
//{
//	return static_cast<size_t>( m_data.size() );
//}
//
//template < class Interface >
//inline size_t CBuffer<Interface>::GetBufferType() const
//{
//	return m_bufferType;
//}
//
//template < class Interface >
//inline CBuffer::BufferUse CBuffer<Interface>::GetUsage() const
//{
//	return m_usage;
//}
//
//template < class Interface >
//inline const uint8_t* CBuffer<Interface>::data() const
//{
//	return &m_data[0];
//}
//
//// ------------------------------------------------------------------------------------------
//// Texture
//// ------------------------------------------------------------------------------------------
//template < class Interface >
//CTexture<Interface>::CTexture(TextureFormat format, TextureType type, size_t sizeInBytes, BufferUse use) : 
//CBuffer(0, use, RU_TEXTURE),
//m_textureFormat(format),
//m_textureType(type)
//{
//
//}
//
//template < class Interface >
//CTexture<Interface>::~CTexture()
//{
//
//}
//
//template < class Interface >
//inline CTexture::TextureFormat CTexture<Interface>::GetTextureFormat() const
//{
//	return m_textureFormat;
//}
//
//template < class Interface >
//inline CTexture::TextureType CTexture<Interface>::GetTextureType() const
//{
//	return m_textureType;
//}