#pragma once
#ifndef __CPOLYLINE_H__
#define __CPOLYLINE_H__

#include "Visual.h"

_ENGINE_BEGIN

class CPolyline : public Visual
{
public:
	// Construction and destruction.
	CPolyline(VertexBufferPtr vbuffer,
		shared_ptr<Effect> effect, bool contiguous);

	// The maximum number of lines
	size_t GetMaxNumLines() const;

	// You can lower the max number of lines but not increase the number
	// higher than that of the number of vertex buffer elements.
	void SetNumLines(size_t numLines); 
	size_t GetNumLines() const;

	// Check if lines are in segments or contiguous
	bool IsContiguous() const;

private:
	// Number of active lines
	size_t m_numLines;

	// Poly lines are in segments or contiguous
	bool m_contiguous;
};

#include "Polyline.inl"

_ENGINE_END // namespace Engine

#endif