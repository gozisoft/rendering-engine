#include "libgeometryafx.h"
#include "Visual.h"
#include "MathsUtils.h"
#include <stdexcept>

using namespace engine;

Visual::Visual(CPrimitiveType::Type type, VertexBufferPtr vbuffer, IndexBufferPtr ibuffer,
	shared_ptr<Effect> effect)
	:
	m_vbuffer(vbuffer),
	m_ibuffer(ibuffer),
	m_effect(effect),
	m_type(type)
{
}

Visual::Visual(CPrimitiveType::Type type, IndexBufferPtr ibuffer, 
	shared_ptr<Effect> effect)
	:
	m_ibuffer(ibuffer),
	m_effect(effect),
	m_type(type)
{
}

Visual::Visual()
{
}

Visual::~Visual()
{
}

void Visual::onDraw(shared_ptr<IPipeline> pipeline)
{
	// Throw an exception as this is not implemented.
}

void Visual::onDraw(shared_ptr<IPipeline> pipeline, const ICamera& camera)
{
	// Throw an exception as this is not implemented.
}

void Visual::UpdateWorldTransform()
{
	// Uses default implementation
	Spatial::UpdateWorldTransform();
}

void Visual::UpdateWorldBound()
{
	// Transform the center pos by the world transform
	m_worldBound.m_center
		= cml::transform_point(m_worldTransform.matrix(), m_worldBound.m_center);

	// Scale the radius to the size of the object in worldspace
	m_worldBound.m_radius = m_worldTransform.getScale()*m_worldBound.m_radius;
}

void Visual::getAll(VisualNodes& nodes)
{
	for (auto node : nodes)
	{
		// If this guy is already in the array, return.
		if (this == node.get())
			return;
	}

	auto this_ptr = std::static_pointer_cast<Visual>(shared_from_this());
	nodes.push_back(this_ptr);
}

// Updates only the local bounding volumes
void Visual::UpdateLocalBound()
{
	const size_t numVertices = m_vbuffer->vertex_count();
	const uint8_t* pData = m_vbuffer->data();

	// Check the node has position elements
	shared_ptr<CFVFormat> format = m_vbuffer->format();
	if (const FVFElement * element = format->GetElement(CVertexSemantic::VS_POSITION))
	{
		if (element->dataType() != CDataType::DT_FLOAT && element->channels() > 4)
		{
			throw std::runtime_error("Position element can only be 3 or 4 float channels [CVisualNode::UpdateLocalBound]");
		}

		// Calculate center of the m_localBound - average value of all the vertex positions.
		m_localBound.m_center = cml::zero_3D();
		for (size_t i = 0; i < numVertices; ++i)
		{
			const float *pos =
				reinterpret_cast<const float*>((pData + element->offset()) + (i * format->totalStride()));

			m_localBound.m_center[0] += pos[0];
			m_localBound.m_center[1] += pos[1];
			m_localBound.m_center[2] += pos[2];
		}

		// Same as [SumOfAllVerts / numVerts]
		float invNumVerts = 1.0f / static_cast<float>(numVertices);
		m_localBound.m_center *= invNumVerts;

		// The radius is the largest distance from the center to the positions.
		// Eqtn of a circle (x - ca)^2 + (y - cb)^2 + (z - cc)^2 = r^2
		// where [ca,cb,cc] are the xyz coords of the center.
		m_localBound.m_radius = 0.0f;
		for (size_t i = 0; i < numVertices; ++i)
		{
			const float *pos = reinterpret_cast<const float*>((pData + element->offset()) + (i * format->totalStride()));
			float difference[3] =
			{
				pos[0] - m_localBound.m_center[0],
				pos[1] - m_localBound.m_center[1],
				pos[2] - m_localBound.m_center[2]
			};
			float radiusSqrd = Sqr(difference[0]) + Sqr(difference[1]) + Sqr(difference[2]);

			// now check if this radius is greater than the last radius
			if (radiusSqrd > m_localBound.m_radius)
			{
				m_localBound.m_radius = radiusSqrd;
			}
		}

		m_localBound.m_radius = Sqrt(m_localBound.m_radius);
	}
}


// Function to update the visual node
//void CVisualNode::Update(double time, double deltaTime, bool initiator)
//{
//	UpdateWorldData(time, deltaTime);
//	UpdateWorldBound();
//	if (initiator)
//	{
//		UpdateFromRoot();
//	}
//}