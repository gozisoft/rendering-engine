#pragma once
#ifndef __GEOMETRY_TYPES_H__
#define __GEOMETRY_TYPES_H__

#include "core.h"

_ENGINE_BEGIN

// =================================================================================================================
// Buffer types
// =================================================================================================================
class CBufferType
{
public:
	enum Type
	{
		BT_VERTEX,
		BT_INDEX
	};
private:
	CBufferType();
};
// =================================================================================================================
// Fixed vertex format semantics
// =================================================================================================================
class CVertexSemantic 
{
public:
	enum Type
	{
		VS_UNKNOWN = ~0,
		VS_POSITION,				// attr 0
		VS_NORMAL,					// attr 1
		VS_TANGENT,					// attr 2
		VS_BINORMAL,				// attr 3							
		VS_COLOUR,					// attr 4-5
		VS_BLEND_INDICES,			// attr 6
		VS_BLEND_WEIGHTS,			// attr 7
		VS_TEXTURE_COORDINATES,		// attr 8-15
		VS_FOG,						// attr 16
		VS_PSIZE,					// attr 17
		VS_COUNT
	};
private:
	CVertexSemantic();
};



_ENGINE_END

#endif 