#ifndef FIXED_VERTEX_FORMAT_INL
#define FIXED_VERTEX_FORMAT_INL

// ------------------------------------------------------------------------------------------
// FVFElement
// ------------------------------------------------------------------------------------------
inline size_t FVFElement::offset() const
{
    return m_offset;
}

inline size_t FVFElement::index() const
{
    return m_semanticIndex;
}

inline size_t FVFElement::channels() const
{
    return m_numChannels;
}

inline size_t FVFElement::size() const
{
    return CDataType::SizeOfType(m_dataType) * m_numChannels;
}

inline CDataType::Type FVFElement::dataType() const
{
    return m_dataType;
}

inline CVertexSemantic::Type FVFElement::semantic() const
{
    return m_semantic;
}

// Comparison operator for FVFElement's
inline bool operator==(const FVFElement &left, const FVFElement &right)
{
    return left.channels() == right.channels() &&
        left.dataType() == right.dataType() &&
        left.index() == right.index() &&
        left.offset() == right.offset() &&
        left.semantic() == right.semantic() &&
        left.size() == right.size();
}

inline bool operator!=(const FVFElement &left, const FVFElement &right)
{
    return !(left == right);
}

// ------------------------------------------------------------------------------------------
// CFVFormat
// ------------------------------------------------------------------------------------------
inline size_t CFVFormat::totalStride() const
{
    return m_totalStride;
}




//// Bit field setting functions for setting a semantic as on or off.
//inline void CFVFormat::SetEnabled(Buffer::VertexSemantic semantic, bool enable)
//{
//	m_semanticFlags &= ~(1 << semantic); // Use this to turn off a bit, if its in the bitset
//	m_semanticFlags |= ( (!!enable) << semantic ); // Use this to add a new bit or a bit thats already existant
//}
//
//// Check if a field is enabled
//inline bool CFVFormat::IsEnabled(Buffer::VertexSemantic semantic) const
//{
//	return !!( m_semanticFlags & (1 << semantic) );
//}

#endif




