#pragma once

#ifndef ICAMERA_H
#define ICAMERA_H

#include "Core.h"
#include "Matrix.h"
#include <array>

_ENGINE_BEGIN

class ICamera
{
public:
	enum Side : size_t
	{
		S_NEAR,
		S_FAR,
		S_BOTTOM,
		S_TOP,
		S_LEFT,
		S_RIGHT,
		NUM_PLANES
	};

	virtual ~ICamera() {};

	/// <summary>
	/// Get the view frustum as a raw C array.
	/// </summary>
	virtual const float* frustrum_c() const = 0;

	/// <summary>
	/// Get the view frustum as C++ vector.
	/// </summary>
	virtual std::array<float, NUM_PLANES> frustrum() const = 0;

	/// <summary>
	/// Return the view matrix of this particualr camera
	/// </summary>
	virtual const Matrix4f& viewMatrix() const = 0;

	/// <summary>
	/// Return the projection matrix.
	/// </summary>
	virtual const Matrix4f& projectionMatrix() const = 0;

	/// <summary>
	/// combination of proj*view
	/// </summary>
	virtual const Matrix4f& pProjViewMatrix() const = 0; 
};

_ENGINE_END

#endif
