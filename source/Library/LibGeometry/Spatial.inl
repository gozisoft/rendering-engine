#ifndef SPATIAL_INL
#define SPATIAL_INL

inline const Transform& Spatial::worldTransform() const
{
	return m_worldTransform;
}

inline Transform& Spatial::worldTransform()
{
	return const_cast<Transform&>(static_cast<const Spatial&>(*this).worldTransform());
}

inline const Transform& Spatial::localTransform() const
{
	return m_localTransform;
}

inline Transform& Spatial::localTransform()
{
	return const_cast<Transform&>(static_cast<const Spatial&>(*this).localTransform());
}

inline const Sphere3f& Spatial::worldBound() const
{
	return m_worldBound;
}

inline Sphere3f& Spatial::worldBound()
{
	return const_cast<Sphere3f&>(static_cast<const Spatial&>(*this).worldBound());
}

inline shared_ptr<VisualGroup> Spatial::getParent() const
{
	return m_parent.lock();
}

inline Spatial::CullMode Spatial::getCullMode() const
{
	return m_cullmode;
}

inline size_t Spatial::getHandle() const
{
	return m_handle;
}

#endif