#include "libgeometryafx.h"
#include "VisualGroup.h"
#include "Intersections.h"
#include <algorithm>
#include <stdexcept>

using namespace engine;

VisualGroup::VisualGroup() : Visual()
{

}

VisualGroup::~VisualGroup()
{

}

void VisualGroup::UpdateLayout()
{
	// Update own layout.
	Visual::UpdateLayout();

	// Now update child layouts.
	for (auto child : m_childList)
	{
		if (child != nullptr)
		{
			child->UpdateLayout();
		}
	}
}

void VisualGroup::UpdateWorldTransform()
{
	// Update self's world transform
	Visual::UpdateWorldTransform();

	// Update all child world transforms
	for (auto child : m_childList)
	{
		if (child != nullptr)
		{
			child->UpdateWorldTransform();
		}
	}
}

size_t VisualGroup::add(shared_ptr<Visual> child)
{
	// does the pointer exist
	if (child->getParent() != nullptr) {
		throw std::runtime_error("Child has already got a parent [CRootNode::AddChild]");
	}

	// search for empty slot and insert child to that pos.
	auto it = std::find_if(std::begin(m_childList), std::end(m_childList),
		[](const shared_ptr<Spatial>& spatial)
	{
		return spatial == nullptr;
	});

	// all slot used, so add child to end of array.
	it = m_childList.insert(it, child);

	// Set the child as having this root as a parent
	child->m_parent = 
		std::static_pointer_cast<VisualGroup>(shared_from_this());

	// Determine the index location of the new iterator
	size_t index = std::distance(std::begin(m_childList), it);
	child->m_handle = index; // Set the handle of the bone so it knows where it lives

	// return the index
	return index;
}

size_t VisualGroup::remove(shared_ptr<Visual> child)
{
	// search for empty slot and insert child to that pos.
	auto it = std::find_if(std::begin(m_childList), std::end(m_childList),
		[&child] (const shared_ptr<Spatial>& myChild)
	{
		return myChild == child;
	});
	// Index of where the child is going to be removed from.
	size_t index = 0;

	// If the child was found remove it and calculate the index
	if (it != std::end(m_childList))
	{
		// Determine the index location of the new iterator
		index = std::distance(std::begin(m_childList), it);
		m_childList.erase(it);
	}

	// Return the index
	return index;
}

void VisualGroup::remove(size_t index)
{
	auto it = std::begin(m_childList);
	std::advance(it, index);
	if (it != std::end(m_childList))
	{
		m_childList.erase(it);
	}
}


void VisualGroup::getVisible(VisualNodes& visual_nodes)
{
	for (auto child : m_childList)
	{
		if (child == nullptr) {
			continue;
		}

		// Add our visible item.
		visual_nodes.push_back(child);
	}
}

void VisualGroup::getAll(VisualNodes& nodes)
{
	for (auto node : nodes)
	{
		
	}
}

void VisualGroup::UpdateWorldBound()
{
	// Start with an invalid bound.
	m_worldBound.m_center = Vector3f(0.0f, 0.0f, 0.0f);
	m_worldBound.m_radius = 0.0f;

	for (auto child : m_childList)
	{
		if (child != nullptr)
		{
			// GrowToContain ignores invalid child bounds.  If the world
			// bound is invalid and a child bound is valid, the child
			// bound is copied to the world bound.  If the world bound and
			// child bound are valid, the smallest bound containing both
			// bounds is assigned to the world bound.
			m_worldBound = MergeWithSphere(m_worldBound, child->worldBound());
		}
	}
}



//void CRootNode::UpdateWorldData(double time, double deltaTime)
//{
//	// Update world transforms.
//	if (m_parent.expired() != true)
//	{
//		m_worldTransform *= m_parent.lock()->GetWorldTransform();
//	}
//
//	// Update each child
//	std::for_each( m_childList.begin(), m_childList.end(),
//		std::bind(&CSpatial::Update, time, deltaTime) );
//}

//void CRootNode::GetVisible(CCuller& culler, CullMode cullmode)
//{
//	// Update each child
//	std::for_each( m_childList.begin(), m_childList.end(),
//		std::bind(&CSpatial::GetVisibleSet, culler, cullmode) );
//}