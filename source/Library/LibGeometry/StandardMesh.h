#pragma once
#ifndef __CSTANDARD_MESH_H__
#define __CSTANDARD_MESH_H__

#include "libgeometryfwd.h"
#include "FVFormat.h"

_ENGINE_BEGIN

// Standard mesh is used to create meshe's based on the
// vertex format supplied. The class itself is uncopyable
// as it only holds a reference to the fixed vertex format 
// data member.
class CStandardMesh
{
public:
	// Constructer takes a flexible vertex format
	CStandardMesh(shared_ptr<IRenderer> renderer, shared_ptr<CFVFormat> format);

	// Create a 3D box
	CTriMeshPtr Box(float xExtent, float yExtent, float zExtent);

	// Create a single Triangle
	CTriMeshPtr Triangle(float xExtent, float yExtent, float zExtent);

	// Create a plane
	CTriMeshPtr Plane(float cellSpacing, size_t rows, size_t cols);

	//// Create a disk
	//CTriMeshPtr Disk(uint32_t shellSamples, uint32_t radialSamples, float radius);
	//// Create a sphere
	//CTriMeshPtr Sphere(uint32_t zSamples, uint32_t radialSamples, float radius);
	//// Create a legend of a Torus
	//CTriMeshPtr Torus(uint32_t circleSamples, uint32_t radialSamples, float outerRadius, float innerRadius);

private:
	// This is a non copyable class
	CStandardMesh() { /**/ }
	CStandardMesh& operator = (const CStandardMesh& other);

	// Instance of the renderer.
	shared_ptr<IRenderer> m_renderer;

	// Reference to the data format
	shared_ptr<CFVFormat> m_format;
};


_ENGINE_END


#endif