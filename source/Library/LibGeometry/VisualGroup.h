#pragma once

#ifndef VISUAL_GROUP_H
#define VISUAL_GROUP_H

#include "Visual.h"
// #include "loki\Visitor.h"

_ENGINE_BEGIN

class VisualGroup : public Visual
{
public:
	// Constructor and destructor
	VisualGroup();
	virtual ~VisualGroup();

	// Ensures that all positions of child objects of a CRootNode
	// are properly updated for layout.
	virtual void UpdateLayout() override;

	// Update the transform of the root node
	virtual void UpdateWorldTransform() override;

	// Update all the child nodes bounding boxes and own
	// bounding box.
	virtual void UpdateWorldBound() override;

	// Return all children belonging to this
	// root node and the children of any of it's
	// child root nodes.
	virtual void getAll(VisualNodes& nodes) override;

	// Get all visual items in this root node.
	virtual void getVisible(VisualNodes& visual_nodes);

	// Sub mesh addition returns an index/handle
	virtual size_t add(shared_ptr<Visual> child);

	// Remove a sub mesh via the pointer
	virtual size_t remove(shared_ptr<Visual> child);

	// Removal of submesh via index
	virtual void remove(size_t index);

	// Return a child at the specified index
	virtual shared_ptr<Visual> getChild(size_t index) const;

	// Return all children of this root node.
	virtual const VisualNodes& getChildList() const;

	// Get the number of children belonging to this group.
	virtual size_t childCount() const;

protected:
	// The array of submeshes
	VisualNodes m_childList;

};

#include "VisualGroup.inl"	


_ENGINE_END

#endif


//namespace Detail 
//{
//	typedef boost::variant<
//		shared_ptr<Visual>,
//		CRootNodePtr,
//		CCameraNodeFPSPtr,
//		CDlodNodePtr> SpatialVariant;
//
//	class ValidityCheck : public boost::static_visitor<CSpatialPtr>
//	{
//	public:
//		template < typename T >
//		CSpatialPtr operator() (const T& variable) {
//			return std::static_pointer_cast<CSpatial>(variable);
//		}
//	};
//
//	class UpdateVisitor : public boost::static_visitor<void>
//	{
//	public:
//		UpdateVisitor(double time, double deltaTime) : 
//		m_time(time),
//		m_deltaTime(deltaTime) { }
//
//		template < class spatial >
//		void operator() (const spatial& child) const {
//			child->Update(m_time, m_deltaTime, false);
//		}
//
//	private:
//		double m_time;
//		double m_deltaTime;
//	};
//
//struct CullingVisitor : public boost::static_visitor<CVisualNode*>
//{
//	CullingVisitor(CCuller& culler, bool notCull) : 
//	m_culler(culler),
//	m_notCull(notCull) { }
//
//	CVisualNode* operator() (const CRootNodePtr& pRoot) const
//	{
//		CSpatial::CullMode cullMode = pRoot->GetCullMode();
//
//		if (cullMode == CSpatial::CM_ALWAYS)
//			return nullptr;
//
//		if (cullMode == CSpatial::CM_NEVER)
//			m_notCull = true;
//
//		// Apply culling testing against "test worthy" planes
//		if ( m_notCull || m_culler.IsVisible( pRoot->GetWorldBound() ) )
//		{
//			pRoot->GetVisible(m_culler, m_notCull);
//		}
//
//		return nullptr;
//	}
//
//	CVisualNode* operator() (const CSceneNodePtr& pSceneNode) const
//	{
//		CSpatial::CullMode cullMode = pSceneNode->GetCullMode();
//
//		if (cullMode == CSpatial::CM_ALWAYS)
//			return nullptr;
//
//		if (cullMode == CSpatial::CM_NEVER)
//			m_notCull = true;
//
//		// save plane states
//		UInt8 planeStates = m_culler.GetPlaneStates();
//
//		// Apply culling testing against "test worthy" planes
//		if ( m_notCull || m_culler.IsVisible( pSceneNode->GetWorldBound() ) )
//		{
//			return pSceneNode.get();
//		}
//
//		// apply saved plane states
//		m_culler.SetPlaneStates(planeStates);
//
//		// Nothing found return null
//		return nullptr;
//	}
//
//	// Special case: if this DLODNode has a root node as a child
//	// the root node functor will be called and it will handle
//	// the addition of scenenodes to the visible set
//	CVisualNode* operator() (const CDlodNodePtr& pDlodNode) const
//	{
//		// Select the correct node
//		pDlodNode->SelectLOD( m_culler.GetCamera() );
//		size_t activeNodeIndex = pDlodNode->GetActiveChildIndex();
//		if (activeNodeIndex == CDlodNode::INACTIVE_CHILD)
//		{
//			 return nullptr;
//		}
//
//		// Get the variant by index (we dont know its type)
//		SpatialVariant pChild = pDlodNode->GetChild(activeNodeIndex);
//		return pChild.apply_visitor(*this);
//	}
//
//private:
//	mutable CCuller& m_culler;
//	mutable bool m_notCull;
//};
//
//} // namespace Detail