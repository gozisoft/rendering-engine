#pragma once
#ifndef __CPOLYPOINT_H__
#define __CPOLYPOINT_H__

#include "Visual.h"

_ENGINE_BEGIN

class CPolypoint : public Visual
{
public:
	// Smart pointer types.
	SMART_PTR_TYPES(CPolypoint);

	// Construction and destruction.
	CPolypoint(VertexBufferPtr vbuffer,
		shared_ptr<Effect> effect);

	// Destructor
	~CPolypoint();

	// Max num of points
	size_t GetMaxNumPoints() const;

	// You can lower the max number of points but not increase the number
	// higher than that of the number of vertex buffer elements.
	void SetMaxNumPoints(size_t numPoints);
	size_t GetNumPoints() const;

private:
	// Protected ctor
	CPolypoint();

	// number of active points
	size_t m_numPoints;
};

#include "Polypoint.inl"

_ENGINE_END // namespace Engine

#endif // __CPOLYPOINT_H__