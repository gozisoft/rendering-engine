#include "libgeometryafx.h"
#include "Buffer.h"

using namespace engine;


// -----------------------------------------------------------------------------------------------
// CBuffer
// -----------------------------------------------------------------------------------------------
Buffer::Buffer()
{
}

Buffer::Buffer(unique_ptr<IBuffer>&& hwbuffer, CBufferType::Type type)
	:
	m_type(type),
	m_hwbuffer(std::move(hwbuffer))
{

}

Buffer::Buffer(unique_ptr<IBuffer>&& hwbuffer, CBufferType::Type type,
	size_t sizeInBytes, uint8_t* data)
	:
	m_data(data, data + sizeInBytes),
	m_type(type),
	m_hwbuffer(std::move(hwbuffer))
{

}

Buffer::~Buffer()
{

}

// ------------------------------------------------------------------------------------------
// CVertexBuffer
// ------------------------------------------------------------------------------------------
VertexBuffer::VertexBuffer(unique_ptr<IBuffer>&& vbuffer, shared_ptr<CFVFormat> const& format)
	:
	Buffer(std::move(vbuffer), CBufferType::BT_VERTEX),
	m_count(m_hwbuffer->size() / format->totalStride()),
	m_format(format)
{

}

VertexBuffer::VertexBuffer(unique_ptr<IBuffer>&& vbuffer, shared_ptr<CFVFormat> const& format,
	uint8_t* data)
	:
	Buffer(std::move(vbuffer), CBufferType::BT_VERTEX, vbuffer->size(), data),
	m_count(m_hwbuffer->size() / format->totalStride()),
	m_format(format)
{

}

// -----------------------------------------------------------------------------------------
// CIndexBuffer
// ------------------------------------------------------------------------------------------
shared_ptr<IndexBuffer> IndexBuffer::create(shared_ptr<IRenderer> renderer,
	CResourceAccess::Type access, size_t sizeInBytes)
{
	return nullptr;
}

shared_ptr<IndexBuffer> IndexBuffer::create(shared_ptr<IRenderer> renderer, CResourceAccess::Type access,
	std::vector<uint16_t> const& data)
{
	auto stride = sizeof(std::vector<uint16_t>::value_type);

	// Create the hardware index buffer.
	auto indexHwBuffer = renderer->createBuffer(CResourceUse::RU_INDEX,
		CResourceAccess::RA_GPU, stride * data.size(),
		reinterpret_cast<const uint8_t*>(&data[0]));

	return std::make_shared<IndexBuffer>(std::move(indexHwBuffer),
		stride);
}

shared_ptr<IndexBuffer> IndexBuffer::create(shared_ptr<IRenderer> renderer,
	CResourceAccess::Type access,
	std::vector<uint32_t> const& data)
{
	auto stride = sizeof(std::vector<uint32_t>::value_type);

	// Create the hardware index buffer.
	auto indexHwBuffer = renderer->createBuffer(CResourceUse::RU_INDEX,	access,
		stride * data.size(), reinterpret_cast<const uint8_t*>(&data[0]));

	return std::make_shared<IndexBuffer>(std::move(indexHwBuffer),
		stride);
}

IndexBuffer::IndexBuffer(unique_ptr<IBuffer>&& ibuffer, size_t stride)
	:
	Buffer(std::move(ibuffer), CBufferType::BT_INDEX),
	m_count(m_hwbuffer->size() / stride),
	m_stride(stride)
{
}

IndexBuffer::IndexBuffer(unique_ptr<IBuffer>&& ibuffer, size_t stride, uint8_t* data)
	:
	Buffer(std::move(ibuffer), CBufferType::BT_INDEX, ibuffer->size(), data),
	m_count(m_hwbuffer->size() / stride),
	m_stride(stride)
{
}

// -----------------------------------------------------------------------------------------
// CVertexGroup
// -----------------------------------------------------------------------------------------
//CVertexGroup::CVertexGroup()
//{
//
//}
//
//CVertexGroup::CVertexGroup(const VBuffers& vbuffers) 
//	:
//m_vbuffers(vbuffers)
//{
//
//}
//
//CVertexGroup::CVertexGroup(const CVertexBufferPtr* vbuffers, size_t count)
//	:
//m_vbuffers(vbuffers, vbuffers+count)
//{
//
//}
//
//void CVertexGroup::AddBuffer(size_t index, const CVertexBufferPtr& vbuffer)
//{
//	// Check index is within range
//	if (index >= m_vbuffers.size)
//		throw std::out_of_range("index is larger than available slots [CVertexGroup::AddBuffer]");
//
//	auto iter = m_vbuffers.begin();
//	std::advance(iter, index);
//	m_vbuffers.insert(iter, vbuffer);
//}




//// -----------------------------------------------------------------------------------------------
//// BASE TEXTURE
//// -----------------------------------------------------------------------------------------------
//CTexture::CTexture(size_t type, CResourceAccess::Type use, CTextureFormat::Type format,
//		CTextureDimension::Type diemension) : 
//m_sizeInBytes(0),
//m_bufferType(type),
//m_usage(use),
//m_textureFormat(format),
//m_textureDimensions(diemension)
//{
//
//}
//
//CTexture::~CTexture()
//{
//
//}
//// ------------------------------------------------------------------------------------------
//// Texture1D
//// ------------------------------------------------------------------------------------------
//CTexture1D::CTexture1D (size_t type, CTextureFormat::Type tformat, CResourceAccess::Type usage, 
//		size_t width, size_t numLevels) : 
//CTexture(type, usage, tformat, CTextureDimension::TD_1D)
//{
//	// We want to make a series of textures that decrease in size by powers of 2
//	size_t logWidth = Maths::Log2OfPowerOfTwo(width);
//	size_t maxMipLevels = logWidth + 1U;
//
//	// Determine the number of mip levels possible.
//	if (numLevels == 0)
//	{
//		numLevels = maxMipLevels;
//	}
//	else if (numLevels > maxMipLevels)
//	{
//		assert( false && ieS("Invalid Number of levels") );
//	}
//
//	// Does what the function says
//	ComputeMipMapInfo(tformat, width, numLevels, m_mipMaps, m_sizeInBytes);
//}
//
//CTexture1D::CTexture1D (size_t type, CResourceAccess::Type use, CTextureFormat::Type tformat,
//		const SMipMap1D* mipmaps, size_t numLevels) : 
//CTexture(type, use, tformat, CTextureDimension::TD_1D),
//m_mipMaps(mipmaps, mipmaps+numLevels)
//{
//
//
//}
//
//CTexture1D::~CTexture1D()
//{
//
//}
//
//bool CTexture1D::HasMipmaps() const
//{
//	// The first mipmap is the original image src data
//	uint32_t srcWidth = m_mipMaps[0].width;
//
//	// We want to make a series of textures that decrease in size by powers of 2
//	uint32_t logWidth = Maths::Log2OfPowerOfTwo(srcWidth);
//	size_t maxMipLevels = static_cast<size_t>(logWidth + 1);
//	return m_mipMaps.size() == maxMipLevels;
//}
//
//// Support for mipmap generation.
//// Jon Blow articles in Game Developer Magazine:
//// Mipmapping, Part 1: 
//// http://number-none.com/product/Mipmapping,%20Part%201/index.html
//// Mipmapping, Part 2: 
//// http://number-none.com/product/Mipmapping,%20Part%202/index.html
//// Non-Power-of-Two Mipmap Creation: 
//// http://http.download.nvidia.com/developer/Papers/2005/NP2_Mipmapping/NP2_Mipmap_Creation.pdf
//void CTexture1D::ComputeMipMapInfo(CTextureFormat::Type tformat, size_t srcWidth, size_t numMipLevels, 
//		MipMap1DArray& mipInfo, size_t& sizeInBytes)
//{
//	// Floating point coloured textures (they dont support mip-mapping)
//	switch (tformat)
//	{
//	case CTextureFormat::TF_R32G32B32A32_FLOAT:
//	case CTextureFormat::TF_R32G32B32_FLOAT:
//	case CTextureFormat::TF_R32G32_FLOAT:
//		if (numMipLevels > 1)
//		{
//			assert( false && ieS("No mipmaps for 32-bit float textures") );
//			numMipLevels = 1;
//		}
//		break;
//	case CTextureFormat::TF_D24S8:
//		if (numMipLevels > 1)
//        {
//            assert( false && ieS("No such thing as a 1D depth textures") );
//            numMipLevels = 1;
//        }
//		break;
//	case CTextureFormat::TF_BC1_TYPELESS:
//	case CTextureFormat::TF_BC1_UNORM:
//	case CTextureFormat::TF_BC1_UNORM_SRGB:
//	case CTextureFormat::TF_BC2_TYPELESS:
//	case CTextureFormat::TF_BC2_UNORM:
//	case CTextureFormat::TF_BC2_UNORM_SRGB:
//	case CTextureFormat::TF_BC3_TYPELESS:
//	case CTextureFormat::TF_BC3_UNORM:
//	case CTextureFormat::TF_BC3_UNORM_SRGB:
//	case CTextureFormat::TF_BC4_TYPELESS:
//	case CTextureFormat::TF_BC4_UNORM:
//	case CTextureFormat::TF_BC4_SNORM:
//	case CTextureFormat::TF_BC5_TYPELESS:
//	case CTextureFormat::TF_BC5_UNORM:
//	case CTextureFormat::TF_BC5_SNORM:
//	case CTextureFormat::TF_BC6_TYPELESS:
//	case CTextureFormat::TF_BC6_UF16:
//	case CTextureFormat::TF_BC6_SF16:
//	case CTextureFormat::TF_BC7_TYPELESS:
//	case CTextureFormat::TF_BC7_UNORM:
//	case CTextureFormat::TF_BC7_UNORM_SRGB:
//		assert( false && ieS("No DXT compression for 1D textures") );
//		numMipLevels = 1;
//		break;
//	default:
//		// do nothing
//		break;
//	}; // switch
//
//	// Allocate array size for number of mipmaps.
//	mipInfo.resize( (size_t)numMipLevels );
//	size_t width = srcWidth;
//	
//	// Setup mip description values
//	for (size_t level = 0; level < numMipLevels; ++level)
//	{
//		size_t numBytes = BytesPerPixel(tformat)*width;
//
//		mipInfo[level].numBytes = numBytes;
//		mipInfo[level].width = width;
//		mipInfo[level].offset = sizeInBytes;
//
//		// shift width down a power of 2
//		if (width > 1)
//			width >>= 1;
//
//		// Increase the number of bytes
//		sizeInBytes += numBytes;
//	}
//}
//// ------------------------------------------------------------------------------------------
//// Texture2D
//// ------------------------------------------------------------------------------------------
//CTexture2D::CTexture2D (size_t type, CTextureFormat::Type tformat, CResourceAccess::Type usage, 
//		size_t width, size_t height, size_t numLevels) 
//	: 
//CTexture(type, usage, tformat, CTextureDimension::TD_1D)
//{
//	// We want to make a series of textures that decrease in size by powers of 2
//	uint32_t logWidth = Maths::Log2OfPowerOfTwo(width);
//	uint32_t logHeight = Maths::Log2OfPowerOfTwo(height);
//	uint32_t maxMipLevels = (logWidth >= logHeight) ? 
//		logWidth + 1 : logHeight + 1;
//
//	// Determine the number of mip levels possible.
//	if (numLevels == 0)
//	{
//		numLevels = maxMipLevels;
//	}
//	else if (numLevels > maxMipLevels)
//	{
//		assert( false && ieS("Invalid Number of levels") );
//	}	
//
//	// Check for too many levels
//	assert( numLevels <= maxMipLevels && ieS("Invalid Number of levels") );
//	
//	// Compute the mip info for data mapping
//	ComputeMipMapInfo(m_textureFormat, width, height, numLevels, m_mipMaps, m_sizeInBytes);
//}
//
//CTexture2D::CTexture2D (size_t type, CResourceAccess::Type use, CTextureFormat::Type tformat,
//		const SMipMap2D* mipmaps, size_t numLevels)
//	: 
//CTexture(type, use, tformat, CTextureDimension::TD_1D),
//m_mipMaps(mipmaps, mipmaps+numLevels)
//{
//
//
//
//}
//
//CTexture2D::~CTexture2D()
//{
//
//}
//
//bool CTexture2D::HasMipmaps() const
//{
//	// The first mipmap is the original image src data
//	uint32_t srcWidth = m_mipMaps[0].width;
//	uint32_t srcHeght =  m_mipMaps[0].height;
//
//	// We want to make a series of textures that decrease in size by powers of 2
//	uint32_t logWidth = Maths::Log2OfPowerOfTwo(srcWidth);
//	uint32_t logHeight = Maths::Log2OfPowerOfTwo(srcHeght);
//	uint32_t maxMipLevels = (logWidth >= logHeight) ? 
//		logWidth + 1 : logHeight + 1;
//
//	return m_mipMaps.size() == maxMipLevels;
//}
//
//// Support for mipmap generation.
//// Jon Blow articles in Game Developer Magazine:
//// Mipmapping, Part 1: 
//// http://number-none.com/product/Mipmapping,%20Part%201/index.html
//// Mipmapping, Part 2: 
//// http://number-none.com/product/Mipmapping,%20Part%202/index.html
//// Non-Power-of-Two Mipmap Creation: 
//// http://http.download.nvidia.com/developer/Papers/2005/NP2_Mipmapping/NP2_Mipmap_Creation.pdf
//void CTexture2D::ComputeMipMapInfo(CTextureFormat::Type tformat, size_t srcWidth, size_t srcHeight, size_t numMipLevels, 
//		MipMap2DArray& mipInfo, size_t& sizeInBytes)
//{
//	// Floating point coloured textures (they dont support mip-mapping)
//	switch (tformat)
//	{
//	case CTextureFormat::TF_R32G32B32A32_FLOAT:
//	case CTextureFormat::TF_R32G32B32_FLOAT:
//	case CTextureFormat::TF_R32G32_FLOAT:
//		if (numMipLevels > 1)
//		{
//			assert( false && ieS("No mipmaps for 32-bit float textures\n") );
//			numMipLevels = 1;
//		}
//		break;
//	case CTextureFormat::TF_D24S8:
//		if (numMipLevels > 1)
//        {
//            assert( false && ieS("No mipmaps for 2D depth textures\n") );
//            numMipLevels = 1;
//        }
//		break;
//	default: // do nothing
//		break;
//	} // switch
//
//	// Allocate array size for number of mipmaps.
//	mipInfo.resize( (size_t)numMipLevels );
//	size_t width = srcWidth;
//	size_t height = srcHeight;
//
//	// Determine if using BC1 or BC4 compression and its bytes per block size
//	bool usingCompression = false;
//	size_t dxtBytesPerBlock = 16;
//	switch (tformat)
//	{
//	case CTextureFormat::TF_BC1_TYPELESS:
//	case CTextureFormat::TF_BC1_UNORM:
//	case CTextureFormat::TF_BC1_UNORM_SRGB:
//	case CTextureFormat::TF_BC4_TYPELESS:
//	case CTextureFormat::TF_BC4_UNORM:
//	case CTextureFormat::TF_BC4_SNORM:
//		dxtBytesPerBlock = 8;
//		usingCompression = true;
//		break;
//	};
//
//	// Is using DXT
//	if (usingCompression)
//	{
//		for (size_t level = 0; level < numMipLevels; ++level)
//		{
//            size_t numBlocksWide = 0;
//            if (width > 0)
//            {
//                numBlocksWide = std::max(1U, width/4);
//            }
//
//			size_t numBlocksHigh = 0;
//			if (height > 0)
//			{
//				numBlocksHigh = std::max(1U, height/4);
//			}
//
//			size_t numBytes = dxtBytesPerBlock*numBlocksWide*numBlocksHigh;
//			mipInfo[level].numBytes = numBytes;
//			mipInfo[level].width = width;
//			mipInfo[level].height = height;
//			mipInfo[level].offset = sizeInBytes;
//
//			if (width > 1)
//				width >>= 1;
//
//			if (height > 1)
//				height >>= 1;
//
//			// Increment the number of bytes
//			sizeInBytes += numBytes;
//		}
//	}
//	else // Not using DXT
//	{
//		for (size_t level = 0; level < numMipLevels; ++level)
//		{
//			size_t numBytes = BytesPerPixel(tformat)*width*height;
//
//			mipInfo[level].numBytes = numBytes;
//			mipInfo[level].width = width;
//			mipInfo[level].height = height;
//			mipInfo[level].offset = sizeInBytes;
//
//			// shift height and width down a power of 2
//			if (width > 1)
//				width >>= 1;
//
//			if (height > 1)
//				height >>= 1;
//
//			// Increment the number of bytes
//			sizeInBytes += numBytes;
//		}
//	}
//}


//CTexture1D::CTexture1D(const CImage* pImage) : 
//CTexture(TF_NONE, TT_1D, CBuffer::RA_NONE)
//{
//	// We want to make a series of textures that decrease in size by powers of 2
//	size_t width = pImage->GetBound(0);
//	size_t numLevels = pImage->GetMipLevels();
//	size_t logWidth = Maths::Log2OfPowerOfTwo(width);
//	size_t maxMipLevels = logWidth + 1U;
//
//	// Determine the number of mip levels possible.
//	if (numLevels == 0)
//	{
//		// User wants max number of mips
//		// else The user wants the specified number of levels.
//		numLevels = maxMipLevels;
//	}
//	else if (numLevels > maxMipLevels)
//	{
//		assert( false && ieS("Invalid Number of levels") );
//	}
//
//	// Does what the function says
//	ComputeMipMapInfo(width, numLevels);
//
//	// Copy across the entire image data to the texture.
//	// If the image already has mipmaps they will be copied across.
//	if (pImage->data() != nullptr && pImage->GetImageSize() != 0)
//	{
//		ConvertFrom(pImage->GetImageFormat(), pImage->GetImageSize(), pImage->data(), 
//			&m_data[0]);
//	}
//
//	// If the image does not have mipmaps, generate the mips.
//	if ( pImage->IsBitMapped() == false && numLevels > 0 )
//		GenerateMipmaps();
//}

//void CTexture1D::GenerateMipmaps()
//{
//	// construct the first mip map (based on actual image)
//	MipMap1DArray::iterator itor = m_mipMaps.begin();
//	SMipMap1D prevMip = *itor++;
//	
//	for (/***/; itor != m_mipMaps.end(); ++itor)
//	{	
//		const uint32_t numTexels = prevMip.width;
//
//		// Temporary storage for generating mipmaps.
//		using std::vector;
//		vector<Colour4f> rgba(prevMip.width);
//		vector<Colour4f>::pointer pRGBA = &rgba[0];
//
//		// Convert the texels from native format to 32-bit float RGBA.
//		UInt8Vector::pointer pDataPrev = &m_data[prevMip.offset];
//		ConvertFrom(m_textureFormat, numTexels, pDataPrev, pRGBA);
//
//		// Current mip map
//		SMipMap1D& curMip = (*itor);
//
//		// Create the next miplevel in-place.
//		for (size_t i = 0; i < curMip.width; ++i)
//		{
//			size_t base = 2 * i;
//			// cycle through the colours and filter them
//			for (size_t colour = 0; colour < rgba[i].size(); ++colour)
//			{
//				rgba[i][colour] = 0.5f*(rgba[base][colour] + rgba[base+1][colour]);
//			}
//		}
//
//		// Convert back to native format from 32-bit float RGBA
//		const size_t numNextTexels = curMip.width;
//		UInt8Vector::pointer pDataCurrent = &m_data[curMip.offset];
//		ConvertTo(m_textureFormat, numNextTexels, pRGBA, pDataCurrent);
//
//		// reassign the previous mip map
//		prevMip = curMip;
//	}
//}


//CTexture2D::CTexture2D(const CImage* pImage)  : 
//CTexture( MapTextureFormat( pImage->GetImageFormat() ), TT_2D, CBuffer::RA_NONE )
//{
//	// We want to make a series of textures that decrease in size by powers of 2
//	size_t width = pImage->GetBound(0);
//	size_t height = pImage->GetBound(1);
//	size_t logWidth = Maths::Log2OfPowerOfTwo(width);
//	size_t logHeight = Maths::Log2OfPowerOfTwo(height);
//	size_t maxMipLevels = (logWidth >= logHeight) ? 
//		logWidth + 1 : logHeight + 1;
//
//	size_t numLevels = pImage->GetMipLevels();
//
//	// Determine the number of mip levels possible.
//	if (numLevels == 0)
//	{
//		// User wants max number of mips
//		numLevels = maxMipLevels;
//	}
//	else if (numLevels > maxMipLevels)
//	{
//		assert( false && ieS("Invalid Number of levels") );
//	}
//
//	// Compute the mip info for data mapping
//	ComputeMipMapInfo(width, height, numLevels);
//
//	// copy across the entire image data to the texture
//	if (pImage->data() != nullptr && pImage->GetImageSize() != 0)
//	{
//		// Convert the image from its native image data format to one compatiable with the texture
//		ConvertFrom(pImage->GetImageFormat(), pImage->GetImageSize(), pImage->data(), 
//			&m_data[0]);
//	}
//
//	// Determine if to generate mipmaps
//	if ( pImage->IsBitMapped() == false && numLevels > 0 )
//		GenerateMipmaps();
//}

//void CTexture2D::GenerateMipmaps()
//{
//	// construct the first mip map (based on actual image)
//	MipMap2DArray::iterator itor = m_mipMaps.begin();
//	SMipMap2D prevMip = *itor++;
//
//	for (/**/; itor != m_mipMaps.end(); ++itor)
//	{
//		// Current number of texels
//		const uint32_t numTexels = prevMip.width * prevMip.height;
//
//		// Temporary storage for generating mipmaps. 
//		// Note : might be expensive to create vector in loop
//		using std::vector;
//		vector<Colour4f> rgba( (size_t)numTexels );
//		vector<Colour4f>::pointer pRGBA = &rgba[0];
//
//		// Convert the texels from native format to 32-bit float RGBA.
//		UInt8Vector::pointer pDataPrev = &m_data[prevMip.offset];
//		ConvertFrom(m_textureFormat, numTexels, pDataPrev, pRGBA);
//
//		// Current mip map
//		const SMipMap2D& curMip = (*itor);
//
//		// Create the next miplevel in-place.
//		for (uint32_t i1 = 0; i1 < curMip.height; ++i1)
//		{
//			for (uint32_t i0 = 0; i0 < curMip.width; ++i0)
//			{
//				uint32_t pixelIndex = i0 + curMip.width*i1;
//				uint32_t base = 2*(i0 + prevMip.width*i1);
//
//				// cycle through the colours and filter them
//				for (size_t colour = 0; colour < rgba[pixelIndex].size(); ++colour)
//				{
//					rgba[pixelIndex][colour] = 
//						0.25f*(rgba[base][colour] + rgba[base + 1][colour] + 
//						rgba[base + prevMip.width][colour] + rgba[base + prevMip.width + 1][colour]);
//				}
//			}
//		}
//
//		// Convert back to native format from 32-bit float RGBA
//		const uint32_t numNextTexels = curMip.width * curMip.height;
//		UInt8Vector::pointer pDataCurrent = &m_data[curMip.offset];
//		ConvertTo(m_textureFormat, numNextTexels, pRGBA, pDataCurrent);
//
//		// swap over the mipmap
//		prevMip = curMip;
//	}
//}