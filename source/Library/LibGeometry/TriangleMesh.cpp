#include "libgeometryafx.h"
#include "TriangleMesh.h"
#include "Buffer.h"
// #include "Culler.h"

using namespace engine;

TriMesh::TriMesh(VertexBufferPtr vbuffer, IndexBufferPtr ibuffer,
	shared_ptr<Effect> effect)
		: 
Triangle(CPrimitiveType::PT_TRIMESH, vbuffer, ibuffer, effect)
{

}

TriMesh::TriMesh() : Triangle()
{

}

TriMesh::~TriMesh()
{

}

bool TriMesh::GetTriangleIndex(size_t pos, uint32_t &i0, uint32_t &i1, uint32_t &i2) const
{
	if (pos >= 0 && pos < GetTriangleCount())
	{
		size_t stride = m_ibuffer->stride();
		uint8_t* data = (3*pos*stride) + m_ibuffer->data();
		i0 = static_cast<uint32_t>( data[pos+0*stride] );
		i1 = static_cast<uint32_t>( data[pos+1*stride] );
		i2 = static_cast<uint32_t>( data[pos+2*stride] );
		return true;
	}

	return false;
}



//void TriMesh::GetVisibleSet(CCuller& culler, CullMode cullmode)
//{
//    if (cullmode == CM_ALWAYS)
//    {
//        return;
//    }
//
//    UInt8 savePlaneState = culler.GetPlaneStates();
//	if (cullmode == CM_NEVER || culler.IsVisible(m_worldBound))
//    {
//		culler.InsertVisible(this);
//    }
//    culler.SetPlaneStates(savePlaneState);
//}


		//switch ( m_ibuffer->GetStride() )
		//{
		//case sizeof(uint16_t):
		//	{
		//		const uint16_t *pIBuffer = (3 * pos) + reinterpret_cast<const uint16_t*>( m_ibuffer->data() );
		//		i0 = *pIBuffer++;
		//		i1 = *pIBuffer++;
		//		i2 = *pIBuffer;
		//		return true;
		//	}
		//	break;

		//case sizeof(uint32_t):
		//	{
		//		const uint32_t *pIBuffer = (3 * pos) + reinterpret_cast<const uint32_t*>( m_ibuffer->data() );
		//		i0 = *pIBuffer++;
		//		i1 = *pIBuffer++;
		//		i2 = *pIBuffer;
		//		return true;
		//	}
		//	break;
		//};