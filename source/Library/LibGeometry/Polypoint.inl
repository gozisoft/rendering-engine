#ifndef __CPOLYPOINT_INL__
#define __CPOLYPOINT_INL__

inline size_t CPolypoint::GetNumPoints() const
{
	return m_numPoints;
}

inline size_t CPolypoint::GetMaxNumPoints() const
{
	return m_vbuffer->vertex_count();
}

#endif