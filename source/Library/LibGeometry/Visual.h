#pragma once

#ifndef VISUAL_H
#define VISUAL_H

#include "Spatial.h"
#include "Buffer.h"
#include "IPipeline.h"
#include "Sphere.h"

_ENGINE_BEGIN

class Visual : public Spatial
{
public:
	enum GeoUpdate
	{
		GU_MODEL_BOUND_ONLY = 0,
		GU_NORMALS,
		GU_USE_GEOMETRY,
		GU_USE_TCOORD_CHANNEL
	};

	// Virtual constructor
	virtual ~Visual();

	// Function to be overloaded by child classes.
	virtual void onDraw(shared_ptr<IPipeline> pipeline);

	// Function to be overloaded by child classes.
	virtual void onDraw(shared_ptr<IPipeline> pipeline, const ICamera& camera);

	// Updates only controllers and transforms
	virtual void UpdateWorldTransform() override;

	// Updates the models world bounding volume through transforming
	// it by the world transform.
	virtual void UpdateWorldBound() override;

	// Puts this visual node into the array of spatial objects.
	virtual void getAll(VisualNodes& nodes) override;

	// Visitor function for applying effect to visual element.
	// virtual void applyEffect(CEffectPtr effect);

	// Update the models local bounding volume
	void UpdateLocalBound();

	// Vertex buffer to make up the mesh, its shared between
	// multiple submesh's via index buffers
	shared_ptr<VertexBuffer> getVBuffer() const;

	// Read access to the index buffer
	shared_ptr<IndexBuffer> getIBuffer() const;

	// Public access to the effect instance.
	shared_ptr<Effect> effect() const;

	// Get visual objects type.
	CPrimitiveType::Type getType() const;

	// The bounding volume of this mesh
	const Sphere3f& getLocalBound() const;

protected:
	// Protected constructor to prevent this objects creation
	Visual(CPrimitiveType::Type type, VertexBufferPtr vbuffer, 
		IndexBufferPtr ibuffer, shared_ptr<Effect> effect);

	// For shared visual nodes that do not own there own index buffer
	Visual(CPrimitiveType::Type type, IndexBufferPtr ibuffer,
		shared_ptr<Effect> effect);

	// Protected default Ctor
	Visual();

	// Geometric data
	shared_ptr<VertexBuffer> m_vbuffer;
	shared_ptr<IndexBuffer> m_ibuffer;

	// The effect to apply to this guy.
	shared_ptr<Effect> m_effect;

	// Bounding volume to model 
	Sphere3f m_localBound;

	// Ranges from Triangles, Quads and Polygon points.
	CPrimitiveType::Type m_type;

	// Pointer to 
};

#include "Visual.inl"

_ENGINE_END



#endif