#pragma once
#ifndef __CTRIANGLE_STRIP_H__
#define __CTRIANGLE_STRIP_H__

#include "Triangle.h"

_ENGINE_BEGIN

class TriStrip : public Triangle
{
public:
	// Smart pointer types.
	SMART_PTR_TYPES(TriStrip);

	// Constructor and destructor
	TriStrip(VertexBufferPtr vbuffer, IndexBufferPtr ibuffer,
		shared_ptr<Effect> effect);

	TriStrip(VertexBufferPtr vbuffer, size_t indexStride);

	~TriStrip();

	// Retreive the number of trianges from within this mesh
	size_t GetTriangleCount() const override;

	// Get the index's of the vertices that make up a single triangle
	bool GetTriangleIndex(size_t pos, uint32_t &i0, uint32_t &i1, uint32_t &i2) const override;

protected:
	TriStrip();

};

_ENGINE_END // namespace Engine

#include "TriangleStrip.inl"

#endif // __CTRIANGLE_STRIP_H__