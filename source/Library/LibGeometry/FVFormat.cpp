#include "libgeometryafx.h"
#include "FVFormat.h"
#include "MapSelect.h"
#include <algorithm>

using namespace engine;

// FVFElement
FVFElement::FVFElement(size_t offset, size_t numChannels, CDataType::Type dataType,
					   CVertexSemantic::Type semantic, size_t semanticIndex)
	:
m_offset(offset),
m_numChannels(numChannels),
m_semanticIndex(semanticIndex),
m_dataType(dataType),
m_semantic(semantic)
{

}

// CFVFormat
CFVFormat::CFVFormat() 
	:
m_totalStride(0)
{

}

// CFVFormat
CFVFormat::~CFVFormat()
{
	m_totalStride = 0;
	m_elementsMap.clear();
}

CFVFormat& CFVFormat::operator ()(size_t numChannels, CDataType::Type dataType, 
		CVertexSemantic::Type semantic)
{
	AddElement(numChannels, dataType, semantic);
	return *this;
}

const FVFElement * CFVFormat::AddElement(size_t numChannels, CDataType::Type dataType, CVertexSemantic::Type semantic)
{
	// Find the group of elements governed by this semantic, or create a new one.
	ElementArray& elements = m_elementsMap[semantic];

	// SemanticIndex is the current index of the array the element occupys
	elements.push_back( new FVFElement(m_totalStride, numChannels, dataType, semantic, elements.size() ) );

	// Access the new element
	const FVFElement * newElement = elements.back();

	// Increment the new total stride
	m_totalStride += newElement->size();

	// Return the new element
	return newElement;
}

void CFVFormat::RemoveElement(CVertexSemantic::Type semantic, size_t semanticIndex)
{
	auto mapItor = m_elementsMap.find(semantic);
	if ( mapItor != std::end(m_elementsMap) )
	{
		// Found the array of elements
		ElementArray& elements = (*mapItor).second;

		// Iterate to the index within the array
		auto arrayItor = elements.begin();
		std::advance(arrayItor, semanticIndex);
		elements.erase(arrayItor);
	}
}

void CFVFormat::RemoveElement(const FVFElement * element)
{
	auto mapItor = m_elementsMap.find( element->semantic() );
	if ( mapItor != std::end(m_elementsMap) )
	{
		ElementArray& elements = (*mapItor).second;
		for (auto arrayItor = elements.begin(); arrayItor != elements.end(); ++arrayItor)
		{
			if ( (*arrayItor) == element )
				elements.erase(arrayItor);
		}
	}
}

const FVFElement * CFVFormat::GetElement(CVertexSemantic::Type semantic, size_t semanticIndex) const
{
	// Find the group of elements governed by this semantic
	auto itor = m_elementsMap.find(semantic);
	if ( itor != m_elementsMap.end() ) // Access the array of elements and add this new one
	{
		const ElementArray& elements = (*itor).second;
		return elements[semanticIndex]; // used for error checking
	}
	
	// nothing was found
	return nullptr;
}

CFVFormat::ElementArray CFVFormat::GetElements(CVertexSemantic::Type semantic) const
{
	// Find the group of elements governed by this semantic
	auto itor = m_elementsMap.find(semantic);
	if ( itor != m_elementsMap.end() )
	{
		const ElementArray& elements = (*itor).second;
		return elements;
	}

	return ElementArray();
}

void CFVFormat::GetElements(CVertexSemantic::Type semantic, FVFElement * const* elemnts,
		size_t* count) const
{
	// Find the group of elements governed by this semantic
	auto itor = m_elementsMap.find(semantic);
	if ( itor != m_elementsMap.end() )
	{
		const ElementArray& elements = (*itor).second;
		elemnts = &elements[0]; // used for error checking
		*count = elements.size();
		return;
	}
	
	// nothing was found
	elemnts = nullptr;
	count = nullptr;
}

size_t CFVFormat::GetElementCount(CVertexSemantic::Type semantic) const
{
	auto itor = m_elementsMap.find(semantic);
	if ( itor != m_elementsMap.end() ) // Access the array of elements and add this new one
	{
		const ElementArray& elements = (*itor).second;
		return elements.size();
	}

	return 0;
}

void CFVFormat::getAllELements(ElementArray& allElements) const
{
	// Concatinate each std::vector found within the map
	//std::transform( m_elementsMap.begin(), m_elementsMap.end(), 
	//	container_back_inserter(allElements),
	//	select2nd<ElementsMap::value_type>() );

	for (auto i = m_elementsMap.begin(); i != m_elementsMap.end(); ++i)
	{
		const ElementArray& elements = (*i).second;
		allElements.insert(allElements.end(), elements.begin(), elements.end());
	}

	// Sort the array by offset values (using default < operator)
	std::sort(std::begin(allElements), std::end(allElements),
			  std::bind(&FVFElement::offset, std::placeholders::_1));
}

void CFVFormat::getAllELements(FVFElement ** elementsOut, size_t count)
{



}