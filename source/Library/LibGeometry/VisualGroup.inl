#ifndef VISUAL_GROUP_INL
#define VISUAL_GROUP_INL

inline shared_ptr<Visual> VisualGroup::getChild(size_t index) const
{
	return m_childList[index];
}

inline const VisualGroup::VisualNodes& VisualGroup::getChildList() const
{
	return m_childList;
}

inline size_t VisualGroup::childCount() const
{
	return m_childList.size();
}


#endif