#include "libgeometryafx.h"
#include "TriangleFan.h"
#include "Buffer.h"

using namespace engine;

TriFan::TriFan(VertexBufferPtr vbuffer, IndexBufferPtr ibuffer, shared_ptr<Effect> effect)
	:
	Triangle(CPrimitiveType::PT_TRIFAN, vbuffer, ibuffer, effect)
{
}

TriFan::TriFan(VertexBufferPtr vbuffer, size_t indexStride)
	:
	Triangle(CPrimitiveType::PT_TRIFAN, vbuffer, nullptr, nullptr)
{
	size_t numelements = vbuffer->vertex_count();

	// Ensure that the number of elements is less than the max size of uint16_t
	if (indexStride == sizeof(uint16_t) && numelements > UINT16_MAX)
	{
		throw std::out_of_range("The number of elements is greater"
			"than the max available index values [TriFan::TriFan]");
	}

	// Create an array of indices
	uint32_t* indices = new uint32_t[numelements];
	for (size_t i = 0; i < numelements; ++i)
		indices[i] = i;

	// Indices are converted from 4 bytes to 1 byte data type (accuracy is preserved)
	// m_ibuffer = std::make_shared<CIndexBuffer>(numelements, indexStride, (uint8_t*)&indices[0]);
}

TriFan::TriFan() : Triangle()
{
}

TriFan::~TriFan()
{
}

bool TriFan::GetTriangleIndex(size_t pos, uint32_t& i0, uint32_t& i1, uint32_t& i2) const
{
	if (pos >= 0 && pos < GetTriangleCount())
	{
		size_t stride = m_ibuffer->stride();
		uint8_t* data = m_ibuffer->data();
		i0 = static_cast<uint32_t>(data[0]);
		i1 = static_cast<uint32_t>(data[pos + 1 * stride]);
		i2 = static_cast<uint32_t>(data[pos + 2 * stride]);
		return true;
	}
	return false;
}


//switch ( m_ibuffer->GetStride() )
//{
//case sizeof(uint16_t):
//	{
//		const uint16_t *pIBuffer = reinterpret_cast<const uint16_t*>( m_ibuffer->data() );
//		i0 = pIBuffer[0];
//		i1 = pIBuffer[pos + 1];
//		i2 = pIBuffer[pos + 2];
//		return true;
//	}
//	break;

//case sizeof(uint32_t):
//	{
//		const uint32_t *pIBuffer = reinterpret_cast<const uint32_t*>( m_ibuffer->data() );
//		i0 = pIBuffer[0];
//		i1 = pIBuffer[pos + 1];
//		i2 = pIBuffer[pos + 2];
//		return true;
//	}
//	break;
//};