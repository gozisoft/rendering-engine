#include "libgeometryafx.h"
#include "Spatial.h"

using namespace engine;

Spatial::Spatial() : m_cullmode(CM_DYNAMIC), m_handle(0)
{

}

Spatial::~Spatial()
{

}

void Spatial::UpdateLayout()
{
	// Update self's world transform and
	// then update the bounding sphere.
	UpdateWorldTransform();
	UpdateWorldBound();
}

shared_ptr<VisualGroup> Spatial::getRoot() const
{
	auto parent = m_parent.lock();
	while (parent != nullptr) {
		parent = parent->getParent();
	}
	return parent;
}

void Spatial::UpdateWorldTransform()
{
	// Update world transforms.
	auto parent = m_parent.lock();
	if (parent != nullptr) {
		m_worldTransform = parent->worldTransform() * m_localTransform;
	}
	else {
		m_worldTransform = m_localTransform;
	}
}
