#ifndef __CTRIANGLE_MESH_INL__
#define __CTRIANGLE_MESH_INL__

inline size_t TriMesh::GetTriangleCount() const
{
	return m_ibuffer->index_count() / 3;
}

#endif