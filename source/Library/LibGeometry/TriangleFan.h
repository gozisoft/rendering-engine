#pragma once
#ifndef __CTRIANGLE_FAN_H__
#define __CTRIANGLE_FAN_H__

#include "Triangle.h"

_ENGINE_BEGIN

class TriFan : public Triangle
{
public:
	// Smart pointer types.
	SMART_PTR_TYPES(TriFan);

	// Constructor that genereates indices
	TriFan(VertexBufferPtr vbuffer, IndexBufferPtr ibuffer, shared_ptr<Effect> effect);

	// Basic constructor
	TriFan(VertexBufferPtr vbuffer, size_t indexStride);

	~TriFan();

	// Retreive the number of trianges from within this mesh
	size_t GetTriangleCount() const;

	// Get the index's of the vertices that make up a single triangle
	bool GetTriangleIndex(size_t pos, uint32_t &i0, uint32_t &i1, uint32_t &i2) const;

protected:
	// protected ctor
	TriFan();

};


#include "TriangleFan.inl"

_ENGINE_END

#endif