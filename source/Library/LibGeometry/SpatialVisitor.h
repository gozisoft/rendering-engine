#pragma once
#ifndef __CSPATIAL_VISITOR_H__
#define __CSPATIAL_VISITOR_H__

#include "libgeometryfwd.h"
#include "Visitor.h"

_ENGINE_BEGIN

class CSpatialVisitor : public Loki::BaseVisitor,
	public Loki::Visitor<VisualGroup>,
	public Loki::Visitor<TriMesh>,
	public Loki::Visitor<TriFan>,
	public Loki::Visitor<TriStrip>,
	public Loki::Visitor<CPolyline>,
	public Loki::Visitor<CPolypoint>
{
public:
   virtual void Visit(VisualGroup&) = 0;	
   virtual void Visit(TriMesh&) = 0;	
   virtual void Visit(TriFan&) = 0;	
   virtual void Visit(TriStrip&) = 0;	
   virtual void Visit(CPolyline&) = 0; 
};




_ENGINE_END

#endif