#pragma once
#ifndef __CTRANSFORM_H__
#define __CTRANSFORM_H__

#include "Core.h"
#include "Vector.h"
#include "Matrix.h"

_ENGINE_BEGIN

class Transform
{
public:
	Transform();
	~Transform();

	// Access resulting matrix
	const Matrix4f& matrix() const;

	// Rotation accessors
	void SetRotation(Matrix4f const& rot);
	const Matrix4f& GetRotation() const;

	// Translation accessors
	void setPosition(const Vector3f& pos);
	const Vector3f& getPosition() const;

	// Scale accessors
	void setScale(float scale);
	float getScale() const;

	// Operator overloads
	Transform operator*(Transform const& right) const;

private:
    // Fill in the entries of m_hMatrix whenever one of the components
    // m_rotation, m_tanslate, or m_scale changes.
    void updateMatrix();

	// Convert a vector 3 to vector 4 with the w component
	// set to zero.
	Vector4f convertW0(Vector3f const& vec) const;
	
	// Data members
	Matrix4f m_matrix;
    Matrix4f m_rotation;	// M (general) or R (rotation)
	Vector3f m_translation; // T (translation)
	float m_scale;			// S (uniform scale)
};

inline Transform::Transform() :
	m_matrix(cml::identity_4x4()),
	m_rotation(cml::identity_4x4()),
	m_translation(),
	m_scale(1.0f)
{
}

inline Transform::~Transform()
{
}

inline const Matrix4f& Transform::matrix() const
{
	return m_matrix;
}

	// Rotation
inline void Transform::SetRotation(const Matrix4f& rot)
{
	m_rotation = rot;
	updateMatrix();
}

inline const Matrix4f& Transform::GetRotation() const
{
	return m_rotation;
}

// Translation
inline void Transform::setPosition(Vector3f const& pos)
{
	m_translation = pos;
	updateMatrix();
}

inline const Vector3f& Transform::getPosition() const
{
	return m_translation;
}

// Scale
inline void Transform::setScale(float scale)
{
	m_scale = scale;
	updateMatrix();
}

inline float Transform::getScale() const
{
	return m_scale;
}

// Matrix-matrix multiplication.
// Order of matrix transform is
// scale, then rotate, then translate.
inline Transform Transform::operator*(Transform const& right) const
{
	// optimisations for identity occasions
	Transform product;
	product.m_rotation = m_rotation * right.m_rotation;
	product.m_scale = m_scale * right.m_scale;

	auto leftT = convertW0(m_translation);
	auto rightT = convertW0(right.m_translation);
	auto trn = m_rotation * (m_scale * rightT);
	trn += leftT;

	// Convert the translation to 4 component for mulitplying with 4x4 matrix.
	product.m_translation[0] = trn[0];
	product.m_translation[1] = trn[1];
	product.m_translation[2] = trn[2];
	product.updateMatrix();
	return product;
}

inline void Transform::updateMatrix()
{
	// Final matrix is scale x rotation.
	m_matrix = m_scale*m_rotation;

	// Set the translate component (not compute, just set it).
	m_matrix.set_basis_element(3, 0, m_translation[0]);
	m_matrix.set_basis_element(3, 1, m_translation[1]);
	m_matrix.set_basis_element(3, 2, m_translation[2]);
	m_matrix.set_basis_element(3, 3, 1.0f);

	/*m_matrix(3, 0) = m_translation[0];
	m_matrix(3, 1) = m_translation[1];
	m_matrix(3, 2) = m_translation[2];*/
	// m_matrix(3, 3) = 1.0f;
}

inline Vector4f Transform::convertW0(Vector3f const& vec) const
{
	return { vec[0], vec[1], vec[2], 0.0f };
}


_ENGINE_END

#endif