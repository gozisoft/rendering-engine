#ifndef VISUAL_INL
#define VISUAL_INL

inline CPrimitiveType::Type Visual::getType() const
{
	return m_type;
}

inline shared_ptr<VertexBuffer> Visual::getVBuffer() const
{
	return m_vbuffer;
}

inline shared_ptr<IndexBuffer> Visual::getIBuffer() const
{
	return m_ibuffer;
}

inline shared_ptr<Effect> Visual::effect() const
{
	return m_effect;
}

inline const Sphere3f& Visual::getLocalBound() const
{
	return m_localBound;
}

#endif