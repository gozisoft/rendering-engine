#include "libgeometryafx.h"
#include "Polypoint.h"

using namespace engine;


CPolypoint::CPolypoint(VertexBufferPtr vbuffer, shared_ptr<Effect> effect) :
Visual(CPrimitiveType::PT_POLYPOINTS, vbuffer, nullptr, effect),
m_numPoints(vbuffer->vertex_count())
{
}

CPolypoint::CPolypoint() :
Visual()
{
}

CPolypoint::~CPolypoint()
{
}

void CPolypoint::SetMaxNumPoints(size_t numPoints)
{
	size_t numVertices = m_vbuffer->vertex_count();
	if (0 <= numPoints && numPoints <= numVertices)
	{
		m_numPoints = numPoints;
	}
	else
	{
		m_numPoints = numVertices;
	}
}