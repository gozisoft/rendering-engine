#ifndef __CPOLYLINES_INL__
#define __CPOLYLINES_INL__

inline size_t CPolyline::GetNumLines() const
{
	return m_numLines;
}

inline bool CPolyline::IsContiguous() const
{
	return m_contiguous;
}

inline size_t CPolyline::GetMaxNumLines() const
{
	int numVertices = m_vbuffer->vertex_count();
	return m_contiguous ? numVertices - 1 : numVertices / 2;
}

#endif