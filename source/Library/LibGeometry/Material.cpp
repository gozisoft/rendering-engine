#include "libgeometryafx.h"
#include "Material.h"

using namespace engine;

CMaterial::CMaterial () :
emissive( alpha_rgba<Vector4f>() ),
ambient( alpha_rgba<Vector4f>() ),
diffuse( alpha_rgba<Vector4f>() ),
specular( black_rgba<Vector4f>() )
{

}

CMaterial::~CMaterial ()
{

}