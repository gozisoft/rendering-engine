#pragma once
#ifndef BUFFER_H__
#define BUFFER_H__

#include "Core.h"
#include "FVFormat.h"
#include "ElementView.h"
#include "IResource.h"
#include "IPipeline.h"
#include "IRenderer.h"

_ENGINE_BEGIN
// -----------------------------------------------------------------------------------------------
// Hardware Buffer
// -----------------------------------------------------------------------------------------------
class Buffer
{
public:
	// No need for a virtual constructor as we only really need to call this one
	virtual ~Buffer();

	// Access to the hardware buffer.
	unique_ptr<IBuffer>& hwbuffer();

	// Function for const access to data
	const uint8_t* data() const;

	// The data total buffer's size in bytes
	size_t size() const;

	// Buffer type determines if its a vertex or index buffer
	CBufferType::Type type() const;

protected:
	// Typedef for array
	typedef std::vector<uint8_t> DataArray;

	// Empty constructor
	Buffer();

	// Protected constructor (without data)
	Buffer(unique_ptr<IBuffer>&& hwbuffer, CBufferType::Type type);

	// Protected constructor (with data)
	Buffer(unique_ptr<IBuffer>&& hwbuffer, CBufferType::Type type, size_t sizeInBytes, uint8_t* data);

	// CPU data
	DataArray m_data;

	// Type of buffer
	CBufferType::Type m_type;

	// Instance of a hardware buffer.
	unique_ptr<IBuffer> m_hwbuffer;
};

// -----------------------------------------------------------------------------------------------
// Vertex Buffer
// -----------------------------------------------------------------------------------------------
class VertexBuffer : public Buffer
{
public:
	// This overrides the data function from cbuffer with a non const type
	using Buffer::data;

	// Factory method for creating a hardware buffer and passing it straight
	// to the proxy class.
	template <typename VertexType>
	static shared_ptr<VertexBuffer> create(shared_ptr<CFVFormat> format,
		std::shared_ptr<IRenderer> renderer,
		CResourceAccess::Type access,
		const std::vector<VertexType>& data);

	template <uint8_t>
	static shared_ptr<VertexBuffer> create(shared_ptr<CFVFormat> format,
		shared_ptr<IRenderer> renderer,
		CResourceAccess::Type access,
		const std::vector<uint8_t>& data);

	// Allocate memory to the vertex buffer prior to filling
	// the buffer space. A bit like std::vector::reserve()
	VertexBuffer(unique_ptr<IBuffer>&& vbuffer, shared_ptr<CFVFormat> const& format);

	// Copy memory to the vertex buffer
	VertexBuffer(unique_ptr<IBuffer>&& vbuffer, shared_ptr<CFVFormat> const& format,
		uint8_t* data);

	// Helper function that allows for a single buffer to 
	// enabled on the rendering pipeline.
	void pipelineEnable(shared_ptr<IPipeline> const& pipeline);

	// Function for non const access to data
	uint8_t* data();

	// The stride in bytes
	// Returns the stride which is calculated by channels * SizeOfType(type).
	// Also known as the size of each vertex { i.e. Vector3f + ColorRBGA = 12 + 16 = 28 stride } 
	size_t stride() const;

	// The number of elements within the Buffer ie 24 Vector3's
	size_t vertex_count() const;

	// Access the format pointer
	shared_ptr<CFVFormat> format() const;

protected:
	// Fixed data format that maps the vertex buffer
	shared_ptr<CFVFormat> m_format;

	// Data members
	size_t m_count; // The number of vertices/indices
	// size_t m_stride; // Size of a single vertex/index
};


// -----------------------------------------------------------------------------------------------
// Index Buffer
// -----------------------------------------------------------------------------------------------
// This index buffer is of uint32_t only as video cards only support
// 32 Bit integer indexing. Possibly in future 64 Bit integer will be added.
// As 32 Bit Integer is the "Optimum" type for an x86 CPU the indices are of
// type Int32. If you want to use 16 Bit Int on the video card, just cast the values over.
class IndexBuffer : public Buffer
{
public:
	// This overides the data function from cbuffer with a non const type
	using Buffer::data;

	// Factory method for creating a hardware buffer and passing it straight
	// to the proxy class.
	static shared_ptr<IndexBuffer> create(shared_ptr<IRenderer> renderer, CResourceAccess::Type access,
		size_t sizeInBytes);

	// Factory method for creating a hardware buffer and passing it straight
	// to the proxy class.
	static shared_ptr<IndexBuffer> create(shared_ptr<IRenderer> renderer,
		CResourceAccess::Type access, const std::vector<uint16_t>& data);

	static shared_ptr<IndexBuffer> create(shared_ptr<IRenderer> renderer,
		CResourceAccess::Type access, const std::vector<uint32_t>& data);

	// Allocate memory to the index buffer prior to filling
	// the buffer space. A bit like std::vector::reserve()
	IndexBuffer(unique_ptr<IBuffer>&& ibuffer, size_t stride);

	// Copy memory to the index buffer
	IndexBuffer(unique_ptr<IBuffer>&& ibuffer, size_t stride, uint8_t* data);

	// Helper function that allows for a single buffer to 
	// enabled on the rendering pipeline.
	void pipelineEnable(shared_ptr<IPipeline> const& pipeline);

	// Function for non const access to data
	uint8_t* data();

	// Retrieve the number of elements
	size_t index_count() const;

	// The stride in bytes
	size_t stride() const;

protected:
	// Data members
	size_t m_count; // The number of vertices/indices
	size_t m_stride; // Size of a single vertex/index
};


template <class ViewType>
element_view<ViewType> CreateElementView(VertexBuffer* vbuffer, const FVFElement * element)
{
	return element_view<ViewType>(
		vbuffer->data(),
		vbuffer->vertex_count(),
		vbuffer->stride(),
		element->offset()
		);
}

template <class ViewType>
element_view<ViewType> CreateElementView(VertexBuffer* vbuffer, CVertexSemantic::Type semantic, size_t index = 0)
{
	const FVFElement * element = vbuffer->format()->GetElement(semantic, index);
	if (!element)
		throw std::runtime_error("No valid element exists for chosen semantic [GetView]");

	return CreateElementView<ViewType>(vbuffer, element);
}

// ------------------------------------------------------------------------------------------
// CBuffer
// ------------------------------------------------------------------------------------------
inline unique_ptr<IBuffer>& Buffer::hwbuffer()
{
	return m_hwbuffer;
}

inline const uint8_t* Buffer::data() const
{
	return &m_data[0];
}

inline size_t Buffer::size() const
{
	return m_data.size();
}

inline CBufferType::Type Buffer::type() const
{
	return m_type;
}


// -----------------------------------------------------------------------------------------------
// CVertexBuffer
// -----------------------------------------------------------------------------------------------
template <typename VertexType>
shared_ptr<VertexBuffer> VertexBuffer::create(shared_ptr<CFVFormat> format,
	shared_ptr<IRenderer> renderer,
	CResourceAccess::Type access,
	const std::vector<VertexType>& data)
{
	auto begin = reinterpret_cast<const uint8_t*>(&data[0]);
	auto end = begin + (sizeof(VertexType) * data.size());
	std::vector<uint8_t> byte_data(begin, end);

	auto hwBuffer = renderer->createBuffer(CResourceUse::RU_VERTEX,
		access, byte_data.size(), &byte_data[0]);

	return std::make_shared<VertexBuffer>(std::move(hwBuffer), format);
}

template <uint8_t>
shared_ptr<VertexBuffer> VertexBuffer::create(shared_ptr<CFVFormat> format,
	shared_ptr<IRenderer> renderer,
	CResourceAccess::Type access,
	const std::vector<uint8_t>& data)
{
	auto hwBuffer = renderer->createBuffer(CResourceUse::RU_VERTEX,
		access, data.size(), &data[0]);

	return std::make_shared<VertexBuffer>(hwBuffer, format);
}

inline void VertexBuffer::pipelineEnable(shared_ptr<IPipeline> const& pipeline)
{
	// Enable vertex buffer.
	const IBuffer* hwbuffer = m_hwbuffer.get();
	size_t stride = m_format->totalStride();
	size_t offset = 0;

	pipeline->enableVertexBuffers(&hwbuffer, 1, 0, &stride, &offset);
}

inline uint8_t* VertexBuffer::data()
{
	return const_cast<uint8_t*>(static_cast<const Buffer&>(*this).data());
}

inline size_t VertexBuffer::stride() const
{
	return m_format->totalStride();
}

inline size_t VertexBuffer::vertex_count() const
{
	return m_count;
}

inline shared_ptr<CFVFormat> VertexBuffer::format() const
{
	return m_format;
}

// -----------------------------------------------------------------------------------------------
// CIndexBuffer
// -----------------------------------------------------------------------------------------------
inline void IndexBuffer::pipelineEnable(shared_ptr<IPipeline> const& pipeline)
{
	// Enable vertex buffer.
	auto indexBuffer = m_hwbuffer.get();
	pipeline->enableIndexBuffer(indexBuffer, m_stride, 0);
}

inline uint8_t* IndexBuffer::data()
{
	return const_cast<uint8_t*>(static_cast<const Buffer&>(*this).data());
}

inline size_t IndexBuffer::index_count() const
{
	return m_count;
}

inline size_t IndexBuffer::stride() const
{
	return m_stride;
}


_ENGINE_END

#include "Buffer.inl"

#endif

//class CVertexGroup
//{
//public:
//	// typedef for array of vertex buffers
//	typedef std::vector<CVertexBufferPtr> buffer_storage;
//
//	typedef buffer_storage* pointer;
//	typedef const buffer_storage* const_pointer;
//
//	typedef buffer_storage& reference;
//	typedef const buffer_storage& const_reference;
//
//	// Default ctor
//	CVertexGroup();
//
//	// Construct from an array of vertex buffers
//	CVertexGroup(const_reference vbuffers);
//
//	// C array style constructor for of vertex buffers
//	CVertexGroup(const CVertexBufferPtr* vbuffers, size_t count);
//
//	// For list_of addition of buffers
//	CVertexGroup& operator ()(size_t index, const CVertexBufferPtr& vbuffer);
//
//	// For single addition of a buffer
//	void AddBuffer(size_t index, const CVertexBufferPtr& vbuffer);
//
//	// Access the buffers
//	const_reference GetBuffers() const;
//
//private:
//	// Vertex buffers, each one indexed to match a 
//	// input slot on an input layout description
//	buffer_storage m_vbuffers;
//};

// -----------------------------------------------------------------------------------------------
// BASE TEXTURE
// -----------------------------------------------------------------------------------------------
// RENDERING TO A TEXTURE
// creating empty textures (default constructor)
// The most common case of creating an empty texture to be filled with data during 
// runtime is the case where an application wants to render to a texture and then 
// use the results of the rendering operation in a subsequent pass. 
// Textures created with this purpose should specify default CBuffer::GPU.
//class CTexture
//{
//	RTTI_DECL;
//public:
//	// Virtual destructor
//	virtual ~CTexture();
//
//	// The data total buffer's size in bytes
//	size_t GetBytesSize() const;
//
//	// Buffer type determines if its a vertex or index buffer
//	size_t GetBufferType() const;
//
//	// Describes the buffers usage with relation to the hardware
//	CResourceAccess::Type GetUsage() const;
//
//	// The textures format
//	CTextureFormat::Type GetTextureFormat() const;
//
//	// The number of dimensions of the texture
//	CTextureDimension::Type GetTextureDimension() const;
//
//	// Check if the texture format is compressed
//	bool IsCompressed() const;
//
//protected:
//	// Constructors (protected)
//	CTexture(size_t type, CResourceAccess::Type use, CTextureFormat::Type format,
//		CTextureDimension::Type diemension);
//
//	// Data members
//	size_t						m_sizeInBytes;	 // The number of bytes within the buffer
//	size_t						m_bufferType;	 // The buffer type. Multiple types can be combined.
//	CResourceAccess::Type			m_usage;		 // The buffer use on the GPU side of things
//
//	// Texture information
//	CTextureFormat::Type		m_textureFormat;
//	CTextureDimension::Type		m_textureDimensions;
//};
//// -----------------------------------------------------------------------------------------------
//// Texture 1D
//// -----------------------------------------------------------------------------------------------
//class CTexture1D : public CTexture, public ITexture1D
//{
//	RTTI_DECL;
//public:
//	// typedef for an array of mip map descriptions
//	typedef std::vector<SMipMap1D> MipMap1DArray;
//
//	// virtual destructor
//	virtual ~CTexture1D();
//
//	// Return the number of mip map levels
//	size_t GetNumMipLevels() const;
//
//    // Get the dimensions of the zero-th level mipmap.
//    size_t GetWidth(size_t level) const;
//
//    // Get the number of bytes used on this level mipmap.
//    size_t size(size_t level) const;
//
//	// Get the offset from the previous level
//    size_t offset(size_t level) const;
//
//    // Support for mipmap generation.
//	// Non-Power-of-Two Mipmap Creation
//    void GenerateMipmaps();
//    bool HasMipmaps() const;
//
//	// Access to the indiviual mip maps (read only)
//	const SMipMap1D& GetMipMap(size_t level) const;
//
//    // Support for mipmap generation.
//	static void ComputeMipMapInfo(CTextureFormat::Type tformat, size_t srcWidth, size_t numMipLevels, 
//		MipMap1DArray& mipInfo, size_t& sizeInBytes);
//
//protected:
//	// Construction and destruction.
//	CTexture1D (size_t type, CTextureFormat::Type tformat, CResourceAccess::Type usage, 
//		size_t width, size_t numLevels);
//
//	// Special constructor from precomputed mip maps
//	CTexture1D (size_t type, CResourceAccess::Type use, CTextureFormat::Type tformat,
//		const SMipMap1D* mipmaps, size_t numLevels);
//
//	// Stored array of mipmaps
//	// First member is the original image
//	MipMap1DArray m_mipMaps;
//};
//// -----------------------------------------------------------------------------------------------
//// Texture 2D
//// -----------------------------------------------------------------------------------------------
//class CTexture2D : public CTexture, public ITexture2D
//{
//	RTTI_DECL;
//public:
//	// typedef for an array of mip map descriptions
//	typedef std::vector<SMipMap2D> MipMap2DArray;
//
//	// virtual destructor
//	virtual ~CTexture2D();
//
//	// Return the number of mip map levels
//	size_t GetNumMipLevels() const;
//
//    // Get the dimensions of the zero-th level mipmap.
//    size_t GetWidth(size_t level) const;
//
//    // Get the dimensions of the zero-th level mipmap.
//    size_t GetHeight(size_t level) const;
//
//    // Get the number of bytes used on this level mipmap.
//    size_t size(size_t level) const;
//
//	// Get the offset from the previous level
//    size_t offset(size_t level) const;
//
//    // Support for mipmap generation.
//	// Non-Power-of-Two Mipmap Creatio
//    bool HasMipmaps() const;
//
//	// Access to a single mip map (read only)
//	const SMipMap2D& GetMipMap(size_t level) const;
//
//    // Support for mipmap generation.
//    static void ComputeMipMapInfo(CTextureFormat::Type tformat, size_t srcWidth, size_t srcHeight, 
//		size_t numMipLevels, MipMap2DArray& mipInfo, size_t& sizeInBytes);
//
//protected:
//    // Construction and destruction.
//	CTexture2D (size_t type, CTextureFormat::Type tformat, CResourceAccess::Type usage, 
//		size_t width, size_t height, size_t numLevels);
//
//	// Special constructor from precomputed mip maps
//	CTexture2D (size_t type, CResourceAccess::Type use, CTextureFormat::Type tformat,
//		const SMipMap2D* mipmaps, size_t numLevels);
//
//	// Stored array of mipmaps
//	// First member is the original image
//	MipMap2DArray m_mipMaps;
//};