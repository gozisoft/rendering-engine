#pragma once
#ifndef __CTRIANGLE_H__
#define __CTRIANGLE_H__

#include "Visual.h"

_ENGINE_BEGIN

// This class uses template pattern of virtual protected functions,
// CalculateNormals uses a combination of before and after actions
// in order to calculate normals of a triangular mesh
class Triangle : public Visual
{
public:
	// Base class for triangle primitives
	virtual ~Triangle();

	// Update functions commmon for triangles
	void CalculateNormals();
	bool CalculateTangents(uint32_t TCoordUnit, bool overwrite);

protected:
	Triangle();

	Triangle(CPrimitiveType::Type primativeType, VertexBufferPtr vbuffer,
		IndexBufferPtr ibuffer, shared_ptr<Effect> effect);

	// Fetches a triangle using the index buffer of a triangle primitive
	virtual bool GetTriangleIndex(size_t pos, uint32_t &i0, uint32_t &i1,
		uint32_t &i2) const = 0;

	// Returns the number of triangles used to make up the primitive
	virtual size_t GetTriangleCount() const = 0;
};

_ENGINE_END


#endif // __TRIANGLE_H__