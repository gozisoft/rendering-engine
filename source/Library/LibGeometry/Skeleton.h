#ifndef __CSKELETON_H__
#define __CSKELETON_H__

#include "libgeometryfwd.h"

_ENGINE_BEGIN

class CSkeleton : public std::enable_shared_from_this<CSkeleton>
{
public:
	// Typedef for an array of bones
	typedef std::vector<CBonePtr> Bones;

	// Add bones to the array
	size_t AddBone(CBonePtr other);

	// Remove bone from the array via index & index compare
	void RemoveBone(size_t index);

	// Remove the bone via pointer lookup & compare
	void RemoveBone(CBonePtr other);

	// The number of bones added
	size_t NumBones() const;

private:
	Bones m_bones;

};

_ENGINE_END

#endif