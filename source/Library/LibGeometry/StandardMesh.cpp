#include "libgeometryafx.h"
#include "StandardMesh.h"

#include "Buffer.h"
#include "TriangleMesh.h"
#include "Vector.h"

using namespace engine;
using std::make_shared;

struct SimpleVertex
{
	Vector3f Pos;
	Vector2f Tex;
};

CStandardMesh::CStandardMesh(shared_ptr<IRenderer> renderer, shared_ptr<CFVFormat> format)
	:
	m_renderer(renderer),
	m_format(format)
{
}

CTriMeshPtr CStandardMesh::Box(float xExtent, float yExtent, float zExtent)
{
	// Create the array for the vertex buffer data.
	size_t numVertices = 8;
	size_t stride = m_format->totalStride(); // The stride of all the formats
	std::vector<uint8_t> vetices(numVertices * stride);

	// If the format has position data
	auto posElement = m_format->GetElement(CVertexSemantic::VS_POSITION);
	if (posElement)
	{
		// Get a data view to the positions
		element_view<Vector3f> pos_view = element_view<Vector3f>(&vetices[0], numVertices,
			stride, posElement->GetOffset());

		// Allocate vertex position data
		pos_view[0] = { -xExtent, yExtent, -zExtent };
		pos_view[1] = { xExtent, yExtent, -zExtent };
		pos_view[2] = { xExtent, -yExtent, -zExtent };
		pos_view[3] = { -xExtent, -yExtent, -zExtent };

		pos_view[4] = { -xExtent, yExtent, zExtent  };
		pos_view[5] = { xExtent, yExtent, zExtent   };
		pos_view[6] = { xExtent, -yExtent, zExtent  };
		pos_view[7] = { -xExtent, -yExtent, zExtent };
	}

	// If the format has texture data
	auto textCoordElement = m_format->GetElement(CVertexSemantic::VS_TEXTURE_COORDINATES);
	if (textCoordElement)
	{
		// Get a data view to the positions
		element_view<Vector2f> tex_view = element_view<Vector2f>(&vetices[0], numVertices,
			stride, textCoordElement->GetOffset());

		// Allocate texture mapping data
		tex_view[0] = { 0.0f, 0.0f };
		tex_view[1] = { 1.0f, 0.0f };
		tex_view[2] = { 1.0f, 1.0f };
		tex_view[3] = { 0.0f, 1.0f };

		tex_view[4] = { 0.0f, 0.0f };
		tex_view[5] = { 1.0f, 0.0f };
		tex_view[6] = { 1.0f, 1.0f };
		tex_view[7] = { 0.0f, 1.0f };
	}

	// Create hardware vertex buffers
	auto vertexHwBuffer = m_renderer->createBuffer(CResourceUse::RU_VERTEX,
		CResourceAccess::RA_GPU, vetices.size(), &vetices[0]);

	// Data buffer for the vertices
	auto vbuffer = make_shared<VertexBuffer>(std::move(vertexHwBuffer), m_format);

	// CVertexBufferPtr vbuffer = make_shared<CVertexBuffer>(vertexHwBuffer, m_format);

	uint32_t indices[] =
	{
		3, 1, 0,
		2, 1, 3,

		6, 4, 5,
		7, 4, 6,

		11, 9, 8,
		10, 9, 11,

		14, 12, 13,
		15, 12, 14,

		19, 17, 16,
		18, 17, 19,

		22, 20, 21,
		23, 20, 22
	};

	// Create the hardware index buffer.
	auto indexHwBuffer = m_renderer->createBuffer(CResourceUse::RU_INDEX,
		CResourceAccess::RA_GPU, sizeof(uint32_t) * sizeof_array(indices),
		reinterpret_cast<uint8_t*>(&indices[0]));

	// Create the index buffer
	auto ibuffer = make_shared<IndexBuffer>(std::move(indexHwBuffer), 
		sizeof(uint32_t));

	// return the mesh
	return make_shared<TriMesh>(vbuffer, ibuffer);
}

CTriMeshPtr CStandardMesh::Triangle(float xExtent, float yExtent, float zExtent)
{
	size_t numVertices = 3;
	size_t stride = m_format->totalStride(); // The stride of all the formats

	SimpleVertex vertices[] =
	{
		{ { -xExtent, -yExtent, zExtent }	, { 0.0f, 0.0f } },
		{ { 0.0f, yExtent, zExtent }		, { 0.5f, 1.0f } },
		{ { xExtent, -yExtent, zExtent }	, { 1.0f, 0.0f } } 
	};

	// Create hardware vertex buffers
	auto vertexHwBuffer = m_renderer->createBuffer(CResourceUse::RU_VERTEX,
		CResourceAccess::RA_GPU, stride, reinterpret_cast<uint8_t*>(&vertices[0]));

	// Data buffer for the vertices
	auto vbuffer = make_shared<VertexBuffer>(std::move(vertexHwBuffer), m_format);

	// Create index buffer
	uint32_t indices[] = { 0, 1, 2, };

	// Create the hardware index buffer.
	auto indexHwBuffer = m_renderer->createBuffer(CResourceUse::RU_INDEX,
		CResourceAccess::RA_GPU,
		sizeof_array(indices) * sizeof(uint32_t), 
		reinterpret_cast<uint8_t*>(&indices[0]));

	// Create the index buffer
	auto ibuffer = make_shared<IndexBuffer>(std::move(indexHwBuffer), sizeof(uint32_t));

	// return the mesh
	return std::make_shared<TriMesh>(vbuffer, ibuffer);
}

CTriMeshPtr CStandardMesh::Plane(float cellSpacing, size_t rows, size_t cols)
{
	size_t numVertices = rows * cols;
	size_t stride = m_format->totalStride(); // The stride of all the formats

	// Array to hold byte vertex data.
	std::vector<uint8_t> vertexData(numVertices*stride);

	float halfWidth = (cols - 1) * cellSpacing * 0.5f;
	float halfDepth = (rows - 1) * cellSpacing * 0.5f;

	// Texture coords
	float du = 1.0f / (cols - 1);
	float dv = 1.0f / (rows - 1);

	// Get a data view to the positions
	const CFVFElement* posElement = m_format->GetElement(CVertexSemantic::VS_POSITION);
	if (posElement != nullptr)
	{
		// Generate vertex positions
		auto pos_view = element_view<Vector3f>(&vertexData[0], numVertices,
			stride, posElement->GetOffset());

		for (size_t i = 0; i < rows; ++i)
		{
			float z = halfDepth - i * cellSpacing;
			for (size_t j = 0; j < cols; ++j)
			{
				float x = -halfWidth + j * cellSpacing;
				float y = 0;
				const size_t index = i * cols + j;

				// auto val = Vector3f({ x, 0, z });
				// pos_view[index].set();
			}
		}
	}

	const CFVFElement* textureElement = m_format->GetElement(CVertexSemantic::VS_TEXTURE_COORDINATES);
	if (textureElement != nullptr)
	{
		// Generate vertex positions
		auto texture_view = element_view<Vector2f>(&vertexData[0], numVertices,
			stride, textureElement->GetOffset());

		for (size_t i = 0; i < rows; ++i)
		{
			float z = halfDepth - i * cellSpacing;
			for (size_t j = 0; j < cols; ++j)
			{
				const size_t index = i * cols + j;

				// Stretch texture over grid.
				texture_view[index] = { j * du, i*dv };
			}
		}
	}

	// Get a data view to the colours
	const CFVFElement* colElement = m_format->GetElement(CVertexSemantic::VS_COLOUR);
	if (colElement != nullptr)
	{
		// Generate vertex positions
		auto col_view = element_view<Colour4f>(&vertexData[0], numVertices,
			stride, colElement->GetOffset());

		for (size_t i = 0; i < rows; ++i)
		{
			for (size_t j = 0; j < cols; ++j)
			{
				const size_t index = i * cols + j;
				col_view[index] = { 0.0f, 0.0f, 1.0f, 1.0f };
			}
		}
	}

	// Create hardware vertex buffers
	auto vertexHwBuffer = m_renderer->createBuffer(CResourceUse::RU_VERTEX,
		CResourceAccess::RA_STATIC, stride, reinterpret_cast<uint8_t*>(&vertexData[0]));

	// Data buffer for the vertices
	auto vbuffer = make_shared<VertexBuffer>(std::move(vertexHwBuffer), m_format);

	// Generate indices (outside view).
	size_t numFaces = (rows - 1) * (cols - 1) * 2;
	std::vector<uint32_t> indices(numFaces * 3);
	int k = 0;
	for (size_t i = 0; i < rows - 1; ++i)
	{
		for (size_t j = 0; j < cols - 1; ++j)
		{
			indices[k] = i * cols + j;
			indices[k + 1] = i * cols + j + 1;
			indices[k + 2] = (i + 1) *cols + j;

			indices[k + 3] = (i + 1) * cols + j;
			indices[k + 4] = i * cols + j + 1;
			indices[k + 5] = (i + 1) * cols + j + 1;

			k += 6; // next quad
		}
	}

	// Create the hardware index buffer.
	auto indexHwBuffer = m_renderer->createBuffer(CResourceUse::RU_INDEX,
		CResourceAccess::RA_GPU, indices.size() * sizeof(uint32_t),
		reinterpret_cast<uint8_t*>(&indices[0]));

	// Create the proxy buffer.
	auto ibuffer = make_shared<IndexBuffer>(std::move(indexHwBuffer), sizeof(uint32_t));

	return std::make_shared<TriMesh>(vbuffer, ibuffer);
}

//CMeshPtr CStandardMesh::Box(float xExtent, float yExtent, float zExtent)
//{
//	size_t numVertices = 24;
//	size_t numTriangles = 12;
//	size_t numIndices = 3*numTriangles;
//	size_t stride = m_format->totalStride(); // The stride of all the formats
//
//	// Create vertex buffer
//	SimpleVertex vertices[] =
//	{
//		{ Vector3f( -xExtent, yExtent, -zExtent ), Vector2f( 0.0f, 0.0f ) },
//		{ Vector3f( xExtent, yExtent, -zExtent ), Vector2f( 1.0f, 0.0f ) },
//		{ Vector3f( xExtent, yExtent, zExtent ), Vector2f( 1.0f, 1.0f ) },
//		{ Vector3f( -xExtent, yExtent, zExtent ), Vector2f( 0.0f, 1.0f ) },
//
//		{ Vector3f( -xExtent, -yExtent, -zExtent ), Vector2f( 0.0f, 0.0f ) },
//		{ Vector3f( xExtent, -yExtent, -zExtent ), Vector2f( 1.0f, 0.0f ) },
//		{ Vector3f( xExtent, -yExtent, zExtent ), Vector2f( 1.0f, 1.0f ) },
//		{ Vector3f( -xExtent, -yExtent, zExtent ), Vector2f( 0.0f, 1.0f ) },
//
//		{ Vector3f( -xExtent, -yExtent, zExtent ), Vector2f( 0.0f, 0.0f ) },
//		{ Vector3f( -xExtent, -yExtent, -zExtent ), Vector2f( 1.0f, 0.0f ) },
//		{ Vector3f( -xExtent, yExtent, -zExtent ), Vector2f( 1.0f, 1.0f ) },
//		{ Vector3f( -xExtent, yExtent, zExtent ), Vector2f( 0.0f, 1.0f ) },
//
//		{ Vector3f( xExtent, -yExtent, zExtent ), Vector2f( 0.0f, 0.0f ) },
//		{ Vector3f( xExtent, -yExtent, -zExtent ), Vector2f( 1.0f, 0.0f ) },
//		{ Vector3f( xExtent, yExtent, -zExtent ), Vector2f( 1.0f, 1.0f ) },
//		{ Vector3f( xExtent, yExtent, zExtent ), Vector2f( 0.0f, 1.0f ) },
//
//		{ Vector3f( -xExtent, -yExtent, -zExtent ), Vector2f( 0.0f, 0.0f ) },
//		{ Vector3f( xExtent, -yExtent, -zExtent ), Vector2f( 1.0f, 0.0f ) },
//		{ Vector3f( xExtent, yExtent, -zExtent ), Vector2f( 1.0f, 1.0f ) },
//		{ Vector3f( -xExtent, yExtent, -zExtent ), Vector2f( 0.0f, 1.0f ) },
//
//		{ Vector3f( -xExtent, -yExtent, zExtent ), Vector2f( 0.0f, 0.0f ) },
//		{ Vector3f( xExtent, -yExtent, zExtent ), Vector2f( 1.0f, 0.0f ) },
//		{ Vector3f( xExtent, yExtent, zExtent ), Vector2f( 1.0f, 1.0f ) },
//		{ Vector3f( -xExtent, yExtent, zExtent ), Vector2f( 0.0f, 1.0f ) },
//	};
//
//
//
//	// Data buffer for the vertices
//	CVertexBuffer* vbuffer = new CVertexBuffer(numVertices, stride, m_format);
//
//	// If the format has position data
//	const CFVFElement* element = m_format->GetElement(CVertexSemantic::VS_POSITION);
//	if (element)
//	{
//		// Get a data view to the positions
//		element_view<Vector3f> pos_view = get_element_view<Vector3f>(vbuffer, element);
//
//		// Allocate vertex position data
//		for (size_t i = 0; i < numVertices; ++i)
//			pos_view[i] = vertices[i].Pos;
//	}
//
//	// If the format has texture data
//	element = m_format->GetElement(CVertexSemantic::VS_TEXTURE_COORDINATES);
//	if (element)
//	{
//		// Get a data view to the positions
//		element_view<Vector3f> tex_view = get_element_view<Vector3f>(vbuffer, element);
//
//		// Allocate texture mapping data
//		for (size_t i = 0; i < numVertices; ++i)
//			tex_view[i] = vertices[i].Tex;
//	}
//
//    UInt16 indices[] =
//    {
//        3,1,0,
//        2,1,3,
//
//        6,4,5,
//        7,4,6,
//
//        11,9,8,
//        10,9,11,
//
//        14,12,13,
//        15,12,14,
//
//        19,17,16,
//        18,17,19,
//
//        22,20,21,
//        23,20,22
//    };
//
//	// Create the index buffer
//	uint8_t* indexData = reinterpret_cast<uint8_t*>(indices);
//	CIndexBuffer* ibuffer = new CIndexBuffer(numIndices, sizeof(UInt16), indexData);
//
//	// Create a submesh
//	CSubMeshPtr submesh = std::make_shared<CSubMesh>(CPrimitiveType::PT_TRIMESH,
//		ibuffer, &vbuffer, 1);
//
//	// Create a mesh object
//	CMeshPtr mesh = std::make_shared<CMesh>(&submesh, 1);
//
//	// return the mesh
//	return mesh;
//}