#pragma once
#ifndef __GEOMETRY_FORWARD_H__
#define __GEOMETRY_FORWARD_H__

#include "Core.h"
#include <memory>

_ENGINE_BEGIN

// Interfaces
class ICamera;

// N-Tree
class Spatial;
class Visual;
class VisualGroup;
class TriMesh;
class TriFan;
class TriStrip;
class CPolyline;
class CPolypoint;

// Simple geometric based methods
class FVFElement;
class CFVFormat;
class Buffer;
class VertexBuffer;
class IndexBuffer;

// Inteface for shader program collection.
class Effect;

// Resources
class Transform;

// N-Tree shared_ptr
typedef std::shared_ptr<Spatial> CSpatialPtr;
typedef std::shared_ptr<VisualGroup> CRootNodePtr;
typedef std::shared_ptr<Visual> VisualPtr;
typedef std::shared_ptr<TriMesh> CTriMeshPtr;
typedef std::shared_ptr<TriFan> CTriFanPtr;
typedef std::shared_ptr<TriStrip> CTriStripPtr;
typedef std::shared_ptr<CPolyline> CPolylinePtr;
typedef std::shared_ptr<CPolypoint> CPolypointPtr;

// Resources shared_ptr
typedef std::shared_ptr<FVFElement> CFVFElementPtr;
typedef std::shared_ptr<CFVFormat> CFVFormatPtr;
typedef std::shared_ptr<Buffer> BufferPtr;
typedef std::shared_ptr<VertexBuffer> VertexBufferPtr;
typedef std::shared_ptr<IndexBuffer> IndexBufferPtr;

// Element view types
template < class Type > class element_view;

//typedef element_view<Vector2f> vector2f_view;
//typedef element_view<Vector3f> vector3f_view;
//typedef element_view<Vector4f> vector4f_view;




_ENGINE_END

#endif

	
//class CBaseMesh;
//class CMesh;
//class CSubMesh;
//
//class CSkeleton;
//class CBone;
//
//typedef std::shared_ptr<CBaseMesh> CBaseMeshPtr;
//typedef std::shared_ptr<CMesh> CMeshPtr;
//typedef std::shared_ptr<CSubMesh> CSubMeshPtr;
//
//typedef std::shared_ptr<CSkeleton> CSkeletonPtr;
//typedef std::shared_ptr<CBone> CBonePtr;