#ifndef __IMAGE_TOOLS_INL__
#define __IMAGE_TOOLS_INL__

// Swap the red and blue channeles on a 24 Bit image
// e.g. R8G8B8 to B8G8R8, or vice versa
inline void Swap24BitRB(const UInt8* pColIn, UInt8* pColOut, UInt32 length)
{
	const UInt8* src = pColIn;
	UInt8* dest = pColOut;

	for (UInt32 i = 0; i < length; ++i, src += 3, dest += 3)
	{
		dest[0] = src[2];
		dest[1] = src[1];
		dest[2] = src[0];
	}
}

// Swap the red and blue channeles on a 32 Bit image
// e.g. A8R8G8B8 to A8B8G8R8, or vice versa
inline void Swap32BitRB(const UInt8* pColIn, UInt8* pColOut, UInt32 length)
{
	const UInt8* src = pColIn;
	UInt32* dest = reinterpret_cast<UInt32*>(pColOut);

	for (UInt32 i = 0; i < length; ++i, src += 4)
	{
		*dest++ = ( (src[3] << 24) | (src[0] << 16) | (src[1] << 8) | src[2] );
	}
}

inline UInt8 UpperByte(UInt8 byte)
{
	return static_cast<UInt8>( (byte >> 4) & 0x0F );
}

inline UInt8 LowerByte(UInt8 byte)
{
	return static_cast<UInt8>( byte & 0x0F );
}

inline UInt8 UpperWord(UInt16 word)
{
	return static_cast<UInt8>( (word >> 8) & 0xFF );
}

inline UInt8 LowerWord(UInt16 word)
{
	return static_cast<UInt8>( word & 0xFF );
}

inline UInt16 MakeWord(UInt8 upperbyte, UInt8 lowerbyte)
{
	return ( ( (upperbyte << 8) & 0xFF00 ) | (lowerbyte & 0xFF) );
}

// bit twiddling method for counting used bits
inline ULong Popcnt(ULong i) 
{
	ULong c = 0;
	for(; i; ++c) 
	{
		i &= i - 1;
	}
	return c;
}

inline UInt32 UsedPaletteEntries(UInt16 bitcount)
{
	if ((bitcount >= 1) && (bitcount <= 8))
		return 1 << bitcount;

	return 0;
}

inline Long CalculateLine(Long width, UInt32 bitdepth)  
{
	return ((width * bitdepth) + 7) / 8; // round up to nearest byte
}

inline Long CalculatePitch(Long line) 
{
	return line + 3 & ~3;
}

// Creates a 16 bit A1R5G5B5 color
inline UInt16 RGBA16(UInt32 r, UInt32 g, UInt32 b, UInt32 a)
{
	UInt16 finalCol = 0;
	finalCol |= ( a & 0x80 ) << 8;
	finalCol |= ( r & 0xF8 ) << 7;
	finalCol |= ( g & 0xF8 ) << 2;
	finalCol |= ( b & 0xF8 ) >> 3;
	return finalCol;
}

// Creates a 16 bit A1R5G5B5 color
inline UInt16 RGB16(UInt32 r, UInt32 g, UInt32 b)
{
	return RGBA16(r,g,b);
}

// Converts a 32bit (A8R8G8B8) color to a 16bit (A1R5G5B5) color
inline UInt16 A8R8G8B8toA1R5G5B5(UInt32 colour)
{
	UInt16 finalCol = 0;
	finalCol |= (colour & 0x80000000) >> 16;
	finalCol |= (colour & 0x00F80000) >> 9;
	finalCol |= (colour & 0x0000F800) >> 6;
	finalCol |= (colour & 0x000000F8) >> 3;
	return finalCol;
}

// Lower bits are extended by the sources higher bits.
// 16 bit colour = hhhll -> hhhl lhhh (one colour made of high and low bits)
inline UInt32 A1R5G5B5toA8R8G8B8(UInt16 colour)
{
	UInt32 finalCol = 0;
	finalCol |= ( ~(colour & 0x00008000) >> static_cast<UInt32>(31) ) & 0xFF000000;
	finalCol |= ( (colour & 0x00007C00) << 9 | (colour & 0x00007000) << 4 );
	finalCol |= ( (colour & 0x000003E0) << 6 | (colour & 0x00000380) << 1 );
	finalCol |= ( (colour & 0x0000001F) << 3 | (colour & 0x0000001C) >> 2 );
	return finalCol;	
}

inline UInt16 A8R8G8B8toR5G6B5(UInt32 colour)
{
	UInt16 finalCol = 0;
	finalCol |= (colour & 0x00F80000) >> 8;
	finalCol |= (colour & 0x0000FC00) >> 5;
	finalCol |= (colour & 0x000000F8) >> 3;
	return finalCol;
}

inline UInt32 R5G6B5toA8R8G8B8(UInt16 colour)
{
	UInt32 finalCol = 0;
	finalCol |= 0xFF000000;
	finalCol |= (colour & 0xF800) << 8;
	finalCol |= (colour & 0x07E0) << 5;
	finalCol |= (colour & 0x001F) << 3;
	return finalCol;
}

// Returns A1R5G5B5 Color from R5G6B5 color
inline UInt16 R5G6B5toA1R5G5B5(UInt16 colour)
{
	return 0x8000 | ( ( (colour & 0xFFC0) >> 1 ) | (colour & 0x1F) );
}



#endif