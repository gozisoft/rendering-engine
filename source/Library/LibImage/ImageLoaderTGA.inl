#ifndef __CIMAGE_LOADER_TGA_INL__
#define __CIMAGE_LOADER_TGA_INL__



inline bool CImageLoaderTGA::IsCompressed(const TGAHeader& header) const
{
	return (header.imageType == TGA_RLE_INDEXED || header.imageType == TGA_RLE_RGB || header.imageType == TGA_RLE_GRAYSCALE);
}

inline bool CImageLoaderTGA::IsUncompressed(const TGAHeader& header) const
{
	return (header.imageType == TGA_INDEXED || header.imageType == TGA_RGB || header.imageType == TGA_GRAYSCALE);
}



#endif