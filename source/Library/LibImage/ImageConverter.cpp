#include "imageafx.h"
#include "ImageConverter.h"
#include "ImageTypes.h"

using namespace Engine;

CFormatImage::CFormatImage(const char* format) : m_bytesPerPixel(0)
{
    // Count destination's bits per pixel and the number 
	// of real values within the string.
	UInt bitsPP = 0;
	UInt numberOfValues = 0;
	for (const char *temp = format; *temp; ++temp)
	{
		// Determine if char is a number
		if ('0' <= *temp && *temp <= '9')
		{
			bitsPP += static_cast<UInt>( *temp - '0' );
			++numberOfValues;
		}
	}

	// Set the size of the bitmask array and shift array
	m_bitmask.resize(numberOfValues);
	m_shiftR.resize(numberOfValues);

	// Main loop: trace format_str and calculate masks and shifts.
	UInt bitsPassed = 0;
	UInt currentIndex = 0; // Index for any 'argb' order.
	while (*format)
	{
		char color = *(format++);
		char numberOfBits = *(format++) - '0';  
		UInt sourceColorStart =
			color == 'A' || color == 'X' ? 0 :
			color == 'R' ? 8 :
			color == 'G' ? 16 :
			color == 'B' ? 24 : 32;            
		
		m_bitmask[currentIndex] = CreateBitMask(sourceColorStart, numberOfBits);
		m_shiftR[currentIndex] = bitsPassed - sourceColorStart + (32 - bitsPP);            
		bitsPassed += numberOfBits;
		++currentIndex;
	}       

	// Set the bytes per pixel
	m_bytesPerPixel = bitsPP / 8;
}

UInt32 CFormatImage::CreateBitMask(UInt32 startpos, UInt32 numbits)
{
	// Push on a 1 bit for every number of bits
	UInt32 result = 0;
	for (UInt32 i = 0; i < numbits; i++) 
	{
		result = (result >> 1) | 0x80000000;
	}

	// Shift that bitmask the appropriate number of bits 
	// to the right to act as a mask for the colour channel
	result = result >> startpos;
	return result;
}