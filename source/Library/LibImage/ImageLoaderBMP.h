#ifndef __CIMAGE_LOADER_BMP_H__
#define __CIMAGE_LOADER_BMP_H__

#include "IImageLoader.h"

// one of the better resources on the bitmap file format
// http://www.fileformat.info/format/bmp/egff.htm
_ENGINE_BEGIN

#pragma pack(push)  // push current alignment to stack
#pragma pack(1)     // set alignment to 1 byte boundary

struct BMPFileMagic
{
	UInt16 magic;			// used to identify the BMP file: 0x42 0x4D
};

struct BMPFileHeader
{
	UInt32 size;			// size of the BMP file in bytes
	UInt16 reserved1; 
	UInt16 reserved2;
	UInt32 dataOffSet;		// starting address, of the byte where the bitmap data can be found.
};

struct XYZ
{
	Long x;
	Long y;
	Long z;
};

struct XYZTriple
{
	XYZ xyzRed;
	XYZ xyzGreen;
	XYZ xyzBlue;
};

// Version 5 header.
struct BMPInfoHeader
{
	UInt32		 headerSize;		/* Size of this header in bytes */
	Long		 width; 			/* Image width in pixels */
	Long		 height; 			/* Image height in pixels */
	UInt16		 planes;			/* Number of color planes */
	UInt16		 bitCount;			/* Number of bits per pixel */
	UInt32		 compression;		/* Compression methods used */
	UInt32		 imagesize; 		/* Size of bitmap in bytes */
	Long		 xPelsPerMeter; 	/* Horizontal resolution in pixels per meter */
	Long		 yPelsPerMeter; 	/* Vertical resolution in pixels per meter */
	UInt32		 clrUsed; 			/* Number of colors in the image */
	UInt32		 clrImportant; 		/* Minimum number of important colors */

	// extension from v3 header. Present in v4 and v5	
	UInt32       redMask;			/* Mask identifying bits of red component */
	UInt32       greenMask;			/* Mask identifying bits of green component */
	UInt32       blueMask;			/* Mask identifying bits of blue component */
	UInt32       alphaMask;			/* Mask identifying bits of alpha component */
	UInt32       type;				/* Color space type */
	XYZTriple    endpoints;			/* XYZ coordinate of color endpoint */
	UInt32       gammaRed;			/* Gamma red coordinate scale value */
	UInt32       gammaGreen;		/* Gamma green coordinate scale value */
	UInt32       gammaBlue;			/* Gamma blue coordinate scale value */

	// extension from v4 header. Present in v5
	UInt32       intent;			
	UInt32       profileData;		
	UInt32       profileSize;		
	UInt32       reserved;			
};									

// Colour palette					
struct BGRQuad
{
	UInt8 blue; 
	UInt8 green; 
	UInt8 red; 
	UInt8 reserved; 
};

#pragma pack(pop)   // restore original alignment from stack

class CImageLoaderBMP : public IImageLoader
{
public:	
	enum BMP_COMPRESSION
	{
		BMP_RGB = 0,
		BMP_RLE8,
		BMP_RLE4,
		BMP_BITFIELDS,
		BMP_JPEG, // BI_JPEG and BI_PNG are for printer drivers and are not supported when rendering to the screen. 
		BMP_PNG
	};

	typedef std::vector<UInt8> uint8Array;
	typedef uint8Array::iterator uint8Itor;

	typedef std::vector<UInt32> paletteArray;
	typedef paletteArray::iterator paletteItor;

public:
	CImagePtr LoadImageData(std::ifstream& inFile);
	bool SaveImage(std::ofstream &outFile, const CImage &image);

private:
	void Decompress4bitRLE(uint8Array& pData, Long stride, Long width, Long height);

	void Decompress8bitRLE(uint8Array& pData, Long stride, Long width, Long height);

	void ConvertBitFieldto16bit(const UInt8* pIn, UInt16* pOut,
		UInt16 bpp, Long width, Long height,
		UInt32 redMask, UInt32 greenMask, UInt32 blueMask, UInt32 alphaMask);

	void OutputInfo(const BMPInfoHeader& infoHeader, const String &mode);

};

_ENGINE_END


#endif


