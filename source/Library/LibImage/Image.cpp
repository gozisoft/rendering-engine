#include "imageafx.h"
#include "Image.h"
#include "ImageTools.h"

using namespace Engine;
using namespace Image;

// 1D image
CImage::CImage(CImageFormat::Type fmt, UInt32 bound0, UInt8* pData, size_t imageSize,
	UInt32 numMips) : 
m_format(fmt),
m_dimension(CImageDimensions::ID_1D),
m_numMipLevels(numMips)
{
	m_bound[0] = bound0;
	m_bound[1] = 1;
	m_bound[2] = 1;

	if (imageSize)
	{
		m_pData.resize(imageSize);
	}
	else
	{
		m_pData.resize( bound0*BytesPerPixel(m_format) );
	}

	// If providing data copy it over
	if (pData != nullptr)
		std::memcpy(&m_pData[0], pData, imageSize);
}

// 2D image
CImage::CImage(CImageFormat::Type fmt, UInt32 bound0, UInt32 bound1, UInt8* pData,
	 size_t imageSize, UInt32 numMips) : 
m_format(fmt),
m_dimension(CImageDimensions::ID_2D),
m_numMipLevels(numMips)
{
	m_bound[0] = bound0;
	m_bound[1] = bound1;
	m_bound[2] = 1;

	if (imageSize)
	{
		m_pData.resize(imageSize);
	}
	else
	{
		m_pData.resize( bound0*bound1*BytesPerPixel(m_format) );
	}

	// If providing data copy it over
	if (pData != nullptr)
		std::memcpy(&m_pData[0], pData, imageSize);
}

// 3D image
CImage::CImage(CImageFormat::Type fmt, UInt32 bound0, UInt32 bound1, UInt32 bound2, UInt8* pData,
	size_t imageSize, UInt32 numMips) : 
m_format(fmt),
m_dimension(CImageDimensions::ID_3D),
m_numMipLevels(numMips)
{
	m_bound[0] = bound0;
	m_bound[1] = bound1;
	m_bound[2] = bound2;

	if (imageSize)
	{
		m_pData.resize(imageSize);
	}
	else
	{
		m_pData.resize( bound0*bound1*bound2*BytesPerPixel(m_format) );
	}

	// If providing data copy it over
	if (pData != nullptr)
		std::memcpy(&m_pData[0], pData, imageSize);
}

CImage::~CImage()
{
	m_pData.clear();
}

