#include "imageafx.h"
#include "IImageLoader.h"
#include "ImageLoaderBMP.h"
#include "ImageLoaderTGA.h"
#include "ImageLoaderDDS.h"

using namespace Engine;

IImageLoaderPtr IImageLoader::CreateImageLoader(ImageLoaderType type)
{
	IImageLoaderPtr pImageLoader = IImageLoaderPtr();
	switch (type)
	{
	case ILT_BMP:
		pImageLoader = std::make_shared<CImageLoaderBMP>();
		break;
	case ILT_TGA:
		pImageLoader = std::make_shared<CImageLoaderTGA>() ;
		break;
	case ILT_DDS:
		pImageLoader = std::make_shared<CImageLoaderDDS>() ;
		break;
	default:
		assert("No valid Image type created");
		break;	
	};

	return pImageLoader;
}

IImageLoader::~IImageLoader()
{

}

IImageLoader::IImageLoader()
{
}