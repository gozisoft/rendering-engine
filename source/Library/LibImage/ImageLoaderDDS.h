#ifndef __CIMAGE_LOADER_DDS_H__
#define __CIMAGE_LOADER_DDS_H__

#include "IImageLoader.h"
#include "IImage.h"
//--------------------------------------------------------------------------------------
// dds.h
//
// This header defines constants and structures that are useful when parsing 
// DDS files.  DDS files were originally designed to use several structures
// and constants that are native to DirectDraw and are defined in ddraw.h,
// such as DDSURFACEDESC2 and DDSCAPS2.  This file defines similar 
// (compatible) constants and structures so that one can use DDS files 
// without needing to include ddraw.h.
//--------------------------------------------------------------------------------------
#include <dxgiformat.h>
#include <d3d9.h>
#include <d3d11.h>

_ENGINE_BEGIN

#pragma pack(push)  // push current alignment to stack
#pragma pack(1)     // set alignment to 1 byte boundary

#define DDS_HEADER_FLAGS_TEXTURE        0x00001007  // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT 
#define DDS_HEADER_FLAGS_MIPMAP         0x00020000  // DDSD_MIPMAPCOUNT
#define DDS_HEADER_FLAGS_VOLUME         0x00800000  // DDSD_DEPTH
#define DDS_HEADER_FLAGS_PITCH          0x00000008  // DDSD_PITCH
#define DDS_HEADER_FLAGS_LINEARSIZE     0x00080000  // DDSD_LINEARSIZE

#define DDS_SURFACE_FLAGS_TEXTURE 0x00001000 // DDSCAPS_TEXTURE
#define DDS_SURFACE_FLAGS_MIPMAP  0x00400008 // DDSCAPS_COMPLEX | DDSCAPS_MIPMAP
#define DDS_SURFACE_FLAGS_CUBEMAP 0x00000008 // DDSCAPS_COMPLEX

#define DDS_CUBEMAP_POSITIVEX 0x00000600 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEX
#define DDS_CUBEMAP_NEGATIVEX 0x00000a00 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEX
#define DDS_CUBEMAP_POSITIVEY 0x00001200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEY
#define DDS_CUBEMAP_NEGATIVEY 0x00002200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEY
#define DDS_CUBEMAP_POSITIVEZ 0x00004200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEZ
#define DDS_CUBEMAP_NEGATIVEZ 0x00008200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEZ

#define DDS_CUBEMAP_ALLFACES ( DDS_CUBEMAP_POSITIVEX | DDS_CUBEMAP_NEGATIVEX |\
                               DDS_CUBEMAP_POSITIVEY | DDS_CUBEMAP_NEGATIVEY |\
                               DDS_CUBEMAP_POSITIVEZ | DDS_CUBEMAP_NEGATIVEZ )

#define DDS_FLAGS_VOLUME 0x00200000 // DDSCAPS2_VOLUME

struct DDS_PIXELFORMAT
{
    ULong size;
    ULong flags;
    ULong fourCC;
    ULong RGBBitCount;
    ULong RBitMask;
    ULong GBitMask;
    ULong BBitMask;
    ULong ABitMask;
};

struct DDS_HEADER
{
    ULong size;
    ULong headerFlags;
    ULong height;
    ULong width;
    ULong pitchOrLinearSize;
    ULong depth; // only if DDS_HEADER_FLAGS_VOLUME is set in dwHeaderFlags
    ULong mipMapCount;
    ULong reserved1[11];
    DDS_PIXELFORMAT ddspf;
    ULong surfaceFlags;
    ULong cubemapFlags;
    ULong reserved2[3];
};

struct DDS_HEADER_DXT10
{
    DXGI_FORMAT dxgiFormat;
    D3D11_RESOURCE_DIMENSION resourceDimension;
    UInt32 miscFlag;
    UInt32 arraySize;
    UInt32 reserved;
};

#pragma pack(pop)   // restore original alignment from stack

class CImageLoaderDDS : public IImageLoader
{
public:	
	typedef std::vector<UInt8> uint8Array;
	typedef uint8Array::iterator uint8Itor;

public:
	CImagePtr LoadImageData(std::ifstream& inFile);
	bool SaveImage(std::ofstream &outFile, const CImage &image);

private:
	bool IsBitmask(const DDS_PIXELFORMAT& ddpf, UInt32 r, UInt32 g, UInt32 b, UInt32 a);

	// Convert from DXGI to engine format
	IImage::ImageFormat ConvertToFormat(DXGI_FORMAT dxgiFormat);

	// Convert from D3DFormat to engine format
	IImage::ImageFormat ConvertToFormat(D3DFORMAT d3dFormat);

	// Determine format from DDS_PIXELFORMAT structure
	D3DFORMAT GetD3D9Format(const DDS_PIXELFORMAT& ddpf);

	DXGI_FORMAT Make_SRGB(DXGI_FORMAT format);

	void GetSurfaceInfo(UInt width, UInt height, DXGI_FORMAT fmt,
		UInt& numBytes, UInt& rowBytes, UInt& numRows);

	UInt32 BitsPerPixel(DXGI_FORMAT fmt);
};

#include "ImageLoaderDDS.inl"

_ENGINE_END



#endif