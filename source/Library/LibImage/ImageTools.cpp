#include "imageafx.h"
#include "ImageTools.h"


_ENGINE_BEGIN

namespace Image
{
	void ConvertA1R5G5B5toA8R8G8B8(const UInt8* pColIn, UInt32* pColOut, UInt32 length)
	{
		// convert the 8 bit byte pointer to 16 bit word pointer
		const UInt16* pIn = reinterpret_cast<const UInt16*>(pColIn); 

		for (UInt32 i = 0; i < length; ++i)
		{
			*pColOut++ = A1R5G5B5toA8R8G8B8(*pIn++);
		}
	}

	void ConvertB8G8R8toA8R8G8B8(const UInt8* pColIn, UInt32* pColOut, UInt32 length)
	{
		for (UInt32 i = 0; i < length; ++i, pColIn += 3)
		{
			*pColOut++ = ( 0xFF000000 | (pColIn[0] << 16) | (pColIn[1] << 8) | pColIn[2] );
		}
	}

	void ConvertB8G8R8toA8B8G8R8(const UInt8* pColIn, UInt8* pColOut, UInt32 length)
	{
		for (UInt32 i = 0; i < length; ++i, pColIn += 3)
		{
			*pColOut++ = ( 0xFF000000 | (pColIn[2] << 16) | (pColIn[1] << 8) | pColIn[0] );
		}
	}



	void Convert1to16Bit(const UInt8* pIn, UInt16* pOut, Long width, Long height, Long padding, bool reverse)
	{
		if (!pIn || !pOut)
			return;

		if (reverse)
			pOut += width * height;

		// convert the 1 bit image to a 16 bit image.
		for (Long y = 0; y < height; ++y)
		{
			UInt8 pixel = 0;

			if (reverse)
				pOut -= width;

			for (Long x = 0; x < width; ++x)
			{
				pOut[x] = ( (*pIn >> pixel) & 0x01 ) ? (UInt16)0xFFFF : (UInt16)0x8000;

				if (++pixel > 7)
				{
					pixel = 0;
					++pIn;
				}
			}

			// Did no reach end of byte
			if (pixel != 0)
			{
				pixel = 0;
				++pIn;
			}

			if (!reverse)
				pOut += width;

			pIn += padding;
		}

	}

	void Convert4to16Bit(const UInt8* pIn, UInt16* pOut, const UInt32* pQuad, Long width, Long height, Long padding, bool reverse)
	{
		if (!pIn || !pOut || !pQuad)
			return;

		if (reverse)
			pOut += width * height;

		// convert the 8 bit image to a 16 bit image.
		for (Long y = 0; y < height; ++y)
		{
			bool evenRun = true;
			if (reverse)
				pOut -= width;

			for (Long x = 0; x < width; ++x)
			{
				if (evenRun)
				{
					pOut[x] = A8R8G8B8toA1R5G5B5( pQuad[ UpperByte( static_cast<UInt8>(*pIn) ) ] );
					evenRun = false;
				}
				else
				{
					pOut[x] = A8R8G8B8toA1R5G5B5( pQuad[ LowerByte( static_cast<UInt8>(*pIn) ) ] );
					evenRun = true;
					++pIn;
				}
			}

			if (!reverse)
				pOut += width;

			pIn += padding;
		}
	}


	void Convert8to16Bit(const UInt8* pIn, UInt16* pOut, const UInt32* pQuad, Long width, Long height, Long padding, bool reverse)
	{
		if (!pIn || !pOut || !pQuad)
			return;

		if (reverse)
			pOut += width * height;

		// convert the 8 bit image to a 16 bit image.
		for (Long y = 0; y < height; ++y)
		{
			if (reverse)
				pOut -= width;

			for (Long x = 0; x < width; ++x)
			{
				pOut[x] = A8R8G8B8toA1R5G5B5( pQuad[static_cast<UInt8>(*pIn)] );
				++pIn;
			}

			if (!reverse)
				pOut += width;

			pIn += padding;
		}
	}

	void Convert16to16Bit(const UInt8* pIn, UInt8* pOut, Long width, Long height, Long padding, bool reverse)
	{
		if (!pIn || !pOut)
			return;

		const UInt16* in = reinterpret_cast<const UInt16*>(pIn);
		UInt16* out = reinterpret_cast<UInt16*>(pOut);

		// Padding added to the input pointer
		const Long linewidth = (width + padding);

		// point to end of array
		if (reverse)
			out += width * height;

		for (Long y = 0; y < height; ++y)
		{
			if (reverse)
				out -= width;

			std::memcpy( out, in, width );

			if (!reverse)
				out += width;

			in += linewidth;
		}
	}

	void Output16to16Bit(const UInt8* pIn, UInt8* pOut, Long width, Long height, Long padding,  bool reverse)
	{
		if (!pIn || !pOut)
			return;

		const Long linewidth = (width + padding);

		// point to end of array
		if (reverse)
			pOut += linewidth * height;

		for (Long y = 0; y < height; ++y)
		{
			if (reverse)
				pOut -= linewidth;

			std::memcpy(pOut, pIn, width);

			// add on null terminated strings to make
			// up the dword boundries.
			for (Long i = width; i < linewidth; ++i) 
				pOut[i] = '\0';

			if (!reverse)
				pOut += linewidth;

			pIn += width;
		}
	}

	// this will compensate for negitive height or positive height images.
	// negative height images are stored top down.
	// positive height images are store bottom up. (flipped)
	void Convert24BitTo24Bit(const UInt8* pIn, UInt8* pOut, Long width, Long height, UInt16 bpp, Long padding, bool swapRGB, bool reverse)
	{
		// Bitmaps are stored in reversed order (ie upside down)
		// and in the BGR colour format. Therefore they need to be
		// reverse read and the BGR needs to be swapped to RGB.
		if (!pIn || !pOut)
			return;

		const Long BytesPerPixel = (bpp / 8);
		const Long lineWidth = width * BytesPerPixel;

		if (reverse) // read data in reversed
			pOut += lineWidth * height; // point to end of array.

		for (Long y = 0; y < height; ++y)
		{
			if (reverse)
				pOut -= lineWidth; // move pointer to the left of a line
	
			if (swapRGB) 
			{			
				// swap blue and red components. Images need to be converted from bgr to rgb.
				Swap24BitRB(pIn, pOut, lineWidth);
			}
			else
			{
				std::memcpy(pOut, pIn, lineWidth);
			}

			if (!reverse)
				pOut += lineWidth;

			pIn += (padding + lineWidth);
		}
	}

	void Convert32BitTo32Bit(const UInt8* pIn, UInt8* pOut, Long width, Long height, UInt16 bpp, Long padding, bool swapRGBA, bool reverse)
	{
		if (!pIn || !pOut)
			return;

		const Long BytesPerPixel = (bpp / 8);
		const Long lineWidth = width * BytesPerPixel;

		if (reverse) // read data in reversed
			pOut += lineWidth * height; // point to end of array.

		for (Long y = 0; y < height; ++y)
		{
			if (reverse)
				pOut -= lineWidth; // move pointer to the left of a line

			if (swapRGBA)
			{
				// the channel order to reverseof input
				Swap32BitRB(pIn, pOut, lineWidth);
			}
			else
			{
				std::memcpy(pOut, pIn, lineWidth);
			}

			if (!reverse)
				pOut += lineWidth;

			pIn += (padding + lineWidth);
		}
	}

	void OutputRGBtoBGR(const UInt8* pIn, UInt8* pOut, Long width, Long height, UInt16 bpp, Long padding, bool swapRGB, bool reverse)
	{
		// Bitmaps are stored in reversed order (ie upside down)
		// and in the BGR colour format. Therefore they need to be
		// reverse read and the BGR needs to be swapped to RGB.
		if (!pIn || !pOut)
			return;

		const Long BytesPerPixel = (bpp / 8);
		const Long lineWidth = width + padding;

		if (reverse) // read data in reversed
			pOut += lineWidth * height; // point to end of array.

		for (Long y = 0; y < height; ++y)
		{
			if (reverse)
				pOut -= lineWidth; // move pointer to the left of a line

			std::memcpy(pOut, pIn, width);

			for (Long i = width; i < lineWidth; ++i)
				pOut[i] = '\0';

			if (swapRGB)
			{
				// swap blue and red components. Images need to be converted from bgr to red green and blue.
				for (Long x = 0; x < lineWidth; x += BytesPerPixel)
				{
					UInt8 blue	= pOut[x+0];
					UInt8 red	= pOut[x+2];
					pOut[x+0]	= red;
					pOut[x+2]	= blue;
				}
			}

			if (!reverse)
				pOut += lineWidth;

			pIn += width;
		}
	}

} // namespace Image


_ENGINE_END // namespace Engine