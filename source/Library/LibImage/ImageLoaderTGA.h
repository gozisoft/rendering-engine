#ifndef __CIMAGE_LOADER_TGA_H__
#define __CIMAGE_LOADER_TGA_H__

#include "IImageLoader.h"


// The best resource I could find on the TGA file spec
// http://tfcduke.developpez.com/tutoriel/format/tga/fichiers/tga_specs.pdf

_ENGINE_BEGIN

#pragma pack(push)  // push current alignment to stack
#pragma pack(1)     // set alignment to 1 byte boundary

struct TGAHeader
{
	UInt8 idLength;			// Size of Image ID field
	UInt8 colourType;		// Color map type
	UInt8 imageType;		// Image type

	// Color Map Specification
	// 2 bytes : index of first colour map entry
	// 2 bytes : total number of colour map entries included
	// 1 byte : number of bits per entry e.g 15, 16, 24, 32.

	UInt16 cmFirstEntry;	// first entry index
	UInt16 cmLength;		// color map length
	UInt8  cmSize;		    // color map entry size, in bits

	// Image specification
	UInt16 xorigin;
	UInt16 yorigin;
	UInt16 width;
	UInt16 height;
	UInt8  bpp;
	UInt8  imageDesc;
};

struct TGAFooter
{
	ULong extensionOffset;
	ULong developerOffset;
	char signature[18];
};

#pragma pack(pop)   // restore original alignment from stack


class CImageLoaderTGA : public IImageLoader
{
public:	
	typedef std::vector<UInt8> uint8Array;
	typedef uint8Array::iterator uint8Itor;

	typedef std::vector<UInt32> colourMapArray;
	typedef colourMapArray::iterator colourMapItor;

public:
	// These masks are AND'd with the imageDesc in the TGA header,
	// bit 4 is left-to-right ordering
	// bit 5 is top-to-bottom
	enum ImageOrientation
	{
		IO_BOTTOM_LEFT = 0x00,	// first pixel is bottom left corner
		IO_BOTTOM_RIGHT = 0x10,	// first pixel is bottom right corner
		IO_TOP_LEFT = 0x20,	// first pixel is top left corner
		IO_TOP_RIGHT = 0x30	// first pixel is top right corner
	};

	enum TGATypes
	{
		TGA_NO_DATA = 0,
		TGA_INDEXED = 1,		// color-mapped images
		TGA_RGB = 2,			// true-color images
		TGA_GRAYSCALE = 3,		// black and white imates
		TGA_RLE_INDEXED = 9,	// run-length encoded (rle), color-mapped images
		TGA_RLE_RGB = 10,		// run-length encoded (rle), true-color images
		TGA_RLE_GRAYSCALE = 11  // run-length encoded (rle), black and white images
	};

	CImagePtr LoadImageData(std::ifstream& inFile);
	bool SaveImage(std::ofstream &outFile, const CImage &image);

private:

	void LoadCompressed(std::ifstream& inFile, uint8Array& data, UInt32 imageSize, UInt32 bytesPerPixel);
	CImagePtr LoadUncompressed(uint8Array& data, const colourMapArray& colours, const TGAHeader& header, bool flipImage);

	bool IsCompressed(const TGAHeader& header) const;
	bool IsUncompressed(const TGAHeader& header) const;

};



#include "ImageLoaderTGA.inl"


_ENGINE_END



#endif