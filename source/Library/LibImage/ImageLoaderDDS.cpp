#include "imageafx.h"
#include "ImageLoaderDDS.h"
#include "MappingD3D11.h"

using namespace Engine;
using namespace Image;

const ULong DDS_MAGIC =		0x20534444; // 'DDS '
const ULong DDS_FOURCC =	0x00000004;  // DDPF_FOURCC
const ULong DDS_RGB =		0x00000040;  // DDPF_RGB
const ULong DDS_RGBA =		0x00000041;  // DDPF_RGB | DDPF_ALPHAPIXELS
const ULong DDS_LUMINANCE = 0x00020000;  // DDPF_LUMINANCE
const ULong DDS_ALPHA =		0x00000002;  // DDPF_ALPHA

const DDS_PIXELFORMAT DDSPF_DXT1 =
    { sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','1'), 0, 0, 0, 0, 0 };

const DDS_PIXELFORMAT DDSPF_DXT2 =
    { sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','2'), 0, 0, 0, 0, 0 };

const DDS_PIXELFORMAT DDSPF_DXT3 =
    { sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','3'), 0, 0, 0, 0, 0 };

const DDS_PIXELFORMAT DDSPF_DXT4 =
    { sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','4'), 0, 0, 0, 0, 0 };

const DDS_PIXELFORMAT DDSPF_DXT5 =
    { sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','5'), 0, 0, 0, 0, 0 };

const DDS_PIXELFORMAT DDSPF_A8R8G8B8 =
    { sizeof(DDS_PIXELFORMAT), DDS_RGBA, 0, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000 };

const DDS_PIXELFORMAT DDSPF_A1R5G5B5 =
    { sizeof(DDS_PIXELFORMAT), DDS_RGBA, 0, 16, 0x00007c00, 0x000003e0, 0x0000001f, 0x00008000 };

const DDS_PIXELFORMAT DDSPF_A4R4G4B4 =
    { sizeof(DDS_PIXELFORMAT), DDS_RGBA, 0, 16, 0x00000f00, 0x000000f0, 0x0000000f, 0x0000f000 };

const DDS_PIXELFORMAT DDSPF_R8G8B8 =
    { sizeof(DDS_PIXELFORMAT), DDS_RGB, 0, 24, 0x00ff0000, 0x0000ff00, 0x000000ff, 0x00000000 };

const DDS_PIXELFORMAT DDSPF_R5G6B5 =
    { sizeof(DDS_PIXELFORMAT), DDS_RGB, 0, 16, 0x0000f800, 0x000007e0, 0x0000001f, 0x00000000 };

// This indicates the DDS_HEADER_DXT10 extension is present (the format is in dxgiFormat)
const DDS_PIXELFORMAT DDSPF_DX10 =
    { sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','1','0'), 0, 0, 0, 0, 0 };


CImagePtr CImageLoaderDDS::LoadImageData(std::ifstream& inFile)
{
	// Do this at start as to avoid any complex
	// Moving of the read pointer around later.
	inFile.seekg(0, std::ios::end);
	std::streamoff length = inFile.tellg();
	inFile.seekg(0, std::ios::beg);

    // Need at least enough data to fill the header and magic number to be a valid DDS
    if( length < (sizeof(DDS_HEADER)+sizeof(DWORD)) )
    {
		inFile.close();
        return nullptr;
    }

    // Create enough space for the file data
	uint8Array imageData( (size_t)length );

    // Read the data in
	inFile.read(reinterpret_cast<char*>(&imageData[0]), length);
	std::streamoff bytesRead = inFile.gcount();

	// Can close the file now
	inFile.close();

	// Check and make sure the length of the file was loaded into memory
    if (bytesRead < length)
        return nullptr;

    // DDS files always start with the same magic number ("DDS ")
	uint8Array::pointer pImageData = &imageData[0];
    ULong magicNumber = *reinterpret_cast<ULong*>(pImageData);
    if (magicNumber != DDS_MAGIC)
        return nullptr;

	// Extract the header from the data (more like point to it!)
    DDS_HEADER* pHeader = reinterpret_cast<DDS_HEADER*>( pImageData + sizeof(ULong) );

    // Verify header to validate DDS file
    if( pHeader->size != sizeof(DDS_HEADER) || pHeader->ddspf.size != sizeof(DDS_PIXELFORMAT) )
        return nullptr;
  
    // Check for DX10 extension
	// If the DDS_PIXELFORMAT dwFlags is set to DDPF_FOURCC and dwFourCC is set to "DX10" an additional 
	// DDS_HEADER_DXT10 structure will be present to accommodate texture arrays or DXGI formats that
	// cannot be expressed as an RGB pixel foramt such as floating point formats, sRGB formats etc. 
	bool DXT10Header = false;
	if ( (pHeader->ddspf.flags & DDS_FOURCC) && (MAKEFOURCC( 'D', 'X', '1', '0' ) == pHeader->ddspf.fourCC) )
	{
		// Must be long enough for both headers and magic value
		if ( length < ( sizeof(ULong) + sizeof(DDS_HEADER) + sizeof(DDS_HEADER_DXT10) ) )
			return nullptr;

		DXT10Header = true;
	}

    // setup the pointers in the process request
    UInt32 offset = sizeof(ULong) + sizeof(DDS_HEADER) + (DXT10Header ? sizeof(DDS_HEADER_DXT10) : 0);
    pImageData += offset;

	// raw image data comes after the headers
	UInt32 rawImageSize = static_cast<UInt32>(length) - offset;

	// Create the image now
	UInt32 mipCount = pHeader->mipMapCount;
    if (0 == mipCount)
        mipCount = 1;

    // Bound miplevels (affects the memory usage below)
    if (mipCount > D3D11_REQ_MIP_LEVELS)
        return nullptr;

	UInt32 arraySize = 0;
	Image::ImageFormat finalFormat = IF_NONE;
    if ( (pHeader->ddspf.flags & DDS_FOURCC) && (MAKEFOURCC('D','X','1','0') == pHeader->ddspf.fourCC) )
    {
        DDS_HEADER_DXT10* d3d10ext = 
			reinterpret_cast<DDS_HEADER_DXT10*>( reinterpret_cast<char*>(pHeader) + sizeof(DDS_HEADER) );

        // For now, we only support 2D textures
        if (d3d10ext->resourceDimension != D3D11_RESOURCE_DIMENSION_TEXTURE2D)
            return nullptr;

        // Bound array sizes (affects the memory usage below)
        if (d3d10ext->arraySize > D3D11_REQ_TEXTURE2D_ARRAY_AXIS_DIMENSION)
            return nullptr;

		// The size of the array
        arraySize = d3d10ext->arraySize;

		// Convert the DXGI format to a supported image format
        finalFormat = ConvertToFormat(d3d10ext->dxgiFormat); 
    }
	else
	{
		// For now only support 2D textures, not cubemaps or volumes
		if (pHeader->cubemapFlags != 0 || (pHeader->headerFlags & DDS_HEADER_FLAGS_VOLUME) )
			return nullptr;

		// Determine the D3D9Format then convert to image format	
		finalFormat = ConvertToFormat( GetD3D9Format(pHeader->ddspf) ) ;
		arraySize = 1;
	
		//// File format is not known
		//if(dxgiFormat == DXGI_FORMAT_UNKNOWN)
		//{
		//	D3DFORMAT fmt = GetD3D9Format(pHeader->ddspf);

		//	// Swizzle some RGB to BGR common formats to be DXGI (1.0) supported
		//	// D3D9 formats are labeled in the order they sit in memory.
		//	// Form: D3DFMT_A8R8G8B8			bits[ A8(24-31) | R8(16-23) | G8(8-15) | B8(0-7) ]
		//	// To: DXGI_FORMAT_R8G8B8A8_UNORM	bits[ A8(24-31) | B8(16-23) | G8(8-15) | R8(0-7) ]
		//	switch (fmt)
		//	{
		//	case D3DFMT_X8R8G8B8:
		//	case D3DFMT_A8R8G8B8:
		//		{
		//			dxgiFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
		//			if (rawImageSize >= 3)
		//			{
		//				// Swap blue and red channels
		//				for( UINT i = 0; i < rawImageSize; i += 4 )						
		//					std::swap(pImageData[i], pImageData[i + 2]);			
		//			}
		//		}
		//		break;

		//		// Need more room to try to swizzle 24bpp formats
		//		// Could also try to expand 4bpp or 3:3:2 formats

		//	default:
		//		return CImagePtr();
		//	}
		//}
	}

	// Create internal image object
	CImagePtr pImage = std::make_shared<CImage>(finalFormat, (UInt32)pHeader->width,
		(UInt32)pHeader->height, pImageData, rawImageSize, mipCount);

	// Return the image pointer
    return pImage;
}

bool CImageLoaderDDS::SaveImage(std::ofstream &outFile, const CImage &image)
{
	UNUSED_PARAMETER(outFile);
	UNUSED_PARAMETER(image);

	return false;
}

D3DFORMAT CImageLoaderDDS::GetD3D9Format(const DDS_PIXELFORMAT& ddpf)
{
    if (ddpf.flags & DDS_RGB)
    {
        switch (ddpf.RGBBitCount)
        {
        case 32:
            if ( IsBitmask(ddpf,0x00ff0000,0x0000ff00,0x000000ff,0xff000000) )
                return D3DFMT_A8R8G8B8;
            if ( IsBitmask(ddpf,0x00ff0000,0x0000ff00,0x000000ff,0x00000000) )
                return D3DFMT_X8R8G8B8;
            if ( IsBitmask(ddpf,0x000000ff,0x0000ff00,0x00ff0000,0xff000000) )
                return D3DFMT_A8B8G8R8;
            if ( IsBitmask(ddpf,0x000000ff,0x0000ff00,0x00ff0000,0x00000000) )
                return D3DFMT_X8B8G8R8;

            // Note that many common DDS reader/writers swap the
            // the RED/BLUE masks for 10:10:10:2 formats. We assumme
            // below that the 'correct' header mask is being used
            if ( IsBitmask(ddpf,0x3ff00000,0x000ffc00,0x000003ff,0xc0000000) )
                return D3DFMT_A2R10G10B10;
            if ( IsBitmask(ddpf,0x000003ff,0x000ffc00,0x3ff00000,0xc0000000) )
                return D3DFMT_A2B10G10R10;

            if ( IsBitmask(ddpf,0x0000ffff,0xffff0000,0x00000000,0x00000000) )
                return D3DFMT_G16R16;
            if ( IsBitmask(ddpf,0xffffffff,0x00000000,0x00000000,0x00000000) )
                return D3DFMT_R32F; // D3DX writes this out as a FourCC of 114
            break;

        case 24:
            if ( IsBitmask(ddpf,0x00ff0000,0x0000ff00,0x000000ff,0x00000000) )
                return D3DFMT_R8G8B8;
            break;

        case 16:
            if ( IsBitmask(ddpf,0x0000f800,0x000007e0,0x0000001f,0x00000000) )
                return D3DFMT_R5G6B5;
            if ( IsBitmask(ddpf,0x00007c00,0x000003e0,0x0000001f,0x00008000) )
                return D3DFMT_A1R5G5B5;
            if ( IsBitmask(ddpf,0x00007c00,0x000003e0,0x0000001f,0x00000000) )
                return D3DFMT_X1R5G5B5;
            if ( IsBitmask(ddpf,0x00000f00,0x000000f0,0x0000000f,0x0000f000) )
                return D3DFMT_A4R4G4B4;
            if ( IsBitmask(ddpf,0x00000f00,0x000000f0,0x0000000f,0x00000000) )
                return D3DFMT_X4R4G4B4;
            if ( IsBitmask(ddpf,0x000000e0,0x0000001c,0x00000003,0x0000ff00) )
                return D3DFMT_A8R3G3B2;
            break;
        }
    }
    else if (ddpf.flags & DDS_LUMINANCE)
    {
        if (8 == ddpf.RGBBitCount)
        {            
			if ( IsBitmask(ddpf,0x0000000f,0x00000000,0x00000000,0x000000f0) )
                return D3DFMT_A4L4;
            if ( IsBitmask(ddpf,0x000000ff,0x00000000,0x00000000,0x00000000) )
                return D3DFMT_L8;
        }

        if (16 == ddpf.RGBBitCount)
        {
            if ( IsBitmask(ddpf,0x0000ffff,0x00000000,0x00000000,0x00000000) )
                return D3DFMT_L16;
            if ( IsBitmask(ddpf,0x000000ff,0x00000000,0x00000000,0x0000ff00) )
                return D3DFMT_A8L8;
        }
    }
    else if (ddpf.flags & DDS_ALPHA)
    {
        if (8 == ddpf.RGBBitCount)
        {
            return D3DFMT_A8;
        }
    }
    else if (ddpf.flags & DDS_FOURCC)
    {
        if ( MAKEFOURCC( 'D', 'X', 'T', '1' ) == ddpf.fourCC )
            return D3DFMT_DXT1;
        if ( MAKEFOURCC( 'D', 'X', 'T', '2' ) == ddpf.fourCC )
            return D3DFMT_DXT2;
        if ( MAKEFOURCC( 'D', 'X', 'T', '3' ) == ddpf.fourCC )
            return D3DFMT_DXT3;
        if ( MAKEFOURCC( 'D', 'X', 'T', '4' ) == ddpf.fourCC )
            return D3DFMT_DXT4;
        if ( MAKEFOURCC( 'D', 'X', 'T', '5' ) == ddpf.fourCC )
            return D3DFMT_DXT5;

        if ( MAKEFOURCC( 'R', 'G', 'B', 'G' ) == ddpf.fourCC )
            return D3DFMT_R8G8_B8G8;
        if ( MAKEFOURCC( 'G', 'R', 'G', 'B' ) == ddpf.fourCC )
            return D3DFMT_G8R8_G8B8;

        if ( MAKEFOURCC( 'U', 'Y', 'V', 'Y' ) == ddpf.fourCC )
            return D3DFMT_UYVY;
        if ( MAKEFOURCC( 'Y', 'U', 'Y', '2' ) == ddpf.fourCC )
            return D3DFMT_YUY2;

        // Check for D3DFORMAT enums being set here
        switch (ddpf.fourCC)
        {
        case D3DFMT_A16B16G16R16:
        case D3DFMT_Q16W16V16U16:
        case D3DFMT_R16F:
        case D3DFMT_G16R16F:
        case D3DFMT_A16B16G16R16F:
        case D3DFMT_R32F:
        case D3DFMT_G32R32F:
        case D3DFMT_A32B32G32R32F:
        case D3DFMT_CxV8U8:
            return (D3DFORMAT)ddpf.fourCC;
        }
    }

    return D3DFMT_UNKNOWN;
}





	//UInt32 numBytes = 0;
	//UInt32 rowBytes = 0;
	//UInt32 numRows = 0;
	//UInt32 totalImageSize = 0;
	//   for(UInt32 j = 0; j < arraySize; ++j)
	//   {
	//	UInt32 width = pHeader->dwWidth;
	//	UInt32 height = pHeader->dwHeight;
	//       for(UInt32 i = 0; i < mipCount; ++i)
	//       {   
	//		GetSurfaceInfo(width, height, dxgiFormat, numBytes, rowBytes, numRows);		
	//           totalImageSize += numBytes;

	//		width >>= 1;
	//		height >>= 1;
	//		if( width == 0 )
	//			width = 1;
	//		if( height == 0 )
	//			height = 1;
	//       }
	//   }


DXGI_FORMAT CImageLoaderDDS::GetDXGIFormat(const DDS_PIXELFORMAT& ddpf)
//{
//	// If texture contains uncompressed RGB data; dwRGBBitCount and the RGB masks 
//	// contain valid data.
//    if (ddpf.flags & DDS_RGB)
//    {
//        switch (ddpf.RGBBitCount)
//        {
//        case 32:
//            // DXGI_FORMAT_B8G8R8A8_UNORM_SRGB & DXGI_FORMAT_B8G8R8X8_UNORM_SRGB should be
//            // written using the DX10 extended header instead since these formats require
//            // DXGI 1.1
//            //
//            // This code will use the fallback to swizzle RGB to BGR in memory for standard
//            // DDS files which works on 10 and 10.1 devices with WDDM 1.0 drivers
//            //
//            // NOTE: We don't use DXGI_FORMAT_B8G8R8X8_UNORM or DXGI_FORMAT_B8G8R8X8_UNORM
//            // here because they were defined for DXGI 1.0 but were not required for D3D10/10.1
//
//            if( IsBitmask(ddpf, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000) )
//                return DXGI_FORMAT_R8G8B8A8_UNORM;
//            if( IsBitmask(ddpf, 0x000000ff, 0x0000ff00, 0x00ff0000, 0x00000000) )
//                return DXGI_FORMAT_R8G8B8A8_UNORM; // No D3DFMT_X8B8G8R8 in DXGI
//
//            // Note that many common DDS reader/writers swap the
//            // the RED/BLUE masks for 10:10:10:2 formats. We assumme
//            // below that the 'correct' header mask is being used. The
//            // more robust solution is to use the 'DX10' header extension and
//            // specify the DXGI_FORMAT_R10G10B10A2_UNORM format directly
//            if( IsBitmask(ddpf, 0x000003ff, 0x000ffc00, 0x3ff00000, 0xc0000000) )
//                return DXGI_FORMAT_R10G10B10A2_UNORM;
//
//            if( IsBitmask(ddpf, 0x0000ffff, 0xffff0000, 0x00000000, 0x00000000) )
//                return DXGI_FORMAT_R16G16_UNORM;
//
//            if( IsBitmask(ddpf, 0xffffffff, 0x00000000, 0x00000000, 0x00000000) )
//                // Only 32-bit color channel format in D3D9 was R32F
//                return DXGI_FORMAT_R32_FLOAT; // D3DX writes this out as a FourCC of 114
//            break;
//
//        case 24:
//            // No 24bpp DXGI formats
//            break;
//
//        case 16:
//            // 5:5:5 & 5:6:5 formats are defined for DXGI, but are deprecated for D3D10+
//
//            if( IsBitmask(ddpf, 0x0000f800, 0x000007e0, 0x0000001f, 0x00000000) )
//                return DXGI_FORMAT_B5G6R5_UNORM;
//            if( IsBitmask(ddpf, 0x00007c00, 0x000003e0, 0x0000001f, 0x00008000) )
//                return DXGI_FORMAT_B5G5R5A1_UNORM;
//            if( IsBitmask(ddpf, 0x00007c00, 0x000003e0, 0x0000001f, 0x00000000) )
//                return DXGI_FORMAT_B5G5R5A1_UNORM; // No D3DFMT_X1R5G5B5 in DXGI
//
//            // No 4bpp or 3:3:2 DXGI formats
//            break;
//        }
//    }
//    else if (ddpf.flags & DDS_LUMINANCE)
//    {
//        if (8 == ddpf.RGBBitCount)
//        {
//            if( IsBitmask(ddpf, 0x000000ff, 0x00000000, 0x00000000, 0x00000000) )
//                return DXGI_FORMAT_R8_UNORM; // D3DX10/11 writes this out as DX10 extension
//
//            // No 4bpp DXGI formats
//        }
//
//        if (16 == ddpf.RGBBitCount)
//        {
//            if( IsBitmask(ddpf, 0x0000ffff, 0x00000000, 0x00000000, 0x00000000) )
//                return DXGI_FORMAT_R16_UNORM; // D3DX10/11 writes this out as DX10 extension
//            if( IsBitmask(ddpf, 0x000000ff, 0x00000000, 0x00000000, 0x0000ff00) )
//                return DXGI_FORMAT_R8G8_UNORM; // D3DX10/11 writes this out as DX10 extension
//        }
//    }
//    else if (ddpf.flags & DDS_ALPHA)
//    {
//        if( 8 == ddpf.RGBBitCount )
//        {
//            return DXGI_FORMAT_A8_UNORM;
//        }
//    }
//    else if (ddpf.flags & DDS_FOURCC) // Texture contains compressed RGB data; dwFourCC contains valid data
//    {
//        if (MAKEFOURCC('D', 'X', 'T', '1') == ddpf.fourCC)
//            return DXGI_FORMAT_BC1_UNORM;
//        if (MAKEFOURCC('D', 'X', 'T', '3') == ddpf.fourCC)
//            return DXGI_FORMAT_BC2_UNORM;
//        if (MAKEFOURCC('D', 'X', 'T', '5') == ddpf.fourCC)
//            return DXGI_FORMAT_BC3_UNORM;
//
//        if (MAKEFOURCC('B', 'C', '4', 'U') == ddpf.fourCC)
//            return DXGI_FORMAT_BC4_UNORM;
//        if (MAKEFOURCC('B', 'C', '4', 'S') == ddpf.fourCC)
//            return DXGI_FORMAT_BC4_SNORM;
//
//        if (MAKEFOURCC('A', 'T', 'I', '2') == ddpf.fourCC)
//            return DXGI_FORMAT_BC5_UNORM;
//        if (MAKEFOURCC('B', 'C', '5', 'S') == ddpf.fourCC)
//            return DXGI_FORMAT_BC5_SNORM;
//
//        if (MAKEFOURCC('R', 'G', 'B', 'G') == ddpf.fourCC)
//            return DXGI_FORMAT_R8G8_B8G8_UNORM;
//        if (MAKEFOURCC('G', 'R', 'G', 'B') == ddpf.fourCC)
//            return DXGI_FORMAT_G8R8_G8B8_UNORM;
//
//        // Check for D3DFORMAT enums being set here
//        switch (ddpf.fourCC)
//        {
//        case D3DFMT_A16B16G16R16: // 36
//            return DXGI_FORMAT_R16G16B16A16_UNORM;
//
//        case D3DFMT_Q16W16V16U16: // 110
//            return DXGI_FORMAT_R16G16B16A16_SNORM;
//
//        case D3DFMT_R16F: // 111
//            return DXGI_FORMAT_R16_FLOAT;
//
//        case D3DFMT_G16R16F: // 112
//            return DXGI_FORMAT_R16G16_FLOAT;
//
//        case D3DFMT_A16B16G16R16F: // 113
//            return DXGI_FORMAT_R16G16B16A16_FLOAT;
//
//        case D3DFMT_R32F: // 114
//            return DXGI_FORMAT_R32_FLOAT;
//
//        case D3DFMT_G32R32F: // 115
//            return DXGI_FORMAT_R32G32_FLOAT;
//
//        case D3DFMT_A32B32G32R32F: // 116
//            return DXGI_FORMAT_R32G32B32A32_FLOAT;
//        }
//    }
//
//    return DXGI_FORMAT_UNKNOWN;
//}


////--------------------------------------------------------------------------------------
//// Helper functions to create SRGB formats from typeless formats and vice versa
////--------------------------------------------------------------------------------------
//DXGI_FORMAT CImageLoaderDDS::Make_SRGB(DXGI_FORMAT format)
//{
//  //  if( !DXUTIsInGammaCorrectMode() )
//  //      return format;
//
//    switch(format)
//    {
//        case DXGI_FORMAT_R8G8B8A8_TYPELESS:
//        case DXGI_FORMAT_R8G8B8A8_UNORM:
//        case DXGI_FORMAT_R8G8B8A8_UINT:
//        case DXGI_FORMAT_R8G8B8A8_SNORM:
//        case DXGI_FORMAT_R8G8B8A8_SINT:
//            return DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
//
//        case DXGI_FORMAT_BC1_TYPELESS:
//        case DXGI_FORMAT_BC1_UNORM:
//            return DXGI_FORMAT_BC1_UNORM_SRGB;
//        case DXGI_FORMAT_BC2_TYPELESS:
//        case DXGI_FORMAT_BC2_UNORM:
//            return DXGI_FORMAT_BC2_UNORM_SRGB;
//        case DXGI_FORMAT_BC3_TYPELESS:
//        case DXGI_FORMAT_BC3_UNORM:
//            return DXGI_FORMAT_BC3_UNORM_SRGB;
//    };
//
//    return format;
//}
//
//void CImageLoaderDDS::GetSurfaceInfo(UInt width, UInt height, DXGI_FORMAT fmt, UInt& numBytes, UInt& rowBytes, UInt& numRows)
//{
//    bool bc = true;
//    int bcnumBytesPerBlock = 16;
//    switch (fmt)
//    {
//    case DXGI_FORMAT_BC1_TYPELESS:
//    case DXGI_FORMAT_BC1_UNORM:
//    case DXGI_FORMAT_BC1_UNORM_SRGB:
//    case DXGI_FORMAT_BC4_TYPELESS:
//    case DXGI_FORMAT_BC4_UNORM:
//    case DXGI_FORMAT_BC4_SNORM:
//        bcnumBytesPerBlock = 8;
//        break;
//
//    case DXGI_FORMAT_BC2_TYPELESS:
//    case DXGI_FORMAT_BC2_UNORM:
//    case DXGI_FORMAT_BC2_UNORM_SRGB:
//    case DXGI_FORMAT_BC3_TYPELESS:
//    case DXGI_FORMAT_BC3_UNORM:
//    case DXGI_FORMAT_BC3_UNORM_SRGB:
//    case DXGI_FORMAT_BC5_TYPELESS:
//    case DXGI_FORMAT_BC5_UNORM:
//    case DXGI_FORMAT_BC5_SNORM:
//    case DXGI_FORMAT_BC6H_TYPELESS:
//    case DXGI_FORMAT_BC6H_UF16:
//    case DXGI_FORMAT_BC6H_SF16:
//    case DXGI_FORMAT_BC7_TYPELESS:
//    case DXGI_FORMAT_BC7_UNORM:
//    case DXGI_FORMAT_BC7_UNORM_SRGB:
//        break;
//
//    default:
//        bc = false;
//        break;
//    }
//
//    if (bc)
//    {
//        int numBlocksWide = 0;
//        if(width > 0)
//            numBlocksWide = max(1, width / 4);
//        int numBlocksHigh = 0;
//        if(height > 0)
//            numBlocksHigh = max(1, height / 4);
//        rowBytes = numBlocksWide * bcnumBytesPerBlock;
//        numRows = numBlocksHigh;
//    }
//    else
//    {
//        UInt bpp = BitsPerPixel(fmt);
//        rowBytes = (width * bpp + 7) / 8; // round up to nearest byte
//        numRows = height;
//    }
//    numBytes = rowBytes * numRows;
//}
//
//UInt32 CImageLoaderDDS::BitsPerPixel(DXGI_FORMAT fmt)
//{
//    switch (fmt)
//    {
//    case DXGI_FORMAT_R32G32B32A32_TYPELESS:
//    case DXGI_FORMAT_R32G32B32A32_FLOAT:
//    case DXGI_FORMAT_R32G32B32A32_UINT:
//    case DXGI_FORMAT_R32G32B32A32_SINT:
//        return 128;
//
//    case DXGI_FORMAT_R32G32B32_TYPELESS:
//    case DXGI_FORMAT_R32G32B32_FLOAT:
//    case DXGI_FORMAT_R32G32B32_UINT:
//    case DXGI_FORMAT_R32G32B32_SINT:
//        return 96;
//
//    case DXGI_FORMAT_R16G16B16A16_TYPELESS:
//    case DXGI_FORMAT_R16G16B16A16_FLOAT:
//    case DXGI_FORMAT_R16G16B16A16_UNORM:
//    case DXGI_FORMAT_R16G16B16A16_UINT:
//    case DXGI_FORMAT_R16G16B16A16_SNORM:
//    case DXGI_FORMAT_R16G16B16A16_SINT:
//    case DXGI_FORMAT_R32G32_TYPELESS:
//    case DXGI_FORMAT_R32G32_FLOAT:
//    case DXGI_FORMAT_R32G32_UINT:
//    case DXGI_FORMAT_R32G32_SINT:
//    case DXGI_FORMAT_R32G8X24_TYPELESS:
//    case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
//    case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
//    case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
//        return 64;
//
//    case DXGI_FORMAT_R10G10B10A2_TYPELESS:
//    case DXGI_FORMAT_R10G10B10A2_UNORM:
//    case DXGI_FORMAT_R10G10B10A2_UINT:
//    case DXGI_FORMAT_R11G11B10_FLOAT:
//    case DXGI_FORMAT_R8G8B8A8_TYPELESS:
//    case DXGI_FORMAT_R8G8B8A8_UNORM:
//    case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
//    case DXGI_FORMAT_R8G8B8A8_UINT:
//    case DXGI_FORMAT_R8G8B8A8_SNORM:
//    case DXGI_FORMAT_R8G8B8A8_SINT:
//    case DXGI_FORMAT_R16G16_TYPELESS:
//    case DXGI_FORMAT_R16G16_FLOAT:
//    case DXGI_FORMAT_R16G16_UNORM:
//    case DXGI_FORMAT_R16G16_UINT:
//    case DXGI_FORMAT_R16G16_SNORM:
//    case DXGI_FORMAT_R16G16_SINT:
//    case DXGI_FORMAT_R32_TYPELESS:
//    case DXGI_FORMAT_D32_FLOAT:
//    case DXGI_FORMAT_R32_FLOAT:
//    case DXGI_FORMAT_R32_UINT:
//    case DXGI_FORMAT_R32_SINT:
//    case DXGI_FORMAT_R24G8_TYPELESS:
//    case DXGI_FORMAT_D24_UNORM_S8_UINT:
//    case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
//    case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
//    case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
//    case DXGI_FORMAT_R8G8_B8G8_UNORM:
//    case DXGI_FORMAT_G8R8_G8B8_UNORM:
//    case DXGI_FORMAT_B8G8R8A8_UNORM:
//    case DXGI_FORMAT_B8G8R8X8_UNORM:
//    case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
//    case DXGI_FORMAT_B8G8R8A8_TYPELESS:
//    case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
//    case DXGI_FORMAT_B8G8R8X8_TYPELESS:
//    case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
//        return 32;
//
//    case DXGI_FORMAT_R8G8_TYPELESS:
//    case DXGI_FORMAT_R8G8_UNORM:
//    case DXGI_FORMAT_R8G8_UINT:
//    case DXGI_FORMAT_R8G8_SNORM:
//    case DXGI_FORMAT_R8G8_SINT:
//    case DXGI_FORMAT_R16_TYPELESS:
//    case DXGI_FORMAT_R16_FLOAT:
//    case DXGI_FORMAT_D16_UNORM:
//    case DXGI_FORMAT_R16_UNORM:
//    case DXGI_FORMAT_R16_UINT:
//    case DXGI_FORMAT_R16_SNORM:
//    case DXGI_FORMAT_R16_SINT:
//    case DXGI_FORMAT_B5G6R5_UNORM:
//    case DXGI_FORMAT_B5G5R5A1_UNORM:
//        return 16;
//
//    case DXGI_FORMAT_R8_TYPELESS:
//    case DXGI_FORMAT_R8_UNORM:
//    case DXGI_FORMAT_R8_UINT:
//    case DXGI_FORMAT_R8_SNORM:
//    case DXGI_FORMAT_R8_SINT:
//    case DXGI_FORMAT_A8_UNORM:
//        return 8;
//
//    case DXGI_FORMAT_R1_UNORM:
//        return 1;
//
//    case DXGI_FORMAT_BC1_TYPELESS:
//    case DXGI_FORMAT_BC1_UNORM:
//    case DXGI_FORMAT_BC1_UNORM_SRGB:
//        return 4;
//
//    case DXGI_FORMAT_BC2_TYPELESS:
//    case DXGI_FORMAT_BC2_UNORM:
//    case DXGI_FORMAT_BC2_UNORM_SRGB:
//    case DXGI_FORMAT_BC3_TYPELESS:
//    case DXGI_FORMAT_BC3_UNORM:
//    case DXGI_FORMAT_BC3_UNORM_SRGB:
//    case DXGI_FORMAT_BC4_TYPELESS:
//    case DXGI_FORMAT_BC4_UNORM:
//    case DXGI_FORMAT_BC4_SNORM:
//    case DXGI_FORMAT_BC5_TYPELESS:
//    case DXGI_FORMAT_BC5_UNORM:
//    case DXGI_FORMAT_BC5_SNORM:
//    case DXGI_FORMAT_BC6H_TYPELESS:
//    case DXGI_FORMAT_BC6H_UF16:
//    case DXGI_FORMAT_BC6H_SF16:
//    case DXGI_FORMAT_BC7_TYPELESS:
//    case DXGI_FORMAT_BC7_UNORM:
//    case DXGI_FORMAT_BC7_UNORM_SRGB:
//        return 8;
//
//    default:
//        assert(false); // unhandled format
//        return 0;
//    }
//}
