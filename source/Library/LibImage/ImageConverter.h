#ifndef __CFORMAT_IAMGE_H__
#define __CFORMAT_IAMGE_H__

#include "ImageTypes.h"

_ENGINE_BEGIN

class CFormatImage
{
public:
	typedef std::vector<UInt32> UInt32Array;

	// Constructor builds the bit mask and shift numbers
	CFormatImage(const char* format);

	// Calculates the number of bits for the mask (32bit)
	UInt32 CreateBitMask(UInt32 startpos, UInt32 numbits);

	// Calculates the number of bits for the mask (64bit)
	UInt64 CreateBitMask(UInt64 startpos, UInt64 numbits);

	// Data members
	UInt32Array m_bitmask;
	UInt32Array m_shiftR;
	UInt		m_bytesPerPixel;
};

_ENGINE_END

#endif