#ifndef __IMAGE_TYPES_H__
#define __IMAGE_TYPES_H__

#include "Core.h"

_ENGINE_BEGIN

// Currently supported formats.  More may be added as needed.
class CImageFormat
{
public: 
	enum Type
	{
		IF_NONE = 0,

		// Small-bit color formats.
		IF_R5G6B5,
		IF_A1R5G5B5,
		IF_A4R4G4B4,

		// 8-bit integer formats.
		IF_A8,
		IF_L8,
		IF_A8L8,
		IF_R8G8B8,		// 8-bit blue | 8-bit green | 8-bit red
		IF_B8G8R8,		// 8-bit red  | 8-bit green | 8-bit blue
		IF_A8R8G8B8,	// 8-bit blue | 8-bit green | 8-bit red  | 8-bit alpha
		IF_A8B8G8R8,	// 8-bit red  | 8-bit green | 8-bit blue | 8-bit alpha

		// 16-bit integer formats.
		IF_L16,
		IF_G16R16,
		IF_A16B16G16R16,

		// 16-bit floating-point formats ('half float' channels).
		IF_R16F,
		IF_G16R16F,
		IF_A16B16G16R16F,

		// 32-bit floating-point formats ('float' channels).
		IF_R32F,
		IF_G32R32F,
		IF_A32B32G32R32F,

		// DXT compressed formats.
		IF_DXT1, // supports 3 colour channels and only a 1-bit (on/off) alpha channel
		IF_DXT3, // supports 3 colour channels and only a 8-bit alpha channel
		IF_DXT5, // supports 2 colour channels

		// Depth-stencil format.
		IF_D32F,  // 32-bit floating point depth
		IF_D24S8, // 8-bit stencil with 24-bit depth
		IF_S8D24, // 24-bit depth with 8 bit stencil
		IF_D16,	  // 16-bit depth

		IF_QUANTITY
	};
private:
	CImageFormat();
};

// Image dimensions describe width, height, depth..
// Allow enum use as UInt: 1D = 1, 2D = 2, 3D = 3
class CImageDimensions
{
public:
	enum Type
	{
		ID_1D = 1,
		ID_2D,
		ID_3D
	};
private:
	CImageDimensions();
};

// Image format size in bytes
UInt BytesPerPixel(CImageFormat::Type format); // [IF_QUANTITY];
UInt BitsPerPixel(CImageFormat::Type format);

#include "ImageTypes.inl"

_ENGINE_END

#endif