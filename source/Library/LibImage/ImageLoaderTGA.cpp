#include "imageafx.h"
#include "ImageLoaderTGA.h"
#include "Image.h"
#include "ImageTools.h"

using namespace Engine;
using namespace Image;


CImagePtr CImageLoaderTGA::LoadImageData(std::ifstream& inFile)
{
	// read in the footer at end of file
	// determines if this is TGA 1.0 or 2.0 spec
	TGAFooter footer;
	std::memset( &footer, 0, sizeof(TGAFooter) );

	// read in the header (12 bytes)
	TGAHeader header;
	std::memset( &header, 0, sizeof(TGAHeader) );
	inFile.read( reinterpret_cast<char*>(&header), sizeof(TGAHeader) );

	if (header.imageType == TGA_NO_DATA)
	{
		assert(!"no data in TGA image");
		return CImagePtr();
	}

	// skip past the id
	if (header.idLength > 0)
		inFile.ignore(header.idLength); // Ignores the next lot of data

	// check for colour map
	colourMapArray palette;
	if (header.colourType)
	{
		// disable warning for:
		// "conversion from 'float' to 'UInt32', possible loss of data"
#pragma warning( push )
#pragma warning( disable : 4244 )
		UInt32 colSize = static_cast<float>(header.cmSize) / 8 * header.cmLength;
#pragma warning( pop ) 

		uint8Array colourData(colSize);
		inFile.read( reinterpret_cast<char*>(&colourData[0]), colSize ); // read color map

		palette.resize(header.cmLength);

		// convert to 32 bit colour palette
		switch (header.cmSize)
		{
		case 16:
			ConvertA1R5G5B5toA8R8G8B8(&colourData[0], &palette[0], header.cmLength);
			break;

		case 24:
			ConvertB8G8R8toA8R8G8B8(&colourData[0], &palette[0], header.cmLength);
			break;

		case 32:
			ConvertB8G8R8A8toA8R8G8B8(&colourData[0], &palette[0], header.cmLength);
			break;
		};

		if (!colourData.empty())
			colourData.clear();
	}

	bool flipImageVert = ( (header.imageDesc & IO_TOP_LEFT) == IO_TOP_LEFT ) ? true : false;

	UInt32 bytesPerPixel = header.bpp / 8;
	UInt32 imageSize = header.width * header.height * bytesPerPixel;

	// read image
	// allocate space for data.
	uint8Array imageData(imageSize);
	if (IsCompressed(header))
	{
		LoadCompressed(inFile, imageData, imageSize, bytesPerPixel);
	}
	else
	{
		inFile.read( reinterpret_cast<char*>(&imageData[0]), imageSize );
	}

	CImagePtr pImage = LoadUncompressed(imageData, palette, header, flipImageVert);

	if (inFile.is_open())					
		inFile.close(); // close file after loading

	return pImage;
}

bool CImageLoaderTGA::SaveImage(std::ofstream &outFile, const CImage &image)
{


	return true;
}

CImagePtr CImageLoaderTGA::LoadUncompressed(uint8Array& data, const colourMapArray& colours, const TGAHeader& header, bool flipImage)
{
	CImagePtr pImage;
	switch (header.bpp)
	{
	case 8:
		pImage = CImagePtr( new CImage(CImage::IF_A1R5G5B5, header.width, header.height) );
		Convert8to16Bit(&data[0], reinterpret_cast<UInt16*>( pImage->GetData() ), &colours[0], header.width, header.height, 0, flipImage);
		break;

	case 16:
		pImage = CImagePtr( new CImage(CImage::IF_A1R5G5B5, header.width, header.height) );
		Convert16to16Bit(&data[0], pImage->GetData(), header.width, header.height, 0, flipImage);
		break;

	case 24: // 24 bits per pixel
		pImage = CImagePtr( new CImage(CImage::IF_R8G8B8, header.width, header.height) );
		Convert24BitTo24Bit(&data[0], pImage->GetData(), header.width, header.height, header.bpp, 0, true, flipImage);
		break;

	case 32: // 32 bits per pixel
		pImage = CImagePtr( new CImage(CImage::IF_A8R8G8B8, header.width, header.height) );
		Convert32BitTo32Bit(&data[0], pImage->GetData(), header.width, header.height, header.bpp, 0, true, flipImage);
		break;
	};

	return pImage;
}


void CImageLoaderTGA::LoadCompressed(std::ifstream& inFile, uint8Array& data, UInt32 imageSize, UInt32 bytesPerPixel)
{
	UInt32 currentByte = 0;

	while (currentByte < imageSize)
	{
		UInt8 chunkheader = 0; // this is used as the pixel count and is always one less than the actual total.
		inFile.read( reinterpret_cast<char*>(&chunkheader), sizeof(UInt8) );

		if (chunkheader < 128)
		{
			++chunkheader;

			UInt32 lineSize = chunkheader * bytesPerPixel;
			inFile.read( reinterpret_cast<char*>(&data[currentByte]), lineSize);
			currentByte += lineSize;
		}
		else
		{
			chunkheader -= 127; // subtract 127 to get rid of the id bit

			// read in pixel value to be repeated
			UInt32 intailByte = currentByte;
			inFile.read( reinterpret_cast<char*>(&data[currentByte]), bytesPerPixel);
			currentByte += bytesPerPixel;

			for (UInt32 counter = 1; counter < chunkheader; ++counter)
			{
				for (UInt32 i = 0; i < bytesPerPixel; ++i)
					data[currentByte + i] = data[intailByte + i];

				currentByte += bytesPerPixel;
			}
		}
	} // while

}


