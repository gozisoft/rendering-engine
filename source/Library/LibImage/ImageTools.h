#ifndef __IMAGE_TOOLS_H__
#define __IMAGE_TOOLS_H__

#include "IImage.h"

_ENGINE_BEGIN

namespace Image
{
	// Byte functions for access to
	// | UpperByte		LowerByte |
	// | 0000			0000      |
	UInt8 UpperByte(UInt8 byte);
	UInt8 LowerByte(UInt8 byte);

	// Word functions for access to (16 bits split to 2x 8bits)
	// | UpperWord		LowerWord |
	// | 0000 0000		0000 0000 |
	UInt8 UpperWord(UInt16 word);
	UInt8 LowerWord(UInt16 word);

	// Form a 16 bit short (word) from 2x 8 bits (2 bytes)
	UInt16 MakeWord(UInt8 upperbyte, UInt8 lowerbyte);

	// bit twiddling method for counting used bits
	ULong Popcnt(ULong i);
	UInt32 UsedPaletteEntries(UInt16 bitcount);
	Long CalculateLine(Long width, UInt32 bitdepth);
	Long CalculatePitch(Long line);

	// Creates a 16 bit A1R5G5B5 color
	UInt16 RGBA16(UInt32 r, UInt32 g, UInt32 b, UInt32 a=0xFF);
	// Creates a 16 bit R5G6B5 color
	UInt16 RGB16(UInt32 r, UInt32 g, UInt32 b);

	// Converts a 32bit (A8R8G8B8) color to a 16bit (A1R5G5B5) color
	UInt16 A8R8G8B8toA1R5G5B5(UInt32 colour); 
	UInt32 A1R5G5B5toA8R8G8B8(UInt16 colour);

	// Converts a 32bit (A8R8G8B8) color to a 16bit (R5G6B5) color
	UInt16 A8R8G8B8toR5G6B5(UInt32 colour); 
	UInt32 R5G6B5toA8R8G8B8(UInt16 colour);

	// Converts a 16bit (R5G6B5) color to a 16bit (A1R5G5B5) color
	UInt16 R5G6B5toA1R5G5B5(UInt16 colour);

	// --------------------------------------------------------------------------------------------------------------------------
	// Colour conversion functions
	// --------------------------------------------------------------------------------------------------------------------------
	void ConvertA1R5G5B5toA8R8G8B8(const UInt8* pColIn, UInt32* pColOut, UInt32 length);
	void ConvertB8G8R8toA8R8G8B8(const UInt8* pColIn, UInt32* pColOut, UInt32 length);
	void ConvertB8G8R8A8toA8R8G8B8(const UInt8* pColIn, UInt32* pColOut, UInt32 length);

	// --------------------------------------------------------------------------------------------------------------------------
	// Image conversion functiions:
	// This will compensate for negitive height or positive height images.
	// Negative height images are stored top down.
	// Positive height images are store bottom up. (flipped)
	// --------------------------------------------------------------------------------------------------------------------------
	void Convert1to16Bit(const UInt8* pIn, UInt16* pOut, Long width, Long height, Long padding, bool reverse);
	void Convert4to16Bit(const UInt8* pIn, UInt16* pOut, const UInt32* pQuad, Long width, Long height, Long padding, bool reverse);
	void Convert8to16Bit(const UInt8* pIn, UInt16* pOut, const UInt32* pQuad, Long width, Long height, Long padding, bool reverse);
	void Convert16to16Bit(const UInt8* pIn, UInt8* pOut, Long width, Long height, Long padding, bool reverse);
	void Convert24BitTo24Bit(const UInt8* pIn, UInt8* pOut, Long width, Long height, UInt16 bpp, Long padding, bool swapRGB, bool reverse);
	void Convert32BitTo32Bit(const UInt8* pIn, UInt8* pOut, Long width, Long height, UInt16 bpp, Long padding, bool swapRGBA, bool reverse);

	void Output16to16Bit(const UInt8* pIn, UInt8* pOut, Long width, Long height, Long padding, bool reverse);
	void OutputRGBtoBGR(const UInt8* pIn, UInt8* pOut, Long width, Long height, UInt16 bpp, Long padding, bool swapRGB, bool reverse);




#include "ImageTools.inl"

} // namespace Image


_ENGINE_END



#endif




/*
		void Convert8to16Bit(const UInt8* pIn, UInt8* pOut, const UInt32* pQuad, Long padding, Long width, Long height, bool reverse)
		{
			if (!pOut)
				return;

			if (reverse)
				pOut += width * height;

			// convert the 8 bit image to a 16 bit image.
			for (Long y = 0; y < height; ++y)
			{
				if (reverse)
					pOut -= width;

				for (Long x = 0; x < width; ++x)
				{
					UInt16 colour = X8R8G8B8toA1R5G5B5( pQuad[static_cast<UInt8>(*pIn)] );
					pOut[x] = LowerWord(colour);
					pOut[x+1] = UpperWord(colour);
					++pIn;
				}

				if (!reverse)
					pOut += width;

				pIn += padding;
			}
		}
*/