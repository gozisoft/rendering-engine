#ifndef __IIMAGE_LOADER_H__
#define __IIMAGE_LOADER_H__

#include "imagefwd.h"

_ENGINE_BEGIN

class IImageLoader
{
public:
	enum ImageLoaderType
	{
		ILT_NONE = 0,
		ILT_BMP,
		ILT_TGA,
		ILT_DDS
	};
	virtual ~IImageLoader();

	static IImageLoaderPtr CreateImageLoader(ImageLoaderType type);

	virtual CImagePtr LoadImageData(std::ifstream& inFile) = 0;
	virtual bool SaveImage(std::ofstream &outFile, const CImage &image) = 0;

protected:
	IImageLoader(); // protected constructor to prevent creation without using static function.

};

_ENGINE_END

#endif