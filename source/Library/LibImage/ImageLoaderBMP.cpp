#include "imageafx.h"
#include "ImageLoaderBMP.h"
#include "Image.h"
#include "ImageTools.h"
#include "AlignedAllocator.h"

using namespace Engine;
using namespace Image;

const UInt16 g_magicNum = 'MB';

// Converts a 32bit (X8R8G8B8) color to a 16bit (A1R5G5B5) color
inline UInt16 X8R8G8B8toA1R5G5B5(BGRQuad color)
{
	return (0x8000 | (color.red << 7) & 0xFC00 | (color.green << 2) & 0x3E0 | (color.blue >> 3) & 0x1F);
}

inline UInt32 ShiftRightByMask(UInt32 colour, UInt32 mask, UInt32 distributeToBits = 8)
{
	if (!mask)
		return 0;

	UInt32 shiftCount = 0;
	UInt32 testBit = 0x00000001;

	// Search from left to right of the bitfield
	// until an active (1) bit is found.
	while (shiftCount < 32) 
	{
		if (mask & testBit)
			break;

		testBit <<= 1;
		++shiftCount;
	}
	// shift the based colour to the right by the shift count
	UInt32 baseColour = (colour & mask) >> shiftCount; 

	// Colour filling
	UInt32 bitCount = 32;
	testBit = 0x80000000;

	// Search from right to left of the bitfield
	// until an active (1) bit is found.
	while (bitCount)
	{
		if ( (mask >> shiftCount) & testBit )
			break;

		testBit >>= 1;
		--bitCount;
	}

	if (distributeToBits > bitCount) 
	{
		// We have to fill lower bits
		UInt32 BitsToFill = distributeToBits - bitCount;
		while (--BitsToFill) 
		{
			baseColour <<= 1;

			if (baseColour & 1)
				baseColour |= 1;
		}
	} 
	else if (distributeToBits < bitCount) 
	{
		baseColour >>= (bitCount - distributeToBits);
	}

	return baseColour;
}

CImagePtr CImageLoaderBMP::LoadImageData(std::ifstream& inFile)
{
	// Do this at start as to avoid any complex
	// Moving of the read pointer around later.
	inFile.seekg(0, std::ios::end);
	std::streamoff length = inFile.tellg();
	inFile.seekg(0, std::ios::beg);

	// advance the buffer, and load in the magic number
	BMPFileMagic fileMagic;
	std::memset( &fileMagic, 0, sizeof(BMPFileMagic) );
	inFile.read( reinterpret_cast<char*>(&fileMagic), sizeof(BMPFileMagic) );

	if (fileMagic.magic != g_magicNum)
		return CImagePtr();

	// advance the buffer, and load in the file header
	BMPFileHeader fileHeader;
	std::memset(&fileHeader, 0, sizeof(BMPFileHeader));
	inFile.read( reinterpret_cast<char*>(&fileHeader), sizeof(BMPFileHeader) );

	BMPInfoHeader infoHeader;
	std::memset(&infoHeader, 0, sizeof(BMPInfoHeader));
	UInt32 headerSize = inFile.peek(); // look into the file for its headersize. Determines V5BMP header.
	inFile.read( reinterpret_cast<char*>(&infoHeader), headerSize );

	// Determine if a colour palette has been used.
	UInt32 coloursUsed = infoHeader.clrUsed;
	if (coloursUsed <= 0 || coloursUsed > UsedPaletteEntries(infoHeader.bitCount) )
		coloursUsed = UsedPaletteEntries(infoHeader.bitCount);

	//bgrArray quads;
	paletteArray quads;
	if (coloursUsed) // colour table available
	{				 // read in the colours
		quads.resize(coloursUsed);
		inFile.read( reinterpret_cast<char*>(&quads[0]), coloursUsed * sizeof(UInt32) ); 
	}

	// adjust bitmap data size to dword boundary
	if ( (infoHeader.imagesize % 4) != 0 )
		infoHeader.imagesize += ( 4 - (infoHeader.imagesize % 4) ) % 4;

	// guess the size, not provided by all tools
	if (!infoHeader.imagesize)
		infoHeader.imagesize = static_cast<UInt32>(length) - fileHeader.dataOffSet;

	// read in the image data
	uint8Array imageData(infoHeader.imagesize);
	inFile.read( reinterpret_cast<char*>(&imageData[0]), infoHeader.imagesize );
	inFile.close(); // finshed with the file. Now can close it.

	// debug data
	OutputInfo( infoHeader, ieS("Loaded Image") );

	// Calculate row bits  width * bitsperpixel
	Long rowBits = infoHeader.width*infoHeader.bitCount;

	// line width (num bytes per image row) based on (dword aligned)
	Long alignedRowBytes = AlignToPowerOf2(rowBits, 4U) / 8;

	// line width without the added padding
	Long actualRowBytes = rowBits / 8;

	// Determine the amount of padding present
	Long paddedBytes = alignedRowBytes - actualRowBytes;

	// some bitmaps store the height in a negative value
	Long height = (infoHeader.height > 0) ? infoHeader.height : -infoHeader.height;
	bool reverse = (infoHeader.height > 0) ? true : false;

	// Image that will be returned at the end of the function
	CImagePtr pImage = CImagePtr();

	switch (infoHeader.bitCount)
	{
	case 1:
		{
			pImage = CImagePtr( new CImage(CImageFormat::IF_A1R5G5B5, infoHeader.width, height) );

			Convert1to16Bit(&imageData[0], reinterpret_cast<UInt16*>( pImage->GetData() ),
				infoHeader.width, height, paddedBytes, reverse);
		}
		break;

	case 4: // involves colour pallette
		{
			if (infoHeader.compression == BMP_RLE4)
				Decompress8bitRLE(imageData, alignedRowBytes, infoHeader.width, height);

			pImage = CImagePtr( new CImage(CImageFormat::IF_A1R5G5B5, infoHeader.width, height) );

			Convert4to16Bit(&imageData[0], reinterpret_cast<UInt16*>( pImage->GetData() ),
				&quads[0], infoHeader.width, height, paddedBytes, reverse);
		}
		break;

	case 8: // involves colour pallette 
		{
			if (infoHeader.compression == BMP_RLE8)
				Decompress8bitRLE(imageData, alignedRowBytes, infoHeader.width, height);

			pImage = std::make_shared<CImage>(CImageFormat::IF_A1R5G5B5, infoHeader.width, height);

			Convert8to16Bit(&imageData[0], reinterpret_cast<UInt16*>( pImage->GetData() ),
				&quads[0], infoHeader.width, height, paddedBytes, reverse);
		}
		break;

	case 16:
		{
			// 0000F800
			// 000007E0
			// 0000001F
			if (infoHeader.compression == BMP_BITFIELDS)
			{				
				uint8Array newData(infoHeader.imagesize);
				UInt16* pNew = reinterpret_cast<UInt16*>(&newData[0]);
				UInt16* pSource = reinterpret_cast<UInt16*>(&imageData[0]);
				for (UInt32 i = 0; i < infoHeader.imagesize / 2; ++i, ++pSource)
				{
					UInt32 red = ShiftRightByMask( *pSource, infoHeader.redMask, 5 );
					UInt32 green = ShiftRightByMask( *pSource, infoHeader.greenMask, 6 );
					UInt32 blue = ShiftRightByMask( *pSource, infoHeader.blueMask, 5 );
				}
			}
			pImage = std::make_shared<CImage>(CImageFormat::IF_A1R5G5B5, infoHeader.width, height);

			Convert16to16Bit( &imageData[0], pImage->GetData(), infoHeader.width, height, paddedBytes, reverse);
		}
		break;

	case 24:
		{
			// 24 bitmap (BGR), represented in memory below:
			// -----------------------------------------------
			// 23:16   15:08	07:00
			// Red	   Green	Blue
			// -----------------------------------------------
			pImage = std::make_shared<CImage>(CImageFormat::IF_R8G8B8, infoHeader.width, height);
			Convert24BitTo24Bit(&imageData[0], pImage->GetData(), infoHeader.width,
				height, infoHeader.bitCount, paddedBytes, false, reverse);
		}
		break;
				
	case 32:
		{
			// 32 bitmap (BGRA), represented in memory below:
			// -----------------------------------------------
			// 31:24	23:16	15:08	07:00
			// Alpha	Red		Green	Blue
			// -----------------------------------------------
			if (infoHeader.compression == BMP_BITFIELDS)
			{

			}		
		
			// Bitmaps are stored in reversed order (ie upside down)
			// and in the ABGR colour format. Therefore they need to be
			// reverse read and the ABGR needs to be swapped to ARGB.
			pImage = std::make_shared<CImage>(CImageFormat::IF_A8R8G8B8, infoHeader.width, height);
			Convert32BitTo32Bit(&imageData[0], pImage->GetData(), infoHeader.width,
					height, infoHeader.bitCount, paddedBytes, false, reverse);
		}
		break;

	default:
		pImage = CImagePtr();
		break;
	};

	if (!imageData.empty())
		imageData.clear();

	if (!quads.empty())
		quads.clear();

	return pImage;
}

// pixels per meter : 39.37 * 72 = 2,834.64
bool CImageLoaderBMP::SaveImage(std::ofstream &outFile, const CImage &image)
{
	// set the magic number
	BMPFileMagic fileMagic = { g_magicNum };

	// write the magic number
	outFile.write( reinterpret_cast<char*>(&fileMagic), sizeof(BMPFileMagic) );

	BMPFileHeader fileHeader;
	std::memset( &fileHeader, 0, sizeof(BMPFileHeader) );

	fileHeader.dataOffSet = sizeof(BMPFileMagic) + sizeof(BMPFileHeader) + sizeof(BMPInfoHeader);
	fileHeader.size = image.GetImageSize() + fileHeader.dataOffSet;

	// write the BMPfileheader
	outFile.write( reinterpret_cast<char*>(&fileHeader), sizeof(BMPFileHeader) );

	BMPInfoHeader infoHeader;
	std::memset( &infoHeader, 0, sizeof(BMPInfoHeader) );

	infoHeader.headerSize = sizeof(BMPInfoHeader);
	infoHeader.bitCount = static_cast<UInt16>( BitsPerPixel( image.GetImageFormat() ) );

	infoHeader.planes = 1;
	infoHeader.width = image.GetBound(0);
	infoHeader.height = image.GetBound(1);
	infoHeader.imagesize = (infoHeader.width * infoHeader.height * infoHeader.bitCount) / 8;

	// line width based on width x bytesperpixel (not dword aligned)
	Long line = CalculateLine(infoHeader.width, infoHeader.bitCount);

	// line width with the added padding (dword aligned)
	Long pitch = CalculatePitch(line);
	Long padding = pitch - line;

	uint8Array data(pitch * infoHeader.height);

	if (infoHeader.bitCount == 24 || infoHeader.bitCount == 32)
		OutputRGBtoBGR(image.GetData(), &data[0], line, infoHeader.height, infoHeader.bitCount, padding, true, true);

	if (infoHeader.bitCount == 16)
		Output16to16Bit(image.GetData(), &data[0], line, infoHeader.height, padding, true);

	outFile.write( reinterpret_cast<char*>(&infoHeader), sizeof(BMPInfoHeader) );
	outFile.write( reinterpret_cast<char*>(&data[0]), data.size() );

	OutputInfo( infoHeader, ieS("Saved Image") );

	data.clear();
	outFile.close();
	return true;
}


// based on the http://www.binaryessence.com/dct/en000073.htm
// Read BITMAPINFOHEADER (3.0) / comments section.
void CImageLoaderBMP::Decompress8bitRLE(uint8Array& pData, Long stride, Long width, Long height)
{
	uint8Array newData(stride * height);
	uint8Itor newItor = newData.begin();
	uint8Itor pDataItor = pData.begin();

	UInt32 line = 0;

	while ( newItor != newData.end() ) // while (src < dst)
	{
		if (*pDataItor == 0) // first byte zero
		{
			++pDataItor; // point to next byte

			switch (*pDataItor)
			{
			case 0: // end of line
				++pDataItor; 
				++line;
				newItor += line * stride;
				break;

			case 1: // end of BMP data
				pData.clear();
				pDataItor = newItor;
				return;

			case 2: // Delta. The following 2 bytes define an unsigned pitch in x and y direction (y being up) The skipped pixels should get a color zero. 
				{
					++pDataItor; // increment pointer
					UInt8 deltaX = static_cast<UInt8>(*pDataItor);
					++pDataItor;
					UInt8 deltaY = static_cast<UInt8>(*pDataItor);
					++pDataItor;
					newItor += deltaX;
					newItor += deltaY * width;
				}
				break;

			default: // >= 3
				{
					// absolute mode based on fist byte set to zero
					UInt32 count = static_cast<UInt8>(*pDataItor);
					++pDataItor;

					for (UInt32 i = 0; i < count; ++i)
					{
						*newItor = *pDataItor;
						++pDataItor;
						++newItor;
					}

					// Align to wrod boundries
					UInt32 readAdditional = 2 - (count % 2) % 2;

					for (UInt32 i = 0; i < readAdditional; ++i)
						++pDataItor;
				}
			}; // switch

		} 
		else
		{
			// encoded Mode
			UInt32 count = static_cast<UInt8>(*pDataItor);
			++pDataItor;
			UInt8 colour = static_cast<UInt8>(*pDataItor); // extract the value of the next byte
			++pDataItor;

			for (UInt32 i = 0; i < count; ++i)
			{
				*newItor = colour;
				++newItor;
			}
		}

	} // while

	pData.clear();
	pDataItor = newItor;
}


// based on the http://www.binaryessence.com/dct/en000072.htm
void CImageLoaderBMP::Decompress4bitRLE(uint8Array& pData, Long stride, Long width, Long height)
{
	uint8Array newData(stride * height);
	uint8Itor newItor = newData.begin();
	uint8Itor pDataItor = pData.begin();

	UInt32 line = 0;

	bool evenRun = true;

	while ( newItor != newData.end() )
	{
		if (*pDataItor == 0)
		{
			++pDataItor; // point to next byte

			switch (*pDataItor)
			{
			case 0: // end of line
				++pDataItor; 
				++line;
				newItor += (line * stride);
				break;

			case 1: // end of BMP data
				pData.clear();
				pDataItor = newItor;
				return;

			case 2: // Delta. The following 2 bytes define an unsigned pitch in x and y direction (y being up) The skipped pixels should get a color zero. 
				{
					++pDataItor; // increment pointer
					UInt8 deltaX = static_cast<UInt8>(*pDataItor);
					++pDataItor;
					UInt8 deltaY = static_cast<UInt8>(*pDataItor);
					++pDataItor;
					newItor += deltaX;
					newItor += deltaY * width;
				}
				break;

			default: // >= 3
				{
					// absolute mode based on fist byte set to zero
					UInt32 count = static_cast<UInt8>(*pDataItor);
					++pDataItor;

					for (UInt32 i = 0; i < count; ++i)
					{
						UInt8 colour = 0;

						if (i % 2 == 0) // even
							colour = UpperByte( static_cast<UInt8>(*pDataItor) );
						else // odd
						{
							colour = LowerByte( static_cast<UInt8>(*pDataItor) );
							++pDataItor;
						}

						if (evenRun)
						{
							*newItor = ( *newItor & (~0xF0) | (colour << 4) & 0xF0 );
							evenRun = false;
						}
						else
						{
							*newItor = ( *newItor & (~0x0F) | colour & 0x0F );
							evenRun = true;
							++newItor;
						}
					}

					// Align to word boundries
					UInt32 readAdditional = ((2 - (count % 2)) % 2);

					for (UInt32 i = 0; i < readAdditional; ++i)
						++pDataItor;
				}
			}; // switch

		} 
		else
		{
			// encoded Mode
			UInt32 count = static_cast<UInt8>(*pDataItor);
			++pDataItor;

			// Color1  colour2
			// 0000    0000   <- 8 bits = 1 byte.
			UInt8 colour1 = UpperByte( static_cast<UInt8>(*pDataItor) ); // colour value stored in the upper byte
			UInt8 colour2 = LowerByte( static_cast<UInt8>(*pDataItor) ); // colour value stored in lower byte

			++pDataItor;

			// if i is an odd number set colour2
			// if i is an even number set colour1
			for (UInt32 i = 0; i < count; ++i)
			{
				UInt8 currentCol = (i % 2 == 0) ? colour1 : colour2;

				if (evenRun)
				{
					*newItor = ( *newItor & (~0xF0) | (currentCol << 4) & 0xF0 );
					evenRun = false;
				}
				else
				{
					*newItor = ( *newItor & (~0x0F) | currentCol & 0x0F );
					evenRun = true;
					++newItor;
				}
			}
		}

	} // while

	pData.clear();
	pDataItor = newItor;
}

void CImageLoaderBMP::OutputInfo(const BMPInfoHeader& infoHeader, const String &mode)
{
	Cout << mode << std::endl;
	Cout << std::endl;
	Cout << ieS("headerSize      =") << infoHeader.headerSize << std::endl;
	Cout << ieS("width           =") << infoHeader.width << std::endl;
	Cout << ieS("height          =") << infoHeader.height << std::endl;
	Cout << ieS("planes          =") << infoHeader.planes << std::endl;
	Cout << ieS("bitCount        =") << infoHeader.bitCount << std::endl;
	Cout << ieS("compression     =") << infoHeader.compression << std::endl;
	Cout << ieS("imagesize       =") << infoHeader.imagesize << std::endl;
	Cout << ieS("biXPelsPerMeter =") << infoHeader.xPelsPerMeter << std::endl;
	Cout << ieS("biYPelsPerMeter =") << infoHeader.yPelsPerMeter << std::endl;
	Cout << ieS("clrImportant    =") << infoHeader.clrImportant << std::endl;
	Cout << ieS("clrUsed         =") << infoHeader.clrUsed << std::endl;
	Cout << std::endl;
}



//else if (infoHeader.bitCount == 16)			
//{
//infoHeader.compression = BMP_BITFIELDS;	// RGB555
//infoHeader.alphaMask   = 0xF8000000;	// 1111 1000 0000 0000 0000 0000 0000 0000
//infoHeader.redMask     = 0x07C00000;	// 0000 0111 1100 0000 0000 0000 0000 0000 
//infoHeader.greenMask   = 0x003E0000;	// 0000 0000 0011 1110 0000 0000 0000 0000 
//infoHeader.blueMask    = 0x0001F000;	// 0000 0000 0000 0001 1111 0000 0000 0000 
//}


//inline UInt32 ShiftRightByMask(UInt32 colour, UInt32 mask, UInt32 distributeToBits = 8)
//{
//	if (!mask)
//		return 0;
//
//	UInt32 shiftCount = 0;
//	UInt32 testBit = 0x00000001;
//
//	// Search from left to right of the bitfield
//	// untill an active (1) bit is found.
//	while (shiftCount < 32) 
//	{
//		if (mask & testBit)
//			break;
//
//		testBit <<= 1;
//		++shiftCount;
//	}
//
//	UInt32 bitCount = 32;
//	testBit = 0x80000000;
//
//	// Search from right to left of the bitfield
//	// untill an active (1) bit is found.
//	while (bitCount)
//	{
//		if ( (mask >> shiftCount) & testBit )
//			break;
//
//		testBit >>= 1;
//		--bitCount;
//	}
//
//	UInt32 baseColour = (colour & mask) >> shiftCount; 
//
//	if (distributeToBits > bitCount) 
//	{
//		// We have to fill lower bits
//		UInt32 BitsToFill = distributeToBits - bitCount;
//		while (--BitsToFill) 
//		{
//			baseColour <<= 1;
//
//			if (baseColour & 1)
//				baseColour |= 1;
//		}
//	} 
//	else if (distributeToBits < bitCount) 
//	{
//		baseColour >>= (bitCount - distributeToBits);
//	}
//
//	return baseColour;
//}