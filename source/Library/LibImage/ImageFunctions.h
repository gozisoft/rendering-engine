#ifndef __IMAGE_FUNCTIONS_H__
#define __IMAGE_FUNCTIONS_H__

#include "IImageLoader.h"

_ENGINE_BEGIN

namespace Image
{
	// functions used to load an image or save an image. This is an example of prefering non-member non-friend
	// functions to member functinos in order to increase encalpsulation, packaging flexibility and functional
	// extensibility. (Effective C++ 3rd ed. p102)
	CImagePtr LoadImageData(const String& fileName);
	bool SaveImage(const String &fileName, const CImage &image, IImageLoader::ImageLoaderType formatType);
}

_ENGINE_END



#endif