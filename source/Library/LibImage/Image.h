#ifndef __CIMAGE_H__
#define __CIMAGE_H__

#include "IImage.h"

_ENGINE_BEGIN

class CImage : public IImage
{
public:
	// 1D image with Mips
	CImage(CImageFormat::Type fmt, UInt bound0, UInt8* pData=nullptr,
		UInt imageSize=0, UInt numMips=0);

	// 2D image with Mips
	CImage(CImageFormat::Type fmt, UInt bound0, UInt bound1, UInt8* pData=nullptr,
		UInt imageSize=0, UInt numMips=0);

	// 3D image with Mips
	CImage(CImageFormat::Type fmt, UInt bound0, UInt bound1, UInt bound2, 
		UInt8* pData=nullptr, UInt imageSize=0, UInt numMips=0);

	// destructor
	~CImage();

	// Get the image format
	CImageFormat::Type GetImageFormat() const;

	// image dimension (1D, 2D, 3D)
	CImageDimensions::Type GetDimension() const;

	// Contains : 0) width, 1) height, 2) pitch
	UInt GetBound(size_t index) const;

	// A default image will have a mip count of 1 
	// (as this is the single fullsize image itself)
	UInt GetMipLevels() const;

	// Check if the image has stored bitmap data
	bool IsBitMapped() const;

	// Size of the image
	UInt GetImageSize() const;

	// const image data acccess
	const UInt8* GetData() const;

	// non const image data
	UInt8* GetData();

protected:
	CImage();

private:
	CImageFormat::Type m_format;
	CImageDimensions::Type m_dimension;		 // number of active dimensions
	UInt m_bound[3];					 // width, height, depth
	UInt m_numMipLevels;
	UInt8Vector m_pData; 
};

_ENGINE_END

#endif



	// Used for accessing a pixel within the image
	// Knowing your image format you can cast the UInt8* to whichever
	// form you need. I.E if your format is RGAB8888 each pixel is 4 x sizeof(char)
	// therefore to get the entire RGBA value into a DWORD, 
	// just cast UInt8* to DWORD. (D3DCOLOR)
	// UInt8* operator() (UInt pos);
