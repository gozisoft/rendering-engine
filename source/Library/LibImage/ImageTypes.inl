#ifndef __IMAGE_TYPES_INL__
#define __IMAGE_TYPES_INL__

inline UInt BytesPerPixel(CImageFormat::ImageFormat tformat)
{
	switch (tformat)
	{
	case CImageFormat::IF_NONE: return 0;				 // CImageFormat::IF_NONE
	case CImageFormat::IF_R5G6B5: return 2;			 // CImageFormat::IF_R5G6B5
	case CImageFormat::IF_A1R5G5B5: return 2;			 // CImageFormat::IF_A1R5G5B5
	case CImageFormat::IF_A4R4G4B4: return 2;			 // CImageFormat::IF_A4R4G4B4
	case CImageFormat::IF_A8: return 1;				 // CImageFormat::IF_A8
	case CImageFormat::IF_L8: return 1;				 // CImageFormat::IF_L8
	case CImageFormat::IF_A8L8: return 2;				 // CImageFormat::IF_A8L8
	case CImageFormat::IF_R8G8B8: return 3;			 // CImageFormat::IF_R8G8B8
	case CImageFormat::IF_B8G8R8: return 3;			 // CImageFormat::IF_B8G8R8
	case CImageFormat::IF_A8R8G8B8: return 4;			 // CImageFormat::IF_A8R8G8B8
	case CImageFormat::IF_A8B8G8R8: return 4;			 // CImageFormat::IF_A8B8G8R8
	case CImageFormat::IF_L16: return 2;				 // CImageFormat::IF_L16
	case CImageFormat::IF_G16R16: return 4;			 // CImageFormat::IF_G16R16
	case CImageFormat::IF_A16B16G16R16: return 8;		 // CImageFormat::IF_A16B16G16R16
	case CImageFormat::IF_R16F: return 2;				 // CImageFormat::IF_R16F
	case CImageFormat::IF_G16R16F: return 4;			 // CImageFormat::IF_G16R16F
	case CImageFormat::IF_A16B16G16R16F: return 8;	 // CImageFormat::IF_A16B16G16R16F
	case CImageFormat::IF_R32F: return 4;				 // CImageFormat::IF_R32F
	case CImageFormat::IF_G32R32F: return 8;			 // CImageFormat::IF_G32R32F
	case CImageFormat::IF_A32B32G32R32F: return 16;	 // CImageFormat::IF_A32B32G32R32F,
	case CImageFormat::IF_DXT1: return 8;				 // CImageFormat::IF_DXT1 (special handling)
	case CImageFormat::IF_DXT3: return 16;			 // CImageFormat::IF_DXT3 (special handling)
	case CImageFormat::IF_DXT5: return 16;			 // CImageFormat::IF_DXT5 (special handling)
	case CImageFormat::IF_D24S8: return 4;			 // CImageFormat::IF_D24S8
	default:
		assert(false); 
		return 0;
	};
}

inline UInt BitsPerPixelIImage(CImageFormat::ImageFormat tformat)
{
	return BytesPerPixel(tformat) * 8;
}

#endif