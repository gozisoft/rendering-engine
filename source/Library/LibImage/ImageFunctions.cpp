#include "imageafx.h"
#include "ImageFunctions.h"
#include "StringFunctions.h"
#include <regex>

_ENGINE_BEGIN

namespace Image
{
	IImageLoader::ImageLoaderType MapImageType(const String& results)
	{
		if ( results == ieS(".bmp") ) return IImageLoader::ILT_BMP;
		else if ( results == ieS(".tga") ) return IImageLoader::ILT_TGA;
		else if ( results == ieS(".dds") ) return IImageLoader::ILT_DDS;

		// No match found
		return IImageLoader::ILT_NONE;
	}

	// functions used to load an image or save an image. This is an example of prefering non-member non-friend
	// functions to member functinos in order to increase encalpsulation, packaging flexibility and functional
	// extensibility. (Effective C++ 3rd ed. p102)
	CImagePtr LoadImageData(const String& fileName)
	{
#if defined (UNICODE) || defined(_UNICODE)
		typedef std::basic_regex<wchar_t> _Regex;
		typedef std::match_results<const wchar_t *> _CMatch;
		typedef std::match_results<std::wstring::const_iterator> _SMatch;
#else
		typedef std::basic_regex<char> _Regex;
		typedef std::match_results<const char *> _CMatch;
		typedef std::match_results<std::string::const_iterator> _SMatch;
#endif

		typedef _Regex Regex;
		typedef _CMatch CMatch;
		typedef _SMatch SMatch;

		// Search for file extension
		Regex reg( ieS("/^.*\\.(bmp|tga|dds)$/i") );

		SMatch match;
		if ( std::regex_search(filename, match, reg) == false )
		{
			assert( false && ieS("Cannot find the correct file extension") );
			return CImagePtr();
		}

		// Map the text to the enum type
		IImageLoader::ImageLoaderType fileType = MapImageType( match.str() );

		// get absolute path
		String pathName = GetFullPath( fileName.c_str() );

		// open file as binary
		std::ifstream fileIn(fileName.c_str(), std::ifstream::binary);

		if (!fileIn.good())
			fileIn.open(pathName.c_str(), std::ifstream::binary); // try to open file using raw filename.

		if (fileIn.good())
		{
			IImageLoaderPtr pImageLoader = IImageLoader::CreateImageLoader(fileType);

			CImagePtr pImage = CImagePtr();

			if (pImageLoader)
				pImage = pImageLoader->LoadImageData(fileIn);

			if (fileIn.is_open())					
				fileIn.close(); // close file after loading

			return pImage;
		}
		else
		{
			assert (!"Could not open File");
			return CImagePtr();
		}
	}


	bool SaveImage(const String &fileName, const CImage &image, IImageLoader::ImageLoaderType formatType)
	{
		std::ofstream fileOut( fileName.c_str(), std::ofstream::binary );

		if (fileOut.good())
		{
			IImageLoaderPtr pImageLoader = IImageLoader::CreateImageLoader(formatType);

			if (pImageLoader)
				return pImageLoader->SaveImage(fileOut, image);

			return false;
		}

		return false;
	}


} // namespace Image



_ENGINE_END