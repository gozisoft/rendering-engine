#ifndef __IMAGE_FORWARD_H__
#define __IMAGE_FORWARD_H__

#include "Core.h"

_ENGINE_BEGIN

// Image
class IImage;
class IImageLoader;

class CImage;
class CImageLoaderBMP;
class CImageLoaderTGA;

// shared_ptr typedefs
typedef std::shared_ptr<IImage> IImagePtr;
typedef std::shared_ptr<IImageLoader> IImageLoaderPtr;

typedef std::shared_ptr<CImage> CImagePtr;
typedef std::shared_ptr<CImageLoaderBMP> CImageLoaderBMPPtr;
typedef std::shared_ptr<CImageLoaderTGA> CImageLoaderTGAPtr;

_ENGINE_END

#endif