#ifndef __IIMAGE_H__
#define __IIMAGE_H__

#include "ImageTypes.h"

_ENGINE_BEGIN

class IImage
{
public:
	// Virtual destructor
	virtual ~IImage() { /**/ }

	// Get the image format
	virtual CImageFormat::Type GetImageFormat() const = 0;

	// image dimension (1D, 2D, 3D)
	virtual CImageDimensions::Type GetDimension() const = 0;

	// Contains : 0) width, 1) height, 2) pitch
	virtual UInt GetBound(size_t index) const = 0;

	// A default image will have a mip count of 1 
	// (as this is the single fullsize image itself)
	virtual UInt GetMipLevels() const = 0;

	// Check if the image has stored bitmap data
	virtual bool IsBitMapped() const = 0;

	// Size of the image
	virtual UInt GetImageSize() const = 0;

	// const image data acccess
	virtual const UInt8* GetData() const = 0;

	// non const image data
	virtual UInt8* GetData() = 0;
};


_ENGINE_END

#endif