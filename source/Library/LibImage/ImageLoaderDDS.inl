#ifndef __CIMAGE_LOADER_DDS_INL__
#define __CIMAGE_LOADER_DDS_INL__

inline Image::ImageFormat CImageLoaderDDS::ConvertToFormat(DXGI_FORMAT dxgiFormat)
{
	switch (dxgiFormat)
	{
	case DXGI_FORMAT_UNKNOWN: return Image::IF_NONE;
	case DXGI_FORMAT_B5G6R5_UNORM:	return Image::IF_R5G6B5;
	case DXGI_FORMAT_B5G5R5A1_UNORM: return Image::IF_A1R5G5B5;

	case DXGI_FORMAT_A8_UNORM:	return Image::IF_A8; 
	case DXGI_FORMAT_R8_UNORM:	return Image::IF_L8;		
	case DXGI_FORMAT_R8G8B8A8_UNORM: return Image::IF_A8B8G8R8;

	case DXGI_FORMAT_R16_UNORM: return Image::IF_L16;
	case DXGI_FORMAT_R16G16_UNORM:	return Image::IF_G16R16;
	case DXGI_FORMAT_R16G16B16A16_UNORM: return Image::IF_A16B16G16R16;

	case DXGI_FORMAT_R16_FLOAT: return Image::IF_R16F;
	case DXGI_FORMAT_R16G16_FLOAT: return Image::IF_G16R16F;
	case DXGI_FORMAT_R16G16B16A16_FLOAT: return Image::IF_A16B16G16R16;
	
	case DXGI_FORMAT_R32_FLOAT: return Image::IF_R32F;
	case DXGI_FORMAT_R32G32_FLOAT: return Image::IF_G32R32F;
	case DXGI_FORMAT_R32G32B32A32_FLOAT: return Image::IF_A32B32G32R32F;

	case DXGI_FORMAT_BC1_UNORM: return Image::IF_DXT1;
	case DXGI_FORMAT_BC2_UNORM: return Image::IF_DXT3;
	case DXGI_FORMAT_BC3_UNORM: return Image::IF_DXT5;

	case DXGI_FORMAT_D32_FLOAT: return Image::IF_D32F;
	case DXGI_FORMAT_D24_UNORM_S8_UINT : return Image::IF_S8D24;
	case DXGI_FORMAT_D16_UNORM: return Image::IF_D16;
	default:
		assert( false && ieS("Unknown texture format: CImageLoaderDDS::ConvertToFormat") );
		return Image::IF_NONE;
	}
}

inline Image::ImageFormat CImageLoaderDDS::ConvertToFormat(D3DFORMAT d3dFormat)
{
	switch (d3dFormat)
	{
		case D3DFMT_UNKNOWN:		return Image::IF_NONE;            
		case D3DFMT_R5G6B5:			return Image::IF_R5G6B5;				
		case D3DFMT_A1R5G5B5:		return Image::IF_A1R5G5B5;            
		case D3DFMT_A4R4G4B4:		return Image::IF_A4R4G4B4;			
		case D3DFMT_A8:				return Image::IF_A8;					
		case D3DFMT_L8:				return Image::IF_L8;					
		case D3DFMT_A8L8:			return Image::IF_A8L8;			  
		case D3DFMT_R8G8B8:			return Image::IF_R8G8B8;		   
		case D3DFMT_A8R8G8B8:		return Image::IF_A8R8G8B8;		   
		case D3DFMT_A8B8G8R8:		return Image::IF_A8B8G8R8;		   
		case D3DFMT_L16:			return Image::IF_L16;					
		case D3DFMT_G16R16:			return Image::IF_G16R16;			
		case D3DFMT_A16B16G16R16:	return Image::IF_A16B16G16R16;	
		case D3DFMT_R16F:			return Image::IF_R16F;				
		case D3DFMT_G16R16F:		return Image::IF_G16R16F;			
		case D3DFMT_A16B16G16R16F:	return Image::IF_A16B16G16R16F;
		case D3DFMT_R32F:			return Image::IF_R32F;				
		case D3DFMT_G32R32F:		return Image::IF_G32R32F;			
		case D3DFMT_A32B32G32R32F:	return Image::IF_A32B32G32R32F;
		case D3DFMT_DXT1:			return Image::IF_DXT1;					
		case D3DFMT_DXT3:			return Image::IF_DXT3;					
		case D3DFMT_DXT5:			return Image::IF_DXT5;					
		case D3DFMT_D24S8:			return Image::IF_D24S8;
		default:
			assert( false && ieS("Unknown texture format: CImageLoaderDDS::ConvertToFormat") );
			return Image::IF_NONE; 
	}
}

inline bool CImageLoaderDDS::IsBitmask(const DDS_PIXELFORMAT& ddpf, UInt32 r, UInt32 g, UInt32 b, UInt32 a)
{
	return ddpf.RBitMask == r &&
		ddpf.GBitMask == g &&
		ddpf.BBitMask == b &&
		ddpf.ABitMask == a;
}


#endif