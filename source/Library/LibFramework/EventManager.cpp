#include "libframeworkafx.h"
#include "EventManager.h"
#include "IEventData.h"
#include "Timer.h"
#include "SafeDelete.h"
#include <algorithm>
#include <iterator>
#include <stdexcept>

using namespace engine;

CEventManager::CEventManager(Timer* timer, size_t numQueues)
	: 
m_timer(timer),
m_queues(numQueues),
m_activeQueue(0)
{

}

CEventManager::~CEventManager()
{
}

void CEventManager::Update(size_t maxProcessTime)
{
	size_t maxMS = maxProcessTime;
	if ( maxMS < std::numeric_limits<size_t>::max() )
	{
		maxMS = static_cast<size_t>( m_timer->GetRealTime() ) + maxProcessTime;
	}

	// Process selected queue
	EventQueue& currentQueue = m_queues[m_activeQueue];
	while ( currentQueue.empty() != true )
	{
		// Trigger the first event data. (FIFO)
		Trigger( currentQueue.front() );

		// Remove the event data from the queue once it has been triggerd.
		currentQueue.pop_front();

		// Check time constraint
		size_t currMS = static_cast<size_t>( m_timer->GetRealTime() );
		if (maxProcessTime != std::numeric_limits<size_t>::max() && currMS >= maxMS)
		{
			// time ran out, break from processing loop.
			break;
		}
	}

	// If we couldn't process all of the events, push the remaining events to the next active queue.
	// Note: To preserve sequencing, go back-to-front, inserting them at the head of the active queue
	if ( currentQueue.empty() != true )
	{
		// using the mod operator stops the number m_activeQueue num going beyond the max number of queues.
		m_activeQueue = (m_activeQueue + 1) % m_queues.size();

		// This will be the next queue to process
		EventQueue& nextQueue = m_queues[m_activeQueue];
		nextQueue.insert( std::begin(nextQueue), std::begin(currentQueue), 
			std::end(currentQueue) );

		// Clear the old queue
		currentQueue.clear();
	}
}

view_handle CEventManager::Attach(EventFunction& function, const IEventData& inType)
{
	// Create a type index
	const CTypeIndex& index = Typeid(inType);

	// The shared ptr of a signal
	EventSignal signal = EventSignal();

	// Find the EventDelegateList for an event type. 
	auto map_iter = m_delegateMap.find(index);
	if ( map_iter != std::end(m_delegateMap) )
	{
		// Get the signal
		signal = (*map_iter).second;
	}
	else
	{
		// Can't find delegate within the map, create a new one
		signal = std::make_shared<signal_t>();

		// Insert the new signal to the map
		m_delegateMap.insert( std::make_pair(index, signal) );		
	}

	// Connect the function
	ConnectionProxy* pCProxy = new ConnectionProxy(signal->connect(function));
	PVIEWHANDLE handle = reinterpret_cast<PVIEWHANDLE>(pCProxy);
	return view_handle(handle);
}

view_handle CEventManager::Attach(EventFunction& function, const IEventData* inTypes, size_t count)
{
	//// The shared ptr of a signal
	//EventSignal signal = EventSignal();

	//// Array of connections
	//PairArray pairs(count);
	//
	//// Iterate over the types
	//for (size_t i = 0; i < count; ++i)
	//{
	//	// Create a type index
	//	CTypeIndex index( Typeid( inTypes[i] ) );

	//	// Find the EventDelegateList for an event type. 
	//	auto map_iter = m_delegateMap.find(index);
	//	if ( map_iter != std::end(m_delegateMap) )
	//	{
	//		// Get the signal
	//		signal = (*map_iter).second;
	//	}
	//	else
	//	{
	//		// Can't find delegate within the map, create a new one
	//		signal = std::make_shared<signal_t>();

	//		// Insert the new signal to the map
	//		m_delegateMap.insert( std::make_pair(index, signal) );		
	//	}

	//	// connect the function
	//	// pairs[i].first = signal->connect(function);
	//	// pairs[i].second = index;
	//}

	//// Create a smart pointer to the connection object and return it
	//auto var = &pairs;
	//return view_handle(nullptr); // view_handle( (PVIEWHANDLE)var, disconnector() );
	return view_handle();
}

void CEventManager::Detach(view_handle handle, const IEventData* inTypes, size_t count)
{
	// Cast to the connection type
	//PairArray& pairs = *(PairArray*)handle.get();
	//for (size_t i = 0; i < count; ++i)
	//{
	//	auto iter = std::find_if(pairs.begin(), pairs.end(),
	//		[&inTypes, &i] (Connection_Type_Pair& conn_type_pair)
	//	{
	//		return conn_type_pair.second == Typeid( inTypes[i] );
	//	} );

	//	if ( iter != pairs.end() )
	//	{
	//		pairs.erase(iter);
	//	}
	//}
}

void CEventManager::Trigger(IEventDataPtr inEvent) const
{
	// Find the EventDelegateList for an event type. 
	//auto map_iter = m_delegateMap.find( Typeid(*inEvent) );
	//if ( map_iter != std::end(m_delegateMap) )
	//{
	//	// Get the delegate list
	//	const EventSignal& signal = (*map_iter).second;

	//	// Execute all the delgates related to this event type
	//	(*signal)(inEvent);
	//}
	//else
	//{
	//	// Can't find delegate within the map
	//	throw std::runtime_error("Can't find EventDelegate within map." \
	//		" [CEventManager::Trigger]"); 
	//}
}

void CEventManager::QueueEvent(IEventDataPtr inEvent)
{
	// Find the EventDelegateList for an event type. 
	//auto map_iter = m_delegateMap.find( Typeid(*inEvent) );
	//if ( map_iter!= std::end(m_delegateMap) )
	//{
	//	m_queues[m_activeQueue].push_back(inEvent);
	//}
	//else
	//{
	//	// Can't find delegate within the map
	//	throw std::runtime_error("Can't find EventDelegate within map." \
	//		" [CEventManager::QueueEvent]"); 
	//}
}

void CEventManager::AbortEvent(IEventDataPtr inEvent, AbortEventRule abortRule)
{
#if defined (DEBUG) || defined (_DEBUG)
	// Validate the eventype, ensure it's within registerd event types
	auto map_iter = m_delegateMap.find( Typeid(*inEvent) );
	if ( map_iter == std::end(m_delegateMap) )
	{
		// Can't find delegate within the map
		throw std::runtime_error("Can't find Event type within map." \
			" [CEventManager::Trigger]"); 
	}
#endif

	// Look into the active queue and find the event data
	EventQueue& queue = m_queues[m_activeQueue];
	switch (abortRule)
	{
	case ALL:
		{
			auto end = std::remove(queue.begin(), queue.end(), inEvent);
			queue.erase( end, queue.end() );
		}
		break;
	case FIRST:
		{
			auto iter = std::find(queue.begin(), queue.end(), inEvent);
			queue.erase(iter);
		}
		break;
	case LAST:
		{
			auto iter = std::find(queue.rbegin(), queue.rend(), inEvent);
			queue.erase( (++iter).base() );
		}
		break;
	default:
		break;
	};	
}
