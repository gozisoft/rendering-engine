#pragma once
#ifndef SIZE_EVENT_H
#define SIZE_EVENT_H

#include "EngineEvent.h"

namespace engine
{

class SizeEvent: public EngineEvent<SizeEvent>
{
public:
    static const std::string SIZE_EVENT;

    SizeEvent(uint_t width, uint_t height)
        : EngineEvent(SIZE_EVENT), m_width(width), m_height(height)
    {

    }

    uint_t width() const
    {
        return m_width;
    }

    uint_t height() const
    {
        return m_height;
    }

private:
    uint_t m_width;
    uint_t m_height;
};

}

#endif