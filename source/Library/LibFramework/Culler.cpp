#include "libframeworkafx.h"
#include "Culler.h"
#include "Camera.h"
#include "VisualGroup.h"
#include "Plane.h"
#include "Intersections.h"
#include <vector>

using namespace engine;


CCuller::visible_items CCuller::computeVisible(const plane_list& planes, CRootNodePtr root)
{
	// List of the visible items to return
	visible_items visibleSet;

	// Test if objects lie within the planes.
	size_t visibleFlags = 0xff;

	// Iterate over the children and test for collision.
	for (size_t i = 0; i < root->childCount(); ++i)
	{
		// Get a spatial pointer (not reference counted!).
		auto spatial = root->getChild(i);
		auto cullMode = spatial->getCullMode();

		// If always cull, we ignore this spatial object.
		if (cullMode == Spatial::CM_ALWAYS)
			continue;

		// If this is a root node
		auto rootPtr = DynamicPointerCast<VisualGroup>(spatial);
		if (rootPtr != nullptr)
		{
			auto newSet = computeVisible(planes, root);
			visibleSet.insert(std::end(visibleSet), std::begin(newSet), std::end(newSet));
		}
		else
		{
			// We have a visual node, cast to that.
			auto visual = DynamicPointerCast<Visual>(spatial);

			// Never cull mode or object is visible.
			if (cullMode == Spatial::CM_NEVER || isVisible(planes, spatial->worldBound()))
			{
				visibleSet.push_back(visual);
			}
		}
	}


	return visibleSet;
}

std::vector<Plane3f> CCuller::calcPlanesStandard(const Camera& camera)
{
	// Grab a reference to the camera's frustum bounds.
	std::vector<float> bounds(camera.GetFrustum(), camera.GetFrustum() + Camera::NUM_PLANES);

	// First calculate the point in the middle of the far plane.
	// Add the position of the camera to forward vector and then
	// scale it by the far distance of the camera.
	auto farCenter = camera.position() + camera.forward() * bounds[Camera::S_FAR];

	// Compute the centers of the near and far planes
	auto nc = camera.position() + camera.forward() * bounds[Camera::S_NEAR];
	auto fc = camera.position() + camera.forward() * bounds[Camera::S_FAR];

	std::vector<Plane3f> plane(Camera::NUM_PLANES);

	// Update the near plane.
	plane[Camera::S_NEAR] = Plane3f(nc, camera.forward());

	// Update the far plane.
	plane[Camera::S_FAR] = Plane3f(nc, -camera.forward());

	// Calculate point on the right plane.
	auto pr = nc + camera.right() * bounds[Camera::S_RIGHT];

	// Vector leading to the right plane from the camera position.
	auto vec = pr - camera.position();
	vec.normalize();

	// Build the plane
	plane[Camera::S_RIGHT] = Plane3f(pr, vec);

	// Build the left plane
	Point3f pl = nc + camera.right() * bounds[Camera::S_LEFT];
	plane[Camera::S_LEFT] = Plane3f(pl, cml::normalize(pl - camera.position()));

	// Build the top plane
	Point3f pt = nc + camera.up() * bounds[Camera::S_TOP];
	plane[Camera::S_TOP] = Plane3f(pt, cml::normalize(pt - camera.position()));

	// Build the bottom plane
	Point3f pb = nc + camera.up() * bounds[Camera::S_BOTTOM];
	plane[Camera::S_TOP] = Plane3f(pt, cml::normalize(pb - camera.position()));

	return plane;
}

std::vector<Plane3f> CCuller::calcPlanesSlow(const Camera& camera)
{
	// Grab a reference to the camera's frustum bounds.
	std::vector<float> bounds(camera.GetFrustum(), camera.GetFrustum() + Camera::NUM_PLANES);

	// Compute the centers of the near and far planes
	Vector3f nc = camera.position() + camera.forward() * bounds[Camera::S_NEAR];
	Vector3f fc = camera.position() + camera.forward() * bounds[Camera::S_FAR];

	// compute the 4 corners of the frustum on the near plane
	Vector3f ntl = nc + (camera.up() * bounds[Camera::S_TOP]) - (camera.right() * bounds[Camera::S_RIGHT]);
	Vector3f ntr = nc + (camera.up() * bounds[Camera::S_TOP]) + (camera.right() * bounds[Camera::S_RIGHT]);
	Vector3f nbr = nc + (-camera.up() * bounds[Camera::S_TOP]) + (camera.right() * bounds[Camera::S_RIGHT]);
	Vector3f nbl = nc + (-camera.up() * bounds[Camera::S_TOP]) - (camera.right() * bounds[Camera::S_RIGHT]);

	// Get the original angle in radians.
	float halfAngleRadians = bounds[Camera::S_TOP] / bounds[Camera::S_NEAR];
	float aspectRatio = bounds[Camera::S_RIGHT] / bounds[Camera::S_TOP];

	// Far bounds
	float farBounds[6];
	farBounds[Camera::S_TOP] = bounds[Camera::S_FAR] * std::tan(halfAngleRadians);
	farBounds[Camera::S_RIGHT] = aspectRatio * farBounds[Camera::S_TOP];
	farBounds[Camera::S_BOTTOM] = -farBounds[Camera::S_TOP];
	farBounds[Camera::S_LEFT] = -farBounds[Camera::S_RIGHT];

	// Compute the 4 corners of the frustum on the far plane
	Vector3f ftl = fc + (camera.up() * farBounds[Camera::S_TOP]) - (camera.right() * farBounds[Camera::S_LEFT]);
	Vector3f ftr = fc + (camera.up() * farBounds[Camera::S_TOP]) + (camera.right() * farBounds[Camera::S_RIGHT]);
	Vector3f fbr = fc + (-camera.up() * farBounds[Camera::S_TOP]) + (camera.right() * farBounds[Camera::S_RIGHT]);
	Vector3f fbl = fc + (-camera.up() * farBounds[Camera::S_TOP]) - (camera.right() * farBounds[Camera::S_LEFT]);

	// Compute the six planes.
	std::vector<Plane3f> planes(Camera::NUM_PLANES);
	planes[Camera::S_TOP] = Plane3f(ntr, ntl, ftl);
	planes[Camera::S_BOTTOM] = Plane3f(nbl, nbr, fbr);
	planes[Camera::S_LEFT] = Plane3f(ntl, nbl, fbl);
	planes[Camera::S_RIGHT] = Plane3f(nbr, ntr, fbr);
	planes[Camera::S_NEAR] = Plane3f(ntl, ntr, nbr);
	planes[Camera::S_FAR] = Plane3f(ftr, ftl, fbl);

	return planes;
}

std::vector<Plane3f> CCuller::calcPlanesWorking(const Camera& camera)
{
	// Get the camera coordinate frame.
	const Vector3f& position = camera.position();
	const Vector3f& forward = camera.forward();
	const Vector3f& up = camera.up();
	const Vector3f& right = camera.right();
	
	// Grab a reference to the camera's frustum bounds.
	std::vector<float> bounds(camera.GetFrustum(), camera.GetFrustum() + Camera::NUM_PLANES);

	// viewing angle
	float dirDotEye = cml::dot(position, forward); 

	// Squared width and heights of the near plane.
	float near2 = Sqr(bounds[Camera::S_NEAR]);
	float bottom2 = Sqr(bounds[Camera::S_BOTTOM]);
	float top2 = Sqr(bounds[Camera::S_TOP]);
	float left2 = Sqr(bounds[Camera::S_LEFT]);
	float right2 = Sqr(bounds[Camera::S_RIGHT]);

	// Planes to be returned.
	std::vector<Plane3f> plane(Camera::NUM_PLANES);

	// Update the near plane.
	plane[Camera::S_NEAR].m_normal = forward;
	plane[Camera::S_NEAR].m_disOrigin = dirDotEye + bounds[Camera::S_NEAR];

	// Update the far plane.
	plane[Camera::S_FAR].m_normal = -forward;
	plane[Camera::S_FAR].m_disOrigin = -(dirDotEye + bounds[Camera::S_FAR]);

	// Update the bottom plane
	float invLength = 1 / std::sqrt(near2 + bottom2);
	float c0 = -bounds[Camera::S_BOTTOM] * invLength;  // D component
	float c1 = +bounds[Camera::S_NEAR] * invLength;	 // U component
	Vector3f normal = c0 * forward + c1 * up;
	plane[Camera::S_BOTTOM].m_normal = normal;
	plane[Camera::S_BOTTOM].m_disOrigin = -cml::dot(position, normal);

	// Update the top plane.
	invLength = 1 / std::sqrt(near2 + top2);
	c0 = +bounds[Camera::S_TOP] * invLength;  // D component
	c1 = -bounds[Camera::S_NEAR] * invLength;  // U component
	normal = c0 * forward + c1 * up;
	plane[Camera::S_TOP].m_normal = normal;
	plane[Camera::S_TOP].m_disOrigin = -cml::dot(position, normal);

	// Update the left plane.
	invLength = 1 / std::sqrt(near2 + left2);
	c0 = -bounds[Camera::S_LEFT] * invLength;  // D component
	c1 = +bounds[Camera::S_NEAR] * invLength;  // R component
	normal = c0 * forward + c1* right;
	plane[Camera::S_LEFT].m_normal = normal;
	plane[Camera::S_LEFT].m_disOrigin = -cml::dot(position, normal);

	// Update the right plane.
	invLength = 1 / std::sqrt(near2 + right2);
	c0 = +bounds[Camera::S_RIGHT] * invLength;  // D component
	c1 = -bounds[Camera::S_NEAR] * invLength;  // R component
	normal = c0 * forward + c1 * right;
	plane[Camera::S_RIGHT].m_normal = normal;
	plane[Camera::S_RIGHT].m_disOrigin = -cml::dot(position, normal);

	// Das planes!! 
	return plane;
}

bool CCuller::isVisible(const plane_list& planes, const Sphere3f& volume)
{
	if (IsZero(volume.m_radius))
	{
		return false;
	}

	for (size_t i = Camera::NUM_PLANES-1; i >= 0; --i)
	{
		const Plane3f& plane = planes[i];

		// Compute the distance of the sphere center to the plane.
		// Formula (p.n + d + radius).
		float dist = cml::planeDotCoord(plane, volume.m_center) + volume.m_radius;
		if (dist <= 0)
		{
			// The object is on the negative side of the plane, so
			// cull it.
			return false;
		} 
		else
		{
			// The object is on the positive side of plane. There is
			// no need to compare subobjects against this plane, so
			// mark it as inactive.	
			return true;
		}
	}
}