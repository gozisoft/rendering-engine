#include "libframeworkafx.h"
#include "OSTimer.h"

#if !defined(WINAPI)
#  ifndef WIN32_LEAN_AND_MEAN
#    define WIN32_LEAN_AND_MEAN 1
#  endif
#include <windows.h>
#  undef WIN32_LEAN_AND_MEAN
#endif

_ENGINE_BEGIN

namespace OS
{


static BOOL multicore = FALSE;
static BOOL highPerformanceTimerSupport = FALSE;

double Timer::m_secondsPerCount = 0;
double Timer::m_deltaTime = -1.0;
int64_t Timer::m_baseTime = 0;
int64_t Timer::m_pauseTime = 0;
int64_t Timer::m_stopTime = 0;
int64_t Timer::m_prevTime = 0;
int64_t Timer::m_currTime = 0;
bool Timer::m_stopped = false;

void Timer::InitTimer()
{
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);
	multicore = (sysinfo.dwNumberOfProcessors > 1);

	LARGE_INTEGER HighPerformanceFreq;
	highPerformanceTimerSupport = QueryPerformanceFrequency(&HighPerformanceFreq);
	m_secondsPerCount = 1.0 / static_cast<double>(HighPerformanceFreq.QuadPart);
}

// Returns the total time elapsed since reset() was called, NOT counting any
// time when the clock is stopped.
double Timer::GetGameTime()
{
	// If we are stopped, do not count the time that has passed since we stopped.
	//
	// ----*---------------*------------------------------*------> time
	//  m_baseTime       m_stopTime                      m_currTime
	if( m_stopped )
	{
		return static_cast<double>(m_stopTime - m_baseTime);
	}
	// The distance m_currTime - m_baseTime includes paused time,
	// which we do not want to count.  To correct this, we can subtract 
	// the paused time from m_currTime:  
	//
	//  (m_currTime - m_pauseTime) - m_baseTime 
	//
	//                     |<-------d------->|
	// ----*---------------*-----------------*------------*------> time
	//  m_baseTime       m_stopTime        startTime     m_currTime
	else
	{
		return static_cast<double>(((m_currTime-m_pauseTime)-m_baseTime));
	}
}


double Timer::GetDeltaTime()
{
	return m_deltaTime;
}

double Timer::GetTime()
{
	int64_t currTime = GetRealTime();
    double appTime = static_cast<double>(currTime - m_baseTime);
    return appTime;
}

void Timer::Reset()
{
	int64_t currTime = GetRealTime();

	m_baseTime = currTime;
	m_prevTime = currTime;
	m_stopTime = 0;
	m_stopped  = false;
}

void Timer::Start()
{
	int64_t startTime = GetRealTime();
	// Accumulate the time elapsed between stop and start pairs.
	//
	//                     |<-------d------->|
	// ----*---------------*-----------------*------------> time
	//  m_baseTime       m_stopTime        startTime     
	if (m_stopped)
	{
		m_pauseTime += (startTime - m_stopTime);	
	}
	m_prevTime = startTime;
	m_stopTime = 0;
	m_stopped  = false;	
}

void Timer::Stop()
{
	if( !m_stopped )
	{
		int64_t currTime = GetRealTime();

		m_stopTime = currTime;
		m_prevTime = currTime;
		m_stopped  = true;
	}
}

void Timer::Update()
{
	if( m_stopped )
	{
		m_deltaTime = 0.0;
		return;
	}

	m_currTime = GetRealTime();

	// Time difference between this frame and the previous.
	m_deltaTime = static_cast<double>(m_currTime - m_prevTime);

	// Prepare for next frame.
	m_prevTime = m_currTime;

	// Force nonnegative.  The DXSDK's CDXUTTimer mentions that if the 
	// processor goes into a power save mode or we get shuffled to another
	// processor, then m_deltaTime can be negative.
	if(m_deltaTime < 0.0)
	{
		m_deltaTime = 0.0;
	}
}

int64_t Timer::GetRealTime()
{
	if (highPerformanceTimerSupport)
	{
		LARGE_INTEGER nTime = { 0 };
		BOOL queriedOK = FALSE;

		// Avoid potential timing inaccuracies across multiple cores by
		// temporarily setting the affinity of this process to one core.	
		if (multicore)
		{
			// Get the current process
			HANDLE hCurrentProcess = ::GetCurrentProcess();

			// Get the processor affinity mask for this process
			DWORD_PTR dwProcessAffinityMask = 0;
			DWORD_PTR dwSystemAffinityMask = 0;

			if ( GetProcessAffinityMask(hCurrentProcess, &dwProcessAffinityMask, &dwSystemAffinityMask) != 0 &&
				dwProcessAffinityMask )
			{
				// Find the lowest processor that our process is allowed to run against
				DWORD_PTR dwAffinityMask = ( dwProcessAffinityMask & ( ( ~dwProcessAffinityMask ) + 1 ) );

				// Set this as the processor that our thread must always run against
				// This must be a subset of the process affinity mask
				DWORD_PTR previousAffinityMask = 0;
				HANDLE hCurrentThread = ::GetCurrentThread();
				if (INVALID_HANDLE_VALUE != hCurrentThread)
				{
					// Assign the previous affinity mask to set back after QueryPerformanceCounter
					previousAffinityMask = ::SetThreadAffinityMask(hCurrentThread, dwAffinityMask);	
				
					// Query the timer function
					queriedOK = ::QueryPerformanceCounter(&nTime);

					// Restore the true affinity.
					::SetThreadAffinityMask(hCurrentThread, previousAffinityMask);

					// Can now close the current thread
					::CloseHandle(hCurrentThread);
				}
			}

			// Close current process
			::CloseHandle(hCurrentProcess);
		}
		else
		{
			// No multicore support, so can skip the above if statement
			queriedOK = ::QueryPerformanceCounter(&nTime);
		}

		if (queriedOK)
			return static_cast<int64_t>( (nTime.QuadPart * 1000) * m_secondsPerCount ); // convert to miliseconds
	}

	// Support vista and above only
	return static_cast<int64_t>( ::GetTickCount64() );
}

//TimerData* WaitableTimer::InitTimer()
//{
//	LARGE_INTEGER HighPerformanceFreq;
//	highPerformanceTimerSupport = ::QueryPerformanceFrequency(&HighPerformanceFreq);
//	m_secondsPerCount = 1.0 / static_cast<double>(HighPerformanceFreq.QuadPart);
//
//	TimerData* pTimerData = new TimerData;
//	pTimerData->timerHandle = static_cast<PTHANDLE>( CreateWaitableTimer( NULL, TRUE, NULL ) );
//	pTimerData->intervalMS = 0;
//	pTimerData->previousMS = 0;
//
//	return pTimerData;
//}
//
//void WaitableTimer::DisableTimer(TimerData* handle)
//{
//	// Verify handle
//	assert(handle != NULL);
//
//	BOOL result = ::CloseHandle(handle);
//	if (result == FALSE)
//	{
//		assert( false && ieS("handle has already been closed [WaitableTimer::DisableTimer]") );
//	}
//}
//
//void WaitableTimer::Start(TimerData* handle)
//{
//	// Verify handle
//	assert(handle != NULL);
//
//	// Conver the interval from miliseconds to nano seconds
//	// 1 millisecond = 1 000 000 nanoseconds
//	LARGE_INTEGER interval;
//	interval.QuadPart = static_cast<ULONGLONG>(handle->intervalMS * -10000000.0);
//
//	// Obtain the previous tick and convert to nano seconds
//	LARGE_INTEGER previousTick;
//	::QueryPerformanceCounter(&previousTick);
//	handle->previousMS = static_cast<int64_t>( (previousTick.QuadPart * 1000) * m_secondsPerCount );
//
//    // Start the timer after X seconds.
//	::SetWaitableTimer((HANDLE)handle->timerHandle, &interval, 0, NULL, NULL, 0);
//}
//
//void WaitableTimer::Stop(TimerData* handle)
//{
//	// Verify handle
//	assert(handle != NULL);
//
//
//}
//
//int64_t WaitableTimer::Wait(TimerData* handle, bool wait)
//{
//    //
//    // Wait for the timer to expire, then start it up again.
//    //
//	if (wait)
//	{
//		// Wait for the timer.
//		::WaitForSingleObject( (HANDLE)handle->timerHandle, INFINITE );
//
//		// Conver the interval from miliseconds to nano seconds
//		// 1 millisecond = 1 000 000 nanoseconds
//		LARGE_INTEGER interval;
//		interval.QuadPart = static_cast<LONGLONG>(handle->intervalMS * -10000000.0);
//
//		// Set a timer to wait for X seconds
//		::SetWaitableTimer( (HANDLE)handle->timerHandle, &interval, 0, NULL, NULL, 0 );
//	}
//
//	//
//	// Calculate the time delta in seconds. We are using the performance counter which varies by system.
//	// The performance frequency is the number of performance counts per second.
//	//
//	::LARGE_INTEGER CurrentTick;
//	::QueryPerformanceCounter(&CurrentTick);
//
//	int64_t currentMS = static_cast<int64_t>( (CurrentTick.QuadPart * 1000) * m_secondsPerCount );
//	int64_t deltaTime = currentMS - handle->previousMS;
//
//	assert( deltaTime >= handle->intervalMS );
//	handle->previousMS = currentMS;
//
//	return deltaTime;
//}



} // namespace OS

_ENGINE_END