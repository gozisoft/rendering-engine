#include "libframeworkafx.h"
#include "EventManager2.h"
#include "IEventData.h"
#include <algorithm>

using namespace Engine;

void CGlobalEvents::Attach(EventFunction function, const IEventData* inTypes, size_t count)
{
	for (size_t i = 0; i < count; ++i)
	{
		EventFunctions& functions = m_delegateMap[ Typeid(inTypes[i]) ];
		auto it = std::find(functions.begin(), functions.end(), function);
		if ( it != functions.end() )
		{
			// Combination already exists
			continue;
		}
		else
		{
			// Push back the new function
			functions.push_back(function);
		}
	}
}

void CGlobalEvents::Detach(EventFunction function, const IEventData* inTypes, size_t count)
{
	for (size_t i = 0; i < count; ++i)
	{
		auto mapiter = m_delegateMap.find( Typeid(inTypes[i]) );
		if ( mapiter != m_delegateMap.end() )
		{
			EventFunctions& functions = mapiter->second;
			auto veciter = std::find(functions.begin(), functions.end(), function);
			if ( veciter != functions.end() )
			{
				functions.erase(veciter);
			}
		}
	}
}