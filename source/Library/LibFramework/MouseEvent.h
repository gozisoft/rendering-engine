#pragma once
#ifndef MOUSE_EVENT_H
#define MOUSE_EVENT_H

#include "EngineEvent.h"

_ENGINE_BEGIN

class MouseEvent: public EngineEvent<MouseEvent>
{
public:
    static const std::string MOUSE_EVENT;

    MouseEvent()
        : EngineEvent(MOUSE_EVENT)
    {

    }
};

_ENGINE_END

#endif