#pragma once
#ifndef __CEVENT_MANAGER_H__
#define __CEVENT_MANAGER_H__

#include "IEventManager.h"
#include "RTTI.h"
#include <unordered_map>
#include <unordered_set>
#include <deque>
#include <vector>
#include <limits>
#include <sigc++/signal.h>


_ENGINE_BEGIN

// Singleton event manager can be used anywhere.
// Even when not used as a singleton the class is non copyable
class CEventManager : IEventManager
{
public:
	// Typedef for the signal
	typedef sigc::signal<void (const IEventDataPtr&)> Signal;
	
	// mapping of event identities to listner list
	typedef std::unordered_multimap<CTypeIndex, Signal> EventDelegateMap;

	// Queue of pending- or processing-events
	// typedef tbb::concurrent_queue<IEventDataPtr> EventQueue;
	typedef std::deque<IEventDataPtr> EventQueue;

	// Typedef for an array of queues
	typedef std::vector<EventQueue> EventQueueArray;

	// Set true to make the event manager a singleton
	CEventManager(CTimer* timer, size_t numQueues = 2);

	// If event manager is a singleton the global instance will
	// be destroyed.
	~CEventManager();

	// This function processes the queued events. If too many events need processed
	// and cannot be procssed within the maxProcessTime limit, then remaining events
	// are added to another queue.
	// #maxProcessTime : is the max time, in milliseconds,  the update function can run for in one loop.
	void Update(size_t maxProcessTime = std::numeric_limits<size_t>::max());
	
    // Registers a delegate function that will get called when the event type is triggered.  Returns true if 
    // successful, false if not.
	view_handle Attach(EventFunction& function, const IEventData* inTypes, size_t count);

	// Removes a delegate / event type pairing from the internal tables.  Returns false if the pairing was not found.
	void Detach(view_handle handle, const CTypeIndex* inTypes, size_t count);

	// Removes a delegate / event type pairing from the internal tables.  Returns false if the pairing was not found.
	// void Detach(connection_t connection, const IEventData& inType);

	// Fire off event NOW.  This bypasses the queue entirely and immediately calls all delegate functions registered 
    // for the event.
	void Trigger(IEventDataPtr inEvent) const;

	// Fire off event.  This uses the queue and will call the delegate function on the next call to VTick(), assuming
    // there's enough time.
	void QueueEvent(IEventDataPtr inEvent);

	// Find the next-available instance of the named event type and remove it from the processing queue.  This 
    // may be done up to the point that it is actively being processed ...  e.g.: is safe to happen during event
	// processing itself.
	//
	// if allOfType is true, then all events of that type are cleared from the input queue.
	//
	// returns true if the event was found and removed, false otherwise
	void AbortEvent(IEventDataPtr inType, AbortEventRule abortRule);

private:
	// Disconnect functionor
	class disconnector
	{
	public:
		void operator () (VIEWHANDLE* handle)
		{
			connection_array connections = *(connection_array*)handle;
			connections.clear();
		}
	};

	// Mappping of event types to listners
	EventDelegateMap m_delegateMap;

	// Double buffered event processing queue
	EventQueueArray m_queues;

	// Which queue is activly processing
	size_t m_activeQueue;

	// Timer class is used for constricting time of update
	CTimer* m_timer;
};



_ENGINE_END

#endif




//namespace tbb
//{
//	// Template class for hash compare
//	template<>
//	class tbb_hash<std::type_index>
//	{
//	public:
//		typedef std::type_index type;
//
//		size_t operator()(type keyval) const
//		{
//			return keyval.hash_code();
//		}
//	};
//}
