#pragma once
#ifndef CAMERA_H
#define CAMERA_H

#include "Vector.h"

_ENGINE_BEGIN

// matrices (view), (proj), (ortho) :: (left handed coords)
// from http://www.codeguru.com/cpp/misc/misc/math/article.php/c10123__3/Deriving-Projection-Matrices.htm
class Camera
{
public:
    // Access the projection matrices of the camera.  The projection matrix
    // that maps to depths [0,1] is what Direct3D uses.  The view matrix that
    // maps to depths [-1,1] is what OpenGL uses.
    enum DepthType
    {
        DT_ZERO_TO_ONE,       // [0,1]
        DT_MINUS_ONE_TO_ONE,  // [-1,1]
        NUM_DEPTH_TYPES
    };

    enum Side
    {
        S_NEAR,
        S_FAR,
        S_BOTTOM,
        S_TOP,
        S_LEFT,
        S_RIGHT,
        NUM_PLANES
    };

    Camera(bool isPerspective = true);
    ~Camera();

    // This function is like using D3DXMatrixLookAtLH
    void SetCamera(const Point3f &pos, const Vector3f &lookat,
                   const Vector3f &yaxis = cml::axis_3D(1));

    // The camera frame is always in world coordinates.
    //   default position  P = (0, 0,  0; 1)
    //   default direction D = (0, 0, -1; 0)
    //   default up        U = (0, 1,  0; 0)
    //   default right     R = (1, 0,  0; 0)
    void SetFrame(const Point3f &pos, const Vector3f &upVector,
                  const Vector3f &rightVector, const Vector3f &lookVector);

    void SetPosition(const Point3f &pos);

    void SetAxis(const Vector3f &xaxis, const Vector3f &yaxis,
                 const Vector3f &zaxis);

    // Camera Coordinate Vectors
    const Point3f &position() const;
    const Vector3f &up() const; // up vector
    const Vector3f &right() const; // right vector
    const Vector3f &forward() const; // forward(lookat) vector

    // Same as gluPerspective
    void SetFrustum(float upFovDegrees, float aspectRatio, float zNear,
                    float zFar);

    // Get the view frustum.
    const float *GetFrustum() const;

    // This will return [0,1], [-1,1]
    DepthType GetDepthType() const;

    // Calculate a view frustum
    std::vector<Plane3f> buildFrustum() const;

    // The frustum values are N (near), F (far), B (bottom), T (top),
    // L (left), and R (right).  The various matrices are as follows.
    //
    // perspective, depth [0,1]
    //   +-                                               -+
    //   | 2*N/(R-L)  0           -(R+L)/(R-L)  0          |
    //   | 0          2*N/(T-B)   -(T+B)/(T-B)  0          |
    //   | 0          0           F/(F-N)       -N*F/(F-N) |
    //   | 0          0           1             0          |
    //   +-                                               -+
    //
    // perspective, depth [-1,1]
    //   +-                                                 -+
    //   | 2*N/(R-L)  0           -(R+L)/(R-L)  0            |
    //   | 0          2*N/(T-B)   -(T+B)/(T-B)  0            |
    //   | 0          0           (F+N)/(F-N)   -2*F*N/(F-N) |
    //   | 0          0           1             0
    //   +-                                                 -+
    //
    // orthographic, depth [0,1]
    //   +-                                       -+
    //   | 2/(R-L)  0  0              -(R+L)/(R-L) |
    //   | 0        2/(T-B)  0        -(T+B)/(T-B) |
    //   | 0        0        1/(F-N)  -N/(F-N)  0  |
    //   | 0        0        0        1            |
    //   +-                                       -+
    //
    // orthographic, depth [-1,1]
    //   +-                                       -+
    //   | 2/(R-L)  0        0        -(R+L)/(R-L) |
    //   | 0        2/(T-B)  0        -(T+B)/(T-B) |
    //   | 0        0        2/(F-N)  -(F+N)/(F-N) |
    //   | 0        0        0        1            |
    //   +-                                       -+
    //
    // The projection matrix multiplies vectors on its right, projMat*vector4.

    // The returned matrix depends on the values of msDepthType and
    // mIsPerspective.

    /// <summary>
    /// Get the view frustum as a raw C array.
    /// </summary>
    const float *frustrum_c() const;

    /// <summary>
    /// Get the view frustum as C++ vector.
    /// </summary>
    std::array<float, NUM_PLANES> frustrum() const;

    /// <summary>
    /// Return the view matrix of this particualr camera
    /// </summary>
    const Matrix4f &viewMatrix() const;

    /// <summary>
    /// Return the projection matrix.
    /// </summary>
    const Matrix4f &projectionMatrix() const;

    /// <summary>
    /// combination of proj*view
    /// </summary>
    const Matrix4f &pProjViewMatrix() const;

private:
    /// <summary>
    /// Called when a camera property relating to the view components
    /// changes.
    /// </summary>
    void onFrameChange();

    /// <summary>
    /// Called when a camera property relating to the view frustum changes..
    /// </summary>
    void onFrustrumChange();

    /// <summary>
    /// The product of the projection and view matrix.  This includes the
    /// post-projection and/or pre-view whenever those are not the identity
    /// matrix.
    /// </summary>
    void UpdatePVMatrix();

    // Frustrum ratios for top, right, bottom, left, near and far planes.
    std::array<float, NUM_PLANES> m_frustum;

    // Cameras look coordinates
    Vector3f m_yaxis;        // up
    Vector3f m_xaxis;        // right
    Vector3f m_zaxis;        // look

    // camera look at vector
    Vector3f m_lookat;

    // Cameras position
    Point3f m_pos;

    // Matrices
    Matrix4f m_viewMatrix; // V matrix
    Matrix4f m_projectionMatrix; // P matrix
    Matrix4f m_projectionViewMatrix; // V * P Matrix

    // Selected depth type
    DepthType m_depthType;

    // If this is true, then the projection matrix is a perspective matrix.
    // Otherwise the matrix is othographic.
    bool m_isPerspective;

    static DepthType m_defaultDepthType;
};

_ENGINE_END

#include "Camera.inl"

#endif