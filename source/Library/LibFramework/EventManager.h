#pragma once
#ifndef __CEVENT_MANAGER_H__
#define __CEVENT_MANAGER_H__

#include "IEventManager.h"
#include "RTTI.h"
#include <boost\signals.hpp>
#include <unordered_map>
#include <unordered_set>
#include <deque>
#include <vector>
#include <limits>

_ENGINE_BEGIN

// Singleton event manager can be used anywhere.
// Even when not used as a singleton the class is non copyable
class CEventManager : public IEventManager
{
public:
	// Typedef for the signal
	// typedef void (*EventFunction)(const IEventDataPtr&);
	typedef boost::signal<void (const IEventDataPtr&)> signal_t;
	typedef std::shared_ptr<signal_t> EventSignal;

	// A connection type
	typedef boost::signals::connection Connection_t;

	// A typdef for type info object reference wrapper, reason for this
	// is because you cannot have an STL container of stack based type info
	// objects. (No empty args constructor).
	typedef std::reference_wrapper<CTypeIndex> CTypeIndexRef;

	// Typedef for connection typeinfo pair
	typedef std::pair<Connection_t, CTypeIndexRef> Connection_Type_Pair;

	// An Array of connections
	typedef std::vector<Connection_Type_Pair> PairArray;

	// mapping of event identities to listner list
	typedef std::unordered_multimap<CTypeIndex, EventSignal> EventDelegateMap;

	// Queue of pending- or processing-events
	// typedef tbb::concurrent_queue<IEventDataPtr> EventQueue;
	typedef std::deque<IEventDataPtr> EventQueue;

	// Typedef for an array of queues
	typedef std::vector<EventQueue> EventQueueArray;

	// Set true to make the event manager a singleton
	CEventManager(Timer* timer, size_t numQueues = 2);

	// If event manager is a singleton the global instance will
	// be destroyed.
	~CEventManager();

	// This function processes the queued events. If too many events need processed
	// and cannot be procssed within the maxProcessTime limit, then remaining events
	// are added to another queue.
	// #maxProcessTime : is the max time, in milliseconds,  the update function can run for in one loop.
	void Update(size_t maxProcessTime = std::numeric_limits<size_t>::max());
	
	// Registers a delegate function that will get called when the event type is triggered.  Returns true if 
    // successful, false if not.
	view_handle Attach(EventFunction& function, const IEventData& inType);

    // Registers a delegate function that will get called when the event type is triggered.  Returns true if 
    // successful, false if not.
	view_handle Attach(EventFunction& function, const IEventData* inTypes, size_t count);

	// Removes a delegate / event type pairing from the internal tables.  Returns false if the pairing was not found.
	void Detach(view_handle handle, const IEventData* inTypes, size_t count);

	// Removes a delegate / event type pairing from the internal tables.  Returns false if the pairing was not found.
	// void Detach(connection_t connection, const IEventData& inType);

	// Fire off event NOW.  This bypasses the queue entirely and immediately calls all delegate functions registered 
    // for the event.
	void Trigger(IEventDataPtr inEvent) const;

	// Fire off event.  This uses the queue and will call the delegate function on the next call to VTick(), assuming
    // there's enough time.
	void QueueEvent(IEventDataPtr inEvent);

	// Find the next-available instance of the named event type and remove it from the processing queue.  This 
    // may be done up to the point that it is actively being processed ...  e.g.: is safe to happen during event
	// processing itself.
	//
	// if allOfType is true, then all events of that type are cleared from the input queue.
	//
	// returns true if the event was found and removed, false otherwise
	void AbortEvent(IEventDataPtr inType, AbortEventRule abortRule);

private:

	// A proxy class that abstracts a connection to a view handle
	class ConnectionProxy
	{
	public:
		// Constructor
		ConnectionProxy(Connection_t connection) :
			m_connection(connection) {}

		// Destructor
		~ConnectionProxy() 
		{
			m_connection.disconnect();
		}

		Connection_t GetConnection() 
		{ 
			return m_connection;
		}

	private:
		Connection_t m_connection;
	};



	// Mappping of event types to listners
	EventDelegateMap m_delegateMap;

	// Double buffered event processing queue
	EventQueueArray m_queues;

	// Which queue is activly processing
	size_t m_activeQueue;

	// Timer class is used for constricting time of update
	Timer* m_timer;
};



_ENGINE_END

#endif



//// Disconnect functionor for smart pointer based handle
//class Disconnector
//{
//public:
//	void operator () (PVIEWHANDLE handle)
//	{
//		ConnectionProxy* pCProxy = reinterpret_cast<ConnectionProxy*>(handle);
//		pCProxy->GetConnection.disconnect();
//	}
//};


//namespace tbb
//{
//	// Template class for hash compare
//	template<>
//	class tbb_hash<std::type_index>
//	{
//	public:
//		typedef std::type_index type;
//
//		size_t operator()(type keyval) const
//		{
//			return keyval.hash_code();
//		}
//	};
//}
