#pragma once
#ifndef __IEVENT_SUBJECT_H__
#define __IEVENT_SUBJECT_H__

#include "RTTI.h"

_ENGINE_BEGIN

class ISubject
{
public:
	// Use's RTTI for type iding
	RTTI_DECL;

	// Attaches an observer to this subject
	virtual void Attach(IObserver* observer, const IEventData* eventData) = 0;

	// Detachs an observer to this subject
	virtual void Detach(IObserver* observer) = 0;
};

_ENGINE_END

#endif