#pragma once

#ifndef CCAMERA_NODE_INL
#define CCAMERA_NODE_INL

inline void CameraNode::setCamera(const Camera& camera)
{
	m_camera = camera;
	onCameraUpdate(m_camera);
}

inline Camera& CameraNode::getCamera()
{
	return m_camera;
}

#endif