#ifndef CAMERA_INL
#define CAMERA_INL

_ENGINE_BEGIN

inline const Point3f & Camera::position() const
{
    return m_pos;
}

inline const Vector3f& Camera::up() const
{
    return m_yaxis;
}

inline const Vector3f& Camera::right() const
{
    return m_xaxis;
}

inline const Vector3f& Camera::forward() const
{
    return m_zaxis;
}

inline const float* Camera::GetFrustum() const
{
    return &m_frustum[0];
}

inline Camera::DepthType Camera::GetDepthType() const
{
    return m_depthType;
}

inline const float* Camera::frustrum_c() const
{
    return &m_frustum[0];
}

inline std::array<float, Camera::NUM_PLANES> Camera::frustrum() const
{
    return m_frustum;
}

inline const Matrix4f& Camera::viewMatrix() const
{
    return m_viewMatrix;
}

inline const Matrix4f& Camera::projectionMatrix() const
{
    return m_projectionMatrix;
}

inline const Matrix4f& Camera::pProjViewMatrix() const
{
    return m_projectionViewMatrix;
}

inline void Camera::SetFrame(const Vector3f& pos, const Vector3f& xaxis,
                             const Vector3f& yaxis, const Vector3f& zaxis)
{
    m_pos = pos;
    SetAxis(xaxis, yaxis, zaxis);
}

inline void Camera::SetPosition(const Vector3f& pos)
{
    m_pos = pos;
    onFrameChange();
}


_ENGINE_END

#endif