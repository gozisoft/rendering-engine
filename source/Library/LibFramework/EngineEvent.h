#pragma once
#ifndef ENGINE_EVENT_H
#define ENGINE_EVENT_H

#include "Core.h"
#include <string>
#include <boost/signals2.hpp>


_ENGINE_BEGIN

/**
 * Base class for events for the engine. These are fixed events.
 */
template < class Child >
class EngineEvent
{
public:
	typedef Child child_type;
    typedef EngineEvent<child_type> base_type;
	typedef boost::signals2::signal< void (const base_type&) > signal_type;
	typedef typename signal_type::slot_type slot_type;

	EngineEvent(const std::string& type) : m_type(type) { /**/ }

	virtual ~EngineEvent() { /**/ };

	const std::string& type() const {
		return m_type;
	}

private:
	std::string m_type;
	
};


_ENGINE_END

#endif