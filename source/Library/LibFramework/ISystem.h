#pragma once
#ifndef __ISYSTEM_H__
#define __ISYSTEM_H__

#include "Core.h"

_ENGINE_BEGIN

class ISystem
{
public:
	virtual ~ISystem() { /**/ }

	// Intilialies a system
	virtual void Initialise( ) = 0;

	// Create a 


};

_ENGINE_END

#endif // __ISCENE_H__