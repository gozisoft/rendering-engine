#ifndef __CPROCESS_MANAGER_INL__
#define __CPROCESS_MANAGER_INL__

inline void CProcessManager::CProcessProxy::Init()
{
	m_pProcess->Init();
}

inline void CProcessManager::CProcessProxy::Update(double deltaMS)
{
	m_pProcess->Update(deltaMS);
}

inline void CProcessManager::CProcessProxy::Success()
{
	m_pProcess->Success();
}

inline void CProcessManager::CProcessProxy::Fail()
{
	m_pProcess->Fail();
}

inline void CProcessManager::CProcessProxy::Abort()
{
	m_pProcess->Abort();
}

// Functions for ending the process.
inline void CProcessManager::CProcessProxy::Succeed()
{
	assert(m_state == RUNNING || m_state == PAUSED);
	m_state = SUCCEEDED;
}

inline void CProcessManager::CProcessProxy::Failure()
{
	assert(m_state == RUNNING || m_state == PAUSED);
	m_state = FAILED;
}

// pause
inline void CProcessManager::CProcessProxy::Pause()
{
	if (m_state == RUNNING)
		m_state = PAUSED;
}

inline void CProcessManager::CProcessProxy::UnPause()
{
	if (m_state == PAUSED)
		m_state = RUNNING;
}

// accessors
inline CProcessManager::State CProcessManager::CProcessProxy::GetState() const
{
	return m_state;
}

inline bool CProcessManager::CProcessProxy::IsAlive() const
{
	return (m_state == RUNNING || m_state == PAUSED); 
}

inline bool CProcessManager::CProcessProxy::IsDead() const
{
	return (m_state == SUCCEEDED || m_state == FAILED || m_state == ABORTED);
}

inline bool CProcessManager::CProcessProxy::IsRemoved() const
{
	return (m_state == REMOVED);
}

inline bool CProcessManager::CProcessProxy::IsPaused() const
{
	return (m_state == PAUSED);
}

// Function to set state
inline void CProcessManager::CProcessProxy::SetState(State newState)
{
	m_state = newState;
}

// Equality testing
inline bool operator == (CProcessManager::CProcessProxyPtr& lhs, IProcess* rhs)
{
	return lhs->m_pProcess == rhs;
}


#endif