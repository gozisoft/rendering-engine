#pragma once
#ifndef __CCAMERA_NODE_H__
#define __CCAMERA_NODE_H__

#include "VisualGroup.h"
#include "Camera.h"

_ENGINE_BEGIN

class CameraNode : public VisualGroup
{
public:
	// Constructor and 
	CameraNode();
	CameraNode(const Camera& camera);

	// destructor
	virtual ~CameraNode();

	// Use this method to set the camera as it will also update
	// the position within the world transform of the object.
	void setCamera(const Camera& camera);

	// Access to the actual camera
	Camera& getCamera();

	// Geometric update
	void UpdateWorldTransform() override;

protected:
	void onCameraUpdate(Camera const& camera);

	Camera m_camera;
	Matrix4f m_prevWorld;
};

#include "CameraNode.inl"

_ENGINE_END

#endif