#include "libframeworkafx.h"
#include "IProcessManager.h"
#include "ProcessManager.h"

using namespace engine;

IProcessManager* IProcessManager::Create()
{
	return new CProcessManager();
}