#pragma once
#ifndef __IPROCESS_H__
#define __IPROCESS_H__

#include "libframeworkfwd.h"

_ENGINE_BEGIN

class IProcess
{
public:
	// Virtual destructor
	virtual ~IProcess() { /**/ } 

	// called during the first update; 
	// responsible for setting the initial state (typically RUNNING)
	virtual void Init() = 0;

	// called every frame
	virtual void Update(double deltaMS) = 0;

	// called if the process succeeds (see below)
	virtual void Success() = 0;

	// called if the process fails (see below)
	virtual void Fail() = 0;

	// called if the process is aborted (see below)
	virtual void Abort() = 0;
};

_ENGINE_END

#endif