#pragma once
#ifndef __IEVENT_OBSERVER_H__
#define __IEVENT_OBSERVER_H__

#include "applicationfwd.h"
#include "RTTI.h"

_ENGINE_BEGIN

class IObserver
{
public:
	// Use's RTTI for type iding
	RTTI_DECL;

    // All interfaces must have virtual destructors
    virtual ~IObserver () { /**/ }

	// Lets the ISubject notify the IObserver in changes in registered aspects of interest.
	virtual void EventOccured(ISubject* pSubject, IEventData* eventData) = 0;
};

_ENGINE_END

#endif