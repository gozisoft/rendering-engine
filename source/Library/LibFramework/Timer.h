#pragma once
#ifndef __CTIMER_H__
#define __CTIMER_H__

#include "OSTimer.h"

_ENGINE_BEGIN

class Timer
{
public:
	Timer()
	{
		OS::Timer::InitTimer();
	}

	void start()
	{
		OS::Timer::Start();
	}
	void stop()	
	{
		OS::Timer::Stop();
	}

	void update()
	{
		OS::Timer::Update();
	}

	void reset()
	{
		OS::Timer::Reset();
	}

	static double getDeltaTime()
	{
		return OS::Timer::GetDeltaTime();
	}

	static double getGameTime()
	{
		return OS::Timer::GetGameTime();
	}

	static double getTime()
	{
		return OS::Timer::GetTime();
	}

	static int64_t getRealTime()
	{
		return OS::Timer::GetRealTime();
	}

private:
	static Timer s_instance;

};

//class CWaitableTimer
//{
//public:
//	typedef OS::TimerData TimerData;
//
//	CWaitableTimer()
//	{
//		m_handle = OS::WaitableTimer::InitTimer();
//	}
//
//	~CWaitableTimer()
//	{
//		OS::WaitableTimer::DisableTimer(m_handle);
//	}
//
//	void Start()
//	{
//		OS::WaitableTimer::Start(m_handle);
//	}
//
//	void Stop()	
//	{
//		OS::WaitableTimer::Stop(m_handle);
//	}
//
//	int64_t Wait(bool wait)
//	{
//		return OS::WaitableTimer::Wait(m_handle, wait);
//	}
//
//private:
//	TimerData* m_handle;
//
//};


_ENGINE_END

#endif // __CTIMER_H__