#pragma once
#ifndef __IPROCESS_MANAGER_H__
#define __IPROCESS_MANAGER_H__

#include "libframeworkfwd.h"

_ENGINE_BEGIN

class IProcessManager
{
public:
	virtual ~IProcessManager() { /**/ }

	// Factory creation function
	static IProcessManager* Create();

	/// Updates all attached processes.
	/// <return> Upper 16 bits (uint16_t) contains success count whilst the lower
	/// 16 bits contain the fail count </return>
	virtual uint32_t Update(double deltaMs) = 0;

	/// Attachs a process to the process mgr
	virtual void Attach(IProcess* pProcess) = 0;  

	/// Removes a process from the manager. 
	/// Calls the Abort() function and removes the process.
	virtual void Detach(IProcess* pProcess) = 0;

	/// Removes every process from the manager. 
	/// If immediate == true, it immediately calls the OnAbort()
	/// function and removes all the processes.
	virtual void DetachAll(bool immediate) = 0;

	/// Aborts all processes.  
	/// If immediate == true, it immediately calls each ones OnAbort()
	/// function and destroys all the processes.
	virtual void AbortAll(bool immediate) = 0;

	/// Removes all proces from the manager
	virtual void Clear() = 0;
};


_ENGINE_END

#endif