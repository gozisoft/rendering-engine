#pragma once
#ifndef __IEVENT_MANAGER_H__
#define __IEVENT_MANAGER_H__

#include "libframeworkfwd.h"
// #include <fastdelegate\FastDelegate.h>
#include <functional>

_ENGINE_BEGIN

// Typedef for a std function object
// typedef fastdelegate::FastDelegate<void (const IEventDataPtr&)> EventFunction;
typedef std::function<void (const IEventDataPtr&)> EventFunction;


class IEventManager
{
public:
	enum AbortEventRule
	{
		ALL = 0,
		FIRST,
		LAST
	};

	//// A handle class to hold a reference to an event connetion.
	//class IConnection 
	//{
	//public:
	//	virtual ~IConnection() {};
	//};


	virtual ~IEventManager() { /**/ }

	// Factory function to create the underlying event manager
	static IEventManager* Create(Timer* timer, size_t numQueues);

	// This function processes the queued events. If too many events need processed
	// and cannot be procssed within the maxProcessTime limit, then remaining events
	// are added to another queue.
	// #maxProcessTime : is the max time, in milliseconds,  the update function can run for in one loop.
	virtual void Update(size_t maxProcessTime) = 0;
	
    // Registers a delegate function that will get called when the event type is triggered.  Returns true if 
    // successful, false if not.
	virtual view_handle Attach(EventFunction& function, const IEventData& inType) = 0;

	// Removes a delegate / event type pairing from the internal tables.  Returns false if the pairing was not found.
	virtual void Detach(view_handle connection, const IEventData& inType) = 0;

	// Removes a delegate / event type pairing from the internal tables.  Returns false if the pairing was not found.
	// void Detach(connection_t connection, const IEventData& inType);

	// Fire off event NOW.  This bypasses the queue entirely and immediately calls all delegate functions registered 
    // for the event.
	virtual void Trigger(IEventDataPtr inEvent) const = 0;

	// Fire off event.  This uses the queue and will call the delegate function on the next call to VTick(), assuming
    // there's enough time.
	virtual void QueueEvent(IEventDataPtr inEvent) = 0;

	// Find the next-available instance of the named event type and remove it from the processing queue.  This 
    // may be done up to the point that it is actively being processed ...  e.g.: is safe to happen during event
	// processing itself.
	//
	// if allOfType is true, then all events of that type are cleared from the input queue.
	//
	// returns true if the event was found and removed, false otherwise
	virtual void AbortEvent(IEventDataPtr inType, AbortEventRule abortRule = ALL) = 0;
};


_ENGINE_END

#endif