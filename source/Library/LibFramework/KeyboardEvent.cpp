//
// Created by Riad Gozim on 16/02/2016.
//
#include "libframeworkafx.h"
#include "KeyboardEvent.h"

using namespace engine;

const std::string KeyboardEvent::KEY_UP = "KeyUpEvent";

const std::string KeyboardEvent::KEY_DOWN = "KeyDownEvent";
