#include "libframeworkafx.h"
#include "ProcessManager.h"
#include "DataFunctions.h"
#include <algorithm>
#include <stdexcept>

using namespace engine;

CProcessManager::CProcessProxy::CProcessProxy(IProcess* process) 
	:
m_pProcess(process)	
{
}

CProcessManager::CProcessProxy::~CProcessProxy()
{
}

CProcessManager::CProcessManager()
{

}

CProcessManager::~CProcessManager()
{

}

uint32_t CProcessManager::Update(double deltaMs)
{
	uint16_t successCount = 0;
	uint16_t failCount = 0;

	for (ProcessListIter it = m_processList.begin(); it != m_processList.end(); ++it)
	{
        // Grab the next process
        CProcessProxyPtr pCurrProcess = (*it);

        // Process is uninitialized, so initialize it
        if (pCurrProcess->GetState() == UNINITIALIZED)
			pCurrProcess->Init();

        // Give the process an update tick if it's running
        if (pCurrProcess->GetState() == RUNNING)
			pCurrProcess->Update(deltaMs);

		// Handle dead processes
		if ( pCurrProcess->IsDead() )
		{
			// Run appropriate exit function
			switch ( pCurrProcess->GetState() )
			{
			case SUCCEEDED:
				{
					pCurrProcess->Succeed();
					++successCount;
				}
				break;
			case FAILED:
				{
					pCurrProcess->Fail();
					++failCount;
				} 
				break;
			case ABORTED:
				{
					pCurrProcess->Fail();
					++failCount;				
				} 
				break;
			}

            // remove the process and destroy it
            m_processList.erase(it);
		}
	}

	return CombineHalves(successCount, failCount);
}

void CProcessManager::Attach(IProcess* pProcess)
{
	auto iter = std::find(m_processList.begin(), m_processList.end(), pProcess);
	if (iter != m_processList.end())
	{
		throw std::runtime_error("Can't add duplicate processes [CProcessManager::Attach]");
	}
	else
	{
		CProcessProxyPtr proxy = std::make_shared<CProcessProxy>(pProcess);
		m_processList.push_back(proxy);
	}
}

void CProcessManager::Detach(IProcess* pProcess)
{
	auto iter = std::find(m_processList.begin(), m_processList.end(), pProcess);
	if (iter != m_processList.end())
	{
		(*iter)->Abort();
		m_processList.erase(iter);		
	}
}

void CProcessManager::AbortAll(bool immediate)
{
	for (auto it = m_processList.begin(); it != m_processList.end(); ++it)
	{
		CProcessProxyPtr& proxy = *it;
		proxy->SetState(ABORTED);
		if (immediate)
		{
			proxy->Abort();
		}
	}
}

void CProcessManager::Clear()
{
	m_processList.clear();
}