//
// Created by Riad Gozim on 16/02/2016.
//

#ifndef KEYBOARD_EVENT_H
#define KEYBOARD_EVENT_H

#include "EngineEvent.h"
#include "Keycodes.h"

_ENGINE_BEGIN

// Modeled from
// http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/flash/events/KeyboardEvent.html
class KeyboardEvent: public EngineEvent<KeyboardEvent>
{
public:
    /** EngineEvent type for a key that was released. */
    static const std::string KEY_UP;

    /** EngineEvent type for a key that was pressed. */
    static const std::string KEY_DOWN;

    /** Location on keybaord of key press. */
    enum class KeyLocation
    {
        LEFT,
        RIGHT,
        STANDARD,
        NUM_PAD
    };

    KeyboardEvent(const std::string &type,
                  engine::KeyCode keycode,
                  KeyLocation key_location,
                  uint_t charcode,
                  bool alt_key,
                  bool ctrl_key,
                  bool shift_key)
        : EngineEvent(type),
          m_keycode(keycode),
          m_keyLocation(key_location),
          m_charcode(charcode),
          m_altKey(alt_key),
          m_ctrlKey(ctrl_key),
          m_shiftKey(shift_key),
          m_isDefaultPrevented(false)
    {

    }

    /** The key code of the key. */
    const KeyCode &keyCode() const
    { return m_keycode; }

    /** Indicates the location of the key on the keyboard. This is useful for differentiating
    *  keys that appear more than once on a keyboard. @see Keylocation */
    const KeyLocation &keyLocation() const
    { return m_keyLocation; }

    /** Contains the character code of the key. */
    uint_t charCode()
    { return m_charcode; }

    /** Indicates whether the Alt key is active on Windows or Linux;
    *  indicates whether the Option key is active on Mac OS. */
    uint_t altKey()
    { return m_altKey; }

    /** Indicates whether the Ctrl key is active on Windows or Linux;
    *  indicates whether either the Ctrl or the Command key is active on Mac OS. */
    uint_t ctrlKey()
    { return m_ctrlKey; }

    /** Indicates whether the Shift key modifier is active (true) or inactive (false). */
    uint_t shiftKey()
    { return m_shiftKey; }

private:
    KeyCode m_keycode;
    KeyLocation m_keyLocation;
    uint_t m_charcode;
    bool m_altKey;
    bool m_ctrlKey;
    bool m_shiftKey;;
    bool m_isDefaultPrevented;
};

_ENGINE_END

#endif //KEYBOARD_EVENT_H
