#include "libframeworkafx.h"
#include "Camera.h"

using namespace engine;

//#if defined(USING_DIRECT3D) || defined(_USING_DIRECT3D)
Camera::DepthType Camera::m_defaultDepthType = DT_ZERO_TO_ONE;
//#endif

#if defined(USING_OPENGL) || defined(_USING_OPENGL)
Camera::DepthType Camera::m_defaultDepthType = DT_MINUS_ONE_TO_ONE;
#endif

Camera::Camera(bool isPerspective) :
	m_isPerspective(isPerspective),
	m_depthType(m_defaultDepthType),
	m_viewMatrix(cml::identity_4x4()),
	m_projectionMatrix(cml::identity_4x4()),
	m_projectionViewMatrix(cml::identity_4x4())
{
	// Set the frame of reference for the view matrix.
	SetFrame(
	{ 0.0f, 0.0f, 0.0f },
	{ 1.0f, 0.0f, 0.0f },
	{ 0.0f, 1.0f, 0.0f },
	{ 0.0f, 0.0f, -1.0f }
	);

	SetFrustum(70.0f,   // use a 70-degree vertical field of view
		1.0f,			// specify the aspect ratio of the window
		0.01f,          // specify the nearest Z-distance at which to draw vertices
		100.0f);
}

Camera::~Camera()
{
}

void Camera::SetCamera(const Point3f& pos, const Vector3f& lookat, const Vector3f& yaxis)
{
	// This will calculate the axis from the pos and lookat
	m_zaxis = lookat - pos;
	m_zaxis.normalize();

	m_xaxis = cml::cross(yaxis, m_zaxis);
	m_xaxis.normalize();

	m_yaxis = cml::cross(m_zaxis, m_xaxis);

	// Build the view matrix
	onFrameChange();
}

void Camera::SetAxis(const Vector3f& xaxis, const Vector3f& yaxis,
	const Vector3f& zaxis)
{
	m_xaxis = xaxis;
	m_yaxis = yaxis;
	m_zaxis = zaxis;

	float const epsilon = 0.01f;
	float det = cml::dot(m_zaxis, cml::cross(m_xaxis, m_yaxis));
	if (std::abs(1.0f - det) > epsilon)
	{
		// The input vectors do not appear to form an orthonormal set.  Time
		// to re-normalise. (zxy order)
		std::vector<Vector3f> axis({ m_zaxis, m_xaxis, m_yaxis });
		// cml::orthonormalize(axis);
		m_zaxis = axis[0];
		m_xaxis = axis[1];
		m_yaxis = axis[2];
	}

	onFrameChange();
}

void Camera::SetFrustum(float upFovDegrees, float aspectRatio, float zNear, float zFar)
{
	// http://www.lighthouse3d.com/tutorials/view-frustum-culling/view-frustums-shape/
	// http://www.songho.ca/opengl/gl_transform.html
	// aspectRatio = width / height
	// f = cotangent(fov / 2)
	auto halfAngleRadians = 0.5f * (upFovDegrees * Const<float>::TO_RAD());
	m_frustum[S_TOP] = zNear * std::tan(halfAngleRadians);
	m_frustum[S_RIGHT] = aspectRatio * m_frustum[S_TOP];
	m_frustum[S_BOTTOM] = -m_frustum[S_TOP];
	m_frustum[S_LEFT] = -m_frustum[S_RIGHT];
	m_frustum[S_NEAR] = zNear;
	m_frustum[S_FAR] = zFar;

	onFrustrumChange();
}

// Build the view matrix LH 
void Camera::onFrameChange()
{
	// Reset matrix to identity.
	m_viewMatrix.identity();

	// Update components.
	// http://stackoverflow.com/questions/349050/calculating-a-lookat-matrix
	// So the operation is: Translate to the origin first (move by -eye), 
	// then rotate so that the vector from eye to At lines up with +z.
	cml::matrix_set_basis_vectors(m_viewMatrix, m_xaxis, m_yaxis, m_zaxis);
	cml::matrix_set_translation(m_viewMatrix,
		-cml::dot(m_pos, m_xaxis),
		-cml::dot(m_pos, m_yaxis), 
		-cml::dot(m_pos, m_zaxis));

	/*m_viewMatrix(0, 0) = m_xaxis[0];
	m_viewMatrix(0, 1) = m_xaxis[1];
	m_viewMatrix(0, 2) = m_xaxis[2];
	m_viewMatrix(0, 3) = -cml::dot(m_xaxis, m_pos);

	m_viewMatrix(1, 0) = m_yaxis[0];
	m_viewMatrix(1, 1) = m_yaxis[1];
	m_viewMatrix(1, 2) = m_yaxis[2];
	m_viewMatrix(1, 3) = -cml::dot(m_yaxis, m_pos);

	m_viewMatrix(2, 0) = m_zaxis[0];
	m_viewMatrix(2, 1) = m_zaxis[1];
	m_viewMatrix(2, 2) = m_zaxis[2];
	m_viewMatrix(2, 3) = -cml::dot(m_zaxis, m_pos);

	m_viewMatrix(3, 0) = 0.0f;
	m_viewMatrix(3, 1) = 0.0f;
	m_viewMatrix(3, 2) = 0.0f;
	m_viewMatrix(3, 3) = 1.0f;*/

	UpdatePVMatrix();
}

// Directx http://www.codeguru.com/cpp/misc/misc/math/article.php/c10123__3/
void Camera::onFrustrumChange()
{
	auto zNear = m_frustum[S_NEAR];
	auto zFar = m_frustum[S_FAR];
	auto yBottom = m_frustum[S_BOTTOM];
	auto yTop = m_frustum[S_TOP];
	auto xLeft = m_frustum[S_LEFT];
	auto xRight = m_frustum[S_RIGHT];

	// Build left handed perspective projection matrix.
	cml::matrix_perspective_RH(
		m_projectionMatrix,
		xLeft,
		xRight,
		yBottom,
		yTop,
		zNear, 
		zFar,
		cml::ZClip::z_clip_zero
		);

	/*float reciprocalWidth = 1.0f / (xRight - xLeft);
	float reciprocalHeight = 1.0f / (yTop - yBottom);
	float reciprocalDistance = 1.0f / (zFar - zNear);

	float sumRMinRMaxInvRDiff = (xLeft + xRight) * reciprocalWidth;
	float sumUMinUMaxInvUDiff = (yBottom + yTop) * reciprocalHeight;

	Matrix4f& proj = m_projectionMatrix;
	float* data = proj.data();
	if (m_isPerspective)
	{
		float twoDMinInvRDiff = 2.0f * zNear * reciprocalWidth;
		float twoDMinInvUDiff = 2.0f * zNear * reciprocalHeight;
		float dMaxInvDDiff = zFar * reciprocalDistance;
		float dMinDMaxInvDDiff = zNear * dMaxInvDDiff;

		// Use perspective matrix
		data[0] = twoDMinInvRDiff;
		data[1] = 0.0f;
		data[2] = 0.0f;
		data[3] = 0.0f;

		data[4] = 0.0f;
		data[5] = twoDMinInvUDiff;
		data[6] = 0.0f;
		data[7] = 0.0f;

		data[8] = -sumRMinRMaxInvRDiff;
		data[9] = -sumUMinUMaxInvUDiff;
		data[10] = dMaxInvDDiff;
		data[11] = 1.0f;

		data[12] = 0.0f;
		data[13] = 0.0f;
		data[14] = -dMinDMaxInvDDiff;
		data[15] = 0.0f;
	}*/

	UpdatePVMatrix();
}


void Camera::UpdatePVMatrix()
{
	Matrix4f& projMatrix = m_projectionMatrix;
	Matrix4f& projViewMatrix = m_projectionViewMatrix;

	projViewMatrix = projMatrix * m_viewMatrix;
	/*if (!mPostProjectionIsIdentity)
    {
        projViewMatrix = mPostProjectionMatrix*projViewMatrix;
    }
    if (!mPreViewIsIdentity)
    {
        projViewMatrix = projViewMatrix*mPreViewMatrix;
    }*/
}

std::vector<Plane3f> Camera::buildFrustum() const
{
	// Get the camera coordinate frame.
	Vector3f position = this->position();
	Vector3f forward = this->forward();
	Vector3f up = this->up();
	Vector3f right = this->right();

	// Grab a reference to the camera's frustum bounds.
	std::vector<float> bounds(GetFrustum(), GetFrustum() + Camera::NUM_PLANES);

	// viewing angle
	float dirDotEye = cml::dot(position, forward);

	// Squared width and heights of the near plane.
	float near2 = Sqr(bounds[Camera::S_NEAR]);
	float bottom2 = Sqr(bounds[Camera::S_BOTTOM]);
	float top2 = Sqr(bounds[Camera::S_TOP]);
	float left2 = Sqr(bounds[Camera::S_LEFT]);
	float right2 = Sqr(bounds[Camera::S_RIGHT]);

	// Planes to be returned.
	std::vector<Plane3f> plane(Camera::NUM_PLANES);

	// Update the near plane.
	plane[Camera::S_NEAR].m_normal = forward;
	plane[Camera::S_NEAR].m_disOrigin = dirDotEye + bounds[Camera::S_NEAR];

	// Update the far plane.
	plane[Camera::S_FAR].m_normal = -forward;
	plane[Camera::S_FAR].m_disOrigin = -(dirDotEye + bounds[Camera::S_FAR]);

	// Update the bottom plane
	float invLength = 1 / std::sqrt(near2 + bottom2);
	float c0 = -bounds[Camera::S_BOTTOM] * invLength; // D component
	float c1 = +bounds[Camera::S_NEAR] * invLength; // U component
	Vector3f normal = c0 * forward + c1 * up;
	plane[Camera::S_BOTTOM].m_normal = normal;
	plane[Camera::S_BOTTOM].m_disOrigin = -cml::dot(position, normal);

	// Update the top plane.
	invLength = 1 / std::sqrt(near2 + top2);
	c0 = +bounds[Camera::S_TOP] * invLength; // D component
	c1 = -bounds[Camera::S_NEAR] * invLength; // U component
	normal = c0 * forward + c1 * up;
	plane[Camera::S_TOP].m_normal = normal;
	plane[Camera::S_TOP].m_disOrigin = -cml::dot(position, normal);

	// Update the left plane.
	invLength = 1 / std::sqrt(near2 + left2);
	c0 = -bounds[Camera::S_LEFT] * invLength; // D component
	c1 = +bounds[Camera::S_NEAR] * invLength; // R component
	normal = c0 * forward + c1 * right;
	plane[Camera::S_LEFT].m_normal = normal;
	plane[Camera::S_LEFT].m_disOrigin = -cml::dot(position, normal);

	// Update the right plane.
	invLength = 1 / std::sqrt(near2 + right2);
	c0 = +bounds[Camera::S_RIGHT] * invLength; // D component
	c1 = -bounds[Camera::S_NEAR] * invLength; // R component
	normal = c0 * forward + c1 * right;
	plane[Camera::S_RIGHT].m_normal = normal;
	plane[Camera::S_RIGHT].m_disOrigin = -cml::dot(position, normal);

	// Das planes!! 
	return plane;
}



/*
void Camera::onFrustrumChange()
{
	float zNear = m_frustum[S_NEAR];
	float zFar = m_frustum[S_FAR];
	float yBottom = m_frustum[S_BOTTOM];
	float yTop = m_frustum[S_TOP];
	float xLeft = m_frustum[S_LEFT];
	float xRight = m_frustum[S_RIGHT];

	float twoNearZ = zNear + zNear;
	float reciprocalWidth = 1.0f / xRight - xLeft;
	float reciprocalHeight = 1.0f/ yTop - yBottom;
	float reciprocalDistance = 1.0f / zFar - zNear;

	float sumLeftRightInvDiff = (xLeft + xRight)*reciprocalWidth;  // (R+L)/(R-L)
	float sumBottomTopInvDiff = (yBottom + yTop)*reciprocalHeight; // (T+B)/(T-B)

	Matrix4f& proj = m_projectionMatrix[m_depthType];

	switch (m_depthType)
	{
	case DT_ZERO_TO_ONE:
		{
			// Direct3d
			float farInvDiff = zFar * reciprocalDistance; // F/(F-N)
			float nearFarInvDiff = zNear*farInvDiff;	// N*F/(F-N)

			proj(0,0) = twoNearZ * reciprocalWidth;
			proj(0,1) = 0.0f;
			proj(0,2) = 0.0f;
			proj(0,3) = 0.0f;

			proj(1,0) = 0.0f;
			proj(1,1) = twoNearZ * reciprocalHeight;
			proj(1,2) = 0.0f;
			proj(1,3) = 0.0f;

			proj(2,0) = -sumLeftRightInvDiff;
			proj(2,1) = -sumBottomTopInvDiff;
			proj(2,2) = farInvDiff;
			proj(2,3) = 1.0f;

			proj(3,0) = 0.0f;
			proj(3,1) = 0.0f;
			proj(3,2) = -nearFarInvDiff;
			proj(3,3) = 0.0f;
		}
		break;

	case DT_MINUS_ONE_TO_ONE:
		{
			// OpenGL
			float farPlusNear = zNear + zFar;
			float farPlusNearInvDiff = farPlusNear * reciprocalDistance; // (F+N)/(F-N)
			float doubleFarPlusNearInvDiff = (2*farPlusNear) * reciprocalDistance; // (2 * far * near) / (far - near)

			proj(0,0) = twoNearZ * reciprocalWidth;
			proj(0,1) = 0.0f;
			proj(0,2) = 0.0f;
			proj(0,3) = 0.0f;

			proj(1,0) = 0.0f;
			proj(1,1) = twoNearZ * reciprocalHeight;
			proj(1,2) = 0.0f;
			proj(1,3) = 0.0f;

			proj(2,0) = -sumLeftRightInvDiff;
			proj(2,1) = -sumBottomTopInvDiff;
			proj(2,2) = farPlusNearInvDiff;
			proj(2,3) = 1.0f;

			proj(3,0) = 0.0f;
			proj(3,1) = 0.0f;
			proj(3,2) = -doubleFarPlusNearInvDiff;
			proj(3,3) = 0.0f;
		}
		break;
	}

	UpdatePVMatrix();
}
*/

