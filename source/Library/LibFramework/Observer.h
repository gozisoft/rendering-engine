#pragma once
#ifndef OBSERVER_H
#define OBSERVER_H

#include <tuple>
#include <utility>
#include <boost/signals2.hpp>

#include "libframeworkfwd.h"

_ENGINE_BEGIN

template < class ObserverTable >
class Observable {
public:
	typedef ObserverTable observer_table;

//	template < class T >
//	struct SlotHelper {
//		typedef boost::signals2::slot<T> slot_type;
//		typedef typename slot_type::signature_type signature_type;
//
//		static auto eval(const slot_type& slot) {
//			return std::get<signature_type>(m_signalsTuple).connect(slot);
//		}
//	};

	template < class T >
	auto addObserver(const boost::signals2::slot<T> & slot) {
		typedef boost::signals2::slot<T> slot_type;
		typedef typename slot_type::signature_type signature_type;
		typedef boost::signals2::signal<signature_type> signal_type;

		return std::get<signal_type>(m_signalsTuple).connect(slot);
	}

	template < class SignalType, class T >
	auto addObserver(T&& slot) {
		return std::get<SignalType>(m_signalsTuple).connect(std::forward<T>(slot));
	}

	/*template < class T >
	auto addObserver(const typename SlotHelper<T>::slot_type& slot) {
	return SlotHelper<T>::eval(slot);
	}*/

	// Registers an observer using tuple index.
	/*template <size_t index, typename F>
	auto addObserver(F&& f) {
		return std::get<index>(m_signalsTuple).connect(std::forward<F>(f));
	}*/

protected:
	Observable() = default;

	// Notifies observers.
	template <size_t ObserverId, typename... Args>
	auto notify(Args&&... args) const {
		return std::get<ObserverId>(m_signalsTuple)(std::forward<Args>(args)...);
	}

	template <typename SignalType, typename... Args>
	auto notify(Args&&... args) const {
		return std::get<SignalType>(m_signalsTuple)(std::forward<Args>(args)...);
	}

	template < class T >
	auto notify(const T& event) {
		typedef typename T::signal_type signal_type;
		return std::get<signal_type>(m_signalsTuple)(event);
	}

	observer_table m_signalsTuple;
};

_ENGINE_END

#endif



// Generic observable mixin - users must derive from it.
//template < typename Observers >
//class Observable {
//public:
//	typedef typename Observers::observer_table observer_table;
//
//	// Registers an observer.
//	template <typename Signature, typename F>
//	auto addObserver(F&& f) {
//		return std::get<Signature>(m_signalsTuple).connect(std::forward<F>(f));
//	}
//	 
//	template < class Signature >
//	auto addObserver(const boost::signals2::slot<Signature>& f) {
//		typedef typename boost::signals2::slot<Signature>::signature_type signature_type;
//		typedef boost::signals2::signal<signature_type> signal_type;
//		return std::get<signal_type>(m_signalsTuple).connect(f);
//	}
//
//	// Registers an observer using tuple index.
//	template <size_t index, typename F>
//	auto addObserver(F&& f) {
//		return std::get<index>(m_signalsTuple).connect(std::forward<F>(f));
//	}
//
//protected:
//	Observable() = default;
//
//	// Notifies observers.
//	template <size_t ObserverId, typename... Args>
//	auto notify(Args&&... args) const {
//		return std::get<ObserverId>(m_signalsTuple)(std::forward<Args>(args)...);
//	}
//
//	template <typename SignalType, typename... Args>
//	auto notify(Args&&... args) const {
//		auto result = std::get<SignalType>(m_signalsTuple);
//		return result(std::forward<Args>(args)...);
//	}
//
//	template <typename SignalType, typename... Args>
//	auto notify(Args&&... args) const {
//		auto result = std::get<SignalType>(m_signalsTuple);
//		return result(std::forward<Args>(args)...);
//	}
//
//private:
//	observer_table m_signalsTuple;
//};