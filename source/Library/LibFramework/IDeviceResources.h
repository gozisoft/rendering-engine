#pragma once

#ifndef IDEVICE_RESOURCES_H
#define IDEVICE_RESOURCES_H

#include "Core.h"

_ENGINE_BEGIN

class IDeviceResources
{
public:
	virtual ~IDeviceResources() { /**/ }

	virtual void resize(long width, long height) = 0;

	virtual void goFullScreen() = 0;

	virtual void goWindowed() = 0;

	virtual void releaseBackBuffer() = 0;

	virtual void configureBackBuffer() = 0;

	virtual shared_ptr<IRenderer> device() const = 0;

	virtual shared_ptr<IPipeline> pipeline() const = 0;

	virtual shared_ptr<IRenderTarget> renderTarget() const = 0;

	virtual shared_ptr<IDepthStencil> depthStencil() const = 0;

};

_ENGINE_END


#endif