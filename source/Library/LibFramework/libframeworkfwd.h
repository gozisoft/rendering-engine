#pragma once
#ifndef __APPLICATION_FORWARD_H__
#define __APPLICATION_FORWARD_H__

#include "Core.h"

_ENGINE_BEGIN

// Base window type
class IWindow;
class IDeviceResources;

class Timer;
class Camera;
class CameraNode;

// EngineEvent/State managment system
class IEventData;

typedef std::shared_ptr<IEventData> IEventDataPtr;

class IEventManager;

typedef std::shared_ptr<IEventManager> IEventManagerPtr;

class CEventManager;

// Process management system
class IProcess;
class IProcessManager;

typedef std::shared_ptr<IProcessManager> IProcessManagerPtr;

// Schedular
class CScheduler;

// Events
// -------------------------------------------------------------------
//template<class Observers>
//class Observable;
//
//template<class Child>
//class EngineEvent;
//
//class SizeEvent;
//class KeyboardEvent;
//class MouseEvent;

// Handle types
typedef struct
{
    int unused;
} VIEWHANDLE; // view handle
typedef VIEWHANDLE *PVIEWHANDLE;

typedef std::shared_ptr<VIEWHANDLE> view_handle;

typedef struct
{
    int unused;
} AINSTANCE; // application instance handle
typedef AINSTANCE *PAINSTANCE;

typedef std::shared_ptr<AINSTANCE> ainstance_handle;

typedef struct
{
    int unused;
} THANDLE; // operating system handle
typedef THANDLE *PTHANDLE;

typedef std::shared_ptr<THANDLE> timer_handle;

_ENGINE_END

#endif