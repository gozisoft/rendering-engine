#include "libframeworkafx.h"
#include "CameraNode.h"

using namespace engine;

CameraNode::CameraNode() : CameraNode(Camera())
{

}

CameraNode::CameraNode(const Camera& camera) :
VisualGroup(),
m_camera(camera)
{
	onCameraUpdate(m_camera);
}

CameraNode::~CameraNode()
{

}

void CameraNode::UpdateWorldTransform()
{
	VisualGroup::UpdateWorldTransform();

	const Transform& world = worldTransform();
	const Vector3f& pos = world.getPosition();
	const Matrix4f& rotation = world.GetRotation();
	
	auto xAxis = cml::matrix_get_x_basis_vector(rotation);
	auto yAxis = cml::matrix_get_y_basis_vector(rotation);
	auto zAxis = cml::matrix_get_z_basis_vector(rotation);

	// The scene graph uses a left handed coordinate system.
	// The notion of an object moving forward, means that the
	// Z value will increase.
	// The camera uses a right handed coordinate system. This means
	// that the frame set from the scene graph object has to convert
	// it's positive Z value to a negative Z value.
	m_camera.SetFrame(pos, xAxis, yAxis, -zAxis);
}

void CameraNode::onCameraUpdate(const Camera& camera) {
	m_worldTransform.setPosition(camera.position());

	Matrix4f rotation(cml::identity_4x4());
	cml::matrix_set_basis_vectors(rotation,
			camera.right(), camera.up(), camera.forward());

	m_worldTransform.SetRotation(rotation);	
}

//bool CameraNode::OnEvent(const IEventData& event)
//{
//	bool absorbed = false;
//
//	if ( !m_animators.empty() )
//	{
//		ISceneControllers::iterator itor = m_animators.begin();
//		for (/**/; itor != m_animators.end(); ++itor)
//		{
//			absorbed = (*itor)->HandleEvent(event);
//		}
//	}
//
//	return absorbed;
//}