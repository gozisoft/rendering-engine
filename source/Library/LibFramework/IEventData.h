#pragma once
#ifndef __IEVENT_DATA_H__
#define __IEVENT_DATA_H__

#include "Core.h"
#include "RTTI.h"
#include <ostream>

_ENGINE_BEGIN

class IEventData
{
public:
	// Use's RTTI for type iding
	RTTI_DECL;

	// Virtual destructor, this is an abstract base class
	virtual ~IEventData() { /**/ }

	// Serialise an event data object into byte code for network
	// packet sending etc.
	virtual void Serialise(std::ostream & os) const = 0;
};

_ENGINE_END

#endif