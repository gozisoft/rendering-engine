#pragma once
#ifndef IWINDOW_H
#define IWINDOW_H

#include "libframeworkfwd.h"
#include "Observer.h"

#include "SizeEvent.h"
#include "KeyboardEvent.h"
#include "MouseEvent.h"
#include "Vector.h"

#include <boost/hana.hpp>

namespace hana = boost::hana;

_ENGINE_BEGIN

    namespace window_observers {
        // Typedef for holding all types of observable events for the window.
        typedef std::tuple<
                SizeEvent::signal_type,
                KeyboardEvent::signal_type,
                MouseEvent::signal_type
        > observer_table;
    }

    class IWindow : public Observable<window_observers::observer_table> {
    public:
        // Pure virtual destructor.
        virtual ~IWindow() {}

        // Obtain the windows coordinates
        virtual Vector4i getWindowCoords() = 0;

        // Access to the rendering device
        virtual shared_ptr<IDeviceResources> getDeviceResources() = 0;
    };

_ENGINE_END

#endif