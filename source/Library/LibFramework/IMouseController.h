#pragma once
#ifndef __IEVENT_DATA_H__
#define __IEVENT_DATA_H__

#include "Core.h"
#include "Vector.h"

_ENGINE_BEGIN

// Abstract interface for a mouse controller.
// This will take mouse data from the OS and translate it into
// data that can be used throughout the framework.
class IMouseController
{
public:
	virtual ~IMouseController() {}

	virtual void SetVisible(bool visible) = 0;

	// Check if cursor is visible
	virtual bool IsVisible() const = 0;

	// Set new pos of cursor
	virtual void SetMousePos(const Point2i& pos) = 0;

	// Sets the relative pos 
	// Value lies between [0.0, 1.0]
	// where (0.0f, 0.0f) is the top left corner and
	// (1.0f, 1.0f) is the bottom right corner of the render window.
	virtual void SetRelativePos(const Point2f& pos) = 0;

	// Get pos of coursor
	// Piexl Pos of the mouse. ie pos based upon entire monitor area
	virtual const Point2i& GetMousePos() = 0;

	// Pos of mouse relative to the size of the active window.
	// Value lies between [0.0, 1.0]
	// where (0.0f, 0.0f) is the top left corner and
	// (1.0f, 1.0f) is the bottom right corner of the render window.
	virtual Point2f GetRelativeMousePos() = 0;

	// retrieve the stored client screen size
	virtual Point2i GetClientSize() const = 0;
};

_ENGINE_END

#endif