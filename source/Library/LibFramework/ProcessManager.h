#pragma once
#ifndef __CPROCESS_MANAGER_H__
#define __CPROCESS_MANAGER_H__

#include "IProcessManager.h"
#include "IProcess.h"
#include <list>
#include <cassert>

_ENGINE_BEGIN

// The process manager is not responsible for deleting process.
// It only controls process behaviour and execution order.
class CProcessManager : public IProcessManager
{
public:
	enum State
	{
		// Processes that are neither dead nor alive
		UNINITIALIZED = 0,  // created but not running
		REMOVED,  // removed from the process list but not destroyed; this can happen when a process that is already running is parented to another process

		// Living processes
		RUNNING,  // initialized and running
		PAUSED,  // initialized but paused

		// Dead processes
		SUCCEEDED,  // completed successfully
		FAILED,  // failed to complete
		ABORTED,  // aborted; may not have started
	};

	enum RemovalPolicy
	{
		ALL = 0,
		FIRST,
		LAST
	};

	class CProcessProxy
	{
	public:
		CProcessProxy(IProcess* process);
		~CProcessProxy();

		// Simple proxy functions call the pointer
		// implementation.
		void Init();
		void Update(double deltaMS);
		void Success();
		void Fail();
		void Abort();

		// Functions for ending the process.
		void Succeed();
		void Failure();

		// pause
		void Pause();
		void UnPause();

		// accessors
		State GetState() const;
		bool IsAlive() const;
		bool IsDead() const;
		bool IsRemoved() const;
		bool IsPaused() const;

		// Function to set state
		void SetState(State newState);

		// Public access to the process this proxy represents
		IProcess* m_pProcess;

		// State maintains current state of process
		State m_state;
	};

	/// List of CProcessProxy's
	typedef std::shared_ptr<CProcessProxy> CProcessProxyPtr;
	typedef std::list<CProcessProxyPtr> ProcessList;
	typedef ProcessList::iterator ProcessListIter;

	/// construction
	CProcessManager();
	~CProcessManager();

	/// Updates all attached processes.
	/// <return> Upper 16 bits (uint16_t) contains success count whilst the lower
	/// 16 bits contain the fail count </return>
	uint32_t Update(double deltaMs);

	/// Attachs a process to the process mgr
	void Attach(IProcess* pProcess);  

	/// Removes a process from the manager. 
	/// Calls the Abort() function and removes the process.
	void Detach(IProcess* pProcess);

	/// Removes every process from the manager. 
	/// If immediate == true, it immediately calls the OnAbort()
	/// function and removes all the processes.
	void DetachAll(bool immediate);

	/// Aborts all processes.  
	/// If immediate == true, it immediately calls each ones OnAbort()
	/// function and destroys all the processes.
	void AbortAll(bool immediate);

	/// Removes all proces from the manager
	void Clear();

private:
	/// The list of processes
	ProcessList m_processList;
};

#include "ProcessManager.inl"

_ENGINE_END

#endif