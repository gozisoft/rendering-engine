#pragma once
#ifndef __OS_TIMER_H__
#define __OS_TIMER_H__

#include "libframeworkfwd.h"

_ENGINE_BEGIN

namespace OS
{
	struct TimerData
	{
		PTHANDLE timerHandle;
		int64_t intervalMS;
		int64_t previousMS;
	};

	class Timer
	{
	public:
		static void InitTimer();
		static void Start();
		static void Stop();
		static void Update();
		static void Reset();
		// static void LimitThreadAffinityToCurrentProc();

		static double GetDeltaTime();
		static double GetGameTime();
		static double GetTime();

		static int64_t GetRealTime();

	private:
		static double m_secondsPerCount;
		static double m_deltaTime;

		static int64_t m_baseTime;
		static int64_t m_pauseTime;
		static int64_t m_stopTime;
		static int64_t m_prevTime;
		static int64_t m_currTime;

		static bool m_stopped;
	};

	//class WaitableTimer
	//{
	//public:
	//	static TimerData*	InitTimer();
	//	static void			DisableTimer(TimerData* handle);
	//	static void			Start(TimerData* handle);
	//	static void			Stop(TimerData* handle);
	//	static int64_t		Wait(TimerData* handle, bool wait);

	//private:
	//	static double m_secondsPerCount;
	//};


}

_ENGINE_END

#endif