#pragma once
#ifndef __CCULLER_H__
#define __CCULLER_H__

#include "Core.h"
#include "libframeworkfwd.h"
#include <functional>

_ENGINE_BEGIN

class CCuller
{
public:
	// Typedef for the collection of items computed as visible.
	typedef std::vector<shared_ptr<Visual>> visible_items;
	typedef std::vector<Plane3f> plane_list;

	// Compute the visible set of nodes.
	static visible_items computeVisible(const plane_list& planes, CRootNodePtr root);

	// Functions for building a set of planes.
	static plane_list calcPlanesStandard(const Camera& camera);
	static plane_list calcPlanesSlow(const Camera& camera);
	static plane_list calcPlanesWorking(const Camera& camera);

private:
	// Tests to see if a volume is on the visible side of the planes.
	static bool isVisible(const plane_list& planes, const Sphere3f& volume);
};



_ENGINE_END

#endif