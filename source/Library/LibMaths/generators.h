/* -*- C++ -*- ------------------------------------------------------------
@@COPYRIGHT@@
*-----------------------------------------------------------------------*/
/** @file
*/

#pragma once

namespace cml {

/** @addtogroup mathlib_matrix_generators */
/*@{*/

/** @defgroup mathlib_matrix_generators_basic Basic Matrix Generators */
/*@{*/

/** Return an NxN identity matrix */
	template < size_t N > inline matrix< double, fixed<N, N>, row_basis, row_major > identity()
	{
		return vector<double, compiled<N>>().zero();
	}


/*@}*/

/*@}*/

} // namespace cml

// -------------------------------------------------------------------------
// vim:ft=cpp:sw=2