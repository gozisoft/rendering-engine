#pragma once

#ifndef __CONST_H__
#define __CONST_H__

#include "libmathsfwd.h"
#include <cmath>

_ENGINE_BEGIN

// A struct of common mathematical constants. Template specialisation is 
// employed to select between float or double precision.
template < typename T >
struct Const
{
	static T PI() { return T(0); }
	static T TWO_PI() { return T(0); }
	static T HALF_PI() { return T(0); }
	static T INV_PI() { return T(0); }

	static T E() { return T(0); }
	static T LN2() { return T(0); }
	static T LN10() { return T(0); }

	static T TO_DEG() { return T(0); }
	static T TO_HALF_DEG() { return T(0); }
	static T TO_RAD() { return T(0); }
	static T TO_HALF_RAD() { return T(0); }
};

template <>
struct Const <float>
{
	typedef float T;

	static T PI ()
	{ 
		return (T)( 4.0*atan(1.0) );
	}

	static T TWO_PI ()
	{ 
		return PI() * 2.0f;
	}

	static T HALF_PI ()
	{ 
		return PI() * 0.5f;
	}

	static T INV_PI ()
	{ 
		return 1.0f / PI();
	}

	static T E ()
	{
		return 2.71828182845904523536f; 
	}

	static T LN2 ()
	{ 
		return 0.693147180559945309417f;
	}

	static T LN10 ()
	{ 
		return 2.30258509299404568402f; 
	}

	static T TO_DEG ()
	{
		return 180.0f / PI(); 
	}

	static T TO_HALF_DEG ()
	{ 
		return ( 180.0f / PI() ) / 2.0f;
	}

	static T TO_RAD ()
	{ 
		return PI() / 180.0f; 
	}

	static T TO_HALF_RAD ()
	{ 
		return ( PI() / 180.0f ) / 2.0f; 
	}
};

template <>
struct Const <double>
{
	typedef double T;

	static T PI ()
	{ 
		return 4.0*atan(1.0);
	}

	static T TWO_PI ()
	{ 
		return PI() * 2.0;
	}

	static T HALF_PI ()
	{ 
		return PI() * 0.5;
	}

	static T INV_PI ()
	{ 
		return 1.0 / PI();
	}

	static T E ()
	{
		return 2.71828182846;
	}

	static T LN2 ()
	{ 
		return 0.693147180560; 
	}

	static T LN10 ()
	{ 
		return 2.30258509299; 
	}

	static T TO_DEG ()
	{
		return 180.0 / PI(); 
	}

	static T TO_HALF_DEG ()
	{ 
		return ( 180.0 / PI() ) / 2.0;
	}

	static T TO_RAD ()
	{ 
		return PI() / 180.0; 
	}

	static T TO_HALF_RAD ()
	{ 
		return (PI() / 180.0) / 2.0; 
	}
};

_ENGINE_END



#endif // CONST_H_


/*
template < typename T >
struct Const
{
	static const T PI;
	static const T TWO_PI;
	static const T HALF_PI;
	static const T INV_PI;

	static const T E;
	static const T LN2;
	static const T LN10;
	static const T EPSILON;
	static const T ROUNDING_ERROR;

	static const T TO_DEG;
	static const T TO_HALF_DEG;
	static const T TO_RAD;
	static const T TO_HALF_RAD;
};
*/