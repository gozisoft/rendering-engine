#ifndef __TRIANGLE_H__
#define __TRIANGLE_H__

#include "Vector.h"

namespace Engine
{

// 2D Triangle class, contains three vertices.
template <typename T>
class Triangle2
{
public:
	// default ctor
	Triangle2();
	Triangle2(const Triangle2 &tri); // copy ctor
	Triangle2(const Vector2<T> &v0, const Vector2<T> &v1, const Vector2<T> &v2);
	Triangle2(const Vector2<T> v[3]);

	Vector2<T> Vec[3];
};

// 3D Triangle class, contains three vertices.
template <typename T>
class Triangle3
{
public:
	// default ctor
	Triangle3();
	Triangle3(const Triangle3 &tri); // copy ctor
	Triangle3(const Vector3<T> &v0, const Vector3<T> &v1, const Vector3<T> &v2);
	Triangle3(const Vector3<T> v[3]);


	Vector3<T> Vec[3];
};



#include "Triangle2.inl"
#include "Triangle3.inl"

}


#endif