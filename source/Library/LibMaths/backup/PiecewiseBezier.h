#ifndef __PIECEWISE_BEZIER_CURVE_H__
#define __PIECEWISE_BEZIER_CURVE_H__

#include "Beziercurve.h"

namespace Engine
{


	template < typename Element >
	class PieceWiseBezier
	{
	public:
		typedef BezierCurve<Element> curve_type;

		typedef Element type; 
		typedef const Element const_type;
		typedef Element* pointer;
		typedef Element& reference;
		typedef const_type* const_pointer;
		typedef const_type& const_reference;

		typedef std::vector<curve_type> curve_array;
		typedef typename curve_array::iterator curveItor;
		typedef typename curve_array::const_iterator constCurveItor;

	public:
		PieceWiseBezier(const curve_array& bezierArray);
		PieceWiseBezier(const curve_type* pArray, size_t size);
		~PieceWiseBezier();

		// Function for adding a single curve via c0 continuity
		// to the piecewise curve.
		void AddSingleCurveC0(const curve_type& bezierCurve);

		// Function for adding a single curve via c1 continuity
		// to the piecewise curve.
		void AddSingleCurveC1(const curve_type& bezierCurve);

		// Function for adding a single curve via c2 continuity
		// to the piecewise curve.
		void AddSingleCurveC2(const curve_type& bezierCurve);

		// Return a curve based on index
		const curve_type& GetCurve(size_t index) const;

		size_t GetNumCurves() const;

	private:
		curve_array m_bezierCurves;

	};

/*
	template < typename Element >
	class PieceWiseBezier
	{
	public:
		typedef shared_ptr< BezierCurve<Element> > curve_type;

		typedef Element type; 
		typedef const Element const_type;
		typedef Element* pointer;
		typedef Element& reference;
		typedef const_type* const_pointer;
		typedef const_type& const_reference;

		typedef std::vector<curve_type> curve_array;

	public:
		PieceWiseBezier(const curve_array& bezierArray);
		PieceWiseBezier(const curve_type* pArray, size_t size);
		~PieceWiseBezier();

		// Function for adding a single curve via c0 continuity
		// to the piecewise curve.
		void AddSingleCurveC0(const curve_type& bezierCurve);

		// Function for adding a single curve via c1 continuity
		// to the piecewise curve.
		void AddSingleCurveC1(const curve_type& bezierCurve);

		// Return a curve based on index
		const curve_type& GetCurve(size_t index) const;

	private:
		curve_array m_bezierCurves;

	};
*/


#include "PiecewiseBezier.inl"

}




#endif