#ifndef __QUATERNION_H__
#define __QUATERNION_H__

#include "MathsFwd.h"
#include "FixedArray.h"
#include "Matrix.h"

_ENGINE_BEGIN

// Multiplaction goes from a left-right order to match matrix
template < typename T >
class Quaternion : public FixedArray<4, T>
{
public:
	typedef FixedArray<4, T> array_type;
	typedef typename array_type::type			 type;
	typedef typename array_type::pointer		 pointer;
	typedef typename array_type::reference		 reference;
	typedef typename array_type::const_pointer   const_pointer;
	typedef typename array_type::const_reference const_reference;

	typedef type Degree, Radian;

public:
	// Default ctor (sets data to zero)
	Quaternion();

	// build quaternion from single elements
	template < typename U > Quaternion(const U& w, const U& x, const U& y, const U& z);

	// build a quaternion from another quaternion
	template < typename U > Quaternion(const Quaternion<U>& quat);

	// build a quaternion from a matrix
	template < typename U > Quaternion(const Matrix4<U>& mat);

	// build a quaternion from axis angle
	template < typename U > Quaternion(const Vector3<U>& Axis, const U& angle); 

	// set equal
	template < typename U > Quaternion& operator = (const Quaternion<U>& quat);

    // Arithmetic updates
    template < typename U > Quaternion& operator += (const Quaternion<U>& quat);
    template < typename U > Quaternion& operator -= (const Quaternion<U>& quat);
    template < typename U > Quaternion& operator *= (const Quaternion<U>& quat);

	// Arithmetic updates :: scalar
    template < typename U > Quaternion& operator *= (const U& scalar);
    template < typename U > Quaternion& operator /= (const U& scalar);

	// Simple set function
	template < typename U > void SetWXYZ(const U& w, const U& x, const U& y, const U& z);

	// Conversion between quaternions, matrices, and axis-angle.
	template < typename U > void FromRotationMatrix(const Matrix4<U>& rotation);
	template < typename U > void ToRotationMatrix(Matrix4<U>& rotation) const;
	template < typename U > void FromAxisAngle(const Vector3<U>& axis, const U& angle);
	template < typename U > void ToAxisAngle(Vector3<U>& axis, U& angle) const;
	template < typename U > void ToHeadPitchRoll(Vector3<U>& rotation) const;

	// unitise Quaternion
	const Quaternion& Normalise();

	// const accessor methods
	const_reference w() const { return (*this)[0]; }
	const_reference x() const { return (*this)[1]; }
	const_reference y() const { return (*this)[2]; }
	const_reference z() const { return (*this)[3]; }

	// accessor methods
	reference w() { return (*this)[0]; }
	reference x() { return (*this)[1]; }
	reference y() { return (*this)[2]; }
	reference z() { return (*this)[3]; }

	// null & one vectors
	static const Quaternion ZERO;
	static const Quaternion ONE;
};

#include "Quaternion.inl"

_ENGINE_END

#endif