#pragma once
#ifndef __LINE_H__
#define __LINE_H__

#include "libmathsfwd.h"
#include "cml/vector.h"

namespace cml
{
	template < typename T, size_t N = 3 >
	class line
	{
	public:
		// Simple type defs
		typedef line< T, N > my_type;
		typedef vector< T, fixed<N> > vector_type;
		//typedef typename vector_type::value_type value_type;
		//typedef typename vector_type::array_type array_type;

		// Default constructor
		line() { /**/ }

		line(const vector_type& origin, const vector_type& direction) 
			:
			m_origin(origin),
			m_direction(direction)
		{

		}

		// Copy constructor and assignment from other type
		line(const line& other)
			:
			m_origin(other.m_origin),
			m_direction(other.m_direction)
		{

		}

		template <typename U, class B> 
		line &operator = (const line& other)
		{
			m_origin = other.m_origin;
			m_direction = other.m_direction;
			return *this;
		}

		vector_type m_origin;
		vector_type m_direction; // unit length direction vector.
	};
} // namespace cml

#endif