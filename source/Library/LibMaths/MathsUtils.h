#pragma once

#ifndef __MATHS_UTILITIES_H__
#define __MATHS_UTILITIES_H__

#include "api_traits.h"
#include <limits>
#include <stdexcept>
#include <cstdlib>
#include <cmath>   // used for math functions tan sin cos

_ENGINE_BEGIN

namespace detail
{
	// test for exact integral less than zero
	template < class T >
	inline bool less_than_zero_impl(
		const T& x,
		typename std::enable_if<std::is_integral<T>::value>::type* dummy = 0
		)  
	{
		UNUSED_PARAMETER(dummy);
		return x < T(0);
	}

	// 32-bit test for approx. less than zero with an absolute or relative tolerance
	template < class T >
	inline bool less_than_zero_impl(
		const T& x,
		typename std::enable_if<is_32bit<T>::value && std::is_floating_point<T>::value>::type* dummy = 0
		) 
	{
		UNUSED_PARAMETER(dummy);
		return ToUInt32(x) > 0x80000000U;
	}

	// 64-bit test for approx. less than zero with an absolute or relative tolerance
	template < class T >
	inline bool less_than_zero_impl(
		const T& x,
		typename std::enable_if<is_64bit<T>::value && std::is_floating_point<T>::value>::type* dummy = 0
		)
	{
		UNUSED_PARAMETER(dummy);
		return ToUInt64(x) > 0x8000000000000000U;
	}

	// test for exact integral less than or equal to zero
	template < class T >
	inline bool less_than_or_equal_zero_impl(
		const T& x,
		typename std::enable_if<std::is_integral<T>::value>::type* dummy = 0
		)  
	{
		UNUSED_PARAMETER(dummy);
		return x <= T(0);
	}

	// 32-bit test for approx. less than or equal to zero with an absolute or relative tolerance
	template < class T >
	inline bool less_than_or_equal_zero_impl(
		const T& x,
		typename std::enable_if<is_32bit<T>::value && std::is_floating_point<T>::value>::type* dummy = 0
		)  
	{
		UNUSED_PARAMETER(dummy);
		return ToInt32(x) <= 0;
	}

	// 64-bit test for approx. less than or equal to zero with an absolute or relative tolerance
	template < class T >
	inline bool less_than_or_equal_zero_impl(
		const T& x,
		typename std::enable_if<is_64bit<T>::value && std::is_floating_point<T>::value>::type* dummy = 0
		)
	{
		UNUSED_PARAMETER(dummy);
		return ToInt64(x) <= 0;
	}

	// test for exact integral equality to zero
	template < class T >
	inline bool is_zero_impl(
		const T& x,
		typename std::enable_if<std::is_integral<T>::value>::type* dummy = 0
		)
	{
		UNUSED_PARAMETER(dummy);
		return x == T(0);
	}

	// 32-bit test for approx. equality to zero with an absolute or relative tolerance
	template < class T >
	inline bool is_zero_impl(
		const T& x,
		typename std::enable_if<is_32bit<T>::value && std::is_floating_point<T>::value>::type* dummy = 0
		) 
	{
		UNUSED_PARAMETER(dummy);
		return ( (ToInt32(x) << 1) == 0 );
	}

	// 64-bit test for approx. equality to zero with an absolute or relative tolerance
	template < class T >
	inline bool is_zero_impl(
		const T& x,
		typename std::enable_if<is_64bit<T>::value && std::is_floating_point<T>::value>::type* dummy = 0
		) 
	{
		UNUSED_PARAMETER(dummy);
		return ( (ToInt64(x) << 1) == 0 );
	}

	// test for EXACT greater than or equal to zero using integral types
	template < class T >
	inline bool greater_than_or_equal_zero_impl(
		const T& x,
		typename std::enable_if<std::is_integral<T>::value>::type* dummy = 0
		) 
	{
		UNUSED_PARAMETER(dummy);
		return x >= T(0);
	}

	// 32-bit test for approx. greater than or equal to zero with an absolute or relative tolerance
	template < class T >
	inline bool greater_than_or_equal_zero_impl(
		const T& x,
		typename std::enable_if<is_32bit<T>::value && std::is_floating_point<T>::value>::type* dummy = 0
		)
	{
		UNUSED_PARAMETER(dummy);
		return ToUInt32(x) <= 0x80000000U;
	}

	// 64-bit test for approx. greater than or equal to zero with an absolute or relative tolerance
	template < class T >
	inline bool greater_than_or_equal_zero_impl(
		const T& x,
		typename std::enable_if<is_64bit<T>::value && std::is_floating_point<T>::value>::type* dummy = 0
		)
	{
		UNUSED_PARAMETER(dummy);
		return ToUInt64(x) <= 0x8000000000000000U;
	}

	// test for EXACT greater than zero using integral types
	template < typename T >
	inline bool greater_than_zero_impl(
		const T& x,
		typename std::enable_if<std::is_integral<T>::value>::type* dummy = 0
		) 
	{
		UNUSED_PARAMETER(dummy);
		return x > T(0);
	}

	// 32-bit test for approx. greater than zero with an absolute or relative tolerance
	template < typename T >
	inline bool greater_than_zero_impl(
		const T& x,
		typename std::enable_if<is_32bit<T>::value && std::is_floating_point<T>::value>::type* dummy = 0
		) 
	{
		UNUSED_PARAMETER(dummy);
		return ToInt32(x) > 0;
	}

	// 64-bit test for approx. greater than zero with an absolute or relative tolerance
	template < typename T >
	inline bool greater_than_zero_impl(
		const T& x,
		typename std::enable_if<is_64bit<T>::value && std::is_floating_point<T>::value>::type* dummy = 0
		) 
	{
		UNUSED_PARAMETER(dummy);
		return ToInt64(x) > 0;
	}

} // namespace detail



//-----------------------------------------------------------------------------------------------------
// Type specific
// Articles
// http://www.gamasutra.com/view/feature/3942/data_alignment_part_1.php?print=1
// http://www.songho.ca/misc/alignment/dataalign.html
//-----------------------------------------------------------------------------------------------------
inline bool IsPowerOf2(uint_t val)
{
	return (val > 0) && ((val & (val - 1)) == 0);
}
//-----------------------------------------------------------------------------------------------------
inline bool IsPowerOf2(int_t val)
{
	return IsPowerOf2( uint_t(val) );
}
//-----------------------------------------------------------------------------------------------------
inline uint32_t Log2OfPowerOfTwo(uint32_t powerOfTwo)
{
	uint32_t log2 = (powerOfTwo & 0xAAAAAAAA) != 0;
	log2 |= ((powerOfTwo & 0xFFFF0000) != 0) << 4;
	log2 |= ((powerOfTwo & 0xFF00FF00) != 0) << 3;
	log2 |= ((powerOfTwo & 0xF0F0F0F0) != 0) << 2;
	log2 |= ((powerOfTwo & 0xCCCCCCCC) != 0) << 1;
	return log2;
}
//-----------------------------------------------------------------------------------------------------
inline int32_t Log2OfPowerOfTwo(int32_t powerOfTwo)
{
	return static_cast<int32_t>( Log2OfPowerOfTwo( uint32_t(powerOfTwo) ) );
}
//-----------------------------------------------------------------------------------------------------
inline uint64_t Log2OfPowerOfTwo(uint64_t powerOfTwo)
{
	UNUSED_PARAMETER(powerOfTwo);
	return 0;
}
//-----------------------------------------------------------------------------------------------------
inline int64_t Log2OfPowerOfTwo(int64_t powerOfTwo)
{
	UNUSED_PARAMETER(powerOfTwo);
	return 0;
}
//-----------------------------------------------------------------------------------------------------
// Templated maths functions
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T Sqr(const T& x)
{
	return x * x;
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T Sqrt (const T& x)
{
#if defined (DEBUG) || defined (_DEBUG) 
		if( x == T(0) )
			throw std::domain_error("Divide by zero [Sqrt]");
#endif
    return sqrt(x);
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T InvSqrt (const T& x)
{
    return T(1) / sqrt(x);
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T Cube(const T& x)
{
	return x * x * x;
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T Abs(const T& x)
{
	return x < T(0) ? -x : x;
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline const T& Min(const T& x, const T& y)
{
	return x < y ? x : y;
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline const T& Min(const T& x, const T& y, const T& z)
{
	return x < y ? Min(x,z) : Min(y,z);
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline const T& Max(const T& x, const T& y)
{
	return x < y ? y : x;
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline const T& Max(const T& x, const T& y, const T& z)
{
	return x < y ? Max(y, z) : Max(x, z);
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T Average(const T& x, const T& y) // Average of two numbers
{
	return (x + y) / T(2);
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline void Swap(T& x, T& y) // exchange the values of two numbers
{
	T temp = x; x = y; y = temp;
}
//-----------------------------------------------------------------------------------------------------
inline void Swap(int& x, int& y) // exchange the values of two integers without temps
{
	x ^= y; y ^= x; x ^= y;
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T Sign(const T& x) // find the sign of a number
{
	return x < T(0) ? T(-1) : (x > T(0) ? T(1) : T(0));
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T Factorial(const T& f) // recursively compute the value of a factorial
{
	return f < T(2) ? T(1) : Factorial(f-1) * f;
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T Random(const T& low, const T& high) // pseudorandom number in the specified range
{
	return std::rand() % high + low;
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T Derivative(T(*f)(T), const T& x, const T& h) // derivative of a function f from first principles
{
	return (f(x + h) - f(x)) / h;
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T ScreenToViewX(T x, T width, T height) // convert x screen coordinate to view space
{
	return (x-width*0.5f) / ((width*0.5f) / (0.41421f * (width / height)));
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline T ScreenToViewY(T y, T width, T height) // convert y screen coordinate to view space
{
	return ((height-y) - height*0.5f) / ((height*0.5f) / 0.41421f);
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline const T& Clamp(const T& x, const T& low, const T& high) // limit a number to the specified range
{
	return Max(low, Min(x, high));
}
//-----------------------------------------------------------------------------------------------------
// Templated float/double comparison functions
//-----------------------------------------------------------------------------------------------------
template < class T >
inline bool LessThanZero(const T& x) 
{
	return detail::less_than_zero_impl<typename std::decay<T>::type>(x);
}

template < class T >
inline bool LessThanOrEqualIsZero(const T& x) 
{
	return detail::less_than_or_equal_zero_impl<typename std::decay<T>::type>(x);
}

template < class T >
inline bool IsZero(const T& x) 
{
	return detail::is_zero_impl<typename std::decay<T>::type>(x);
}

template < class T >
inline bool GreaterThanOrEqualIsZero(const T& x) 
{
	return detail::greater_than_or_equal_zero_impl<typename std::decay<T>::type>(x);
}

template < class T >
inline bool GreaterThanZero(const T& x) 
{
	return detail::greater_than_zero_impl<typename std::decay<T>::type>(x);
}
//-----------------------------------------------------------------------------------------------------
template < typename T >
inline bool IsEqual(const T& x, const T& y, const T& err = std::numeric_limits<T>::epsilon() )
{
	return Abs(x-y) < err;
}
//-----------------------------------------------------------------------------------------------------





//template < typename T, typename U >
//inline bool IsEqual( const T& x, const U& y, const T& tolerance = std::numeric_limits<T>::round_error() ) // test for approx. equality with an absolute or relative tolerance
//{
//	return ( ( x + tolerance ) >= y ) && ( ( x - tolerance ) <= y );
//}

//// test for approx. equality to zero with an absolute or relative tolerance
//template < typename T >
//inline bool IsZero( const T& x, const T& tolerance = Const<T>::ROUNDING_ERROR() )
//{
//	return Abs(x) <= tolerance;
//}



// return a Binomial coefficient from Pascals triangle ( n = degree )
//template < typename T >
//inline const T& BinomialCoefficient(const T& n, const T& r)
//{
//	if ( r > (n - k) ) //take advantage of symmetry
//		k = n - k;
//
//	size_t c = 1;
//	for (size_t r = 0; r <= n; ++r)
//	{
//		m_ppPascalsTrig[n][r] = c;
//		c = c * (n - r) / (r + 1);
//	}
//
//	return c;
//}


_ENGINE_END

#endif // __MATHS_UTILITIES_H__
