#pragma once

#ifndef CML_FWD_H
#define CML_FWD_H

namespace cml
{
	// Forward declare
	template < typename T, size_t N > class line;
	template < typename T, size_t N > class plane;
	template < typename T, size_t N > class sphere;

	// Typedefs for 3d lines
	typedef line<float, 3> line3f;
	typedef line<double, 3> line3d;

	// Typedefs for 4d lines
	typedef line<float, 4> line4f;
	typedef line<double, 4> line4d;

	// Typedefs for 3d planes
	typedef plane<float, 3> plane3f;
	typedef plane<double, 3> plane3d;

	// Typedefs for 4d planes
	typedef plane<float, 4> plane4f;
	typedef plane<double, 4> plane4d;

	// Typedefs for 3d spheres
	typedef sphere<float, 3> sphere3f;
	typedef sphere<double, 3> sphere3d;

	// Typedefs for 4d spheres
	typedef sphere<float, 4> sphere4f;
	typedef sphere<double, 4> sphere4d;
}

#endif