#pragma once
#ifndef __SPHERE_H__
#define __SPHERE_H__

#include "libmathsfwd.h"
#include "cml/vector.h"
#include <utility>

namespace cml
{
	// The sphere is represented as |X-C| = R where C is the center and R is
	// the radius.
	template < typename T, size_t N >
	class sphere
	{
	public:
		// Simple type defs
		typedef sphere< T, N > my_type;
		typedef vector< T, fixed<N> > vector_type;
		typedef typename vector_type::value_type value_type;

		// Default ctor
		sphere() : m_center({ T(0), T(0), T(0) }), m_radius(T(0)) { /**/ }

		template <typename U> 
		sphere (const vector_type& center, U radius)
			:
			m_center(center),
			m_radius(radius)
		{
		}

		sphere(const sphere& other)
			: m_center(other.m_center),
			  m_radius(other.m_radius)
		{
		}

		sphere(sphere&& other)
			: m_center(std::move(other.m_center)),
			  m_radius(std::move(other.m_radius))
		{
		}

		// assignment
		sphere& operator=(const sphere& other)
		{
			if (this == &other)
				return *this;
			m_center = other.m_center;
			m_radius = other.m_radius;
			return *this;
		}

		sphere& operator=(sphere&& other)
		{
			if (this == &other)
				return *this;
			m_center = std::move(other.m_center);
			m_radius = std::move(other.m_radius);
			return *this;
		}

		// Data memebers
		vector_type m_center;
		value_type m_radius;
	};


}


#endif