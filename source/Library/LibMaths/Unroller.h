#pragma once

#ifndef __UNROLLER_H__
#define __UNROLLER_H__

#include "MathsUtils.h"

_ENGINE_BEGIN

//=============================================================================
// Unrolled operations
//=============================================================================
struct AssignOpAssign
{
	template < typename lhs, typename rhs >
	inline static void doOperation(lhs & left, const rhs & right) 
	{ 
		left = right;
	}
};

struct AssignOpAdd
{
	template < typename lhs, typename rhs >
	inline static void doOperation(lhs & left, const rhs & right) 
	{ 
		left += right;
	}
};

struct AssignOpSub
{
	template < typename lhs, typename rhs >
	inline static void doOperation(lhs & left, const rhs & right) 
	{ 
		left -= right;
	}
};

struct AssignOpMul
{
	template < typename lhs, typename rhs >
	inline static void doOperation(lhs & left, const rhs & right) 
	{ 
		left *= right;
	}
};

//=============================================================================
// Unrolled comparison
//=============================================================================
struct CompareOpEqual
{
	template < typename lhs, typename rhs >
	inline static bool doOperation(const lhs & left, const rhs & right, const lhs& tolerence) 
	{ 
		return IsEqual(left, right);
	}
};

struct CompareOpNotEqual
{
	template < typename lhs, typename rhs >
	inline static bool doOperation(const lhs & left, const rhs & right, const lhs& tolerence) 
	{ 
		return !IsEqual(left, right); 
	}
};

struct CompareOpLessEqual
{
	template < typename lhs, typename rhs >
	inline static bool doOperation(const lhs & left, const rhs & right, const lhs& tolerence) 
	{ 
		return (left < right) || IsEqual<lhs>(left, right, tolerence); 
	}
};

struct CompareOpGreaterEqual
{
	template < typename lhs, typename rhs >
	inline static bool doOperation(const lhs & left, const rhs & right, const lhs& tolerence) 
	{ 
		return (left > right) || IsEqual<lhs>(left, right, tolerence);
	}
};

struct CompareOpLessThan
{
	template < typename lhs, typename rhs >
	inline static bool doOperation(const lhs & left, const rhs & right, const lhs& tolerence) 
	{ 
		return (left < right) && !IsEqual<lhs>(left, right, tolerence);
	}
};

struct CompareOpGreaterThan
{
	template < typename lhs, typename rhs >
	inline static bool doOperation(const lhs & left, const rhs & right, const lhs& tolerence) 
	{ 
		return (left > right) && !IsEqual<lhs>(left, right, tolerence);
	}
};

//=============================================================================
// Unrolled zero comparison
//=============================================================================
struct GenericBoolOp
{
	template < typename Type, class Func >
	inline bool operator () (const Type& value)
	{ 
		return Func(value);
	}

	template < typename lhs, typename rhs, typename Func >
	inline bool operator () (const lhs & left, const rhs & right)
	{
		return Func(left, right);
	}
};
//=============================================================================
// The unroller struct for Vector * Vector or other operators
//=============================================================================
template <typename lhs, typename rhs, class _operator>
struct AssignOpLoopUnroller
{
	template < size_t N >
	struct Loop
	{
		enum
        {
            Nxt_I = (N - 1)
        };

		inline static void eval(lhs * LArray, const rhs * RArray)
		{
			_operator::doOperation( LArray[Nxt_I], RArray[Nxt_I] );
			Loop<Nxt_I>::eval(LArray, RArray);
		}
	};

	template <> // specialisation for zero case (loop zero)
	struct Loop<0>
	{
		inline static void eval(lhs * LArray, const rhs * RArray) 
		{ 
			UNUSED_PARAMETER(LArray);
			UNUSED_PARAMETER(RArray);
		}
	};
};

template <typename lhs, typename rhs, class _operator>
struct CompareLoopUnroller
{
	typedef lhs left_type;
	typedef rhs right_type;

	template < size_t N >
	struct Loop
	{
		enum { Nxt_I = (N - 1)
        };

		inline static bool eval(const left_type* LArray, const right_type* RArray, const lhs& tolerence)
		{
			// If an operation is true return the result, else evaluate to the next loop.
			bool result = _operator::doOperation( LArray[Nxt_I], RArray[Nxt_I], tolerence ) 
				? true : Loop<Nxt_I>::eval(LArray, RArray, tolerence);

			return result;
		}
	};

	template <> // specialisation for zero case (loop zero)
	struct Loop<0>
	{
		inline static bool eval(lhs * LArray, rhs * RArray, const lhs& tolerence) 
		{ 
			UNUSED_PARAMETER(LArray);
			UNUSED_PARAMETER(RArray);
			UNUSED_PARAMETER(tolerence);

			return false;
		}
	};
};

struct IsTrueLoopUnroller
{
	template < size_t N >
	struct Loop
	{
		enum { Nxt_I = N - 1 };

		template < typename Type, class Predicate >
		inline static bool eval(Type* LArray, Predicate pred)
		{
			// If an operation is true return the result, else evaluate to the next loop.
			bool result = pred( LArray[Nxt_I] ) ? true : Loop<Nxt_I>::eval(LArray, pred);
	    
			// Keep the compiler happy
			return result;
		}
	};

	template <> // specialisation for zero case (loop zero)
	struct Loop<0>
	{
		template < typename Type, class Predicate >
		inline static bool eval(Type* LArray, Predicate pred) 
		{ 
			UNUSED_PARAMETER(LArray);
			UNUSED_PARAMETER(pred);
			return false;
		}
	};
};



//=============================================================================
// The unroller struct for Vector * scalar or other operators
//=============================================================================
template <typename _lhs, typename _rhs, class _operator>
struct ScalarOpLoopUnroller
{
	template < size_t N >
	struct Loop
	{
		enum { Nxt_I = N - 1 };

		inline static void eval(_lhs * __restrict aLArray, _rhs scalarVal)
		{
			_operator::doOperation( aLArray[Nxt_I], scalarVal );
			Loop<Nxt_I>::eval(aLArray, scalarVal);
		}
	};

	template <> // specialisation for zero case (loop zero)
	struct Loop<0>
	{
		inline static void eval(_lhs * __restrict aLArray, _rhs scalarVal)
		{ 
			UNUSED_PARAMETER(aLArray);
			UNUSED_PARAMETER(scalarVal);
		}
	};
};

//=============================================================================
// The unroller struct for Dot products : Vector Dot Vector
//=============================================================================
template <typename _lhs, typename _rhs>
struct DotProductUnroller
{
	template < size_t N >
	struct Loop
	{
		enum { Nxt_I = N - 1 };
		enum { Nxt_Nxt_I = Nxt_I - 1 };

		inline static _lhs eval(_lhs *LArray, _rhs *RArray)
		{
			return LArray[Nxt_I] * RArray[Nxt_I] + Loop<Nxt_I>::eval( LArray[Nxt_Nxt_I], RArray[Nxt_Nxt_I] );	
		}
	};

	template <> // specialisation for zero case (loop zero)
	struct Loop<0>
	{
		inline static void eval(_lhs *larray, _rhs *rarray)
		{ 
			larray[0] * rarray[0];
		}
	};
};


_ENGINE_END

#endif