#ifndef __BEZIER_CURVE_INL__
#define __BEZIER_CURVE_INL__



#endif












/*

#ifndef __BEZIER_CURVE_INL__
#define __BEZIER_CURVE_INL__


template < typename Element >
BezierCurve<Element>::BezierCurve(const Vector3f<type>* ctrlPoints, size_t numPoints) : 
m_degree(numPoints-1),
m_pCtrlPoints(ctrlPoints, ctrlPoints + numPoints)
{
	assert( m_degree >= 2 && ieS("The degree must be three or larger\n") );

	// build pascals triangle based on num of ctrl points
	m_pascal = Allocate2DArray<Element>( GetNumCtrlPoints(), GetNumCtrlPoints() );

	// m_pascal.resize( GetNumCtrlPoints() );

	for (size_t n = 0; n < GetNumCtrlPoints(); ++n)
	{
		type c = type(1);
		for (size_t r = 0; r <= n; ++r)
		{
			m_pascal[n][r] = c;
			c = c * (n - r) / (r + 1);
		}
	}

	// Store ctrl points for the first derivative of the curve
	m_pDerv1CtrlPoints.resize( GetNumCtrlPoints() - 1 );
	for (size_t i = 0; i < GetNumCtrlPoints() - 1; ++i)
	{
		m_pDerv1CtrlPoints[i] = m_pCtrlPoints[i+1] - m_pCtrlPoints[i];
	}

}

template < typename Element >
BezierCurve<Element>::BezierCurve(const ctrlPointArray& ctrlPoints) : 
m_degree(ctrlPoints.size() - 1),
m_pCtrlPoints(ctrlPoints)
m_pascal( GetNumCtrlPoints(), GetNumCtrlPoints() )
{
	assert( m_degree >= 2 && ieS("The degree must be three or larger\n") );

	// build pascals triangle based on num of ctrl points
	// m_pascal = Allocate2DArray<Element>( GetNumCtrlPoints(), GetNumCtrlPoints() );

	

	for (size_t n = 0; n < GetNumCtrlPoints(); ++n)
	{
		type c = type(1);
		for (size_t r = 0; r <= n; ++r)
		{
			m_pascal[n][r] = c;
			c = c * (n - r) / (r + 1);
		}
	}

	// Store ctrl points for the first derivative of the curve
	m_pDerv1CtrlPoints.resize( GetNumCtrlPoints() - 1 );
	for (size_t i = 0; i < GetNumCtrlPoints() - 1; ++i)
	{
		m_pDerv1CtrlPoints[i] = m_pCtrlPoints[i+1] - m_pCtrlPoints[i];
	}

}

// protected constructor, for use with derived class piecewiseBezier
template < typename Element >
BezierCurve<Element>::BezierCurve() :
m_degree(0)
{

}

template < typename Element >
BezierCurve<Element>::~BezierCurve()
{
	Free2DArray( m_pascal, m_pCtrlPoints.size() );
}

template < typename Element >
const Element& BezierCurve<Element>::GetDegree() const
{
	return m_degree;
}

template < typename Element >
size_t BezierCurve<Element>::GetNumCtrlPoints() const
{
	return m_pCtrlPoints.size();
}

template < typename Element >
const Vector3f<Element>* BezierCurve<Element>::GetControlPoints() const
{
	return &m_pCtrlPoints[0];
}

template < typename Element >
Vector3f<Element>* BezierCurve<Element>::GetControlPoints()
{
	return const_cast< Vector3f<Element>* >( static_cast<const this_type&>(*this).GetControlPoints() );
}

template < typename Element >
Vector3f<Element> BezierCurve<Element>::GetPosition(const_reference t) const
{
	assert( t >= 0 || t <= 1 );

	type oneMinusT = static_cast<type>(1) - t;
	type powT = t;
	Vector3f<Element> result = oneMinusT * m_pCtrlPoints[0]; // r = (1-t)*b0

    for (size_t i = 1; i < m_degree; ++i)
    {
        type coeff = m_pascal[m_degree][i] * powT;
        result = (result + coeff * m_pCtrlPoints[i]) * oneMinusT;
        powT *= t;
    }

    result += powT * m_pCtrlPoints[m_degree]; 

    return result;
}

template < typename Element >
Vector3f<Element> BezierCurve<Element>::GetFirstDerivative(const_reference t) const
{
	assert( t >= 0 || t <= 1 );

	type oneMinusT = static_cast<type>(1) - t;
	type powT = t;
	Vector3f<Element> result = oneMinusT * m_pDerv1CtrlPoints[0]; // r = (1-t)*b0

	const size_t degree1 = m_degree - 1;
    for (size_t i = 1; i < degree1; ++i)
    {
        type coeff = m_pascal[degree1][i] * powT;
		result = (result + coeff * m_pDerv1CtrlPoints[i]) * oneMinusT;
        powT *= t;
    }

    result += powT * m_pDerv1CtrlPoints[m_degree]; 
	result *= m_degree;

    return result;
}



#endif
*/