# Project details
# --------------------
project(maths)

# Include CML Maths headers
# --------------------
find_package(CML)
if (NOT CML_FOUND)
    message(FATAL_ERROR "CML not found: ${result}")
endif ()

# Headers and sources
# --------------------
set(${PROJECT_NAME}_HEADER
        libcmlfwd.h
        libmathsfwd.h
        libmathsheaders.h
        Const.h
        Vector.h
        Matrix.h
        MathsUtils.h
        Intersections.h
        Point.h
        Line.h
        Plane.h
        Sphere.h)


# Create the library as static (SHARED for DLL)
# --------------------
# add_library(${PROJECT_NAME} STATIC ${${PROJECT_NAME}_HEADER})

set(${PROJECT_NAME}_SOURCE_DIRS
        ${CML_INCLUDE_DIRS}
        ${api_SOURCE_DIR}
        ${PROJECT_SOURCE_DIR} PARENT_SCOPE)

include_directories(${${PROJECT_NAME}_SOURCE_DIRS})