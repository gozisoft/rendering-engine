#pragma once

#include "cml/vector.h"
#include <tuple>

namespace cml
{
	// http://stackoverflow.com/q/14261183/272446
	// 
	// Statically iterate over a parameter pack 
	// and call a functor passing each argument.
	struct static_for_each
	{
	private:

		// 
		// Get the parameter pack argument at index i.
		template <size_t i, typename... Args>
		static auto get_arg(Args&&... as)
			-> decltype(std::get<i>(std::forward_as_tuple(std::forward<Args>(as)...)))
		{
			return std::get<i>(std::forward_as_tuple(std::forward<Args>(as)...));
		}

		//
		// Recursive template for iterating over 
		// parameter pack and calling the functor.
		template <size_t Start, size_t End>
		struct internal_static_for
		{
			template <typename Functor, typename... Ts>
			void operator()(Functor f, Ts&&... args)
			{
				f(get_arg<Start>(args...));
				internal_static_for<Start + 1, End>()(f, args...);
			}
		};

		//
		// Specialize the template to end the recursion.
		template <size_t End>
		struct internal_static_for<End, End>
		{
			template <typename Functor, typename... Ts>
			void operator()(Functor f, Ts&&... args) {}
		};

	public:
		// 
		// Publically exposed operator()(). 
		// Handles template recursion over parameter pack.
		// Takes the functor to be executed and a parameter 
		// pack of arguments to pass to the functor, one at a time.
		template<typename Functor, typename... Ts>
		void operator()(Functor f, Ts&&... args)
		{
			// 
			// Statically iterate over parameter
			// pack from the first argument to the
			// last, calling functor f with each 
			// argument in the parameter pack.
			internal_static_for<0u, sizeof...(Ts)>()(f, args...);
		}
	};

	struct ortho_normal_unroller
	{
	private:
		// 
		// Get the parameter pack argument at index i.
		template <size_t i, typename... Args>
		static auto get_arg(Args&&... as)
			-> decltype(std::get<i>(std::forward_as_tuple(std::forward<Args>(as)...)))
		{
			return std::get<i>(std::forward_as_tuple(std::forward<Args>(as)...));
		}

		//
		// Recursive template for iterating over 
		// parameter pack and calling the functor.
		template <size_t Start, size_t End>
		struct internal_static_for
		{
			template <typename Functor, typename... Ts>
			void operator()(Functor f, Ts&&... args)
			{
				f(get_arg<Start>(args...));
				internal_static_for<Start + 1, End>()(f, args...);
			}
		};

		//
		// Specialize the template to end the recursion.
		template <size_t Start>
		struct internal_static_for<Start, Start>
		{
			template <typename Functor, typename... Ts>
			void operator()(Functor f, Ts&&... args)
			{
				auto first = get_arg<Start>(args...);



				auto v0 = get_arg<Start>(args...);
			}
		};

		//
		// Specialize the template to end the recursion.
		template <size_t End>
		struct internal_static_for<End, End>
		{
			template <typename Functor, typename... Ts>
			void operator()(Functor f, Ts&&... args) {}
		};

	public:
		// 
		// Publically exposed operator()(). 
		// Handles template recursion over parameter pack.
		// Takes the functor to be executed and a parameter 
		// pack of arguments to pass to the functor, one at a time.
		template<typename Functor, typename... Ts>
		void operator()(Functor f, Ts&&... args)
		{
			// 
			// Statically iterate over parameter
			// pack from the first argument to the
			// last, calling functor f with each 
			// argument in the parameter pack.
			internal_static_for<0u, sizeof...(Ts)>()(f, args...);
		}
	};

	template <typename T, typename... Args>
	void call(T f, Args... args)
	{
		static_for_each()(f, args...);
	}

	template<class First, class Second>
	auto ortho_calculate(First& u1, Second& v2)
	{
		// Params:
		// u1 == normalised vector 1.
		// v2 == vector 2.
	
		// Gram -Schmidt:
		// u2' = v2 - (u1 dot v2) u1
		auto dot = dot(u1, v2);
		v2 -= u1 * dot;
	
		// u2 = norm(u2')
		return normalize(v2);
	}

	template<class T, class... Args>
	auto orthonormalize(T& first, Args&... args)
	{
		// Normalise the first param
		auto minLength = first.normalize();

		auto calculate = [&] { ortho_calculate(minLength, ) }

		static_for_each()(first, args...);

		// Now do the rest.
		auto length = orthonormalize_depth(first, args...);
		if (length < minLength)
		{
			minLength = length;
		}

		for (auto i = 1; i < numInputs; ++i)
		{
			for (auto j = 0; j < i; ++j)
			{
				auto dot = dot(v[i], v[j]);
				v[i] -= v[j] * dot;
			}
			auto length = normalize(v[i]);
			if (length < minLength)
			{
				minLength = length;
			}
		}

		// Return the min length.
		return orthonormalize(args...);
	}
}