#pragma once
#ifndef __INTERSECTIONS_H__
#define __INTERSECTIONS_H__

#include "Plane.h"
#include "Sphere.h"
#include "MathsUtils.h"
#include <limits>

namespace cml
{

//------------------------------------------------------------------------------------------------------------
// Sphere Collisions
//------------------------------------------------------------------------------------------------------------

// Test if Sphere a intersects Sphere b.
template <typename T, size_t N>
bool TestSphereSphere(const sphere<T,N>& a, const sphere<T,N>& b)
{
	// Typedefs
	typedef typename sphere<T,N>::vector_type vector_type;
	typedef typename sphere<T,N>::value_type value_type;

	// Calculate squared distance between centers
	vector_type d = a.m_center - b.m_center;
	value_type distance = dot(d, d);

	// Spheres intersect if squared distance is less than squared sum of radii
	value_type radiusSum = a.m_radius + b.m_radius;

	return ( distance <= engine::Sqr(radiusSum) );
}

// Computes the bounding sphere out of spheres a and b.
template <typename T, size_t N>
sphere<T,N> MergeWithSphere(const sphere<T,N>& a, const sphere<T,N>& b)
{
	// Typedefs
	typedef typename sphere<T,N>::vector_type vector_type;
	typedef typename sphere<T,N>::value_type value_type;

	// Compute the squared distance between the sphere centers
	vector_type d = b.m_center - a.m_center;
	value_type disSqr = dot(d, d);
	value_type radiusDiff = b.m_radius - a.m_radius;

	// A new sphere to be returned
	sphere<T,N> out;

	// By squaring the difference of the sphere radii, you remove the
	// negative if the shperes overlap. This results in a value greator
	// than the distance between the two spheres
	if ( engine::Sqr<value_type>(radiusDiff) >= disSqr )
	{
		// The sphere with the larger radius encloses the other;
		// just set out to be the larger of the two spheres
		if( engine::GreaterThanOrEqualIsZero<value_type>(radiusDiff) ) // radiusDiff >= 0.0f
		{
			out.m_center = b.m_center;
			out.m_radius = b.m_radius;
		}
	}
	else
	{
		// Spheres partially overlapping or disjoint
		value_type dist = engine::Sqrt<value_type>(disSqr);
		if ( dist > std::numeric_limits<value_type>::epsilon() )
		{
			value_type coeff = (dist + radiusDiff) / (T(2) * dist);
			out.m_center += coeff * d;
		}
		out.m_radius = (dist + a.m_radius + b.m_radius) / T(2);
	}

	// Return the new sphere
	return out;
}

//----------------------------------------------------------------------------------------
// Plane
//----------------------------------------------------------------------------------------

// Check intersection side of plane Returns:
//  > 0 if the point 'pt' lies in front of the plane 'p' (same direction as normal)
//  < 0 if the point 'pt' lies behind the plane 'p'(opposite direction of normal)
//    0 if the point 'pt' lies on the plane 'p'
// The signed distance from the point 'pt' to the plane 'p' is returned.
// http://mathworld.wolfram.com/HessianNormalForm.html
template <typename T, size_t N>
typename plane<T,N>::value_type planeDotCoord(const plane<T,N>& plane, const vector< T, fixed<N> >& point)
{
	return dot(plane.m_normal, point) + plane.m_disOrigin;
}


//----------------------------------------------------------------------------------------
// AABB Collisions
//----------------------------------------------------------------------------------------
/*
// Test if AABB a intersects AABB b
template < typename T, typename U >
inline bool TestAABBAABB(const Aabbox<T>& a, const Aabbox<U>& b)
{
// Exit with no intersection if separated along an axis
if (a.m_max[0] < b.m_min[0] || a.m_min[0] > b.m_max[0]) return false;
if (a.m_max[1] < b.m_min[1] || a.m_min[1] > b.m_max[1]) return false;
if (a.m_max[2] < b.m_min[2] || a.m_min[2] > b.m_max[2]) return false;

// Overlapping on all axes means AABBs are intersecting
return true;
}

// Test if AABB b intersects plane p
template < typename T, typename U >
inline bool TestAABBPlane(const Aabbox<T>& aabb, const Plane<U>& plane)
{
// These two lines not necessary with a (center, extents) AABB representation
Vecto3<T> c = aabb.GetCenter();		// Compute AABB center
Vecto3<T> e = aabb.m_max - c;		// Compute positive extents

// Compute the projection interval radius of b onto L(t) = b.c + t * p.n
T r = Dot( e, Abs(plane.m_normal) );
// Compute distance of box center from plane
T s = Dot( plane.m_normal, c) - p.d;

// Intersection occurs when distance s falls within [-r,+r] interval
return Abs(s) <= r;
}
*/






/*
template < typename T >
inline void ComputeBoundingSphere(Sphere<T>& out, const T* pPosData, size_t numVertices)
{
// Caclulate center of the sphere - average value of all the vertex positions.
out.m_center = Vector3f::ZERO;
for (size_t i = 0; i < numVertices; ++i)
{
out.m_center[0] += pPosData[0];
out.m_center[1] += pPosData[1];
out.m_center[2] += pPosData[2];
}
// Same as [SumOfAllVerts / numVerts]
T invNumVerts = 1.0f / static_cast<T>(numVertices);
out.m_center *= invNumVerts;

// The radius is the largest distance from the center to the positions.
// Eqtn of a circle (x - ca)^2 + (y - cb)^2 + (z - cc)^2 = r^2
// where [ca,cb,cc] are the xyz coords of the center.
out.m_radius = 0.0f;
for (size_t i = 0; i < numVertices; ++i)
{
float difference[3] = 
{
pPosData[0] - out.m_center[0],
pPosData[1] - out.m_center[1],
pPosData[2] - out.m_center[2]
};
float radiusSqrd = Sqr(difference[0]) + Sqr(difference[1]) + Sqr(difference[2]);

// now check if this radius is greator than the last radius
if (radiusSqrd > out.m_radius)
{
out.m_radius = radiusSqrd;
}
}

out.m_radius = sqrt(out.m_radius);
}

//----------------------------------------------------------------------------------------
// Ray Collisions
//----------------------------------------------------------------------------------------
template < typename T >
inline bool TestRaySphere(const Raycast<T>& ray, const Sphere<T>& sphere)
{
Vector3f<T> w(sphere.m_center - ray.m_origin);
T wsq = Dot(w, w);
T proj = Dot(w, direction);
T rsq = sphere.m_radius * sphere.m_radius;

// Early out: if sphere is behind the ray then there's no intersection.
if (proj < T(0) && wsq > rsq)
return false;

T vsq = Dot(direction, direction);

// Test length of difference vs. radius.
return ( Sqr(vsq) - Sqr(proj) <= Sqr(vsq) );
}
*/


} // namespace cml



#endif

//template <typename T, size_t N> template<class AT, typename BO, typename L>
//sphere< T, fixed<N> >
//	TransformSphere(const sphere< T, fixed<N> >& in, const matrix<T,AT,BO,L>& transform)
//{
//	// Typedefs
//	typedef vector< T, fixed<N> > vector_type;
//	typedef T value_type;
//
//	out.m_center = transform_point(transform, in.m_center);
//
//	// build pascals triangle based on num of ctrl points
//	bool topTriangle = true;
//	for (size_t n = 1; n < transform.size(); ++n)
//	{
//		for (size_t r = 0; r <= n; ++r)
//		{
//			topTriangle = Engine::IsZero( transform(n, r) );
//			if (!topTriangle)
//				break;
//		}
//	}
//
//	if (topTriangle)
//	{
//		bool bottomTriangle = true;
//		for (size_t n = 1; n < transform.size(); ++n)
//		{
//			for (size_t r = 0; r <= n; ++r)
//			{
//				bottomTriangle = Engine::IsZero( transform(n, r) );
//				if (!bottomTriangle)
//					break;
//			}
//		}
//
//		if (bottomTriangle)
//			scale = Vector3f( transform(0,0), transform(1,1), transform(2,2) );
//	}
//	else
//	{
//		// We have to do the full calculation.
//		scale = vector_type(
//			std::sqrt( Sqr( transform(0,0) ) +  Sqr( transform(0,1) ) +  Sqr( transform(0,2) ) ),
//			std::sqrt( Sqr( transform(1,0) ) +  Sqr( transform(1,1) ) +  Sqr( transform(1,2) ) ),
//			std::sqrt( Sqr( transform(2,0) ) +  Sqr( transform(2,1) ) +  Sqr( transform(0,2) ) ) 
//			);
//	}
//
//	// Deal with the 0 rotation case first
//	vector_type scale;
//	if ( IsZero( transform(0,1) ) && IsZero( transform(0,2) ) &&
//		IsZero( transform(1,0) ) && IsZero( transform(1,2) ) &&
//		IsZero( transform(2,0) ) && IsZero( transform(2,1) ) )
//	{
//		scale = Vector3f( transform(0,0), transform(1,1), transform(2,2) );
//	}
//	else
//	{
//
//	}
//
//	float maxScale = 0.0f;
//	for (size_t i = 0; i < scale.size(); ++i)
//	{
//		if ( Abs(scale[i]) > maxScale )
//			maxScale = Abs(scale[i]);
//	}
//
//	out.m_radius = maxScale;
//}