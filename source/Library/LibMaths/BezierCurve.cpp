#include "libmathsafx.h"
#include "Beziercurve.h"

using namespace engine;

CBezierCurve::CBezierCurve(const ctrlPointArray& ctrlPoints) : 
m_degree(ctrlPoints.size() - 1),
m_pCtrlPoints(ctrlPoints),
m_pascal( ctrlPoints.size(), ctrlPoints.size() )
{
	assert( m_degree >= 2 && ieS("The degree must be three or larger") );

	// Store ctrl points for the first derivative of the curve
	m_pDerv1CtrlPoints.resize( GetNumCtrlPoints() - 1 );
	for (size_t i = 0; i < GetNumCtrlPoints() - 1; ++i)
	{
		m_pDerv1CtrlPoints[i] = m_pCtrlPoints[i+1] - m_pCtrlPoints[i];
	}

	// build pascals triangle based on num of ctrl points
	for (size_t n = 0; n < GetNumCtrlPoints(); ++n)
	{
		float c = 1.0f;
		for (size_t r = 0; r <= n; ++r)
		{
			m_pascal(n, r) = c;
			c = c * (n - r) / (r + 1);
		}
	}
}

CBezierCurve::~CBezierCurve()
{

}

Vector3f CBezierCurve::GetPosition(float t) const
{
	float oneMinusT = static_cast<float>(1) - t;
	float powT = t;
	Vector3f result = oneMinusT * m_pCtrlPoints[0]; // r = (1-t)*b0

    for (size_t i = 1; i < m_degree; ++i)
    {
        float coeff = m_pascal(m_degree, i) * powT;
        result = (result + coeff * m_pCtrlPoints[i]) * oneMinusT;
        powT *= t;
    }

    result += powT * m_pCtrlPoints[m_degree]; 
    return result;
}

Vector3f CBezierCurve::GetFirstDerivative(float t) const
{
	float oneMinusT = static_cast<float>(1) - t;
	float powT = t;
	Vector3f result = oneMinusT * m_pDerv1CtrlPoints[0]; // r = (1-t)*b0

	const size_t degree1 = m_degree - 1;
    for (size_t i = 1; i < degree1; ++i)
    {
        float coeff = m_pascal(degree1, i) * powT;
		result = (result + coeff * m_pDerv1CtrlPoints[i]) * oneMinusT;
        powT *= t;
    }

    result += powT * m_pDerv1CtrlPoints[degree1]; 
	result *= static_cast<float>(m_degree);
    return result;
}

size_t CBezierCurve::GetDegree() const
{
	return m_degree;
}

const Vector3f* CBezierCurve::GetControlPoints() const
{
	return &m_pCtrlPoints[0];
}

Vector3f* CBezierCurve::GetControlPoints()
{
	return const_cast<Vector3f*>( static_cast<const CBezierCurve&>(*this).GetControlPoints() );
}

size_t CBezierCurve::GetNumCtrlPoints() const
{
	return m_pCtrlPoints.size();
}

// --------------------------------------------------------------------------
// First derivative
// --------------------------------------------------------------------------
const Vector3f* CBezierCurve::GetDerivedPoints() const
{
	return &m_pDerv1CtrlPoints[0];
}

Vector3f* CBezierCurve::GetDerivedPoints()
{
	return const_cast<Vector3f*>( static_cast<const CBezierCurve&>(*this).GetDerivedPoints() );
}

size_t CBezierCurve::GetNumDerCtrlPoints() const
{
	return m_pDerv1CtrlPoints.size();
}
