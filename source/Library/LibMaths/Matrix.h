#pragma once

#ifndef __MATRIX_H__
#define __MATRIX_H__

#include "Core.h"
#include "libmathsfwd.h"
#include "cml/matrix.h"
#include "cml/mathlib/mathlib.h"

_ENGINE_BEGIN

///** Fixed-size, fixed-memory matrix. */
//template<typename Element, int Rows, int Cols, typename BasisOrient, typename Layout>
//class Matrix<Element, cml::fixed<Rows, Cols>, BasisOrient, Layout> 
//	: public cml::matrix<Element, cml::fixed<Rows, Cols>, BasisOrient, Layout>
//{
//public:
//	/* Shorthand for the generator: */
//	typedef cml::fixed<Rows, Cols> generator_type;
//
//	/* Shorthand for the type of this matrix: */
//	typedef cml::matrix<Element, generator_type, BasisOrient, Layout> matrix_type;
//
//	/* For integration into the expression template code: */
//	typedef matrix_type expr_type;
//
//	/* For integration into the expression template code: */
//	typedef matrix_type temporary_type;
//
//	/* Standard: */
//	typedef typename matrix_type::array_type array_type;
//	typedef typename array_type::value_type value_type;
//
//	typedef typename array_type::reference reference;
//	typedef typename array_type::const_reference const_reference;
//
//	typedef matrix_type& expr_reference;
//	typedef const matrix_type& expr_const_reference;
//
//	/* For matching by basis: */
//	typedef BasisOrient basis_orient;
//
//	/* For matching by memory layout: */
//	typedef typename array_type::layout layout;
//
//	/* For matching by storage type if necessary: */
//	typedef typename array_type::memory_tag memory_tag;
//
//	/* For matching by size type if necessary: */
//	typedef typename array_type::size_tag size_tag;
//
//	/* For matching by result type: */
//	typedef typename matrix_type::result_tag result_tag;
//
//	/* For matching by assignability: */
//	typedef typename matrix_type::assignable_tag assignable_tag;
//
//	/* To simplify the matrix transpose operator: */
//	typedef typename matrix_type::transposed_type transposed_type;
//
//	/* To simplify the matrix row and column operators: */
//	typedef typename matrix_type::row_vector_type row_vector_type;
//
//	typedef typename matrix_type::col_vector_type col_vector_type;
//
//	// Accesses
//	value_type& operator[](size_t i)
//	{
//		assert(i < this->length());
//		return this->value[i];
//	}
//
//	value_type const& operator[](size_t i) const
//	{
//		assert(i < this->length());
//		return this->value[i];
//	}
//};
//
///** Resizeable, dynamic-memory matrix. */
//template <typename Element, typename Alloc, typename BasisOrient, typename Layout>
//class Matrix<Element, cml::dynamic<Alloc>, BasisOrient, Layout> 
//	: public cml::matrix<Element, cml::dynamic<Alloc>, BasisOrient, Layout>
//{
//public:
//	/* Shorthand for the generator: */
//	typedef cml::dynamic<Alloc> generator_type;
//
//	/* Shorthand for the type of this matrix: */
//	typedef Matrix<Element, generator_type, BasisOrient, Layout> matrix_type;
//
//	/* For integration into the expression template code: */
//	typedef matrix_type expr_type;
//
//	/* For integration into the expression template code: */
//	typedef matrix_type temporary_type;
//
//	/* Standard: */
//	typedef typename matrix_type::array_type array_type;
//	typedef typename array_type::value_type value_type;
//	typedef typename array_type::reference reference;
//	typedef typename array_type::const_reference const_reference;
//
//	/* For integration into the expression templates code: */
//	typedef matrix_type& expr_reference;
//	typedef const matrix_type& expr_const_reference;
//
//	/* For matching by basis: */
//	typedef BasisOrient basis_orient;
//
//	/* For matching by memory layout: */
//	typedef typename matrix_type::layout layout;
//
//	/* For matching by storage type: */
//	typedef typename matrix_type::memory_tag memory_tag;
//
//	/* For matching by size type if necessary: */
//	typedef typename matrix_type::size_tag size_tag;
//
//	/* For matching by resizability: */
//	typedef typename matrix_type::resizing_tag resizing_tag;
//
//	/* For matching by result type: */
//	typedef typename matrix_type::result_tag result_tag;
//
//	/* For matching by assignability: */
//	typedef typename matrix_type::assignable_tag assignable_tag;
//
//	/* To simplify the matrix transpose operator: */
//	typedef typename matrix_type::transposed_type transposed_type;
//
//	/* To simplify the matrix row and column operators: */
//	typedef typename matrix_type::row_vector_type row_vector_type;
//
//	typedef typename matrix_type::col_vector_type col_vector_type;
//
//	// Accesses
//	value_type& operator[](size_t i)
//	{
//		assert(i < this->length());
//		return this->value[i];
//	}
//
//	value_type const& operator[](size_t i) const
//	{
//		assert(i < this->length());
//		return this->value[i];
//	}
//};


_ENGINE_END


#endif