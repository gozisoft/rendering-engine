#include "libmathsafx.h"
#include "PiecewiseBezier.h"

using namespace engine;

CPieceWiseBezier::CPieceWiseBezier(const curve_array& bezierArray)
{
	// Go through array of curves
	for (auto itor = bezierArray.begin(); itor != bezierArray.end(); ++itor)
	{
		curve_type pCurve = *itor;
		AddSingleCurveC1(pCurve);	
	}
}

CPieceWiseBezier::CPieceWiseBezier(const curve_type* pArray, size_t size)
{


}

CPieceWiseBezier::~CPieceWiseBezier()
{

}

void CPieceWiseBezier::AddSingleCurveC0(const curve_type& bezierCurve)
{
	// if the first curve
	if ( m_bezierCurves.empty() )
	{
		m_bezierCurves.push_back(bezierCurve);
	}
	else // not the first curve
	{	
		// ----------------------------------------------------------
		// Previous curve
		// ----------------------------------------------------------
		// Get the end curve
		const curve_type &curve = m_bezierCurves.back();
		
		// Get the control points and index the last end one
		const Vector3f *pCtrlPoints = curve.GetControlPoints();
		size_t lastControlPointIndex = curve.GetNumCtrlPoints();

		// Get the end control point of stored curve
		const Vector3f &endCtrlPoint = pCtrlPoints[ lastControlPointIndex - 1 ];

		// ----------------------------------------------------------
		// Input curve
		// ----------------------------------------------------------
		using std::vector;
		vector< Vector3f > NewCtrlPoints( bezierCurve.GetControlPoints(), bezierCurve.GetControlPoints() + bezierCurve.GetNumCtrlPoints() );

		// Set it equal to the first control point of the new bezier curve (c0 continuity)
		NewCtrlPoints[0] = endCtrlPoint;

		// Create the new tweaked curve
		curve_type newCurve(NewCtrlPoints);

		// ----------------------------------------------------------
		// Adding the new curve to the list
		// ----------------------------------------------------------
		m_bezierCurves.push_back(newCurve);
	}

}

void CPieceWiseBezier::AddSingleCurveC1(const curve_type& bezierCurve)
{
	// if the first curve
	if ( m_bezierCurves.empty() )
	{
		m_bezierCurves.push_back(bezierCurve);
	}
	else // not the first curve
	{	
		// ----------------------------------------------------------
		// Previous curve
		// ----------------------------------------------------------
		// Get the end curve
		curve_type &curve = m_bezierCurves.back();
		
		// Get the control points and index the last end one
		const Vector3f *pCtrlPoints = curve.GetControlPoints();
		size_t lastControlPointIndex = curve.GetNumCtrlPoints();

		// Get the end control point of stored curve and second end point
		const Vector3f &endCtrlPoint = pCtrlPoints[ lastControlPointIndex - 1 ];
		const Vector3f &oneLessThanEndCtrlPoint = pCtrlPoints[ lastControlPointIndex - 2 ];
		// ----------------------------------------------------------
		// Input curve
		// ----------------------------------------------------------
		using std::vector;
		vector< Vector3f > NewCtrlPoints( bezierCurve.GetControlPoints(), bezierCurve.GetControlPoints() + bezierCurve.GetNumCtrlPoints() );

		// Set it equal to the first control point of the new bezier curve (c1 continuity)
		NewCtrlPoints[0] = endCtrlPoint;

		// now set tangent of point of the previous to equal that of new.
		NewCtrlPoints[1] = (endCtrlPoint - oneLessThanEndCtrlPoint) + NewCtrlPoints[0];
	
		// Create the new tweaked curve
		curve_type newCurve(NewCtrlPoints);	

		// ----------------------------------------------------------
		// Adding the new curve to the list
		// ----------------------------------------------------------
		m_bezierCurves.push_back(newCurve);
	}
}


void CPieceWiseBezier::AddSingleCurveC2(const curve_type& bezierCurve)
{
	// if the first curve
	if ( m_bezierCurves.empty() )
	{
		m_bezierCurves.push_back(bezierCurve);
	}
	else // not the first curve
	{	
		// ----------------------------------------------------------
		// Previous curve
		// ----------------------------------------------------------
		// Get the end curve
		curve_type &curve = m_bezierCurves.back();
		
		// Get the control points and index the last end one
		const Vector3f *pCtrlPoints = curve.GetControlPoints();
		size_t lastControlPointIndex = curve.GetNumCtrlPoints();

		// Get the end control point of stored curve and second end point
		const Vector3f &endCtrlPoint = pCtrlPoints[ lastControlPointIndex - 1 ];
		const Vector3f &oneLessThanEndCtrlPoint = pCtrlPoints[ lastControlPointIndex - 2 ];
		// ----------------------------------------------------------
		// Input curve
		// ----------------------------------------------------------
		using std::vector;
		vector< Vector3f > NewCtrlPoints( bezierCurve.GetControlPoints(), bezierCurve.GetControlPoints() + bezierCurve.GetNumCtrlPoints() );

		// Set it equal to the first control point of the new bezier curve (c1 continuity)
		NewCtrlPoints[0] = endCtrlPoint;

		// now set tangent of point of the previous to equal that of new.
		NewCtrlPoints[1] = (endCtrlPoint - oneLessThanEndCtrlPoint) + NewCtrlPoints[0];

		// Create the new tweaked curve
		curve_type newCurve(NewCtrlPoints);	

		// ----------------------------------------------------------
		// Adding the new curve to the list
		// ----------------------------------------------------------
		m_bezierCurves.push_back(newCurve);
	}
}

const CBezierCurve& CPieceWiseBezier::GetCurve(size_t index) const
{
	return m_bezierCurves.at(index);
}

size_t CPieceWiseBezier::GetNumCurves() const
{
	return m_bezierCurves.size();
}
