#pragma once

#ifndef __VECTOR4_H__
#define __VECTOR4_H__

#include "Core.h"
#include "libmathsfwd.h"
#include <DirectXMath.h>

_ENGINE_BEGIN
_DETAIL_BEGIN

namespace dx = DirectX;

//------------------------------------------------------------------------------
// 4D vector
class Vector4f : public dx::XMFLOAT4
{
public:
	Vector4f() : dx::XMFLOAT4(0.f, 0.f, 0.f, 0.f) {}

	explicit Vector4f(float x) : dx::XMFLOAT4(x, x, x, x) {}

	Vector4f(float _x, float _y, float _z, float _w) : dx::XMFLOAT4(_x, _y, _z, _w) {}

	explicit Vector4f(_In_reads_(4) const float *pArray) : dx::XMFLOAT4(pArray) {}

	Vector4f(const dx::FXMVECTOR& V) { dx::XMStoreFloat4(this, V); }

	operator dx::XMVECTOR() const { return dx::XMLoadFloat4(this); }

	// Comparison operators
	bool operator == (const Vector4f& V) const;
	bool operator != (const Vector4f& V) const;

	// Assignment operators
	Vector4f& operator= (const Vector4f& V) { x = V.x; y = V.y; z = V.z; w = V.w; return *this; }
	Vector4f& operator+= (const Vector4f& V);
	Vector4f& operator-= (const Vector4f& V);
	Vector4f& operator*= (const Vector4f& V);
	Vector4f& operator*= (float S);
	Vector4f& operator/= (float S);

	// Urnary operators
	Vector4f operator+ () const { return *this; }
	Vector4f operator- () const;

	// Vector operations
	bool in_bounds(const Vector4f& Bounds) const;

	float length() const;
	float length_squared() const;

	float dot(const Vector4f& V) const;
	void cross(const Vector4f& v1, const Vector4f& v2, Vector4f& result) const;
	Vector4f cross(const Vector4f& v1, const Vector4f& v2) const;

	void normalize();
	void normalize(Vector4f& result) const;

	void clamp(const Vector4f& vmin, const Vector4f& vmax);
	void clamp(const Vector4f& vmin, const Vector4f& vmax, Vector4f& result) const;
};

// Binary operators
Vector4f operator+ (const Vector4f& V1, const Vector4f& V2);
Vector4f operator- (const Vector4f& V1, const Vector4f& V2);
Vector4f operator* (const Vector4f& V1, const Vector4f& V2);
Vector4f operator* (const Vector4f& V, float S);
Vector4f operator/ (const Vector4f& V1, const Vector4f& V2);
Vector4f operator* (float S, const Vector4f& V);

// Static functions
float vector_distance(const Vector4f& v1, const Vector4f& v2);
float vector_distance_squared(const Vector4f& v1, const Vector4f& v2);

void vector_min(const Vector4f& v1, const Vector4f& v2, Vector4f& result);
Vector4f vector_min(const Vector4f& v1, const Vector4f& v2);

void vector_max(const Vector4f& v1, const Vector4f& v2, Vector4f& result);
Vector4f vector_max(const Vector4f& v1, const Vector4f& v2);

void vector_lerp(const Vector4f& v1, const Vector4f& v2, float t, Vector4f& result);
Vector4f vector_lerp(const Vector4f& v1, const Vector4f& v2, float t);

void vector_smooth_step(const Vector4f& v1, const Vector4f& v2, float t, Vector4f& result);
Vector4f vector_smooth_step(const Vector4f& v1, const Vector4f& v2, float t);

void vector_barycentric(const Vector4f& v1, const Vector4f& v2, const Vector4f& v3, float f, float g, Vector4f& result);
Vector4f vector_barycentric(const Vector4f& v1, const Vector4f& v2, const Vector4f& v3, float f, float g);

void vector_catmull_rom(const Vector4f& v1, const Vector4f& v2, const Vector4f& v3, const Vector4f& v4, float t, Vector4f& result);
Vector4f vector_catmull_rom(const Vector4f& v1, const Vector4f& v2, const Vector4f& v3, const Vector4f& v4, float t);

void vector_hermite(const Vector4f& v1, const Vector4f& t1, const Vector4f& v2, const Vector4f& t2, float t, Vector4f& result);
Vector4f vector_hermite(const Vector4f& v1, const Vector4f& t1, const Vector4f& v2, const Vector4f& t2, float t);

void vector_reflect(const Vector4f& ivec, const Vector4f& nvec, Vector4f& result);
Vector4f vector_reflect(const Vector4f& ivec, const Vector4f& nvec);

void vector_refract(const Vector4f& ivec, const Vector4f& nvec, float refractionIndex, Vector4f& result);
Vector4f vector_refract(const Vector4f& ivec, const Vector4f& nvec, float refractionIndex);





_DETAIL_END
_ENGINE_END

#include "Vector4.inl"

#endif