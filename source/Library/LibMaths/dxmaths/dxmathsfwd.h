#pragma once

#ifndef __DXMATHS_EXTRA_H__
#define __DXMATHS_EXTRA_H__

#include "Core.h"

_ENGINE_BEGIN
_DETAIL_BEGIN

class Vector2f;
class Vector3f;
class Vector4f;
class Matrix4f;
class Quaternionf;

class Colour4f;

_DETAIL_END
_ENGINE_END

#endif