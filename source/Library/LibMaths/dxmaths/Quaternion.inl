#ifndef __QUATERNION_INL__
#define __QUATERNION_INL__

_ENGINE_BEGIN
_DETAIL_BEGIN
/****************************************************************************
*
* Quaternionf
*
****************************************************************************/

Quaternionf::Quaternionf() : dx::XMFLOAT4(0, 0, 0, 1.f) {}

Quaternionf::Quaternionf(float _x, float _y, float _z, float _w)
	: dx::XMFLOAT4(_x, _y, _z, _w)
{}

Quaternionf::Quaternionf(const Vector3f& v, float scalar)
	: dx::XMFLOAT4(v.x, v.y, v.z, scalar)
{}

Quaternionf::Quaternionf(const Vector4f& v)
	: dx::XMFLOAT4(v.x, v.y, v.z, v.w)
{}

Quaternionf::Quaternionf(_In_reads_(4) const float *pArray) :
dx::XMFLOAT4(pArray)
{}

Quaternionf::Quaternionf(const dx::FXMVECTOR& V)
{
	dx::XMStoreFloat4(this, V);
}

Quaternionf::operator dx::XMVECTOR()
{
	return dx::XMLoadFloat4(this);
}

//------------------------------------------------------------------------------
// Comparision operators
//------------------------------------------------------------------------------


inline bool Quaternionf::operator == (const Quaternionf& q) const
{
	DirectX::XMVECTOR q1 = DirectX::XMLoadFloat4(this);
	DirectX::XMVECTOR q2 = DirectX::XMLoadFloat4(&q);
	return DirectX::XMQuaternionEqual(q1, q2);
}

inline bool Quaternionf::operator != (const Quaternionf& q) const
{
	DirectX::XMVECTOR q1 = DirectX::XMLoadFloat4(this);
	DirectX::XMVECTOR q2 = DirectX::XMLoadFloat4(&q);
	return DirectX::XMQuaternionNotEqual(q1, q2);
}

//------------------------------------------------------------------------------
// Assignment operators
//------------------------------------------------------------------------------

inline Quaternionf& Quaternionf::operator+= (const Quaternionf& q)
{
	using namespace DirectX;
	XMVECTOR q1 = XMLoadFloat4(this);
	XMVECTOR q2 = XMLoadFloat4(&q);
	XMStoreFloat4(this, XMVectorAdd(q1, q2));
	return *this;
}

inline Quaternionf& Quaternionf::operator-= (const Quaternionf& q)
{
	using namespace DirectX;
	XMVECTOR q1 = XMLoadFloat4(this);
	XMVECTOR q2 = XMLoadFloat4(&q);
	XMStoreFloat4(this, XMVectorSubtract(q1, q2));
	return *this;
}

inline Quaternionf& Quaternionf::operator*= (const Quaternionf& q)
{
	using namespace DirectX;
	XMVECTOR q1 = XMLoadFloat4(this);
	XMVECTOR q2 = XMLoadFloat4(&q);
	XMStoreFloat4(this, XMQuaternionMultiply(q1, q2));
	return *this;
}

inline Quaternionf& Quaternionf::operator*= (float S)
{
	using namespace DirectX;
	XMVECTOR q = XMLoadFloat4(this);
	XMStoreFloat4(this, XMVectorScale(q, S));
	return *this;
}

inline Quaternionf& Quaternionf::operator/= (const Quaternionf& q)
{
	using namespace DirectX;
	XMVECTOR q1 = XMLoadFloat4(this);
	XMVECTOR q2 = XMLoadFloat4(&q);
	q2 = XMQuaternionInverse(q2);
	XMStoreFloat4(this, XMQuaternionMultiply(q1, q2));
	return *this;
}

//------------------------------------------------------------------------------
// Urnary operators
//------------------------------------------------------------------------------

inline Quaternionf Quaternionf::operator- () const
{
	using namespace DirectX;
	XMVECTOR q = XMLoadFloat4(this);

	Quaternionf R;
	XMStoreFloat4(&R, XMVectorNegate(q));
	return R;
}

//------------------------------------------------------------------------------
// Binary operators
//------------------------------------------------------------------------------

inline Quaternionf operator+ (const Quaternionf& Q1, const Quaternionf& Q2)
{
	using namespace DirectX;
	XMVECTOR q1 = XMLoadFloat4(&Q1);
	XMVECTOR q2 = XMLoadFloat4(&Q2);

	Quaternionf R;
	XMStoreFloat4(&R, XMVectorAdd(q1, q2));
	return R;
}

inline Quaternionf operator- (const Quaternionf& Q1, const Quaternionf& Q2)
{
	using namespace DirectX;
	XMVECTOR q1 = XMLoadFloat4(&Q1);
	XMVECTOR q2 = XMLoadFloat4(&Q2);

	Quaternionf R;
	XMStoreFloat4(&R, XMVectorSubtract(q1, q2));
	return R;
}

inline Quaternionf operator* (const Quaternionf& Q1, const Quaternionf& Q2)
{
	using namespace DirectX;
	XMVECTOR q1 = XMLoadFloat4(&Q1);
	XMVECTOR q2 = XMLoadFloat4(&Q2);

	Quaternionf R;
	XMStoreFloat4(&R, XMQuaternionMultiply(q1, q2));
	return R;
}

inline Quaternionf operator* (const Quaternionf& Q, float S)
{
	using namespace DirectX;
	XMVECTOR q = XMLoadFloat4(&Q);

	Quaternionf R;
	XMStoreFloat4(&R, XMVectorScale(q, S));
	return R;
}

inline Quaternionf operator/ (const Quaternionf& Q1, const Quaternionf& Q2)
{
	using namespace DirectX;
	XMVECTOR q1 = XMLoadFloat4(&Q1);
	XMVECTOR q2 = XMLoadFloat4(&Q2);
	q2 = XMQuaternionInverse(q2);

	Quaternionf R;
	XMStoreFloat4(&R, XMQuaternionMultiply(q1, q2));
	return R;
}

inline Quaternionf operator* (float S, const Quaternionf& Q)
{
	using namespace DirectX;
	XMVECTOR q1 = XMLoadFloat4(&Q);

	Quaternionf R;
	XMStoreFloat4(&R, XMVectorScale(q1, S));
	return R;
}

//------------------------------------------------------------------------------
// Quaternionf operations
//------------------------------------------------------------------------------

inline float Quaternionf::length() const
{
	using namespace DirectX;
	XMVECTOR q = XMLoadFloat4(this);
	return XMVectorGetX(XMQuaternionLength(q));
}

inline float Quaternionf::lengthSquared() const
{
	using namespace DirectX;
	XMVECTOR q = XMLoadFloat4(this);
	return XMVectorGetX(XMQuaternionLengthSq(q));
}

inline void Quaternionf::normalize()
{
	using namespace DirectX;
	XMVECTOR q = XMLoadFloat4(this);
	XMStoreFloat4(this, XMQuaternionNormalize(q));
}

inline void Quaternionf::normalize(Quaternionf& result) const
{
	using namespace DirectX;
	XMVECTOR q = XMLoadFloat4(this);
	XMStoreFloat4(&result, XMQuaternionNormalize(q));
}

inline void Quaternionf::conjugate()
{
	using namespace DirectX;
	XMVECTOR q = XMLoadFloat4(this);
	XMStoreFloat4(this, XMQuaternionConjugate(q));
}

inline void Quaternionf::conjugate(Quaternionf& result) const
{
	using namespace DirectX;
	XMVECTOR q = XMLoadFloat4(this);
	XMStoreFloat4(&result, XMQuaternionConjugate(q));
}

inline void Quaternionf::inverse(Quaternionf& result) const
{
	using namespace DirectX;
	XMVECTOR q = XMLoadFloat4(this);
	XMStoreFloat4(&result, XMQuaternionInverse(q));
}

inline float Quaternionf::dot(const Quaternionf& q) const
{
	using namespace DirectX;
	XMVECTOR q1 = XMLoadFloat4(this);
	XMVECTOR q2 = XMLoadFloat4(&q);
	return XMVectorGetX(XMQuaternionDot(q1, q2));
}

//------------------------------------------------------------------------------
// Static functions
//------------------------------------------------------------------------------

inline Quaternionf Quaternionf::createFromAxisAngle(const Vector3f& axis, float angle)
{
	using namespace DirectX;
	XMVECTOR a = XMLoadFloat3(&axis);

	Quaternionf R;
	XMStoreFloat4(&R, XMQuaternionRotationAxis(a, angle));
	return R;
}

inline Quaternionf Quaternionf::createFromRotationMatrix(const Matrix4f& M)
{
	using namespace DirectX;
	XMMATRIX M0 = XMLoadFloat4x4(&M);

	Quaternionf R;
	XMStoreFloat4(&R, XMQuaternionRotationMatrix(M0));
	return R;
}

inline Quaternionf Quaternionf::createFromYawPitchRoll(float yaw, float pitch, float roll)
{
	using namespace DirectX;
	Quaternionf R;
	XMStoreFloat4(&R, XMQuaternionRotationRollPitchYaw(pitch, yaw, roll));
	return R;
}

inline void Quaternionf::lerp(const Quaternionf& q1, const Quaternionf& q2, float t, Quaternionf& result)
{
	using namespace DirectX;
	XMVECTOR Q0 = XMLoadFloat4(&q1);
	XMVECTOR Q1 = XMLoadFloat4(&q2);

	XMVECTOR dot = XMVector4Dot(Q0, Q1);

	XMVECTOR R;
	if (XMVector4GreaterOrEqual(dot, XMVectorZero()))
	{
		R = XMVectorLerp(Q0, Q1, t);
	}
	else
	{
		XMVECTOR tv = XMVectorReplicate(t);
		XMVECTOR t1v = XMVectorReplicate(1.f - t);
		XMVECTOR X0 = XMVectorMultiply(Q0, t1v);
		XMVECTOR X1 = XMVectorMultiply(Q1, tv);
		R = XMVectorSubtract(X0, X1);
	}

	XMStoreFloat4(&result, XMQuaternionNormalize(R));
}

inline Quaternionf Quaternionf::lerp(const Quaternionf& q1, const Quaternionf& q2, float t)
{
	using namespace DirectX;
	XMVECTOR Q0 = XMLoadFloat4(&q1);
	XMVECTOR Q1 = XMLoadFloat4(&q2);

	XMVECTOR dot = XMVector4Dot(Q0, Q1);

	XMVECTOR R;
	if (XMVector4GreaterOrEqual(dot, XMVectorZero()))
	{
		R = XMVectorLerp(Q0, Q1, t);
	}
	else
	{
		XMVECTOR tv = XMVectorReplicate(t);
		XMVECTOR t1v = XMVectorReplicate(1.f - t);
		XMVECTOR X0 = XMVectorMultiply(Q0, t1v);
		XMVECTOR X1 = XMVectorMultiply(Q1, tv);
		R = XMVectorSubtract(X0, X1);
	}

	Quaternionf result;
	XMStoreFloat4(&result, XMQuaternionNormalize(R));
	return result;
}

inline void Quaternionf::slerp(const Quaternionf& q1, const Quaternionf& q2, float t, Quaternionf& result)
{
	using namespace DirectX;
	XMVECTOR Q0 = XMLoadFloat4(&q1);
	XMVECTOR Q1 = XMLoadFloat4(&q2);
	XMStoreFloat4(&result, XMQuaternionSlerp(Q0, Q1, t));
}

inline Quaternionf Quaternionf::slerp(const Quaternionf& q1, const Quaternionf& q2, float t)
{
	using namespace DirectX;
	XMVECTOR Q0 = XMLoadFloat4(&q1);
	XMVECTOR Q1 = XMLoadFloat4(&q2);

	Quaternionf result;
	XMStoreFloat4(&result, XMQuaternionSlerp(Q0, Q1, t));
	return result;
}

inline void Quaternionf::concatenate(const Quaternionf& q1, const Quaternionf& q2, Quaternionf& result)
{
	using namespace DirectX;
	XMVECTOR Q0 = XMLoadFloat4(&q1);
	XMVECTOR Q1 = XMLoadFloat4(&q2);
	XMStoreFloat4(&result, XMQuaternionMultiply(Q1, Q0));
}

inline Quaternionf Quaternionf::concatenate(const Quaternionf& q1, const Quaternionf& q2)
{
	using namespace DirectX;
	XMVECTOR Q0 = XMLoadFloat4(&q1);
	XMVECTOR Q1 = XMLoadFloat4(&q2);

	Quaternionf result;
	XMStoreFloat4(&result, XMQuaternionMultiply(Q1, Q0));
	return result;
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


void vector_transform(const Vector2f& v, const Quaternionf& quat, Vector2f& result)
{
	using namespace DirectX;
	XMVECTOR v1 = XMLoadFloat2(&v);
	XMVECTOR q = XMLoadFloat4(&quat);
	XMVECTOR X = XMVector3Rotate(v1, q);
	XMStoreFloat2(&result, X);
}

//Vector2f vector_transform(const Vector2f& v, const Quaternionf& quat)
//{
//	using namespace DirectX;
//	XMVECTOR v1 = XMLoadFloat2(&v);
//	XMVECTOR q = XMLoadFloat4(&quat);
//	XMVECTOR X = XMVector3Rotate(v1, q);
//
//	Vector2f result;
//	XMStoreFloat2(&result, X);
//	return result;
//}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


void vector_transform(const Vector3f& v, const Quaternionf& quat, Vector3f& result)
{
	using namespace DirectX;
	XMVECTOR v1 = XMLoadFloat3(&v);
	XMVECTOR q = XMLoadFloat4(&quat);
	XMVECTOR X = XMVector3Rotate(v1, q);
	XMStoreFloat3(&result, X);
}

//Vector3f vector_transform(const Vector3f& v, const Quaternionf& quat)
//{
//	using namespace DirectX;
//	XMVECTOR v1 = XMLoadFloat3(&v);
//	XMVECTOR q = XMLoadFloat4(&quat);
//	XMVECTOR X = XMVector3Rotate(v1, q);
//
//	Vector3f result;
//	XMStoreFloat3(&result, X);
//	return result;
//}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

inline void vector_transform(const Vector2f& v, const Quaternionf& quat, Vector4f& result)
{
	using namespace DirectX;
	XMVECTOR v1 = XMLoadFloat2(&v);
	XMVECTOR q = XMLoadFloat4(&quat);
	XMVECTOR X = XMVector3Rotate(v1, q);
	X = XMVectorSelect(g_XMIdentityR3, X, g_XMSelect1110); // result.w = 1.f
	XMStoreFloat4(&result, X);
}

inline Vector4f vector_transform(const Vector2f& v, const Quaternionf& quat)
{
	using namespace DirectX;
	XMVECTOR v1 = XMLoadFloat2(&v);
	XMVECTOR q = XMLoadFloat4(&quat);
	XMVECTOR X = XMVector3Rotate(v1, q);
	X = XMVectorSelect(g_XMIdentityR3, X, g_XMSelect1110); // result.w = 1.f

	Vector4f result;
	XMStoreFloat4(&result, X);
	return result;
}

inline void vector_transform(const Vector3f& v, const Quaternionf& quat, Vector4f& result)
{
	using namespace DirectX;
	XMVECTOR v1 = XMLoadFloat3(&v);
	XMVECTOR q = XMLoadFloat4(&quat);
	XMVECTOR X = XMVector3Rotate(v1, q);
	X = XMVectorSelect(g_XMIdentityR3, X, g_XMSelect1110); // result.w = 1.f
	XMStoreFloat4(&result, X);
}

inline Vector4f Transform(const Vector3f& v, const Quaternionf& quat)
{
	using namespace DirectX;
	XMVECTOR v1 = XMLoadFloat3(&v);
	XMVECTOR q = XMLoadFloat4(&quat);
	XMVECTOR X = XMVector3Rotate(v1, q);
	X = XMVectorSelect(g_XMIdentityR3, X, g_XMSelect1110); // result.w = 1.f

	Vector4f result;
	XMStoreFloat4(&result, X);
	return result;
}

inline void vector_transform(const Vector4f& v, const Quaternionf& quat, Vector4f& result)
{
	using namespace DirectX;
	XMVECTOR v1 = XMLoadFloat4(&v);
	XMVECTOR q = XMLoadFloat4(&quat);
	XMVECTOR X = XMVector3Rotate(v1, q);
	X = XMVectorSelect(v1, X, g_XMSelect1110); // result.w = v.w
	XMStoreFloat4(&result, X);
}

inline Vector4f vectr_transform(const Vector4f& v, const Quaternionf& quat)
{
	using namespace DirectX;
	XMVECTOR v1 = XMLoadFloat4(&v);
	XMVECTOR q = XMLoadFloat4(&quat);
	XMVECTOR X = XMVector3Rotate(v1, q);
	X = XMVectorSelect(v1, X, g_XMSelect1110); // result.w = v.w

	Vector4f result;
	XMStoreFloat4(&result, X);
	return result;
}


_DETAIL_END
_ENGINE_END

#endif