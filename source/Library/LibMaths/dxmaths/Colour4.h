#pragma once

#ifndef __COLOUR4_H__
#define __COLOUR4_H__

#include "Core.h"
#include "dxmathsfwd.h"
#include "Vector3.h"
#include "Vector4.h"
#include <DirectXMath.h>
#include <DirectXPackedVector.h>

_ENGINE_BEGIN
_DETAIL_BEGIN

namespace dx = DirectX;

//------------------------------------------------------------------------------
// Color
class Colour4f : public dx::XMFLOAT4
{
public:
	Colour4f() : dx::XMFLOAT4(0, 0, 0, 1.f) {}

	Colour4f(float _r, float _g, float _b) : dx::XMFLOAT4(_r, _g, _b, 1.f) {}

	Colour4f(float _r, float _g, float _b, float _a) : dx::XMFLOAT4(_r, _g, _b, _a) {}

	explicit Colour4f(const Vector3f& clr) : dx::XMFLOAT4(clr.x, clr.y, clr.z, 1.f) {}

	explicit Colour4f(const Vector4f& clr) : dx::XMFLOAT4(clr.x, clr.y, clr.z, clr.w) {}

	explicit Colour4f(_In_reads_(4) const float *pArray) : dx::XMFLOAT4(pArray) {}

	Colour4f(dx::FXMVECTOR V) { dx::XMStoreFloat4(this, V); }

	explicit Colour4f(const dx::PackedVector::XMCOLOR& Packed);
	// BGRA Direct3D 9 D3DCOLOR packed color

	explicit Colour4f(const dx::PackedVector::XMUBYTEN4& Packed);
	// RGBA XNA Game Studio packed color

	operator dx::XMVECTOR() const { return dx::XMLoadFloat4(this); }
	operator const float*() const { return reinterpret_cast<const float*>(this); }

	// Comparision operators
	bool operator == (const Colour4f& c) const;
	bool operator != (const Colour4f& c) const;

	// Assignment operators
	Colour4f& operator= (const Colour4f& c) { x = c.x; y = c.y; z = c.z; w = c.w; return *this; }
	Colour4f& operator+= (const Colour4f& c);
	Colour4f& operator-= (const Colour4f& c);
	Colour4f& operator*= (const Colour4f& c);
	Colour4f& operator*= (float S);
	Colour4f& operator/= (const Colour4f& c);

	// Urnary operators
	Colour4f operator+ () const { return *this; }
	Colour4f operator- () const;

	// Properties
	float R() const { return x; }
	void R(float r) { x = r; }

	float G() const { return y; }
	void G(float g) { y = g; }

	float B() const { return z; }
	void B(float b) { z = b; }

	float A() const { return w; }
	void A(float a) { w = a; }

	// Color operations
	DirectX::PackedVector::XMCOLOR BGRA() const;
	DirectX::PackedVector::XMUBYTEN4 RGBA() const;

	Vector3f ToVector3f() const;
	Vector4f ToVector4f() const;

	void Negate();
	void Negate(Colour4f& result) const;

	void Saturate();
	void Saturate(Colour4f& result) const;

	void Premultiply();
	void Premultiply(Colour4f& result) const;

	void AdjustSaturation(float sat);
	void AdjustSaturation(float sat, Colour4f& result) const;

	void AdjustContrast(float contrast);
	void AdjustContrast(float contrast, Colour4f& result) const;
};

// Binary operators
Colour4f operator+ (const Colour4f& C1, const Colour4f& C2);
Colour4f operator- (const Colour4f& C1, const Colour4f& C2);
Colour4f operator* (const Colour4f& C1, const Colour4f& C2);
Colour4f operator* (const Colour4f& C, float S);
Colour4f operator/ (const Colour4f& C1, const Colour4f& C2);
Colour4f operator* (float S, const Colour4f& C);

// Static functions
void modulate(const Colour4f& c1, const Colour4f& c2, Colour4f& result);
Colour4f modulate(const Colour4f& c1, const Colour4f& c2);

void lerp(const Colour4f& c1, const Colour4f& c2, float t, Colour4f& result);
Colour4f lerp(const Colour4f& c1, const Colour4f& c2, float t);



_DETAIL_END
_ENGINE_END


#include "Colour4.inl"

#endif