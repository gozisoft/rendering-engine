#ifndef __COLOR_INL__
#define __COLOR_INL__

_ENGINE_BEGIN
_DETAIL_BEGIN

/****************************************************************************
*
* Colour4f
*
****************************************************************************/

inline Colour4f::Colour4f(const DirectX::PackedVector::XMCOLOR& Packed)
{
	using namespace DirectX;
	XMStoreFloat4(this, PackedVector::XMLoadColor(&Packed));
}

inline Colour4f::Colour4f(const DirectX::PackedVector::XMUBYTEN4& Packed)
{
	using namespace DirectX;
	XMStoreFloat4(this, PackedVector::XMLoadUByteN4(&Packed));
}

//------------------------------------------------------------------------------
// Comparision operators
//------------------------------------------------------------------------------
inline bool Colour4f::operator == (const Colour4f& c) const
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(this);
	XMVECTOR c2 = XMLoadFloat4(&c);
	return XMColorEqual(c1, c2);
}

inline bool Colour4f::operator != (const Colour4f& c) const
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(this);
	XMVECTOR c2 = XMLoadFloat4(&c);
	return XMColorNotEqual(c1, c2);
}

//------------------------------------------------------------------------------
// Assignment operators
//------------------------------------------------------------------------------

inline Colour4f& Colour4f::operator+= (const Colour4f& c)
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(this);
	XMVECTOR c2 = XMLoadFloat4(&c);
	XMStoreFloat4(this, XMVectorAdd(c1, c2));
	return *this;
}

inline Colour4f& Colour4f::operator-= (const Colour4f& c)
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(this);
	XMVECTOR c2 = XMLoadFloat4(&c);
	XMStoreFloat4(this, XMVectorSubtract(c1, c2));
	return *this;
}

inline Colour4f& Colour4f::operator*= (const Colour4f& c)
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(this);
	XMVECTOR c2 = XMLoadFloat4(&c);
	XMStoreFloat4(this, XMVectorMultiply(c1, c2));
	return *this;
}

inline Colour4f& Colour4f::operator*= (float S)
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMStoreFloat4(this, XMVectorScale(c, S));
	return *this;
}

inline Colour4f& Colour4f::operator/= (const Colour4f& c)
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(this);
	XMVECTOR c2 = XMLoadFloat4(&c);
	XMStoreFloat4(this, XMVectorDivide(c1, c2));
	return *this;
}

//------------------------------------------------------------------------------
// Urnary operators
//------------------------------------------------------------------------------

inline Colour4f Colour4f::operator- () const
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	Colour4f R;
	XMStoreFloat4(&R, XMVectorNegate(c));
	return R;
}

//------------------------------------------------------------------------------
// Binary operators
//------------------------------------------------------------------------------

inline Colour4f operator+ (const Colour4f& C1, const Colour4f& C2)
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(&C1);
	XMVECTOR c2 = XMLoadFloat4(&C2);
	Colour4f R;
	XMStoreFloat4(&R, XMVectorAdd(c1, c2));
	return R;
}

inline Colour4f operator- (const Colour4f& C1, const Colour4f& C2)
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(&C1);
	XMVECTOR c2 = XMLoadFloat4(&C2);
	Colour4f R;
	XMStoreFloat4(&R, XMVectorSubtract(c1, c2));
	return R;
}

inline Colour4f operator* (const Colour4f& C1, const Colour4f& C2)
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(&C1);
	XMVECTOR c2 = XMLoadFloat4(&C2);
	Colour4f R;
	XMStoreFloat4(&R, XMVectorMultiply(c1, c2));
	return R;
}

inline Colour4f operator* (const Colour4f& C, float S)
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(&C);
	Colour4f R;
	XMStoreFloat4(&R, XMVectorScale(c, S));
	return R;
}

inline Colour4f operator/ (const Colour4f& C1, const Colour4f& C2)
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(&C1);
	XMVECTOR c2 = XMLoadFloat4(&C2);
	Colour4f R;
	XMStoreFloat4(&R, XMVectorDivide(c1, c2));
	return R;
}

inline Colour4f operator* (float S, const Colour4f& C)
{
	using namespace DirectX;
	XMVECTOR c1 = XMLoadFloat4(&C);
	Colour4f R;
	XMStoreFloat4(&R, XMVectorScale(c1, S));
	return R;
}

//------------------------------------------------------------------------------
// Colour4f operations
//------------------------------------------------------------------------------

inline DirectX::PackedVector::XMCOLOR Colour4f::BGRA() const
{
	using namespace DirectX;
	XMVECTOR clr = XMLoadFloat4(this);
	PackedVector::XMCOLOR Packed;
	PackedVector::XMStoreColor(&Packed, clr);
	return Packed;
}

inline DirectX::PackedVector::XMUBYTEN4 Colour4f::RGBA() const
{
	using namespace DirectX;
	XMVECTOR clr = XMLoadFloat4(this);
	PackedVector::XMUBYTEN4 Packed;
	PackedVector::XMStoreUByteN4(&Packed, clr);
	return Packed;
}

inline Vector3f Colour4f::ToVector3f() const
{
	return Vector3f(x, y, z);
}

inline Vector4f Colour4f::ToVector4f() const
{
	return Vector4f(x, y, z, w);
}

inline void Colour4f::Negate()
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMStoreFloat4(this, XMColorNegative(c));
}

inline void Colour4f::Negate(Colour4f& result) const
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMStoreFloat4(&result, XMColorNegative(c));
}

inline void Colour4f::Saturate()
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMStoreFloat4(this, XMVectorSaturate(c));
}

inline void Colour4f::Saturate(Colour4f& result) const
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMStoreFloat4(&result, XMVectorSaturate(c));
}

inline void Colour4f::Premultiply()
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMVECTOR a = XMVectorSplatW(c);
	a = XMVectorSelect(g_XMIdentityR3, a, g_XMSelect1110);
	XMStoreFloat4(this, XMVectorMultiply(c, a));
}

inline void Colour4f::Premultiply(Colour4f& result) const
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMVECTOR a = XMVectorSplatW(c);
	a = XMVectorSelect(g_XMIdentityR3, a, g_XMSelect1110);
	XMStoreFloat4(&result, XMVectorMultiply(c, a));
}

inline void Colour4f::AdjustSaturation(float sat)
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMStoreFloat4(this, XMColorAdjustSaturation(c, sat));
}

inline void Colour4f::AdjustSaturation(float sat, Colour4f& result) const
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMStoreFloat4(&result, XMColorAdjustSaturation(c, sat));
}

inline void Colour4f::AdjustContrast(float contrast)
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMStoreFloat4(this, XMColorAdjustContrast(c, contrast));
}

inline void Colour4f::AdjustContrast(float contrast, Colour4f& result) const
{
	using namespace DirectX;
	XMVECTOR c = XMLoadFloat4(this);
	XMStoreFloat4(&result, XMColorAdjustContrast(c, contrast));
}

//------------------------------------------------------------------------------
// Static functions
//------------------------------------------------------------------------------

inline void modulate(const Colour4f& c1, const Colour4f& c2, Colour4f& result)
{
	using namespace DirectX;
	XMVECTOR C0 = XMLoadFloat4(&c1);
	XMVECTOR C1 = XMLoadFloat4(&c2);
	XMStoreFloat4(&result, XMColorModulate(C0, C1));
}

inline Colour4f modulate(const Colour4f& c1, const Colour4f& c2)
{
	using namespace DirectX;
	XMVECTOR C0 = XMLoadFloat4(&c1);
	XMVECTOR C1 = XMLoadFloat4(&c2);

	Colour4f result;
	XMStoreFloat4(&result, XMColorModulate(C0, C1));
	return result;
}

inline void  lerp(const Colour4f& c1, const Colour4f& c2, float t, Colour4f& result)
{
	using namespace DirectX;
	XMVECTOR C0 = XMLoadFloat4(&c1);
	XMVECTOR C1 = XMLoadFloat4(&c2);
	XMStoreFloat4(&result, XMVectorLerp(C0, C1, t));
}

inline Colour4f lerp(const Colour4f& c1, const Colour4f& c2, float t)
{
	using namespace DirectX;
	XMVECTOR C0 = XMLoadFloat4(&c1);
	XMVECTOR C1 = XMLoadFloat4(&c2);

	Colour4f result;
	XMStoreFloat4(&result, XMVectorLerp(C0, C1, t));
	return result;
}

_DETAIL_END
_ENGINE_END

#endif