#pragma once

#ifndef __VECTOR3_H__
#define __VECTOR3_H__

#include "Core.h"
#include "libmathsfwd.h"
#include <DirectXMath.h>

_ENGINE_BEGIN
_DETAIL_BEGIN

namespace dx = DirectX;

//------------------------------------------------------------------------------
// 3D vector
class Vector3f : public dx::XMFLOAT3
{
public:
	Vector3f() : dx::XMFLOAT3(0.f, 0.f, 0.f) {}

	explicit Vector3f(float x) : dx::XMFLOAT3(x, x, x) {}

	Vector3f(float _x, float _y, float _z) : dx::XMFLOAT3(_x, _y, _z) {}

	explicit Vector3f(_In_reads_(3) const float *pArray) : dx::XMFLOAT3(pArray) {}

	Vector3f(dx::FXMVECTOR V) { dx::XMStoreFloat3(this, V); }

	operator dx::XMVECTOR() const { return dx::XMLoadFloat3(this); }

	// Comparison operators
	bool operator == (const Vector3f& V) const;
	bool operator != (const Vector3f& V) const;

	// Assignment operators
	Vector3f& operator= (const Vector3f& V) { x = V.x; y = V.y; z = V.z; return *this; }
	Vector3f& operator+= (const Vector3f& V);
	Vector3f& operator-= (const Vector3f& V);
	Vector3f& operator*= (const Vector3f& V);
	Vector3f& operator*= (float S);
	Vector3f& operator/= (float S);

	// Urnary operators
	Vector3f operator+ () const { return *this; }
	Vector3f operator- () const;

	// Vector operations
	bool InBounds(const Vector3f& Bounds) const;

	float Length() const;
	float LengthSquared() const;

	float Dot(const Vector3f& V) const;
	void Cross(const Vector3f& V, Vector3f& result) const;
	Vector3f Cross(const Vector3f& V) const;

	void Normalize();
	void Normalize(Vector3f& result) const;

	void Clamp(const Vector3f& vmin, const Vector3f& vmax);
	void Clamp(const Vector3f& vmin, const Vector3f& vmax, Vector3f& result) const;


};

// Binary operators
Vector3f operator+ (const Vector3f& V1, const Vector3f& V2);
Vector3f operator- (const Vector3f& V1, const Vector3f& V2);
Vector3f operator* (const Vector3f& V1, const Vector3f& V2);
Vector3f operator* (const Vector3f& V, float S);
Vector3f operator/ (const Vector3f& V1, const Vector3f& V2);
Vector3f operator* (float S, const Vector3f& V);

// Static functions
float vector_distance(const Vector3f& v1, const Vector3f& v2);
float vector_distance_squared(const Vector3f& v1, const Vector3f& v2);

void vector_min(const Vector3f& v1, const Vector3f& v2, Vector3f& result);
Vector3f vector_min(const Vector3f& v1, const Vector3f& v2);

void vector_max(const Vector3f& v1, const Vector3f& v2, Vector3f& result);
Vector3f vector_max(const Vector3f& v1, const Vector3f& v2);

void vector_lerp(const Vector3f& v1, const Vector3f& v2, float t, Vector3f& result);
Vector3f vector_lerp(const Vector3f& v1, const Vector3f& v2, float t);

void vector_smooth_step(const Vector3f& v1, const Vector3f& v2, float t, Vector3f& result);
Vector3f vector_smooth_step(const Vector3f& v1, const Vector3f& v2, float t);

void vector_barycentric(const Vector3f& v1, const Vector3f& v2, const Vector3f& v3, float f, float g, Vector3f& result);
Vector3f vector_barycentric(const Vector3f& v1, const Vector3f& v2, const Vector3f& v3, float f, float g);

void vector_catmull_rom(const Vector3f& v1, const Vector3f& v2, const Vector3f& v3, const Vector3f& v4, float t, Vector3f& result);
Vector3f vector_catmull_rom(const Vector3f& v1, const Vector3f& v2, const Vector3f& v3, const Vector3f& v4, float t);

void vector_hermite(const Vector3f& v1, const Vector3f& t1, const Vector3f& v2, const Vector3f& t2, float t, Vector3f& result);
Vector3f vector_hermite(const Vector3f& v1, const Vector3f& t1, const Vector3f& v2, const Vector3f& t2, float t);

void vector_reflect(const Vector3f& ivec, const Vector3f& nvec, Vector3f& result);
Vector3f vector_reflect(const Vector3f& ivec, const Vector3f& nvec);

void vector_refract(const Vector3f& ivec, const Vector3f& nvec, float refractionIndex, Vector3f& result);
Vector3f vector_refract(const Vector3f& ivec, const Vector3f& nvec, float refractionIndex);





_DETAIL_END
_ENGINE_END

#include "Vector3.inl"

#endif