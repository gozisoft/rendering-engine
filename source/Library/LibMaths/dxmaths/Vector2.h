#pragma once

#ifndef __VECTOR2_H__
#define __VECTOR2_H__

#include "Core.h"
#include "libmathsfwd.h"
#include <DirectXMath.h>

_ENGINE_BEGIN
_DETAIL_BEGIN

namespace dx = DirectX;

//------------------------------------------------------------------------------
// 2D vector
class Vector2f : public dx::XMFLOAT2
{
public:
	Vector2f() : dx::XMFLOAT2(0.f, 0.f) {}

	explicit Vector2f(float x) : dx::XMFLOAT2(x, x) {}

	Vector2f(float _x, float _y) : dx::XMFLOAT2(_x, _y) {}

	explicit Vector2f(_In_reads_(2) const float *pArray) : dx::XMFLOAT2(pArray) {}

	Vector2f(dx::FXMVECTOR V) { dx::XMStoreFloat2(this, V); }

	operator dx::XMVECTOR() const { return dx::XMLoadFloat2(this); }

	// Comparison operators
	bool operator == (const Vector2f& V) const;
	bool operator != (const Vector2f& V) const;

	// Assignment operators
	Vector2f& operator= (const Vector2f& V) { x = V.x; y = V.y; return *this; }
	Vector2f& operator+= (const Vector2f& V);
	Vector2f& operator-= (const Vector2f& V);
	Vector2f& operator*= (const Vector2f& V);
	Vector2f& operator*= (float S);
	Vector2f& operator/= (float S);

	// Urnary operators
	Vector2f operator+ () const { return *this; }
	Vector2f operator- () const { return Vector2f(-x, -y); }

	// Vector operations
	bool InBounds(const Vector2f& Bounds) const;

	float Length() const;
	float LengthSquared() const;

	float Dot(const Vector2f& V) const;
	void Cross(const Vector2f& V, Vector2f& result) const;
	Vector2f Cross(const Vector2f& V) const;

	void Normalize();
	void Normalize(Vector2f& result) const;

	void Clamp(const Vector2f& vmin, const Vector2f& vmax);
	void Clamp(const Vector2f& vmin, const Vector2f& vmax, Vector2f& result) const;


};

// Binary operators
Vector2f operator+ (const Vector2f& V1, const Vector2f& V2);
Vector2f operator- (const Vector2f& V1, const Vector2f& V2);
Vector2f operator* (const Vector2f& V1, const Vector2f& V2);
Vector2f operator* (const Vector2f& V, float S);
Vector2f operator/ (const Vector2f& V1, const Vector2f& V2);
Vector2f operator* (float S, const Vector2f& V);

// Static functions
float vector_distance(const Vector2f& v1, const Vector2f& v2);
float vector_distance_squared(const Vector2f& v1, const Vector2f& v2);

void vector_min(const Vector2f& v1, const Vector2f& v2, Vector2f& result);
Vector2f vector_min(const Vector2f& v1, const Vector2f& v2);

void vector_max(const Vector2f& v1, const Vector2f& v2, Vector2f& result);
Vector2f vector_max(const Vector2f& v1, const Vector2f& v2);

void vector_lerp(const Vector2f& v1, const Vector2f& v2, float t, Vector2f& result);
Vector2f vector_lerp(const Vector2f& v1, const Vector2f& v2, float t);

void vector_smooth_step(const Vector2f& v1, const Vector2f& v2, float t, Vector2f& result);
Vector2f vector_smooth_step(const Vector2f& v1, const Vector2f& v2, float t);

void vector_barycentric(const Vector2f& v1, const Vector2f& v2, const Vector2f& v3, float f, float g, Vector2f& result);
Vector2f vector_barycentric(const Vector2f& v1, const Vector2f& v2, const Vector2f& v3, float f, float g);

void vector_catmull_rom(const Vector2f& v1, const Vector2f& v2, const Vector2f& v3, const Vector2f& v4, float t, Vector2f& result);
Vector2f vector_catmull_rom(const Vector2f& v1, const Vector2f& v2, const Vector2f& v3, const Vector2f& v4, float t);

void vector_hermite(const Vector2f& v1, const Vector2f& t1, const Vector2f& v2, const Vector2f& t2, float t, Vector2f& result);
Vector2f vector_hermite(const Vector2f& v1, const Vector2f& t1, const Vector2f& v2, const Vector2f& t2, float t);

void vector_reflect(const Vector2f& ivec, const Vector2f& nvec, Vector2f& result);
Vector2f vector_reflect(const Vector2f& ivec, const Vector2f& nvec);

void vector_refract(const Vector2f& ivec, const Vector2f& nvec, float refractionIndex, Vector2f& result);
Vector2f vector_refract(const Vector2f& ivec, const Vector2f& nvec, float refractionIndex);




_DETAIL_END
_ENGINE_END

// Include .inl file for implementation.
#include "Vector2.inl"

#endif