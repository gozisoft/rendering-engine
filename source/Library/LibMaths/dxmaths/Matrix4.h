#pragma once

#ifndef __MATRIX4_H__
#define __MATRIX4_H__

#include "Core.h"
#include "dxmathsfwd.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Quaternion.h"
#include "DirectxMath.h"

_ENGINE_BEGIN
_DETAIL_BEGIN

namespace DX = DirectX;

//------------------------------------------------------------------------------
// 4x4 Matrix4f (assumes right-handed coordinates)
class Matrix4f : public DX::XMFLOAT4X4
{
public:
	Matrix4f();

	Matrix4f(float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33);

	Matrix4f(const DX::CXMMATRIX& M);

	explicit Matrix4f(_In_reads_(16) const float *pArray) : DX::XMFLOAT4X4(pArray) {}

	explicit Matrix4f(const Vector3f& r0, const Vector3f& r1, const Vector3f& r2);

	explicit Matrix4f(const Vector4f& r0, const Vector4f& r1, const Vector4f& r2, const Vector4f& r3);

	operator DX::XMMATRIX() const { return XMLoadFloat4x4(this); }

	// Comparison operators
	bool operator == (const Matrix4f& M) const;
	bool operator != (const Matrix4f& M) const;

	// Assignment operators
	Matrix4f& operator= (const Matrix4f& M);
	Matrix4f& operator+= (const Matrix4f& M);
	Matrix4f& operator-= (const Matrix4f& M);
	Matrix4f& operator*= (const Matrix4f& M);
	Matrix4f& operator*= (float S);
	Matrix4f& operator/= (float S);

	// Element-wise divide
	Matrix4f& operator/= (const Matrix4f& M);

	// Urnary operators
	Matrix4f operator+ () const;
	Matrix4f operator- () const;

	// Properties
	Vector3f up() const;
	void up(const Vector3f& v);

	Vector3f down() const;
	void down(const Vector3f& v);

	Vector3f right() const;
	void right(const Vector3f& v);

	Vector3f left() const;
	void left(const Vector3f& v);

	Vector3f forward() const;
	void forward(const Vector3f& v);

	Vector3f backward() const;
	void backward(const Vector3f& v);

	Vector3f translation() const;
	void translation(const Vector3f& v);

	// Matrix4f operations
	bool decompose(Vector3f& scale, Quaternionf& rotation, Vector3f& translation);

	Matrix4f transpose() const;
	void transpose(Matrix4f& result) const;

	Matrix4f invert() const;
	void invert(Matrix4f& result) const;

	float determinant() const;
};

// Binary operators
Matrix4f operator+ (const Matrix4f& M1, const Matrix4f& M2);
Matrix4f operator- (const Matrix4f& M1, const Matrix4f& M2);
Matrix4f operator* (const Matrix4f& M1, const Matrix4f& M2);
Matrix4f operator* (const Matrix4f& M, float S);
Matrix4f operator/ (const Matrix4f& M, float S);
Matrix4f operator/ (const Matrix4f& M1, const Matrix4f& M2);

// Element-wise divide
Matrix4f operator* (float S, const Matrix4f& M);

// Static functions
Matrix4f identity();

Matrix4f matrix_translation(const Vector3f& position);
Matrix4f matrix_translation(float x, float y, float z);

Matrix4f matrix_scale(const Vector3f& scales);
Matrix4f matrix_scale(float xs, float ys, float zs);
Matrix4f matrix_scale(float scale);

Matrix4f matrix_rotate_x(float radians);
Matrix4f matrix_rotate_y(float radians);
Matrix4f matrix_rotate_z(float radians);

Matrix4f matrix_from_axis_angle(const Vector3f& axis, float angle);

Matrix4f matrix_perspective_xfov_RH(float fov, float aspectRatio, float nearPlane, float farPlane);
Matrix4f matrix_perspective_RH(float width, float height, float nearPlane, float farPlane);
Matrix4f matrix_perspective_off_center_RH(float left, float right, float bottom, float top, float nearPlane, float farPlane);
Matrix4f matrix_orthographic(float width, float height, float zNearPlane, float zFarPlane);
Matrix4f matrix_orthographic_off_center(float left, float right, float bottom, float top, float zNearPlane, float zFarPlane);

Matrix4f matrix_look_at(const Vector3f& position, const Vector3f& target, const Vector3f& up);
Matrix4f createWorld(const Vector3f& position, const Vector3f& forward, const Vector3f& up);

Matrix4f createFromQuaternion(const Quaternionf& quat);

Matrix4f createFromYawPitchRoll(float yaw, float pitch, float roll);

/*static Matrix4f CreateShadow(const Vector3f& lightDir, const Plane& plane);

static Matrix4f CreateReflection(const Plane& plane);*/

void matrix_lerp(const Matrix4f& M1, const Matrix4f& M2, float t, Matrix4f& result);
Matrix4f matrix_lerp(const Matrix4f& M1, const Matrix4f& M2, float t);

void matrix_transform(const Matrix4f& M, const Quaternionf& rotation, Matrix4f& result);
Matrix4f matrix_transform(const Matrix4f& M, const Quaternionf& rotation);

//
// Vector2f
//

void vector_transform(const Vector2f& v, const Matrix4f& m, Vector2f& result);
// Vector2f vector_transform(const Vector2f& v, const Matrix4f& m);
void vector_transform(_In_reads_(count) const Vector2f* varray, size_t count, const Matrix4f& m, _Out_writes_(count) Vector2f* resultArray);

void vector_transform(const Vector2f& v, const Matrix4f& m, Vector4f& result);
void vector_transform(_In_reads_(count) const Vector2f* varray, size_t count, const Matrix4f& m, _Out_writes_(count) Vector4f* resultArray);

void vector_transform_normal(const Vector2f& v, const Matrix4f& m, Vector2f& result);
// Vector2f vector_transform_normal(const Vector2f& v, const Matrix4f& m);
void vector_transform_normal(_In_reads_(count) const Vector2f* varray, size_t count, const Matrix4f& m, _Out_writes_(count) Vector2f* resultArray);

//
// Vector3f
//

void vector_transform(const Vector3f& v, const Matrix4f& m, Vector3f& result);
//Vector3f vector_transform(const Vector3f& v, const Matrix4f& m);
void vector_transform(_In_reads_(count) const Vector3f* varray, size_t count, const Matrix4f& m, _Out_writes_(count) Vector3f* resultArray);

void vector_transform(const Vector3f& v, const Matrix4f& m, Vector4f& result);
void vector_transform(_In_reads_(count) const Vector3f* varray, size_t count, const Matrix4f& m, _Out_writes_(count) Vector4f* resultArray);

void vector_transform_normal(const Vector3f& v, const Matrix4f& m, Vector3f& result);
//Vector3f vector_transform_normal(const Vector3f& v, const Matrix4f& m);
void vector_transform_normal(_In_reads_(count) const Vector3f* varray, size_t count, const Matrix4f& m, _Out_writes_(count) Vector3f* resultArray);

//
// Vector4f
//

void vector_transform(const Vector4f& v, const Matrix4f& m, Vector4f& result);
Vector4f vector_transform(const Vector4f& v, const Matrix4f& m);
void vector_transform(_In_reads_(count) const Vector4f* varray, size_t count, const Matrix4f& m, _Out_writes_(count) Vector4f* resultArray);


_DETAIL_END
_ENGINE_END

#include "Matrix4.inl"

#endif