#pragma once

#ifndef __QUATERNION_H__
#define __QUATERNION_H__

#include "Core.h"
#include "dxmathsfwd.h"
#include <DirectxMath.h>

_ENGINE_BEGIN
_DETAIL_BEGIN

namespace dx = DirectX;

//------------------------------------------------------------------------------
// Quaternionf
class Quaternionf : public dx::XMFLOAT4
{
public:
	Quaternionf();

	Quaternionf(float _x, float _y, float _z, float _w);

	Quaternionf(const Vector3f& v, float scalar);

	explicit Quaternionf(const Vector4f& v);

	explicit Quaternionf(_In_reads_(4) const float *pArray);

	Quaternionf(const dx::FXMVECTOR& V);

	operator dx::XMVECTOR();

	// Comparison operators
	bool operator == (const Quaternionf& q) const;
	bool operator != (const Quaternionf& q) const;

	// Assignment operators
	Quaternionf& operator= (const Quaternionf& q) { x = q.x; y = q.y; z = q.z; w = q.w; return *this; }
	Quaternionf& operator+= (const Quaternionf& q);
	Quaternionf& operator-= (const Quaternionf& q);
	Quaternionf& operator*= (const Quaternionf& q);
	Quaternionf& operator*= (float S);
	Quaternionf& operator/= (const Quaternionf& q);

	// Urnary operators
	Quaternionf operator+ () const { return *this; }
	Quaternionf operator- () const;

	// Quaternionf operations
	float length() const;
	float lengthSquared() const;

	void normalize();
	void normalize(Quaternionf& result) const;

	void conjugate();
	void conjugate(Quaternionf& result) const;

	void inverse(Quaternionf& result) const;

	float dot(const Quaternionf& Q) const;

	// Static functions
	static Quaternionf createFromAxisAngle(const Vector3f& axis, float angle);
	static Quaternionf createFromYawPitchRoll(float yaw, float pitch, float roll);
	static Quaternionf createFromRotationMatrix(const Matrix4f& M);

	static void lerp(const Quaternionf& q1, const Quaternionf& q2, float t, Quaternionf& result);
	static Quaternionf lerp(const Quaternionf& q1, const Quaternionf& q2, float t);

	static void slerp(const Quaternionf& q1, const Quaternionf& q2, float t, Quaternionf& result);
	static Quaternionf slerp(const Quaternionf& q1, const Quaternionf& q2, float t);

	static void concatenate(const Quaternionf& q1, const Quaternionf& q2, Quaternionf& result);
	static Quaternionf concatenate(const Quaternionf& q1, const Quaternionf& q2);
};


// Binary operators
Quaternionf operator+ (const Quaternionf& Q1, const Quaternionf& Q2);
Quaternionf operator- (const Quaternionf& Q1, const Quaternionf& Q2);
Quaternionf operator* (const Quaternionf& Q1, const Quaternionf& Q2);
Quaternionf operator* (const Quaternionf& Q, float S);
Quaternionf operator/ (const Quaternionf& Q1, const Quaternionf& Q2);
Quaternionf operator* (float S, const Quaternionf& Q);

//
// Vector2f
//

void vector_transform(const Vector2f& v, const Quaternionf& quat, Vector2f& result);
// Vector2f vector_transform(const Vector2f& v, const Quaternionf& quat);

//
// Vector3f
//

void vector_transform(const Vector3f& v, const Quaternionf& quat, Vector3f& result);
//Vector3f vector_transform(const Vector3f& v, const Quaternionf& quat);

//
// Vector4f
//

void vector_transform(const Vector2f& v, const Quaternionf& quat, Vector4f& result);
Vector4f vector_transform(const Vector2f& v, const Quaternionf& quat);

void vector_transform(const Vector3f& v, const Quaternionf& quat, Vector4f& result);
Vector4f Transform(const Vector3f& v, const Quaternionf& quat);

void vector_transform(const Vector4f& v, const Quaternionf& quat, Vector4f& result);
Vector4f vectr_transform(const Vector4f& v, const Quaternionf& quat);

_DETAIL_END
_ENGINE_END


#include "Quaternion.inl"

#endif