#pragma once

#ifndef MATHS_HEADERS_H
#define MATHS_HEADERS_H

#include "Const.h"
#include "MathsUtils.h"
#include "Vector.h"
#include "Sphere.h"
#include "Plane.h"
#include "Line.h"
#include "Intersections.h"

#endif