﻿#ifndef __BEZIER_CURVE_H__
#define __BEZIER_CURVE_H__

#include "libmathsfwd.h"
#include <vector>
#include <cassert>

namespace engine
{
	template < typename T >
	class MultiArray
	{
	public:
		typedef MultiArray<T> this_type;

		typedef T type;
		typedef T* pointer;
		typedef T& reference;
		typedef const T* const_pointer;
		typedef const T& const_reference;

		MultiArray(size_t rows, size_t cols) :
		m_rows(rows),
		m_cols(cols),
		m_data(rows*cols)
		{

		}

		const_reference operator() (size_t row, size_t col) const
		{
			assert( row < GetRowSize() || col < GetColSize() );
			return m_data[ GetColSize() * row + col ];
		}

		reference operator() (size_t row, size_t col)
		{
			return const_cast<reference>( static_cast<const this_type&>(*this)(row, col) );
		}

		size_t GetRowSize() const
		{
			return m_rows;
		}

		size_t GetColSize() const
		{
			return m_cols;
		}

		size_t m_rows;
		size_t m_cols;
		std::vector<T> m_data;
	};

	// Bezier curve of degree N of which follows the general eqation of 
	// B(t)= ∑_(i=0)^n (n¦i) * (1-t)^(n-i) * t^i * b_i
	// B'(t)= n * ∑_(i=0)^n-1 (n-1¦i) * (1-t)^(n-i-1) * t^i * (b_i_+_1 - b_i) first derivative
	class CBezierCurve
	{
	public:
		typedef std::vector<Vector3f> ctrlPointArray;
		typedef MultiArray<float> array_type;

		CBezierCurve(const ctrlPointArray& ctrlPoints);
		~CBezierCurve();

		// Calculates a position on the cuve from the value t [0,1]
		Vector3f GetPosition(float t) const;

		// Calculates a position on the derivative of the cuve from the value t [0,1]
		Vector3f GetFirstDerivative(float t) const;

		// number of control points - 1
		size_t GetDegree() const;

		// normal 
		// control points
		const Vector3f* GetControlPoints() const;
		Vector3f* GetControlPoints();
		size_t GetNumCtrlPoints() const;

		// 1st derivative
		// Derivative points
		const Vector3f* GetDerivedPoints() const;
		Vector3f* GetDerivedPoints();
		size_t GetNumDerCtrlPoints() const;

	private:
		ctrlPointArray m_pCtrlPoints;		// array of ctrl points
		ctrlPointArray m_pDerv1CtrlPoints;  // array of derived ctrl points for tangent
		size_t m_degree;					// Bezier curve to the nth degree (cubic, quadratic, etc)
		array_type m_pascal;				// 2d-array containing pascals triangle

	};


#include "Beziercurve.inl"


}



#endif




/*
template <typename T>
class dynamic_array
{
public:
dynamic_array(){};
dynamic_array(int rows, int cols)
{
for(int i=0; i<rows; ++i)
{
data_.push_back(std::vector<T>(cols));
}
}

// other ctors ....

inline std::vector<T> & operator[](int i) { return data_[i]; }

inline const std::vector<T> & operator[] (int i) const { return data_[i]; }

// other accessors, like at() ...

void resize(int rows, int cols)
{
data_.resize(rows);
for(int i = 0; i < rows; ++i)
data_[i].resize(cols);
}

// other member functions, like reserve()....

private:
std::vector<std::vector<T> > data_;  
};
*/