#pragma once
#ifndef __FIXED_ARRAY_H__
#define __FIXED_ARRAY_H__

#include "Unroller.h"

_ENGINE_BEGIN

template < size_t Size, typename Element >
class FixedArray
{	
public:
	typedef FixedArray<Size, Element> this_type;

	typedef Element type;
	typedef Element* pointer;
	typedef Element& reference;

	typedef const Element* const_pointer;
	typedef const Element& const_reference;

	typedef type data_impl[Size];

	static const size_t m_arraySize = Size;

	size_t GetSize() const
	{ 
		return m_arraySize;
	}

	size_t size() const
	{ 
		return m_arraySize;
	}

	const_reference operator[] (size_t pos) const
	{ 
		assert(pos < Size); return m_data[pos];
	}

	reference operator[] (size_t pos) 
	{ 
		return const_cast<reference>( static_cast<const this_type&>(*this)[pos] );
	}

	/** Return access to the data as a raw pointer. */
	const_pointer GetData() const
	{ 
		return &(*this)[0]; 
	}

	/** Return access to the data as a raw pointer. */
	pointer GetData() 
	{ 
		return const_cast<pointer>( static_cast<const this_type&>(*this).GetData() );
	}

protected:
	//-----------------------------------------------------------------------
	// Operator Functions
	//-----------------------------------------------------------------------
	// assignment :: Vector
	template < typename U >
	this_type & _AddAssign (const FixedArray<Size, U> & v1)
	{                           						
		AssignOpLoopUnroller<type, const U, AssignOpAdd>::Loop<m_arraySize>::eval( GetData(), v1.GetData() );                                  		
		return (*this);
	}

	template < typename U >
	this_type& _SubAssign (const FixedArray<Size, U> & v1)
	{
		AssignOpLoopUnroller<type, const U, AssignOpSub>::Loop<m_arraySize>::eval( GetData(), v1.GetData() );                                  		
		return (*this);
	}

	template < typename U >
	this_type & _MulAssign (const FixedArray<Size, U> & v1)
	{
		AssignOpLoopUnroller<type, const U, AssignOpMul>::Loop<m_arraySize>::eval( GetData(), v1.GetData() );                                  		
		return (*this);
	}

	// assignment :: scalar
	template < typename U >
	this_type& _MulAssign (const U& val)
	{
		ScalarOpLoopUnroller<type, const U, AssignOpMul>::Loop<m_arraySize>::eval( GetData(), val );                                  		
		return (*this);
	}

	template < typename U >
	this_type& _DivAssign (const U& val)
	{
		assert(val != 0);
		type inv = 1 / val;
		return _MulAssign(inv);
	}

protected:
	FixedArray() 
	{
		ScalarOpLoopUnroller<type, type, AssignOpAssign>::Loop<m_arraySize>::eval( GetData(), type(0) );
	}

protected:
	data_impl m_data;
};

//-----------------------------------------------------------------------
// Comparison Operators
//-----------------------------------------------------------------------
template < size_t S, typename T, typename U >
const bool operator == (const FixedArray<S, T>& v0, const FixedArray<S, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpEqual>::Loop<S>::eval( v0.GetData(), v1.GetData(), Const<T>::ROUNDING_ERROR() );  
}

template < size_t S, typename T, typename U >
const bool operator != (const FixedArray<S, T>& v0, const FixedArray<S, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpNotEqual>::Loop<S>::eval( v0.GetData(), v1.GetData(), Const<T>::ROUNDING_ERROR() );
}

template < size_t S, typename T, typename U >
const bool operator <= (const FixedArray<S, T>& v0, const FixedArray<S, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpLessEqual>::Loop<S>::eval( v0.GetData(), v1.GetData(), Const<T>::ROUNDING_ERROR() );
}

template < size_t S, typename T, typename U >
const bool operator >= (const FixedArray<S, T>& v0, const FixedArray<S, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpGreaterEqual>::Loop<S>::eval( v0.GetData(), v1.GetData(), Const<T>::ROUNDING_ERROR() ); 
}

template < size_t S, typename T, typename U >
const bool operator < (const FixedArray<S, T>& v0, const FixedArray<S, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpLessThan>::Loop<S>::eval( v0.GetData(), v1.GetData(), Const<T>::ROUNDING_ERROR() );  
}	

template < size_t S, typename T, typename U >
const bool operator > (const FixedArray<S, T>& v0, const FixedArray<S, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpGreaterThan>::Loop<S>::eval( v0.GetData(), v1.GetData(), Const<T>::ROUNDING_ERROR() ); 
}

//-----------------------------------------------------------------------
// Comparison Functions
//-----------------------------------------------------------------------
template < size_t S, typename T, typename U >
const bool IsEqual(const FixedArray<S, T>& v0, const FixedArray<S, U>& v1, const T& tolerance = Const<T>::ROUNDING_ERROR() )
{
	return CompareLoopUnroller<const T, const U, CompareOpEqual>::Loop<S>::eval( v0.GetData(), v1.GetData(), tolerance );  
}


_ENGINE_END







#endif