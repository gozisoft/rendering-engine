#pragma once

#ifndef __MATHS_FORWARD_H__
#define __MATHS_FORWARD_H__

#include "Core.h"
#include "libcmlfwd.h"
#include "cml/vector/types.h"
#include "cml/matrix/types.h"
#include "cml/quaternion/types.h"

_ENGINE_BEGIN

// ----------------------------------------------------------------------------------------
// Linear Algebra
// ----------------------------------------------------------------------------------------

// vector2 typedefs
typedef cml::vector2i Vector2i;
typedef cml::vector2f Vector2f;
typedef cml::vector2d Vector2d;

typedef Vector2i Point2i;
typedef Vector2f Point2f;
typedef Vector2d Point2d;

// vector3 typedefs
typedef cml::vector3i Vector3i;
typedef cml::vector3f Vector3f;
typedef cml::vector3d Vector3d;

typedef Vector3i Point3i;
typedef Vector3f Point3f;
typedef Vector3d Point3d;

// vector4 typedefs
typedef cml::vector4i Vector4i;
typedef cml::vector4f Vector4f;
typedef cml::vector4d Vector4d;

typedef Vector4i Point4i;
typedef Vector4f Point4f;
typedef Vector4d Point4d;

// dynamically resizable vectors 
typedef cml::vectori vectori;
typedef cml::vectorf vectorf;
typedef cml::vectord vectord;

// Quaternion typdef
typedef cml::quaternionf_n Quaternionf;
typedef cml::quaterniond_n Quaterniond;

#if defined (RE_USE_ROW_MAJOR)
// Matrix typedef, fixed size
typedef cml::matrix44i_r Matrix4i;
typedef cml::matrix44f_r Matrix4f;
typedef cml::matrix44d_r Matrix4d;

// Matrix typedef, dynamic size
typedef cml::matrixi_r Matrixi;
typedef cml::matrixf_r Matrixf;
typedef cml::matrixd_r Matrixd;
#else
// Matrix typedef, fixed size
typedef cml::matrix44i_c Matrix4i;
typedef cml::matrix44f_c Matrix4f;
typedef cml::matrix44d_c Matrix4d;

// Matrix typedef, dynamic size
typedef cml::matrixi_c Matrixi;
typedef cml::matrixf_c Matrixf;
typedef cml::matrixd_c Matrixd;
#endif

// Typedef for colour class.
typedef Vector4f Colour4f;

// ----------------------------------------------------------------------------------------
// Curves
// ----------------------------------------------------------------------------------------
class CBezierCurve;
class CPieceWiseBezier;

// ----------------------------------------------------------------------------------------
// Collision
// ----------------------------------------------------------------------------------------

// Typedefs for 3d lines
typedef cml::line3f Line3f;
typedef cml::line3d Line3d;

// Typedefs for 4d lines
typedef cml::line4f Line4f;
typedef cml::line4d Line4d;

// Typedefs for 3d planes
typedef cml::plane3f Plane3f;
typedef cml::plane3d Plane3d;

// Typedefs for 4d planes
typedef cml::plane4f Plane4f;
typedef cml::plane4d Plane4d;

// Typedefs for 3d spheres
typedef cml::sphere3f Sphere3f;
typedef cml::sphere3d Sphere3d;

// Typedefs for 4d spheres
typedef cml::sphere4f Sphere4f;
typedef cml::sphere4d Sphere4d;

_ENGINE_END


#endif


//// Viewport typedefs
//typedef Viewport<int> Viewporti;
//typedef Viewport<long> Viewportl;
//typedef Viewport<float> Viewportf;
//typedef Viewport<double> Viewportd;
//typedef Viewport<unsigned int> Viewportu;


//// Custom matrix class forward declare.
//template <typename Element, typename Storage, typename BasisOrient, typename Layout> class Matrix;
//
//// Typedef for fixed size matrix.
//typedef Matrix< int, cml::fixed<4, 4>, cml::row_basis, cml::row_major > matrix44i_r;
//typedef Matrix< int, cml::fixed<4, 4>, cml::col_basis, cml::col_major > matrix44i_c;
//typedef Matrix< float, cml::fixed<4, 4>, cml::row_basis, cml::row_major > matrix44f_r;
//typedef Matrix< float, cml::fixed<4, 4>, cml::col_basis, cml::col_major > matrix44f_c;
//typedef Matrix< double, cml::fixed<4, 4>, cml::row_basis, cml::row_major > matrix44d_r;
//typedef Matrix< double, cml::fixed<4, 4>, cml::col_basis, cml::col_major > matrix44d_c;
//
//// Typedef for dynamic size matrix.
//typedef Matrix< int, cml::dynamic<>, cml::row_basis, cml::row_major > matrixi_r;
//typedef Matrix< int, cml::dynamic<>, cml::col_basis, cml::col_major > matrixi_c;
//typedef Matrix< float, cml::dynamic<>, cml::row_basis, cml::row_major > matrixf_r;
//typedef Matrix< float, cml::dynamic<>, cml::col_basis, cml::col_major > matrixf_c;
//typedef Matrix< double, cml::dynamic<>, cml::row_basis, cml::row_major > matrixd_r;
//typedef Matrix< double, cml::dynamic<>, cml::col_basis, cml::col_major > matrixd_c;