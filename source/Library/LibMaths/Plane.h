#pragma once
#ifndef __PLANE_H__
#define __PLANE_H__

#include "libmathsfwd.h"
#include "cml/vector.h"
#include <stdexcept>
#include <utility>

namespace cml
{
	template < typename T, size_t N = 3 >
	class plane 
	{
	public:
		// Simple type defs
		typedef plane< T, N > my_type;
		typedef vector< T, fixed<N> > vector_type;
		typedef T value_type;

		// Uninitalised
		plane()	: m_disOrigin( T(0) ) { /**/ }

		// Copy Ctor
		plane(const plane& other)
			: m_normal(other.m_normal),
			m_disOrigin(other.m_disOrigin)
		{
		}

		plane(plane&& other)
			: m_normal(std::move(other.m_normal)),
			m_disOrigin(std::move(other.m_disOrigin))
		{
		}

		plane(const vector_type& normal, const vector_type& point)
			:
			m_normal(normal),
			m_disOrigin( -dot(normal, point) )
		{
		}

		plane(const vector_type& point1, const vector_type& point2, const vector_type& point3)
			:
			m_normal( normalize( cross(point2 - point1, point3 - point1) ) ),
			m_disOrigin( -dot(m_normal, point1) )
		{
		}

		// Assignment
		plane& operator=(const plane& other)
		{
			if (this == &other)
				return *this;
			m_normal = other.m_normal;
			m_disOrigin = other.m_disOrigin;
			return *this;
		}

		plane& operator=(plane&& other)
		{
			if (this == &other)
				return *this;
			m_normal = std::move(other.m_normal);
			m_disOrigin = std::move(other.m_disOrigin);
			return *this;
		}

		// Normalise the plane normal and its distance to origin
		void normalise()
		{
			T recipial =  T(1) / m_normal.length();
			m_normal *= recipial;
			m_disOrigin *= recipial;
		}

		// plane normal vector
		vector_type	m_normal;		

		// distance to origin
		value_type m_disOrigin;		
	};

	// The signed distance of Q to the plane is given by just returning the computed value of t
	// Calculate distance to point. Plane normal must be normalized.
	// [From |n.p = d| => |n.p - d = 0|]
	template < typename T, size_t N >
	T scalar_distance(const plane<T,N>& plane, const vector< T, fixed<N> >& point)
	{		
#if defined (DEBUG) || defined (_DEBUG) 
		// Check normal vector is normailsed, magnitude should be equal to one
		if( length(plane.m_normal) == T(1) )
			throw std::domain_error("Plane is not normalised [distance]");
#endif
		return dot(plane.m_normal, point) - plane.m_disOrigin; 
	}

	// The code for computing the closest point on the plane to a point
	template < typename T, size_t N >
	T point_distance(const plane<T,N>& plane, const vector< T, fixed<N> >& point)
	{
#if defined (DEBUG) || defined (_DEBUG)
		// Check normal vector is normailsed, magnitude should be equal to one
		if( length(plane.m_normal) == T(1) )
			throw std::domain_error("Plane is not normalised [distance]");
#endif
		T scalardistance = scalar_distance(plane, point);	
		return point - scalardistance * plane.m_normal; 
	}

	// Normalise a plane
//	template < typename T, size_t N >
//	plane<T,N> normalise(const plane<T, N>& plane)
//	{
//        typedef plane<T,N> plane_type;
//
//        plane_type newPlane = plane;
//        newPlane.normalise();
//		return newPlane;
//	}
}

#endif

