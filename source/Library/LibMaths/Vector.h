#pragma once

#ifndef __VECTOR_H__
#define __VECTOR_H__

#include "libmathsfwd.h"
#include "MathsUtils.h"

#include "cml/vector.h"
#include "cml/mathlib/mathlib.h"
#include <initializer_list>

_ENGINE_BEGIN

//////////////////////////////////////////////////////////////////////////////
// Fixed RGBA functions
//////////////////////////////////////////////////////////////////////////////
template < typename T >
inline T white_rgba() {
	return std::initializer_list<T>(1, 1, 1, 1);
}

template<typename T>
inline T black_rgba() {
	return T(0, 0, 0, 0);
}

template<typename T>
inline T red_rgba() {
	return T(1, 0, 0, 0);
}

template<typename T>
inline T green_rgba() {
	return T(0, 1, 0, 0);
}

template<typename T>
inline T blue_rgba() {
	return T(0, 0, 1, 0);
}

template<typename T>
inline T yellow_rgba() {
	return T(1, 1, 0, 1);
}

template<typename T>
inline T cyan_rgba() {
	return T(0, 1, 1, 1);
}

template<typename T>
inline T magenta_rgba() {
	return T(1, 0, 1, 1);
}

template<typename T>
inline T alpha_rgba() {
	return T(0, 0, 0, 1);
}

////////////////////////////////////////////////////////////////////////////
// Additional comparison functions for both points and vectors
//////////////////////////////////////////////////////////////////////////////

template <typename T>
inline bool less_than_zero(const T* data, size_t dimens)
{
	for (size_t i = 0; i < dimens; ++i)
	{
		if (!LessThanZero(data[i]))
		{
			// If any value is greater than zero, we return false.
			return false;
		}
	}
	return true;
}

template <typename T>
inline bool less_than_or_is_zero(const T* data, size_t dimens)
{
	for (size_t i = 0; i < dimens; ++i)
	{
		if (!LessThanOrEqualIsZero(data[i]))
		{
			// If any value is greater than zero, we return false.
			return false;
		}
	}
	return true;
}

template <typename T>
inline bool is_zero(const T* data, size_t dimens)
{
	for (size_t i = 0; i < dimens; ++i)
	{
		if (!IsZero(data[i]))
		{
			// If any value is greater than zero, we return false.
			return false;
		}
	}
	return true;
}

template <typename T>
inline bool greater_than_or_is_zero(const T* data, size_t dimens)
{
	for (size_t i = 0; i < dimens; ++i)
	{
		if (!GreaterThanOrEqualIsZero(data[i]))
		{
			// If any value is greater than zero, we return false.
			return false;
		}
	}
	return true;
}

template <typename T>
inline bool greater_than_zero(const T* data, size_t dimens)
{
	for (size_t i = 0; i < dimens; ++i)
	{
		if (!GreaterThanZero(data[i]))
		{
			// If any value is greater than zero, we return false.
			return false;
		}
	}
	return true;
}

_ENGINE_END


#endif



//namespace cml 
//{
//	namespace E = Engine;
//
//	//////////////////////////////////////////////////////////////////////////////
//	// N-d functions
//	//////////////////////////////////////////////////////////////////////////////
//
//	/** Return an N-d cardinal axis by index */
//	template < size_t N >
//	inline vector< double, fixed<N> > channel(size_t i)
//	{
//		return axis<N>(i);
//	}
//
//	//////////////////////////////////////////////////////////////////////////////
//	// Fixed RGB functions
//	//////////////////////////////////////////////////////////////////////////////
//
//	inline vector< double, fixed<3> > white_rgb()
//	{
//		typedef vector< double, fixed<3> > vector_type;
//		return vector_type(1, 1, 1);
//	}
//
//	inline vector< double, fixed<3> > black_rgb()
//	{
//		return zero<3>();
//	}
//
//	inline vector< double, fixed<3> > red_rgb() 
//	{
//		return channel<3>(0);
//	}
//
//	inline vector< double, fixed<3> > green_rgb()
//	{
//		return channel<3>(1);
//	}
//
//	inline vector< double, fixed<3> > blue_rgb() 
//	{
//		return channel<3>(2);
//	}
//
//	inline vector< double, fixed<3> > yellow_rgb() 
//	{
//		typedef vector< double, fixed<3> > vector_type;
//		return vector_type(1, 1, 0);
//	}
//
//	inline vector< double, fixed<3> > cyan_rgb() 
//	{
//		typedef vector< double, fixed<3> > vector_type;
//		return vector_type(0, 1, 1);
//	}
//
//	inline vector< double, fixed<3> > magenta_rgb()
//	{
//		typedef vector< double, fixed<3> > vector_type;
//		return vector_type(1, 0, 1);
//	}
//
//	//////////////////////////////////////////////////////////////////////////////
//	// Fixed RGBA functions
//	//////////////////////////////////////////////////////////////////////////////
//
//	inline vector< double, fixed<4> > channel_rgba(size_t i)
//	{
//		return axis<4>(i);
//	}
//
//	inline vector< double, fixed<4> > white_rgba() 
//	{
//		typedef vector< double, fixed<4> > vector_type;
//		return vector_type(1, 1, 1, 1);
//	}
//
//	inline vector< double, fixed<4> > black_rgba()
//	{
//		return zero<4>();
//	}
//
//	inline vector< double, fixed<4> > red_rgba() 
//	{
//		return channel_rgba(0);
//	}
//
//	inline vector< double, fixed<4> > green_rgba()
//	{
//		return channel_rgba(1);
//	}
//
//	inline vector< double, fixed<4> > blue_rgba() 
//	{
//		return channel_rgba(2);
//	}
//
//	inline vector< double, fixed<4> > yellow_rgba()
//	{
//		typedef vector< double, fixed<4> > vector_type;
//		return vector_type(1, 1, 0, 1);
//	}
//
//	inline vector< double, fixed<4> > cyan_rgba()
//	{
//		typedef vector< double, fixed<4> > vector_type;
//		return vector_type(0, 1, 1, 1);
//	}
//
//	inline vector< double, fixed<4> > magenta_rgba() 
//	{
//		typedef vector< double, fixed<4> > vector_type;
//		return vector_type(1, 0, 1, 1);
//	}
//
//	inline vector< double, fixed<4> > alpha_rgba()
//	{
//		return channel_rgba(3);
//	}
//
//	//////////////////////////////////////////////////////////////////////////////
//	// Additional affine point functions
//	//////////////////////////////////////////////////////////////////////////////
//
//	// euclidean distance
//	template<typename E, class AT>
//	inline typename vector<E,AT>::value_type distance(const vector<E,AT>& v1, const vector<E,AT>& v2)
//	{
//		return length(v1-v2);
//	}
//
//	// euclidean distance squared (again, for comparisons)
//	template<typename E, class AT>
//	inline typename vector<E,AT>::value_type distance_squared(const vector<E,AT>& v1, const vector<E,AT>& v2)
//	{
//		return length_squared(v1-v2);
//	}
//
//	//////////////////////////////////////////////////////////////////////////////
//	// Additional comparison functions for both points and vectors
//	//////////////////////////////////////////////////////////////////////////////
//
//	template< typename E1, class AT1 >
//	inline bool less_than_zero (const vector<E1,AT1>& v0)
//	{
//		typedef vector<E1,AT1> vector_type;
//		typedef typename vector_type::value_type value_type;
//
//		return E::IsTrueLoopUnroller::Loop<
//			vector_type::dimension
//		>::eval(&v0[0], E::LessThanZero<value_type>);  
//	}
//
//	template< typename E1, class AT1 >
//	inline bool less_than_or_is_zero (const vector<E1,AT1>& v0)
//	{
//		typedef vector<E1,AT1> vector_type;
//		typedef typename vector_type::value_type value_type;
//
//		return E::IsTrueLoopUnroller::Loop<
//			vector_type::dimension
//		>::eval(&v0[0], E::LessThanOrEqualIsZero<value_type>);  
//	}
//
//	template< typename E1, class AT1 >
//	inline bool is_zero (const vector<E1,AT1>& v0)
//	{
//		typedef vector<E1,AT1> vector_type;
//		typedef typename vector_type::value_type value_type;
//
//		return E::IsTrueLoopUnroller::Loop<
//			vector_type::dimension
//		>::eval(&v0[0], E::IsZero<value_type>);
//	}
//
//	template< typename E1, class AT1 >
//	inline bool greater_than_or_is_zero (const vector<E1,AT1>& v0)
//	{
//		typedef vector<E1,AT1> vector_type;
//		typedef typename vector_type::value_type value_type;
//
//		return E::IsTrueLoopUnroller::Loop<
//			vector_type::dimension
//		>::eval(&v0[0], E::GreaterThanOrEqualIsZero<value_type>);  
//	}
//
//	template< typename E1, class AT1 >
//	inline bool greater_than_zero (const vector<E1,AT1>& v0)
//	{
//		typedef vector<E1,AT1> vector_type;
//		typedef typename vector_type::value_type value_type;
//
//		return E::IsTrueLoopUnroller::Loop<
//			vector_type::dimension
//		>::eval(&v0[0], E::GreaterThanZero<value_type>);  
//	}
//
//} // namespace cml