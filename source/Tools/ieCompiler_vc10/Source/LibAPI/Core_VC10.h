#pragma once
#ifndef __CORE_VC10_H__
#define __CORE_VC10_H__


#if defined(COMPILER_MSVC_10) || defined(COMPILER_MSVC_10)

	//-------------------------------------------------------------------------------------------
	// STL Types and headers
	//-------------------------------------------------------------------------------------------
	#if defined(USING_STL) || defined(_USING_STL)
		// limts header for types
		#include <limits>

		// C++ Common STL headers	
		#include <exception>
		#include <vector>
		#include <queue>
		#include <deque>
		#include <list>
		#include <map>
		#include <hash_map>
		#include <set>
		#include <hash_set>
		#include <bitset>
		#include <utility>
		#include <iterator>
		#include <algorithm> // std::transform etc

		// String Types, with support for streaming (wchar_t, char)
		#include <fstream>
		#include <sstream>
		#include <istream> // for std::basic_istream
		#include <ostream> // for std::basic_ostream
		#include <locale>  // for converting to UTF-8 format
		#include <string>

		//-------------------------------------------------------------------------------------------
		// VS has wchar types of the STL
		//-------------------------------------------------------------------------------------------
		#if defined (UNICODE) || defined(_UNICODE) // Use unicode versions of Microsoft API
			typedef std::wstring _StringBase;
			typedef std::wstringstream _StringStreamBase;
			typedef std::wostringstream _OutputStringStreamBase;
			typedef std::wistringstream _InputStringStreamBase;
			typedef std::wifstream _InputFileStreamBase;
			typedef std::wofstream _OutputFileStreamBase;
			typedef std::wostream _OutputStream;
			typedef std::wistream _InputStream;
		#else
			typedef std::string _StringBase;
			typedef std::stringstream _StringStreamBase;
			typedef std::ostringstream _OutputStringStreamBase;
			typedef std::istringstream _InputStringStreamBase;
			typedef std::ifstream _InputFileStreamBase;
			typedef std::ofstream _OutputFileStreamBase;
			typedef std::ostream _OutputStream;
			typedef std::istream _InputStream;
		#endif

	#endif

	//-------------------------------------------------------------------------------------------
	// C99 standard types and headers
	//-------------------------------------------------------------------------------------------
	#if defined(USING_C99) || defined(_USING_C99)
	
		#if defined (USING_STL) || defined (USING_STL)
			// You don't need climits if you have limits included		
		#else
			#include <climits>
		#endif

		// C++ 99 standard
		#include <cassert>
		#include <cstring>
		#include <cstdio>
		#include <cstdlib>
		#include <cctype> // tolower function
	#endif	

	//-------------------------------------------------------------------------------------------
	// Basic Types	(Integers, Chars)
	//-------------------------------------------------------------------------------------------
	typedef __int8					Int8;
	typedef __int16					Int16;
	typedef __int32					Int32;
	typedef __int64					Int64;
	typedef unsigned __int8		    UInt8;
	typedef unsigned __int16	    UInt16;
	typedef unsigned __int32	    UInt32;
	typedef unsigned __int64        UInt64;

	typedef short					Short;
	typedef long					Long;
	typedef long long				LLong;

	typedef unsigned short			UShort;
	typedef unsigned long			ULong;
	typedef unsigned long long		ULLong;
			
	#ifdef _WIN64 
	typedef UInt64    UInt;
	typedef Int64     Int;
	const UInt        _UINT_MAX     = _UI64_MAX;
	const Int         _INT_MAX      = _I64_MAX;
	const Int         _INT_MIN      = _I64_MIN;
	#else
	typedef UInt32    UInt;
	typedef Int32     Int;
	const UInt        _UINT_MAX     = _UI32_MAX;
	const Int         _INT_MAX      = _I32_MAX;
	const Int         _INT_MIN      = _I32_MIN;
	#endif

	// Integer types capable of holding object pointers
	#if defined (WIN64) || defined(_WIN64) 
	typedef __int64	 IntPtr;
	typedef unsigned __int64  UIntPtr;
	#else 
	typedef _W64 int IntPtr;
	typedef _W64 unsigned int UIntPtr;
	#endif // WIN64

	// Use unicode versions of Microsoft API
	#if defined (UNICODE) || defined(_UNICODE)
		typedef wchar_t Char;
		#define ieS__(s) L##s		// string macro - used in conjunction with UNICODE and USING_WIDE_CHAR.  
		#define ieS(s) ieS__(s)		
	#else
		typedef char Char;
		#define ieS(s) (s) 			// string macro - used in conjunction with ASCII and NOT_USING_WIDE_CHAR.    							
	#endif // UNICODE

	// Main char typedef (wchar_t, char)
	typedef Char* pChar;
	typedef const pChar Const_pChar;

	//-------------------------------------------------------------------------------------------
	// Includes for C++ 0x std::shared_ptr and more, this will change in future
	//-------------------------------------------------------------------------------------------
	#if defined(HAS_TR1) || defined(_HAS_TR1)

		// includes the TR1 std::shared_ptr, alternative to boost.
		#include <memory> 
		#include <functional>
		#include <regex>

	#endif // _HAS_TR1


#endif // COMPILER_MSVC_10




#endif // __CORE_VC10_H__