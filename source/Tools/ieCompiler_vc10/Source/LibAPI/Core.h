#ifndef __CORE_H__
#define __CORE_H__

#if defined(WIN32) || defined(_WIN32)
#ifndef _WIN32
#define _WIN32
#endif
#ifndef WIN32
#define WIN32
#endif
#define LITTLE_ENDIAN
#elif defined(WIN64) || defined(_WIN64)
#ifndef _WIN64
#define _WIN64
#endif
#ifndef WIN64
#define WIN64
#endif
#define LITTLE_ENDIAN
#endif

#if defined(_MSC_VER) || defined(MSC_VER)

	#if _MSC_VER > 1000
	#pragma once
	#endif

    #if (_MSC_VER >= 1600 && _MSC_VER < 1700) 
        // Define to indicate the compiler is MS Visual Studio Version 10.                                                         
        #define COMPILER_MSVC_10
		#define USING_C99
		#define USING_STL
		#define USING_BOOST
        #include "Core_VC10.h"
	#elif (_MSC_VER >= 1500 && _MSC_VER < 1600) 
	    // Define to indicate the compiler is MS Visual Studio Version 9.                                                         
        #define COMPILER_MSVC_9
		#define USING_C99
		#define USING_STL
		//#define USING_BOOST
        #include "Core_VC9.h"
	#elif (_MSC_VER >= 1400 && _MSC_VER < 1500) 
	    // Define to indicate the compiler is MS Visual Studio Version 9.                                                         
        #define COMPILER_MSVC_8
		#define USING_BOOST
		#define USING_BASIC_TYPES
		#define USING_STREAMS
        #include "Core_VC8.h"
	#endif

#endif

// #define USING_BOOST
// Place boost headers that you want to use in here, this will disable any TR1 use
#ifdef USING_BOOST

	#if defined(HAS_TR1) || defined(_HAS_TR1)
		#define BOOST_HAS_TR1
	#endif

	#include <boost/config.hpp>

	// This will include the multi index capabability of boost
	#include <boost/multi_index_container.hpp>
	#include <boost/multi_index/mem_fun.hpp>
	#include <boost/multi_index/composite_key.hpp>
	#include <boost/multi_index/member.hpp>

	// These do have fwd declares, but I have no idea how to use them
	#include <boost/multi_index/hashed_index.hpp>
	#include <boost/multi_index/sequenced_index.hpp>

	// The boost varient
	#include <boost/variant.hpp>

#endif

// Removes warnings for unused paramaters
#define UNUSED_PARAMETER(P) (P)

// Enables extra asserts for the BufferIO class
#if defined(DEBUG) || defined(_DEBUG)
	#define BUFFERIO_VALIDATE_OPERATION
#endif

// namespace Engine defines
#define _ENGINE_BEGIN namespace Engine {
#define _ENGINE_END	  }
#define _ENGINE		  ::Engine::

//------------------------------------------------------------------------------
// Basic Storage Type Declares (Arraytors)
//------------------------------------------------------------------------------
#if defined(USING_STL) || defined(_USING_STL)

// Stream typedefs
extern _OutputStream &Cout;
extern _InputStream &Cin;

// Vector typedefs
typedef std::vector<Int8>  _Int8Vector;
typedef std::vector<Int16> _Int16Vector;
typedef std::vector<Int32> _Int32Vector;
typedef std::vector<Int64> _Int64Vector;
typedef std::vector<Int>   _IntVector;

typedef std::vector<UInt8>  _UInt8Vector;
typedef std::vector<UInt16> _UInt16Vector;
typedef std::vector<UInt32> _UInt32Vector;
typedef std::vector<UInt64> _UInt64Vector;
typedef std::vector<UInt>   _UIntVector;

typedef std::vector<float>  _FloatVector;
typedef std::vector<double> _DoubleVector;

_ENGINE_BEGIN

	//------------------------------------------------------------------------------
	// Final typedefs with compatibility for wchar_t and char
	//------------------------------------------------------------------------------
	typedef _StringBase String;
	typedef _StringStreamBase StringStream;
	typedef _OutputStringStreamBase OStringStream;
	typedef _InputStringStreamBase IStringStream;
	typedef _InputFileStreamBase IfStream;
	typedef _OutputFileStreamBase OfStream;
	typedef _OutputStream OStream;
	typedef _InputStream IStream;

	// Vector typedefs
	typedef _Int8Vector Int8Vector;
	typedef _Int16Vector Int16Vector;
	typedef _Int32Vector Int32Vector;
	typedef _Int64Vector Int64Vector;
	typedef _IntVector IntVector;

	typedef _UInt8Vector UInt8Vector;
	typedef _UInt16Vector UInt16Vector;
	typedef _UInt32Vector UInt32Vector;
	typedef _UInt64Vector UInt64Vector;
	typedef _UIntVector UIntVector;

	typedef _FloatVector FloatVector;
	typedef _DoubleVector DoubleVector;

_ENGINE_END

#endif


//#define USING_MEMORY_MGR
#include "MemoryMgr.h"
#include "Helper.h"



#endif