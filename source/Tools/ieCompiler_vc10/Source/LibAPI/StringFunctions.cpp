#include "apiafx.h"
#include "StringFunctions.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <direct.h>
#include <tchar.h>


_ENGINE_BEGIN


String GetFullPath(const Char* path)
{
	// get absolute path
	Char fullpath[_MAX_PATH];
	String pathName = _tfullpath(&fullpath[0], path, _MAX_PATH);

	for (size_t i = 0; i < pathName.length(); ++i)
	{
		if ( pathName[i] == ieS('\\') )
			pathName[i] = ieS('/');
	}

	return pathName;
}

String GetWorkingDirectory(const Char* path)
{
	Char* pBuffer = 0;

	// Passing NULL as the buffer forces getcwd to allocate
	// memory for the path, which allows the code to support file paths
	// longer than _MAX_PATH, which are supported by NTFS.

	// Get the current working directory: 
	if( (pBuffer = _tgetcwd( NULL, 0 )) == NULL )
	{
		_tperror( ieS("_tperror error") );
		return String();
	}
	else
	{
		StringStream oss;
		oss << pBuffer << ieS('/') << path;
		String pathName = oss.str();

		for (size_t i = 0; i < pathName.length(); ++i)
		{
			if ( pathName[i] == ieS('\\') )
				pathName[i] = ieS('/');
		}

		return pathName;
	}
}

std::wstring Widen(const std::string& str)
{
	using std::wostringstream;
	wostringstream wstm;

	using std::ctype;
	using std::use_facet;
	const ctype<wchar_t>& ctfacet = use_facet< ctype<wchar_t> >( wstm.getloc() );

	for( size_t i=0 ; i<str.size() ; ++i ) 
	{
		wstm << ctfacet.widen( str[i] );
	}

	return wstm.str();
}

std::string Narrow(const std::wstring& str)
{
	using std::ostringstream;
	ostringstream stm;

	using std::ctype;
	using std::use_facet;
	const ctype<char>& ctfacet = use_facet< ctype<char> >( stm.getloc() ) ;

	for( size_t i=0 ; i<str.size() ; ++i ) 
	{
		stm << ctfacet.narrow( str[i], 0 ) ;
	}

	return stm.str() ;
}

int AnsiToWide(wchar_t* dest, const char* src, int inputLen, int outputLen)
{
	if( dest == 0 || src == 0 || outputLen < 1 )
		return 1;

	int result = MultiByteToWideChar(CP_ACP, 0, src, inputLen, dest, outputLen);

	if(result == 0)
		return 1;

	return 0;
}

int WideToAnsi(char* dest, const wchar_t* src, int inputLen, int outputLen)
{
	if( dest == 0 || src == 0 || outputLen < 1 )
		return 1;

	int result = WideCharToMultiByte(CP_UTF8, 0, src, inputLen, dest, outputLen, 0, 0);

	if( result == 0 )
		return 1;

	return 0;
}

int GenericToAnsi(char* dest, const Char* src, int inputLen, int outputLen)
{
	if( dest == 0 || src == 0 || outputLen < 1 )
		return 1;

#if defined(_UNICODE) | defined(UNICODE)
	return WideToAnsi(dest, src, inputLen, outputLen);
#else
	strncpy(dest, src, outputLen);

	if (inputLen < 1)
		dest[outputLen-1] = '\0';

	return 0;
#endif   
}

int AnsiToGeneric(Char* dest, const char* src, int inputLen, int outputLen)
{
	if( dest == 0  || src == 0 || outputLen < 1 )
		return 1;

#if defined(_UNICODE) | defined(UNICODE)
	return AnsiToWide( dest, src, inputLen, outputLen );
#else
	strncpy(dest, src, outputLen);

	if (inputLen < 1)
		dest[outputLen-1] = '\0';

	return 0;
#endif    
}


CHashedString::CHashedString(const char* pIdentString) : 
m_ident( hash_name( pIdentString ) ),
m_identStr(pIdentString)
{

}



void* CHashedString::hash_name(const char* pIdentStr)
{
	// Relatively simple hash of arbitrary text string into a
	// 32-bit identifier Output value is
	// input-valid-deterministic, but no guarantees are made
	// about the uniqueness of the output per-input
	//
	// Input value is treated as lower-case to cut down on false
	// separations cause by human mistypes. Sure, it could be
	// construed as a programming error to mix up your cases, and
	// it cuts down on permutations, but in Real World Usage
	// making this text case-sensitive will likely just lead to
	// Pain and Suffering.
	//
	// This code lossely based upon the adler32 checksum by Mark
	// Adler and published as part of the zlib compression
	// library sources.

	// largest prime smaller than 65536
	ULong BASE = 65521L;

	// NMAX is the largest n such that 255n(n+1)/2 +
	// (n+1)(BASE-1) <= 2^32-1
	ULong NMAX = 5552;

#define DO1(buf,i)  {s1 += std::tolower(buf[i]); s2 += s1;}
#define DO2(buf,i)  DO1(buf,i); DO1(buf,i+1);
#define DO4(buf,i)  DO2(buf,i); DO2(buf,i+2);
#define DO8(buf,i)  DO4(buf,i); DO4(buf,i+4);
#define DO16(buf)   DO8(buf,0); DO8(buf,8);

	if (pIdentStr == NULL)
		return NULL;

	ULong s1 = 0;
	ULong s2 = 0;

	for ( size_t len = std::strlen( pIdentStr ); len > 0 ; )
	{
		ULong k = len < NMAX ? len : NMAX;

		len -= k;

		while (k >= 16)
		{
			DO16(pIdentStr);
			pIdentStr += 16;
			k -= 16;
		}

		if (k != 0) do
		{
			s1 += std::tolower( *pIdentStr++ );
			s2 += s1;
		} while (--k);

		s1 %= BASE;
		s2 %= BASE;
	}

#pragma warning(push)
#pragma warning(disable : 4312)

	return reinterpret_cast<void*>( (s2 << 16) | s1 );

#pragma warning(pop)
#undef DO1
#undef DO2
#undef DO4
#undef DO8
#undef DO16

}


_ENGINE_END







	/*
	std::wstring widen(const std::string& s, std::locale loc)
	{
	std::char_traits<wchar_t>::state_type state = { 0 };

	typedef std::codecvt<wchar_t, char, std::char_traits<wchar_t>::state_type >
	ConverterFacet;

	ConverterFacet const& converter(std::use_facet<ConverterFacet>(loc));

	char const* nextToRead = s.data();
	wchar_t buffer[BUFSIZ];
	wchar_t* nextToWrite;
	std::codecvt_base::result result;
	std::wstring wresult;

	while ((result
	= converter.in
	(state,
	nextToRead, s.data()+s.size(), nextToRead,
	buffer, buffer+sizeof(buffer)/sizeof(*buffer), nextToWrite))
	== std::codecvt_base::partial)
	{
	wresult.append(buffer, nextToWrite);
	}

	if (result == std::codecvt_base::error)
	{
	throw std::runtime_error("Encoding error");
	}

	wresult.append(buffer, nextToWrite);
	return wresult;
	}

	*/
