#ifndef __STRING_FUNCTIONS_INL__
#define __STRING_FUNCTIONS_INL__

template< class T >
inline String ToString(const T& t)
{
	OStringStream stream;
	stream << t;
	return stream.str();
}

template< class T >
inline T FromString(const String& s)
{
	IStringStream stream(s);
	T t;
	stream >> t;
	return t;
}

inline ULong CHashedString::GetHashValue() const
{
	return reinterpret_cast<ULong>( m_ident );
}

inline const std::string & CHashedString::GetStr() const
{
	return m_identStr;
}

inline bool CHashedString::operator < (const CHashedString& o) const
{
	bool r = ( GetHashValue() < o.GetHashValue() );
	return r;
}

inline bool CHashedString::operator == (const CHashedString& o) const
{
	bool r = ( GetHashValue() == o.GetHashValue() );
	return r;
}



#endif