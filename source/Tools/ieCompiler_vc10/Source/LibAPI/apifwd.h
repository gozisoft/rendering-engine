#ifndef API_FORWARD_H
#define API_FORWARD_H

#include "Core.h"

namespace Engine
{
	// Memory manager
	class CHeap;
	class CHeapFactory;

	// String functions
	class CHashedString;

	// Input/Output
	class CEndian;
	class CBufferIO;
	



}

#endif