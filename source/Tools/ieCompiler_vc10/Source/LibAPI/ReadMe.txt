========================================================================
    STATIC LIBRARY : api Project Overview
========================================================================

created by Riad Gozim
rgozim1@hotmail.com

api.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

/////////////////////////////////////////////////////////////////////////////
Other notes:

API is responsible for the typedefs of data types throughout the application.
It holds compatilbilty for switching between x64 and x86 compiler options as well
as support for switching between Unicode and ASCII text.

This library can be upgraded to contain custom tools which can be used across an engine
or a large project. The tools with which it could contain are those such as a custom
STL or specialised STL type library.

Current compiler support:

# Visual Studio 2010
# Visual Studio 2008
# Visual Studio 2005 (requires BOOST)

Thirdparty library used:

# TR1 support through <memory.h> 
  used for std::shared_ptr.

# BOOST library support of <std::shared_ptr.hpp>
  used for std::shared_ptr. Required Preprocessor define of USING_BOOST .

/////////////////////////////////////////////////////////////////////////////
Additions:

Ver 0.8 (20/06/2010):-

* Preprocesser defines have been added across all Core_VCxx.h headers. Example, to include streaming to a project
  the define USING_STREAMING has to have been defined. If unsure look at "Core.h".

* Support for streaming, STL streaming has now been typedef'd to use the usual STL
  name but declared with a capital letter. Example 'std::ifstream' is 'IfStream'.

* Support for using 'std::cout' or 'std::wcout' without having to make use of preprocessor defines.
  Example 'std::cout' is 'Cout'.




