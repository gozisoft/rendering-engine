#ifndef __CHEAP_H__
#define __CHEAP_H__


#include <cstddef>

#ifdef USING_MEMORY_MGR

namespace Engine
{


class CHeap
{
public:
	struct AllocHeader
	{
		int           nSignature;
		int           nAllocNum;
		int           nSize;
		CHeap *       pHeap;
		AllocHeader * pNext;
		AllocHeader * pPrev;
	};

	void Initialize();

	void * Allocate(size_t bytes);
	static void Deallocate (void * pMem);

	void Activate (const char * name);
	void Deactivate ();

	void AttachTo (CHeap * pParent);

	bool IsActive () const { return m_bActive; }
	const char * GetName() const;

	void PrintTreeInfo (int indentLevel = 0) const;
	void PrintInfo (int indentLevel = 0) const;

	int ReportMemoryLeaks (int nBookmark1, int nBookmark2);

	static int GetMemoryBookmark ();

private:
	void Deallocate(AllocHeader * pHeader);
	void GetTreeStats (size_t & totalBytes, size_t & totalPeak, int & totalInstances) const;

	enum { NAMELENGTH = 128 };

	bool		  m_bActive;
	char		  m_name[NAMELENGTH];
	size_t		  m_bytesAllocated;
	size_t		  m_bytesPeak;
	int			  m_nInstances;
	AllocHeader * m_pHeadAlloc;

	CHeap * m_pParent;
	CHeap * m_pFirstChild;
	CHeap * m_pNextSibling;
	CHeap * m_pPrevSibling;

	static int s_nNextAllocNum;
};

}

#endif

#endif // __CHEAP_H__