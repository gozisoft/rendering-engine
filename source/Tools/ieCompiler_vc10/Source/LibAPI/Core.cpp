#include "apiafx.h"
#include "Core.h"

#if defined (USING_STL) || defined(_USING_STL)

	#include <iostream> // for std::[w]cin and std::[w]cout

	#if defined (UNICODE) || defined(_UNICODE)
		_OutputStream& Cout = std::wcout;
		_InputStream& Cin = std::wcin;
	#else
		_OutputStream& Cout = std::cout;
		_InputStream& Cin = std::cin;
	#endif

#endif // USING_STL
