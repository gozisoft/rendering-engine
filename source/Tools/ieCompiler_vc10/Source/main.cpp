#include "stdafx.h"

#define _CRTDBG_MAP_ALLOC
#include <tchar.h>
#include <crtdbg.h>

using namespace Engine;

// ------------------------------------------------------------------------------------------------------
// Summary: Application entry point
// Parameters:
// [in] hInstance - Application instance
// [in] hPrevInstance - Junk
// [in] lpCmdLine - Command line arguments
// [in] nCmdShow - Window display flags
// ------------------------------------------------------------------------------------------------------
int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{ 
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	UNREFERENCED_PARAMETER(nCmdShow);

	// Enable run-time memory check for debug builds.
    _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDERR);

	int tmpDbgFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	tmpDbgFlag |= _CRTDBG_DELAY_FREE_MEM_DF;
	tmpDbgFlag |= _CRTDBG_LEAK_CHECK_DF;
	_CrtSetDbgFlag(tmpDbgFlag);		

	LoadEffectFromDisk("XMLFile1.xml");

    // Rock and roll
    return 0;
}