#ifndef __CVERTEX_FORMAT_H__
#define __CVERTEX_FORMAT_H__

#include "VertexElement.h"

_ENGINE_BEGIN

class CVertexFormat
{
public:
	// Element container
	typedef std::vector<CVertexElement*> ElementContainer;

	// Default ctor
	CVertexFormat();

	// Preset constructor
	CVertexFormat(const ElementContainer& elements, size_t totalStride = 0);

	// default destructor
	~CVertexFormat();

	// Add an element to the end of the list
	const CVertexElement* AddElement(UInt sourceIndex, Buffer::VertexType type, 
		const std::string& semantic, UInt semanticIndex = 0);

	// Insert an element into an already filled array.
	const CVertexElement* InsertElement(UInt pos, UInt sourceIndex, Buffer::VertexType type,
		const std::string& semantic, UInt semanticIndex = 0);

	// Get an element by its semantic type
	const CVertexElement* GetElementBySemantic(const std::string& semantic,
		UInt semanticIndex = 0) const;

	// Access to all the elements
	const ElementContainer& GetElements() const;

	// Get an element based on its position in the array
	const CVertexElement* GetElement(UInt pos) const;

	// Lookup and element list by its stream index
	// void GetElementsByStream(UInt streamindex, ElementList& elements);

	// Remove an element based on array position
	void RemoveElement(UInt pos);

	// Remove an element type by its semantic index ( ie more than one elemnt of the semantic )
	void RemoveElement(const std::string& semantic, UInt semanticIndex = 0);

	// Clear the enitre array of elements
	void RemoveAllElements();

	// Number of elements within the vertex format, best used with GetElements()
	// for copying the list of elements
	UInt GetElementCount() const;

	// Lookup the number of active element semantics. Use when looking for the number
	// of textures or colours semenatics within the vertex format.
	UInt GetElementSemanticCount(const std::string& semantic) const;
	
	// The total length of all the element types combined
	UInt GetTotalStride() const;

	//// Bit field setting functions for setting a semantic as on or off.
	//void SetEnabled(Buffer::VertexSemantic semantic, bool enable);
	//bool IsEnabled(Buffer::VertexSemantic semantic) const;

	// Constants
	static const UInt MAX_TCOORDS = 8; // default: 8
	static const UInt MAX_COLOURS = 2; // default: 2

protected:
	ElementContainer m_elements;
	UInt m_totalStride;

};

#include "VertexFormat.inl"

_ENGINE_END

#endif


//// tags
//struct linear { };
//struct semantic { };
//struct semantic_and_index { };

//// typedefs for multi_index
//typedef boost::multi_index::sequenced< 
//	boost::multi_index::tag<linear>
//> LinearIndex;

//typedef boost::multi_index::composite_key<
//	CVertexElement,
//	boost::multi_index::const_mem_fun<CVertexElement, const std::string&, &CVertexElement::GetVertexSemantic>,
//	boost::multi_index::const_mem_fun<CVertexElement, UInt, &CVertexElement::GetSemanticIndex> 
//> CompositeSemanticIndex;

//typedef boost::multi_index::hashed_non_unique<
//	boost::multi_index::tag<semantic_and_index>,
//	CompositeSemanticIndex
//> SemanticAndIDIndex;

//typedef boost::multi_index::hashed_non_unique<
//	boost::multi_index::tag<semantic>,
//	boost::multi_index::const_mem_fun<CVertexElement, const std::string&, &CVertexElement::GetVertexSemantic> 
//> SemanticIndex;

//typedef boost::multi_index::multi_index_container <
//	CVertexElement*,
//	boost::multi_index::indexed_by <
//		LinearIndex,
//		SemanticAndIDIndex,
//		SemanticIndex
//	> // index_by
//> ElementContainer;



//
//#include "FrameFwd.h"
//#include "VertexTypes.h"
//
//_ENGINE_BEGIN
//
//class CVertexFormat
//{
//public:
//	// Vertex Elements array typedef
//	typedef std::vector<CVertexElementPtr> ElementList;
//	typedef ElementList::iterator ElementItor; 
//	typedef ElementList::const_iterator Const_ElementItor;
//
//	// Vertex Semantics bitset typedef
//	typedef std::bitset<Buffer::NUM_VERTEX_SEMANTICS> SemanticBSet;
//
//	// Default ctor
//	CVertexFormat();
//
//	// Constructor that forward sets the number of formats that will be used
//	CVertexFormat(UInt numUniqueElements);
//
//	// default destructor
//	~CVertexFormat();
//
//	// Add an element to the end of the list
//	CVertexElementPtr AddElement(UInt sourceIndex, Buffer::VertexType type,
//		Buffer::VertexSemantic semantic, UInt semanticIndex = 0);
//
//	// Insert an element into an already filled array.
//	CVertexElementPtr InsertElement(UInt pos, UInt sourceIndex, Buffer::VertexType type,
//		Buffer::VertexSemantic semantic, UInt semanticIndex = 0);
//
//	// Remove an element based on array position
//	void RemoveElement(UInt pos);
//
//	// Remove an element type by its semantic index ( ie more than one elemnt of the semantic )
//	void RemoveElement(Buffer::VertexSemantic semantic, UInt semanticIndex = 0);
//
//	// Clear the enitre array of elements
//	void RemoveAllElements();
//
//	// Returns a list of elements that make up the vertex format
//	const ElementList& GetElements() const;
//
//	// Returns a list of elements that make up the vertex format (non const)
//	ElementList& GetElements();
//
//	// Number of elements within the vertex format, best used with GetElements()
//	// for copying the list of elements
//	UInt GetElementCount() const;
//
//	// Lookup the number of active element semantics. Use when looking for the number
//	// of textures or colours semenatics within the vertex format.
//	UInt GetElementSemanitcCount(Buffer::VertexSemantic semantic) const;
//	
//	// Get an element by its semantic type
//	CVertexElementPtr GetElementBySemantic(Buffer::VertexSemantic semantic, UInt semanticIndex = 0);
//
//	// Get an element based on its position in the array
//	CVertexElementPtr GetElement(UInt pos);
//
//	// Lookup and element list by its stream index
//	// void GetElementsByStream(UInt streamindex, ElementList& elements);
//
//	// The total length of all the element types combined
//	UInt GetTotalStride() const;
//
//	// Bit field setting functions for setting a semantic as on or off.
//	void SetEnabled(Buffer::VertexSemantic semantic, bool enable);
//
//	bool IsEnabled(Buffer::VertexSemantic semantic) const;
//
//	static const UInt MAX_TCOORDS = 8; // default: 8
//	static const UInt MAX_COLOURS = 2; // default: 2
//
//protected:
//	ElementList	m_elements;
//	SemanticBSet m_semanticFlags;
//	UInt m_totalStride;
//
//private:
//	DECLARE_HEAP;
//
//};
//
//#include "VertexFormat.inl"
//
//_ENGINE_END
//
//#endif
