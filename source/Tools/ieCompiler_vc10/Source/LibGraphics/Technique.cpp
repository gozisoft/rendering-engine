#include "frameafx.h"
#include "Technique.h"
#include "VertexShader.h"
#include "AlphaState.h"
#include "CullState.h"
#include "DepthState.h"
#include "OffsetState.h"
#include "StencilState.h"
#include "WireState.h"
#include "ZBufferState.h"
#include "Helper.h"

using namespace Engine;

CVisualPass::CVisualPass() :
m_pAlphaState(nullptr),
m_pCullState(nullptr),
m_pDepthState(nullptr),
m_pOffsetState(nullptr),
m_pStencilState(nullptr),
m_pWireState(nullptr)
{

}

CVisualPass::~CVisualPass()
{
	SafeDelete(m_pAlphaState);
	SafeDelete(m_pCullState);
	SafeDelete(m_pDepthState);
	SafeDelete(m_pOffsetState);
	SafeDelete(m_pStencilState);
	SafeDelete(m_pWireState);
}

CSceneTechnique::CSceneTechnique(const std::string contextName, const std::string& techVersion) : 
m_contexName(contextName),
m_techVersion(techVersion)
{

}

CSceneTechnique::~CSceneTechnique()
{
	for (auto itor = m_visualPasses.begin(); itor != m_visualPasses.end(); ++itor)
	{
		CVisualPass* pVisualPass = (*itor);
		if (pVisualPass)
		{
			delete pVisualPass;
			pVisualPass = nullptr;
		}
	}
}
