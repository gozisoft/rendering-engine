#ifndef __CCONSTANT_ELEMENT_INL__
#define __CCONSTANT_ELEMENT_INL__

// ----------------------------------------------------------------------------------------------
// CConstantElement
// ----------------------------------------------------------------------------------------------
inline const std::string& CConstantElement::GetVarName() const
{
	return m_variableName;
}

inline Shader::DataType CConstantElement::GetDataType() const
{
	return m_dataType;
}

inline Shader::VariableType CConstantElement::GetVariableType() const
{
	return m_variableType;
}

inline size_t CConstantElement::GetRows() const
{
	return m_rows;
}

inline size_t CConstantElement::GetCols() const
{
	return m_columns;
}

inline size_t CConstantElement::GetOffset() const
{
	return m_offset;
}

inline void* CConstantElement::GetDefaultValue() const
{
	return m_pDefaultValue;
}


#endif

/*
inline const std::string& CConstantElement::GetName() const
{
	return m_name;
}

inline VariableType CConstantElement::GetVariableType() const
{
	return m_variableType;
}


*/