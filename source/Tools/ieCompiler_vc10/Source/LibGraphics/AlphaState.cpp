#include "frameafx.h"
#include "AlphaState.h"

using namespace Engine;


DEFINE_HEAP(CAlphaState, "CAlphaState");


CAlphaState::CAlphaState() : 
m_alphaBlendEnabled(false), m_srcBlend(SB_SRC_ALPHA), m_dstBlend(DB_ONE_MINUS_SRC_ALPHA),
m_compareEnabled(false), m_compareMode(CM_ALWAYS), m_ref(0)
{
	
}

CAlphaState::CAlphaState(bool blendEnable, SrcBlend srcBlend, DstBlend dstBlend,
					     bool compareEnable, CompareMode compareMode, float ref, const Colour4f& col) : 
m_alphaBlendEnabled(blendEnable), m_srcBlend(srcBlend), m_dstBlend(dstBlend),
m_compareEnabled(compareEnable), m_compareMode(compareMode), m_ref(ref), m_constantColour(col)
{

}

CAlphaState::~CAlphaState()
{

}
