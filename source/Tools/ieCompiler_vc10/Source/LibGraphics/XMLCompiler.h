#ifndef __CXML_COMPILER_H__
#define __CXML_COMPILER_H__

#include "IEffectLoader.h"
#include "ShaderTypes.h"
#include "VertexTypes.h"
#include "BaseEffect.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

_ENGINE_BEGIN

namespace Detail 
{
	class SBuildTuple
	{
	public:
		  SBuildTuple() : pTechnique(nullptr), passIndex(0)
		  {
		  }

		  SBuildTuple(const std::string& buildFlag, const std::string& name,
			  CSceneTechnique* pTech, size_t index) 
			  : shaderBuild(buildFlag),
			  entryPoint(name),
			  pTechnique(pTech),
			  passIndex(index)
		  {
		  }

		  std::string shaderBuild;
		  std::string entryPoint;
		  CSceneTechnique* pTechnique;
		  size_t passIndex;
	};

	class SConstantElementDesc
	{
	public:
		SConstantElementDesc() : rows(0), columns(0), pDefaultValue(nullptr)
		{
		}

		std::string variableName;
		Shader::DataType dataType;
		Shader::VariableType variableType;
		size_t rows;
		size_t columns;
		UInt8* pDefaultValue;
	};

	class SConstantBuffferDesc
	{
	public:
		SConstantBuffferDesc() 
		{ }

		std::string bufferName;
	};

	class SVertexElementDesc
	{
	public:
		SVertexElementDesc() : semanticIndex(0)
		{ }

		UInt semanticIndex;
		Buffer::VertexType vertexType;
		std::string semantic;
	};

	// typedef BuildTupleContainer BuildMap;
	typedef std::vector<Detail::SBuildTuple> BuildMap;
}

class CXMLCompiler : public IEffectLoader
{
public:
	CBaseEffectPtr LoadShader(const std::string& headerName);

protected:
	CXMLCompiler();

	void BuildConstantFormats(boost::property_tree::iptree &itorLevel, CBaseEffect::ConstantFormats& constants);
	void CompileShader(boost::property_tree::iptree &itorLevel, Detail::SBuildTuple& buildTuple);
	void BuildContext(boost::property_tree::iptree &itorLevel, Detail::BuildMap& buildMapOut);

};

_ENGINE_END

#endif