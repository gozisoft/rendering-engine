#ifndef __CBASE_EFFECT_INL__
#define __CBASE_EFFECT_INL__

//---------------------------------------------------------------------------------------------------------
// CBaseEffect
//---------------------------------------------------------------------------------------------------------

// Member access
inline size_t CBaseEffect::GetNumTechniques() const
{
	return m_techniques.size();
}

inline const CSceneTechnique* CBaseEffect::GetTechnique(size_t techniqueIndex) const
{
	return m_techniques[techniqueIndex];
}

inline CSceneTechnique* CBaseEffect::GetTechnique(size_t techniqueIndex)
{
	return const_cast<CSceneTechnique*>( static_cast<const CBaseEffect&>(*this).GetTechnique(techniqueIndex) );
}

inline CSceneTechnique* CBaseEffect::GetTechnique(const std::string& techName)
{
	return const_cast<CSceneTechnique*>( static_cast<const CBaseEffect&>(*this).GetTechnique(techName) );
}

// VisualPass access
inline const CVertexShader* CBaseEffect::GetVertexShader(size_t techniqueIndex, size_t passIndex) const
{
	return m_techniques.at(techniqueIndex)->GetVertexShader(passIndex);
}

inline const CPixelShader* CBaseEffect::GetPixelShader(size_t techniqueIndex, size_t passIndex) const
{
	return m_techniques.at(techniqueIndex)->GetPixelShader(passIndex);
}

inline const CAlphaState* CBaseEffect::GetAlphaState(size_t techniqueIndex, size_t passIndex) const
{
	return m_techniques.at(techniqueIndex)->GetAlphaState(passIndex);
}

inline const CCullState* CBaseEffect::GetCullState(size_t techniqueIndex, size_t passIndex) const
{
	return m_techniques.at(techniqueIndex)->GetCullState(passIndex);
}

inline const CDepthState* CBaseEffect::GetDepthState(size_t techniqueIndex, size_t passIndex) const
{
	return m_techniques.at(techniqueIndex)->GetDepthState(passIndex);
}

inline const COffsetState* CBaseEffect::GetOffsetState(size_t techniqueIndex, size_t passIndex) const
{
	return m_techniques.at(techniqueIndex)->GetOffsetState(passIndex);
}

inline const CStencilState* CBaseEffect::GetStencilState(size_t techniqueIndex, size_t passIndex) const
{
	return m_techniques.at(techniqueIndex)->GetStencilState(passIndex);
}

inline const CWireState* CBaseEffect::GetWireState(size_t techniqueIndex, size_t passIndex) const
{
	return m_techniques.at(techniqueIndex)->GetWireState(passIndex);
}




#endif