#ifndef __CSHADER_INL__
#define __CSHADER_INL__

// --------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------
inline void CShader::SetShaderProgram(const std::string& program)
{
	m_shaderProgram = program;
}

// --------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------
inline const std::string& CShader::GetFunctionName() const
{
	return m_functionName;
}

inline const std::string& CShader::GetProgram() const
{
	return m_shaderProgram;
}

inline CShader::ShaderType CShader::GetShaderType() const
{
	return m_shaderType;
}

inline CShader::ShaderVersion CShader::GetShaderVersion() const
{
	return m_shaderVersion;
}

inline void CShader::SetShaderMarker(void* pShaderMarker)
{
	m_pShaderMarker = pShaderMarker;
}

inline void CShader::SetShaderVars(const ShaderVarContainer& variables)
{
	m_shaderVariables = variables;
}

#endif