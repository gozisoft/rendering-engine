#ifndef FRAME_HEADERS_H
#define FRAME_HEADERS_H

// Effect Loader
#include "IEffectLoader.h"

// Effect
#include "BaseEffect.h"
#include "Shader.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "Technique.h"

// Shader desc
#include "VertexTypes.h"
#include "ShaderTypes.h"
#include "ConstantElement.h"
#include "ConstantFormat.h"
#include "VertexElement.h"
#include "VertexFormat.h"


#endif