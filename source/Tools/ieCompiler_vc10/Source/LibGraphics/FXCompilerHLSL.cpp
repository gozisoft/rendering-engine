#include "frameafx.h"

#if defined (USING_DIRECT3D11) || defined (_USING_DIRECT3D11)

#include "FXCompiler.h"
#include "Technique.h"
#include "VertexFormat.h"
#include "ConstantFormat.h"
#include "ShaderSampler.h"

// Enable extra D3D debugging in debug builds if using the debug DirectX runtime.  
// This makes D3D objects work well in the debugger watch window, but slows down 
// performance slightly.
#if defined(DEBUG) || defined(_DEBUG)
#ifndef D3D_DEBUG_INFO
#define D3D_DEBUG_INFO
#include <D3Dcompiler.h>
#undef D3D_DEBUG_INFO
#endif
#endif

using namespace Engine;

using std::for_each;
using std::make_shared;

std::string HLSLCompileTarget(CShader::ShaderVersion version, CShader::ShaderType shaderType)
{
	char* pTarget = nullptr;
	switch (version)
	{
	case CShader::SV_2_0: pTarget = "2_0"; break;
	case CShader::SV_3_0: pTarget = "3_0"; break;
	case CShader::SV_4_0: pTarget = "4_0"; break;
	case CShader::SV_4_1: pTarget = "4_1"; break;
	case CShader::SV_5_0: pTarget = "5_0"; break;
	default:
		assert( false && ieS("Unknown Feature Level") );
		break;
	};

	char* pType = nullptr;
	switch (shaderType)
	{
	case CShader::ST_VERTEX: pType = "vs_"; break;
	case CShader::ST_GEOMETRY: pType = "gs_"; break; // only avaiable on shader model 4 or greater
	case CShader::ST_PIXEL: pType = "ps_"; break;
	default:
		assert( false && ieS("Unknown shader type") );
		break;
	};

	// Combine the strings
	std::stringstream sstream;
	sstream << pType << pTarget;

	// add the target on to the type
	return sstream.str();
}

Buffer::VertexType HLSLFormatType(D3D_REGISTER_COMPONENT_TYPE type, BYTE mask)
{
	if (mask < 1)
	{
		assert( false && ieS("Unknown shader input mask value : D3D11RegisterTypeToFormatType()") );
		return Buffer::VT_UNKNOWN;
	}

	// count the used bits within the mask
	size_t maskValue = 0;
	for (; mask; ++maskValue)
	{
		mask &= (mask-1);
	}

	size_t index = maskValue;
	switch (type)
	{
	case D3D_REGISTER_COMPONENT_UNKNOWN:
		return Buffer::VT_UNKNOWN;
	case D3D_REGISTER_COMPONENT_UINT32:
		index += static_cast<size_t>(Buffer::VT_UINT1)-1;	
		break;
	case D3D_REGISTER_COMPONENT_SINT32:
		index += static_cast<size_t>(Buffer::VT_INT1)-1;
		break;
	case D3D_REGISTER_COMPONENT_FLOAT32:	
		index += static_cast<size_t>(Buffer::VT_FLOAT1)-1;
		break;
	default:
		assert( false && ieS("Unknown register component type : D3D11RegisterTypeToFormatType()") );
		return Buffer::VT_UNKNOWN;
	};

	Buffer::VertexType finalType = (Buffer::VertexType)index;
	return finalType;
}

Shader::ShaderReturnType HLSLShaderReturnType(D3D_RESOURCE_RETURN_TYPE returnType)
{
	switch (returnType)
	{
	case D3D_RETURN_TYPE_UNORM:
		return Shader::SRT_UNORM;
	case D3D_RETURN_TYPE_SNORM:
		return Shader::SRT_SNORM;
	case D3D_RETURN_TYPE_SINT:
		return Shader::SRT_SINT;
	case D3D_RETURN_TYPE_UINT:
		return Shader::SRT_UINT;
	case D3D_RETURN_TYPE_FLOAT:
		return Shader::SRT_FLOAT;
	case D3D_RETURN_TYPE_MIXED:
		return Shader::SRT_MIXED;
	case D3D_RETURN_TYPE_DOUBLE:
		return Shader::SRT_DOUBLE;
	case D3D_RETURN_TYPE_CONTINUED:
		return Shader::SRT_CONTINUED;
	default:
		return Shader::SRT_NONE;
	};
}

Shader::ShaderDimensions HLSLShaderDimensions(D3D_SRV_DIMENSION dimension)
{
	switch (dimension)
	{
	case D3D_SRV_DIMENSION_UNKNOWN:	return Shader::SD_UNKNOWN;
	case D3D_SRV_DIMENSION_BUFFER: return Shader::SD_BUFFER;
	case D3D_SRV_DIMENSION_TEXTURE1D: return Shader::SD_TEXTURE1D;
	case D3D_SRV_DIMENSION_TEXTURE1DARRAY: return Shader::SD_TEXTURE1DARRAY;
	case D3D_SRV_DIMENSION_TEXTURE2D: return Shader::SD_TEXTURE2D;
	case D3D_SRV_DIMENSION_TEXTURE2DARRAY:
	case D3D_SRV_DIMENSION_TEXTURE2DMS:	return Shader::SD_TEXTURE2DARRAY;
	case D3D_SRV_DIMENSION_TEXTURE2DMSARRAY: return Shader::SD_TEXTURE2DARRAY;
	default:
		return Shader::SD_UNKNOWN;
	};
}

Shader::DataType HLSLDataType(D3D_SHADER_VARIABLE_CLASS varClass)
{
	switch (varClass)
	{
	case D3D_SVC_SCALAR:
		return Shader::DT_SCALAR;
	case D3D_SVC_VECTOR:
		return Shader::DT_VECTOR;
	case D3D_SVC_MATRIX_ROWS:
	case D3D_SVC_MATRIX_COLUMNS:	
		return Shader::DT_MATRIX;
	default:
		assert(false);
		return Shader::DT_UNKNOWN;
	};
}

Shader::VariableType HLSLVariableType(D3D_SHADER_VARIABLE_TYPE type)
{
	switch (type)
	{
	case D3D_SVT_BOOL:
		return Shader::VT_BOOL;
	case D3D_SVT_INT:
		return Shader::VT_INT;
	case D3D_SVT_UINT:
		return Shader::VT_UINT;
	case D3D_SVT_FLOAT:
		return Shader::VT_FLOAT;
	case D3D_SVT_DOUBLE:
		return Shader::VT_DOUBLE;
	case D3D_SVT_TEXTURE1D: 
		return Shader::VT_TEXTURE_1D;
	case D3D_SVT_TEXTURE2D:
		return Shader::VT_TEXTURE_2D;
	case D3D_SVT_TEXTURE3D:
		return Shader::VT_TEXTURE_3D;
	default:
		assert(false);
		return Shader::VT_UNKNOWN;
	};
}

void CFXCompiler::CompileHLSLShader(CShader* pShader)
{
	// Compile the shader with Direct3D
	const std::string& hlslTarget =
		HLSLCompileTarget( pShader->GetShaderVersion(), pShader->GetShaderType() );

	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
	dwShaderFlags |= D3DCOMPILE_PACK_MATRIX_ROW_MAJOR;
#if defined(DEBUG) || defined(_DEBUG)
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

	ID3DBlob* pShaderBlob = nullptr;
	ID3DBlob* pErrorBlob = nullptr; 
	HRESULT hr = D3DCompile(pShader->GetProgram().c_str(), 
		pShader->GetProgram().size(),
		nullptr,
		nullptr,
		nullptr,
		pShader->GetFunctionName().c_str(),
		hlslTarget.c_str(),
		dwShaderFlags,
		0,
		&pShaderBlob,
		&pErrorBlob);

	// Error check
	if ( FAILED(hr) )
	{
		if (pErrorBlob)
		{
			OutputDebugStringA( (char*)pErrorBlob->GetBufferPointer() );		
			pErrorBlob->Release(); // No longer used
		}
	}

	// Set the shader blob
	pShader->SetShaderMarker(pShaderBlob);

	// Reflect on the shader to ensure what we have is correct
	ID3D11ShaderReflection* pShaderReflection = nullptr;
	hr = D3DReflect( (void*)pShaderBlob->GetBufferPointer(),
		pShaderBlob->GetBufferSize(),
		IID_ID3D11ShaderReflection,	// can't do __uuidof(ID3D11ShaderReflection) here...
		(void**)&pShaderReflection );

	D3D11_SHADER_DESC shaderDesc;
	pShaderReflection->GetDesc(&shaderDesc);

	// enum input parameters
	Buffer::VertexType vertexType;
	size_t paddedSize = 0;
	std::vector<CVertexElement*> elements(shaderDesc.InputParameters);
	for (UINT i = 0; i < shaderDesc.InputParameters; ++i)
	{
		D3D11_SIGNATURE_PARAMETER_DESC param_desc;
		hr = pShaderReflection->GetInputParameterDesc(i, &param_desc);
		if (FAILED(hr))
			break;

		// Shader Type
		vertexType = HLSLFormatType(param_desc.ComponentType, param_desc.Mask);

		// Add an input element
		elements[i] = new CVertexElement(param_desc.Register, 
			param_desc.Stream,
			vertexType, // does not matter for component type
			param_desc.SemanticName,
			param_desc.SemanticIndex);

		// Increment the typeSize
		paddedSize += SizeOfType(vertexType);
	}

	// Set the input format of the shader!
	pShader->SetInputFormat( make_shared<CVertexFormat>(elements, paddedSize) );

	// enum output parameters
	paddedSize = 0;
	elements.resize(shaderDesc.InputParameters);
	for (UINT i = 0; i < shaderDesc.InputParameters; ++i)
	{
		D3D11_SIGNATURE_PARAMETER_DESC param_desc;
		hr = pShaderReflection->GetOutputParameterDesc(i, &param_desc);
		if (FAILED(hr))
			break;

		// Shader Type
		vertexType = HLSLFormatType(param_desc.ComponentType, param_desc.Mask);

		// Add an input element
		elements[i] = new CVertexElement(param_desc.Register, 
			param_desc.Stream,
			vertexType, // does not matter for component type
			param_desc.SemanticName,
			param_desc.SemanticIndex);

		// Increment the typeSize
		paddedSize += SizeOfType(vertexType);
	}

	// Set the output format of the shader!
	pShader->SetOutputFormat( make_shared<CVertexFormat>(elements, paddedSize) );

	// Check for resource buffer usage
	std::vector<CShaderVariable*> shaderResources(shaderDesc.BoundResources);
	for(UINT i = 0; i < shaderDesc.BoundResources; ++i)
	{
		D3D11_SHADER_INPUT_BIND_DESC resource_desc;
		hr = pShaderReflection->GetResourceBindingDesc(i, &resource_desc);
		if ( FAILED(hr) ) // No constant buffer present
			break;

		switch (resource_desc.Type)
		{
		case D3D_SIT_TEXTURE:
			{	
				shaderResources[i] = new CShaderTexture(resource_desc.Name,
					resource_desc.NumSamples,
					HLSLShaderReturnType(resource_desc.ReturnType),				
					HLSLShaderDimensions(resource_desc.Dimension),
					resource_desc.BindPoint,
					resource_desc.BindCount);
			}
			break;

		case D3D_SIT_SAMPLER:
			{
				shaderResources[i] = new CShaderSampler(resource_desc.Name, 
					resource_desc.BindPoint,
					resource_desc.BindCount);
			}
			break;

		case D3D_SIT_CBUFFER:
			{
				for (UINT j = 0; j < shaderDesc.ConstantBuffers; ++j)
				{
					// Get the constant buffer
					ID3D11ShaderReflectionConstantBuffer* pConstantReflection = 
						pShaderReflection->GetConstantBufferByIndex(j);

					// Get its description
					D3D11_SHADER_BUFFER_DESC const_buff_desc;
					if ( FAILED( pConstantReflection->GetDesc(&const_buff_desc) ) ) // No constant buffer present
						break;

					// Check this is the right constant
					if ( std::strcmp(resource_desc.Name, const_buff_desc.Name) )
						break;

					// This will build an array of elements(variables) which will be stored within 
					// a single constant format
					std::vector<CConstantElement*> constants(const_buff_desc.Variables);
					for (UINT j = 0; j < const_buff_desc.Variables; ++j) // Go over the variables
					{
						ID3D11ShaderReflectionVariable* pVariable = pConstantReflection->GetVariableByIndex(j);
						ID3D11ShaderReflectionType* pType = pVariable->GetType(); 

						// Get the variable type description and store it
						D3D11_SHADER_TYPE_DESC type_desc;
						pType->GetDesc(&type_desc);

						// Get the variable description and store it
						D3D11_SHADER_VARIABLE_DESC var_desc;
						pVariable->GetDesc(&var_desc);

						// http://msdn.microsoft.com/en-us/library/ff728734(v=vs.85).aspx
						// Check if variable is actually used ? D3D_SVF_USED

						// Add the type to the constant format
						constants[j] = new CConstantElement(
							var_desc.Name,
							HLSLDataType(type_desc.Class),
							HLSLVariableType(type_desc.Type),
							type_desc.Rows,
							type_desc.Columns,
							var_desc.StartOffset,
							(void*)var_desc.DefaultValue );
					}

					shaderResources[i] = new CConstantFormat(constants, 
						const_buff_desc.Name, 
						const_buff_desc.Size,
						resource_desc.BindPoint,
						resource_desc.BindCount);
				}
			}
			break;

		}; // switch
	} // for

	// Set the shader variables
	pShader->SetShaderVars(shaderResources);
}


#endif // USING_DIRECT3D11



//#ifndef INITGUID
//#define INITGUID
//#include <D3Dcompiler.h>
//#undef INITGUID
//#endif

//typedef HRESULT (WINAPI *pD3DReflect)
//	(LPCVOID pSrcData,
//	SIZE_T SrcDataSize,
//	REFIID pInterface,
//	void** ppReflector);

//// Assign the compile function
//HMODULE hModCompile = LoadLibrary(D3DCOMPILER_DLL);
//if (!hModCompile)
//{
//	assert("Could not load D3DCompiler_43.dll");
//	return;
//}

//// This is the compile function
//pD3DCompile D3DCompileFunc = reinterpret_cast<pD3DCompile>(::GetProcAddress(hModCompile, "D3DCompile"));
//if (!D3DCompileFunc)
//{
//	assert("Could not load D3DCompile function");
//	return;
//}

//// This is the reflect function
//pD3DReflect D3DReflectFunc = reinterpret_cast<pD3DReflect>(::GetProcAddress(hModCompile, "D3DReflect"));
//if (!D3DReflectFunc)
//{
//	assert("Could not load D3DReflect function");
//	return;
//}