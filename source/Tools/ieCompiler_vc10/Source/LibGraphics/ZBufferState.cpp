#include "frameafx.h"
#include "ZBufferState.h"

using namespace Engine;



DEFINE_HEAP(CZBufferState, "CZBufferState");


CZBufferState::CZBufferState() : m_isEnabled(true), m_isWritable(true), m_compare(CT_LEQUAL)
{

}

CZBufferState::~CZBufferState()
{


}