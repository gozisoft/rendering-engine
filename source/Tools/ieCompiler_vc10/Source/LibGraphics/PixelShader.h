#ifndef __CPIXEL_SHADER_H__
#define __CPIXEL_SHADER_H__

#include "Shader.h"

_ENGINE_BEGIN

class CPixelShader : public CShader
{
public:
	CPixelShader(const std::string& functionName, ShaderVersion version);
	~CPixelShader();

};

_ENGINE_END

#endif