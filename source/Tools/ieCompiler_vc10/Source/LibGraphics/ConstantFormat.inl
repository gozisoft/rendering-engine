#ifndef __CCONSTANT_FORMAT_INL__
#define __CCONSTANT_FORMAT_INL__

// ----------------------------------------------------------------------------------------------
// CConstantFormat
// ----------------------------------------------------------------------------------------------
inline const CConstantElement* CConstantFormat::GetElementByIndex(size_t index) const
{
	assert( index < GetElementCount() );
	return m_constElements[index];
}

inline size_t CConstantFormat::GetElementCount() const
{
	return m_constElements.size();
}

inline size_t CConstantFormat::GetPaddedSize() const
{
	return m_paddedSize;
}

inline size_t CConstantFormat::GetSize() const
{
	return m_size;
}

// ----------------------------------------------------------------------------------------------
// CShaderTexture
// ----------------------------------------------------------------------------------------------
inline Shader::ShaderReturnType CShaderTexture::GetReturnType() const 
{
	return m_returnType;
}

inline Shader::ShaderDimensions CShaderTexture::GetShaderDimensions() const
{
	return m_dimension;
}

inline size_t CShaderTexture::GetNumSamples() const
{
	return m_numSamples;
}

#endif


//// Operator overloads
//inline bool operator != (const CConstantFormat& format0, const CConstantFormat& format1)
//{
//	if ( format0.GetElements() != format1.GetElements() || 
//		 format0.GetFormatName() != format1.GetFormatName() )
//	{
//		return true;
//	}
//
//	return false;
//}
//
//inline bool operator == (const CConstantFormat& format0, const CConstantFormat& format1)
//{
//	return !(format0 != format1);
//}