#include "frameafx.h"
#include "FXCompiler.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include "BaseEffect.h"

using namespace Engine;

using std::for_each;
using std::make_shared;

CFXCompiler::CFXCompiler()
{

}

CFXCompiler::~CFXCompiler()
{


}

CBaseEffectPtr CFXCompiler::LoadEffect(std::istream& inputStream)
{
	using boost::property_tree::xml_parser::no_comments;
	using boost::property_tree::xml_parser::no_concat_text;
	using boost::property_tree::xml_parser::trim_whitespace;

	// Create an empty property tree object
	iptree pt;
	TechniqueArray techniques;
	TechniqueMap shadersToCompile;

	// Load the XML file into the property tree. If reading fails
	// (cannot open file, parse error), an exception is thrown.
	int flags = no_comments | trim_whitespace;
	read_xml(inputStream, pt, flags);

	// Parse over the shader variables
	iptree& shaderLevel = pt.get_child("Shader");
	BOOST_FOREACH(auto level, shaderLevel)
	{
		// Obtain a map of the shaders to build
		if (level.first == "Contexts")
		{	
			BuildContext(level.second, techniques, shadersToCompile);	
		}
	}

	// This will parse each shader and determine IO, globals, etc..
	for (auto i = shadersToCompile.begin(); i != shadersToCompile.end(); ++i) 
	{
		// Temp stuff from the pair
		size_t index = (*i).first;
		CSceneTechnique* pTech = (*i).second;

		// Get the correct pass
		CVisualPass* pPass = pTech->GetPass(index);

		// Vertex shader
		CShader* pShader = nullptr;
		if (pPass->m_pVertexShader)
			CompileShader(shaderLevel, pPass->m_pVertexShader);

		// Pixel shader
		if (pPass->m_pPixelShader)
			CompileShader(shaderLevel, pPass->m_pPixelShader);	
	}

	// Create the base effect and return it
	return make_shared<CBaseEffect>(techniques);
}

CBaseEffectPtr CFXCompiler::LoadEffect(std::wistream& inputStream)
{

	return nullptr;
}

void CFXCompiler::CompileShader(CFXCompiler::iptree &itorLevel, CShader* pShader)
{
	// A typedef to shorten text
	typedef iptree::value_type& valueType;

	// Determine the shader type
	char* pShaderType = nullptr;
	switch ( pShader->GetShaderType() )
	{
	case CShader::ST_VERTEX: 
		pShaderType = "Vertex"; 		
		break;
	case CShader::ST_GEOMETRY: 
		pShaderType = "Geometry"; 	
		break;
	case CShader::ST_PIXEL: 
		pShaderType = "Pixel"; 		
		break;
	};

	// Get the name of the shader and load in the shader text
	iptree& codeLevel = itorLevel.get_child(pShaderType);
	BOOST_FOREACH(valueType data, codeLevel)
	{
		// Load in the shader Text
		if (data.first == "HLSL") 
		{		
			std::string& shaderProgram = data.second.get<std::string>("");
			pShader->SetShaderProgram(shaderProgram);
		}
	}

	// Now time to compile the shader using an API dependant compiler
#if defined (USING_DIRECT3D11) || defined (_USING_DIRECT3D11)
	CompileHLSLShader(pShader);		
#endif
}

void CFXCompiler::BuildContext(iptree &itorLevel, TechniqueArray& uniqueTechs, TechniqueMap& techMap)
{
	typedef iptree::value_type& valueType;

	// Go over the context children
	iptree& contextLevel = itorLevel.get_child("");
	BOOST_FOREACH(valueType contexts, contextLevel)
	{
		if (contexts.first != "Context")
		{		
			// Somthing went wrong here, no context avaiable
			assert(false);
			break;
		}

		BOOST_FOREACH( valueType pass, contexts.second.get_child("Pass") ) 
		{
			std::string result = pass.first;


		}

		// Go over each context
		BOOST_FOREACH( valueType context, contexts.second.get_child("") )
		{
			if (context.first == "<xmlattr>")
			{
				// These will be attributes of the context
				std::string techName;
				std::string techVersion;

				// Go over the attributes of the context
				BOOST_FOREACH( auto leaf, context.second.get_child("") )
				{		
					if (leaf.first == "name")
						techName = leaf.second.get<std::string>("");					
					else if (leaf.first == "version")
						techVersion = leaf.second.get<std::string>("");
				}

				// Craete the technique
				CSceneTechnique* pTechnique = new CSceneTechnique(techName, techVersion);
				uniqueTechs.push_back(pTechnique);
			}
			else if (context.first == "Pass")
			{
				// Create the visual pass and add it to the technique		
				CVisualPass* pPass = new CVisualPass();

				// Add the pass to the current technique
				CSceneTechnique* pTechnique = uniqueTechs.back();
				pTechnique->AddVisualPass(pPass);

				// Now go over the pass leaves
				BOOST_FOREACH(valueType pass, context.second.get_child("") )
				{
					if (pass.first == "<xmlattr>")
					{
						// Get the pass index (used to look up the pass within the tech)
						BOOST_FOREACH( auto attribute, pass.second.get_child("") )
						{
							assert(attribute.first == "index");
							size_t passIndex = attribute.second.get<size_t>("");
							techMap.insert( std::make_pair(passIndex, pTechnique) );
						}						
					}
					else if (pass.first == "Compile")
					{
						BOOST_FOREACH( valueType compile, pass.second.get_child("") )
						{
							// Temp pointer to store shader build
							std::string shaderBuild;
							std::string entryPoint;
							BOOST_FOREACH( valueType attribute, compile.second.get_child("") )
							{
								if (attribute.first == "shader")
									shaderBuild = attribute.second.get_value<std::string>("");							
								else if (attribute.first == "function")
									entryPoint = attribute.second.get<std::string>("");							
							}

							// Determine the shader version
							CShader::ShaderVersion shaderVersion = CShader::SV_UNKNOWN;
							if (pTechnique->GetTechVersion() == "4.1")
								shaderVersion= CShader::SV_4_1;
							else if (pTechnique->GetTechVersion() == "4.0")
								shaderVersion= CShader::SV_4_0;
							else if (pTechnique->GetTechVersion() == "3.0")
								shaderVersion= CShader::SV_3_0;

							// Check for valid shader version
							assert(shaderVersion != CShader::SV_UNKNOWN);

							// Check which shader type we have here
							if (shaderBuild == "Vertex")
							{
								pPass->m_pVertexShader = new CVertexShader(entryPoint, shaderVersion);
							}
							else if (shaderBuild == "Pixel")
							{
								pPass->m_pPixelShader = new CPixelShader(entryPoint, shaderVersion);
							}		
						}
					} // if (compile)
				} 
			} 
		} 
	} // contextLevel
}