#ifndef __CCONSTANT_FORMAT_H__
#define __CCONSTANT_FORMAT_H__

#include "ShaderVariable.h"

_ENGINE_BEGIN

class CConstantFormat : public CShaderVariable
{
public:
	// tags
	typedef std::vector<CConstantElement*> ElementContainer;

	// Constructor for delayed creation
	CConstantFormat(const std::string& bufferName, size_t paddedSize, size_t bindPoint=0,
		size_t bindCount=0);

	// Constructor for fixed creation
	CConstantFormat(const ElementContainer& elements, const std::string& bufferName,
		size_t paddedSize, size_t bindPoint=0, size_t bindCount=0);

	// Non virtual destrcutor
	~CConstantFormat();

	// Push back an element to the format
	const CConstantElement* PushElement(const std::string& variableName, Shader::DataType dataType,
	Shader::VariableType variable, size_t rows, size_t cols, UInt8* pDefaultValue = nullptr);

	// Get element by its name
	const CConstantElement* GetElementByName(const std::string& variableName) const;

	// Get element by Index
	const CConstantElement* GetElementByIndex(size_t index) const;

	// Number of elements
	size_t GetElementCount() const;

	// Size of all the elements combined + Padding
	size_t GetPaddedSize() const;

	// Size of all the elements combined
	size_t GetSize() const;

private:
	ElementContainer m_constElements;
	size_t m_paddedSize; // This size includes any padding
	size_t m_size;		 // This size is unpadded

};

class CShaderTexture : public CShaderVariable
{
public:
	CShaderTexture(const std::string& name,	size_t numSamples, Shader::ShaderReturnType returnType,
		Shader::ShaderDimensions dimensions, size_t bindPoint=0, size_t bindCount=0);

	Shader::ShaderReturnType GetReturnType() const;
	Shader::ShaderDimensions GetShaderDimensions() const;
	size_t GetNumSamples() const;

protected:
	CShaderTexture();

	Shader::ShaderReturnType m_returnType;	// Return type (if texture)
	Shader::ShaderDimensions m_dimension;	// Dimension (if texture)
	size_t m_numSamples;
};

#include "ConstantFormat.inl"

_ENGINE_END

#endif