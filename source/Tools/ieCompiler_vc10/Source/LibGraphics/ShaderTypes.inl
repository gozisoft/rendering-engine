#ifndef __SHADER_TYPES_INL__
#define __SHADER_TYPES_INL__


inline size_t SizeOfType(VariableType type)
{
	switch (type)
	{
	case VT_BYTE:
	case VT_UBYTE:
		return sizeof(Int8);
	case VT_SHORT:
	case VT_USHORT:
		return sizeof(Int16);
	case VT_INT:
	case VT_UINT:
		return sizeof(Int32);
	case VT_FLOAT:
		return sizeof(float);
	case VT_DOUBLE:
		return sizeof(double);
	default:
		assert(false && ieS("Unknown variable type: SizeOfType()"));
		return 0;
	};
}



#endif