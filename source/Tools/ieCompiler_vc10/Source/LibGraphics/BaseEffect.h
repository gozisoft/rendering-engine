#ifndef __CCONTEXT_H__
#define __CCONTEXT_H__

#include "Technique.h"

_ENGINE_BEGIN

// Effect class consits of a number of different effect techniques
class CBaseEffect
{
public:
	typedef std::vector<CSceneTechnique*> TechniqueArray;

	CBaseEffect();
	CBaseEffect(const TechniqueArray& techs);

	~CBaseEffect();

	// Access a member of TechniquesArray
	size_t GetNumTechniques() const;
	const CSceneTechnique* GetTechnique(size_t techniqueIndex) const;
	CSceneTechnique* GetTechnique(size_t techniqueIndex);

	// Lookup by the tech's name
	const CSceneTechnique* GetTechnique(const std::string& techName) const;
	CSceneTechnique* GetTechnique(const std::string& techName);

	// VisualPass access
	const CVertexShader* GetVertexShader(size_t techniqueIndex, size_t passIndex) const;
	const CPixelShader* GetPixelShader(size_t techniqueIndex, size_t passIndex) const;
	const CAlphaState* GetAlphaState(size_t techniqueIndex, size_t passIndex) const;
	const CCullState* GetCullState(size_t techniqueIndex, size_t passIndex) const;
	const CDepthState* GetDepthState(size_t techniqueIndex, size_t passIndex) const;
	const COffsetState* GetOffsetState(size_t techniqueIndex, size_t passIndex) const;
	const CStencilState* GetStencilState(size_t techniqueIndex, size_t passIndex) const;
	const CWireState* GetWireState(size_t techniqueIndex, size_t passIndex) const;

protected:
	// Array of specified techniques ( might be better using std::set? )
	TechniqueArray m_techniques;
};

#include "BaseEffect.inl"

_ENGINE_END

#endif