#ifndef __CCONSTANT_ELEMENT_H__
#define __CCONSTANT_ELEMENT_H__

#include "ShaderTypes.h"

_ENGINE_BEGIN

// m_offset ::
// Number of bytes to advance between elements.
// Typically a multiple of 16 for arrays, vectors, matrices.
// For scalars and small vectors/matrices, this can be 4 or 8.    
class CConstantElement
{
public:
	CConstantElement(const std::string& variableName, Shader::DataType dataType,
		Shader::VariableType variable, size_t rows, size_t columns,
		size_t offset, void* pDefaultValue);

	// Access a variable by name
	const std::string& GetVarName() const;

	// The class type: scalar, vector, matrix
	Shader::DataType GetDataType() const;

	// Returns the type of vertex, V_UNKNOWN = ~0, V_BYTE = 0, V_UBYTE,
	// VT_BOOL, VT_INT, V_UINT, VT_FLOAT, VT_DOUBLE.
	Shader::VariableType GetVariableType() const;

	// Number of rows of this element
	size_t GetRows() const;

	// Number of columns of this element
	size_t GetCols() const;

	// Get the offset of this element from the start of the buffer array
	size_t GetOffset() const;

	// Default value of the constant buffer
	void* GetDefaultValue() const;

protected:
	CConstantElement();

	std::string m_variableName;
	Shader::DataType m_dataType;
	Shader::VariableType m_variableType;
	size_t m_rows;
	size_t m_columns;
	size_t m_offset;
	void* m_pDefaultValue;	
};



#include "ConstantElement.inl"

_ENGINE_END

#endif



/*
class CConstantElement
{
public:
	CConstantElement(const std::string& name, VariableType type, size_t offset);
	~CConstantElement();

	// Retirieve the constant variables name
	const std::string& GetName() const;

	// Returns the type of vertex, V_UNKNOWN = ~0, V_BYTE = 0, V_UBYTE,
	// VT_BOOL, VT_INT, V_UINT, VT_FLOAT, VT_DOUBLE.
	VariableType GetVariableType() const;

	// Used to distinguish where the element lies within the array
	size_t GetOffset() const;

private:
	std::string	m_name;
	VariableType m_variableType;
	size_t m_offset;
};
*/