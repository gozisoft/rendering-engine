#ifndef NET_FORWARD_H
#define NET_FORWARD_H

#include "Core.h"

_ENGINE_BEGIN

// Shader descriptors
class CShaderVariable;
class CShaderTexture;
class CConstantElement;
class CConstantFormat;
class CVertexElement;
class CVertexFormat;

typedef std::shared_ptr<CShaderVariable> CShaderVariablePtr;
typedef std::shared_ptr<CShaderTexture> CShaderTexturePtr;
typedef std::shared_ptr<CConstantFormat> CConstantFormatPtr;
typedef std::shared_ptr<CConstantElement> CConstantElementPtr;
typedef std::shared_ptr<CVertexElement> CVertexElementPtr;
typedef std::shared_ptr<CVertexFormat> CVertexFormatPtr;

// Shaders (generic)
class CShader;
class CVertexShader;
class CPixelShader;

typedef std::shared_ptr<CShader> CShaderPtr;
typedef std::shared_ptr<CVertexShader> CVertexShaderPtr;
typedef std::shared_ptr<CPixelShader> CPixelShaderPtr;

// States (generic)
class CGlobalState;
class CAlphaState;
class CCullState;
class CDepthState;
class COffsetState;
class CStencilState;
class CWireState;
class CZBufferState;

typedef std::shared_ptr<CGlobalState> CGlobalStatePtr;
typedef std::shared_ptr<CAlphaState> CAlphaStatePtr;
typedef std::shared_ptr<CCullState> CCullStatePtr;
typedef std::shared_ptr<CDepthState> CDepthStatePtr;
typedef std::shared_ptr<COffsetState> COffsetStatePtr;
typedef std::shared_ptr<CStencilState> CStencilStatePtr;
typedef std::shared_ptr<CWireState> CWireStatePtr;
typedef std::shared_ptr<CZBufferState> CZBufferStatePtr;

// Effect model
class CVisualPass;
class CSceneTechnique;
class CBaseEffect;
class IEffectLoader;

typedef std::shared_ptr<CBaseEffect> CBaseEffectPtr;
typedef std::shared_ptr<IEffectLoader> IEffectLoaderPtr;


_ENGINE_END

#endif