#include "frameafx.h"
#include "XMLCompiler.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include "Technique.h"
#include "VertexFormat.h"
#include "ConstantFormat.h"
#include "BaseEffect.h"

using namespace Engine;
using namespace Detail;

using boost::property_tree::iptree; 
using boost::property_tree::xml_parser::no_comments;
using boost::property_tree::xml_parser::no_concat_text;
using std::for_each;
using std::make_shared;

Shader::DataType GetDataType(const std::string& ctype)
{
	if (ctype == "matrix")
		return Shader::DT_MATRIX;
	if (ctype == "vector")
		return Shader::DT_VECTOR;
	if (ctype == "scalar")
		return Shader::DT_SCALAR;
	else
	{
		assert( false && ieS("Unknown ClassType:XML") );
		Shader::DT_UNKNOWN;
	}
}

Shader::VariableType GetVariableType(const std::string& vtype)
{
	if (vtype == "bool")
		return Shader::VT_BOOL;
	if (vtype == "char")
		return Shader::VT_BYTE;
	if (vtype == "unsigned char")
		return Shader::VT_UBYTE;
	if (vtype == "short")
		return Shader::VT_SHORT;
	if (vtype == "unsigned short")
		return Shader::VT_USHORT;
	if (vtype == "int")
		return Shader::VT_INT;
	if (vtype == "unsigned int")
		return Shader::VT_UINT;
	if (vtype == "float")
		return Shader::VT_FLOAT;
	if (vtype == "double")
		return Shader::VT_DOUBLE;
	else
	{
		assert( false && ieS("Unknown VariableType:XML") );
		Shader::VT_UNKNOWN;
	}
}

Buffer::VertexType GetVertexType(const std::string& vtype)
{
	if (vtype == "float1" || vtype == "float")
		return Buffer::VT_FLOAT1;
	if (vtype == "float2")
		return Buffer::VT_FLOAT2;
	if (vtype == "float3")
		return Buffer::VT_FLOAT3;
	if (vtype == "float4")
		return Buffer::VT_FLOAT4;

	if (vtype == "int1" || vtype == "int")
		return Buffer::VT_INT1;
	if (vtype == "int2")
		return Buffer::VT_INT2;
	if (vtype == "int3")
		return Buffer::VT_INT3;
	if (vtype == "int4")
		return Buffer::VT_INT4;
	else
	{
		assert( false && ieS("Unknown VertexType:XML") );
		return Buffer::VT_UNKNOWN;
	}
}

CXMLCompiler::CXMLCompiler()
{

}

CBaseEffectPtr CXMLCompiler::LoadShader(const std::string& headerName)
{
	// Create an empty property tree object
	iptree pt;
	CBaseEffect::ConstantFormats constants;
	CBaseEffect::TechniqueArray techniques;

	// Load the XML file into the property tree. If reading fails
	// (cannot open file, parse error), an exception is thrown.
	int flags = no_comments;
	read_xml(headerName, pt, flags);

	// Parse over the shader variables
	iptree& shaderLevel = pt.get_child("Shader");
	BOOST_FOREACH(auto level, shaderLevel)
	{
		if (level.first == "Constants")
		{
			BuildConstantFormats(level.second, constants);
		}
		else if (level.first == "Contexts")
		{
			// Obtain a map of the shaders to build
			BuildMap shadersToCompile;
			BuildContext(level.second, shadersToCompile);	

			// # Important not to use level.second for finding the functions to compile.
			for (auto i = shadersToCompile.begin(); i != shadersToCompile.end(); ++i) 
			{
				CompileShader(shaderLevel, *i);
			}
		}
	}

	// Create the base effect and return it
	return make_shared<CBaseEffect>(techniques, constants);
}

void CXMLCompiler::BuildConstantFormats(iptree &itorLevel, CBaseEffect::ConstantFormats& constants)
{
	using std::string;

	// iterate over the constants
	BOOST_FOREACH( auto constant, itorLevel.get_child("") )
	{	
		CConstantFormat* pFormat = nullptr;

		if (constant.first == "CBuffer")
		{
			// Go over the leaves of the constant buffer
			BOOST_FOREACH( auto leaf, constant.second.get_child("CBuffer.<xmlattr>") )
			{
				SConstantBuffferDesc bufferDesc;
				if (leaf.first == "name")
				{
					bufferDesc.bufferName = leaf.second.get<string>("");
				}

				pFormat = new CConstantFormat(bufferDesc.bufferName, 0);
			}

			// Now go over the variables
			BOOST_FOREACH( iptree::value_type& leaf, itorLevel.get_child("CBuffer") )
			{
				if (leaf.first == "Variable")
				{
					SConstantElementDesc elementDesc;	
					BOOST_FOREACH( iptree::value_type& leaf, leaf.second.get_child("<xmlattr>") )
					{
						if (leaf.first == "name")
							elementDesc.variableName = leaf.second.get<string>("");

						if (leaf.first == "class")
							elementDesc.dataType = GetDataType(leaf.second.get<string>(""));

						if (leaf.first == "type")
							elementDesc.variableType = GetVariableType(leaf.second.get<string>(""));

						if (leaf.first == "rows")
							elementDesc.rows = leaf.second.get<size_t>("");

						if (leaf.first == "cols")	
							elementDesc.columns = leaf.second.get<size_t>("");	
					}
					// Push back the built up element desc
					pFormat->PushElement(elementDesc.variableName, elementDesc.dataType,
						elementDesc.variableType, elementDesc.rows, elementDesc.columns);
				}
			}

			// Push back the completed constant
			constants.push_back(pFormat);
		}
	}
}

void CXMLCompiler::CompileShader(iptree &itorLevel, SBuildTuple& buildTuple)
{
	typedef iptree::value_type& valueType;

	// The shader that will be build
	CShader* pShader = nullptr;

	// Get the name of the shader
	iptree& shaderLevel = itorLevel.get_child(buildTuple.shaderBuild);
	BOOST_FOREACH(valueType shader, shaderLevel)
	{
		if (shader.first == "<xmlattr>")
		{
			// Determine which type of shader to create
			BOOST_FOREACH( valueType attribute, shader.second.get_child("") )
			{
				if (attribute.first == "name")
				{
					if (attribute.second.get<std::string>("") == 
						buildTuple.entryPoint)
					{
						// Assign the shaders program name
						if (buildTuple.shaderBuild == "Vertex")
						{
							pShader = new CVertexShader(buildTuple.entryPoint);
						}
						else if (buildTuple.shaderBuild == "Pixel")
						{
							pShader = new CPixelShader(buildTuple.entryPoint);
						}
					}
				}
			}
		}
		else if (shader.first == "Inputs")
		{
			// Quick error check
			if (!pShader)
				break;

			// Build the input layout
			CVertexFormatPtr pInput = make_shared<CVertexFormat>();
			pShader->SetInputFormat(pInput);

			// Iterate over the <Input> layer
			BOOST_FOREACH( valueType attribute, shader.second.get_child("") )
			{
				assert (attribute.first == "Input");

				// Element desc [type, semantic, index]
				SVertexElementDesc desc;

				// Build the element desc
				BOOST_FOREACH( valueType leaf, attribute.second.get_child("<xmlattr>") )
				{
					if (leaf.first == "type")
						desc.vertexType = GetVertexType( leaf.second.get<std::string>("") );

					if (leaf.first == "value")
						desc.semantic = leaf.second.get<std::string>("");

					if (leaf.first == "semantic")
						desc.semanticIndex = leaf.second.get<size_t>("");		
				}

				// Push back the built up element desc
				pInput->AddElement(0, desc.vertexType, desc.semantic,
					desc.semanticIndex);
			}		
		}
		else if (shader.first == "Outputs")
		{
			// Quick error check
			if (!pShader)
				break;
			
			// Build the output layout
			CVertexFormatPtr pOutput = make_shared<CVertexFormat>();
			pShader->SetOutputFormat(pOutput);

			// Iterate over the <Output> layer
			BOOST_FOREACH( valueType attribute, shader.second.get_child("") )
			{
				// make sure this is an output next
				assert (attribute.first == "Output");

				// Element desc [type, semantic, index]
				SVertexElementDesc desc;

				// Build the element desc
				BOOST_FOREACH( valueType leaf, attribute.second.get_child("<xmlattr>") )
				{
					if (leaf.first == "type")
						desc.vertexType = GetVertexType( leaf.second.get<std::string>("") );

					if (leaf.first == "value")
						desc.semantic = leaf.second.get<std::string>("");

					if (leaf.first == "semantic")
						desc.semanticIndex = leaf.second.get<size_t>("");		
				}

				// Push back the built up element desc
				pOutput->AddElement(0, desc.vertexType, desc.semantic,
					desc.semanticIndex);
			}
		}
		else if (shader.first == "HLSL") 
		{
			// Quick error check
			if (!pShader)
				break;
			
			// Load in the shader Text
			const std::string& shaderProgram = shader.second.get<std::string>("");
			pShader->SetShaderProgram(shaderProgram);
		}
	}

	// Check for a valid shader first
	if (!pShader)
	{
		assert(pShader && ieS("Shader was not found CompileShader()") );
		return;
	}

	// Get the index to the technique
	size_t techIndex = buildTuple.passIndex;
	CVisualPass* pPass = buildTuple.pTechnique->GetPass(techIndex);

	// ensure the pass is valud
	assert(pPass);

	// Assign the shader within the pass
	switch ( pShader->GetShaderType() )
	{
	case CShader::ST_VERTEX:
		pPass->m_pVertexShader = static_cast<CVertexShader*>(pShader);
		break;
	case CShader::ST_PIXEL:
		pPass->m_pPixelShader = static_cast<CPixelShader*>(pShader);
		break;
	};
}

void CXMLCompiler::BuildContext(iptree &itorLevel, BuildMap& buildMapOut)
{
	typedef iptree::value_type& valueType;

	// Go over the context children
	iptree& contextLevel = itorLevel.get_child("");
	BOOST_FOREACH(valueType contexts, contextLevel)
	{
		if (contexts.first != "Context")
		{		
			// Somthing went wrong here, no context avaiable
			assert(false);
			break;
		}

		// This tuple will be constructed immediatly
		SBuildTuple tuple;

		// Go over each context
		BOOST_FOREACH(valueType context, contexts.second.get_child("") )
		{
			if (context.first == "<xmlattr>")
			{
				// Go over the attributes of the context
				BOOST_FOREACH( auto leaf, context.second.get_child("") )
				{		
					if (leaf.first == "name")
					{
						// Create the technique
						auto techName = context.second.get<std::string>("");
						tuple.pTechnique = new CSceneTechnique(techName);
					}
				}
			}
			else if (context.first == "Pass")
			{
				// Create the visual pass and add it to the technique
				CVisualPass* pPass = new CVisualPass();
				tuple.pTechnique->AddVisualPass(pPass);

				// Now go over the pass leaves
				BOOST_FOREACH(valueType pass, context.second.get_child("") )
				{
					if (pass.first == "<xmlattr>")
					{
						// Get the pass index (used to look up the pass within the tech)
						BOOST_FOREACH( auto attribute, pass.second.get_child("") )
						{
							assert(attribute.first == "index");
							tuple.passIndex = attribute.second.get<size_t>("");
						}						
					}
					else if (pass.first == "Compile")
					{
						BOOST_FOREACH( valueType compile, pass.second.get_child("") )
						{
							BOOST_FOREACH( valueType attribute, compile.second.get_child("") )
							{
								// Quick error check
								if (attribute.first == "shader")
								{
									tuple.shaderBuild = attribute.second.get<std::string>("");
								}							
								else if (attribute.first == "function")
								{
									tuple.entryPoint = attribute.second.get<std::string>("");
								}			
							}
		
							// Push back the tuple
							buildMapOut.push_back(tuple);

						} // <Compile>
					}
				} // <Pass>
			} 
		} // <Contexts>

	} // contextLevel

	// Make sure the build map is not empty
	assert( buildMapOut.empty() != true );
}






//// tags
//struct SLinearTag { };
//struct SShaderBuildTag { };
//struct SEntryPointTag { };
//
//// typedefs for multi_index
//typedef boost::multi_index::sequenced< 
//	boost::multi_index::tag<SLinearTag>
//> LinearIndex;
//
//typedef boost::multi_index::hashed_unique<
//	boost::multi_index::tag<SShaderBuildTag>,
//	boost::multi_index::member<SBuildTuple, ShadersToBuild, &SBuildTuple::ShaderBuild> 
//> ShaderBuildIndex;
//
//typedef boost::multi_index::multi_index_container <
//	SBuildTuple,
//	boost::multi_index::indexed_by <
//	LinearIndex,
//	ShaderBuildIndex
//	> // index_by
//> BuildTupleContainer;













// Read to the <Effects> portion
//	ptree &itorEffects = pt.get_child( ieS("Effects") );

	//// Get the <Effect> attributes
	//BOOST_FOREACH( ptree::value_type &v, pt.get_child( ieS("Effects.Effect.<xmlattr>") ) )
	//{   
	//	std::string attributeName = v.first;
	//	std::string attributeValue = v.second.get<std::string>( ieS("") );
	//}

	//// Read to the <Functions> portion
	//ptree &itorFunctions = pt.get_child("Effects.Effect.Functions");

	//// Get the <Function> attributes
	//BOOST_FOREACH( ptree::value_type &v, itorFunctions.get_child( ieS("Function.<xmlattr>") ) )
	//{   
	//	std::string attributeName = v.first;
	//	std::string attributeValue = v.second.get<std::string>( ieS("") );
	//}



	//// Read to the passes

	//// Read to the <Programs> portion
	//ptree &itorPrograms = pt.get_child( ieS("Programs") );

	//// Get the <Program> attributes
	//BOOST_FOREACH( ptree::value_type &v, itorPrograms.get_child( ieS("Program.<xmlattr>") ) )
	//{   
	//	std::string attributeName = v.first;
	//	std::string attributeValue = v.second.get<std::string>( ieS("") );

	//	// Program not used in .fx file
	//	if ( attributeValue == ieS("NULL") || attributeValue == ("0") )
	//	{
	//		continue;
	//	}

	//	// Set up the function
	//	Function shaderFunction;
	//	shaderFunction.type = attributeName; // VertexShader or PixelShader
	//	shaderFunction.name = attributeValue; // Null or asm

	//	// Set up the input and output variables
	//	ptree &itorInputs = pt.get_child( ieS("Inputs") );
	//	itorInputs = itorInputs.get_child( ieS("Input.<xmlattr>") );
	//	BOOST_FOREACH( ptree::value_type &v1, itorInputs )
	//	{
	//		Input inputVar;
	//		inputVar.name = v1.first;


	//	}
	//}




//void BuildShaderIO(iptree &itorLevel, CVertexFormatPtr& pInput, CVertexFormatPtr& pOutput)
//{
//	BOOST_FOREACH(iptree::value_type& vertex, itorLevel.get_child("Vertex") )
//	{	
//		if (vertex.first == "Program")
//		{
//			BOOST_FOREACH(auto program, itorLevel.get_child("Vertex.Program.<xmlattr>") )
//			{
//
//
//
//			}
//
//			// Now go over the Inputs
//			SVertexElementDesc desc;
//			BOOST_FOREACH( auto inputs, itorLevel.get_child("Vertex.Program.Inputs") )
//			{
//				if (inputs.first == "Input")
//				{
//					pInput = make_shared<CVertexFormat>();
//					BOOST_FOREACH( auto leaf, inputs.second.get_child("<xmlattr>") )
//					{
//						if (leaf.first == "type")
//							desc.vertexType = GetVertexType( leaf.second.get<std::string>("") );
//
//						if (leaf.first == "value")
//							desc.semantic = GetVSemantic( leaf.second.get<std::string>("") );
//
//						if (leaf.first == "semantic")
//							desc.semanticIndex = leaf.second.get<size_t>("");		
//					}
//
//					// Push back the built up element desc
//					pInput->AddElement(0, desc.vertexType, desc.semantic,
//						desc.semanticIndex);
//				}
//			}
//		}
//	}
//}