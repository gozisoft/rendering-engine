#ifndef __CFX_COMPILER_H__
#define __CFX_COMPILER_H__

#include "IEffectLoader.h"
#include "ShaderTypes.h"
#include "VertexTypes.h"
#include "BaseEffect.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

_ENGINE_BEGIN

class CFXCompiler : public IEffectLoader
{
public:
	typedef std::vector<CSceneTechnique*> TechniqueArray;
	typedef std::map<size_t, CSceneTechnique*> TechniqueMap; 
	typedef boost::property_tree::iptree iptree;
	typedef boost::property_tree::wiptree wiptree;

	CFXCompiler();
	~CFXCompiler();

	CBaseEffectPtr LoadEffect(std::istream& inputStream);
	CBaseEffectPtr LoadEffect(std::wistream& inputStream);

protected:
	void CompileShader(iptree &itorLevel, CShader* pShader);
	void CompileHLSLShader(CShader* pShader);
	void BuildContext(iptree &itorLevel, TechniqueArray& uniqueTechs, TechniqueMap& techMap);

};




_ENGINE_END

#endif