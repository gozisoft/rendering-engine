#include "frameafx.h"
#include "ShaderVariable.h"

using namespace Engine;
using namespace Shader;

CShaderVariable::CShaderVariable(const std::string& name, ClassType classType, 
	size_t bindPoint, size_t bindCount) : 
m_resourceName(name),
m_classType(classType),
m_bindPoint(bindPoint),
m_bindCount(bindCount)
{

}

CShaderVariable::~CShaderVariable()
{
	
}
