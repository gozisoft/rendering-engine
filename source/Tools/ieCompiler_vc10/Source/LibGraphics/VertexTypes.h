#ifndef __VERTEX_TYPES_H__
#define __VERTEX_TYPES_H__

#include "Core.h"

_ENGINE_BEGIN

namespace Buffer
{

// ----------------------------------------------------------------------------------
// VERTEX
// ----------------------------------------------------------------------------------
enum VertexType
{ 
	VT_UNKNOWN = ~0,
	VT_BYTE1 = 0,
	VT_BYTE2,
	VT_BYTE4,
	VT_UBYTE1,
	VT_UBYTE2,
	VT_UBYTE4,
	VT_SHORT1,
	VT_SHORT2,
	VT_SHORT4,
	VT_USHORT1,
	VT_USHORT2,
	VT_USHORT4,
	VT_INT1,
	VT_INT2,
	VT_INT3,
	VT_INT4,
	VT_UINT1,
	VT_UINT2,
	VT_UINT3,
	VT_UINT4,
	VT_FLOAT1,
	VT_FLOAT2,
	VT_FLOAT3,
	VT_FLOAT4,
	NUM_VERTEX_TYPES
};

enum VertexSemantic 
{
	VS_UNKNOWN = ~0,
	VS_POSITION,				// attr 0
	VS_NORMAL,					// attr 1
	VS_TANGENT,					// attr 2
	VS_BINORMAL,				// attr 3							
	VS_COLOUR,					// attr 4-5
	VS_BLEND_INDICES,			// attr 6
	VS_BLEND_WEIGHTS,			// attr 7
	VS_TEXTURE_COORDINATES,		// attr 8-15
	VS_FOG,						// attr 16
	VS_PSIZE,					// attr 17
	NUM_VERTEX_SEMANTICS
};

// Size function
size_t SizeOfType(VertexType type);
size_t ChannelCount(VertexType type);

// ----------------------------------------------------------------------------------
// INDEX
// ----------------------------------------------------------------------------------
// indices can either be 16 bit 32 bit
enum IndexType
{
	IT_USHORT,
	IT_UINT
};

template < Buffer::IndexType T > struct EnumIType2Type	  { typedef void type;   };
template <> struct EnumIType2Type<IT_USHORT>	  { typedef UShort type; };
template <> struct EnumIType2Type<IT_UINT>		  { typedef UInt32 type; };

// Size function
size_t SizeOfType(IndexType type);

#include "VertexTypes.inl"

}


_ENGINE_END



#endif




	/*
	enum Buffer::VertexType
	{ 
	V_UNKNOWN = ~0,
	V_BYTE = 0,
	V_UBYTE, 
	V_SHORT,
	V_USHORT1, V_USHORT2,	V_USHORT3,	V_USHORT4,
	V_INT1,		V_INT2,		V_INT3,		V_INT4,
	V_UINT1,	V_UINT2,	V_UINT3,	V_UINT4,
	V_FLOAT1,	V_FLOAT2,	V_FLOAT3,	V_FLOAT4
	V_DOUBLE 
	};
	*/



	/*

	template < typename T > struct Type2EnumType	  { enum { type = V_UNKNOWN }; };
	template <> struct Type2EnumType<Int8>            { enum { type = V_BYTE }; };
	template <> struct Type2EnumType<UInt8>			  { enum { type = V_UBYTE }; };
	template <> struct Type2EnumType<Short>           { enum { type = V_SHORT }; };
	template <> struct Type2EnumType<UShort>		  { enum { type = V_USHORT }; };
	template <> struct Type2EnumType<Int>             { enum { type = V_INT }; };
	template <> struct Type2EnumType<UInt>			  { enum { type = V_UINT }; };
	template <> struct Type2EnumType<float>           { enum { type = V_FLOAT }; };
	template <> struct Type2EnumType<double>          { enum { type = V_DOUBLE }; };

	template < Buffer::VertexType T > struct EnumVType2Type	  { typedef void type; };
	template <> struct EnumVType2Type<V_BYTE>		  { typedef Int8 type; };
	template <> struct EnumVType2Type<V_UBYTE>		  { typedef UInt8 type; };
	template <> struct EnumVType2Type<V_SHORT>		  { typedef Short type; };
	template <> struct EnumVType2Type<V_USHORT>		  { typedef UShort type; };
	template <> struct EnumVType2Type<V_INT>		  { typedef Int type; };
	template <> struct EnumVType2Type<V_UINT>		  { typedef UInt type; };
	template <> struct EnumVType2Type<V_FLOAT>		  { typedef float type; };
	template <> struct EnumVType2Type<V_DOUBLE>		  { typedef double type; };

	*/