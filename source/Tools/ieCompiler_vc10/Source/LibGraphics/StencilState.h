#ifndef __CSTENCIL_STATE_H__
#define __CSTENCIL_STATE_H__

#include "Core.h"

namespace Engine
{


class CStencilState
{
public:
	// Stenticl comparision.
	enum CompareMode
	{
		CM_NEVER,
		CM_LESS,
		CM_EQUAL,
		CM_LEQUAL,
		CM_GREATER,
		CM_NOTEQUAL,
		CM_GEQUAL,
		CM_ALWAYS,
		NUM_CMP_MODES
	};

	enum OperationMode
	{
		OM_KEEP,
		OM_ZERO,
		OM_REPLACE,
		OM_INCREMENT,
		OM_DECREMENT,
		OM_INVERT,
		NUM_OP_MODES
	};

public:
	CStencilState();
	CStencilState(bool enable,
		CompareMode compare,
		UInt32 ref,
		UInt32 mask,
		UInt32 writeMask,
		OperationMode onFail,
		OperationMode onZFail,
		OperationMode onZPass);

	~CStencilState();

	bool m_enabled;            // default: false
	CompareMode m_compare;     // default: CM_NEVER
	UInt32 m_reference;		   // default: 0
	UInt32 m_mask;			   // default: UINT_MAX (0xFFFFFFFF)
	UInt32 m_writeMask;		   // default: UINT_MAX (0xFFFFFFFF)
	OperationMode m_onFail;    // default: OT_KEEP
	OperationMode m_onZFail;   // default: OT_KEEP
	OperationMode m_onZPass;   // default: OT_KEEP

private:
	DECLARE_HEAP;

};




} // namespace Engine



#endif