#include "frameafx.h"
#include "ShaderSampler.h"

using namespace Engine;


CShaderSampler::CShaderSampler(const std::string& name,	size_t bindPoint, size_t bindCount) : 
CShaderVariable(name, Shader::CT_SAMPLER, bindPoint, bindCount),
m_lodBias(0.0f),
m_anisotropy(0.0f)
{

}