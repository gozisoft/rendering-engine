#ifndef __CVERTEX_SHADER_H__
#define __CVERTEX_SHADER_H__

#include "Shader.h"

_ENGINE_BEGIN

class CVertexShader : public CShader
{
public:
	CVertexShader(const std::string& functionName, ShaderVersion version);
	~CVertexShader();

};

_ENGINE_END


#endif