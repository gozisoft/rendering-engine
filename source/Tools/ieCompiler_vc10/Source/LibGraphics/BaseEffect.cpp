#include "frameafx.h"
#include "BaseEffect.h"
#include "Technique.h"
#include "Helper.h"

using namespace Engine;

CBaseEffect::CBaseEffect()
{


}

CBaseEffect::CBaseEffect(const TechniqueArray& techs) : 
m_techniques(techs)
{


}

CBaseEffect::~CBaseEffect()
{
	// Delete all the scene techniques
	for (auto i = m_techniques.begin(); i != m_techniques.end(); ++i)
	{
		SafeDelete(*i);
	}
}

const CSceneTechnique* CBaseEffect::GetTechnique(const std::string& techName) const
{
	auto itor = std::find_if(m_techniques.begin(), m_techniques.end(), [techName] (CSceneTechnique* tech)
	{
		return tech->GetName() == techName;
	} );

	assert( itor != m_techniques.end() );
	return *itor;
}
