#include "Frameafx.h"
#include "VertexElement.h"

using namespace Engine;
using namespace Buffer;

DEFINE_HEAP(CVertexElement, "CVertexElement");

CVertexElement::CVertexElement(UInt sourceIndex, UInt offset,
	VertexType type, const std::string& semantic, UInt semanticIndex) :
m_streamIndex(sourceIndex),
m_stride(offset),
m_vertexType(type),
m_semantic(semantic),
m_semanticIndex(semanticIndex)
{
}