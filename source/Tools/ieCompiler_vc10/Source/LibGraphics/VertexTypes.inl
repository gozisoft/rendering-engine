#ifndef __VERTEX_TYPES_INL__
#define __VERTEX_TYPES_INL__


inline size_t SizeOfType(VertexType type)
{
	switch (type)
	{
	case VT_BYTE1:
	case VT_UBYTE1:
	case VT_BYTE2:
	case VT_UBYTE2:
	case VT_BYTE4:
	case VT_UBYTE4:
		return sizeof(UInt8) * ChannelCount(type);

	case VT_SHORT1:
	case VT_USHORT1:
	case VT_SHORT2:
	case VT_USHORT2:
	case VT_SHORT4:
	case VT_USHORT4:
		return sizeof(UInt16) * ChannelCount(type);
			
	case VT_INT1:
	case VT_UINT1:
	case VT_INT2:
	case VT_UINT2:
	case VT_INT3:
	case VT_UINT3:	
	case VT_INT4:
	case VT_UINT4:
		return sizeof(UInt32) * ChannelCount(type);

	case VT_FLOAT1:
	case VT_FLOAT2:
	case VT_FLOAT3:
	case VT_FLOAT4:
		return sizeof(float) * ChannelCount(type);

	default:
		assert( false && ieS("Unkown Buffer::VertexType type : SizeOfType()") );
		return 0;
	};

}

inline size_t ChannelCount(VertexType type)
{
	switch (type)
	{
	case VT_BYTE1:
	case VT_UBYTE1:
	case VT_SHORT1:
	case VT_USHORT1:
	case VT_INT1:
	case VT_UINT1:
	case VT_FLOAT1:
		return 1;

	case VT_BYTE2:
	case VT_UBYTE2:
	case VT_SHORT2:
	case VT_USHORT2:
	case VT_INT2:
	case VT_UINT2:
	case VT_FLOAT2:
		return 2;

	case VT_INT3:
	case VT_UINT3:
	case VT_FLOAT3:
		return 3;

	case VT_BYTE4:
	case VT_UBYTE4:
	case VT_SHORT4:
	case VT_USHORT4:
	case VT_INT4:
	case VT_UINT4:
	case VT_FLOAT4:
		return 4;

	default:
		assert( false && ieS("Unkown Buffer::VertexType type : ChannelCount()") );
		return 0;
	};
}

inline size_t SizeOfType(IndexType type)
{
	switch(type)
	{
	case IT_USHORT:
		return sizeof(UInt16);
	case IT_UINT:
		return sizeof(UInt32);
	default:
		assert( false && ieS("Unkown IndexType type : SizeOfType()") );
		return 0;
	};
}




#endif