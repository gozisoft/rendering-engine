#ifndef __CVERTEX_ELEMENT_INL__
#define __CVERTEX_ELEMENT_INL__

inline UInt CVertexElement::GetStreamIndex() const
{
	return m_streamIndex;
}

inline UInt CVertexElement::GetStride() const
{
	return m_stride;
}

inline Buffer::VertexType CVertexElement::GetVertexType() const
{
	return m_vertexType;
}

inline const std::string& CVertexElement::GetVertexSemantic() const
{
	return m_semantic;
}

inline UInt CVertexElement::GetSemanticIndex() const
{
	return m_semanticIndex;
}

inline UInt CVertexElement::GetVertexChannels() const
{
	return Buffer::ChannelCount(m_vertexType);
}

#endif