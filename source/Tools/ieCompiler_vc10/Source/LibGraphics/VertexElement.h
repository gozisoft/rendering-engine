#ifndef __CVERTEX_ELEMENT_H__
#define __CVERTEX_ELEMENT_H__

#include "VertexTypes.h"

_ENGINE_BEGIN

class CVertexElement
{
public:
	CVertexElement(UInt sourceIndex, UInt stride, Buffer::VertexType type,
		const std::string& semantic, UInt semanticIndex = 0);

	UInt GetStreamIndex() const;

	// Stride between elements
	UInt GetStride() const;

	// Returns the type of vertex, V_UNKNOWN = ~0, V_BYTE = 0, V_UBYTE,
	// V_SHORT, V_USHORT, V_INT, V_UINT, V_FLOAT, V_DOUBLE.
	Buffer::VertexType GetVertexType() const;

	// Vertex semantic is a text string to match the input assembler of 
	// a vertex shader.
	const std::string& GetVertexSemantic() const;

	// Returns the number of vertex channels. These range from 1 - 4.
	UInt GetVertexChannels() const;

	// Used to distinguish if there are more than one type of semantic 
	// i.e.
	// semantic = Texcoord, semantic index = 0
	// semantic = Texcoord, semantic index = 1
	UInt GetSemanticIndex() const;

private:
	UInt m_streamIndex;
	UInt m_stride;
	UInt m_semanticIndex;
	Buffer::VertexType m_vertexType;
	std::string m_semantic;

private:
	DECLARE_HEAP;
};


#include "VertexElement.inl"


_ENGINE_END



#endif