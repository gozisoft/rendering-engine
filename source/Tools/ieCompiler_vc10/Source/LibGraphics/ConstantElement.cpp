#include "frameafx.h"
#include "ConstantElement.h"

using namespace Engine;
using namespace Shader;

CConstantElement::CConstantElement(const std::string& name, DataType dataType, VariableType variable,
	size_t rows, size_t columns, size_t offset, void* pValue) :
m_dataType(dataType),
m_variableType(variable),
m_rows(rows),
m_columns(columns),
m_offset(offset),
m_pDefaultValue(pValue)
{

}

