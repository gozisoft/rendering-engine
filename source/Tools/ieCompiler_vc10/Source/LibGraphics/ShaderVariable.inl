#ifndef __CSHADER_VARIABLE_INL__
#define __CSHADER_VARIABLE_INL__

// ----------------------------------------------------------------------------------------------
// CShaderVariable
// ----------------------------------------------------------------------------------------------
inline const std::string& CShaderVariable::GetVariableName() const
{
	return m_resourceName;
}

inline Shader::ClassType CShaderVariable::GetClassType() const
{
	return m_classType;
}

inline size_t CShaderVariable::GetBindPoint() const
{
	return m_bindPoint;
}

inline size_t CShaderVariable::GetBindCount() const
{
	return m_bindCount;
}

#endif