#ifndef __IEFFECT_LOADER_H__
#define __IEFFECT_LOADER_H__

#include "framefwd.h"

_ENGINE_BEGIN

class IEffectLoader
{
public:
	virtual ~IEffectLoader() {} 

	enum LoaderType
	{
		LT_XML
	};

	static IEffectLoaderPtr CreateEffectLoader(LoaderType type = LT_XML);

	virtual CBaseEffectPtr LoadEffect(std::istream& inputStream) = 0;
	virtual CBaseEffectPtr LoadEffect(std::wistream& inputStream) = 0;
};


void LoadEffectFromDisk(const std::string& fileName);
void LoadEffectFromDisk(const std::wstring& filename);


_ENGINE_END

#endif