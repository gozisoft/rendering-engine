#ifndef __CSHADER_H__
#define __CSHADER_H__

#include "framefwd.h"

_ENGINE_BEGIN

// Base class for a shader.
// Can be of any kind of shader for example a vertex of pixel shader. 
// This design can be extended to include any new graphics pipeline shader types.
// In essence the shader is the function of a .fx file that attributed to processing
// things like vertices or pixels. They usually have input and output variables.
class CShader
{
public:
	// Sampler, Texture, ConstantBuffer etc
	typedef std::vector<CShaderVariable*> ShaderVarContainer;

	// Types of shader function supported
	enum ShaderType
	{
		ST_UNKNOWN = ~0,
		ST_VERTEX,
		ST_GEOMETRY,
		ST_PIXEL,
		NUM_SHADER_TYPES
	};

	// Support for .fx in HLSL and in GLSL
	enum FXTypes 
	{
		FT_UNKNOWN = ~0,
		FT_HLSL,
		FT_GLSL,
		NUM_FX_TYPES
	};

    // Shader profile information.
    enum ShaderVersion
    {
        SV_UNKNOWN = ~0,
        SV_2_0,
        SV_3_0,
		SV_4_0,
		SV_4_1,
		SV_5_0,
        SV_ARBVP1
    };

	virtual ~CShader();

	// Support for adding a single program
	void SetShaderProgram(const std::string& program);

	// Get the function name of the shader
	const std::string& GetFunctionName() const;

	// Access to the programs
	const std::string& GetProgram() const;

	// Used to determine the shader type.
	ShaderType GetShaderType() const;

	// Used to determine the shader version
	ShaderVersion GetShaderVersion() const;

	// Set the input format
	void SetInputFormat(CVertexFormatPtr pInput);

	// Set the output format
	void SetOutputFormat(CVertexFormatPtr pOutput);

	// Set the shader data marker
	void SetShaderMarker(void* pShaderMarker);

	// Set the shader variables
	void SetShaderVars(const ShaderVarContainer& variables);

protected:
	// Contstructor, usually filled in by a vertex or pixel shader.
	CShader(const std::string& functionName, ShaderVersion version, ShaderType type);

	// Shaders program in text
	std::string m_shaderProgram;

	// Function name
	std::string m_functionName;

	// shader descriptors
	ShaderVarContainer m_shaderVariables;

	// Input format
	CVertexFormatPtr m_pInput;

	// Output format
	CVertexFormatPtr m_pOutput;

	// Shader Marker
	void* m_pShaderMarker;

	// The type of shader
	ShaderType m_shaderType;

	// The shader version
	ShaderVersion m_shaderVersion;

};



#include "Shader.inl"


_ENGINE_END

#endif