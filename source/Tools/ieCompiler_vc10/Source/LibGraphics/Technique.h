#ifndef __CTECHNIQUE_H__
#define __CTECHNIQUE_H__

#include "framefwd.h"

_ENGINE_BEGIN

class CVisualPass
{
public:
	CVisualPass();
	~CVisualPass();

	// Does not own these objects there no deletor
	CVertexShader* m_pVertexShader;
	CPixelShader* m_pPixelShader;
	CAlphaState* m_pAlphaState;
    CCullState* m_pCullState;
    CDepthState* m_pDepthState;
    COffsetState* m_pOffsetState;
    CStencilState* m_pStencilState;
    CWireState* m_pWireState;
	CZBufferState* m_pZBufferState;
};

class CSceneTechnique
{
public:
	CSceneTechnique(const std::string contextName, const std::string& techVersion);
	~CSceneTechnique();

	// Adds a viaul pass to the array
	void AddVisualPass(CVisualPass* pPass);

	// Member access
    size_t GetNumPasses() const;
    const CVisualPass* GetPass(UInt passIndex) const;
	CVisualPass* GetPass(UInt passIndex);

	// VisualPass access
	const CVertexShader* GetVertexShader(UInt index) const;
	const CPixelShader* GetPixelShader(UInt index) const;
	const CAlphaState* GetAlphaState(UInt index) const;
	const CCullState* GetCullState(UInt index) const;
	const CDepthState* GetDepthState(UInt index) const;
	const COffsetState* GetOffsetState(UInt index) const;
	const CStencilState* GetStencilState(UInt index) const;
	const CWireState* GetWireState(UInt index) const;

	// Access to the tecnnique name
	const std::string& GetName() const;

	// Get the technique version
	const std::string& GetTechVersion() const;

protected:
	typedef std::vector<CVisualPass*> VisualPassArray;

	// An array of visual pass structures used to describe shaders
	// and their assoiciated states
	VisualPassArray m_visualPasses;

	// Name of the Context
	std::string m_contexName;

	// Shader version
	std::string m_techVersion;

};

#include "Technique.inl"

_ENGINE_END

#endif