#include "frameafx.h"
#include "IEffectLoader.h"
#include "FXCompiler.h"

_ENGINE_BEGIN

#if defined (UNICODE) || defined(_UNICODE)
	typedef std::basic_regex<wchar_t> _Regex;
	typedef std::match_results<const wchar_t *> _CMatch;
	typedef std::match_results<std::wstring::const_iterator> _SMatch;
#else
	typedef std::basic_regex<char> _Regex;
	typedef std::match_results<const char *> _CMatch;
	typedef std::match_results<std::string::const_iterator> _SMatch;
#endif

typedef _Regex Regex;
typedef _CMatch CMatch;
typedef _SMatch SMatch;



IEffectLoaderPtr IEffectLoader::CreateEffectLoader(LoaderType type)
{
	switch (type)
	{
	case LT_XML:
		return std::make_shared<CFXCompiler>();
	default:
		assert(false);
		return nullptr;
	};
}

void LoadEffectFromDisk(const std::string& fileName)
{
	// Search for file extension
	std::regex reg("/^.*\\.(xml|fx|effect)$/i");

	std::smatch match;
	if (std::regex_search(fileName, match, reg) == false)
	{
		assert(false && "Cannot find the correct file extension");
		return;
	}

	std::ifstream effectFile(fileName);
	IEffectLoaderPtr pEffectLoader = nullptr;

	//if ( match[1] == "xml" )
	//{
	//	pEffectLoader = std::make_shared<CFXCompiler>();
	//	pEffectLoader->LoadEffect(effectFile);
	//}
}

//void WLoadEffectFromDisk(const std::wstring& filename)
//{
//	// Search for file extension
//	std::wregex reg( L"/^.*\.(xml|fx|effect)$/i" );
//
//	std::wsmatch match;
//	if (std::regex_match(fileName, match, reg) == false)
//	{
//		assert( ieS("Cannot find the correct file extension") );
//		return;
//	}
//
//	if ( match[1] == L"xml" )
//	{
//		CFXCompiler.LoadEffectDisk(   )
//	}
//}

_ENGINE_END