#pragma once

#pragma comment( lib, "dxguid.lib" )
#pragma comment( lib, "d3dcompiler.lib" )

#include "targetver.h"

#define USING_DIRECT3D11

#include "frameheaders.h"