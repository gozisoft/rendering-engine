#include "Frameafx.h"
#include "VertexFormat.h"
#include "Helper.h"

using namespace Engine;
using namespace Buffer;

CVertexFormat::CVertexFormat() : 
m_totalStride(0)
{

}

CVertexFormat::CVertexFormat(const ElementContainer& elements, size_t totalStride) :
m_elements(elements),
m_totalStride(totalStride)
{
	if (m_totalStride == 0)
	{
		for (auto i = m_elements.begin(); i != m_elements.end(); ++i)
		{
			VertexType type = (*i)->GetVertexType();
			m_totalStride += SizeOfType(type);
		}
	}
}

CVertexFormat::~CVertexFormat()
{
	std::for_each(m_elements.begin(), m_elements.end(), [] (CVertexElement* pElement)
	{
		SafeDelete(pElement);
	} );

	m_elements.clear();
}

const CVertexElement* CVertexFormat::AddElement(UInt sourceIndex, VertexType type, const std::string& semantic,
	UInt semanticIndex)
{
	// Create the vertex attribute and set its data.
	m_elements.push_back( new CVertexElement(sourceIndex, m_totalStride, type, semantic, semanticIndex) );
	m_totalStride += SizeOfType(type);
	return m_elements.back();
}

const CVertexElement* CVertexFormat::InsertElement(UInt pos, UInt sourceIndex, VertexType type,
	const std::string& semantic, UInt index)
{
	if ( pos >= m_elements.size() )
		return AddElement(sourceIndex, type, semantic, index);

	// create the new vertex and increment the totalstride with the new attribute
	CVertexElement* pVertex = new CVertexElement(sourceIndex, m_totalStride, type, semantic, index);
	m_totalStride += SizeOfType(type);

	// itor = m_elements.insert( itor, pVertex );
	m_elements[pos] = pVertex;
	return pVertex;
}

void CVertexFormat::RemoveElement(UInt pos)
{
	assert(pos < m_elements.size());

	auto itor = m_elements.begin();
	for (UInt i = 0; i < pos; ++i)
		++itor;

	m_elements.erase(itor);
}

void CVertexFormat::RemoveElement(const std::string& semantic, UInt semanticIndex)
{
	for (auto itor = m_elements.begin(); itor != m_elements.end(); ++itor)
	{
		auto pTemp = (*itor);

		if (pTemp->GetVertexSemantic() == semantic &&  pTemp->GetSemanticIndex() == semanticIndex)
		{
			m_elements.erase(itor);
			return;
		}
	}
	
	assert( false && ieS("Could not find Semantic CVertexFormat::RemoveElement") );
}

UInt CVertexFormat::GetElementSemanticCount(const std::string& semantic) const 
{
	UInt i = 0;
	for (auto itor = m_elements.begin(); itor != m_elements.end(); ++itor)
	{
		auto pTemp = (*itor);

		if (pTemp->GetVertexSemantic() == semantic)
			++i;
	}
	return i;
}

const CVertexElement* CVertexFormat::GetElementBySemantic(const std::string& semantic, UInt semanticIndex) const
{
	for (auto itor = m_elements.begin(); itor != m_elements.end(); ++itor)
	{
		auto pTemp = (*itor);

		if (pTemp->GetVertexSemantic() == semantic && pTemp->GetSemanticIndex() == semanticIndex)
			return pTemp;
	}

	assert( false && ieS("Count not find semantic CVertexFormat::GetElementBySemantic") );
	return nullptr;
}









//CVertexFormat::CVertexFormat() : 
//m_totalStride(0),
//m_semanticFlags(0)
//{
//
//}
//
////CVertexFormat::CVertexFormat(UInt numElements) :
////m_totalStride(0), 
////m_elements( boost::make_tuple( numElements,
////			boost::multi_index::index<ElementContainer, Engine::semantic>::type::ctor_args(),
////			boost::multi_index::index<ElementContainer, Engine::semantic_and_index>::type::ctor_args()
////		) )
////{
//////	m_elements.resize(numElements, 
////}
//
//CVertexFormat::~CVertexFormat()
//{
//	
//}
//
//void CVertexFormat::AddElement(UInt sourceIndex, VertexType type, const std::string& vertexSemantic, UInt semanticIndex)
//{
//	typedef ElementContainer::index<linear>::type element_linear;
//	element_linear& index_type = m_elements.get<linear>();
//
//	// create the new vertex and increment the totalstride with the new attribute
//	CVertexElementPtr pVertex = std::make_shared<CVertexElement>(sourceIndex, m_totalStride, type, vertexSemantic, semanticIndex);
//	index_type.push_back(pVertex);
//	m_totalStride += SizeOfType(type);
//	SetEnabled(vertexSemantic, true);
//}
//
//void CVertexFormat::InsertElement(UInt pos, UInt sourceIndex, Buffer::VertexType type, const std::string& vertexSemantic, UInt index)
//{
//	if ( pos >= m_elements.size() || IsEnabled(vertexSemantic) )
//		return AddElement(sourceIndex, type, vertexSemantic, index);
//
//	typedef ElementContainer::index<linear>::type element_linear;
//	element_linear& index_type = m_elements.get<linear>();
//
//	auto itor = index_type.begin();
//	for (UInt i = 0; i < pos; ++i)
//		++itor;
//
//	// create the new vertex and increment the totalstride with the new attribute
//	CVertexElementPtr pVertex = std::make_shared<CVertexElement>(sourceIndex, m_totalStride, type, vertexSemantic, index);
//	m_totalStride += SizeOfType(type);
//	index_type.insert(itor, pVertex);
//}
//
//const CVertexElement* CVertexFormat::GetElementBySemantic(const std::string& semantic, UInt semanticIndex) const
//{
//	typedef ElementContainer::index<semantic_and_index>::type element_by_semantic_and_index;
//	const element_by_semantic_and_index& index_type = m_elements.get<semantic_and_index>();
//
//	auto itor = index_type.find( boost::make_tuple(semantic, semanticIndex) ); 
//	if ( itor != m_elements.get<semantic_and_index>().end() )
//	{
//		auto pElement = (*itor);
//		if ( pElement->GetVertexSemantic() == semantic && pElement->GetSemanticIndex() == semanticIndex )
//			return pElement;
//	}
//
//	return nullptr;
//}
//
//void CVertexFormat::RemoveElement(UInt pos)
//{
//	assert(pos < m_elements.size());
//
//	typedef ElementContainer::index<linear>::type element_by_linear;
//	element_by_linear& index_type = m_elements.get<linear>();
//
//	auto itor = index_type.begin();
//	for (UInt i = 0; i < pos; ++i)
//		++itor;
//
//	m_elements.erase(itor);
//}
//
//void CVertexFormat::RemoveElement(const std::string& vertexSemantic, UInt semanticIndex)
//{
//	typedef ElementContainer::index<semantic_and_index>::type element_by_semantic_and_index;
//	element_by_semantic_and_index& index_type = m_elements.get<semantic_and_index>();
//
//	auto itor = index_type.find( boost::make_tuple(vertexSemantic, semanticIndex) ); // ( boost::make_tuple(semantic, semanticIndex) );
//	if ( itor != index_type.end() )
//	{
//		index_type.erase(itor);
//	}
//}
//
//void CVertexFormat::RemoveAllElements()
//{
//	m_elements.clear();
//}
//
//UInt CVertexFormat::GetElementSemanitcCount(const std::string& vertexSemantic) const
//{
//	typedef ElementContainer::index<semantic>::type element_by_semantic;
//	const element_by_semantic& index_type = m_elements.get<semantic>();
//	return index_type.count(vertexSemantic);
//}