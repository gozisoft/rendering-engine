#ifndef __SHADER_TYPES_H__
#define __SHADER_TYPES_H__

#include "Core.h"

_ENGINE_BEGIN

namespace Shader
{
	// Types for the input and output variables of the shader program.
	enum VariableType
	{
		VT_UNKNOWN = ~0,
		VT_BYTE = 0,
		VT_UBYTE,
		VT_SHORT,
		VT_USHORT,
		VT_INT,
		VT_UINT,
		VT_FLOAT,
		VT_DOUBLE,
		VT_BOOL,
		VT_TEXTURE_1D,
		VT_TEXTURE_2D,
		VT_TEXTURE_3D,
		NUM_VARIABLE_TYPES
	};

	// Cool enum type functions
	template < typename T > struct Type2EnumType	  { static const VariableType type = VT_UNKNOWN; };
	template <> struct Type2EnumType<Int8>			  { static const VariableType type = VT_BYTE; };
	template <> struct Type2EnumType<UInt8>			  { static const VariableType type = VT_UBYTE; };
	template <> struct Type2EnumType<Int16>			  { static const VariableType type = VT_SHORT; };
	template <> struct Type2EnumType<UInt16>		  { static const VariableType type = VT_USHORT; };
	template <> struct Type2EnumType<Int32>			  { static const VariableType type = VT_INT; };
	template <> struct Type2EnumType<UInt32>		  { static const VariableType type = VT_UINT; };
	template <> struct Type2EnumType<float>			  { static const VariableType type = VT_FLOAT; };
	template <> struct Type2EnumType<double>		  { static const VariableType type = VT_DOUBLE; };
	template <> struct Type2EnumType<bool>			  { static const VariableType type = VT_BOOL; };

	// Size functions for variable types
	size_t SizeOfType(VariableType type);

	// Types for the input and output variables of the shader program.
	enum ClassType
	{
		CT_UNKNOWN = ~0,
		CT_CBUFFER,
		CT_TBUFFER,
		CT_TEXTURE,
		CT_SAMPLER,
		NUM_CLASS_TYPES
	};

	// Types for generalised shader resources
	enum DataType
	{
		DT_UNKNOWN = ~0,
		DT_SCALAR,
		DT_VECTOR,
		DT_MATRIX,
		DT_OBJECT,
		DT_STRUCT,
		NUM_DATA_TYPES
	};

	// Types for generalised shader resources
	enum ShaderReturnType
	{
		SRT_NONE = 0,
		SRT_UNORM,
		SRT_SNORM,
		SRT_SINT,
		SRT_UINT,
		SRT_FLOAT,
		SRT_MIXED,
		SRT_DOUBLE,
		SRT_CONTINUED,
		NUM_SHADER_RETURN_TYPES
	};

	// Types for generalised shader resources
	enum ShaderDimensions // used mainly for texture dimensions dsecribed by a shader
	{
		SD_UNKNOWN = ~0,
		SD_BUFFER,
		SD_TEXTURE1D,
		SD_TEXTURE1DARRAY,
		SD_TEXTURE2D,
		SD_TEXTURE2DARRAY,
		NUM_SHADER_DIMENSIONS
	};


#include "ShaderTypes.inl"

}

_ENGINE_END

#endif




	//// Semantics for the input and output variables of the shader program.
	//enum VariableSemantic
	//{
	//	VS_NONE = 0,
	//	VS_POSITION,        // ATTR0
	//	VS_BLENDWEIGHT,     // ATTR1
	//	VS_NORMAL,          // ATTR2
	//	VS_COLOUR0,         // ATTR3 (and for render targets)
	//	VS_COLOUR1,         // ATTR4 (and for render targets)
	//	VS_FOGCOORD,        // ATTR5
	//	VS_PSIZE,           // ATTR6
	//	VS_BLENDINDICES,    // ATTR7
	//	VS_TEXCOORD0,       // ATTR8
	//	VS_TEXCOORD1,       // ATTR9
	//	VS_TEXCOORD2,       // ATTR10
	//	VS_TEXCOORD3,       // ATTR11
	//	VS_TEXCOORD4,       // ATTR12
	//	VS_TEXCOORD5,       // ATTR13
	//	VS_TEXCOORD6,       // ATTR14
	//	VS_TEXCOORD7,       // ATTR15
	//	VS_FOG,             // same as VS_FOGCOORD (ATTR5)
	//	VS_TANGENT,         // same as VS_TEXCOORD6 (ATTR14)
	//	VS_BINORMAL,        // same as VS_TEXCOORD7 (ATTR15)
	//	VS_COLOR2,          // support for multiple render targets
	//	VS_COLOR3,          // support for multiple render targets
	//	VS_DEPTH0,          // support for multiple render targets
	//	NUM_VARIABLE_SEMANTICS
	//};