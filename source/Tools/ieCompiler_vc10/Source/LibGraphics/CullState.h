#ifndef __CCULL_STATE_H__
#define __CCULL_STATE_H__

#include "Core.h"

namespace Engine
{


class CCullState 
{
public:
	CCullState();
	CCullState(bool enableCull, bool ccOrder);
	~CCullState();

	bool m_cullEnabled;	// default: true
	bool m_ccOrder;		// default: true (clockwise order)

private:
	DECLARE_HEAP;
};




} // namespace Engine


#endif