#ifndef __CVERTEX_FORMAT_INL__
#define __CVERTEX_FORMAT_INL__

inline const CVertexFormat::ElementContainer& CVertexFormat::GetElements() const
{
	return m_elements;
}

//sequenced_index.
inline const CVertexElement* CVertexFormat::GetElement(UInt index) const
{
	assert (index < m_elements.size());
	return m_elements[index];
}

inline UInt CVertexFormat::GetElementCount() const
{
	return m_elements.size();
}

inline UInt CVertexFormat::GetTotalStride() const
{
	return m_totalStride;
}

inline void CVertexFormat::RemoveAllElements()
{
	m_elements.clear();
}

#endif



//inline void CVertexFormat::SetEnabled(Buffer::VertexSemantic semantic, bool enable)
//{
//	assert(semantic >= Buffer::VS_POSITION && semantic <= Buffer::VS_PSIZE);
//    m_semanticFlags &= ~(1<<semantic); // Clearing a bit (must invert the bit string with the bitwise NOT operator (~), then AND it)
//    m_semanticFlags |= ( (!!enable)<<semantic ); // Setting a bit
//}
//
//inline bool CVertexFormat::IsEnabled(Buffer::VertexSemantic semantic) const
//{
//	assert(semantic >= Buffer::VS_POSITION && semantic <= Buffer::VS_PSIZE);
//	return !!( m_semanticFlags & (1<<semantic) );
//}

//inline void CVertexFormat::SetEnabled(Buffer::VertexSemantic semantic, bool enable)
//{
//	assert(semantic >= Buffer::VS_POSITION && semantic <= Buffer::VS_PSIZE);
//	m_semanticFlags[semantic] = enable;
//}
//
//inline bool CVertexFormat::IsEnabled(Buffer::VertexSemantic semantic) const
//{
//	assert(semantic >= Buffer::VS_POSITION && semantic <= Buffer::VS_PSIZE);
//	return m_semanticFlags[semantic];
//}


//inline CVertexElementPtr CVertexFormat::GetElement(UInt index) const
//{
//	//sequenced_index.
//	typedef ElementContainer::index<linear>::type element_by_linear;
//	const element_by_linear& index_type = m_elements.get<linear>();
//
//	size_t size = index_type.size();
//	if ( index < size )
//	{
//		auto itor = index_type.begin();
//		for (UInt i = 0; i < index; ++i)
//			++itor;
//
//		const CVertexElementPtr& pElement = (*itor);		
//		return pElement;
//	}
//	else
//	{
//		assert( false && ieS("Invalid index called for GetElement") );
//		return CVertexElementPtr();
//	}
//}
