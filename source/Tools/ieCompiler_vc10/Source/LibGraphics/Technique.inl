#ifndef __CTECHNIQUE_INL__
#define __CTECHNIQUE_INL__

//---------------------------------------------------------------------------------------------------------
// CSceneTechnique
//---------------------------------------------------------------------------------------------------------

// Adds a viaul pass to the array
inline void CSceneTechnique::AddVisualPass(CVisualPass* pPass)
{
	assert( pPass && ieS("Invalid CVisualPass pointer") );
	m_visualPasses.push_back(pPass);
}

// Member access
inline size_t CSceneTechnique::GetNumPasses() const
{
	return m_visualPasses.size();
}

inline const CVisualPass* CSceneTechnique::GetPass(UInt passIndex) const
{
	return m_visualPasses[passIndex];
}

inline CVisualPass* CSceneTechnique::GetPass(UInt passIndex)
{
	return const_cast<CVisualPass*>( static_cast<const CSceneTechnique&>(*this).GetPass(passIndex) );
}

// VisualPass access
inline const CVertexShader* CSceneTechnique::GetVertexShader(UInt index) const
{
	return m_visualPasses[index]->m_pVertexShader;
}

inline const CPixelShader* CSceneTechnique::GetPixelShader(UInt index) const
{
	return m_visualPasses[index]->m_pPixelShader;
}

inline const CAlphaState* CSceneTechnique::GetAlphaState(UInt index) const
{
	return m_visualPasses[index]->m_pAlphaState;
}

inline const CCullState* CSceneTechnique::GetCullState(UInt index) const
{
	return m_visualPasses[index]->m_pCullState;
}

inline const CDepthState* CSceneTechnique::GetDepthState(UInt index) const
{
	return m_visualPasses[index]->m_pDepthState;
}

inline const COffsetState* CSceneTechnique::GetOffsetState(UInt index) const
{
	return m_visualPasses[index]->m_pOffsetState;
}

inline const CStencilState* CSceneTechnique::GetStencilState(UInt index) const
{
	return m_visualPasses[index]->m_pStencilState;
}

inline const CWireState* CSceneTechnique::GetWireState(UInt index) const
{
	return m_visualPasses[index]->m_pWireState;
}

inline const std::string& CSceneTechnique::GetName() const
{
	return m_contexName;
}

inline const std::string& CSceneTechnique::GetTechVersion() const
{
	return m_techVersion;
}


#endif