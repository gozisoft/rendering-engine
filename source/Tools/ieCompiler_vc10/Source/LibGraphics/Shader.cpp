#include "frameafx.h"
#include "Shader.h"
#include "VertexFormat.h"
#include "VertexElement.h"

using namespace Engine;

CShader::CShader(const std::string& functionName, ShaderVersion version, ShaderType type) : 
m_functionName(functionName),
m_shaderVersion(version),
m_shaderType(type)
{


}

CShader::~CShader()
{

}

// Set the input format
void CShader::SetInputFormat(CVertexFormatPtr pInput)
{
#if defined (DEBUG) || defined (_DEBUG)
	if (m_shaderType == ST_GEOMETRY ||
		m_shaderType == ST_PIXEL)
	{
		bool validFormat = false;
		auto elements = pInput->GetElements();
		for (auto itor = elements.begin(); itor != elements.end(); ++itor)
		{
			const CVertexElement* pElement = *itor;
			const std::string semantic = pElement->GetVertexSemantic();
			if (semantic == "SV_POSITION")
				validFormat = true;
			else if (semantic == "SV_NORMAL")
				validFormat = true;
			else if (semantic == "SV_TANGENT")
				validFormat = true;
			else if (semantic == "SV_BINORMAL")
				validFormat = true;

			else if (semantic == "SV_COLOUR")
				validFormat = true;
			else if (semantic == "SV_BLEND_INDICES")
				validFormat = true;
			else if (semantic == "SV_BLEND_WEIGHTS")
				validFormat = true;
			else if (semantic == "SV_TCOORD")
				validFormat = true;

			else if (semantic == "SV_FOG")
				validFormat = true;
			else if (semantic == "SV_PSIZE")
				validFormat = true;

			// Check for error
			if ( validFormat != true )
			{
				assert( validFormat && ieS("Unknown semantic CVertexShader::Output") );
				break;
			}
		}
	}
#endif

	m_pInput = pInput;
}

// Set the output format
void CShader::SetOutputFormat(CVertexFormatPtr pOutput)
{
//#if defined (DEBUG) || defined (_DEBUG)
//	bool validFormat = false;
//	auto elements = pOutput->GetElements();
//	for (auto itor = elements.begin(); itor != elements.end(); ++itor)
//	{
//		const CVertexElement* pElement = *itor;
//		const std::string semantic = pElement->GetVertexSemantic();
//		if (semantic == "SV_POSITION")
//			validFormat = true;
//		else if (semantic == "SV_NORMAL")
//			validFormat = true;
//		else if (semantic == "SV_TANGENT")
//			validFormat = true;
//		else if (semantic == "SV_BINORMAL")
//			validFormat = true;
//
//		else if (semantic == "SV_COLOUR")
//			validFormat = true;
//		else if (semantic == "SV_BLEND_INDICES")
//			validFormat = true;
//		else if (semantic == "SV_BLEND_WEIGHTS")
//			validFormat = true;
//		else if (semantic == "SV_TCOORD")
//			validFormat = true;
//
//		else if (semantic == "SV_FOG")
//			validFormat = true;
//		else if (semantic == "SV_PSIZE")
//			validFormat = true;
//
//		// Check for error
//		if ( validFormat != true )
//		{
//			assert( validFormat && ieS("Unknown semantic CVertexShader::Output") );
//			break;
//		}
//	}
//#endif

	m_pOutput = pOutput;
}