#ifndef __SPHERE_H__
#define __SPHERE_H__


#include "MathsFwd.h"

_ENGINE_BEGIN

// The sphere is represented as |X-C| = R where C is the center and R is
// the radius.
class CSphere
{
public:
	Point3f m_center;
	float m_radius;

	// Default ctor
	CSphere ();  
	CSphere (const Point3f& center, float radius);
	CSphere (const CSphere& other);

	// assignment
	CSphere& operator = (const CSphere& other);

};




_ENGINE_END



#endif