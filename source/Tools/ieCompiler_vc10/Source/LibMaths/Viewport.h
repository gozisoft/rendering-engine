#ifndef __VIEWPORT_H__
#define __VIEWPORT_H__

#include "MathsFwd.h"
#include "FixedArray.h"

namespace Engine
{

template < typename T >
class Viewport : public FixedArray<4, T>
{
public:
	typedef FixedArray<4, T> array_type;
	typedef typename array_type::type			 type;
	typedef typename array_type::pointer		 pointer;
	typedef typename array_type::reference		 reference;
	typedef typename array_type::const_pointer   const_pointer;
	typedef typename array_type::const_reference const_reference;

public:
	// const accessor methods
	const_reference left()	  const { return (*this)[0]; }
	const_reference top()	  const { return (*this)[1]; }
	const_reference right()   const { return (*this)[2]; }
	const_reference bottom()  const { return (*this)[3]; }

	// accessor methods
	reference left()	{ return (*this)[0]; }
	reference top()		{ return (*this)[1]; }
	reference right()	{ return (*this)[2]; }
	reference bottom()	{ return (*this)[3]; }

	void SetLTRB(const Viewport & viewport);
	void SetLTRB(const_reference left, const_reference top, const_reference right, const_reference bottom);

	float AspectRatio() const;

	T width() const;
	T height() const;

	void Center();

	Viewport();
	Viewport(const_reference left, const_reference top, const_reference right, const_reference bottom);
	Viewport(const Viewport & viewport);

	// null & one 
	static const Viewport ZERO;
	static const Viewport ONE;
};


#include "Viewport.inl"


}



#endif