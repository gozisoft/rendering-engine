#ifndef __MATHS_H__
#define __MATHS_H__


#include "Const.h"

#include <cmath>   // used for math functions tan sin cos

namespace Engine
{


// the value of a number squared
template < typename T >
inline const T Sqr(const T& x)
{
	return x * x;
}

template < typename T >
inline const T InvSqrt (const T& x)
{
	assert( x != T(0) );
    return T(1) / std::sqrt(x);
}

// the value of a number cubed
template < typename T >
inline const T Cube(const T& x)
{
	return x * x * x;
}

// absolute value of a number
template < typename T >
inline const T Abs(const T& x)
{
	return x < T(0) ? -x : x;
}

// the lower of two numbers
template < typename T >
inline const T& Min(const T& x, const T& y)
{
	return x < y ? x : y;
}

// the lower of three numbers
template < typename T >
inline const T& Min(const T& x, const T& y, const T& z)
{
	return x < y ? Min(x,z) : Min(y,z);
}

// the higher of two numbers
template < typename T >
inline const T& Max(const T& x, const T& y)
{
	return x < y ? y : x;
}

// the higher of three numbers
template < typename T >
inline const T& Max(const T& x, const T& y, const T& z)
{
	return x < y ? Max(y, z) : Max(x, z);
}

// the average of two numbers
template < typename T >
inline const T Average(const T& x, const T& y)
{
	return (x + y) / T(2);
}

// exchange the values of two numbers
template < typename T >
inline void Swap(T& x, T& y)
{
	T temp = x; x = y; y = temp;
}

// exchange the values of two integers
inline void Swap(int& x, int& y)
{
	x ^= y; y ^= x; x ^= y;
}

// find the sign of a number
template < typename T >
inline const T Sign(const T& x)
{
	return x < T(0) ? T(-1) : (x > T(0) ? T(1) : T(0));
}

// recursively compute the value of a factorial
template < typename T >
const T Factorial(const T& f)
{
	return f < T(2) ? T(1) : Factorial(f-1) * f;
}

// pseudorandom number in the specified range
template < typename T >
const T Random(const T& low, const T& high)
{
	return rand() % high + low;
}

// derivative of a function f from first principles
template < typename T >
const T Derivative(T(*f)(T), const T& x, const T& h)
{
	return (f(x + h) - f(x)) / h;
}

// convert x screen coordinate to view space
template < typename T >
inline const T ScreenToViewX(T x, T width, T height)
{
	return (x-width*0.5f) / ((width*0.5f) / (0.41421f * (width / height)));
}

// convert y screen coordinate to view space
template < typename T >
inline const T ScreenToViewY(T y, T width, T height)
{
	return ((height-y) - height*0.5f) / ((height*0.5f) / 0.41421f);
}

// test for approx. equality to zero with an absolute or relative tolerance
template < typename T >
inline bool IsZero( const T& x, const T& tolerance = Const<T>::ROUNDING_ERROR() )
{
	return Abs(x) <= tolerance;
}

// if x equals zero, taking rounding errors into account
inline bool IsZero(Int32 x, Int32 tolerance = 0)
{
	return ( x & 0x7ffffff ) <= tolerance;
}

// if x equals zero, taking rounding errors into account
inline bool IsZero(UInt32 x, UInt32 tolerance = 0)
{
	return x <= tolerance;
}

// test for approx. equality with an absolute or relative tolerance
template < typename T, typename U >
inline bool IsEqual( const T& x, const U& y, const T& tolerance = Const<T>::ROUNDING_ERROR() )
{
	return ( ( x + tolerance ) >= y ) && ( ( x - tolerance ) <= y );
}

/*
template < typename T >
inline bool IsEqual(const T& x, const T& y, const T& err = Max<T>(1.0f, Abs<T>(x), Abs<T>(y) ))
{
	return Abs(x-y) <= ( Const<T>::EPSILON() * err );
}
*/

// limit a number to the specified range
template < typename T >
inline const T& Clamp(const T& x, const T& low, const T& high)
{
	return Max(low, Min(x, high));
}

/*
// return a Binomial coefficient from Pascals triangle ( n = degree )
template < typename T >
inline const T& BinomialCoefficient(const T& n, const T& r)
{
	if ( r > (n - k) ) //take advantage of symmetry
		k = n - k;

	size_t c = 1;
	for (size_t r = 0; r <= n; ++r)
	{
		m_ppPascalsTrig[n][r] = c;
		c = c * (n - r) / (r + 1);
	}

	return c;
}
*/

}

#endif // MATHS_H_
