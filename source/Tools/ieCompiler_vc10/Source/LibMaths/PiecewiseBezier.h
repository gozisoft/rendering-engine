#ifndef __PIECEWISE_BEZIER_CURVE_H__
#define __PIECEWISE_BEZIER_CURVE_H__

#include "Beziercurve.h"

namespace Engine
{
	class CPieceWiseBezier
	{
	public:
		typedef CBezierCurve curve_type;
		typedef std::vector<curve_type> curve_array;

	public:
		CPieceWiseBezier(const curve_array& bezierArray);
		CPieceWiseBezier(const curve_type* pArray, size_t size);
		~CPieceWiseBezier();

		// Function for adding a single curve via c0 continuity
		// to the piecewise curve.
		void AddSingleCurveC0(const curve_type& bezierCurve);

		// Function for adding a single curve via c1 continuity
		// to the piecewise curve.
		void AddSingleCurveC1(const curve_type& bezierCurve);

		// Function for adding a single curve via c2 continuity
		// to the piecewise curve.
		void AddSingleCurveC2(const curve_type& bezierCurve);

		// Return a curve based on index
		const curve_type& GetCurve(size_t index) const;

		size_t GetNumCurves() const;

	private:
		curve_array m_bezierCurves;

	};

#include "PiecewiseBezier.inl"

}




#endif