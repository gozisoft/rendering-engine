#include "mathsafx.h"
#include "Plane.h"
#include "Unroller.h"

using namespace Engine;

CPlane::CPlane() : m_disOrigin(0.0f)
{

}

CPlane::CPlane(const CPlane& plane) :
m_normal(plane.m_normal),
m_disOrigin(plane.m_disOrigin)
{

}

const CPlane& CPlane::operator = (const CPlane& plane)
{
	m_normal = plane.m_normal;
	m_disOrigin = plane.m_disOrigin;
	return (*this);
}

CPlane::CPlane(float x, float y, float z, float distance)
{
	Set(x, y, z, distance);
}

CPlane::CPlane(const Vector3f& point, const Vector3f& normal)
{
	FromPointNormal(point, normal);
}


CPlane::CPlane(const Vector3f &point1, const Vector3f &point2, const Vector3f &point3)
{
	FromPoints(point1, point2, point3);
}

void CPlane::Set(float x, float y, float z, float distance)
{
	m_normal.set(x, y, z);
	m_disOrigin = -distance;
}


void CPlane::Set(const Vector3f& normal, float distance)
{
	m_normal = normal;
	m_disOrigin = -distance;
}

// Calculate distance to point. Plane normal must be normalized.
float CPlane::Distance(const Vector3f& point) const
{
	Vector3f result;
	for (size_t i = 0; m_normal.size(); ++i)
	{
		result[i] = m_normal[i] * point[i];
	}

	Vector3f final;
	for (size_t i = 0; m_normal.size(); ++i)
	{
		final[i] = result[i] - m_disOrigin;
	}
		
	return 0.0f; // Abs(final);
}

void CPlane::PlaneNormalise()
{
	float length = 1.0f / cml::length(m_normal);
    m_normal *= length;
    m_disOrigin *= length;
}

void CPlane::FromPointNormal(const Vector3f &normal, const Vector3f &point)
{
	Set(normal, cml::dot(normal, point));
	PlaneNormalise();
}

void CPlane::FromPoints(const Vector3f &point1, const Vector3f &point2, const Vector3f &point3)
{
	m_normal = cml::normalize( cml::cross(point2 - point1, point3 - point1) );  
    m_disOrigin = -cml::dot(m_normal, point1);
}

