#ifndef __MATHS_FORWARD_H__
#define __MATHS_FORWARD_H__

#include "cml\cml.h"
#include "Colour.h"
#include "Viewport.h"

namespace cml {
	typedef vector< long,    fixed<2> > vector2l;
	typedef vector< long,    fixed<3> > vector3l;
	typedef vector< long,    fixed<4> > vector4l;
}

_ENGINE_BEGIN

// Const type
template < typename T = float > struct Const;

// ----------------------------------------------------------------------------------------
// Linear Algebra
// ----------------------------------------------------------------------------------------

// vector2 typedefs
typedef cml::vector2i Vector2i;
typedef cml::vector2l Vector2l;
typedef cml::vector2f Vector2f;
typedef cml::vector2d Vector2d;

typedef cml::vector2i Point2i;
typedef cml::vector2l Point2l;
typedef cml::vector2f Point2f;
typedef cml::vector2d Point2d;

// vector3 typedefs
typedef cml::vector3i Vector3i;
typedef cml::vector3l Vector3l;
typedef cml::vector3f Vector3f;
typedef cml::vector3d Vector3d;

typedef cml::vector3i Point3i;
typedef cml::vector3l Point3l;
typedef cml::vector3f Point3f;
typedef cml::vector3d Point3d;

// vector4 typedefs
typedef cml::vector4i Vector4i;
typedef cml::vector4l Vector4l;
typedef cml::vector4f Vector4f;
typedef cml::vector4d Vector4d;

typedef cml::vector4i Point4i;
typedef cml::vector4l Point4l;
typedef cml::vector4f Point4f;
typedef cml::vector4d Point4d;

// Matrix typedef
typedef cml::matrix44i_r Matrix4i;
typedef cml::matrix44f_r Matrix4f;
typedef cml::matrix44d_r Matrix4d;

// Dynamic size matrix typedef's
typedef cml::matrix< int, cml::dynamic<>, cml::row_basis, cml::row_major > Matrixi;
typedef cml::matrix< long, cml::dynamic<>, cml::row_basis, cml::row_major > Matrixl;
typedef cml::matrix< float, cml::dynamic<>, cml::row_basis, cml::row_major > Matrixf;
typedef cml::matrix< double, cml::dynamic<>, cml::row_basis, cml::row_major > Matrixd;

// Quaternion typdef
typedef cml::quaternionf_n Quaternionf;
typedef cml::quaterniond_n Quaterniond;

// HLSL types
typedef cml::vector< float, cml::external<> > hlsl_vector;
typedef cml::matrix< float, cml::external<>, cml::row_basis, cml::row_major > hlsl_matrix;

// ----------------------------------------------------------------------------------------
// Engine Tools
// ----------------------------------------------------------------------------------------
// Colour typedefs
typedef ColourRGB<float> Colour3f;
typedef ColourRGB<double> Colour3d;

// Colour typedefs
typedef ColourRGBA<float> Colour4f;
typedef ColourRGBA<double> Colour4d;

// Viewport typedefs
typedef Viewport<int> Viewporti;
typedef Viewport<long> Viewportl;
typedef Viewport<float> Viewportf;
typedef Viewport<double> Viewportd;
typedef Viewport<unsigned int> Viewportu;

// ----------------------------------------------------------------------------------------
// Curves
// ----------------------------------------------------------------------------------------
class CBezierCurve;
class CPieceWiseBezier;

typedef std::shared_ptr<CBezierCurve> CBezierCurvePtr;
typedef std::shared_ptr<CPieceWiseBezier> CPieceWiseBezierPtr;

// ----------------------------------------------------------------------------------------
// Collision
// ----------------------------------------------------------------------------------------
class CPlane;
class CSphere;

// Shapes
class CLine2;
class CLine3;

//template < typename T = float > class Triangle2;
//template < typename T = float > class Triangle3;
//
//// Triangle typedef
//typedef Triangle2<float> Triangle2f;
//typedef Triangle2<double> Triangle2d;
//typedef Triangle3<float> Triangle3f;
//typedef Triangle3<double> Triangle3d;






_ENGINE_END



#endif
