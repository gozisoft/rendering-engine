#ifndef __TABLE_ARRAY_H__
#define __TABLE_ARRAY_H__

#include "Unroller.h"

namespace Engine
{

template < size_t Rows, size_t Cols, typename Element >
class TableArray
{	
public:
	typedef TableArray<Rows, Cols, Element> this_type;

	typedef Element type;
	typedef Element* pointer;
	typedef Element& reference;
	typedef const Element* const_pointer;
	typedef const Element& const_reference;

	static const size_t m_rows = Rows;
	static const size_t m_cols = Cols;
	static const size_t m_arraySize = m_rows * m_cols;

	typedef type data_impl[m_arraySize];

public:
	size_t GetSize() const
	{ 
		return size_t(m_arraySize);
	}

	size_t GetRowSize() const
	{
		return m_rows;
	}

	size_t GetColSize() const
	{
		return m_cols;
	}

	// access member of the 1D array
	const_reference operator[] (size_t pos) const
	{ 
		assert( pos < m_arraySize ); 
		return m_data[pos];
	}

	// access member of the 1D array
	reference operator[] (size_t pos) 
	{ 
		return const_cast<reference>( static_cast<const this_type&>(*this)[pos] );
	}

	// row and col must be less than the number of cols or rows within the table
	const_reference operator() (size_t row, size_t col) const
	{
		assert( row < GetRowSize() || col < GetColSize() );
		return m_data[ ( GetColSize() * row ) + col ];
	}

	reference operator() (size_t row, size_t col)
	{
		return const_cast<reference>( static_cast<const this_type&>(*this)(row, col) );
	}

	// Return access to the data as a raw pointer.
	const_pointer GetData() const
	{ 
		return &(*this)[0]; 
	}

	//Return access to the data as a raw pointer. 
	pointer GetData() 
	{ 
		return const_cast<pointer>( static_cast<const this_type&>(*this).GetData() );
	}

protected:
	//-----------------------------------------------------------------------
	// Operator Functions
	//-----------------------------------------------------------------------
	// assignment :: Matrix
	template < typename U >
	this_type & _AddAssign (const TableArray<Rows, Cols, U>& v1)
	{
		AssignOpLoopUnroller<type, const U, AssignOpAdd>::Loop<m_arraySize>::eval( GetData(), v1.GetData() );                                  		
		return (*this);
	}

	template < typename U >
	this_type& _SubAssign (const TableArray<Rows, Cols, U>& v1)
	{
		AssignOpLoopUnroller<type, const U, AssignOpSub>::Loop<m_arraySize>::eval( GetData(), v1.GetData() );                                  		
		return (*this);
	}

	// assignment :: scalar
	template < typename U >
	this_type& _MulAssign (const U& val)
	{
		ScalarOpLoopUnroller<type, const U, AssignOpMul>::Loop<m_arraySize>::eval( GetData(), val );                                  		
		return (*this);
	}

	template < typename U >
	this_type& _DivAssign (const U& val)
	{
		assert( val != U(0) );
		type inv = type(1) / val;
		return _MulAssign(inv);
	}

protected:
	TableArray() 
	{
		ScalarOpLoopUnroller<type, type, AssignOpAssign>::Loop<m_arraySize>::eval( GetData(), type(0) );
	}

protected:
	data_impl m_data;
};

//-----------------------------------------------------------------------
// Comparison Operators
//-----------------------------------------------------------------------
template < size_t R, size_t C, typename T, typename U >
const bool operator == (const TableArray<R, C, T>& v0, const TableArray<R, C, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpEqual>::Loop<S>::eval( v0.GetData(), v1.GetData() );  
}

template < size_t R, size_t C, typename T, typename U >
const bool operator != (const TableArray<R, C, T>& v0, const TableArray<R, C, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpNotEqual>::Loop<S>::eval( v0.GetData(), v1.GetData() );  ;  
}

template < size_t R, size_t C, typename T, typename U >
const bool operator <= (const TableArray<R, C, T>& v0, const TableArray<R, C, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpLessEqual>::Loop<S>::eval( v0.GetData(), v1.GetData() );  ;  
}

template < size_t R, size_t C, typename T, typename U >
const bool operator >= (const TableArray<R, C, T>& v0, const TableArray<R, C, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpGreaterEqual>::Loop<S>::eval( v0.GetData(), v1.GetData() );  ;  
}

template < size_t R, size_t C, typename T, typename U >
const bool operator < (const TableArray<R, C, T>& v0, const TableArray<R, C, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpLessThan>::Loop<S>::eval( v0.GetData(), v1.GetData() );  ;  
}	

template < size_t R, size_t C, typename T, typename U >
const bool operator > (const TableArray<R, C, T>& v0, const TableArray<R, C, U>& v1)
{
	return CompareLoopUnroller<const T, const U, CompareOpGreaterThan>::Loop<S>::eval( v0.GetData(), v1.GetData() );  ;  
}

}



#endif