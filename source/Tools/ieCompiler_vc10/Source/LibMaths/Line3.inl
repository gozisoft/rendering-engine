#ifndef __LINE3_INL__
#define __LINE3_INL__

CLine3::CLine3() 
{

}

CLine3::CLine3(const CLine3 &line) : 
m_origin(line.m_origin),
m_direction(line.m_direction)
{

}

CLine3::CLine3(const Point3f& origin, const Vector3f& direction) : 
m_origin(origin),
m_direction(direction)
{

}

CLine3 & CLine3::operator = (const CLine3 &line)
{
	m_origin = line.m_origin;
	m_direction = line.m_direction;
	return *this;
}

#endif