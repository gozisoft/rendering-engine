#ifndef __PLANE_H__
#define __PLANE_H__

#include "MathsFwd.h"
#include "Maths.h"

_ENGINE_BEGIN

class CPlane 
{
public:
	enum IntersectSide
	{
		ISS_FRONT = 0,
		ISS_BACK,     
		ISS_PLANAR
	};

public:
	Vector3f m_normal;	// plane normal vector
	float m_disOrigin;	// distance to origin

	// Uninitalised
	CPlane();

	// Copy Ctor
	CPlane(const CPlane& plane);

	// Assignment
	const CPlane& operator = (const CPlane& plane);

	CPlane(float x, float y, float z, float distance);
	CPlane(const Vector3f& point, const Vector3f& normal);
	CPlane(const Vector3f& point1, const Vector3f& point2, const Vector3f& point3);

	// Distance from point
	float Distance(const Vector3f& point) const;

	// Normalise the plane
	void PlaneNormalise();	

	// Set functions
	void Set(float x, float y, float z, float distance);
	void Set(const Vector3f& normal, float distance);

	// Point on a plane tests
	void FromPointNormal(const Vector3f &normal, const Vector3f &point);
	void FromPoints(const Vector3f &point1, const Vector3f &point2, const Vector3f &point3);
};


_ENGINE_END


#endif