#ifndef __BIT_HACKS_H__
#define __BIT_HACKS_H__

#include "Core.h"

_ENGINE_BEGIN

namespace Maths
{

	inline bool IsPowerOf2(UInt val)
	{
		return (val > 0) && ((val & (val - 1)) == 0);
	}

	inline bool IsPowerOf2(Int val)
	{
		return (val > 0) && ((val & (val - 1)) == 0);
	}

	inline UInt32 Log2OfPowerOfTwo(UInt32 powerOfTwo)
	{
		UInt log2 = (powerOfTwo & 0xAAAAAAAA) != 0;
		log2 |= ((powerOfTwo & 0xFFFF0000) != 0) << 4;
		log2 |= ((powerOfTwo & 0xFF00FF00) != 0) << 3;
		log2 |= ((powerOfTwo & 0xF0F0F0F0) != 0) << 2;
		log2 |= ((powerOfTwo & 0xCCCCCCCC) != 0) << 1;
		return log2;
	}

	inline Int32 Log2OfPowerOfTwo(Int32 powerOfTwo)
	{
		UInt32 log2 = (powerOfTwo & 0xAAAAAAAA) != 0;
		log2 |= ((powerOfTwo & 0xFFFF0000) != 0) << 4;
		log2 |= ((powerOfTwo & 0xFF00FF00) != 0) << 3;
		log2 |= ((powerOfTwo & 0xF0F0F0F0) != 0) << 2;
		log2 |= ((powerOfTwo & 0xCCCCCCCC) != 0) << 1;
		return static_cast<Int32>(log2);
	}

	inline UInt64 Log2OfPowerOfTwo(UInt64 powerOfTwo)
	{
		UNUSED_PARAMETER(powerOfTwo);
		return 0;
	}

	inline Int64 Log2OfPowerOfTwo(Int64 powerOfTwo)
	{
		UNUSED_PARAMETER(powerOfTwo);
		return 0;
	}

	// Computing padding 
	// http://en.wikipedia.org/wiki/Data_structure_alignment
	inline UInt AlignToPowerOf2(UInt value, UInt alignment)
	{
		// to align to 2^N, add 2^N - 1 and AND with all but lowest N bits set
		assert( (alignment & (alignment - 1) ) == 0 );	
		return (value + alignment - 1) & (~(alignment - 1));
	}

}

_ENGINE_END




#endif