#ifndef __COLOUR_INL__
#define __COLOUR_INL__


template < typename T > const ColourRGB<T> ColourRGB<T>::BLACK ( T(0), T(0), T(0) );
template < typename T > const ColourRGB<T> ColourRGB<T>::WHITE ( T(1), T(1), T(1) );
template < typename T > const ColourRGB<T> ColourRGB<T>::RED   ( T(1), T(0), T(0) );
template < typename T > const ColourRGB<T> ColourRGB<T>::GREEN ( T(0), T(1), T(0) );
template < typename T > const ColourRGB<T> ColourRGB<T>::BLUE  ( T(0), T(0), T(1) );

template < typename T > const ColourRGB<T> ColourRGB<T>::INVALID (T(-1), T(-1), T(-1));

template < typename T >
ColourRGB<T>::ColourRGB() : array_type()
{


}

template < typename T >
ColourRGB<T>::ColourRGB(const ColourRGB & colourRGB) 
{
	SetRGB(colourRGB);
}

template < typename T >
ColourRGB<T>::ColourRGB(const_reference red, const_reference green, const_reference blue)
{
	SetRGB(red, green, blue);
}

template < typename T >
void ColourRGB<T>::SetRGB(const ColourRGB & colourRGB)
{
	SetRGB( colourRGB.r(), colourRGB.g(), colourRGB.b() );
}


template < typename T >
void ColourRGB<T>::SetRGB(const_reference red, const_reference green, const_reference blue)
{
	this->r() = red;
	this->g() = green;
	this->b() = blue;
}

template < typename T >
void ColourRGB<T>::Clamp()
{

}

template < typename T >
void ColourRGB<T>::ScaleByMax()
{
}


template < typename T > const ColourRGBA<T>  ColourRGBA<T>::BLACK ( T(0), T(0), T(0), T(0) );
template < typename T > const ColourRGBA<T>  ColourRGBA<T>::WHITE ( T(1), T(1), T(1), T(1) );
template < typename T > const ColourRGBA<T>  ColourRGBA<T>::RED   ( T(1), T(0), T(0), T(1) );
template < typename T > const ColourRGBA<T>  ColourRGBA<T>::GREEN ( T(0), T(1), T(0), T(1) );
template < typename T > const ColourRGBA<T>  ColourRGBA<T>::BLUE  ( T(0), T(0), T(1), T(1) );
template < typename T > const ColourRGBA<T>  ColourRGBA<T>::INVALID ( T(-1), T(-1), T(-1), T(-1) );

template < typename T >
ColourRGBA<T>::ColourRGBA() : array_type()
{

}

template < typename T >
ColourRGBA<T>::ColourRGBA(const ColourRGBA<T> & colourRGBA) 
{
	SetRGBA(colourRGBA);
}

template < typename T >
ColourRGBA<T>::ColourRGBA(const_reference red, const_reference green, const_reference blue, const_reference alpha)
{
	SetRGBA(red, green, blue, alpha);
}

template < typename T >
void ColourRGBA<T>::SetRGBA(const ColourRGBA & colourRGBA)
{
	SetRGBA( colourRGBA.r(), colourRGBA.g(), colourRGBA.b(), colourRGBA.a() );
}

template < typename T >
void ColourRGBA<T>::SetRGBA(const_reference red, const_reference green, const_reference blue, const_reference alpha)
{
	this->r() = red;
	this->g() = green;
	this->b() = blue;
	this->a() = alpha;
}

template < typename T >
void ColourRGBA<T>::Clamp()
{

}

template < typename T >
void ColourRGBA<T>::ScaleByMax()
{
}




#endif