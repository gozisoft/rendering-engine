﻿#ifndef __BEZIER_CURVE_H__
#define __BEZIER_CURVE_H__

#include "Vector.h"
#include "Helper.h"


namespace Engine
{
	template < typename T >
	class MultiArray
	{
	public:
		typedef MultiArray<T> this_type;

		typedef T type;
		typedef T* pointer;
		typedef T& reference;
		typedef const T* const_pointer;
		typedef const T& const_reference;

		MultiArray(size_t rows, size_t cols) :
		m_rows(rows),
		m_cols(cols),
		m_data(rows*cols)
		{

		}

		const_reference operator() (size_t row, size_t col) const
		{
			assert( row < GetRowSize() || col < GetColSize() );
			return m_data[ GetColSize() * row + col ];
		}

		reference operator() (size_t row, size_t col)
		{
			return const_cast<reference>( static_cast<const this_type&>(*this)(row, col) );
		}

		size_t GetRowSize() const
		{
			return m_rows;
		}

		size_t GetColSize() const
		{
			return m_cols;
		}

		size_t m_rows;
		size_t m_cols;
		std::vector<T> m_data;
	};



	// Bezier curve of degree N of which follows the general eqation of 
	// B(t)= ∑_(i=0)^n (n¦i) * (1-t)^(n-i) * t^i * b_i
	// B'(t)= n * ∑_(i=0)^n-1 (n-1¦i) * (1-t)^(n-i-1) * t^i * (b_i_+_1 - b_i) first derivative
	template < typename Element >
	class BezierCurve
	{
	public:
		typedef BezierCurve<Element> this_type;

		typedef Element type; 
		typedef const Element const_type;
		typedef Element* pointer;
		typedef Element& reference;
		typedef const_type* const_pointer;
		typedef const_type& const_reference;

		typedef std::vector< Vector3<type> > ctrlPointArray;
		typedef typename ctrlPointArray::iterator ctrlPointItor;
		// typedef Element** array_type;

		typedef MultiArray<Element> array_type;

	public:
		BezierCurve(const Vector3<type>* ctrlPoints, size_t numPoints);
		BezierCurve(const ctrlPointArray& ctrlPoints);
		~BezierCurve();

		// Calculates a position on the cuve from the value t [0,1]
		Vector3<type> GetPosition(const_reference t) const;

		// Calculates a position on the derivative of the cuve from the value t [0,1]
		Vector3<type> GetFirstDerivative(const_reference t) const;

		// number of control points - 1
		const_reference GetDegree() const;

		// normal 
		// control points
		const Vector3<type>* GetControlPoints() const;
		Vector3<type>* GetControlPoints();
		size_t GetNumCtrlPoints() const;

		// 1st derivative
		// Derivative points
		const Vector3<type>* GetDerivedPoints() const;
		Vector3<type>* GetDerivedPoints();
		size_t GetNumDerCtrlPoints() const;

	private:
		ctrlPointArray m_pCtrlPoints;		// array of ctrl points
		ctrlPointArray m_pDerv1CtrlPoints;  // array of derived ctrl points for tangent
		size_t m_degree;					// Bezier curve to the nth degree (cubic, quadratic, etc)
		
		array_type m_pascal;				// 2d-array containing pascals triangle

	};


#include "Beziercurve.inl"


}



#endif




/*
template <typename T>
class dynamic_array
{
public:
dynamic_array(){};
dynamic_array(int rows, int cols)
{
for(int i=0; i<rows; ++i)
{
data_.push_back(std::vector<T>(cols));
}
}

// other ctors ....

inline std::vector<T> & operator[](int i) { return data_[i]; }

inline const std::vector<T> & operator[] (int i) const { return data_[i]; }

// other accessors, like at() ...

void resize(int rows, int cols)
{
data_.resize(rows);
for(int i = 0; i < rows; ++i)
data_[i].resize(cols);
}

// other member functions, like reserve()....

private:
std::vector<std::vector<T> > data_;  
};
*/