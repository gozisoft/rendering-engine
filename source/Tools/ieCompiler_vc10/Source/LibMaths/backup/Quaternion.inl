#ifndef __QUATERNION_INL__
#define __QUATERNION_INL__

template < typename T > 
const Quaternion<T> Quaternion<T>::ZERO( T(0), T(0), T(0), T(0) ); 

template < typename T > 
const Quaternion<T> Quaternion<T>::ONE( T(1), T(1), T(1), T(1) );

template < typename T > 
Quaternion<T>::Quaternion() : array_type()
{

}

template < typename T > template < typename U >
Quaternion<T>::Quaternion(const U& w, const U& x, const U& y, const U& z)
{
	SetWXYZ(w,z,y,z);
}

template < typename T > template < typename U >
Quaternion<T>::Quaternion(const Quaternion<U>& quat)
{
	AssignOpLoopUnroller<type, const U, AssignOpAssign>::Loop<m_arraySize>::eval( GetData(), quat.GetData() );
	return (*this);
}

template < typename T > template < typename U >
Quaternion<T>::Quaternion(const Matrix4<U>& mat)
{
	FromRotationMatrix(mat);
}

template < typename T > template < typename U >
Quaternion<T>::Quaternion(const Vector3<U>& axis, const U& angle)
{
	FromAxisAngle(axis, angle);
}

// set equal
template < typename T > template < typename U > 
Quaternion<T>& Quaternion<T>::operator = (const Quaternion<U>& quat)
{
	AssignOpLoopUnroller<type, const U, AssignOpAssign>::Loop<m_arraySize>::eval( GetData(), quat.GetData() );
	return (*this);
}

//-----------------------------------------------------------------------
// Operators
//-----------------------------------------------------------------------
template < typename T > template < typename U >
Quaternion<T>& Quaternion<T>::operator += (const Quaternion<U>& quat)
{
	return static_cast< Quaternion<T>& >( _AddAssign( static_cast< const Quaternion<U>::array_type& >(quat) ) );
}

template < typename T > template < typename U >
Quaternion<T>& Quaternion<T>::operator -= (const Quaternion<U>& quat)
{
	return static_cast< Quaternion<T>& >( _SubAssign( static_cast< const Quaternion<U>::array_type& >(quat) ) );
}

template < typename T > template < typename U > 
Quaternion<T>& Quaternion<T>::operator *= (const Quaternion<U>& quat)
{
    // NOTE:  Multiplication is not generally commutative, so in most
    // cases p*q != q*p.
    Quaternion<T> result;
    result[0] = (*this).w() * quat.w() - (*this).x() * quat.x() - (*this).y() * quat.y() - (*this).z() * quat.z();
    result[1] = (*this).w() * quat.x() + (*this).x() * quat.w() + (*this).z() * quat.y() - (*this).y() * quat.z();
    result[2] = (*this).w() * quat.y() + (*this).y() * quat.w() + (*this).x() * quat.z() - (*this).z() * quat.x();
    result[3] = (*this).w() * quat.z() + (*this).z() * quat.w() + (*this).y() * quat.x() - (*this).x() * quat.y();

	(*this) = result;

	return (*this);
}

template < typename T > template < typename U >
Quaternion<T>& Quaternion<T>::operator *= (const U& scalar)
{
	return static_cast< Quaternion<T>& >( _MulAssign(scalar) );
}

template < typename T > template < typename U >
Quaternion<T>& Quaternion<T>::operator /= (const U& scalar)
{
	return static_cast< Quaternion<T>& >( _DivAssign(scalar) );
}

template < typename T > template < typename U >
void Quaternion<T>::SetWXYZ(const U& w, const U& x, const U& y, const U& z)
{
	(*this).w() = w;
	(*this).x() = x;
	(*this).y() = y;
	(*this).z() = z;
}

//-----------------------------------------------------------------------
// External Operators
//-----------------------------------------------------------------------
template < typename T, typename U >
Quaternion<T> operator + (const Quaternion<T>& quat0, const Quaternion<U>& quat1)
{
	Quaternion<T> result(quat0);
	return result += scalar;
}

template < typename T, typename U >
Quaternion<T> operator - (const Quaternion<T>& quat0, const Quaternion<U>& quat1)
{
	Quaternion<T> result(quat0);
	return result -= scalar;
}

template < typename T, typename U >
Quaternion<T> operator * (const Quaternion<T>& quat0, const Quaternion<U>& quat1)
{
    // NOTE:  Multiplication is not generally commutative, so in most
    // cases p*q != q*p.
    Quaternion<T> result(quat0);
    return result *= quat1;
}

template < typename T, typename U >
Quaternion<T> operator * (const Quaternion<T>& quat0, const U& scalar)
{
	Quaternion<T> result(quat0);
	return result *= scalar;
}

template < typename T, typename U >
Quaternion<T> operator * (const T& scalar, const Quaternion<U>& quat0)
{
	return quat0 * scalar;
}

template < typename T, typename U >
Quaternion<T> operator / (const Quaternion<T>& quat0, const U& scalar)
{
	Quaternion<T> result(quat0);
	return result /= scalar;
}

template < typename T, typename U >
Quaternion<T> operator - (const Quaternion<U>& quat0)
{
	Quaternion<T> result(quat0);
	return result *= T(-1);
}

//-----------------------------------------------------------------------
// Internal Functions :: Conversion between quaternions, matrices, and axis-angle.
//-----------------------------------------------------------------------

// unitise Quaternion
template < typename T >
const Quaternion<T>& Quaternion<T>::Normalise()
{
	return !IsZero(*this) ? (*this) /= Mag(*this) : (*this);
}

template < typename T > template < typename U > 
void Quaternion<T>::FromRotationMatrix(const Matrix4<U>& rotation)
{
    // Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
    // article "Quaternion Calculus and Fast Animation".

    T trace = rotation(0,0) + rotation(1,1) + rotation(2,2);
    T root = T(0);

    if (trace > (T)0)
    {
        // |w| > 1/2, may as well choose w > 1/2
        root = std::sqrt( trace + (T)1 );  // 2w
        (*this)[0] = T(0.5) * root;
        root = T(0.5) / root;  // 1/(4w)
        (*this)[1] = ( rotation(1,2) - rotation(2,1) ) * root;
        (*this)[2] = ( rotation(2,0) - rotation(0,2) ) * root;
        (*this)[3] = ( rotation(0,1) - rotation(1,0) ) * root;
    }
    else
    {
		// |w| <= 1/2
        int i = 0;
		const int next[3] = { 1, 2, 0 };

        if (rotation(1,1) > rotation(0,0))
        {
            i = 1;
        }
        if (rotation(2,2) > rotation(i,i))
        {
            i = 2;
        }
        int j = next[i];
        int k = next[j];
        root = std::sqrt( ( rotation(i,i) - (rotation(j,j) + rotation(k,k)) + T(1) ) );

        T* quat[3] = { &(*this)[1], &(*this)[2], &(*this)[3] };
        *quat[i] = T(0.5) * root;
        root = T(0.5) / root;

        (*this)[0] = (rotation(j,k) - rotation(k,j)) * root; // w
        *quat[j] = (rotation(i,j) + rotation(j,i)) * root;
        *quat[k] = (rotation(i,k) + rotation(k,i)) * root;
    }
}

template < typename T > template < typename U > 
void Quaternion<T>::ToRotationMatrix(Matrix4<U>& rotation) const
{
    // Converts this quaternion to a rotation matrix.
    //
    //  | 1 - 2(y^2 + z^2)	2(xy + wz)			2(xz - wy)		 |
    //  | 2(xy - wz)		1 - 2(x^2 + z^2)	2(yz + wx)		 |
    //  | 2(xz + wy)		2(yz - wx)			1 - 2(x^2 + y^2) |

    T twoX  = (*this)[1]+(*this)[1];
    T twoY  = (*this)[2]+(*this)[2];
    T twoZ  = (*this)[3]+(*this)[3];
    T twoWX = twoX*(*this)[0];
    T twoWY = twoY*(*this)[0];
    T twoWZ = twoZ*(*this)[0];
    T twoXX = twoX*(*this)[1];
    T twoXY = twoY*(*this)[1];
    T twoXZ = twoZ*(*this)[1];
    T twoYY = twoY*(*this)[2];
    T twoYZ = twoZ*(*this)[2];
    T twoZZ = twoZ*(*this)[3];

    rotation(0,0) = T(1) - (twoYY + twoZZ);
    rotation(0,1) = twoXY + twoWZ;
    rotation(0,2) = twoXZ - twoWY;

    rotation(1,0) = twoXY - twoWZ;
    rotation(1,1) = T(1) - (twoXX + twoZZ);
    rotation(1,2) = twoYZ + twoWX;

    rotation(2,0) = twoXZ + twoWY;
    rotation(2,1) = twoYZ - twoWX;
    rotation(2,2) = T(1) - (twoXX + twoYY);
}

template < typename T > template < typename U >
void Quaternion<T>::FromAxisAngle(const Vector3<U>& axis, const U& angle)
{
    // assert:  axis[] is unit length
    //
    // The quaternion representing the rotation is
    //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

    T halfAngle = T(0.5) * angle;
    T sn = std::sin(halfAngle);
    (*this)[0] = std::cos(halfAngle);
    (*this)[1] = sn*axis[0];
    (*this)[2] = sn*axis[1];
    (*this)[3] = sn*axis[2];
}

template < typename T > template < typename U > 
void Quaternion<T>::ToAxisAngle(Vector3<U>& axis, U& angle) const
{
    // The quaternion representing the rotation is
    //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

    T sqrLength = (*this)[1]*(*this)[1] +
				  (*this)[2]*(*this)[2] + 
				  (*this)[3]*(*this)[3];

    if ( sqrLength > Const<T>::ROUNDING_ERROR() )
    {
		angle = T(2) * std::acos( (*this)[0] );
		T invLength = InvSqrt(sqrLength);
        axis[0] = (U)( (*this)[1]*invLength );
        axis[1] = (U)( (*this)[2]*invLength );
        axis[2] = (U)( (*this)[3]*invLength );
    }
}

template < typename T > template < typename U > 
void Quaternion<T>::ToHeadPitchRoll(Vector3<U>& rotation) const
{
    Matrix4<U> mat;
	ToRotationMatrix(mat);
    ToHeadPitchRoll(mat, rotation);
}

//-----------------------------------------------------------------------
// External Functions :: Conversion between quaternions, matrices, and axis-angle.
//-----------------------------------------------------------------------

// modulus / length / magnitude
template < typename T >
T Mag(const Quaternion<T>& quat)
{
	return std::sqrt( Dot(quat,quat) );
}

// modulus / length / magnitude (removes square root for faster comparisons)
template < typename T >
T MagSqrd(const Quaternion<T>& quat)
{
	return Dot(quat,quat);
}

// dot product
template < typename T, typename U >
T Dot(const Quaternion<T>& quat0, const Quaternion<U>& quat1)
{
	T ret = T(0);
	for (size_t i = 0; i < quat0.GetSize(); ++i)
		ret += quat0[i] * quat1[i];

	return ret;
}

// component-wise absolute value
template < typename T >
Quaternion<T> Abs(const Quaternion<T>& quat)
{
	Quaternion<T> ret;
	for (size_t i = 0; i < quat.GetSize(); ++i)
		ret[i] = Abs(v[i]);

	return ret;
}

// component-wise minimum vector
template < typename T, typename U >
Quaternion<T> Min(const Quaternion<T>& quat0, const Quaternion<U>& quat1)
{
	Quaternion<T> ret;
	for (size_t i = 0; i < quat0.GetSize(); ++i)
		ret[i] = Min(quat0[i], quat1[i]);

	return ret;
}

// component-wise maximum vector
template < typename T, typename U >
Quaternion<T> Max(const Quaternion<T>& quat0, const Quaternion<U>& quat1)
{
	Quaternion<T> ret;
	for (size_t i = 0; i < quat0.GetSize(); ++i)
		ret[i] = Max(quat0[i], quat1[i]);

	return ret;
}

//! test for null vector
template < typename T >
const bool IsZero(const Quaternion<T> &quat)
{
	for (size_t i = 0; i < quat.GetSize(); ++i)
		if ( !IsZero( quat[i], Abs(quat[i]) ) )
			return false;

	return true;
}


// Rotation of a vector by a quaternion.
template < typename T, typename U >
Vector3<T> Rotate(const Quaternion<T>& quat, const Vector3<U>& vec)
{
    // Given a vector u = (x0,y0,z0) and a unit length quaternion
    // q = <w,x,y,z>, the vector v = (x1,y1,z1) which represents the
    // rotation of u by q is v = q*u*q^{-1} where * indicates quaternion
    // multiplication and where u is treated as the quaternion <0,x0,y0,z0>.
    // Note that q^{-1} = <w,-x,-y,-z>, so no real work is required to
    // invert q.  Now
    //
    //   q*u*q^{-1} = q*<0,x0,y0,z0>*q^{-1}
    //     = q*(x0*i+y0*j+z0*k)*q^{-1}
    //     = x0*(q*i*q^{-1})+y0*(q*j*q^{-1})+z0*(q*k*q^{-1})
    //
    // As 3-vectors, q*i*q^{-1}, q*j*q^{-1}, and 2*k*q^{-1} are the columns
    // of the rotation matrix computed in Quaternion<Real>::ToRotationMatrix.
    // The vector v is obtained as the product of that rotation matrix with
    // vector u.  As such, the quaternion representation of a rotation
    // matrix requires less space than the matrix and more time to compute
    // the rotated vector.  Typical space-time tradeoff...

    Matrix4<T> rot;
    quat.ToRotationMatrix(rot);
    return rot*vec;
}


#endif