#ifndef __VECTOR3_INL__
#define __VECTOR3_INL__


template < typename T > const Vector3<T> Vector3<T>::UNIT_X (T(1), T(0), T(0));
template < typename T > const Vector3<T> Vector3<T>::UNIT_Y (T(0), T(1), T(0));
template < typename T > const Vector3<T> Vector3<T>::UNIT_Z (T(0), T(0), T(1));

template < typename T > const Vector3<T> Vector3<T>::ZERO (T(0), T(0), T(0));
template < typename T > const Vector3<T> Vector3<T>::ONE (T(1), T(1), T(1));

//-----------------------------------------------------------------------
// Constructors
//-----------------------------------------------------------------------
template < typename T >
Vector3<T>::Vector3() : array_type()
{

}

template < typename T > template < typename U >
Vector3<T>::Vector3(const Vector3<U> & vector3)
{
	AssignOpLoopUnroller<type, const U, AssignOpAssign>::Loop<m_arraySize>::eval( GetData(), v1.GetData() );
}

template < typename T > template < typename U >
Vector3<T>::Vector3(const U& x, const U& y, const U& z)
{
	SetXYZ(x, y, z);	
}

//-----------------------------------------------------------------------
// Operators
//-----------------------------------------------------------------------
// assignment :: Vector
template < typename T > template < typename U >
Vector3<T> & Vector3<T>::operator += (const Vector3<U> & v1)
{
	return static_cast< Vector3<T>& >( _AddAssign( static_cast< const Vector3<U>::array_type& >(v1) ) );
}

template < typename T > template < typename U >
Vector3<T> & Vector3<T>::operator -= (const Vector3<U> & v1)
{
	return static_cast< Vector3<T>& >( _SubAssign( static_cast< const Vector3<U>::array_type& >(v1) ) );
}

template < typename T > template < typename U >
Vector3<T> & Vector3<T>::operator *= (const Vector3<U> & v1)
{
	return static_cast< Vector3<T>& >( _MulAssign( static_cast< const Vector3<U>::array_type& >(v1) ) );
}

// assignment :: scalar 
template < typename T > template < typename U >
Vector3<T> & Vector3<T>::operator *= (const U& val)
{
	return static_cast< Vector3<T>& >( _MulAssign( val ) );
}

template < typename T > template < typename U >
Vector3<T> & Vector3<T>::operator /= (const U& val)
{
	return static_cast< Vector3<T>& >( _DivAssign( val ) );
}

// assignment :: set equal
template < typename T > template < typename U > 
Vector3<T>& Vector3<T>::operator = (const Vector3<U>& v1)
{
	AssignOpLoopUnroller<type, const U, AssignOpAssign>::Loop<m_arraySize>::eval( GetData(), v1.GetData() );
	return (*this);
}

//-----------------------------------------------------------------------
// External Operators
//-----------------------------------------------------------------------
// vector vector
template < typename T, typename U >
Vector3<T> operator + (const Vector3<T>& v1, const Vector3<U>& v2)
{
	Vector3<T> out(v1);
	return out += v2;
}

template < typename T, typename U >
const Vector3<T> operator - (const Vector3<T>& v1, const Vector3<U>& v2)
{
	Vector3<T> out(v1);
	return out -= v2;
}

// Not mathimatically defined
template < typename T, typename U >
Vector3<T> operator * (const Vector3<T>& v1, const Vector3<U>& v2)
{
	Vector3<T> out(v1)
	return out *= v2;
}

// vector scalar

//! multiplication by scalar
template < typename T, typename U >
Vector3<T> operator * (const Vector3<T>& v, const U& s)
{
	Vector3<T> out(v);
	return out *= s;
}

//! multiplication by scalar
template < typename T, typename U >
Vector3<T> operator * (const U &s, const Vector3<T>& v)
{
	Vector3<T> out(v);
	return out *= s;
}

//! division by scalar
template < typename T, typename U >
Vector3<T> operator / (const Vector3<T>& v, const U &s)
{
	Vector3<T> out(v);
	return out /= s;
}

//! division by scalar
template < typename T, typename U >
Vector3<T> operator / (const U &s, const Vector3<T>& v)
{
	Vector3<T> out(v1);
	return out /= s;
}

// assignment :: set negative
template < typename T >
const Vector3<T>& operator - (Vector3<T>& v1)
{
	return v1 *= T(-1);
}

//-----------------------------------------------------------------------
// Internal Functions
//-----------------------------------------------------------------------
template < typename T > template < typename U >
void Vector3<T>::SetXYZ(const U& x, const U& y, const U& z)
{
	(*this).x() = x;
	(*this).y() = y;
	(*this).z() = z;
}

template < typename T >
const Vector3<T>& Vector3<T>::Normalise()
{
	return !IsZero(*this) ? (*this) /= Mag(*this) : (*this);
}

	// canonical rotations
template < typename T >
const Vector3<T> & Vector3<T>::RotateX(Degree angle)
{
	Radian s = sin( angle * Const<T>::TO_RAD() );
	Radian c = cos( angle * Const<T>::TO_RAD() );

	y() = c * y() - s * z();
	z() = c * z() + s * y();

	return *this;
}

template < typename T >
const Vector3<T> & Vector3<T>::RotateY(Degree angle)
{
	Radian s = sin( angle * Const<T>::TO_RAD() );
	Radian c = cos( angle * Const<T>::TO_RAD() );

	x() = c * x() + s * z();
	z() = c * z() - s * x();

	return *this;
}

template < typename T >
const Vector3<T> & Vector3<T>::RotateZ(Degree angle)
{
	Radian s = sin( angle * Const<T>::TO_RAD() );
	Radian c = cos( angle * Const<T>::TO_RAD() );

	x() = c * x() - s * y();
	y() = c * y() + s * x();

	return *this;
}

template < typename T >
Vector3<T> Vector3<T>::GetHorizontalAngle()
{
	Vector3<T> angle;

	// Set the yaw (y axis)
	const double tmp = (double)( std::atan2( (double)x(), (double)z() ) * Const<double>::TO_DEG() );
	angle.y() = (T)tmp;

	if (angle.y() < 0)
		angle.y() += 360;
	if (angle.y() >= 360)
		angle.y() -= 360;

	// Set the pitch (x axis)
	const double z1 = std::sqrt( (double)( Sqr( x() ) + Sqr( z() ) ) );
	angle.x() = (T)( std::atan2( (double)z1, (double)y() ) * Const<double>::TO_DEG() - 90.0 );

	if (angle.x() < 0)
		angle.x() += 360;
	if (angle.x() >= 360)
		angle.x() -= 360; 

	return angle;
}

template < typename T >
Vector3<T> Vector3<T>::GetSphericalCoordinateAngles()
{
	Vector3<T> angle;
	const double length = Sqr( x() ) + Sqr( y() ) + Sqr( z() );

	if (length)
	{
		if ( x() != 0 )
		{
			angle.y() = (T)( atan2( double( z() ),  double( x() ) ) * Const<double>::TO_DEG() );
		}
		else if ( z() < 0 )
		{
			angle.y() = 180;
		}

		angle.x() = (T)( acos( y() * T(1) / sqrt(length) ) * Const<double>::TO_DEG() );
	}

	return angle;
}

template < typename T >
Vector3<T> Vector3<T>::GetYawPitch()
{
	// Calculate yaw and pitch 
	Vector3<T> angle;

	const T& lookLengthOnXZ = sqrt( Sqr( z() ) + Sqr( x() ) ); // Sqrt ( Z^2 + X^2 )
	angle.x() = atan2( y(), lookLengthOnXZ ); 
	angle.y() = atan2( x(), z() );

	return angle;
}

template < typename T >
Vector3<T> Vector3<T>::RotationToDirection(const Vector3<T> & forwards) const
{
	const double cr = cos( Const<double>::TO_DEG() * x() );
	const double sr = sin( Const<double>::TO_DEG() * x() );
	const double cp = cos( Const<double>::TO_DEG() * y() );
	const double sp = sin( Const<double>::TO_DEG() * y() );
	const double cy = cos( Const<double>::TO_DEG() * z() );
	const double sy = sin( Const<double>::TO_DEG() * z() );

	const double srsp = sr*sp;
	const double crsp = cr*sp;

	const double pseudoMatrix[] = {
		( cp*cy ), ( cp*sy ), ( -sp ),
		( srsp*cy-cr*sy ), ( srsp*sy+cr*cy ), ( sr*cp ),
		( crsp*cy+sr*sy ), ( crsp*sy-sr*cy ), ( cr*cp )};

		return Vector3<T>(
			(T)(forwards.x() * pseudoMatrix[0] +
				forwards.y() * pseudoMatrix[3] +
				forwards.z() * pseudoMatrix[6]),
			(T)(forwards.x() * pseudoMatrix[1] +
				forwards.y() * pseudoMatrix[4] +
				forwards.z() * pseudoMatrix[7]),
			(T)(forwards.x() * pseudoMatrix[2] +
				forwards.y() * pseudoMatrix[5] +
				forwards.z() * pseudoMatrix[8]));
}

//-----------------------------------------------------------------------
// External Functions
//-----------------------------------------------------------------------
//! modulus / length / magnitude
template < typename T >
const float Mag(const Vector3<T> &v)
{
	return sqrt(Dot(v,v));
}

//! modulus squared (removes square root for faster comparisons)
template < typename T >
const float MagSqrd(const Vector3<T> &v)
{
	return Dot(v,v);
}

//! euclidean distance
template < typename T, typename U >
const T Dist(const Vector3<T> &v1, const Vector3<U> &v2)
{
	return Mag(v1-v2);
}

//! euclidean distance squared (again, for comparisons)
template < typename T, typename U >
const T DistSqrd(const Vector3<T> &v1, const Vector3<U> &v2)
{
	return MagSqrd(v1-v2);
}

//! randomise a vector within the given range
template < typename T >
const Vector3<T> Random(Vector3<T> &v, const T &low, const T &high)
{
	for (size_t i = 0; i < 3; ++i)
		v[i] = Random(low, high);

	return v;
}

//! midpoint of two vectors
template < typename T, typename U >
const Vector3<T> Midpoint(const Vector3<T> &v1, const Vector3<U> &v2)
{
	Vector3<T> ret;
	for (size_t i = 0; i < 3; ++i)
		ret[i] = (v1[i] + v2[i]) / T(2);

	return ret;
}

//! component-wise absolute value
template < typename T >
const Vector3<T> Abs(const Vector3<T> &v)
{
	Vector3<T> ret;
	for (size_t i = 0; i < 3; ++i)
		ret[i] = Abs(v[i]);

	return ret;
}

//! component-wise minimum vector
template < typename T, typename U >
const Vector3<T> Min(const Vector3<T> &v1, const Vector3<U> &v2)
{
	Vector3<T> ret;
	for (size_t i = 0; i < 3; ++i)
		ret[i] = Min(v1[i], v2[i]);

	return ret;
}

//! component-wise maximum vector
template < typename T, typename U >
const Vector3<T> Max(const Vector3<T> &v1, const Vector3<U> &v2)
{
	Vector3<T> ret;
	for (size_t i = 0; i < 3; ++i)
		ret[i] = Max(v1[i], v2[i]);

	return ret;
}

//! linear interpolation
template < typename T, typename U >
const Vector3<T> Lerp(const Vector3<T> &v1, const Vector3<U> &v2, const T &t)
{
	return v1 + (v2 - v1) * t;
}

//! dot product
template < typename T, typename U >
const T Dot(const Vector3<T> &v1, const Vector3<U> &v2)
{
	T ret = 0;
	for (size_t i = 0; i < 3; ++i)
		ret += v1[i] * v2[i];

	return ret;

	// return DotProductUnroller<const T, const U>::Loop<Vector3<T> >::eval( &v1[0], &v2[0] );
}

//! 3D cross product
template < typename T, typename U >
const Vector3<T> Cross(const Vector3<T> &v1, const Vector3<U> &v2)
{
	return Vector3<T>(v1[1] * v2[2] - v1[2] * v2[1],
		v1[2] * v2[0] - v1[0] * v2[2],
		v1[0] * v2[1] - v1[1] * v2[0]);
}

//! scalar triple product / mixed product
template < typename T, typename U, typename V >
const T ScaTrip(const Vector3<T> &v1, const Vector3<U> &v2, const Vector3<V> &v3)
{
	return Dot(v1,Cross(v2,v3));
}

//! vector triple product
template < typename T, typename U, typename V >
const Vector3<T> VecTrip(const Vector3<T> &v1, const Vector3<U> &v2, const Vector3<V> &v3)
{
	// lagrange's formula [a x (b x c) = b(a . c) - c(a . b)]
	return v2 * Dot(v1,v3) - v3 * Dot(v1,v2);
}

//! angle between vectors in radians
template < typename T >
const T Angle(const Vector3<T> &v1, const Vector3<T> &v2)
{
	return acos( Dot(v1,v2) );
}

//! project a vector onto another
template < typename T, typename U >
Vector3<T> Project(const Vector3<T> &v1, const Vector3<U> &v2)
{
	assert( Dot(v2,v2) != T(0) );
	return ( v2 * Dot(v1,v2) ) / Dot(v2,v2);
}

//! reflect a vector about its normal
template < typename T, typename U >
Vector3<T> Reflect(const Vector3<T> &v, const Vector3<U> &n)
{
	return v - (T(2) * Dot(v,n) * n);
}

//! unitised vector copy
template < typename T >
Vector3<T> Normalised(const Vector3<T>& v)
{
	Vector3<T> ret(v);
	return ret.Normalise();
}

//! test for null vector
template < typename T >
const bool IsZero(const Vector3<T>& v)
{
	for (size_t i = 0; i < 3; ++i)
	{
		if ( !IsZero( v[i], Abs(v[i]) ) )
			return false;
	}

	return true;
}

//! test for unit vector
template < typename T >
const bool IsUnit(const Vector3<T> &v)
{
	return IsZero(T(1), Mag(v), Mag(v));
}

//! test for parallel vectors
template < typename T, typename U >
const bool IsParallel(const Vector3<T> &v1, const Vector3<U> &v2)
{
	return IsZero( Cross(v1,v2) );
}

//! test for perpendicular vectors
template < typename T, typename U >
const bool IsPerp(const Vector3<T> &v1, const Vector3<U> &v2)
{
	return IsZero( Dot(v1,v2), Abs( Dot(v1,v2) ) );
}

// Get the rotations that would make a (0,0,1) direction vector
// point in the same direction as this direction vector.
template < typename T >
Vector3<T> GetHorizontalAngle(const Vector3<T>& v1)
{
	Vector3<T> angle;

	const T tmp = std::atan2(v1.x(), v1.z()) * Const<T>::TO_DEG();
	angle.y() = (T)tmp;

	if (angle.y() < 0)
		angle.y() += 360;
	if (angle.y() >= 360)
		angle.y() -= 360;

	const T lookLengthOnXZ = sqrt( Sqr( v1.x() ) + Sqr( v1.z() ) );
	angle.x() = std::atan2(v1.y(), lookLengthOnXZ) * Const<T>::TO_DEG() - 90.0;

	if (angle.x() < 0)
		angle.x() += 360;
	if (angle.x() >= 360)
		angle.x() -= 360;

	return angle;
}

// Keep camera's axes orthogonal to each other and of unit length.
template < typename T >
void OrthoNormaliseVectors(Vector3<T> &zaxis, Vector3<T> &yaxis, Vector3<T> &xaxis)
{
    // If the input vectors are v0, v1, and v2, then the Gram-Schmidt
    // orthonormalization produces vectors u0, u1, and u2 as follows,
    //
    //   u0 = v0/|v0|
    //   u1 = (v1-(u0*v1)u0)/|v1-(u0*v1)u0|
    //   u2 = (v2-(u0*v2)u0-(u1*v2)u1)/|v2-(u0*v2)u0-(u1*v2)u1|
    //
    // where |A| indicates length of vector A and A*B indicates dot
    // product of vectors A and B.

	// set the direction vector
	zaxis.Normalise();

    // set the up vector
    T dot0 = Dot(zaxis, yaxis); 
    yaxis -= dot0 * zaxis;
    yaxis.Normalise();

    // set the right vector
    T dot1 = Dot(yaxis, xaxis); 
    dot0 = Dot(zaxis, xaxis);
    xaxis -= (dot0 * zaxis) + (dot1 * yaxis);
    xaxis.Normalise();
}

///////////////////////////////////////////////////////////////////////////////
// convert Euler angles(x,y,z) to axes(left, up, forward)
// Each column of the rotation matrix represents left, up and forward axis.
// The order of rotation is Roll->Yaw->Pitch (Rx*Ry*Rz)
// Rx: rotation about X-axis, pitch
// Ry: rotation about Y-axis, yaw(heading)
// Rz: rotation about Z-axis, roll
//    Rx           Ry          Rz
// |1  0   0| | Cy  0 Sy| |Cz -Sz 0|   | CyCz        -CySz         Sy  |
// |0 Cx -Sx|*|  0  1  0|*|Sz  Cz 0| = | SxSyCz+CxSz -SxSySz+CxCz -SxCy|
// |0 Sx  Cx| |-Sy  0 Cy| | 0   0 1|   |-CxSyCz+SxSz  CxSySz+SxCz  CxCy|
// 
// ***Right hand rule***
///////////////////////////////////////////////////////////////////////////////
template < typename T >
void AnglesToAxisRH(const Vector3<T>& rotation, Vector3<T>& xaxis, Vector3<T>& yaxis, Vector3<T>& zaxis)
{
	// rotation angle about X-axis (pitch)
	double theta = rotation.x() * Const<double>::TO_RAD();
	double sx = sin(theta);
	double cx = cos(theta);

	// rotation angle about Y-axis (yaw)
	theta = rotation.y() * Const<double>::TO_RAD();
	double sy = sin(theta);
	double cy = cos(theta);

	// rotation angle about Z-axis (roll)
	theta = rotation.z() * Const<double>::TO_RAD();
	double sz = sin(theta);
	double cz = cos(theta);

	// determine right axis
	xaxis.x() = static_cast<T>(cy*cz);
	xaxis.y() = static_cast<T>(sx*sy*cz + cx*sz);
	xaxis.z() = static_cast<T>(-cx*sy*cz + sx*sz);

	// determine up axis
	yaxis.x() = static_cast<T>(-cy*sz);
	yaxis.y() = static_cast<T>(-sx*sy*sz + cx*cz);
	yaxis.z() = static_cast<T>(cx*sy*sz + sx*cz);

	// determine forward axis
	zaxis.x() = static_cast<T>(sy);
	zaxis.y() = static_cast<T>(-sx*cy);
	zaxis.z() = static_cast<T>(cx*cy);
}


///////////////////////////////////////////////////////////////////////////////
// convert Euler angles(x,y,z) to axes(left, up, forward)
// Each column of the rotation matrix represents left, up and forward axis.
// The order of rotation is Roll->Yaw->Pitch (Rx*Ry*Rz)
// Rx: rotation about X-axis, pitch
// Ry: rotation about Y-axis, yaw(heading)
// Rz: rotation about Z-axis, roll
//    Rx           Ry          Rz
// |1  0   0| |Cy  0 -Sy| | Cz  Sz 0|   | CyCz         CySz         -Sy  |
// |0 Cx  Sx|*| 0  1   0|*|-Sz  Cz 0| = | SxSyCz-CxSz  SxSySz+CxCz  SxCy |
// |0 -Sx Cx| |Sy  0  Cy| | 0   0  1|   | CxSyCz+SxSz  CxSySz-SxCz  CxCy |
// 
// ***Left hand rule*** clockwise rotation
///////////////////////////////////////////////////////////////////////////////
template < typename T >
void AnglesToAxisLH(const Vector3<T>& rotation, Vector3<T>& xaxis, Vector3<T>& yaxis, Vector3<T>& zaxis)
{
	// rotation angle about X-axis (pitch)
	double theta = rotation.x() * Const<double>::TO_RAD();
	double sx = sin(theta);
	double cx = cos(theta);

	// rotation angle about Y-axis (yaw)
	theta = rotation.y() * Const<double>::TO_RAD();
	double sy = sin(theta);
	double cy = cos(theta);

	// rotation angle about Z-axis (roll)
	theta = rotation.z() * Const<double>::TO_RAD();
	double sz = sin(theta);
	double cz = cos(theta);

	// determine first row
	xaxis[0] = static_cast<T>(cy*cz);
	yaxis[0] = static_cast<T>(cy*sz);
	zaxis[0] = static_cast<T>(-sy);

	// determine second row
	xaxis[1] = static_cast<T>(sx*sy*cz - cx*sz);
	yaxis[1] = static_cast<T>(sx*sy*sz + cx*cz);
	zaxis[1] = static_cast<T>(sx*cy);

	// determine third row
	xaxis[2] = static_cast<T>(cx*sy*cz + sx*sz);
	yaxis[2] = static_cast<T>(cx*sy*sz - sx*cz);
	zaxis[2] = static_cast<T>(cx*cy);
}

// compute transform axis from object position and target point
template < typename T >
void lookAtToAxis(const Vector3<T>& pos, const Vector3<T>& target,
	Vector3<T>& xaxis, Vector3<T>& yaxis, Vector3<T>& zaxis)
{
	// compute the forward vector
	zaxis = target - pos;
	zaxis.Normalise();

	// compute temporal up vector based on the forward vector
    // watch out when look up/down at 90 degree
    // for example, forward vector is on the Y axis
	const T epsilon = 0.001;
	if (Abs( zaxis.x() ) < epsilon && Abs( zaxis.z() ) < epsilon)
	{
		// forward vector is pointing +Y axis
		if ( zaxis.y() > 0 )
			yaxis = Vector3<T>(0, 0, -1);
		// forward vector is pointing -Y axis
		else
			yaxis = Vector3<T>(0, 0, 1);
	}
	else // in general, up vector is straight up
	{
		yaxis = Vector3<T>(0, 1, 0);
	}

	// compute the right vector
	xaxis = Cross(yaxis, zaxis);
	xaxis.Normalise();

	// re-calculate the orthonormal up vector
	yaxis = Cross(zaxis, xaxis);
	yaxis.Normalise();
}

template < typename T >
void lookAtToAxis(const Vector3<T>& pos, const Vector3<T>& target, const Vector3<T>& upDir,
                  Vector3<T>& xaxis, Vector3<T>& yaxis, Vector3<T>& zaxis)
{
    // compute the forward vector
    zaxis = target - pos;
    zaxis.Normalise();

    // compute the left vector
    xaxis = Cross(upDir, zaxis); 
    xaxis.Normalise();

    // compute the orthonormal up vector
    yaxis = Cross(zaxis, xaxis); 
    yaxis.Normalise();
}



#endif