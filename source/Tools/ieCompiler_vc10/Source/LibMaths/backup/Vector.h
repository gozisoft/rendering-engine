#ifndef __VECTOR_H__
#define __VECTOR_H__

#include "MathsFwd.h"
#include "FixedArray.h"
#include "Maths.h"

_ENGINE_BEGIN
 
template < typename T >
class Vector2 : public FixedArray<2, T>
{
public:
	typedef FixedArray<2, T> array_type;
	typedef typename array_type::type			 type;
	typedef typename array_type::pointer		 pointer;
	typedef typename array_type::reference		 reference;
	typedef typename array_type::const_pointer   const_pointer;
	typedef typename array_type::const_reference const_reference;

	typedef type Degree, Radian;

public:
	// Ctors
	Vector2();

	template < typename U > Vector2(const U& x, const U& y);

	// Copy ctor
	template < typename U > Vector2(const Vector2<U>& other);

	// assignment :: set equal
	template < typename U > Vector2& operator = (const Vector2<U>& v1);

	// assignment :: Vector
	template < typename U >	Vector2& operator += (const Vector2<U> & v1);
	template < typename U > Vector2& operator -= (const Vector2<U> & v1);
	template < typename U > Vector2& operator *= (const Vector2<U> & v1);

	// assignment :: scalar 
	template < typename U > Vector2 & operator *= (const U& val);
	template < typename U > Vector2 & operator /= (const U& val);

	// set methods
	template < typename U > void SetXY(const U& x, const U& y);

	// unitise vector
	const Vector2 & Normalise();

	// const accessor methods
	const_reference x() const { return (*this)[0]; }
	const_reference y() const { return (*this)[1]; }

	// accessor methods
	reference x() { return (*this)[0]; }
	reference y() { return (*this)[1]; }

	static const Vector2 UNIT_X; //!< i-versor
	static const Vector2 UNIT_Y; //!< j-versor

	// null & one vectors
	static const Vector2 ZERO;
	static const Vector2 ONE;	
};


template < typename T >
class Vector3 : public FixedArray<3, T>
{
public:
	typedef FixedArray<3, T>					 array_type;
	typedef Vector3<T>							 this_type;
	typedef typename array_type::type			 type;
	typedef typename array_type::pointer		 pointer;
	typedef typename array_type::reference		 reference;
	typedef typename array_type::const_pointer   const_pointer;
	typedef typename array_type::const_reference const_reference;
	typedef T Degree, Radian;

public:
	// Ctor
	Vector3();

	template < typename U > Vector3(const U& x, const U& y, const U& z);

	// Copy ctor
	template < typename U > Vector3(const Vector3<U>& other);

	// assignment :: set equal
	template < typename U > Vector3 & operator = (const Vector3<U>& v1);

	// assignment :: Vector
	template < typename U >	Vector3 & operator += (const Vector3<U> & v1);
	template < typename U > Vector3 & operator -= (const Vector3<U> & v1);
	template < typename U > Vector3 & operator *= (const Vector3<U> & v1);

	// assignment :: scalar 
	template < typename U > Vector3& operator *= (const U& val);
	template < typename U > Vector3& operator /= (const U& val);

	// Set all compoents
	template < typename U > void SetXYZ(const U& x, const U& y, const U& z);

	//! unitise vector
	const Vector3 & Normalise();

	// canonical rotations
	const Vector3 & RotateX(Degree angle);
	const Vector3 & RotateY(Degree angle);
	const Vector3 & RotateZ(Degree angle);

	// orientate towards specific targets
	Vector3 GetHorizontalAngle();
	Vector3 GetYawPitch();
	Vector3 GetSphericalCoordinateAngles();
	Vector3 RotationToDirection(const Vector3& forwards= Vector3::UINT_Z) const;

	// const accessor methods
	const_reference x() const { return (*this)[0]; }
	const_reference y() const { return (*this)[1]; }
	const_reference z() const { return (*this)[2]; }

	// accessor methods
	reference x() { return (*this)[0]; }
	reference y() { return (*this)[1]; }
	reference z() { return (*this)[2]; }
	
	// unit vectors
	static const Vector3 UNIT_X; //!< i-versor
	static const Vector3 UNIT_Y; //!< j-versor
	static const Vector3 UNIT_Z; //!< k-versor

	// null & one vectors
	static const Vector3 ZERO;
	static const Vector3 ONE;
};


template < typename T >
class Vector4 : public FixedArray<4, T>
{
public:
	typedef FixedArray<4, T>					 array_type;
	typedef Vector4<T>							 this_type;
	typedef typename array_type::type			 type;
	typedef typename array_type::pointer		 pointer;
	typedef typename array_type::reference		 reference;
	typedef typename array_type::const_pointer   const_pointer;
	typedef typename array_type::const_reference const_reference;
	typedef T Degree, Radian;

public:
	Vector4();

	template < typename U > Vector4(const U& x, const U& y, const U& z, const U& w);

	template < typename U > Vector4(const Vector4<U>& v1);

	// assignment :: set equal
	template < typename U > Vector4& operator = (const Vector4<U>& other);

	// assignment :: Vector
	template < typename U >	Vector4 & operator += (const Vector4<U> & v1);
	template < typename U > Vector4 & operator -= (const Vector4<U> & v1);
	template < typename U > Vector4 & operator *= (const Vector4<U> & v1);

	// assignment :: scalar 
	template < typename U > Vector4 & operator *= (const U& val);
	template < typename U > Vector4 & operator /= (const U& val);

	// Set function
	template < typename U > void SetXYZW(const U& x, const U& y, const U& z, const U& w);

	//! unitise vector
	const Vector4 & Normalise();

	// const accessor methods
	const_reference x() const { return (*this)[0]; }
	const_reference y() const { return (*this)[1]; }
	const_reference z() const { return (*this)[2]; }
	const_reference w() const { return (*this)[3]; }

	// accessor methods
	reference x() { return (*this)[0]; }
	reference y() { return (*this)[1]; }
	reference z() { return (*this)[2]; }
	reference w() { return (*this)[3]; }

	// unit vectors
	static const Vector4 UNIT_X; //!< i-versor
	static const Vector4 UNIT_Y; //!< j-versor
	static const Vector4 UNIT_Z; //!< k-versor
	static const Vector4 UNIT_W;

	// null & one vectors
	static const Vector4 ZERO;
	static const Vector4 ONE;
};



#include "Vector2.inl"
#include "Vector3.inl"
#include "Vector4.inl"


_ENGINE_END

#endif // VECTOR_H_
