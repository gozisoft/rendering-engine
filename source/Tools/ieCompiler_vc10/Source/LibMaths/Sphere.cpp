#include "mathsafx.h"
#include "Sphere.h"

using namespace Engine;

CSphere::CSphere() : m_radius(0.0f)
{

}

CSphere::CSphere (const Point3f& center, float radius) : 
m_center(center),
m_radius(radius)
{

}

CSphere::CSphere (const CSphere& other) : 
m_center(other.m_center),
m_radius(other.m_radius)
{

}

CSphere& CSphere::operator = (const CSphere& other)
{	
	m_center = other.m_center;
	m_radius = other.m_radius;
	return *this;
}
