#ifndef __LINE2_INL__
#define __LINE2_INL__

CLine2::CLine2() 
{

}

CLine2::CLine2(const CLine2 &line) : 
m_origin(line.m_origin),
m_direction(line.m_direction)
{

}

CLine2::CLine2(const Point2f& origin, const Vector2f& direction) :
m_origin(origin),
m_direction(direction)
{

}

CLine2& CLine2::operator = (const CLine2& line)
{
	m_origin = line.m_origin;
	m_direction = line.m_direction;
	return *this;
}


#endif