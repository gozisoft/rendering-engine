#ifndef __LINE2_H__
#define __LINE2_H__

#include "MathsFwd.h"

_ENGINE_BEGIN

class CLine2
{
public:
	CLine2();
	CLine2(const CLine2& line);
	CLine2(const Point2f& origin, const Vector2f& direction);

	CLine2 &operator = (const CLine2& line);

	Point2f m_origin;
	Vector2f m_direction; // unit length direction vector.
};

class CLine3
{
public:
	CLine3();
	CLine3(const CLine3& line);
	CLine3(const Point3f &origin, const Vector3f &direction);

	CLine3 &operator = (const CLine3& line);

	Point3f m_origin;
	Vector3f m_direction; // unit length direction vector.
};


#include "Line2.inl"
#include "Line3.inl"


_ENGINE_END


#endif