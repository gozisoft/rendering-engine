
#import "gtest/gtest.h"
#import "RendererMetal.h"

using namespace engine;

class RendererMetalTest : public testing::Test {
protected:
protected:
    virtual void TearDown() {
        if (testSubject != nullptr) {
            delete (testSubject);
        }
    }

    virtual void SetUp() {
        testSubject = new RendererMetal();
    }


    RendererMetal *testSubject;
};

TEST_F(RendererMetalTest, CreateBufferNoData) {
    auto buffer = testSubject->createBuffer(0, CResourceAccess::RA_STATIC, 32, nullptr);
    ASSERT_TRUE(buffer != nullptr);
}