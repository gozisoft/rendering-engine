//
// Created by Riad Gozim on 19/02/2017.
//


#import "gtest/gtest.h"
#import "MappingMetal.h"

using namespace engine;

TEST(MapToMetal, BufferUsage) {
    EXPECT_EQ(MTLResourceCPUCacheModeDefaultCache, ApiToMetal::BufferUsage(CResourceAccess::RA_GPU));
    EXPECT_EQ(MTLResourceCPUCacheModeWriteCombined, ApiToMetal::BufferUsage(CResourceAccess::RA_DYNAMIC));
    EXPECT_EQ(MTLResourceStorageModeShared, ApiToMetal::BufferUsage(CResourceAccess::RA_STAGING));
    EXPECT_EQ(MTLResourceStorageModePrivate, ApiToMetal::BufferUsage(CResourceAccess::RA_STATIC));
};
