#pragma once
#ifndef __CRENDER_PROCESS_H__
#define __CRENDER_PROCESS_H__

// Engine headers
#include "IProcess.h"
#include "IRenderer.h"
#include "IView.h"

_ENGINE_BEGIN

// Class represents the processing of a rendering application
class CRenderProcess : public IProcess
{
public:
	// Constructor
	CRenderProcess(IRenderer* renderer, IPipeline* pipeline);

	// Desructor
	~CRenderProcess();

	// called every frame
	void Update(double deltaMS);

private:
	// Rendering device used in this application
	IRenderer* m_device;
	IPipeline* m_pipeline;
	IRenderTarget* m_backTarget;

	// Handle to the swap chain
	size_t m_swapChainHandle;
};

_ENGINE_END

#endif