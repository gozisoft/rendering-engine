#pragma once
#ifndef __CRENDER_VIEW_H__
#define __CRENDER_VIEW_H__

#include "Core.h"

// Framework headers
#include "IEventManager.h"
#include "IProcessManager.h"

_ENGINE_BEGIN

class CRenderVeiw
{
public:
	CRenderVeiw();
	~CRenderVeiw();

	void Intialise();

	//
	// Call back functions
	//
	void OnCreate(const IEventDataPtr& eventData);

	void OnDestroy(const IEventDataPtr& eventData);

	void OnSize(const IEventDataPtr& eventData);




private:
	// The Event handler
	IEventManager* m_eventMgr;

	// Manager class for processes
	IProcessManager* m_processMgr;
};



_ENGINE_END

#endif