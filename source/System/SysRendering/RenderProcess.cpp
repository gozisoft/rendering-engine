#include "RenderProcess.h"

#include "IResource.h"
#include "Vector.h"
#include "SafeDelete.h"


using namespace engine;

CRenderProcess::CRenderProcess(IRenderer* renderer, IPipeline* pipeline) 
	:
m_device(renderer),
m_pipeline(pipeline)
{
	// Obtain the swap chains first back buffer
	ITexture2D* pBackBuffer = nullptr;
	m_device->GetBackBuffer(&pBackBuffer, m_swapChainHandle, 0);
	m_backTarget = pBackBuffer->createRTView();
}

CRenderProcess::~CRenderProcess()
{
	SafeDelete(m_backTarget);
	m_backTarget = nullptr;
}

void CRenderProcess::Update(double deltaMS)
{
	// Clear any state within the pipeline 
	m_pipeline->ClearState();



}