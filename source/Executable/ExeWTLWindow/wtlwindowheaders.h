#pragma once
#ifndef __WTL_TEST_HEADERS_H__
#define __WTL_TEST_HEADERS_H__

#include "wtlwindowfwd.h"

// Window
#include "IWindow.h"
#include "IGameLooper.h"
#include "IDeviceResources.h"

// Events
#include "EngineEvent.h"
#include "SystemEvents.h"


#endif