#pragma once
#ifndef RENDER_VIEW_H
#define RENDER_VIEW_H

// Remaining ATL header
#include <atlbase.h>
#include <atlwin.h>

// #undef NOT_BUILD_WINDOWS_DEPRECATE

// WTL headers
#include "atlapp.h"
#include "atlcrack.h"
#include "atlmisc.h"

// Framework headers
#include "IWindow.h"

// Custom WTL headers
#include "IGameLooper.h"

// Renderer headers
#include "DeviceResourcesD3D11.h"

#include "SystemEvents.h"

_ENGINE_BEGIN

// Class represents the client area of a window
// that renders 3D graphics.
class RenderWindow : public CWindowImpl<RenderWindow, CWindow, CFrameWinTraits>, 
	public std::enable_shared_from_this<RenderWindow>,
	public IGameLooper, 
	public IWindow
{
public:
	// Typedef for the base window class
	typedef CWindowImpl<RenderWindow, CWindow, CFrameWinTraits> my_base;

	// This is a window class, and ::RegisterWindow is called here!!!
	DECLARE_WND_CLASS("RenderWindow")

	// Constructor
	RenderWindow( CGameLoop& gameloop);

	// Non virtual destructor
	~RenderWindow();

	BEGIN_MSG_MAP_EX(my_base)
        MSG_WM_CREATE(OnCreate)
		MSG_WM_CLOSE(OnClose)
        MSG_WM_DESTROY(OnDestroy)
		MSG_WM_SIZE(OnSize)
		MSG_WM_ENTERSIZEMOVE(OnEnterSizeMove)
		MSG_WM_EXITSIZEMOVE(OnExitSizeMove)
		MSG_WM_GETMINMAXINFO(OnGetMinMaxInfo)
		MSG_WM_KEYDOWN(OnKeyDown)
		MSG_WM_KEYUP(OnKeyUp)
		MSG_WM_INPUT(OnInput)
	END_MSG_MAP()

	// Obtain the windows coordinates
	Vector4i getWindowCoords() override;

	// Get the render target created for the back buffer.
	shared_ptr<IDeviceResources> getDeviceResources() override;

protected:

	// Override of IGameLoopers OnUpdate function to update the rendering
	// window in real time.
	void OnUpdate() override;

	// Called after the window has been created, but just before it is shown.
	LRESULT OnCreate(LPCREATESTRUCT lpcs);

	// Called when the user presses to close the window.
	void OnClose();

	// Called when the window is being destroyed
	void OnDestroy();

	// Prevents the client view from going below a certain size
	void OnGetMinMaxInfo(LPMINMAXINFO pMinMaxInfo);

	// Called on a change of window size
	void OnSize(UINT nType, WTL::CSize size);

	// Called when starting to move or resize the window
	void OnEnterSizeMove();

	// Called when finished moving the window
	void OnExitSizeMove();

	// Called when the user presses down on a key.
	void OnKeyDown(TCHAR keycode, UINT repeatCount, UINT);

	// Called when a user releases a key.
	void OnKeyUp(TCHAR keycode, UINT repeatCount, UINT);

	// Called when the user uses an input device (keyboard, mouse, etc.)
	// LRESULT OnAppCommand(HWND hwnd, short nCommand, WORD uDevice, int uKeys);

	// Called when a raw input device makes a move
	void OnInput(WPARAM inputCommand, HRAWINPUT rawInput);

	/*auto notify(const Event<KeyboardEvent>& keyboad) {
		return std::get<Event<KeyboardEvent>::signal_type>(m_signalsTuple)(keyboad);
	}*/

private:
	// Used for displaying the last windows error message.
	void displayLastError(LPTSTR lpszFunction);

	// Called from size events
	bool CheckForWindowSizeChange(const WTL::CSize& size);
	bool CheckForWindowChangingMonitors();

	// Create input devices
	void registerInputDevices();
	void unRegisterInputDevices();

	// Rawinput keyboard handling.
	void handleKeyboardInput(const RAWKEYBOARD& keyboard);
	void handleMouseInput(const RAWMOUSE& mouse);

	// Method for reading last error message from Windows32
	std::string GetLastErrorAsString();

	// Reference to the game looper class. This window will
	// add itself to the game looper duron OnCreate.
	CGameLoop& m_gameloop;

	// Pointer to the application class
	shared_ptr<Application> m_application;

	// Rendering device used in this application.
	shared_ptr<DeviceResourcesD3D11> m_devRes;

	// Window specific stuff
	HACCEL m_hAccel;

	// Previous window size
	WTL::CSize m_prevSize;

	// Window state flags
	bool m_windowed;
	bool m_minimised;
	bool m_maximised;
	bool m_borderDrag;
	bool m_closing;
};


_ENGINE_END

#endif


// Handler prototypes (uncomment arguments if needed):
//	LRESULT MessageHandler(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
//	LRESULT CommandHandler(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
//	LRESULT NotifyHandler(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/)