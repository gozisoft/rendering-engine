#pragma once
#ifndef RAW_INPUT_H
#define RAW_INPUT_H


namespace RawInput
{
	//http://www.microsoft.com/whdc/archive/HID_HWID.mspx
	enum usage_page {
		HID_USAGE_PAGE = 0x01
	};

	// http://www.usb.org/developers/devclass_docs/Hut1_12v2.pdf
	enum usage_id {
		HID_DEVICE_SYSTEM_POINTER = 0x01,
		HID_DEVICE_SYSTEM_MOUSE = 0x02,
		HID_DEVICE_SYSTEM_JOYSTICK = 0x04,
		HID_DEVICE_SYSTEM_GAMEPAD = 0x05,
		HID_DEVICE_SYSTEM_KEYBOARD = 0x06,
		HID_DEVICE_SYSTEM_KEYPAD = 0x07,
		HID_DEVICE_SYSTEM_CONTROL = 0x08,
	};
}


#endif