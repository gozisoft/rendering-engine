#pragma once
#ifndef IRENDERABLE_H
#define IRENDERABLE_H

#include "Core.h"
#include "IRenderer.h"

_ENGINE_BEGIN

class IRenderable
{
public:
	virtual ~IRenderable() {}

	virtual void onDraw(IPipelinePtr pipeline) = 0;
};


_ENGINE_END

#endif