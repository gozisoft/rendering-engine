#pragma once

#ifndef __RESOURCES_H__
#define __RESOURCES_H__

#include "Core.h"
#include "StringLruCache.h"
#include <mutex>

_ENGINE_BEGIN

class file_not_found_error : public std::runtime_error
{
public:
	explicit file_not_found_error(const std::string& _Message)
		: runtime_error(_Message)
	{
	}

	explicit file_not_found_error(const char* _Message)
		: runtime_error(_Message)
	{
	}
};


class Resources
{
public:
	// Singleton access function.
	static std::shared_ptr<Resources>& get_instance()
	{
		static std::shared_ptr<Resources> instance = nullptr;
		if (!instance)
		{
			std::lock_guard<std::mutex> lock(_mutex);
			if (!instance)
			{
				instance.reset(new Resources());
			}
		}
		return instance;
	}

	std::string getShader(const std::string& filename);

private:
	// Singleton instance members.
	static std::mutex _mutex;

	// The max size of the shader cache.
	const static size_t SHADER_CACHE_SIZE = 64000000; // 64mb

													  // Constructors.
	Resources() : m_shaderCache(SHADER_CACHE_SIZE) {}
	Resources(const Resources& rs);
	Resources& operator = (const Resources& rs);

	// Loads a shader file from disk drive.
	std::string load_shader_file(std::string const& filename);

	// Last recently used cache for shader files.
	StringLruCache m_shaderCache;
};

_ENGINE_END

#endif