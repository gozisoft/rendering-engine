#include "wtlwindowafx.h"
#include "TriangleTest.h"
#include "Vector.h"

using namespace engine;

//-------------------------------------------------------------------------------------- 
// Structures 
//-------------------------------------------------------------------------------------- 
struct SimpleVertex
{
	Vector3f Pos;
};

// Create vertex buffer 
SimpleVertex vertices[] =
{
	Vector3f(0.0f, 0.5f, 0.5f),
	Vector3f(0.5f, -0.5f, 0.5f),
	Vector3f(-0.5f, -0.5f, 0.5f),
};

CTriangleTest::CTriangleTest(IWindowPtr window, IRendererPtr renderer) :
m_window(window),
m_device(renderer)
{
	// Obtain a pointer to the main rendering pipeline!
	m_pipeline = IPipelinePtr(m_device->GetPipeline());
}

CTriangleTest::~CTriangleTest()
{
}

void CTriangleTest::OnDestroy()
{
	m_pipeline.reset();
	m_device.reset();
}

void CTriangleTest::OnCreate()
{
	//
	// Model stuffs.
	//

	// Create the shader for drawing a triangle

	m_testShader = std::make_shared<CTriangleShader>(m_device);

	// Create vertex buffers

	m_vertexBuffer = m_device->createBuffer(CResourceUse::RU_VERTEX, CResourceAccess::RA_GPU,
		sizeof(SimpleVertex) * 3, (uint8_t*)&vertices[0]);
}


void CTriangleTest::OnResume()
{
	// Create a render target for the view port
	ITexture2DPtr back_buffer = m_window->getBackBuffer();
	m_renderTarget = IRenderTargetPtr(back_buffer->createRTView());

	// Get window coordinates and create a viewport on resume
	const Vector4i& rect = m_window->getWindowCoords();
	CViewPort viewport(rect.data(), 0.0f, 1.0f);

	// Set the pipeline ViwePort.
	m_pipeline->SetViewports(&viewport, 1);
}

void CTriangleTest::OnPause()
{
	// Clear any state within the pipeline before destroying
	// resources
	m_pipeline->ClearState();

	// Reset the depth stencil
	m_depthStencil.reset();

	// Reset the depth stencil resource
	m_renderTarget.reset();
}

void CTriangleTest::OnUpdate()
{
	// Set the render target.

	const IRenderTarget* renderTarget = m_renderTarget.get();
	m_pipeline->setRenderTargets(&renderTarget, 1, nullptr);

	// Clear the render target to a black background.

	float color[4] = { 0.098039225f, 0.098039225f, 0.439215720f, 1.000000000f };
	m_pipeline->Clear(m_renderTarget, color);

	// Set the primitive topology.

	m_pipeline->setInputDrawingMode();

	// Set vertex buffers

	size_t stride = sizeof(SimpleVertex);
	size_t offset = 0;
	const IBuffer* vertexBuffer = m_vertexBuffer.get();
	m_pipeline->enableVertexBuffers(&vertexBuffer, 1, 0, &stride, &offset);

	//
	// Set the input layout.
	//
	m_pipeline->enableInputLayout(m_testShader->shaderInput().get());

	//
	// Vertex shader stage.
	//
	m_pipeline->enableShader(m_testShader->vertexShader().get());

	//
	// Pixel shader stage.
	//
	m_pipeline->enableShader(m_testShader->pixelShader().get());

	// Call the basic draw method.
	m_pipeline->draw(3, 0);
}
