#include "wtlwindowafx.h"
#include "Resources.h"
#include "StringFunctions.h"
#include <regex>
#include <fstream>
#include <vector>

using namespace engine;

std::mutex Resources::_mutex{};

std::string Resources::getShader(std::string const& filename)
{
	auto iter = m_shaderCache.get(filename);
	if (iter != m_shaderCache.end())
	{
		// We found an entry in our cache, return it.
		return (*iter).second;
	}

	auto fileText = load_shader_file(filename);
	if (!fileText.empty())
	{
		// We have a text file here :)
		return fileText;
	}

	throw new file_not_found_error("Could not find file: " + filename);
}

// functions used to load an image or save an image. This is an example of preferring non-member non-friend
// functions to member functions in order to increase encapsulation, packaging flexibility and functional
// extensibility. (Effective C++ 3rd ed. p102)
std::string Resources::load_shader_file(const std::string& fileName)
{
#if defined (UNICODE) || defined(_UNICODE)
	typedef std::basic_regex<wchar_t> _Regex;
	typedef std::match_results<const wchar_t *> _CMatch;
	typedef std::match_results<std::wstring::const_iterator> _SMatch;
#else
	typedef std::basic_regex<char> _Regex;
	typedef std::match_results<const char *> _CMatch;
	typedef std::match_results<std::string::const_iterator> _SMatch;
#endif

	typedef _Regex Regex;
	typedef _CMatch CMatch;
	typedef _SMatch SMatch;

	// Search for file extension
	Regex reg(ieS("/^.*\\.(hlsl|glsl)$/i"));

	SMatch match;
	if (std::regex_search(fileName, match, reg) == false)
	{
		assert(false && ieS("Cannot find the correct file extension"));
		return std::string();
	}

	// try to open file using raw filename.
	std::ifstream fileIn(fileName, std::ifstream::binary);
	if (!fileIn.good()) {
		// Otherwise get absolute path.
		std::string pathName = StringUtils::GetFullPath(fileName.c_str());
		if (!pathName.empty())
			fileIn.open(pathName.c_str(), std::ifstream::binary);
	}

	if (fileIn.good())
	{
		// Read the contents of the text file.
		std::string contents((std::istreambuf_iterator<char>(fileIn)),
			std::istreambuf_iterator<char>());

		if (fileIn.is_open())
			fileIn.close(); // close file after loading

		return contents;
	}
	else
	{
		assert(!"Could not open File");
		return std::string();
	}
}