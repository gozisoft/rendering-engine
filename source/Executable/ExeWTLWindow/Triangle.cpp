#include "wtlwindowafx.h"
#include "Triangle.h"
#include "ColourEffect.h"
#include "Vector.h"

using namespace engine;

struct SimpleVertex {
	Vector3f Pos;
	Vector3f color; // color
};

// Create vertex buffer 
SimpleVertex vertices[] =
{
	{ Vector3f(-0.5f, -0.5f, 0.5f), Vector3f(1, 0, 0) },
	{ Vector3f(0.0f, 0.5f, 0.5f),   Vector3f(1, 0, 0) },
	{ Vector3f(0.5f, -0.5f, 0.5f),  Vector3f(1, 0, 0) },
};

uint32_t triangleIndices[] =
{
	0, 1, 2,
};

shared_ptr<Triforce> Triforce::create(shared_ptr<IRenderer> renderer) {

	// Create the shader for drawing a triangle
	auto shader = make_shared<ColourEffect>(renderer);

	// Build requirements for input format.
	auto format = make_shared<CFVFormat>();
	format->AddElement(3, CDataType::DT_FLOAT, CVertexSemantic::VS_POSITION);
	format->AddElement(4, CDataType::DT_FLOAT, CVertexSemantic::VS_COLOUR);

	// Convert SimpleCubeVertex to uint8_t (bytes).
	std::vector<SimpleVertex> vertexData(vertices, vertices + sizeof_array(vertices));
	auto vertexBuffer = VertexBuffer::create(format, renderer, CResourceAccess::RA_GPU, vertexData);

	// Create index buffer
	std::vector<uint32_t> indexData(&triangleIndices[0], &triangleIndices[0] + sizeof_array(triangleIndices));
	auto ibuffer = IndexBuffer::create(renderer, CResourceAccess::RA_GPU, indexData);

	// Return our triangle.
	return make_shared<Triforce>(vertexBuffer, ibuffer, shader);
}

Triforce::Triforce(shared_ptr<VertexBuffer> const& vbuffer, shared_ptr<IndexBuffer> const& ibuffer,
	shared_ptr<ColourEffect> const& effect)
	: TriMesh(vbuffer, ibuffer, effect), m_colourEffect(effect) {
}

void Triforce::onDraw(IPipelinePtr pipeline, ICamera const& camera) {

	// Build constnt data object
	ColourEffect::ConstantBuffer constants;
	constants.model = worldTransform().matrix();
	constants.view = camera.viewMatrix();
	constants.projection = camera.projectionMatrix();

	// Update the constant buffers used for rendering.
	m_colourEffect->updateConstants(constants);

	// Set the primitive topology.
	pipeline->setInputDrawingMode();

	// Enable vertex buffer.
	m_vbuffer->pipelineEnable(pipeline);

	// Enable vertex buffer.
	m_ibuffer->pipelineEnable(pipeline);

	// Apply effect
	m_colourEffect->apply(pipeline);

	// Draw the vertices.
	pipeline->drawIndexd(m_ibuffer->index_count(), 0);
}