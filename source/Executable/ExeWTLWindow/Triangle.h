#pragma once
#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "TriangleMesh.h"

_ENGINE_BEGIN

class Triforce : public TriMesh
{
public:
	// Factory method for creating the crate.
	static shared_ptr<Triforce> create(shared_ptr<IRenderer> renderer);

	// Simple constructor
	Triforce(shared_ptr<VertexBuffer> const& vbuffer,
		shared_ptr<IndexBuffer> const& ibuffer,
		shared_ptr<ColourEffect> const& effect);

	void onDraw(IPipelinePtr pipeline, ICamera const& camera) override;

private:
	shared_ptr<IInputFormat> m_inputFormat;
	shared_ptr<ColourEffect> m_colourEffect;
};

_ENGINE_END

#endif