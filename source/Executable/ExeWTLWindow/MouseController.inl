#ifndef CMOUSE_CONTROLLER_INL
#define CMOUSE_CONTROLLER_INL

inline void CMouseController::SetRelativePos(const Point2f& pos)
{
	SetMousePos(Point2i(
		(int)(pos[0] * m_winSize[0]),
		(int) (pos[1] * m_winSize[1])
		));
}

inline const Point2i& CMouseController::GetMousePos()
{
	UpdateCursorPos();
	return m_cursorPos;
}

inline Point2f CMouseController::GetRelativeMousePos()
{
	UpdateCursorPos();
	return Point2f(m_invClientSize[0] * m_cursorPos[0], m_invClientSize[1] * m_cursorPos[1]);
}

inline bool CMouseController::IsVisible() const
{
	return m_isVisible;
}

inline Point2i CMouseController::GetClientSize() const
{
	return m_winSize;
}

inline void CMouseController::SetHWND(HWND hwnd)
{
	m_hwnd = hwnd;
}

#endif