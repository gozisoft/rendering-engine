#pragma once

#include "Core.h"
#include "EngineEvent.h"
#include "Keycodes.h"

_ENGINE_BEGIN

class SizeEvent : public Event<SizeEvent> {
public:
	static const std::string SIZE_EVENT;

	SizeEvent(uint_t width, uint_t height) :
		Event(SIZE_EVENT), m_width(width), m_height(height) {
	}

	uint_t width() const {
		return m_width;
	}

	uint_t height() const {
		return m_height;
	}

private:
	uint_t m_width;
	uint_t m_height;
};


// Modeled from 
// http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/flash/events/KeyboardEvent.html
class KeyboardEvent : public Event<KeyboardEvent> {
public:
	/** Event type for a key that was released. */
	static const std::string KEY_UP;

	/** Event type for a key that was pressed. */
	static const std::string KEY_DOWN;

	/** Location on keybaord of key press. */
	enum class KeyLocation {
		LEFT,
		RIGHT,
		STANDARD,
		NUM_PAD
	};

	KeyboardEvent(const std::string& type, KeyCode keycode, KeyLocation key_location, uint_t charcode,
		bool alt_key = false, bool ctrl_key = false, bool shift_key = false)
		: Event(type),
		m_keycode(keycode),
		m_keyLocation(key_location),
		m_charcode(charcode),
		m_altKey(alt_key),
		m_ctrlKey(ctrl_key),
		m_shiftKey(shift_key),
		m_isDefaultPrevented(false) {
	}

	/** The key code of the key. */
	const KeyCode& keyCode() const { return m_keycode; }

	/** Indicates the location of the key on the keyboard. This is useful for differentiating
	*  keys that appear more than once on a keyboard. @see Keylocation */
	const KeyLocation& keyLocation() const { return m_keyLocation; }

	/** Contains the character code of the key. */
	uint_t charCode() { return m_charcode; }

	/** Indicates whether the Alt key is active on Windows or Linux;
	*  indicates whether the Option key is active on Mac OS. */
	uint_t altKey() { return m_altKey; }

	/** Indicates whether the Ctrl key is active on Windows or Linux;
	*  indicates whether either the Ctrl or the Command key is active on Mac OS. */
	uint_t ctrlKey() { return m_ctrlKey; }

	/** Indicates whether the Shift key modifier is active (true) or inactive (false). */
	uint_t shiftKey() { return m_shiftKey; }

private:
	KeyCode m_keycode;
	KeyLocation m_keyLocation;
	uint_t m_charcode;
	bool m_altKey;
	bool m_ctrlKey;
	bool m_shiftKey;;
	bool m_isDefaultPrevented;
};

class MouseEvent : public Event<MouseEvent>{
public:
	static const std::string MOUSE_EVENT;

	MouseEvent() : Event(std::string(MOUSE_EVENT)) {
	}
};


_ENGINE_END