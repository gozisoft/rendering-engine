#include "wtlwindowafx.h"
#include "GameView.h"
#include "PlayerView.h"
#include "Crate.h"

using namespace engine;

GameView::GameView(const shared_ptr<IDeviceResources>& device) :
	VisualGroup(), m_devRes(device)
{

}

void GameView::resize(const shared_ptr<IWindow>& window)
{
	// Split the window size across all the viewports.
	// Get window coordinates and create a viewport on resume
	auto rect = window->getWindowCoords();
	auto numplayers = m_players.size();
	for (size_t i = 0; i < rect.array_size; ++i)
	{
		rect[i] /= numplayers;
	}
	
	// Update view ports of each player.
	for (auto player : m_players)
	{
		player->resize(rect);
	}
}


void GameView::add(const shared_ptr<PlayerView>& player)
{
	if (player == nullptr)
	{
		throw std::runtime_error("Invalid player view.");
	}

	// Add the player view to the visual nodes list.
	VisualGroup::add(player);

	// Add the player view specifically to this game view.
	m_players.push_back(player);
}

void GameView::draw()
{
	float color[4] = {};

	// Get the device resources associated with the window.
	auto renderer = m_devRes->device();
	auto pipeline = m_devRes->pipeline();
	auto renderTarget = m_devRes->renderTarget();
	auto depthStencil = m_devRes->depthStencil();

	// Set the render target for the backbuffer.
	//------------------------------------------------------
	const IRenderTarget* target = renderTarget.get();
	// pipeline->setRenderTargets(&target, 1, depthStencil.get());
	pipeline->setRenderTargets(&target, 1, nullptr);

	// Clear the back buffer.
	//------------------------------------------------------
	pipeline->Clear(renderTarget, color);

	// Clear the depth buffer.
	//------------------------------------------------------
	pipeline->Clear(depthStencil.get(), 0, 1.0f, 0);

	// For each player view determine what is visible 
	// and then trigger the rendering of those items.
	auto& childList = getChildList();

	// ToDo: For each player view, calculate the number of visible items,
	// and perform a draw operation for each visible child.
	for (auto player : m_players)
	{
		// Compute what is visible to this player view
		// --------------------------------------------
		// RootNode:: getParent();
		pipeline->SetViewports(&player->viewport(), 1);

		// Iterate over each visible item and trigger it to
		// draw.
		for (auto child : childList)
		{
			const auto& camera = player->getCamera();

			if (child != player) {
				child->onDraw(pipeline, camera);
			}
			else {
				player->onDraw(pipeline, camera);
			}
		}
	}
}