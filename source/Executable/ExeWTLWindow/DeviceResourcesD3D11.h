#pragma once

#ifndef DEVICE_RESOURCES_H
#define DEVICE_RESOURCES_H

#include "IDeviceResources.h"
#include "D3D11Fwd.h"
#include "RendererD3D11.h"
#include "PipelineD3D11.h"

_ENGINE_BEGIN

// https://msdn.microsoft.com/en-us/library/windows/desktop/dn643742(v=vs.85).aspx
class DeviceResourcesD3D11 : public IDeviceResources {
public:
	// Default constructor to attach window to rendering device.
	DeviceResourcesD3D11(shared_ptr<IWindow> const& window,
		 shared_ptr<IRenderer> const& renderer);

	//************************************
	// Method:    resize
	// FullName:  engine::DeviceResources::resize
	// Access:    public 
	// Returns:   void
	// Qualifier:
	// Parameter: long width
	// Parameter: long height
	//************************************
	void resize(long width, long height) override;

	//************************************
	// Method:    goFullScreen
	// FullName:  engine::DeviceResources::goFullScreen
	// Access:    public 
	// Returns:   void
	// Qualifier:
	//************************************
	void goFullScreen() override;

	//************************************
	// Method:    goWindowed
	// FullName:  engine::DeviceResources::goWindowed
	// Access:    public 
	// Returns:   void
	// Qualifier:
	//************************************
	void goWindowed() override;

	//************************************
	// Method:    releaseBackBuffer
	// FullName:  engine::DeviceResources::releaseBackBuffer
	// Access:    public 
	// Returns:   void
	// Qualifier:
	//************************************
	void releaseBackBuffer() override;

	//************************************
	// Method:    configureBackBuffer
	// FullName:  engine::DeviceResources::configureBackBuffer
	// Access:    public 
	// Returns:   void
	// Qualifier:
	//************************************
	void configureBackBuffer() override;

	IDXGISwapChainPtr swap_chain() const {
		return m_swapChain;
	}

	shared_ptr<IRenderer> device() const override {
		return m_device;
	}

	shared_ptr<IPipeline> pipeline() const override {
		return m_pipeline;
	}

	shared_ptr<IRenderTarget> renderTarget() const override {
		return m_renderTarget;
	}

	shared_ptr<IDepthStencil> depthStencil() const override {
		return m_depthStencilView;
	}

	shared_ptr<RendererD3D11> const& _device() const {
		return m_device;
	}

	shared_ptr<PipelineD3D11> const& _pipeline() const {
		return m_pipeline;
	}

	shared_ptr<CRenderTargetD3D11> const& _renderTarget() const {
		return m_renderTarget;
	}

	shared_ptr<CTexture2DD3D11> const& _depthStencil() const {
		return m_depthStencil;
	}

private:
	// Swap chain stuff
	IDXGISwapChainPtr m_swapChain;

	//-----------------------------------------------------------------------------
	// Direct3D device
	//-----------------------------------------------------------------------------
	shared_ptr<RendererD3D11> m_device;
	shared_ptr<PipelineD3D11> m_pipeline;

	//-----------------------------------------------------------------------------
	// Rasterization state.
	//-----------------------------------------------------------------------------
	shared_ptr<RasterizerStateD3D11> m_rasteState;

	//-----------------------------------------------------------------------------
	// DXGI swap chain device resources
	//-----------------------------------------------------------------------------
	shared_ptr<CTexture2DD3D11> m_backBuffer;
	shared_ptr<CRenderTargetD3D11> m_renderTarget;

	//-----------------------------------------------------------------------------
	// Direct3D device resources for the depth stencil
	//-----------------------------------------------------------------------------
	shared_ptr<CTexture2DD3D11> m_depthStencil;
	shared_ptr<CDepthStencilD3D11> m_depthStencilView;

	// Constant for setting the number of SwapChain back
	// buffers.
	const size_t NUMBACKBUFFERS = 2U;
};

_ENGINE_END


#endif
