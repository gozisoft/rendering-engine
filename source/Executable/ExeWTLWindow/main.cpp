#include "wtlwindowafx.h"
#include "RenderView.h"
#include "GameLoop.h"

using namespace engine;

CAppModule module;

int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	// Create the game loop manager.
	CGameLoop gameloop;

	// Create the rendering device here as it allows us to have
	// one rendering device for the entire application, but at the 
	// same time allows for multiple rendering windows and thus
	// swap chains.
	// auto renderer = std::make_shared<RendererD3D11>();

	// Create out rendering window and pass in a pointer to the gamelooper.
	// Once the windows is created it will add itself to the game loopers list 
	// of IGameLooper's.
	auto renderView = std::make_shared<RenderWindow>(gameloop);
	HWND handle = renderView->Create(nullptr, CWindow::rcDefault, "Rendering Engine",
		0, 0, 0U, nullptr);
	if (handle == nullptr)
	{
		ATLTRACE(_T("Main window creation failed!\n"));
		return 0;
	}
	
	// Set the window parameters.
    renderView->ShowWindow(nCmdShow);
	renderView->UpdateWindow();

	// Run the message loop of the window.
	int nRet = gameloop.Run();

	// Destroy the window now.
	renderView->DestroyWindow();

	// Loops ended return value
	return nRet;
}