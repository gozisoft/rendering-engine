#pragma once
#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include "Resources.h"
#include "GameView.h"

_ENGINE_BEGIN

class Application
{
public:
	/// Constructor 
	Application(const shared_ptr<IWindow>& window);

	/// Destructor
	~Application();

	/// <summary> 
	/// Called during the creation stage of the application class.
	/// </summary>
	void OnCreate();
	void OnResume();
	void OnPause();
	void OnUpdate();
	void OnDestroy();

private:
	// System events sent to application.
	void onSizeEvent(const SizeEvent& sizeEvent);

	// An instance of the window
	shared_ptr<IWindow> m_window;

	// Rendering device used in this application
	shared_ptr<IDeviceResources> m_devRes;

	// Root scene node, for storing holding
	// all objects attached to the scene.
	shared_ptr<GameView> m_gameView;

	// Global resources for application.
	shared_ptr<Resources> m_resources;

	// Simple timer tied to application lifecycle.
	Timer m_timer;
};

#include "Application.inl"

_ENGINE_END

#endif