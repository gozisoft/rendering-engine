#pragma once
#ifndef __CTRIANGLE_SHADER_H__
#define __CTRIANGLE_SHADER_H__

#include "Core.h"
#include "librendererfwd.h"
#include "Effect.h"

_ENGINE_BEGIN

class TriangleShader : public Effect
{
public:
	// Constructor
	TriangleShader(IRendererPtr renderer);

	void apply(std::shared_ptr<IPipeline> const& pipeline) override;

	// Accessors
	IShaderPtr vertexShader() const { return m_vertexShader; }
	IShaderPtr pixelShader() const { return m_pixelShader; }
	IInputFormatPtr shaderInput() const { return m_shaderInput; }

private:
	IShaderPtr m_vertexShader;
	IShaderPtr m_pixelShader;
	IInputFormatPtr m_shaderInput;
};


_ENGINE_END

#endif