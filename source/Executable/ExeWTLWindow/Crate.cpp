#include "wtlwindowafx.h"
#include "Crate.h"
#include "ColourEffect.h"

using namespace engine;

shared_ptr<ColorCrate> ColorCrate::createCrate(shared_ptr<IRenderer> renderer) {
	//
	// Geometry stuff.
	//
	struct ColorVertex {
		Vector3f pos; // position
		Vector4f color; // color
	};

	// Create cube geometry.
	ColorVertex cubeVertices[] = {
		// front
		{ { -0.5f, 0.5f, -0.5f },	 { 0.0f, 0.0f, 1.0f, 1.0f } },
		{ {  0.5f, 0.5f, -0.5f },	 { 0.0f, 1.0f, 0.0f, 1.0f } },
		{ {  0.5f, 0.5f,  0.5f },	 { 0.0f, 1.0f, 1.0f, 1.0f } },
		{ { -0.5f, 0.5f,  0.5f },	 { 1.0f, 0.0f, 0.0f, 1.0f } },

		// back
		{ { -0.5f, -0.5f,  0.5f }, { 1.0f, 0.0f, 1.0f, 1.0f } },
		{ {  0.5f, -0.5f,  0.5f }, { 1.0f, 1.0f, 0.0f, 1.0f } },
		{ {  0.5f, -0.5f, -0.5f }, { 1.0f, 1.0f, 1.0f, 1.0f } },
		{ { -0.5f, -0.5f, -0.5f }, { 0.0f, 0.0f, 0.0f, 1.0f } }
	};

	// Convert SimpleCubeVertex to uint8_t (bytes).
	std::vector<ColorVertex> vertexData(&cubeVertices[0], &cubeVertices[0] + sizeof_array(cubeVertices));

	// Build requirements for input format.
	auto format = std::make_shared<CFVFormat>();
	format->AddElement(3, CDataType::DT_FLOAT, CVertexSemantic::VS_POSITION);
	format->AddElement(4, CDataType::DT_FLOAT, CVertexSemantic::VS_COLOUR);

	// Create vertex buffer
	auto vbuffer = VertexBuffer::create(format, renderer, CResourceAccess::RA_GPU, vertexData);

	uint32_t cubeIndices[] = {
		/*// front
		0, 1, 2,
		2, 3, 0,
		// top
		3, 2, 6,
		6, 7, 3,
		// back
		7, 6, 5,
		5, 4, 7,
		// bottom
		4, 5, 1,
		1, 0, 4,
		// left
		4, 0, 3,
		3, 7, 4,
		// right
		1, 5, 6,
		6, 2, 1,*/

		0, 1, 2,
        0, 2, 3,

        4, 5, 6,
        4, 6, 7,

        3, 2, 5,
        3, 5, 4,

        2, 1, 6,
        2, 6, 5,

        1, 7, 6,
        1, 0, 7,

        0, 3, 4,
        0, 4, 7
	};

	// Create index buffer
	std::vector<uint32_t> indexData(&cubeIndices[0], &cubeIndices[0] + sizeof_array(cubeIndices));
	auto ibuffer = IndexBuffer::create(renderer, CResourceAccess::RA_GPU, indexData);

	//
	// Shader stuff.
	//
	auto effect = std::make_shared<ColourEffect>(renderer);

	//
	// Create the crate.
	//
	auto crate_oject = std::make_shared<ColorCrate>(vbuffer, ibuffer, effect);

	// Return our wonderful crate.
	return crate_oject;
}

ColorCrate::ColorCrate(shared_ptr<VertexBuffer> const& vbuffer,
	shared_ptr<IndexBuffer> const& ibuffer,
	shared_ptr<ColourEffect> const& effect)
	: TriMesh(vbuffer, ibuffer, effect), m_colourEffect(effect) {
}

void ColorCrate::onDraw(shared_ptr<IPipeline> pipeline, const ICamera& camera) {
	// Set the primitive topology.
	pipeline->setInputDrawingMode();

	// Enable vertex buffer.
	m_vbuffer->pipelineEnable(pipeline);

	// Enable index buffer.
	m_ibuffer->pipelineEnable(pipeline);

	// Build constnt data object
	ColourEffect::ConstantBuffer constants;
	constants.model = m_worldTransform.matrix();
	constants.view = camera.viewMatrix();
	constants.projection = camera.projectionMatrix();

	// Update the constant buffers used for rendering.
	m_colourEffect->updateConstantsImmediate(pipeline, constants);

	// Apply effect
	m_colourEffect->apply(pipeline);

	// Draw the vertices.
	pipeline->drawIndexd(m_ibuffer->index_count(), 0);

	// pipeline->draw(m_vbuffer->vertex_count(), 0);
}


/*SimpleCubeVertex cubeVertices[] =
{
{ Vector3f(-0.5f, 0.5f, -0.5f), Vector3f(0.0f, 1.0f, 0.0f) }, // +Y (top face)
{ Vector3f(0.5f, 0.5f, -0.5f),  Vector3f(1.0f, 1.0f, 0.0f) },
{ Vector3f(0.5f, 0.5f, 0.5f),   Vector3f(1.0f, 1.0f, 1.0f) },
{ Vector3f(-0.5f, 0.5f, 0.5f),  Vector3f(0.0f, 1.0f, 1.0f) },

{ Vector3f(-0.5f, -0.5f, 0.5f),  Vector3f(0.0f, 0.0f, 1.0f) }, // -Y (bottom face)
{ Vector3f(0.5f, -0.5f, 0.5f),   Vector3f(1.0f, 0.0f, 1.0f) },
{ Vector3f(0.5f, -0.5f, -0.5f),  Vector3f(1.0f, 0.0f, 0.0f) },
{ Vector3f(-0.5f, -0.5f, -0.5f), Vector3f(0.0f, 0.0f, 0.0f) },
};*/

