#include "wtlwindowafx.h"
#include "ApplicationTest.h"
#include "ColourEffect.h"

#include "Vector.h"
#include "Matrix.h"

using namespace engine;


struct ColorVertex
{
	Vector3f pos;   // position
	Vector4f color; // color
};

/*ColorVertex cubeVertices[] =
{
	{ Vector3f(-1.0f, 1.0f, -1.0f), Vector4f(0.0f, 0.0f, 1.0f, 1.0f) },
	{ Vector3f(1.0f, 1.0f, -1.0f), Vector4f(0.0f, 1.0f, 0.0f, 1.0f) },
	{ Vector3f(1.0f, 1.0f, 1.0f), Vector4f(0.0f, 1.0f, 1.0f, 1.0f) },
	{ Vector3f(-1.0f, 1.0f, 1.0f), Vector4f(1.0f, 0.0f, 0.0f, 1.0f) },
	{ Vector3f(-1.0f, -1.0f, -1.0f), Vector4f(1.0f, 0.0f, 1.0f, 1.0f) },
	{ Vector3f(1.0f, -1.0f, -1.0f), Vector4f(1.0f, 1.0f, 0.0f, 1.0f) },
	{ Vector3f(1.0f, -1.0f, 1.0f), Vector4f(1.0f, 1.0f, 1.0f, 1.0f) },
	{ Vector3f(-1.0f, -1.0f, 1.0f), Vector4f(0.0f, 0.0f, 0.0f, 1.0f) },
};*/

/*uint32_t cubeIndices[] =
{
	3, 1, 0,
	2, 1, 3,

	0, 5, 4,
	1, 5, 0,

	3, 4, 7,
	0, 4, 3,

	1, 6, 5,
	2, 6, 1,

	2, 7, 6,
	3, 7, 2,

	6, 4, 5,
	7, 4, 6,
};*/

// Create cube geometry.
ColorVertex cubeVertices[] = {
	// front
	{ Vector3f(-1.0f, -1.0f, 1.0f),	 Vector4f(0.0f, 0.0f, 1.0f, 1.0f) },
	{ Vector3f(1.0f, -1.0f, 1.0f),	 Vector4f(0.0f, 1.0f, 0.0f, 1.0f) },
	{ Vector3f(1.0f,  1.0f, 1.0f),	 Vector4f(0.0f, 1.0f, 1.0f, 1.0f) },
	{ Vector3f(1.0f,  1.0f, 1.0f),	 Vector4f(1.0f, 0.0f, 0.0f, 1.0f) },

	// back
	{ Vector3f(-1.0f, -1.0f, -1.0f), Vector4f(1.0f, 0.0f, 1.0f, 1.0f) },
	{ Vector3f(1.0f, -1.0f, -1.0f),	 Vector4f(1.0f, 1.0f, 0.0f, 1.0f) },
	{ Vector3f(1.0f,  1.0f, -1.0f),	 Vector4f(1.0f, 1.0f, 1.0f, 1.0f) },
	{ Vector3f(-1.0f,  1.0f, -1.0f), Vector4f(0.0f, 0.0f, 0.0f, 1.0f) }
};



uint32_t cubeIndices[] = {
	// front
	0, 1, 2,
	2, 3, 0,
	// top
	3, 2, 6,
	6, 7, 3,
	// back
	7, 6, 5,
	5, 4, 7,
	// bottom
	4, 5, 1,
	1, 0, 4,
	// left
	4, 0, 3,
	3, 7, 4,
	// right
	1, 5, 6,
	6, 2, 1,
};

ColorVertex triangle_vertices[] =
{
	{ Vector3f(-1.0f, -1.0f, 1.0f), Vector4f(0.0f, 0.0f, 1.0f, 1.0f) },
	{ Vector3f(1.0f, -1.0f, 1.0f), Vector4f(0.0f, 1.0f, 0.0f, 1.0f) },
	{ Vector3f(0.0f,  1.0f, 1.0f), Vector4f(0.0f, 1.0f, 1.0f, 1.0f) },
};

uint32_t triangle_indices[] =
{
	0, 1, 2
};

ApplicationTest::ApplicationTest(shared_ptr<IWindow> window) :
m_window(window),
m_devRes(window->getDeviceResources())
{
}

ApplicationTest::~ApplicationTest()
{
}

void ApplicationTest::OnDestroy()
{
}

void ApplicationTest::OnCreate()
{
	auto renderer = m_devRes->device();

	m_vertexBuffer = renderer->createBuffer(CResourceUse::RU_VERTEX, CResourceAccess::RA_GPU,
		sizeof(ColorVertex) * sizeof_array(cubeVertices), (uint8_t*)&cubeVertices[0]);

	m_indexBuffer = renderer->createBuffer(CResourceUse::RU_INDEX, CResourceAccess::RA_GPU,
		sizeof(uint32_t) * sizeof_array(cubeIndices), (uint8_t*)&cubeIndices[0]);

	m_testShader = std::make_shared<ColourEffect>(renderer);
}

void ApplicationTest::OnResume()
{
	auto rect = m_window->getWindowCoords();
	ViewPort viewport(rect.data(), 0.0f, 1.0f);

	// Set the pipeline ViwePort.
	auto pipeline = m_devRes->pipeline();
	pipeline->SetViewports(&viewport, 1);

	// Set the model matrix to identity
	m_constantBufferData.model.identity();

	// Update the camera frustum with the new viewport.
	m_camera.SetFrustum(70.0f,                                // use a 70-degree vertical field of view
		viewport.aspect_ratio(),							  // specify the aspect ratio of the window
		0.01f,                                                // specify the nearest Z-distance at which to draw vertices
		100.0f);

	m_camera.SetPosition({ 0, 0, -5 });

	// Set the view matrix from the camera
	m_constantBufferData.view = m_camera.viewMatrix();
	m_constantBufferData.projection = m_camera.projectionMatrix();

	// Initialize the view matrix
	/*Vector3f eye = Vector3f(0.0f, 1.0f, -5.0f);
	Vector3f at = Vector3f(0.0f, 1.0f, 0.0f);
	Vector3f up = Vector3f(0.0f, 1.0f, 0.0f);
	cml::matrix_look_at_RH(m_constantBufferData.view, eye, at, up);*/

	// Finally, update the constant buffer perspective projection parameters
	// to account for the size of the application window.  In this sample,
	// the parameters are fixed to a 70-degree field of view, with a depth
	// range of 0.01 to 100.  See Lesson 5 for a generalized camera class.

	/*float xScale = 1.42814801f;
	float yScale = 1.42814801f;
	if (viewport.width() > viewport.height())
	{
		xScale = yScale *
			static_cast<float>(viewport.height()) /
			static_cast<float>(viewport.width());
	}
	else
	{
		yScale = xScale *
			static_cast<float>(viewport.width()) /
			static_cast<float>(viewport.height());
	}

	m_constantBufferData.projection = Matrix4f(
		xScale, 0.0f, 0.0f, 0.0f,
		0.0f, yScale, 0.0f, 0.0f,
		0.0f, 0.0f, -1.0f, -0.01f,
		0.0f, 0.0f, -1.0f, 0.0f
		);*/

	// Initialize the projection matrix
	/*float aspectRatio = viewport.aspect_ratio();
	cml::matrix_perspective_xfov_RH(m_constantBufferData.projection,
		Const<float>::HALF_PI(), aspectRatio, 0.01f, 100.0f, cml::z_clip_zero);*/
}

void ApplicationTest::OnPause()
{
}

void ApplicationTest::OnUpdate()
{
	// Get the device resources associated with the window.
	auto renderer = m_devRes->device();
	auto pipeline = m_devRes->pipeline();
	auto renderTarget = m_devRes->renderTarget();
	auto depthStencil = m_devRes->depthStencil();

	pipeline->Clear(depthStencil.get(), 0, 1.0f, 0);
	const IRenderTarget* target = renderTarget.get();
	pipeline->setRenderTargets(&target, 1, nullptr);

	// Clear the render target to a dark blue background.
	float color[4] = { 0.098039225f, 0.098039225f, 0.439215720f, 1.000000000f };
	pipeline->Clear(target, color);

	//
	// Update the constant buffer to rotate the cube model.
	//
	ColourEffect::ConstantBuffer cb;
	cb.model = m_constantBufferData.model; // cml::transpose(m_constantBufferData.model);
	cb.view = m_constantBufferData.view; // cml::transpose(m_constantBufferData.view);
	cb.projection = m_constantBufferData.projection; // cml::transpose(m_constantBufferData.projection);
	m_testShader->updateConstantsImmediate(pipeline, cb);

	//
	// Set the primitive topology.
	//
	pipeline->setInputDrawingMode();

	//
	// Renders a cube
	//
	size_t stride = sizeof(ColorVertex);
	size_t offset = 0;
	const IBuffer* vertexBuffer = m_vertexBuffer.get();
	pipeline->enableVertexBuffers(&vertexBuffer, 1, 0, &stride, &offset);
	pipeline->enableIndexBuffer(m_indexBuffer.get(), sizeof(uint32_t), 0);

	const IBuffer* constantBuffer = m_testShader->constantBuffer().get();
	pipeline->enableConstantBuffer(&constantBuffer, 1, 0);
	pipeline->enableInputLayout(m_testShader->shaderInput().get());
	pipeline->enableShader(m_testShader->vertexShader().get());
	pipeline->enableShader(m_testShader->pixelShader().get());
	pipeline->drawIndexd(sizeof_array(cubeIndices), 0);
}