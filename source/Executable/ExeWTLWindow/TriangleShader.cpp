#include "wtlwindowafx.h"
#include "TriangleShader.h"
#include "ShaderCompilerHLSL.h"


using namespace engine;

TriangleShader::TriangleShader(IRendererPtr renderer)
{
	// Load the vertex shader
	shader_handle shaderCode = Shaderhlsl::compileFromFile(CShaderVersion::SV_4_0,
		CShaderType::ST_VERTEX, "VS", L"TriVertex.hlsl");
	m_vertexShader = renderer->createShader(CShaderType::ST_VERTEX, shaderCode);

	// Create the input layout
	CInputElement elements[]
	{
		CInputElement(0, 0, CDataType::DT_FLOAT, 3, "POSITION")
	};

	size_t arraySize = sizeof(elements) / sizeof(elements[0]);
	m_shaderInput = renderer->createInputFormat(&elements[0], arraySize, shaderCode);

	// Load the pixel shader
	shaderCode = Shaderhlsl::compileFromFile(CShaderVersion::SV_4_0,
		CShaderType::ST_PIXEL, "PS", L"TriPixel.hlsl");
	m_pixelShader = renderer->createShader(CShaderType::ST_PIXEL, shaderCode);
}

void TriangleShader::apply(std::shared_ptr<IPipeline> const& pipeline)
{
	//
	// Set the input layout.
	//
	pipeline->enableInputLayout(m_shaderInput.get());

	//
	// Vertex shader stage.
	//
	pipeline->enableShader(m_vertexShader.get());
	
	//
	// Pixel shader stage.
	//
	pipeline->enableShader(m_pixelShader.get());


	// throw std::logic_error("The method or operation is not implemented.");
}
