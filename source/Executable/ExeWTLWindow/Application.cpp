#include "wtlwindowafx.h"
#include "Application.h"
#include "IWindow.h"
#include "GameView.h"

// Objects
#include "Crate.h"
#include "PlayerView.h"
#include "SystemEvents.h"

using namespace engine;


Application::Application(const shared_ptr<IWindow>& window) 
	:
	m_window(window),
	m_devRes(window->getDeviceResources())
{
	auto onSizeEvent = bind(&Application::onSizeEvent, this, _1);
	m_window->addObserver(SizeEvent::slot_type(onSizeEvent));
}

Application::~Application()
{
}

void Application::OnCreate()
{
	// Create the playerview.
	//------------------------------------------------------
	auto playerView = std::make_shared<PlayerView>(m_window);
	playerView->localTransform().setPosition({ 0.0f, 0.0f, -5.0f });

	// Create the cube and set it's data.
	//------------------------------------------------------
	auto crate = ColorCrate::createCrate(m_devRes->device());
	crate->localTransform().setPosition({ 0.0f, 0.0f, -1.0f });

	// Create the root node for the entire scene.
	//------------------------------------------------------
	m_gameView = std::make_shared<GameView>(m_devRes);
	m_gameView->add(playerView);
	m_gameView->add(crate);
}

void Application::OnDestroy()
{

}

void Application::OnResume()
{
	// Start the timer
	//------------------------------------------------------
	m_timer.start();

	// Resize any window dependant rendering resources.
	//------------------------------------------------------
	m_gameView->resize(m_window);
}

void Application::OnPause()
{
	// Stop the timer, the game is paused.
	//------------------------------------------------------
	m_timer.stop();

}

void Application::OnUpdate()
{
	// Timer tick
	//------------------------------------------------------
	m_timer.update();

	/*Matrix4f rotation(cml::identity_4x4());
	cml::matrix_rotation_world_y(rotation, (angle * Const<float>::TO_RAD()));

	angle += 1.0f;

	auto crate = m_gameView->getChild(crateIndex);
	crate->localTransform().SetRotation(rotation); */

	// Update the root view which will trigger an update of all
	// child nodes.
	//------------------------------------------------------
	m_gameView->UpdateLayout();

	// Draw the scene
	m_gameView->draw();
}

void Application::onSizeEvent(const SizeEvent& sizeEvent) {


}