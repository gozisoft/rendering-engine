#ifndef __IGAME_LOOPER_H__
#define __IGAME_LOOPER_H__

#include "Core.h"

_ENGINE_BEGIN

class IGameLooper
{
public:
	virtual ~IGameLooper() { /**/ }
	virtual void OnUpdate() = 0;
};

_ENGINE_END

#endif