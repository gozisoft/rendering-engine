#pragma once
#ifndef __CMOUSE_CONTROLLER_H__
#define __CMOUSE_CONTROLLER_H__

#include "IMouseController.h"

// Exclude rarely-used stuff from Windows headers
#define WIN32_LEAN_AND_MEAN		
#include <Windows.h>
#undef WIN32_LEAN_AND_MEAN

_ENGINE_BEGIN

class CMouseController : public IMouseController
{
public:
	CMouseController(const Point2i& wsize, HWND hwnd, bool windowed);
	~CMouseController();

	void SetVisible(bool visible);

	// Check if cursor is visible
	bool IsVisible() const;

	// Set new pos of cursor
	void SetMousePos(const Point2i& pos);

	// Sets the relative pos 
	// Value lies between [0.0, 1.0]
	// where (0.0f, 0.0f) is the top left corner and
	// (1.0f, 1.0f) is the bottom right corner of the render window.
	void SetRelativePos(const Point2f& pos);

	// Get pos of coursor
	// Piexl Pos of the mouse. ie pos based upon entire monitor area
	const Point2i& GetMousePos();

	// Pos of mouse relative to the size of the active window.
	// Value lies between [0.0, 1.0]
	// where (0.0f, 0.0f) is the top left corner and
	// (1.0f, 1.0f) is the bottom right corner of the render window.
	Point2f GetRelativeMousePos();

	// retrieve the stored client screen size
	Point2i GetClientSize() const;

	// Notify on window resize
	void OnResize(const Point2i& size);

	// Notifies cursur on that resizable sttings have changed
	void UpdateBorderSize(bool windowed, bool resizable);

	// Set the current window handle on window input
	void SetHWND(HWND hwnd);

private:
	// update internal cursor position
	void UpdateCursorPos();

	Point2i m_cursorPos;

	Point2f m_invClientSize;
	Point2i m_winSize;

	int32_t m_borderX;
	int32_t m_borderY;

	bool m_isVisible;

	HWND m_hwnd;
};

#include "MouseController.inl"

_ENGINE_END



#endif