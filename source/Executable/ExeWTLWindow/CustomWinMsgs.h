#ifndef __CUSTOM_WINDOWS_MESSAGES_H__
#define __CUSTOM_WINDOWS_MESSAGES_H__

#include <atlbase.h>

// Declarating of custom registered messages using this macro.
#define DECLARE_USER_MESSAGE(name) \
     static const UINT name = ::RegisterWindowMessage(name##_MSG);

// Constant strings for custom messages.
static const UINT WM_GAMELOOP_ADDED = (WM_APP+1);
static const UINT WM_GAMELOOP_REMOVED  = (WM_APP+2);

///////////////////////////////////////////////////////////////////////////////
// Message map macro for custom cracked handlers

// void OnGameLoopAdded(CGameLoop& gameloop)
#define MSG_WM_GAMELOOP_ADDED(func) \
	if (uMsg == WM_GAMELOOP_ADDED) \
	{ \
		SetMsgHandled(TRUE); \
		lResult = (LRESULT)func( reinterpret_cast<CGameLoop>(lParam) ); \
		if(IsMsgHandled()) \
			return TRUE; \
	}

// void OnGameLoopRemoved(CGameLoop& gameloop)
#define MSG_WM_GAMELOOP_ADDED(func) \
	if (uMsg == WM_GAMELOOP_ADDED) \
	{ \
		SetMsgHandled(TRUE); \
		lResult = (LRESULT)func( reinterpret_cast<CGameLoop>(lParam) ); \
		if(IsMsgHandled()) \
			return TRUE; \
	}


#endif