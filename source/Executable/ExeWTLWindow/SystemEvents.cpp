#include "wtlwindowafx.h"
#include "SystemEvents.h"

using namespace engine;

const std::string SizeEvent::SIZE_EVENT = "SizeEvent";
const std::string KeyboardEvent::KEY_UP = "KeyUpEvent";
const std::string KeyboardEvent::KEY_DOWN = "KeyDownEvent";
const std::string MouseEvent::MOUSE_EVENT = "MounseEvent";