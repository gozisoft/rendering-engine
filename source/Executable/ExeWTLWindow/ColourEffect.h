#pragma once
#ifndef COLOUREFFECT_H
#define COLOUREFFECT_H

#include "Core.h"
#include "Effect.h"
#include <string>
#include <vector>

_ENGINE_BEGIN

class ColourEffect : public Effect
{
public:
	// Constant buffer structure
	struct ConstantBuffer
	{
		Matrix4f model;
		Matrix4f view;
		Matrix4f projection;
	};

	// Constructor
	ColourEffect(std::shared_ptr<IRenderer> const& renderer);

	// Apply the effect, I guess.
	void apply(std::shared_ptr<IPipeline> const& pipeline) override;

	// Update the constants when apply is called.
	void updateConstants(ConstantBuffer const& data);

	void updateConstantsImmediate(shared_ptr<IPipeline> const& pipeline,
		const ConstantBuffer& data);

	//
	// Accessors
	//

	// Constant buffer that can interact with constant registers
	// in this effect.
	ConstantBuffer ConstantData() const;

	IShaderPtr vertexShader() const;
	IShaderPtr pixelShader() const;

	IBufferPtr constantBuffer() const;

	IInputFormatPtr shaderInput() const;

private:
	ConstantBuffer m_constantData;
	IShaderPtr m_vertexShader;
	IShaderPtr m_pixelShader;
	IBufferPtr m_constantBuffer;
	IInputFormatPtr m_shaderInput;

	// Dirty flag for when updateConstants is called
	// and data changes.
	bool m_dirty;

	// Objects to render
	std::vector<CSpatialPtr> m_renderables;
};

#include "ColourEffect.inl"


_ENGINE_END


#endif