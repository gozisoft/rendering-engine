#pragma once

inline ColourEffect::ConstantBuffer ColourEffect::ConstantData() const
{
	return m_constantData; 
}

inline IShaderPtr ColourEffect::vertexShader() const
{ 
	return m_vertexShader;
}

inline IShaderPtr ColourEffect::pixelShader() const
{ 
	return m_pixelShader; 
}

inline IBufferPtr ColourEffect::constantBuffer() const
{ 
	return m_constantBuffer; 
}

inline IInputFormatPtr ColourEffect::shaderInput() const
{ 
	return m_shaderInput; 
}