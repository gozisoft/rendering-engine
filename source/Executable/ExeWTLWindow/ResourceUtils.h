#pragma once
#ifndef RESOURCE_UTILS_H
#define RESOURCE_UTILS_H

#include <atomic>

class CResourceUtils
{
public:
	/**
	* Generate a value suitable for use in {@link #setId(int)}.
	* This value will not collide with ID values generated at build time by aapt for R.id.
	*
	* @return a generated ID value
	*/
	static uint32_t generateResId() {
		for (;;) {
			uint32_t result = sNextGeneratedResId.load();
			// aapt-generated IDs have the high byte nonzero; clamp to the range under that.
			uint32_t newValue = result + 1;
			if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
			if (sNextGeneratedResId.compare_exchange_weak(result, newValue)) {
				return result;
			}
		}
	}

private:
	// Atomic integer for generatiing unique id's for
	// resources and assets.
	static std::atomic_uint32_t sNextGeneratedResId;

};


#endif