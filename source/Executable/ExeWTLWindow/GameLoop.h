#ifndef __CGAME_LOOP_H__
#define __CGAME_LOOP_H__

#include "wtlwindowfwd.h"

// ATL headers
#include <atlbase.h>

_ENGINE_BEGIN

class CGameLoop
{
public:
	VOID AddGameLooper(shared_ptr<IGameLooper> gamelooper);
	VOID RemoveGameLooper(shared_ptr<IGameLooper> gamelooper);
	int Run();

private:
	ATL::CSimpleArray< shared_ptr<IGameLooper> > m_gameloopers;
};

_ENGINE_END

#endif