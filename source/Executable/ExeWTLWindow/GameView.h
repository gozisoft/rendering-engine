#pragma once

#ifndef GAMEVIEW_H
#define GAMEVIEW_H

#include "VisualGroup.h"
#include <vector>

_ENGINE_BEGIN

/// <summary>
/// The game view represents the entire window view. It contains
/// all PlayerView instances that are used to split the 
/// screen in multiple view ports.
/// </summary>
class GameView : public VisualGroup 
{
public:
	// We're using the base methods add method and
	// adding an override.
	using VisualGroup::add;

	// Default constructor requires renderer.
	GameView(const shared_ptr<IDeviceResources>& device);

	///
	/// Method for adding player views specifically.
	/// 
	void add(const shared_ptr<PlayerView>& player);

	///
	/// Resizes the player view viewports.
	///
	void resize(const shared_ptr<IWindow>& window);

	///
	/// Override the on draw method to update scene data.
	///
	void draw();

private:
	// The player view that deals with rendering
	// objects on screen.
	std::vector<shared_ptr<PlayerView>> m_players;

	// Rendering device used in this application
	shared_ptr<IDeviceResources> m_devRes;
};


_ENGINE_END

#endif