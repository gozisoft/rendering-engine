#pragma once
#ifndef __CTRIANGLE_TEST_H__
#define __CTRIANGLE_TEST_H__

#include "IRenderer.h"
#include "IResource.h"
#include "IView.h"
#include "IEventManager.h"
#include "IProcessManager.h"
#include "TriangleShader.h"

_ENGINE_BEGIN

class CTriangleTest 
{
private:
	// The number of view ports
	static const uint32_t NUM_VIEWPORTS = 1;

	// An instance of the window
	IWindowPtr m_window;

	// Rendering device used in this application
	IRendererPtr m_device;
	IPipelinePtr m_pipeline;
	IRenderTargetPtr m_renderTarget;
	IDepthStencilPtr m_depthStencil;

	// Manager class for processes
	IProcessManagerPtr m_procensMgr;


	IBufferPtr m_vertexBuffer;
	std::shared_ptr<CTriangleShader> m_testShader;

public:
	/// Constructor 
	CTriangleTest(IWindowPtr window, IRendererPtr renderer);

	/// Destructor
	~CTriangleTest();

	// Startup function
	void OnCreate();
	void OnResume();
	void OnPause();
	void OnUpdate();
	void OnDestroy();
};

_ENGINE_END

#endif