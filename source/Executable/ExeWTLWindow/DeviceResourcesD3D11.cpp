#include "wtlwindowafx.h"
#include "DeviceResourcesD3D11.h"
#include "ExceptionD3D.h"
#include "RenderView.h"

using namespace engine;

// Attach this rendering device to a window.
DeviceResourcesD3D11::DeviceResourcesD3D11(
	shared_ptr<IWindow> const& window,
	shared_ptr<IRenderer> const& renderer)
{
	auto windowPtr = std::dynamic_pointer_cast<CWindow>(window);
	if (windowPtr == nullptr) {
		throw std::runtime_error("Cannot downcast window to CWindow type.");
	}

	m_device = std::dynamic_pointer_cast<RendererD3D11>(renderer);
	if (m_device == nullptr) {
		throw std::runtime_error("Cannot downcast IRenderer to RendererD3D11 type.");
	}

	m_pipeline = m_device->_getPipeline();

	// Create a swap chain for back buffer rendering
	m_swapChain = m_device->CreateSwapChain(NUMBACKBUFFERS, windowPtr->m_hWnd,
		CTextureFormat::TF_R8G8B8A8_UNORM, CBufferSwap::BS_DISCARD);

	// Create default raster state
	m_rasteState = m_device->createRasterState(std::make_shared<RasterizerState>());
	m_pipeline->enableRasterState(m_rasteState.get());

	// Create resources that are depenant on the window.
	DeviceResourcesD3D11::configureBackBuffer();
}

void DeviceResourcesD3D11::resize(long width, long height)
{
	// When you want to resize the swap chain, you need to call 
	// Release on both the ID3D11Texture2D as well as the ID3D11RenderTargetView for the back buffer. 
	// Then you can call ResizeBuffers on the swap chain. 
	releaseBackBuffer();

	// Rendering data changed.
	ThrowIfFailed(m_swapChain->ResizeBuffers(
		0,													// Number of buffers. Set this to 0 to preserve the existing setting.
		0, 0,												// Width and height of the swap chain. Set to 0 to match the screen resolution.
		CAPI_To_D3D11::DXGIFormat(CTextureFormat::TF_NONE), // This tells DXGI to retain the current back buffer format.
		0));

	// Obtain the new back buffer
	configureBackBuffer();
}

void DeviceResourcesD3D11::goFullScreen()
{
	ThrowIfFailed(m_swapChain->SetFullscreenState(TRUE, nullptr));

	// Swap chains created with the DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL flag need to
    // call ResizeBuffers in order to realize a full-screen mode switch. Otherwise, 
    // your next call to Present will fail.

	// Before calling ResizeBuffers, you have to release all references to the back 
	// buffer device resource.
	releaseBackBuffer();

	// Rendering data changed.
	ThrowIfFailed(m_swapChain->ResizeBuffers(
		0,													// Number of buffers. Set this to 0 to preserve the existing setting.
		0, 0,												// Width and height of the swap chain. Set to 0 to match the screen resolution.
		CAPI_To_D3D11::DXGIFormat(CTextureFormat::TF_NONE), // This tells DXGI to retain the current back buffer format.
		0));

	// Then we can recreate the back buffer, depth buffer, and so on.
	configureBackBuffer();
}

void DeviceResourcesD3D11::goWindowed()
{
	ThrowIfFailed(m_swapChain->SetFullscreenState(FALSE, nullptr));

	// Swap chains created with the DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL flag need to
	// call ResizeBuffers in order to realize a change to windowed mode. Otherwise, 
	// your next call to Present will fail.

	// Before calling ResizeBuffers, you have to release all references to the back 
	// buffer device resource.
	releaseBackBuffer();

	// Now we can call ResizeBuffers.
	ThrowIfFailed(m_swapChain->ResizeBuffers(
		0,                   // Number of buffers. Set this to 0 to preserve the existing setting.
		0, 0,                // Width and height of the swap chain. MUST be set to a non-zero value. For example, match the window size.
		DXGI_FORMAT_UNKNOWN, // This tells DXGI to retain the current back buffer format.
		0
		));

	// Then we can recreate the back buffer, depth buffer, and so on.
	configureBackBuffer();
}

void DeviceResourcesD3D11::configureBackBuffer()
{
	// Obtain the back buffer from the swap chain
	m_backBuffer = m_device->getBackBuffer(m_swapChain, 0);

	// Create a render target for the view port
	//------------------------------------------------------
	m_renderTarget = m_backBuffer->_createRTView();

	// Once the render target view is created, create a depth stencil view. This 
	// allows API to efficiently render objects closer to the camera in front 
	// of objects further from the camera. 
	//------------------------------------------------------
	SMipMap2D mip{ m_backBuffer->width(), m_backBuffer->height(), 0, 0 };

	m_depthStencil = m_device->_createTexture(
		CResourceUse::RU_DEPTHSTENCIL,
		CResourceAccess::RA_GPU,
		CTextureFormat::TF_D32_FLOAT,
		&mip, 1, nullptr);

	// Create the depth stencil view.
	//------------------------------------------------------
	m_depthStencilView = m_depthStencil->_createDSView();
}

void DeviceResourcesD3D11::releaseBackBuffer()
{
	// Release the render target view based on the back buffer:
	m_renderTarget.reset();

	// Release the back buffer itself:
	m_backBuffer.reset();

	// The depth stencil will need to be resized, so release it (and view):
	m_depthStencilView.reset();
	m_depthStencil.reset();

	// After releasing references to these resources, we need to call Flush() to 
	// ensure that Direct3D also releases any references it might still have to
	// the same resources - such as pipeline bindings.
	m_pipeline->flush();
}