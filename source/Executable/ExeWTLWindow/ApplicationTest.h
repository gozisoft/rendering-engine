#pragma once
#ifndef __APPLICATION_TEST_H__
#define __APPLICATION_TEST_H__

#include "ColourEffect.h"
#include "Camera.h"

_ENGINE_BEGIN

class ApplicationTest
{
private:
	// The number of view ports
	static const uint32_t NUM_VIEWPORTS = 1;

	// An instance of the window
	shared_ptr<IWindow> m_window;
	shared_ptr<IDeviceResources> m_devRes;

	// Rendering device used in this application
	shared_ptr<IBuffer> m_vertexBuffer;
	shared_ptr<IBuffer> m_indexBuffer;

	// Shader
	ColourEffect::ConstantBuffer m_constantBufferData;
	shared_ptr<ColourEffect> m_testShader;

	// Camera
	Camera m_camera;

public:
	/// Constructor 
	ApplicationTest(shared_ptr<IWindow> window);

	/// Destructor
	~ApplicationTest();

	// Startup function
	void OnCreate();
	void OnResume();
	void OnPause();
	void OnUpdate();
	void OnDestroy();
};

_ENGINE_END

#endif