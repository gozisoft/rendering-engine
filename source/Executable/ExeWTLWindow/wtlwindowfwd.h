#pragma once
#ifndef __WTL_TEST_H__
#define __WTL_TEST_H__

#include "Core.h"

_ENGINE_BEGIN

class RenderWindow;
class CGameLoop;

// -------------------------------------------------------------------
// Interface
// -------------------------------------------------------------------
class IGameLooper;

// -------------------------------------------------------------------
// Effects
// -------------------------------------------------------------------
class ColourEffect;

// -------------------------------------------------------------------
// Logic
// -------------------------------------------------------------------
class IDeviceResources;
class DeviceResourcesD3D11;

class GameView;
class PlayerView;
class Application;
class ApplicationTest;


// -------------------------------------------------------------------
// Events
// -------------------------------------------------------------------
template <class T> class Event;
class SizeEvent;
class KeyboardEvent;
class MouseEvent;

_ENGINE_END



#endif