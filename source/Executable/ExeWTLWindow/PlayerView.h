#pragma once
#ifndef __CPLAYER_VIEW_H__
#define __CPLAYER_VIEW_H__

#include "CameraNode.h"
#include "RendererTypes.h"

_ENGINE_BEGIN

class PlayerView : public CameraNode
{
public:
	// Default constructor
	PlayerView(const shared_ptr<IWindow>& window);
	~PlayerView();

	// Set the size of the players view.
	void resize(const Vector4i& rect);

	// Accessors for the view port.
	const ViewPort& viewport() const;

	// Used to listen for keyboard events.
	void onKeyEvent(const KeyboardEvent& keyEvent);
	void onMouseEvent(const MouseEvent& mouseEvent);

private:
	// This player views view port.
	ViewPort m_viewPort;

};

#include "PlayerView.inl"

_ENGINE_END

#endif