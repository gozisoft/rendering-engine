#ifndef __CPLAYER_VIEW_INL__
#define __CPLAYER_VIEW_INL__

inline const ViewPort& PlayerView::viewport() const
{
	return m_viewPort;
}

#endif