#pragma once
#ifndef CRATE_H
#define CRATE_H

#include "TriangleMesh.h"

_ENGINE_BEGIN

class ColorCrate : public TriMesh
{
public:
	// Factory method for creating the crate.
	static shared_ptr<ColorCrate> createCrate(shared_ptr<IRenderer> renderer);

	// Simple constructor
	ColorCrate(shared_ptr<VertexBuffer> const& vbuffer,
		shared_ptr<IndexBuffer> const& ibuffer,
		shared_ptr<ColourEffect> const& effect);

	// Special draw function to transfer
	// camera info to draw call.
	void onDraw(shared_ptr<IPipeline> pipeline, const ICamera& camera) override;
	
private:
	shared_ptr<IInputFormat> m_inputFormat;
	shared_ptr<ColourEffect> m_colourEffect;
};

_ENGINE_END

#endif