#include "wtlwindowafx.h"
#include "PlayerView.h"
#include "SystemEvents.h"

using namespace engine;

PlayerView::PlayerView(const shared_ptr<IWindow>& window)
	: CameraNode()
{
	auto onKeyEvent = boost::bind(&PlayerView::onKeyEvent, this, _1);
	auto onMouseEvent = boost::bind(&PlayerView::onMouseEvent, this, _1);

	window->addObserver(KeyboardEvent::slot_type(onKeyEvent));
	window->addObserver(MouseEvent::slot_type(onMouseEvent));
}

PlayerView::~PlayerView()
{
}

void PlayerView::resize(const Vector4i& rect)
{
	// Set the viewport size.
	m_viewPort = ViewPort(rect.data(), 0.0f, 1.0f);

	// Update the camera frustum with the new viewport.
	m_camera.SetFrustum(70.0f,                                // use a 70-degree vertical field of view
		m_viewPort.aspect_ratio(),							  // specify the aspect ratio of the window
		0.01f,                                                // specify the nearest Z-distance at which to draw vertices
		100.0f);                                              // specify the farthest Z-distance at which to draw vertices
}

void PlayerView::onKeyEvent(const KeyboardEvent& keyEvent)
{
	if (KeyboardEvent::KEY_DOWN == keyEvent.type())
	{
		double deltaTime = OS::Timer::GetDeltaTime();

		// Get latest pos.
		Vector3f posDelta = m_worldTransform.getPosition();
		Vector3f moveDir = m_camera.forward() - posDelta;

		// Move the camera here.
		if (keyEvent.keyCode() == KeyCode::KEY_UP) {
			// increment the camera position
			posDelta += moveDir * deltaTime * 1.0f;
		}

		// Move the camera here.
		if (keyEvent.keyCode() == KeyCode::KEY_DOWN) {
			// increment the camera position
			posDelta -= moveDir * deltaTime * 1.0f;
		}
	}
}

void PlayerView::onMouseEvent(const MouseEvent& mouseEvent)
{



}