#pragma once
#ifndef __CRAW_INPUT_ERROR_H__
#define __CRAW_INPUT_ERROR_H__

#include <stdexcept>

class rawinput_error : public std::runtime_error
{
public:
	typedef std::runtime_error my_base;

	explicit rawinput_error(const char* _Message)
		: my_base(_Message) {}

	explicit rawinput_error(const std::string& _Message)
		: my_base(_Message) {}

	virtual ~rawinput_error() {};
};



//inline void ThrowIfFailed(UINT result)
//{
//	if (result < 0)
//	{
//		// Set a breakpoint on this line to catch DX API errors.
//		throw rawinput_error(::GetLastError());
//	}
//}
//
//inline void ThrowIfFailed(BOOL result)
//{
//	if (!result)
//	{
//		// Set a breakpoint on this line to catch DX API errors.
//		throw rawinput_error(::GetLastError());
//	}
//}

#endif