#pragma once

#ifndef PASS_INFO_H
#define PASS_INFO_H

#include "Core.h"
#include <vector>

_ENGINE_BEGIN

class IPassInfo {
	virtual ~IPassInfo() {}

	virtual const uint8_t* get_constant_data_c() const = 0;

	virtual std::vector<uint8_t> get_constant_data() const = 0;
};

class IRenderPassable {
public:



};

_ENGINE_END

#endif