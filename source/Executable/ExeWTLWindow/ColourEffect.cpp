#include "wtlwindowafx.h"
#include "ColourEffect.h"
#include "ShaderCompilerHLSL.h"

using namespace engine;


ColourEffect::ColourEffect(std::shared_ptr<IRenderer> const& renderer)
{
	// auto resources = Resources::get_instance();
	// resources->getShader()

	// Load the vertex shader
	shader_handle shaderCode = Shaderhlsl::compileFromFile(CShaderVersion::SV_4_0,
		CShaderType::ST_VERTEX,	"SimpleVertexShader", L"ColourVertexShader.hlsl");
	m_vertexShader = renderer->createShader(CShaderType::ST_VERTEX, shaderCode);

	// Create the input layout
	CInputElement elements[]
	{
		CInputElement(0, 0, CDataType::DT_FLOAT, 3, "POSITION"),
		CInputElement(0, 12, CDataType::DT_FLOAT, 4, "COLOR")
	};

	size_t arraySize = sizeof_array(elements);
	m_shaderInput = renderer->createInputFormat(&elements[0], arraySize, shaderCode);

	// Create a constant buffer for passing model, view, and projection matrices 
	// to the vertex shader. This will allow us to rotate the cube and apply 
	// a perspective projection to it. 
	m_constantBuffer = renderer->createBuffer(CResourceUse::RU_CONSTANT,
		CResourceAccess::RA_DYNAMIC, sizeof(ConstantBuffer));

	// Load the pixel shader
	shaderCode = Shaderhlsl::compileFromFile(CShaderVersion::SV_4_0,
		CShaderType::ST_PIXEL, "SimplePixelShader", L"ColourPixelShader.hlsl");
	m_pixelShader = renderer->createShader(CShaderType::ST_PIXEL, shaderCode);
}

void ColourEffect::apply(std::shared_ptr<IPipeline> const&  pipeline) {
	if (m_dirty) {
		// Update the constant buffers
		updateConstantsImmediate(pipeline, m_constantData);
	}

	pipeline->enableInputLayout(m_shaderInput);
	pipeline->enableConstantBuffer({ m_constantBuffer }, 0);
	pipeline->enableShader(m_vertexShader);
	pipeline->enableShader(m_pixelShader);
}

void ColourEffect::updateConstants(ConstantBuffer const& data)
{
	// Copy data over and set the dirty flag.
	m_constantData.model = data.model;
	m_constantData.view = data.view;
	m_constantData.projection = data.projection;

	// Data has changed set dirty to true.
	m_dirty = true;
}

void ColourEffect::updateConstantsImmediate(shared_ptr<IPipeline> const& pipeline,
	const ConstantBuffer& data)
{
	// Update the constant buffer, gpu side..
	size_t alignedSize = 0;
	ConstantBuffer* gpu = static_cast<ConstantBuffer*>(m_constantBuffer->Map(pipeline.get(),
		CResourceLock::RL_DISCARD, alignedSize));

	// Copy the matrices into the constant buffer.
	gpu->model = data.model;
	gpu->view = data.view;
	gpu->projection = data.projection;

	// Unlock the constant buffer.
	m_constantBuffer->UnMap(pipeline.get());

	// Reset the dirty flag as data has been transferred.
	m_dirty = false;
}

//void ColourEffect::addSceneItems(std::vector<std::shared_ptr<Spatial>> const& items)
//{
//	// Increase the size of the array to hold all the items.
//	m_renderables.resize(m_renderables.size() + items.size());
//
//	// Add the scene items to the renderables.
//	m_renderables.insert(m_renderables.end(), items.begin(), items.end());
//}
