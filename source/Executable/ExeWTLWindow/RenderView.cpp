#include "wtlwindowafx.h"
#include "RenderView.h"
#include "GameLoop.h"

// The application class implementation
#include "Application.h"

// The raw input interface
#include "RawInput.h"

// Renderer stuff.
#include "RendererD3D11.h"
#include "ExceptionD3D.h"

// Used for formatting error message.
#include <strsafe.h>

#include "RawInputError.h"

using namespace engine;

const size_t NUMBACKBUFFERS = 2U;

RenderWindow::RenderWindow(CGameLoop& gameloop)
	:
	my_base(),
	m_gameloop(gameloop),
	m_windowed(false),
	m_minimised(false),
	m_maximised(false),
	m_borderDrag(false),
	m_closing(false) {
}

RenderWindow::~RenderWindow() {

}

Vector4i RenderWindow::getWindowCoords() {
	RECT rect;
	GetClientRect(&rect);
	return{ rect.left, rect.top, rect.right, rect.bottom };
}

shared_ptr<IDeviceResources> RenderWindow::getDeviceResources() {
	return m_devRes;
}

// Maybe have a while loop here for updating game
// simulating for a period of time before
// moving on to update the rendering.
void RenderWindow::OnUpdate() {
	try {
		// Check for paused state.
		if (m_minimised || m_borderDrag || m_closing) {
			return;
		}

		// Update the application runtime
		m_application->OnUpdate();

		// Clear the rendering device.
		ThrowIfFailed(
			m_devRes->swap_chain()->Present(1, 0)
			);
	}
	catch (std::exception& error) {
		// Display the error message
		MessageBoxA(error.what());
	}
}

// The window procedure of the new window receives this message after the window is created,
// but before the window becomes visible.
LRESULT RenderWindow::OnCreate(LPCREATESTRUCT lpcs) {
	try {
		// Register input devices (mouse, keyboard, etc.)
		registerInputDevices();

		// Add this class instance to the game looper if it is present.
		m_gameloop.AddGameLooper(shared_from_this());

		// Create rendering resources for attachment to window.
		m_devRes = make_shared<DeviceResourcesD3D11>(shared_from_this(),
			make_shared<RendererD3D11>());

		// Create the application class
		// m_application = make_shared<ApplicationTest>(shared_from_this());
		m_application = make_shared<Application>(shared_from_this());

		// m_application = make_shared<CApplicationTest>(shared_from_this());
		m_application->OnCreate();
		m_application->OnResume();
	}
	catch (std::exception& error) {
		MessageBoxA(error.what());
		DestroyWindow();
	}

	SetMsgHandled(true);
	return 0;
}

void RenderWindow::OnClose() {
	// Set window destroying flag
	m_closing = true;

	// Tell the application to pause first
	m_application->OnPause();

	// Cleanup raw input
	unRegisterInputDevices();

	// Post the quit message
	PostQuitMessage(0);
}

void RenderWindow::OnDestroy() {
	// then to destroy
	m_application->OnDestroy();
}

void RenderWindow::OnGetMinMaxInfo(LPMINMAXINFO pMinMaxInfo) {
	// Catch this message so to prevent the window from becoming too small.
	pMinMaxInfo->ptMinTrackSize.x = 200;
	pMinMaxInfo->ptMinTrackSize.y = 200;
}

void RenderWindow::OnSize(UINT nType, WTL::CSize size) {
	RECT rcClient;
	GetClientRect(&rcClient);
	if (!rcClient.top && !rcClient.bottom) {
		// Rapidly clicking the task bar to minimize and restore a window
		// can cause a WM_SIZE message with SIZE_RESTORED when 
		// the window has actually become minimized due to rapid change
		// so just ignore this message
		return;
	}

	switch (nType) {
	case SIZE_MINIMIZED:
		// Disable application on minimized
		if (m_application != nullptr) {
			m_application->OnPause();
		}
		break;

	case SIZE_MAXIMIZED:
		if (m_application != nullptr) {
			// Call onResume to reset application screen sizes and 
			// other resources.
			m_application->OnResume();
		}

		m_minimised = false;
		m_maximised = true;
		break;

	case SIZE_RESTORED: // The window has been resized, but neither the SIZE_MINIMIZED nor SIZE_MAXIMIZED value applies.
		if (m_minimised) {
			// Restoring from minimized state?
			if (m_application != nullptr) {
				m_application->OnResume();
			}
			m_minimised = false;
		}
		else if (m_maximised) {
			// Restoring from maximized state?
			CheckForWindowSizeChange(size);
			CheckForWindowChangingMonitors();
			m_maximised = false;
		}
		else if (m_borderDrag) {
			// If user is dragging the resize bars, we do not resize 
			// the buffers here because as the user continuously 
			// drags the resize bars, a stream of WM_SIZE messages are
			// sent to the window, and it would be pointless (and slow)
			// to resize for each WM_SIZE message received from dragging
			// the resize bars.  So instead, we reset after the user is 
			// done resizing the window and releases the resize bars, which 
			// sends a WM_EXITSIZEMOVE message.
		}
		else {
			// This WM_SIZE come from resizing the window via an API like SetWindowPos() so 
			// resize and reset the device now.
			CheckForWindowSizeChange(size);
			CheckForWindowChangingMonitors();
		}
		break;
	}
}

void RenderWindow::OnEnterSizeMove() {
	if (m_application != nullptr) {
		m_application->OnPause();
	}

	m_borderDrag = true;
}

void RenderWindow::OnExitSizeMove() {
	if (m_application != nullptr) {
		m_application->OnResume();
	}

	m_borderDrag = false;
}

void RenderWindow::OnKeyDown(TCHAR keycode, UINT repeatCount, UINT) {


}

void RenderWindow::OnKeyUp(TCHAR keycode, UINT repeatCount, UINT) {


}

void RenderWindow::OnInput(WPARAM inputCommand, HRAWINPUT rawInput) {
	// The size, in bytes, of the data in pData
	UINT dataSize = 0;
	GetRawInputData(rawInput, RID_INPUT, nullptr, &dataSize, sizeof(RAWINPUTHEADER));

	// Reserve data size.
	std::vector<uint8_t> data(dataSize);

	// Read in the data from raw input.
	if (GetRawInputData(rawInput, RID_INPUT, &data[0], &dataSize, sizeof(RAWINPUTHEADER)) != dataSize) {
		OutputDebugStringA(GetLastErrorAsString().c_str());
	}

	// Cast from byte pointer to RAWINPUT
	auto raw = reinterpret_cast<RAWINPUT*>(&data[0]);
	switch (raw->header.dwType) {
	case RIM_TYPEKEYBOARD: handleKeyboardInput(raw->data.keyboard); break;
	case RIM_TYPEMOUSE: handleMouseInput(raw->data.mouse); break;
	}
}

bool RenderWindow::CheckForWindowSizeChange(const WTL::CSize& size) {
	if (!m_windowed || size == m_prevSize) {
		return false;
	}

	// set the new client size (client area)
	m_prevSize = size;

	// Resize rendering resources.
	m_devRes->resize(size.cx, size.cy);

	// Dispatch a window size change event.

	// Size change occurred, return true.
	return true;
}

bool RenderWindow::CheckForWindowChangingMonitors() {
	return false;
}

void RenderWindow::registerInputDevices() {

	// RIDEV_NOLEGACY

	// Register to listen for raw input messages for the keyboard and mouse, ignore 
	// default Windows messages by setting RIDEV_NOLEGACY.
	RAWINPUTDEVICE devices[] = {
		{ // Mouse
			RawInput::HID_USAGE_PAGE,
			RawInput::HID_DEVICE_SYSTEM_MOUSE,
			0,
			m_hWnd
		},
		{ // Keyboard
			RawInput::HID_USAGE_PAGE,
			RawInput::HID_DEVICE_SYSTEM_KEYBOARD,
			0,
			m_hWnd
		}
	};

	// Register input devices
	if (!RegisterRawInputDevices(&devices[0], ARRAYSIZE(devices), sizeof(RAWINPUTDEVICE)))
	{
		displayLastError(TEXT("registerInputDevices()"));
	}
}

void RenderWindow::unRegisterInputDevices() {
	// Unregister to listen for raw input messages for the keyboard and mouse
	RAWINPUTDEVICE devices[] = {
		{ // Mouse
			RawInput::HID_USAGE_PAGE,
			RawInput::HID_DEVICE_SYSTEM_MOUSE,
			RIDEV_REMOVE,
			m_hWnd
		},
		{ // Keyboard
			RawInput::HID_USAGE_PAGE,
			RawInput::HID_DEVICE_SYSTEM_KEYBOARD,
			RIDEV_REMOVE,
			m_hWnd
		}
	};

	// Register input devices
	/*if (!RegisterRawInputDevices(&devices[0], ARRAYSIZE(devices), sizeof(RAWINPUTDEVICE))) {
		throw rawinput_error(GetLastErrorAsString());
	}*/
}

void RenderWindow::handleKeyboardInput(const RAWKEYBOARD& keyboard) {
	if (keyboard.VKey == 255) {
		// discard "fake keys" which are part of an escaped sequence
		return;
	}

	// Keyboard.MakeCode
	// The scan code from the key depression. 
	// The scan code for keyboard overrun is KEYBOARD_OVERRUN_MAKE_CODE.

	// keyboard.VKey
	// VKey : Windows message compatible virtual-key code.

	// Keyboard.Message
	// Message : Corresponding window message e.g. WM_KEYDOWN, WM_SYSKEYDOWN

	// keyboard.Flags
	// RI_KEY_BREAK : the key is up.
	// RI_KEY_MAKE : the key is down.
	// RI_KEY_E0 : the left version of the key.
	// RI_KEY_E1 : the right version of the key.

	// Get the keycode as an enum.
	KeyCode keycode = static_cast<KeyCode>(keyboard.VKey);

	// Virtual keycode is translated into unshifted character value.
	uint_t charkey = static_cast<uint_t>(MapVirtualKey(keyboard.VKey, MAPVK_VK_TO_CHAR));

	// Determine which key side was pressed
	KeyboardEvent::KeyLocation location = KeyboardEvent::KeyLocation::STANDARD;
	if ((keyboard.Flags & RI_KEY_E0) != 0)
	{
		location = KeyboardEvent::KeyLocation::LEFT;
	} 
	else if ((keyboard.Flags & RI_KEY_E1) != 0)
	{
		location = KeyboardEvent::KeyLocation::RIGHT;
	}

	// Send event to application
	bool isDown = (keyboard.Flags & RI_KEY_MAKE) != 0;
	if (isDown) {
		notify(KeyboardEvent(KeyboardEvent::KEY_DOWN, keycode, location, charkey));		
	}
	
	bool isUp = (keyboard.Flags & RI_KEY_BREAK) != 0;
	if (isUp) {
		notify(KeyboardEvent(KeyboardEvent::KEY_UP, keycode, location, charkey));
	}
	

	// Getting a human-readable string
//	bool isE0 = ((keyboard.Flags & RI_KEY_E0) != 0);
//	UINT key = (keyboard.MakeCode << 16) | (isE0 << 24);
//	char buffer[256] = {};
//	GetKeyNameText(key, buffer, 256);
//
//	// Format the string for pressing.
//	std::stringstream ss;
//	ss << buffer << std::endl;
//
//	// Print the string.
//	OutputDebugString(ss.str().c_str());
}

void RenderWindow::displayLastError(LPTSTR lpszFunction)
{
	// Retrieve the system error message for the last-error code
	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	// Display the error message and exit the process

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));

	StringCchPrintf((LPTSTR)lpDisplayBuf, LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"), lpszFunction, dw, lpMsgBuf);

	MessageBox((LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}

void RenderWindow::handleMouseInput(const RAWMOUSE& mouse) {



}

// http://www.codeproject.com/Tips/479880/GetLastError-as-std-string
std::string RenderWindow::GetLastErrorAsString() {
	//Get the error message, if any.
	DWORD errorMessageID = ::GetLastError();
	if (errorMessageID == 0) {
		return "No error message has been recorded";
	}

	LPSTR messageBuffer = nullptr;
	DWORD flags = FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS;
	DWORD size = FormatMessageA(flags, nullptr, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		reinterpret_cast<LPSTR>(&messageBuffer), 0, nullptr);

	// Copy messageBuffer over to std::string.
	std::string message(messageBuffer, size_t(size));

	//Free the buffer.
	LocalFree(messageBuffer);

	// Return the message
	return message;
}

//// Load accelerator table
//m_hAccel = ::LoadAccelerators( _AtlBaseModule.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME) );
//if (m_hAccel == NULL)
//{
//	ATLASSERT( false && _T("Failed to load accelerator table") );
//	return 0;
//}

//int CRenderView::RunMessageLoop()
//{
//	MSG msg;
//	SecureZeroMemory( &msg, sizeof(MSG) );*
//
//	// Process while no quit message has been received
//	// or onidle has not failed.
//	for (/**/;/**/;/**/)
//	{
//		// Process all messages in the queue.
//		while ( ::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) == TRUE )
//		{
//			// Attmpt to translate and dispatch the messages.
//			ProcessMessage(&msg);
//		}
//
//		// Check for the WM_QUIT message to exit the loop.
//		if (msg.message == WM_QUIT)
//		{
//			ATLTRACE2(atlTraceUI, 0, _T("CMainFrame::RunMessageLoop - exiting\n"));
//			break;		// WM_QUIT, exit message loop
//		}
//
//		// Idle processing
//		if ( OnIdle() == FALSE )
//		{
//			ATLTRACE2(atlTraceUI, 0, _T("CMainFrame::RunMessageLoop - exiting\n"));
//			break;		// WM_QUIT, exit message loop
//		}
//	}
//
//	// Return the message on termination
//	return (int)msg.wParam;
//}
//
//BOOL CRenderView::PreTranslateMessage(MSG* pMsg)
//{
//	if (m_hAccel != NULL && ::TranslateAccelerator(m_hWnd, m_hAccel, pMsg))
//		return TRUE;
//
//	return FALSE;
//}
//
//void CRenderView::ProcessMessage(MSG* pMsg)
//{
//	if(PreTranslateMessage(pMsg) == FALSE)
//	{
//		::TranslateMessage(pMsg);
//		::DispatchMessage(pMsg);
//	}
//}
