#include "wtlwindowafx.h"
#include "GameLoop.h"
#include "IGameLooper.h"

using namespace engine;

VOID CGameLoop::AddGameLooper(shared_ptr<IGameLooper> gamelooper)
{
	// Add the game looper to the array.
	BOOL result = m_gameloopers.Add(gamelooper);
	if (!result)
	{
		ATLASSERT(!"Could not add gamelooper.");
		return;
	}
}

VOID CGameLoop::RemoveGameLooper(shared_ptr<IGameLooper> gamelooper)
{
	// Remove the game looper from the array.
	BOOL result = m_gameloopers.Remove(gamelooper);
	if (!result)
	{
		ATLASSERT(!"Could not remove gamelooper.");
		return;
	}
}

int CGameLoop::Run()
{
	MSG msg;
	SecureZeroMemory(&msg, sizeof(MSG));

	// Process while no quit message has been received
	// or onidle has not failed.
	while (TRUE)
	{
		// Process all messages in the queue.
		while (::PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE) == TRUE)
		{
			// Attmpt to translate and dispatch the messages.
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);

			// Check for the WM_QUIT message to exit inner loop.
			if (msg.message == WM_QUIT) {
				break;
			}
		}

		// Check for the WM_QUIT message to exit the outer loop.
		if (msg.message == WM_QUIT) {
			// ATLTRACE2(atlTraceUI, 0, _T("CGameLoop::Run - exiting\n"));
			break;		// WM_QUIT, exit message loop
		}

		// Idle processing
		auto gameLoopCount = m_gameloopers.GetSize();
		for (auto i = 0; i < gameLoopCount; ++i)
		{
			m_gameloopers[i]->OnUpdate();
		}
	}

	// Return the message on termination
	return static_cast<int>(msg.wParam);
}