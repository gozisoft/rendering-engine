// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// External headers
#include "libapiheaders.h"
#include "libmathsheaders.h"
#include "librendererheaders.h"
#include "libgeometryheaders.h"
#include "libframeworkheaders.h"

// Internal headers
#include "wtlwindowheaders.h"