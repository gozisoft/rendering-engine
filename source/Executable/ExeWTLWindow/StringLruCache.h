#pragma once

#ifndef STRING_LRU_CACHE_H
#define STRING_LRU_CACHE_H

#include "Core.h"
#include "LruCache.h"
#include <string>

_ENGINE_BEGIN

class StringLruCache : public LruCache <std::string, std::string>
{
public:
	explicit StringLruCache(size_t maxSize)
		: LruCache<std::string, std::basic_string<char>>(maxSize)
	{
	}

	size_t size_of(const std::string& key, const std::string& value) override
	{
		return value.length();
	}
};


_ENGINE_END

#endif