#pragma once

#ifndef LRUCACHE_H
#define LRUCACHE_H

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <utility>
#include <mutex>

template <class K, class V>
class LruCache
{
public:
	typedef K key_t;
	typedef V value_t;

	// Storage type inside the multi_index.
	typedef std::pair<key_t, value_t> entry_type;

	// Tags for accessing the corresponding indices of employee_set.
	struct key { /**/ };
	struct value { /**/ };

	typedef boost::multi_index::sequenced<> sequenced;
	typedef boost::multi_index::hashed_unique<
		boost::multi_index::tag<key>,
		boost::multi_index::member<entry_type, K, &entry_type::first>
	> hashed_unique;

	typedef boost::multi_index_container<
		entry_type,
		boost::multi_index::indexed_by<
		sequenced,
		hashed_unique
		>
	> map_type;

	typedef typename map_type::iterator iterator;
	typedef typename map_type::reverse_iterator reverse_iterator;
	typedef typename map_type::const_iterator const_iterator;
	typedef typename map_type::const_reverse_iterator const_reverse_iterator;
	typedef typename map_type::value_type value_type;

	typedef typename boost::multi_index::nth_index<map_type, 1>::type hashed_unique_entries;

	LruCache(size_t maxSize) : m_maxSize(maxSize), m_size(0)
	{
		/*do nothing.*/
	}

	virtual ~LruCache() { /*do nothing.*/ }

	// Caches {@code value} for {@code key}. The value is moved to the head of
	// the queue.
	// return: the previous value mapped by {@code key}.
	iterator put(const key_t& key, const value_t& value)
	{
		iterator result;

		std::unique_lock<std::mutex> lock(_mutex);
		lock.lock();
		{
			// Get the lookup by key set.
			hashed_unique_entries& key_set = m_map.get<1>();

			// The object to return.
			entry_type newEntry(key, value);

			// Find a previous entry.
			auto it = key_set.find(key);
			if (it != key_set.end()) {
				// We found a previous entry, remove it.
				if (key_set.replace(it, newEntry)) {
					m_size -= size_of(key, (*it).second);
				}
			}
			else {
				auto pair = key_set.insert(newEntry);
				auto pair_iter = pair.first;
				if (pair_iter == key_set.end())
				{
					// Log that we failed to insert our new element.
					throw std::runtime_error("Failed to insert or replace element.");
				}
				else
				{
					// Project the iterator to bi directional type.
					result = m_map.project<0>(pair_iter);
				}
			}

			// Increment the put count.
			m_putCount++;

			// Increment total size.
			m_size += size_of(key, value);
		}
		lock.unlock();

		// Keep the LruCache in size.
		trimToSize(m_maxSize);

		// Return the result
		return result;
	}

	iterator get(const K& key)
	{
		std::unique_lock<std::mutex> lock(_mutex);
		lock.lock();
		{
			// Get the lookup by key set.
			hashed_unique_entries& key_set = m_map.get<1>();

			// Get the item we want to access.
			auto iter = key_set.find(key);
			if (iter != key_set.end())
			{
				m_hitCount++;
				return m_map.project<0>(iter);;
			}
			m_missCount++;
		}
		lock.unlock();

		return end();
	}


	// Sets the size of the cache.
	// param: maxSize The new maximum size.
	void resize(size_t maxSize)
	{
		if (maxSize <= 0)
		{
			throw std::underflow_error("maxSize <= 0");
		}

		std::unique_lock<std::mutex> lock(_mutex);
		lock.lock();
		m_maxSize = maxSize;
		lock.unlock();

		// Do the resize operation.
		trimToSize(maxSize);
	}

	// Remove the eldest entries until the total of remaining entries is at or
	// below the requested size.
	// param: maxSize The new maximum size.
	void trimToSize(size_t maxSize)
	{
		std::unique_lock<std::mutex> lock(_mutex);
		while (true) {
			lock.lock();
			{
				if (m_map.empty()) {
					if (m_size != 0) {
						throw std::runtime_error("LruCache .size_of() is \
												 					 							 reporting inconsistent results!");
					}
					break;
				}

				if (m_size <= m_maxSize) {
					break;
				}

				// Get the oldest entry in the map.
				auto back_entry = m_map.back();

				// Remove the oldest item from the container.
				m_map.pop_back();

				// Update the size of the LruCache.
				m_size -= size_of(back_entry.first, back_entry.second);
				m_evictionCount++;
			}
			lock.unlock();
		}
	}

	iterator begin()
	{
		return m_map.begin();
	}

	iterator end()
	{
		return m_map.end();
	}

protected:

	/**
	* Returns the size of the entry for {@code key} and {@code value} in
	* user-defined units.  The default implementation returns 1 so that size
	* is the number of entries and max size is the maximum number of entries.
	*
	* <p>An entry's size must not change while it is in the cache.
	*/
	virtual size_t size_of(const K& key, const V& value)
	{
		UNUSED_PARAMETER(key);
		UNUSED_PARAMETER(value);
		return 1;
	}

private:

	map_type m_map;

	// Size of this cache in units. Not necessarily the number of elements.
	size_t m_size;
	size_t m_maxSize;

	size_t m_putCount;
	size_t m_createCount;
	size_t m_evictionCount;
	size_t m_hitCount;
	size_t m_missCount;

	// Internal mutex for thread saftey.
	std::mutex _mutex;
};


#endif