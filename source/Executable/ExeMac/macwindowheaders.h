#pragma once
#ifndef MAC_WINDOW_HEADERS_H
#define MAC_WINDOW_HEADERS_H

#import "macwindowfwd.h"

// Window
#import "IWindow.h"

// Events
#import "EngineEvent.h"
#import "MouseEvent.h"


#endif