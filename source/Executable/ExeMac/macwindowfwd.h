#pragma once
#ifndef MAC_WINDOW_FWD_H
#define MAC_WINDOW_FWD_H

#include "Core.h"

_ENGINE_BEGIN

class RenderWindow;
class CGameLoop;

// -------------------------------------------------------------------
// Interface
// -------------------------------------------------------------------
class IGameLooper;

// -------------------------------------------------------------------
// Effects
// -------------------------------------------------------------------
class ColourEffect;

// -------------------------------------------------------------------
// Logic
// -------------------------------------------------------------------
class IDeviceResources;
class DeviceResourcesD3D11;

class GameView;
class PlayerView;
class Application;
class ApplicationTest;


_ENGINE_END



#endif