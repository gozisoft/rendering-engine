//
// Created by Riad Gozim on 15/12/2015.
//
#pragma once

#ifndef WINDOW_H
#define WINDOW_H

#import "IWindow.h"
#import "MetalFwd.h"
#import "Vector.h"

#import <Cocoa/Cocoa.h>

_ENGINE_BEGIN

class Window: public IWindow
{
public:
    Window(shared_ptr<RendererMetal> const& renderer);
    ~Window();

    Vector4i getWindowCoords() override;

    shared_ptr<IDeviceResources> getDeviceResources() override;

private:
    NSWindow* m_window;

    // Reference to the renderer
    shared_ptr<RendererMetal> m_device;
};

_ENGINE_END


#endif //ENGINE_WINDOW_H
