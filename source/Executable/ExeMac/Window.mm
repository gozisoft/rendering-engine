//
// Created by Riad Gozim on 15/12/2015.
//

#import "macwindowafx.h"
#import "Window.h"
#import "RendererMetal.h"

using namespace engine;

Window::Window(shared_ptr<RendererMetal> const &renderer)
    : m_device(renderer)
{
    NSUInteger styleMask =
        NSWindowStyleMaskTitled |
        NSWindowStyleMaskClosable |
        NSWindowStyleMaskMiniaturizable |
        NSWindowStyleMaskResizable;

    NSRect contentRect = NSMakeRect(0, 0, 800, 600);

    m_window = [[NSWindow alloc] initWithContentRect:contentRect
                  styleMask:styleMask
                    backing:NSBackingStoreBuffered
                      defer:NO];

    if (m_window == nil) {
        throw std::runtime_error("Cocoa: Failed to create window");
    }
}

Window::~Window()
{

}
 
Vector4i Window::getWindowCoords()
{
    const NSRect rectInScreen = [m_window convertRectToScreen:[m_window frame]];
    

    
    return cml::Vector4i(rectInScreen.origin.x, rectInScreen.y, );
}

shared_ptr<IDeviceResources> Window::getDeviceResources()
{
    return shared_ptr<IDeviceResources>();
}
