//
// Created by Riad Gozim on 02/10/2015.
//
// http://stackoverflow.com/a/14083212
//

/*
 * test1.cpp
 * This program shows how to access Cocoa GUI from pure C/C++
 * and build a truly functional GUI application (although very simple).
 *
 * Compile using:
 *   g++ -framework Cocoa -o test1 test1.cpp
 *
 * that will output 'test1' binary.
 */

#import "macwindowafx.h"
#import "Window.h"
#import "RendererMetal.h"

#import <Foundation/Foundation.h>

using namespace engine;

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
    }
    
    [NSApplication sharedApplication];

    // Create our rendering device.
    auto renderer = std::make_shared<RendererMetal>();
    auto window = std::make_unique<Window>(renderer);
    
    [NSApp run];
    
    // Close the program
    exit(EXIT_SUCCESS);
}
