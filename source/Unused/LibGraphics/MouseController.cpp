#include "Frameafx.h"
#include "MouseController.h"

#include <WindowsX.h>

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CMouseController, "CMouseController", "IMouseController");


CMouseController::CMouseController(const Point2l& clientsize, HWND hwnd, bool windowed) :
m_winSize(clientsize),
m_isVisible(true),
m_hwnd(hwnd)
{
	if (m_winSize[0] != 0)
		m_invClientSize[0] = 1.0f / m_winSize[0];

	if (m_winSize[1] != 0)
		m_invClientSize[1] = 1.0f / m_winSize[1];

	UpdateBorderSize(windowed, true);
}

CMouseController::~CMouseController()
{

}

void CMouseController::SetVisible(bool visible)
{
	CURSORINFO info;
	info.cbSize = sizeof(CURSORINFO);
	BOOL gotCursorInfo = GetCursorInfo(&info);

	while (gotCursorInfo)
	{
		if ( (info.flags == CURSOR_SHOWING && visible) ||
			 (info.flags == 0 && !visible) )
		{
			// cursor is already visible or hidden
			break;
		}
		int showResult = ShowCursor(visible); 
		if ( showResult < 0 )
		{
			break;
		}

		info.cbSize = sizeof(CURSORINFO); // yes, it really must be set each time
		gotCursorInfo = GetCursorInfo(&info);
	}
	
	m_isVisible = visible;
}

void CMouseController::SetMousePos(const Point2l& pos)
{
	RECT rect;
	if ( GetWindowRect(m_hwnd, &rect) )
		SetCursorPos( pos[0] + rect.left + m_borderX, pos[1] + rect.top + m_borderY);

	m_cursorPos = pos;
}

void CMouseController::OnResize(const Point2l& size)
{
	// Set the basic window size
	m_winSize = size;

	// Set the inverse window size
	if (size[0] != 0)
		m_invClientSize[0] = 1.0f / size[0];
	else
		m_invClientSize[0] = 0.0f;

	if (size[1] != 0)
		m_invClientSize[1] = 1.0f / size[1];
	else
		m_invClientSize[1] = 0.0f;
}

void CMouseController::UpdateBorderSize(bool windowed, bool resizable)
{
	if (windowed)
	{
		if (resizable)
		{
			m_borderX = GetSystemMetrics(SM_CXSIZEFRAME);
			m_borderY = GetSystemMetrics(SM_CYCAPTION) + GetSystemMetrics(SM_CYSIZEFRAME);
		}
		else // For dialog boxes
		{
			m_borderX = GetSystemMetrics(SM_CXDLGFRAME);
			m_borderY = GetSystemMetrics(SM_CYCAPTION) + GetSystemMetrics(SM_CYDLGFRAME);
		}
	}
	else
	{
		m_borderX = 0;
		m_borderY = 0;
	}
}

void CMouseController::UpdateCursorPos()
{
	POINT p;
	if (!GetCursorPos(&p))
	{
		DWORD xy = GetMessagePos();
		p.x = GET_X_LPARAM(xy); // the x-coordinate is in the low-order short
		p.y = GET_Y_LPARAM(xy); // the y-coordinate is in the high-order short		
	}

	RECT rc;
	if (GetWindowRect(m_hwnd, &rc)) // Get the rect from the active window
	{
		m_cursorPos[0] = p.x - rc.left - m_borderX;
		m_cursorPos[1] = p.y - rc.top - m_borderY;
	}
	else // no window around
	{
		m_cursorPos[0] = -1;
		m_cursorPos[1] = -1;
	}
}