#ifndef __EFFECT_VARIABLE_CREATE_H__
#define __EFFECT_VARIABLE_CREATE_H__

#include "framefwd.h"

_ENGINE_BEGIN

namespace
{
	typedef CVariable* (*VarCreationPtr)(const CShaderDesc* desc);
	typedef std::map<CRTTI, VarCreationPtr> CreationMap;

	VarCreationPtr Lookup(const CRTTI& rtti);

	CreationMap* IntialiseMap();
	CVariable* CreateShaderTextureVar(const CShaderDesc* desc);
	CVariable* CreateShaderBufferVar(const CShaderDesc* desc);



};

CVariable* CreateVariable(const CShaderDesc* desc);
CVariable* CreateConstant(const CConstantFormat* constFormat);

_ENGINE_END


#endif