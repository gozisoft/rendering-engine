#ifndef CFRAMEWORK_H
#define CFRAMEWORK_H

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#undef WIN32_LEAN_AND_MEAN

#include "MathsFwd.h"
#include "framefwd.h"
#include "IFramework.h"

_ENGINE_BEGIN

class CFramework : public IFramework
{
public:
	CFramework( IBaseAppPtr pGameApp,
		const String& title,
		bool windowed,
		HINSTANCE hInstance,
		const Point2l& windowedSize=Point2l(512,512),
		const Point2l& fullSize=cml::zero_2D() );

	~CFramework();

	bool Initialise();
	void Release();
	int Run(void* hAccel);

	// Accessors
	HWND GetHWND() const;
	void SetHWND(HWND hwnd);
	HINSTANCE GetHINSTANCE() const;

	// Window messege handler
	static LRESULT CALLBACK StaticWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

private:
	LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	void OnCreateDevice();
	void OnResetDevice();
	void OnLostDevice();
	void OnDestroyDevice();
	void OnUpdateFrame();
	void OnRenderFrame();
	void OnWindowEvent(const CWinEvent& event);

	void Pause(bool rendering, bool timer);
	void CheckForWindowSizeChange();
	void CheckForWindowChangingMonitors();
	void ToggleFullScreen();

	HWND m_hwnd;
	HINSTANCE m_hInstance;
	WINDOWPLACEMENT m_wp;

	// Window Details
	Point2l m_clientSize;
	Point2l m_winSize;
	Point2l m_fullSize;
	String m_title;
	bool m_active;
	bool m_windowed;
	bool m_minimised;
	bool m_maximised;
	bool m_borderDrag;
	bool m_closing;

	// Timing
	bool m_renderingPaused;
	bool m_timerPaused;
	Int m_renderingPauseCount;
	Int m_timerPauseCount;

	// Window owns these
	CTimerPtr m_pTimer;
	CMouseControllerPtr m_pMouse;
	IRendererPtr m_pRenderer;

	// Window does not own these
	IBaseAppPtr m_pGameApp;

private:
	DECLARE_HEAP;

};


#include "Framework.inl"



_ENGINE_END

#endif