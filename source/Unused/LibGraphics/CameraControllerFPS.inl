#ifndef CCAMERA_CONTROLLER_FPS_INL
#define CCAMERA_CONTROLLER_FPS_INL

inline float CCameraControllerFPS::GetMoveSpeed() const
{
	return m_moveSpeed;
}

inline void CCameraControllerFPS::SetMoveSpeed(float speed)
{
	m_moveSpeed = speed;
}

inline float CCameraControllerFPS::GetRotateSpeed() const
{
	return m_rotateSpeed;
}

inline void CCameraControllerFPS::SetRotateSpeed(float speed)
{
	m_rotateSpeed = speed;
}

inline void CCameraControllerFPS::setVerticalMovement(bool allow)
{
	m_vertMovement = allow;
}

inline void CCameraControllerFPS::SetInverseMouse(bool invert)
{
	m_mouseYDirection = invert ? -1.0f : 1.0f;
}

inline void CCameraControllerFPS::SetKeyMap(const KeyMapVector& keyMap)
{
	m_keyMap = keyMap;
}

inline void CCameraControllerFPS::SetAllKeysUp()
{
	for (UInt32 i = 0; i < KA_COUNT; ++i)
		m_cursorkeys[i] = false;
}

inline void CCameraControllerFPS::SetAllButtonsUp()
{
	for (UInt32 i = 0; i < MA_COUNT; ++i)
		m_mouseButtons[i] = false;
}

/*
inline SceneControllerType CCameraControllerFPS::GetType() const
{
	return SCT_CAMERA_FPS;
}
*/



#endif