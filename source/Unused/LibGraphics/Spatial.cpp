#include "frameafx.h"
#include "Spatial.h"

using namespace Engine;

CSpatial::CSpatial()
{

}

CSpatial::~CSpatial()
{

}


void CSpatial::UpdateFromRoot()
{
	if (m_parent)
	{
        m_parent->UpdateWorldBound();
        m_parent->UpdateFromRoot();
	}
}