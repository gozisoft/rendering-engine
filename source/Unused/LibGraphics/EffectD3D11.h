#ifndef __CEFFECT_D3D11_H__
#define __CEFFECT_D3D11_H__

#include "D3D11Fwd.h"
#include "ShaderD3D11.h"
#include "Effect.h"

#include "loki\AssocVector.h"

_ENGINE_BEGIN

namespace {
	// Tags
	struct random_tag { };
	struct name_tag { };
	struct type_tag { };

	// Typedefs
	typedef boost::multi_index::random_access< 
		boost::multi_index::tag<random_tag>
	> RandomIndex;

	typedef boost::multi_index::hashed_unique<
		boost::multi_index::tag<name_tag>,
		boost::multi_index::const_mem_fun<CVariable, const std::string&, &CVariable::GetVariableName>
	> NameIndex;

	typedef boost::multi_index::hashed_non_unique<
		boost::multi_index::tag<type_tag>,
		boost::multi_index::const_mem_fun<CVariable, Shader::ClassType, &CVariable::GetClassType>
	> ClassTypeIndex; 

	typedef boost::multi_index::multi_index_container <
		CVariable*,
		boost::multi_index::indexed_by <
		RandomIndex, 
		NameIndex,
		ClassTypeIndex
		> // index_by
	> GlobalVariables;

	typedef GlobalVariables::index<random_tag>::type variable_random;
	typedef GlobalVariables::index<name_tag>::type variable_by_name;
	typedef GlobalVariables::index<type_tag>::type variable_by_type;
}

class CShaderBlockD3D11 : public IShaderBlock
{
public:
	CShaderBlockD3D11(ID3D11DeviceChildPtr pShaderD3D, SD3DShaderVTable* vtable);
	~CShaderBlockD3D11();

	void NotifyDirty(IResourceProxy* proxy);

	void AddVariable(CConstantBuffer* buffer);

	void AddView();

private:
	typedef Loki::AssocVector<CConstantBuffer*, ID3D11Resource*> ConstantsMap;

	// Associative vector for constants
	ConstantsMap m_constantsMap;

	// Constant buffers
	std::vector<ID3D11Resource*> constants;

	// Texture views, buffer views, etc
	std::vector<ID3D11View*> views;

	// D3D shader pointer (don't need to do look up)
	ID3D11DeviceChildPtr pShaderD3D;

	// Pointer to the shader table
	SD3DShaderVTable* pVTable;
};

typedef std::tr1::tuple<ShaderMap, ResourceMap, ViewMap> EffectResTuple;

// CEffectD3D11 is a class that mirrors the CEffect class. It contains D3D
// specific resources in that are stored within CRenderResources class.
class CEffectD3D11 : public IEffect
{
public:
	// Constructor
	CEffectD3D11(const CEffect* pEffect, EffectResTuple& eTuple, 
		ID3D11DevicePtr device);

	~CEffectD3D11();

	// Access varibles by name
	CVariable* GetVariable(const std::string& name) const;

	// Access variable by index
	CVariable* GetVariable(size_t index) const;

	// Function to add a variable
	void AddVariable(CVariable* variable);

	// This will enable the effect at the direct3d rendering level.
	void Enable(CEffect* pEffect, ID3D11DeviceContextPtr& context);

	// This is used to apply specific shader variables.
	// It is effeciently mapped between engine types and Direct3D types.
	void ApplyShader(CShaderBlockD3D11* pShader, ID3D11DeviceContextPtr& context);

protected:
	// Function for mapping shaders to shader specific resources
	void SortVariables(const CShader* shader, ID3D11DevicePtr device);

	// Container of unique variables
	GlobalVariables m_variables;

	// Map of D3DShaderblocks
	std::map<CShader*, CShaderBlockD3D11*> m_shaderBlocks;

	// Container of unique resource proxy's
	std::map<std::string, IResourceProxy*> m_proxies;

	// Container of unique constants ownded by effect
	std::vector<CConstantBuffer*> m_uniqueConstants;

	// D3DDevice context
	ID3D11DeviceContextPtr m_context;

	// Shared resources (resource management from Renderer)
	ShaderMap& m_shaderMap;
	ResourceMap& m_resources;
	ViewMap& m_viewMap;

};

_ENGINE_END

#endif













//template < class EngineType, class D3DType >
//class CResDependency
//{
//public:
//	typedef EngineType engine_type;
//	typedef D3DType d3d_type;
//	typedef std::pair<engine_type, d3d_type> pair_type;
//
//public:
//	CResDependency() : 
//	pEngineRes(nullptr), 
//	pD3DRes(nullptr),
//	numResources(0),
//	numReserves(0)
//	{
//
//	}
//
//	void SetSize(size_t newSize)
//	{
//		// First reserve the memory slots
//		Reserve(newSize);
//
//		// Store the old size
//		size_t oldSize = numResources;
//
//		// if the number of resources is less than the new request
//		// add new elements.
//		if (numResources < newSize)
//		{
//			if (pEngineRes && pD3DRes)
//			{
//				// Adding elements. Call ctor.
//				for (size_t i = oldSize; i < newSize; ++i)
//				{
//					::new ( &pEngineRes[i] ) engine_type;
//					::new ( &pD3DRes[i] ) D3DType;
//				}
//			}
//		}
//	}
//
//	void Push_Back(const engine_type& engineRes, const D3DType& d3dRes)
//	{
//		// Reserve an extra slot
//		Reserve(numResources+1); 
//
//		// Construct the new element
//		::new ( &pEngineRes[numResources] ) engine_type;
//		::new ( &pD3DRes[numResources] ) D3DType;
//
//		// Assign
//		pEngineRes[numResources] = engineRes;
//		pD3DResp[numResources] = d3dRes;
//
//		// Increment the size
//		++numResources;
//	}
//
//	// Increase the number of mem slots
//	bool Reserve(size_t newSize)
//	{		
//		if (numReserves == newSize)
//			return true;
//
//		// Shrink to 0 size & cleanup
//		if (!newSize)
//		{		
//			if(pEngineRes && pD3DRes)
//			{
//				std::free(pEngineRes);
//				std::free(pD3DRes);
//			}
//
//			maxSize = 0;
//			size = 0;
//		}
//
//		// Increase reserves
//		if ( (!pEngineRes && !pD3DRes) || (newSize > maxSize) )
//		{
//			// Verify that (newSize * sizeof(TYPE)) is not greater than UINT_MAX or the realloc will overrun
//			if( (sizeof(engine_type) > UINT_MAX / newSiz) || (sizeof(d3d_type) > UINT_MAX / newSize) )
//				return false;
//
//			// Reallocate the memory for the resources
//			engine_type* newEngRes = std::realloc( pEngineRes, numSlots * sizeof(engine_type) );
//			d3d_type* newD3DRes = std::realloc( pD3DRes, numSlots * sizeof(d3d_type) );		
//			if (newEngRes == nullptr || newD3DRes == nullptr)
//			{
//				// Out of memory!!
//				assert( false && ieS("Out of memory!") );
//				return false;
//			}
//
//			pEngineRes = newEngRes;
//			pD3DRes = newD3DRes;
//		}
//	}
//
//	std::pair<engine_type, d3d_type> At(size_t index)
//	{
//		assert(index < size);
//		return std::make_pair(pEngineRes[index], pD3DRes[index]);
//	}
//
//	size_t Size() const
//	{
//		return size;
//	}
//
//	size_t Count() const
//	{
//		return maxSize;
//	}
//
//	engine_type* pEngineRes;
//	d3d_type* pD3DRes;
//
//private:
//	size_t size;
//	size_t maxSize;
//};