#ifndef IGAMEAPP_H
#define IGAMEAPP_H

#include "framefwd.h"

_ENGINE_BEGIN

// ------------------------------------------------------------------------------------
// Summary: Interface that the main game application must implement
// ------------------------------------------------------------------------------------
class IBaseApp
{
public:
    virtual ~IBaseApp() {}

	virtual void OnCreateDevice(IRendererPtr pRenderer, IMouseControllerPtr pMouse) = 0;
    virtual void OnResetDevice(IRendererPtr pRenderer) = 0;
    virtual void OnLostDevice() = 0;
    virtual void OnDestroyDevice() = 0;
    virtual void OnUpdateFrame(double time, double deltaTime) = 0;
    virtual void OnRenderFrame(IRendererPtr pRenderer, double time, double deltaTime) = 0;
	virtual void OnWindowEvent(const CWinEvent& event) = 0;
};

_ENGINE_END

#endif