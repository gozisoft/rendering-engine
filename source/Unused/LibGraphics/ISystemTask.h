#ifndef __ISYSTEM_H__
#define __ISYSTEM_H__

#include "framefwd.h"

_ENGINE_BEGIN

////////////////////////////////////////////////////////////////////////////////////////////////////
// ISystemTask is an interface class designed to work with a task manager for starting the
// system's task and spawning off new tasks as need be.
////////////////////////////////////////////////////////////////////////////////////////////////////

class ISystemTask
{
    //friend ISystem;

public:

	/// <summary cref="ISystemTask::ISystemTask(ISystemScene*)">
    ///   Constructor.
    /// </summary>
    ISystemTask( ISystemScene* pSystemScene )
        : m_pSystemScene( pSystemScene )
    {
    }

    /// <summary cref="ISystemTask::GetSystemScene">
    ///   Gets the scene for this task.
    /// </summary>
    /// <returns>The scene for this task.</returns>
	ISystemScene* GetSystemScene( void )
    {
        return m_pSystemScene;
    }

    /// <summary cref="ISystemTask::GetSystemType">
    ///   Gets the system type for this system task.
    /// </summary>
    /// <remarks>
    ///   This is a shortcut to getting the system type without having to go the system first.
    /// </remarks>
    /// <returns>The type of the system.</returns>
    virtual System::Type GetSystemType( void ) = 0;

	/// <summary cref="ISystemTask::Update">
    ///   Function informing the task to perform its updates.
    /// </summary>
    /// <param name="DeltaTime">The time delta from the last call.</param>
    virtual void Update( f32 DeltaTime ) = 0;

    /// <summary cref="ISystemTask::IsPrimaryThreadOnly">
	///   Implementing tasks should return True to indicate that their <c>Update</c> function
	///   should only be called from the primary thread.  False allows their <c>Update</c> function
    ///   to be called from an arbitrary thread.
    /// </summary>
    virtual Bool IsPrimaryThreadOnly( void ) = 0;

protected:

	ISystemScene*               m_pSystemScene;
};

_ENGINE_END

#endif