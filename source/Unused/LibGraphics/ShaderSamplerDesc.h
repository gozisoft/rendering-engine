#ifndef __CSAMPLER_STATE_H__
#define __CSAMPLER_STATE_H__

#include "ShaderVariableDesc.h"
#include "MathsFwd.h"

_ENGINE_BEGIN

class CShaderSampler : public CShaderDesc
{
	// Run time type info
	RTTI_DECL;

public:
    // The sampler type for interpreting the texture assigned to the sampler.
    enum SamplerType
    {
        ST_UNKNOWN = ~0,
        ST_1D,
        ST_2D,
        ST_3D,
        ST_CUBE,
        ST_QUANTITY
    };

    // Filtering modes for the samplers.
    enum SamplerFilter
    {
        SF_UNKNOWN = ~0,
        SF_NEAREST,          // nearest neighbor (default)
        SF_LINEAR,           // linear filtering
        SF_NEAREST_NEAREST,  // nearest within image, nearest across images
        SF_NEAREST_LINEAR,   // nearest within image, linear across images
        SF_LINEAR_NEAREST,   // linear within image, nearest across images
        SF_LINEAR_LINEAR,    // linear within image, linear across images
        SF_QUANTITY
    };

    // Texture coordinate modes for the samplers.
    enum SamplerCoordinate
    {
        SC_UNKNOWN,
        SC_CLAMP,
        SC_REPEAT,
        SC_MIRRORED_REPEAT,
        SC_CLAMP_BORDER,
        SC_CLAMP_EDGE,  // default
        SC_QUANTITY
    };

	// Default ctor
	CShaderSampler(const std::string& name, size_t bindPoint=0, size_t bindCount=0);

	// Data members set by application
	SamplerType m_samplerType;
	SamplerFilter m_filter;
	SamplerCoordinate m_coordinates[3];
    float m_lodBias;
    float m_anisotropy;
	Colour4f m_borderColor;
};


_ENGINE_END

#endif