#ifndef __CVISUAL_PASS_H__
#define __CVISUAL_PASS_H__

_ENGINE_BEGIN

class CVisualPass
{
public:
	// Constructor, requires a builder class. Obtains ownership
	// of the data members of the builder class.
	CVisualPass(const CVisualPassBuilder& builder);

	// Destructor destroys the owned data members.
	~CVisualPass();

	// Data Accessors (read only)
	const char* GetPassName() const;
	const CVertexShader* GetVertexShader() const;
	const CPixelShader* GetPixelShader() const;
	const CAlphaState* GetAlphaState() const;
	const CCullState* GetCullState() const;
	const CDepthState* GetDepthState() const;
	const COffsetState* GetOffsetState() const;
	const CStencilState* GetStencilState() const;
	const CWireState* GetWireState() const;
	const CZBufferState* GetZBufferState() const;

private:
	std::string m_passName;
	CVertexShader* m_pVertexShader;
	CPixelShader* m_pPixelShader;
	CAlphaState* m_pAlphaState;
	CCullState* m_pCullState;
	CDepthState* m_pDepthState;
	COffsetState* m_pOffsetState;
	CStencilState* m_pStencilState;
	CWireState* m_pWireState;
	CZBufferState* m_pZBufferState;
};


_ENGINE_END

#endif