#ifndef __CSHADER_VARIABLE_H__
#define __CSHADER_VARIABLE_H__

#include "framefwd.h"
#include "ShaderTypes.h"
#include "RTTI.h"

_ENGINE_BEGIN

class CShaderDesc
{
	RTTI_DECL; // Run time type infos

public:
	CShaderDesc(const std::string& name, Shader::ClassType classType,
		size_t bindPoint, size_t bindCount);

	virtual ~CShaderDesc();

	const std::string& GetVariableName() const;
	Shader::ClassType GetClassType() const;
	size_t GetBindPoint() const;
	size_t GetBindCount() const;

private:
	std::string m_resourceName;				// Name of the resource (e.g. m_worldViewProj)
	Shader::ClassType m_classType;			// Type of resource (e.g. texture, cbuffer, etc.)
	size_t m_bindPoint;						// Starting bind point
	size_t m_bindCount;						// Number of contiguous bind points (for arrays)
};

#include "ShaderVariableDesc.inl"

_ENGINE_END

#endif