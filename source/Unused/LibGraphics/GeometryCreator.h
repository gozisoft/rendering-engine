#ifndef CGEOMETRY_CREATOR_H
#define CGEOMETRY_CREATOR_H

#include "FrameFwd.h"
#include "Buffer.h"
#include "Sphere.h"

namespace Engine	
{

	class CGeometry
	{
	public:
		CGeometry(CFVFormatPtr pVFormat, bool insideNormals = false, CBuffer::BufferUse usage = CBuffer::RA_STATIC);

		// Create a 3D box
		CTriMeshPtr Box(float xExtent, float yExtent, float zExtent);
		// Create a single Triangle
		CTriMeshPtr Triangle(float xExtent, float yExtent, float zExtent);
		// Createa a plane
		CTriMeshPtr Plane(float cellSpacing, UInt rows, UInt cols);

		//// Create a disk
		//CTriMeshPtr Disk(UInt32 shellSamples, UInt32 radialSamples, float radius);
		//// Create a sphere
		//CTriMeshPtr Sphere(UInt32 zSamples, UInt32 radialSamples, float radius);
		//// Create a legend of a Torus
		//CTriMeshPtr Torus(UInt32 circleSamples, UInt32 radialSamples, float outerRadius, float innerRadius);

		// Used for calculating radius of a bounding sphere from geometric data
		void ComputeBoundingSphere(const UInt8* pData, UInt numVertices, CSphere& sphere);

	private:
		CFVFormatPtr m_pVFormat;
		CBuffer::BufferUse m_usage;

		bool m_insideNormals;
		bool m_hasNormals;
		bool m_hasTCoords;
		bool m_hasColours;
	};


}


#endif