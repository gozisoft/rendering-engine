#ifndef __CCULLER_H__
#define __CCULLER_H__

#include "framefwd.h"
#include "Camera.h"
#include "Plane.h"

_ENGINE_BEGIN

class CCuller
{
public:
	typedef std::vector<CVisualNode*> SceneNodes;

	// Constructor and destructor
	CCuller(CCameraPtr& camera);
	~CCuller();

	// Camera access, set frustrum etc
	void SetCamera(CCameraPtr& pCamera); 
	const CCameraPtr& GetCamera() const;

	// Frustrum functions
	void SetFrustum(const float* frustum);
	void SetFrustum(const Matrix4f& projView);
	const float* GetFrustum() const;

	// Compute visibility
	bool IsVisible(const CSphere& volume);
	void ComputeVisibleSet(const CRootNodePtr& pRootNode);

	// Access to the visible array
	void InsertVisible(CVisualNode* visual);
	const SceneNodes& GetVisibleArray() const;
	size_t GetVisibleCount() const;

	// Access to the frustrum planes o
	UInt8 GetPlaneStates() const;
	void SetPlaneStates(UInt8 states);

private:
	// Vector of all the objects calcluated to be within
	// the frustum. These will be rendered.
	SceneNodes m_visibleSet;

	// Pointer to the camera.
	CCameraPtr m_pCamera;

	// Updated frustum per frame.
	float m_frustum[CCamera::NUM_PLANES];

	// Planes calculated from the eular vectors of camera and frustum.
	CPlane m_plane[CCamera::NUM_PLANES];

	// Bitset - active planes.
	UInt8 m_planeStates;
};


#include "Culler.inl"


_ENGINE_END




#endif