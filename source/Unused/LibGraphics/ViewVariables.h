#ifndef __EFFECT_TEXTURE_VARIABLE_H__
#define __EFFECT_TEXTURE_VARIABLE_H__

#include "BaseVariable.h"

_ENGINE_BEGIN

//////////////////////////////////////////////////////////////////////////
// texture_variable - implements raw set/get functionality
//////////////////////////////////////////////////////////////////////////
class texture_variable : public CVariable
{
public:
	// default constructor
	texture_variable(CTextureView* view, IResourceProxy* proxy);
	~texture_variable();

	// Const Access the format descriptor
	const CShaderTextureDesc* GetDesc() const
	{
		return boost::get<CShaderTextureDesc*>(m_desc);
	}

	// Const Access the resource view
	const CTextureView* GetTexView() const
	{
		return boost::get<CTextureView*>(m_resource);
	}

	// Non-Const Access the format descriptor
	CShaderTextureDesc* GetDesc() 	
	{
		return const_cast<CShaderTextureDesc*>( static_cast<const texture_variable&>(*this).GetDesc() );
	}

	// Non-Const Access the constant buffer
	CTextureView* GetTexView()
	{
		return const_cast<CTextureView*>( static_cast<const texture_variable&>(*this).GetTexView() );
	}

	// Function to set the texture
	void SetView(CTextureView* textureView)
	{
		m_resource = textureView;
	}

};

//////////////////////////////////////////////////////////////////////////
// buffer_variable - implements raw set/get functionality
//////////////////////////////////////////////////////////////////////////
class buffer_variable : public CVariable
{
public:
	// default constructor
	buffer_variable(CBufferView* view, IResourceProxy* proxy);
	~buffer_variable();

	// Const Access the format descriptor
	const CShaderBufferDesc* GetDesc() const
	{
		return boost::get<CShaderBufferDesc*>(m_desc);
	}

	// Const Access the resource view
	const CBufferView* GetBufferView() const
	{
		return boost::get<CBufferView*>(m_resource);
	}

	// Non-Const Access the format descriptor
	CShaderBufferDesc* GetDesc() 	
	{
		return const_cast<CShaderBufferDesc*>( static_cast<const buffer_variable&>(*this).GetDesc() );
	}

	// Non-Const Access the constant buffer
	CBufferView* GetBufferView()
	{
		return const_cast<CBufferView*>( static_cast<const buffer_variable&>(*this).GetBufferView() );
	}

	// Function to set the texture
	void SetView(CBufferView* bufferView)
	{
		m_resource = bufferView;
	}

};



_ENGINE_END

#endif