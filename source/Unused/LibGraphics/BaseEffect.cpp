#include "frameafx.h"
#include "BaseEffect.h"
#include "Helper.h"

using namespace Engine;

void CBaseEffect::AddTechnique(const std::string& techName, const SVisualPass** passes,
	size_t numPasses)
{
	CTechnique technique( techName, VisualPassArray(passes, passes+numPasses) );
	m_techniques.push_back(technique);
}

void CBaseEffect::AddTechnique(const std::string& techName,
	const VisualPassArray& passes)
{
	m_techniques.push_back( CTechnique(techName, passes) );
}

const CTechnique& CBaseEffect::GetTechnique(const std::string& techName) const
{
	const tech_by_name& nameIndex = m_techniques.get<name_tag>();
	auto itor = nameIndex.find(techName);
	assert( itor != nameIndex.end() );	
	return (*itor);
}
