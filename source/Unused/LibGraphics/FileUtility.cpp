#include "frameafx.h"
#include "FileUtility.h"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#undef WIN32_LEAN_AND_MEAN

#include <tchar.h>

_ENGINE_BEGIN

// Summary: Searches through folders to find a media file. Adapted from the DirectX Sample Framework.
// Parameters:
// [in] file - Name of the file we're looking for.</param>
// Returns: The path of the file or NULL if the file wasn't found
String CFileUtility::GetMediaFile(const String& file)
{
    Char exeName[MAX_PATH] = {0};
    Char exeFolder[MAX_PATH] = {0};

    // Get full executable path
    GetModuleFileName( NULL, exeFolder, MAX_PATH );
    exeFolder[MAX_PATH - 1] = 0;

    // Get pointer to beginning of executable file name
    // which is after the last slash
    Char* pCutPoint = NULL;
    for ( int i = 0; i < MAX_PATH; ++i )
    {
        if ( exeFolder[i] == TEXT('\\') )
        {
            pCutPoint = &exeFolder[i + 1];
        }
    }

    if ( pCutPoint )
    {
        // Copy over the exe file name
        _tcscpy_s( exeName, pCutPoint );

        // Chop off the exe file name from the path so we
        // just have the exe directory
        *pCutPoint = 0;

        // Get pointer to start of the .exe extension 
        pCutPoint = NULL;
        for ( int i = 0; i < MAX_PATH; i++ )
        {
            if ( exeName[i] == TEXT('.') )
            {
                pCutPoint = &exeName[i];
            }
        }

        // Chop the .exe extension from the exe name
        if ( pCutPoint )
        {
            *pCutPoint = 0;
        }

        // Add a slash
        _tcscat_s( exeName, TEXT("\\") );
    }

    // Search all the folders in searchFolders
	String path = SearchFolders( file, exeFolder, exeName );
	if ( !path.empty()  )
    {
        return path;
    }

    // Search all the folders in searchFolders with \media\ appended to the end
    Char mediaFile[MAX_PATH] = TEXT("media\\%s");
    _tcscat_s( mediaFile, &file[0] );
	path = SearchFolders(mediaFile, exeFolder, exeName);
	if ( !path.empty()  )
    {
        return path;
    }

	OutputDebugString( TEXT("Can not find the specified file.") );
    return String();
}


// Summary: Searches through folders to find a media file. Adapted from the DirectX Sample Framework.
// Parameters:
// [in] filename - File we are looking for
// [in] exeFolder - Folder of the executable
// [in] exeName - Name of the executable
// [in] fullPath - Returned path if file is found.
// Returns: true if the file was found, false otherwise.
String CFileUtility::SearchFolders(const String& filename, Char* exeFolder, Char* exeName)
{
    // Typical directories:
    //      .\
    //      ..\
    //      ..\..\
    //      %EXE_DIR%\
    //      %EXE_DIR%\..\
    //      %EXE_DIR%\..\..\
    //      %EXE_DIR%\..\%EXE_NAME%
    //      %EXE_DIR%\..\..\%EXE_NAME%

    Char* searchFolders[] = 
    { 
        TEXT(".\\"),
		TEXT("..\\"),
		TEXT("..\\..\\"),
		TEXT("%s\\"),
		TEXT("%s..\\"),
		TEXT("%s..\\..\\"),
		TEXT("%s..\\%s"),
		TEXT("%s..\\..\\%s")
    };

    // Look through each folder to find the file
    Char currentPath[MAX_PATH] = {0};
    for (UINT i = 0; i < ARRAYSIZE(searchFolders); ++i)
    {
        _stprintf_s(currentPath, searchFolders[i], exeFolder, exeName);
        _tcscat_s(currentPath, &filename[0]);
        if (GetFileAttributes( currentPath ) != INVALID_FILE_ATTRIBUTES)
        {
			String fullPath = currentPath;
            return fullPath;
        }
    }

    // Crap...didn't find it
    return String();
}

String FindMediaFile(const Char* fileName, UInt pathSize)
{
	if ( !fileName || fileName[0] == 0 || pathSize < 10 )
	{
		assert( false && ieS("Bad inputs used in FindMediaFile()") );
		return 0;
	}

	// Get the exe name, and exe path
	Char exePath[MAX_PATH] = { 0 };
	Char exeName[MAX_PATH] = { 0 };

	// Get dir of where exe is running from
	GetModuleFileName(0, exePath, MAX_PATH);
	exePath[MAX_PATH - 1] = TEXT('\0'); // Add null terminating character

	// Locate last occurrence of 'slash' character in string
	Char* lastSlash = _tcsrchr( exePath, TEXT( '\\' ) );
	if (lastSlash)
	{
		_tcscpy_s( exeName, MAX_PATH, &lastSlash[1] );

		// Chop the exe name from the exe path
		*lastSlash = 0;

		// Chop the .exe from the exe name
		lastSlash = _tcsrchr( exeName, TEXT('.') );
		if(lastSlash)
			*lastSlash = 0;
	}

	// Typical directory search
	String& dest = FindMediaSearchTypicalDirs(fileName, exePath, exeName, pathSize);
	if ( !dest.empty() )
	{
		return dest;
	}

	// Typical directory search again, but also look in a subdir called "\media\"
	Char searchFor[MAX_PATH];
	_stprintf_s(searchFor, MAX_PATH, TEXT("media\\%s"), fileName);
	dest = FindMediaSearchTypicalDirs(searchFor, exePath, exeName, pathSize);
	if ( !dest.empty() )
	{
		return dest;
	}

	// Search all parent directories starting at .\ and using strFilename as the leaf name
	Char leafName[MAX_PATH] = { 0 };
	_tcscpy_s(leafName, MAX_PATH, fileName);
	dest = FindMediaSearchParentDirs(TEXT("."), leafName, pathSize);
	if ( !dest.empty() )
	{
		return dest;
	}

	// Search all parent directories starting at the exe's dir and using strFilename as the leaf name
	dest = FindMediaSearchParentDirs(exePath, leafName, pathSize);
	if ( !dest.empty() )
	{
		return dest;
	}

	// Search all parent directories starting at .\ and using "media\strFilename" as the leaf name
	_stprintf_s(leafName, MAX_PATH, TEXT("media\\%s"), fileName);	
	dest = FindMediaSearchParentDirs(TEXT("."), leafName, pathSize);
	if ( !dest.empty() )
	{
		return dest;
	}

	// Search all parent directories starting at the exe's dir and using "media\strFilename" as the leaf name
	dest = FindMediaSearchParentDirs(exePath, leafName, pathSize);
	if ( !dest.empty() )
	{
		return dest;
	}

	// On failure return 0
	return String();
}

String FindMediaSearchTypicalDirs(const Char* leafName, const Char* exePath, const Char* exeName, UInt pathSize)
{
    // Typical directories:
    //      .\
    //      ..\
    //      ..\..\
    //      ..\..\..\
    //      %EXE_DIR%\
    //      %EXE_DIR%\..\
    //      %EXE_DIR%\..\..\
    //      %EXE_DIR%\..\..\..\
    //      %EXE_DIR%\..\%EXE_NAME%
    //      %EXE_DIR%\..\..\%EXE_NAME%
	//      %EXE_DIR%\..\..\..\%EXE_NAME%

    // Search in .\  
	String searchPath(leafName);
    if( GetFileAttributes(leafName) != INVALID_FILE_ATTRIBUTES )
        return searchPath;

	// Didnt find it clear the string
	searchPath.clear();	

    Char* leafFolders[] = 
    { 
        TEXT("..\\"),
		TEXT("..\\..\\"),
		TEXT("..\\..\\"),
		TEXT("..\\..\\..\\"),
    };

    // Look through each leaf folder to find the file
    for (UINT i = 0; i < ARRAYSIZE(leafFolders); ++i)
    {
		OStringStream oss;
		oss << leafFolders[i] << leafName;
		const String& tempPath = oss.str();
		if (GetFileAttributes( tempPath.c_str() ) != INVALID_FILE_ATTRIBUTES)
			return tempPath;
    }

    Char* exepathFolders[] = 
    { 
		TEXT("\\"),
		TEXT("\\..\\"),
		TEXT("\\..\\..\\"),
		TEXT("\\..\\..\\..\\"),
    };

    // Look through exePath leaf folder to find the file
    for (UINT i = 0; i < ARRAYSIZE(exepathFolders); ++i)
    {
		OStringStream oss;
		oss << exePath << exepathFolders[i] << leafName;
		const String& tempPath = oss.str();
		if (GetFileAttributes( tempPath.c_str() ) != INVALID_FILE_ATTRIBUTES)
			return tempPath;
    }

    Char* exenameFolders[] = // This matches the DirectX SDK layout
    { 
		TEXT("\\..\\"),
		TEXT("\\..\\..\\"),
		TEXT("\\..\\..\\..\\")
    };

    // Look through exePath leaf folder to find the file
    for (UINT i = 0; i < ARRAYSIZE(exenameFolders); ++i)
    {
		OStringStream oss;
		oss <<  exePath << exenameFolders[i] << exeName << TEXT("\\") << leafName;
		const String& tempPath = oss.str();
		if (GetFileAttributes( tempPath.c_str() ) != INVALID_FILE_ATTRIBUTES)
			return tempPath;
    }

	// Failed to find the file in typical dirs
    return String();
}

String FindMediaSearchParentDirs(const Char* startAt, const Char* leafName, UInt pathSize)
{
    TCHAR strFullPath[MAX_PATH] = { 0 };
    TCHAR strFullFileName[MAX_PATH] = { 0 };
    TCHAR* strFilePart = 0;

    GetFullPathName(startAt, MAX_PATH, strFullPath, &strFilePart);
    if(strFilePart == NULL)
        return String();

    while( strFilePart != NULL && *strFilePart != TEXT('\0') )
    {
		OStringStream searchPath;
		searchPath << TEXT("\\") << strFullPath << leafName;
		const String& tempPath = searchPath.str();

		if( GetFileAttributes( tempPath.c_str() ) != INVALID_FILE_ATTRIBUTES )
        {
            return tempPath;
        }

		OStringStream strSearch;
		strSearch << TEXT("%s\\..") << strFullPath;
		GetFullPathName( strSearch.str().c_str(), MAX_PATH, strFullPath, &strFilePart );
    }

    return String();
}


_ENGINE_END




//String FindMediaSearchTypicalDirs(const Char* leafName, const Char* exePath, const Char* exeName, UInt pathSize)
//{
//    // Typical directories:
//    //      .\
//    //      ..\
//    //      ..\..\
//    //      ..\..\..\
//    //      %EXE_DIR%\
//    //      %EXE_DIR%\..\
//    //      %EXE_DIR%\..\..\
//    //      %EXE_DIR%\..\..\..\
//    //      %EXE_DIR%\..\%EXE_NAME%
//    //      %EXE_DIR%\..\..\%EXE_NAME%
//	//      %EXE_DIR%\..\..\..\%EXE_NAME%
//
//    // Search in .\  
//	Char* searchPath = new Char[pathSize];
//    _tcscpy_s(searchPath, pathSize, leafName);
//    if( GetFileAttributes(leafName) != INVALID_FILE_ATTRIBUTES )
//        return searchPath;
//
//    Char* leafFolders[] = 
//    { 
//        TEXT("..\\%s"),
//		TEXT("..\\..\\%s"),
//		TEXT("..\\..\\%s"),
//		TEXT("..\\..\\..\\%s"),
//    };
//
//    // Look through each leaf folder to find the file
//    for (UINT i = 0; i < ARRAYSIZE(leafFolders); ++i)
//    {
//        _stprintf_s(searchPath, pathSize, leafFolders[i], leafName);
//        if (GetFileAttributes(searchPath) != INVALID_FILE_ATTRIBUTES)
//            return searchPath;
//    }
//
//    Char* exepathFolders[] = 
//    { 
//		TEXT("%s\\%s"),
//		TEXT("%s\\..\\%s"),
//		TEXT("%s\\..\\..\\%s"),
//		TEXT("%s\\..\\..\\..\\%s"),
//    };
//
//    // Look through exePath leaf folder to find the file
//    for (UINT i = 0; i < ARRAYSIZE(exepathFolders); ++i)
//    {
//        _stprintf_s(searchPath, pathSize, exepathFolders[i], exePath, leafName);
//        if (GetFileAttributes(searchPath) != INVALID_FILE_ATTRIBUTES)
//            return searchPath;
//    }
//
//    Char* exenameFolders[] = // This matches the DirectX SDK layout
//    { 
//		TEXT("%s\\..\\%s\\%s"),
//		TEXT("%s\\..\\..\\%s\\%s"),
//		TEXT("%s\\..\\..\\..\\%s\\%s")
//    };
//
//    // Look through exePath leaf folder to find the file
//    for (UINT i = 0; i < ARRAYSIZE(exenameFolders); ++i)
//    {
//        _stprintf_s(searchPath, pathSize, exenameFolders[i], exePath, exeName, leafName);
//        if (GetFileAttributes(searchPath) != INVALID_FILE_ATTRIBUTES)
//            return searchPath;
//    }
//
//	// Clear up resources
//	SafeDelete(searchPath);
//
//	// Failed to find the file in typical dirs
//    return String();
//}
//
//String FindMediaSearchParentDirs(const Char* startAt, const Char* leafName, UInt pathSize)
//{
//    TCHAR strFullPath[MAX_PATH] = { 0 };
//    TCHAR strFullFileName[MAX_PATH] = { 0 };
//    TCHAR strSearch[MAX_PATH] = { 0 };
//    TCHAR* strFilePart = 0;
//
//    GetFullPathName( startAt, MAX_PATH, strFullPath, &strFilePart );
//    if( strFilePart == NULL )
//        return false;
//
//    while( strFilePart != NULL && *strFilePart != TEXT('\0') )
//    {
//		_stprintf_s( strFullFileName, MAX_PATH, TEXT("%s\\%s"), strFullPath, leafName );
//        if( GetFileAttributes( strFullFileName ) != 0xFFFFFFFF )
//        {
//			Char* searchPath = new Char[pathSize];
//            _tcscpy_s( searchPath, pathSize, strFullFileName );
//            return searchPath;
//        }
//
//        _stprintf_s( strSearch, MAX_PATH, TEXT("%s\\.."), strFullPath );
//        GetFullPathName( strSearch, MAX_PATH, strFullPath, &strFilePart );
//    }
//
//    return String();
//}
