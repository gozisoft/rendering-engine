#ifndef __CCULLER_INL__
#define __CCULLER_INL__

inline const CCameraPtr& CCuller::GetCamera() const
{
	return m_pCamera;
}

inline const float* CCuller::GetFrustum() const
{
	return &m_frustum[0];
}

inline const CCuller::SceneNodes& CCuller::GetVisibleArray() const
{
	return m_visibleSet;
}

inline size_t CCuller::GetVisibleCount() const
{
	return m_visibleSet.size();
}

inline UInt8 CCuller::GetPlaneStates() const
{
	return m_planeStates;
}

inline void CCuller::SetPlaneStates(UInt8 states)
{
	m_planeStates = states;
}

inline void CCuller::InsertVisible(CVisualNode* visual)
{
	m_visibleSet.push_back(visual);
}

#endif


//inline void CCuller::InsertVisible(CVisualNode* spatial)
//{
//	size_t size = m_visibleSet.size();
//    if (m_numVisible < size)
//    {
//        m_visibleSet[m_numVisible] = pSpatial;
//    }
//    else
//    {
//        m_visibleSet.push_back(pSpatial);
//    }
//    ++m_numVisible;
//}