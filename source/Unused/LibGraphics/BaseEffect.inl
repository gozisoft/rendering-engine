#ifndef __CBASE_EFFECT_INL__
#define __CBASE_EFFECT_INL__

// Member access
inline size_t CBaseEffect::GetNumTechniques() const
{
	return m_techniques.size();
}

inline const CTechnique& CBaseEffect::GetTechnique(size_t techniqueIndex) const
{
	return m_techniques[techniqueIndex];
}

inline CTechnique& CBaseEffect::GetTechnique(size_t techniqueIndex)
{
	return const_cast<CTechnique&>( static_cast<const CBaseEffect&>(*this).GetTechnique(techniqueIndex) );
}

inline CTechnique& CBaseEffect::GetTechnique(const std::string& techName)
{
	return const_cast<CTechnique&>( static_cast<const CBaseEffect&>(*this).GetTechnique(techName) );
}

#endif