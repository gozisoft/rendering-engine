#include "frameafx.h"
#include "Framework.h"
#include "IBaseApp.h"
#include "IRenderer.h"
#include "IEventReciever.h"
#include "Timer.h"
#include "MouseController.h"
#include "FileUtility.h"
#include "StringFunctions.h"
#include "WinMsgEvent.h"

using namespace Engine;

DEFINE_HEAP(CFramework, "CFramework");

MouseInputEvent MessageMap(UINT message)
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
		return MIE_LMOUSE_DOWN;
	case WM_LBUTTONUP:
		return MIE_LMOUSE_UP;
	case WM_RBUTTONDOWN:
		return MIE_RMOUSE_DOWN;
	case WM_RBUTTONUP:
		return MIE_RMOUSE_UP;
	case WM_MBUTTONDOWN:
		return MIE_MMOUSE_DOWN;
	case WM_MBUTTONUP:
		return MIE_MMOUSE_UP;
	case WM_MOUSEMOVE:
		return MIE_MOUSE_MOVED;
	case WM_MOUSEWHEEL:
		return MIE_MOUSE_WHEEL;
	}

	return MIE_UNKOWN;
}

UInt32 MouseButtonMap(Int32 buttonstate)
{
	UInt32 buttons = 0;

	if ( (buttonstate & MK_RBUTTON) != 0 )
		buttons |= MBS_RIGHT;

	if ( (buttonstate & MK_LBUTTON) != 0 )
		buttons |= MBS_LEFT;

	if ( (buttonstate & MK_MBUTTON) != 0 )
		buttons |= MBS_MIDDLE;

	if ( (buttonstate & MK_XBUTTON1) != 0 )
		buttons |= MBS_EXTRA1;

	if ( (buttonstate & MK_XBUTTON2) != 0 )
		buttons |= MBS_EXTRA2;

	return buttons;
}

// ---------------------------------------------------------------------------------------------------------
// Summary: Default constructor
// ---------------------------------------------------------------------------------------------------------
CFramework::CFramework(IBaseAppPtr pGameApp,
	const String& title,
	bool windowed,
	HINSTANCE hInstance,
	const Point2l& windowedSize,
	const Point2l& fullSize) : 
m_hwnd(0),
m_hInstance(hInstance),
m_winSize(windowedSize),
m_fullSize(fullSize),
m_windowed(windowed),
m_minimised(false),
m_maximised(false),
m_borderDrag(false),
m_closing(false),
m_title(title),
m_active(true),
m_pGameApp(pGameApp)
{
	SecureZeroMemory(&m_wp, sizeof(WINDOWPLACEMENT));

	if (!m_hInstance)
		m_hInstance = GetModuleHandle(0);
	else
		m_hInstance = hInstance;

	if ( m_fullSize == cml::zero_2D() )
	{
		m_fullSize[0] = GetSystemMetrics(SM_CXSCREEN);
		m_fullSize[1] = GetSystemMetrics(SM_CYSCREEN);
	}

	m_pTimer = std::make_shared<CTimer>();
}


CFramework::~CFramework()
{

}

// ---------------------------------------------------------------------------------------------------------
// Summary: Destructor
// ---------------------------------------------------------------------------------------------------------
void CFramework::Release()
{
	m_closing = true;
    OnLostDevice();
    OnDestroyDevice();
}

// ---------------------------------------------------------------------------------------------------------
// Summary: Creates the window and initializes DirectX Engine.
// Parameters:
// [in] szTitle - Title of the window
// [in] hInstance - Application instance.
// [in] iWidth - Window width.
// [in] iHeight - Window height.
// [in] bWindowed - Window mode (TRUE). Fullscreen mode (FALSE).
// ---------------------------------------------------------------------------------------------------------
bool CFramework::Initialise()
{
	// Define the window
	WNDCLASSEX wcex; 
	SecureZeroMemory(&wcex, sizeof(WNDCLASSEX));

	wcex.cbSize         = sizeof(WNDCLASSEX); 
	wcex.style          = CS_DBLCLKS;
	wcex.lpfnWndProc    = (WNDPROC)CFramework::StaticWndProc;
	wcex.cbClsExtra     = 0;
	wcex.cbWndExtra     = 0;
	wcex.hInstance      = m_hInstance;
	wcex.hIcon          = 0;
	wcex.hCursor        = LoadCursor(0, IDC_ARROW);
	wcex.hbrBackground  = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName   = 0;
	wcex.lpszClassName  = m_title.c_str();
	wcex.hIconSm        = 0; 

	// get absolute path
	String pathName = FindMediaFile( TEXT("media\\logo.ico") );

	// if there is an icon, load it
	wcex.hIcon = (HICON)LoadImage(m_hInstance, pathName.c_str(), IMAGE_ICON, 0, 0, LR_LOADFROMFILE);

	// Register the window
	RegisterClassEx(&wcex);

	DWORD style = WS_POPUP;

	if (m_windowed)
		style = WS_OVERLAPPEDWINDOW;

	// Adjust to desired size
	RECT rect = { 0, 0, m_winSize[0], m_winSize[1] };
	AdjustWindowRect(&rect, style, FALSE);

	// Center the window
	const LONG realWidth = rect.right - rect.left;
	const LONG realHeight = rect.bottom - rect.top;

	// Calculate the position of the window
	LONG windowLeft = (GetSystemMetrics(SM_CXSCREEN) - realWidth) / 2;
	LONG windowTop = (GetSystemMetrics(SM_CYSCREEN) - realHeight) / 2;

	// Create the window
	m_hwnd = CreateWindow(m_title.c_str(), m_title.c_str(), style, windowLeft, windowTop,
		realWidth, realHeight, 0, 0, m_hInstance, this);

	// Set the window pos
	SetWindowPos( m_hwnd, HWND_TOP, 0, 0, rect.right - rect.left, rect.bottom - rect.top, 
		SWP_NOZORDER | SWP_NOMOVE );

	// Create the window hidden until the render is working
	ShowWindow(m_hwnd, SW_HIDE);
	UpdateWindow(m_hwnd);

	// Save current location/size
	m_wp.length = sizeof(WINDOWPLACEMENT);
	GetWindowPlacement(m_hwnd, &m_wp);

	// Create the mouse controller
	m_pMouse = std::make_shared<CMouseController>(m_winSize, m_hwnd, m_windowed);

	// Initialise Direct3D / OpenGL
	if (!m_pRenderer)
	{

#if defined(USING_DIRECT3D11)
		IRenderer::RendererType type = IRenderer::RT_DIRECT3D11;
#elif defined(USING_DIRECT3D9)
		IRenderer::RendererType type = IRenderer::RT_DIRECT3D9;
#elif defined(USING_OPENGL)
		IRenderer::RendererType type = IRenderer::RT_OPENGL;
#endif
		m_pRenderer = IRenderer::Create(type, m_hwnd, m_hInstance, m_windowed);
	}

	// Show the window and set forground for a higher process affinety
	ShowWindow(m_hwnd, SW_SHOW);
	SetForegroundWindow(m_hwnd);
	UpdateWindow(m_hwnd);

	// Run these to get started
    OnCreateDevice();
    OnResetDevice();

	// Start the timer
    Pause(false, false);

    return true;
}


// ---------------------------------------------------------------------------------------------------------
// Summary: Runs the application
// ---------------------------------------------------------------------------------------------------------
int CFramework::Run(void* hAccel)
{
    MSG msg;
	msg.message = WM_NULL;
	PeekMessage(&msg, NULL, 0U, 0U, PM_NOREMOVE);
	bool bGotMsg = false;

	while (WM_QUIT != msg.message)
	{
		// Use PeekMessage() so we can use idle time to render the scene. 
		bGotMsg = ( PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE) != 0 );

		if (bGotMsg)
		{
			if (m_hwnd == NULL || hAccel == NULL || 0 == TranslateAccelerator(m_hwnd, (HACCEL)hAccel, &msg))
			{
				TranslateMessage (&msg);
				DispatchMessage (&msg);
			}
		}
		else
		{
			if (m_renderingPaused || !m_active)
			{
				// Window is minimized/paused/occluded/or not exclusive so yield CPU time to other processes
				Sleep(50);
			}
			else
			{
				OnUpdateFrame();
				OnRenderFrame();
			}
		}
	} // while

    // Cleanup the accelerator table
    if(hAccel != NULL)
	{
		HACCEL accel = reinterpret_cast<HACCEL>(hAccel);
        DestroyAcceleratorTable(accel);
	}

	return static_cast<int>(msg.wParam);
}

// ---------------------------------------------------------------------------------------------------------
// Summary: Called after the device is created. Create D3DPOOL_MANAGED resources here.
// ---------------------------------------------------------------------------------------------------------
void CFramework::OnCreateDevice()
{
    if (m_pGameApp && m_pRenderer && m_pMouse)
    {
		m_pGameApp->OnCreateDevice(m_pRenderer, m_pMouse);
    }
}

// ---------------------------------------------------------------------------------------------------------
// Summary: Called after the device is reset. Create D3DPOOL_DEFAULT resources here.
// ---------------------------------------------------------------------------------------------------------
void CFramework::OnResetDevice()
{
    if (m_pGameApp && m_pRenderer)
    {
		// reset the renderer viewport.
		m_pRenderer->Resize( m_clientSize[0], m_clientSize[1] );

		// Resets camera view params
        m_pGameApp->OnResetDevice(m_pRenderer);
    }
}

// ---------------------------------------------------------------------------------------------------------
// Summary: Called when the device is lost. Release D3DPOOL_DEFAULT resources here.
// ---------------------------------------------------------------------------------------------------------
void CFramework::OnLostDevice()
{
	if (!m_closing)
	{
		// Stop the timer only if we're not closing down
        // or else stack corruption on return from Pause
		Pause(true, true);
	}

    if (m_pGameApp)
        m_pGameApp->OnLostDevice();
    
}

// ---------------------------------------------------------------------------------------------------------
// Summary: Called after the device is destroyed. Release D3DPOOL_MANAGED resources here.
// ---------------------------------------------------------------------------------------------------------
void CFramework::OnDestroyDevice()
{
    if (m_pGameApp)
        m_pGameApp->OnDestroyDevice();

	if (m_pTimer)
		m_pTimer->Reset();
}

// ---------------------------------------------------------------------------------------------------------
// Summary: Updates the current frame.
// ---------------------------------------------------------------------------------------------------------
void CFramework::OnUpdateFrame()
{
    if (m_pTimer)
    {
        m_pTimer->Update();
    }

    if (m_pGameApp && m_pTimer)
    {
		// Update the game render loop
		m_pGameApp->OnUpdateFrame( m_pTimer->GetGameTime(),	m_pTimer->GetDeltaTime() );
    }
}

// ---------------------------------------------------------------------------------------------------------
// Summary: Renders the current frame.
// ---------------------------------------------------------------------------------------------------------
void CFramework::OnRenderFrame()
{
	if (m_pGameApp && m_pTimer && m_pRenderer && !m_renderingPaused)
    {
		m_pGameApp->OnRenderFrame( m_pRenderer, m_pTimer->GetGameTime(), m_pTimer->GetDeltaTime() );
    }
}

void CFramework::CheckForWindowSizeChange()
{
	if (!m_windowed)
		return;

	RECT clientRect = { 0 };
	GetClientRect(m_hwnd, &clientRect);
	if ( clientRect.right != m_clientSize[0] || clientRect.bottom != m_clientSize[1] )
	{
		// set the new client size (client area)
		m_clientSize = Point2l(clientRect.right, clientRect.bottom); 

		// resize the mouse area
		m_pMouse->OnResize(m_clientSize); 

		// Rest the graphics
		OnResetDevice(); 
	}

	RECT windowRect = { 0 };
	GetWindowRect(m_hwnd, &windowRect);
	if ( windowRect.right != m_winSize[0] || windowRect.bottom != m_winSize[1] )
	{
		// set the new window size
		m_winSize = Point2l(windowRect.right, windowRect.bottom);
	}

}

void CFramework::CheckForWindowChangingMonitors()
{



}

void CFramework::ToggleFullScreen()
{
    if (!m_active)
        return;

    Pause(true, true);

	m_windowed = !m_windowed;

	if (m_windowed)
	{
		// going windowed mode
		SetWindowLongPtr(m_hwnd, GWL_EXSTYLE, WS_EX_LEFT);
		SetWindowLongPtr(m_hwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW | WS_VISIBLE);
	}
	else
	{
		// Save old window location and size
        SecureZeroMemory( &m_wp, sizeof(WINDOWPLACEMENT) );
        m_wp.length = sizeof(WINDOWPLACEMENT);
        GetWindowPlacement(m_hwnd, &m_wp);

		// Going to fullscreen mode
		SetWindowLongPtr(m_hwnd, GWL_EXSTYLE, WS_EX_APPWINDOW | WS_EX_TOPMOST);
		SetWindowLongPtr(m_hwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);
	}

    // Reset the Device
    OnLostDevice();
	OnResetDevice();

	if (m_windowed)
	{
		// Going to windowed mode
        // Restore the window location/size
        SetWindowPlacement(m_hwnd, &m_wp);
	}
	else
	{
		SetWindowPos(m_hwnd, HWND_TOPMOST, 0, 0, m_fullSize[0], m_fullSize[1], SWP_SHOWWINDOW);
	}

	Pause(false, false);
}


// ---------------------------------------------------------------------------------------------------------
// Summary: Event handler. Routes messages to appropriate instance.
// Parameters:
// [in] hWnd - Unique handle to the window.
// [in] message - Incoming message.
// [in] wParam - Parameter of the message (unsigned int).
// [in] lParam - Parameter of the message (long).
// ---------------------------------------------------------------------------------------------------------
LRESULT CALLBACK CFramework::StaticWndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	CFramework *pTargetApp = 0;

    if (msg == WM_CREATE)
    {
		LPCREATESTRUCT lpcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
		pTargetApp = reinterpret_cast<CFramework*>(lpcs->lpCreateParams);
		pTargetApp->SetHWND(hWnd);
        SetWindowLongPtr( hWnd, GWLP_USERDATA, reinterpret_cast<LPARAM>(pTargetApp) );
    }
	else
	{
		pTargetApp = reinterpret_cast<CFramework*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) );
	}

    if (pTargetApp)
    {
        return pTargetApp->WndProc( hWnd, msg, wParam, lParam );
    }
	else
	{
		return DefWindowProc( hWnd, msg, wParam, lParam );
	}
}

// ---------------------------------------------------------------------------------------------------------
// Summary: Pauses or unpauses rendering and the timer.
// Parameters:
// [in] rendering - TRUE to pause rendering, FALSE to unpause rendering.
// [in] timer - TRUE to pause the timer, FALSE to unpause the timer.
// ---------------------------------------------------------------------------------------------------------
void CFramework::Pause(bool rendering, bool timer)
{
	// Render pause check
	m_renderingPauseCount += (rendering) ? 1 : -1;
	m_renderingPauseCount = (m_renderingPauseCount < 0) ? 0 : m_renderingPauseCount;
	m_renderingPaused = (m_renderingPauseCount > 0);

	// Timer pause check
    m_timerPauseCount += timer ? 1 : -1;
    m_timerPauseCount = (m_timerPauseCount < 0) ? 0 : m_timerPauseCount;
	m_timerPaused = (m_timerPauseCount > 0);
	
	switch (m_timerPaused)
	{
	case true:
		if (m_pTimer)
		{
			m_pTimer->Stop();
		}
		break;

	case false:
		if (m_pTimer)
		{
			m_pTimer->Start();
		}
		break;
	};
}

void CFramework::OnWindowEvent(const CWinEvent& event)
{
	if (m_pGameApp)
	{
		m_pGameApp->OnWindowEvent(event);
	}
}


// ---------------------------------------------------------------------------------------------------------
// Summary: Application event handler.
// Parameters:
// [in] hWnd - Unique handle to the window.
// [in] message - Incoming message.
// [in] wParam - Parameter of the message (unsigned int).
// [in] lParam - Parameter of the message (long).
// ---------------------------------------------------------------------------------------------------------
LRESULT CALLBACK CFramework::WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	CWinEvent event;

	static UInt32 ClickCount = 0;
	if ( GetCapture() != hWnd && ClickCount > 0 )
		ClickCount = 0;

	// Initiate fullscreen
	if (message == WM_SYSKEYDOWN)
	{
		switch (wParam)
		{
		case VK_RETURN:
			{
				// Toggle full screen upon alt-enter 
				DWORD dwMask = (1 << 29);
				if ( (lParam & dwMask) != 0 ) // Alt is down also
				{
					// Toggle the full screen/window mode
					Pause(true, true);
					ToggleFullScreen();
					Pause(false, false);
					break;
				}
			}
		}
	}

	// Quit application
	if (message == WM_KEYDOWN) 
	{
		switch (wParam)
		{
		case VK_ESCAPE:
			{
				SendMessage(hWnd, WM_CLOSE, 0, 0);
				break;
			}
		}
	}

    switch (message) 
    {
    case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
        break;

	case WM_CLOSE:
		{
			TCHAR ClassName[MAX_PATH];
			SecureZeroMemory(&ClassName[0], MAX_PATH);
			GetClassName(hWnd, &ClassName[0], MAX_PATH);
			UnregisterClass(&ClassName[0], m_hInstance);
			DestroyWindow(hWnd);
		}
		break;

    case WM_PAINT:
		{
			ValidateRect(hWnd, 0);
		}
        break;

	case WM_SIZE:
		if (m_pRenderer) 
		{
			if (wParam == SIZE_MINIMIZED)
			{
				// Disable application on minimized
				Pause(true, true);

				m_active = false;
				m_minimised = true;
				m_maximised = false;
			}
			else
			{
				m_active = true;
				RECT rcClient;
				GetClientRect(m_hwnd, &rcClient);
				if(!rcClient.top && !rcClient.bottom)
				{
					// Rapidly clicking the task bar to minimize and restore a window
					// can cause a WM_SIZE message with SIZE_RESTORED when 
					// the window has actually become minimized due to rapid change
					// so just ignore this message
				}
				else if (wParam == SIZE_MAXIMIZED)
				{
					if (m_minimised)
						Pause(false, false);

					m_minimised = false;
					m_maximised = true;
					CheckForWindowSizeChange();
					CheckForWindowChangingMonitors();
				}
				else if (wParam == SIZE_RESTORED)
				{
					if (m_minimised) // Restoring from minimized state?
					{
						Pause(false, false);
						m_minimised = false;
						CheckForWindowSizeChange();
						CheckForWindowChangingMonitors();
					}
					else if (m_maximised) // Restoring from maximized state?
					{
						m_maximised = false;
						CheckForWindowSizeChange();
						CheckForWindowChangingMonitors();
					}	
					else if (m_borderDrag)
					{
						// If user is dragging the resize bars, we do not resize 
						// the buffers here because as the user continuously 
						// drags the resize bars, a stream of WM_SIZE messages are
						// sent to the window, and it would be pointless (and slow)
						// to resize for each WM_SIZE message received from dragging
						// the resize bars.  So instead, we reset after the user is 
						// done resizing the window and releases the resize bars, which 
						// sends a WM_EXITSIZEMOVE message.
					}
					else
					{
						// This WM_SIZE come from resizing the window via an API like SetWindowPos() so 
                        // resize and reset the device now.
						CheckForWindowSizeChange();
						CheckForWindowChangingMonitors();
					}
				}
			}
		}
		break;

	// WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
	case WM_ENTERSIZEMOVE:
		// Halt frame movement while the app is sizing or moving
		Pause(true, true);
		m_borderDrag = true;
		break;

	case WM_EXITSIZEMOVE:
		Pause(false, false);
		m_borderDrag = false;
		CheckForWindowSizeChange();
		break;
	
	case WM_GETMINMAXINFO:
		// Catch this message so to prevent the window from becoming too small.
		((MINMAXINFO*)lParam)->ptMinTrackSize.x = 200;
		((MINMAXINFO*)lParam)->ptMinTrackSize.y = 200; 
		break;

	case WM_SYSCOMMAND:
		// prevent screensaver or monitor powersave mode from starting
		if ((wParam & 0xFFF0) == SC_SCREENSAVE || (wParam & 0xFFF0) == SC_MONITORPOWER)
			return 0;
		break;

	case WM_SETCURSOR:
		break;

	// Keyboard input recieved
	case WM_SYSKEYDOWN:
	case WM_SYSKEYUP:
	case WM_KEYDOWN:
	case WM_KEYUP:
		{
			event.EventType = CWinEvent::W_KEYBOARD_EVENT;
			event.KeyInput.key = static_cast<KeyCode>(wParam);
			event.KeyInput.pressedDown = ( message == WM_KEYDOWN || message == WM_SYSKEYDOWN );

			BYTE keyStates[256];
			GetKeyboardState(keyStates);

			OnWindowEvent(event);

			if (message == WM_SYSKEYDOWN || message == WM_SYSKEYUP)
				return DefWindowProc(hWnd, message, wParam, lParam);
			else
				break;
		}

	// Mouse input recieved
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_LBUTTONDBLCLK:
	case WM_MBUTTONDOWN:
	case WM_MBUTTONUP:
	case WM_MBUTTONDBLCLK:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_RBUTTONDBLCLK:
	case WM_XBUTTONDOWN:
	case WM_XBUTTONUP:
	case WM_XBUTTONDBLCLK:
	case WM_MOUSEMOVE:
	case WM_MOUSEWHEEL:
		{
			event.EventType = CWinEvent::W_MOUSE_EVENT;
			event.MouseInput.mouseEvent = MessageMap(message);
			event.MouseInput.x = static_cast<short>( LOWORD(lParam) );
			event.MouseInput.y = static_cast<short>( HIWORD(lParam) );
			event.MouseInput.shift = ( (LOWORD(wParam) & MK_SHIFT) != 0 );
			event.MouseInput.ctrl = ( (LOWORD(wParam) & MK_CONTROL) != 0 );

			// left or right mouse buttons
			event.MouseInput.ButtonStates = MouseButtonMap( LOWORD(wParam) );

			// Mouse wheel input
			event.MouseInput.Wheel = 0.0f;
			if( message == WM_MOUSEWHEEL )
			{
				// WM_MOUSEWHEEL passes screen mouse coords
				// so convert them to client coords
				POINT pt = { 0, 0 };
				pt.x = event.MouseInput.x;
				pt.y = event.MouseInput.y;
				ScreenToClient( hWnd, &pt );
				event.MouseInput.x = pt.x;
				event.MouseInput.y = pt.y;
				event.MouseInput.Wheel = static_cast<float>( (short)HIWORD(wParam) / (float)WHEEL_DELTA );
			}

			m_pMouse->SetHWND(hWnd);
			OnWindowEvent(event);
		}
		break;

    }

    return DefWindowProc( hWnd, message, wParam, lParam );
}

/*
struct messageMap
{
Int32 group;
UINT winMessage;
MouseInputEvent myMessage;
};

static messageMap mouseMap[] = 
{
{ 0, WM_LBUTTONDOWN, MIE_LMOUSE_DOWN },
{ 1, WM_LBUTTONUP, MIE_LMOUSE_UP },
{ 0, WM_RBUTTONDOWN, MIE_RMOUSE_DOWN },
{ 1, WM_RBUTTONUP, MIE_RMOUSE_UP },
{ 0, WM_MBUTTONDOWN, MIE_MMOUSE_DOWN },
{ 1, WM_MBUTTONUP, MIE_MMOUSE_UP },
{ 2, WM_MOUSEMOVE, MIE_MOUSE_MOVED },
{ 3, WM_MOUSEWHEEL, MIE_MOUSE_WHEEL },
{ -1, 0, MIE_UNKOWN }
};
*/