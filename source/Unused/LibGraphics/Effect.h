#ifndef __CEFFECT_H__
#define __CEFFECT_H__

#include "BaseVariable.h"

_ENGINE_BEGIN



class CShaderBlock;

class IEffect
{
public:
	virtual ~IEffect() { }

	// Gets a constant buffer variable by name. Constant buffers have structure types in both
	// D3D10/11 and OpenGL (3.1). They are only of actual structure type however in OpenGL.
	// If it is a global variable you want access to use the name "$(Global)".
	// This is like D3D10/11 which store all global variables / uniforms within a reserverd
	// global buffer called $(Globals).
	virtual CVariable* GetVariableByName(const std::string& name) const = 0;

	// Access a single variable at a time via index
	virtual CVariable* GetVariableByIndex(size_t index) const = 0;

	// Dirty list building
	virtual void NotifyDirty(CVariable* variable) = 0;
};


class CEffect
{
public:
	// Public typedefs
	typedef GlobalVariables Variables;
	typedef std::vector<CShaderBlock*> ShaderBlocks;

	// Special Ctor for use with instances. Involves using a 
	// construction determined technique.
	CEffect(const CBaseEffectPtr& effect, const std::string& techName);

	// non virtual destructor
	virtual ~CEffect();

	// Gets a constant buffer variable by name. Constant buffers have structure types in both
	// D3D10/11 and OpenGL (3.1). They are only of actual structure type however in OpenGL.
	// If it is a global variable you want access to use the name "$(Global)".
	// This is like D3D10/11 which store all global variables / uniforms within a reserverd
	// global buffer called $(Globals).
	CVariable* GetVariableByName(const std::string& name) const;

	// Access a single variable at a time via index
	CVariable* GetVariableByIndex(size_t index) const;

	// Constant access to the global variables
	const Variables& GetVariables() const;

	// Get access to the active chosen technique
	const CTechnique& GetTechnique() const;
	
protected:
	// Function to setup the effect from given data
	void ProcessVariables(CShaderBlock* shaderBlock);

	// These are the effect variables
	Variables m_variablePool;

	// Shader blocks used at lower level
	ShaderBlocks m_shaderBlocks;

	// Contains effect rasterisor states
	CBaseEffectPtr m_pEffect;

	// Chosen technique
	std::reference_wrapper<CTechnique> m_tech;
};



#include "Effect.inl"

_ENGINE_END

#endif



	//// Constructor requires a base effect as it contains all the details
	//// of vertex / pixel / geometry shaders.
	//CEffect(const CBaseEffectPtr& effect);