#ifndef __EFFECT_GLOBALS_H__
#define __EFFECT_GLOBALS_H__

#include "ShaderTypes.h"

_ENGINE_BEGIN

namespace Shader
{
	inline void DwordMemcpy(void * __restrict pDest, const void * __restrict pSource, UInt byteCount)
	{
		// Debug error check
		assert(byteCount % 4 == 0);

		// Used within the for loops
		UInt32 i = 0;

#ifdef _AMD64_
		const UInt qwordCount = byteCount >> 3;

		__int64* src64 = (__int64*) pSource;
		__int64* dst64 = (__int64*) pDest;

		for (i=0; i<(qwordCount & 0x3); i++)
		{
			*(dst64) = *(src64);
			dst64++;
			src64++;
		}

		for (; i<qwordCount; i+= 4)
		{
			*(dst64     ) = *(src64     );
			*(dst64 + 1 ) = *(src64 + 1 );
			*(dst64 + 2 ) = *(src64 + 2 );
			*(dst64 + 3 ) = *(src64 + 3 );
			dst64 += 4;
			src64 += 4;
		}

		ANALYSIS_ASSUME( dst64 - static_cast< __int64* >(pDest) <= byteCount - 4 );
		ANALYSIS_ASSUME( src64 - static_cast< const __int64* >(pSource) <= byteCount - 4 );
		if( byteCount & 0x4 )
		{
			*((UInt32*)dst64) = *((UInt32*)src64);
		}
#else
		const UInt32 dwordCount = byteCount >> 2;

		for (i = 0; i < (dwordCount & 0x3); ++i)
		{
#pragma prefast(suppress: __WARNING_UNRELATED_LOOP_TERMINATION, "(dwordCount & 03) < dwordCount")
			((UInt32*)pDest)[i  ] = ((UInt32*)pSource)[i ];
		}
		for (/**/; i < dwordCount; i += 4)
		{
			((UInt32*)pDest)[i  ] = ((UInt32*)pSource)[i  ];
			((UInt32*)pDest)[i+1] = ((UInt32*)pSource)[i+1];
			((UInt32*)pDest)[i+2] = ((UInt32*)pSource)[i+2];
			((UInt32*)pDest)[i+3] = ((UInt32*)pSource)[i+3];
		}
#endif
	}

	// Specialisation function for equal types
	template < typename T >
	void CopyScalarValue(const T& srcValue, T& destValue)
	{
		destValue = srcValue;
	}

	// Specialisation function for bool types
	template < typename T >
	void CopyScalarValue(bool srcValue, T& destValue)
	{
		destValue = (srcValue) ? U(1) : U(0);
	}

	// Specialisation function for bool types
	template < typename T >
	void CopyScalarValue(const T& srcValue, bool destValue)
	{
		destValue = IsZero(srcValue) ? true : false;
	}

	// Specialisation function for different types (that are not bool)
	template < typename T, typename U >
	void CopyScalarValue(const T& srcValue, U& destValue)
	{
		destValue = static_cast<U>(srcValue);
	}

	// Specialisation function for equal vector types
	template < typename T >
	void CopyDataWithTypeConversion(const T* pSource, T* pDest, size_t srcVecSize,
		size_t dstVecSize, size_t elementCount, size_t vecCount=1U)
	{
		for (size_t i = 0; i < vecCount; ++i, pDest += dstVecSize, pSource += srcVecSize)
		{
			DwordMemcpy( pDest, pSource, elementCount * sizeof(T) );
		}
	}

	// Specialisation function for different vector types
	template < typename T, typename U >
	void CopyDataWithTypeConversion(const T* pSource, U* pDest, size_t srcVecSize,
		size_t dstVecSize, size_t elementCount, size_t vecCount=1U)
	{
		for (size_t i = 0; i < vecCount; ++i, pDest += dstVecSize, pSource += srcVecSize)
		{
			for (size_t j = 0; j < elementCount; ++j)
			{
				CopyScalarValue(pSource[j], pDest[j]);
			}
		}
	}

} // namespace Shader

_ENGINE_END

#endif


//	template< Detail::TemplateVarType EnumDestType,
//		Detail::TemplateVarType EnumSrcType, 
//		typename DestType,
//		typename SourceType >
//		inline void SetScalarArray(DestType *pDestValues, const SourceType *pSrcValues, UInt Offset, UInt Count, 
//		SType *pType, UINT  TotalUnpackedSize, const char *pFuncName)
//	{
//#ifdef _DEBUG    
//		if (!AreBoundsValid(Offset, Count, pSrcValues, pType, TotalUnpackedSize))
//		{
//			assert( false && ieS("Invalid range specified SetScalarArray()") );
//		}
//#endif
//
//		UInt delta = pType->NumericType.IsPackedArray ? 1 : SType::c_ScalarsPerRegister;
//		pDestValues += Offset * delta;
//		for (UInt i = 0, UInt j = 0; j < Count; i += delta, ++ j)
//		{
//			// pDestValues[i] = (DEST_TYPE)pSrcValues[j];
//			CopyScalarValue<SourceType, DestType, SRC_TYPE, FALSE>(pSrcValues[j], &pDestValues[i], "SetScalarArray");
//		}
//	}


//// non template version called by template function
//template< typename T, typename U, VariableType srcType, VariableType destType >
//void CopyScalarValue(const T& srcValue, U& destValue)
//{
//	switch (srcType)
//	{
//	case VT_BOOL: // specialisation for bool
//		switch (destType)
//		{
//		case VT_BOOL:
//			destValue = srcValue;
//			return;
//		case VT_INT:
//		case VT_UINT:
//		case VT_FLOAT:
//			destValue = (srcValue) ? U(1) : U(0);
//			return;
//		default:
//			assert(false); // invalid destination type
//			break;
//		}
//		return; // No more checks required here

//	case VT_INT: // specialisation for int int (no cast required)
//		switch (destType)
//		{
//		case VT_INT:
//			destValue = srcValue;
//			return;
//		}
//		break;

//	case VT_UINT: // specialisation for uint uint (no cast required)
//		switch (destType)
//		{
//		case VT_UINT:
//			destValue = srcValue;
//			return;
//		}
//		break;

//	case VT_FLOAT: // specialisation for float float (no cast required)
//		switch (destType)
//		{
//		case VT_FLOAT:
//			destValue = srcValue;
//			return;
//		}
//		break;

//	default:
//		switch (destType)
//		{
//		case VT_BOOL:
//			destValue = IsZero(srcValue) ? true : false;
//			return;
//		case VT_INT:
//		case VT_UINT:
//		case VT_FLOAT:
//			destValue = static_cast<U>(srcValue);
//			return;
//		}
//		break;
//	}

//	// This should never be reached
//	assert(false); // invalid source type
//}