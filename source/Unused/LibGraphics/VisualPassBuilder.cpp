#include "frameafx.h"
#include "VisualPassBuilder.h"
#include "VisualPass.h"
#include "Helper.h"

using namespace Engine;

CVisualPassBuilder::CVisualPassBuilder() :
m_ownerTransferred(false),
m_vertexShader(nullptr),
m_pixelShader(nullptr),
m_alphaState(nullptr),
m_cullState(nullptr),
m_depthState(nullptr),
m_offsetState(nullptr),
m_stencilState(nullptr),
m_wireState(nullptr),
m_ZBufferState(nullptr)
{

}

CVisualPassBuilder::~CVisualPassBuilder()
{
	if (!m_ownerTransferred)
	{
		if (m_vertexShader) SafeDelete(m_vertexShader);
		if (m_pixelShader) SafeDelete(m_pixelShader);
		if (m_alphaState) SafeDelete(m_alphaState);
		if (m_cullState) SafeDelete(m_cullState);
		if (m_depthState) SafeDelete(m_depthState);
		if (m_offsetState) SafeDelete(m_offsetState);
		if (m_stencilState) SafeDelete(m_stencilState);
		if (m_wireState) SafeDelete(m_wireState);
		if (m_ZBufferState) SafeDelete(m_ZBufferState);
	}
}

void CVisualPassBuilder::PassName(const char* name)
{
	m_passName = name;
}

void CVisualPassBuilder::VertexShader(CVertexShader* shader)
{
	m_vertexShader = shader;
}

void CVisualPassBuilder::PixelShader(CPixelShader* shader)
{
	m_pixelShader = shader;
}

void CVisualPassBuilder::AlphaState(CAlphaState* state)
{
	m_alphaState = state;
}

void CVisualPassBuilder::CullState(CCullState* state)
{
	m_cullState = state;
}

void CVisualPassBuilder::DepthState(CDepthState* state)
{
	m_depthState = state;
}

void CVisualPassBuilder::OffsetState(COffsetState* state)
{
	m_offsetState = state;
}

void CVisualPassBuilder::StencilState(CStencilState* state)
{
	m_stencilState = state;
}

void CVisualPassBuilder::WireState(CWireState* state)
{
	m_wireState = state;
}

void CVisualPassBuilder::ZBufferState(CZBufferState* state)
{
	m_ZBufferState = state;
}

CVisualPass* CVisualPassBuilder::BuildVisualPass()
{
	return new CVisualPass(*this);
}