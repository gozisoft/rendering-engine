#ifndef __CVISUAL_PASS_BUILDER_H__
#define __CVISUAL_PASS_BUILDER_H__

#include "framefwd.h"

_ENGINE_BEGIN

class CVisualPass;

class CVisualPassBuilder
{
	friend class CVisualPass;

public:
	// Constructor and destructor
	CVisualPassBuilder();
	~CVisualPassBuilder();

	// Functions to set data members
	void PassName(const char* name);
	void VertexShader(CVertexShader* shader);
	void PixelShader(CPixelShader* shader);
	void AlphaState(CAlphaState* state);
	void CullState(CCullState* state);
	void DepthState(CDepthState* state);
	void OffsetState(COffsetState* state);
	void StencilState(CStencilState* state);
	void WireState(CWireState* state);
	void ZBufferState(CZBufferState* state);

	// Creation function to create a visual pass
	CVisualPass* BuildVisualPass();

private:
	mutable bool m_ownerTransferred;
	std::string m_passName;
	CVertexShader* m_vertexShader;
	CPixelShader* m_pixelShader;
	CAlphaState* m_alphaState;
	CCullState* m_cullState;
	CDepthState* m_depthState;
	COffsetState* m_offsetState;
	CStencilState* m_stencilState;
	CWireState* m_wireState;
	CZBufferState* m_ZBufferState;
};

_ENGINE_END

#endif