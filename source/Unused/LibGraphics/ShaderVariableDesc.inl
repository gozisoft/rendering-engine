#ifndef __CSHADER_VARIABLE_INL__
#define __CSHADER_VARIABLE_INL__

// ----------------------------------------------------------------------------------------------
// CShaderDesc
// ----------------------------------------------------------------------------------------------
inline const std::string& CShaderDesc::GetVariableName() const
{
	return m_resourceName;
}

inline Shader::ClassType CShaderDesc::GetClassType() const
{
	return m_classType;
}

inline size_t CShaderDesc::GetBindPoint() const
{
	return m_bindPoint;
}

inline size_t CShaderDesc::GetBindCount() const
{
	return m_bindCount;
}

#endif