#include "frameafx.h"
#include "ShaderResourceDesc.h"

using namespace Engine;
using namespace Shader;

RTTI_IMPL_1(Engine, CShaderTextureDesc, CShaderDesc);
RTTI_IMPL_1(Engine, CShaderBufferDesc, CShaderDesc);

CShaderTextureDesc::CShaderTextureDesc(const std::string& name, size_t numSamples, ShaderReturnType returnType,
	ShaderDimensions dimensions, size_t bindPoint, size_t bindCount) : 
CShaderDesc(name, CT_CBUFFER, bindPoint, bindCount), m_numSamples(numSamples)
{


}

CShaderBufferDesc::CShaderBufferDesc(const std::string& name, size_t bindPoint=0, size_t bindCount=0)
{


}