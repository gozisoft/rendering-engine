#include "frameafx.h"
#include "VisualPass.h"
#include "VisualPassBuilder.h"

using namespace Engine;

CVisualPass::CVisualPass(const CVisualPassBuilder& builder) :
m_passName(builder.m_passName),
m_pVertexShader(builder.m_vertexShader),
m_pPixelShader(builder.m_pixelShader),
m_pAlphaState(builder.m_alphaState),
m_pCullState(builder.m_cullState),
m_pDepthState(builder.m_depthState),
m_pOffsetState(builder.m_offsetState),
m_pStencilState(builder.m_stencilState),
m_pWireState(builder.m_wireState),
m_pZBufferState(builder.m_ZBufferState)
{
	builder.m_ownerTransferred = true;
}

//CVisualPass::CVisualPass(const CVisualPassBuilder& builder) :
//m_pVertexShader( new CVertexShader(builder.m_vertexShader) ),
//m_pPixelShader( new CPixelShader(builder.m_pixelShader) ),
//m_pAlphaState( new CAlphaState(builder.m_alphaState) ),
//m_pCullState( new CCullState(builder.m_cullState) ),
//m_pDepthState( new CDepthState(builder.m_depthState) ),
//m_pOffsetState( new COffsetState(builder.m_offsetState) ),
//m_pStencilState( new CStencilState(builder.m_stencilState) ),
//m_pWireState( new CWireState(builder.m_wireState) ),
//m_pZBufferState( new CZBufferState(builder.m_ZBufferState) )
//{
//
//
//
//}

