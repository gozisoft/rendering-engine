#ifndef __EFFECT_CONSTANT_VARIABLE_H__
#define __EFFECT_CONSTANT_VARIABLE_H__

#include "BaseVariable.h"

_ENGINE_BEGIN

class numeric_variable;

//////////////////////////////////////////////////////////////////////////
// coonstant_variable - implements raw set/get functionality
//////////////////////////////////////////////////////////////////////////
class constant_object : public CVariable
{
public:
	// typedef typelist
	typedef std::vector<numeric_variable*> Variables;

	// default constructor
	constant_object(CConstantBuffer* buffer, CConstantFormat* pFormat, IResourceProxy* proxy);
	~constant_object();

	// Const Access the format descriptor
	const CConstantFormat* GetDesc() const
	{
		return boost::get<CConstantFormat*>(m_desc);
	}

	// Const Access the constant buffer
	const CConstantBuffer* GetBuffer() const
	{
		return boost::get<CConstantBuffer*>(m_resource);
	}

	// Non-Const Access the format descriptor
	CConstantFormat* GetDesc() 	
	{
		return const_cast<CConstantFormat*>( static_cast<const constant_object&>(*this).GetDesc() );
	}

	// Non-Const Access the constant buffer
	CConstantBuffer* GetBuffer()
	{
		return const_cast<CConstantBuffer*>( static_cast<const constant_object&>(*this).GetBuffer() );
	}

	Variables m_variables;
};



_ENGINE_END

#endif