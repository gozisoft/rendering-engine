#ifndef __EFFECT_FACTORY_H__
#define __EFFECT_FACTORY_H__

_ENGINE_BEGIN

//////////////////////////////////////////////////////////////////////////
// Factory creation function
//////////////////////////////////////////////////////////////////////////
CVariable* CreateGlobalVariable(Shader::ClassType classtype, Shader::VariableType varType);
CVariable* CreateGlobalVariable(Shader::ShaderDimensions dimensions);



_ENGINE_END

#endif