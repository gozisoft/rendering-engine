#ifndef NET_FORWARD_H
#define NET_FORWARD_H

#include "Core.h"

_ENGINE_BEGIN

//----------------------------------------------------------------------------
// Interfaces
//----------------------------------------------------------------------------
class IVertexBuffer;
class IIndexBuffer;

// Effect system
class IEffectVariable;
class IEffectScalarVariable;
class IEffectVectorVariable;
class IEffectMatrixVariable;
class IEffectStringVariable;
class IEffectResourceVariable;
class IEffectLoader;


// Scene graph
class ISceneController;
class ISceneControllerFPS;
//----------------------------------------------------------------------------
// Interface Smart Pointers
//----------------------------------------------------------------------------


// Rendering device
typedef std::shared_ptr<IRenderer> IRendererPtr;

// Rendering pipline
typedef std::shared_ptr<IPipeline> IPipelinePtr;

// Rendering Resource
typedef std::shared_ptr<IResource> IResourcePtr;
typedef std::shared_ptr<IBuffer> IBufferPtr;
typedef std::shared_ptr<IVertexBuffer> IVertexBufferPtr;
typedef std::shared_ptr<IIndexBuffer> IIndexBufferPtr;
typedef std::shared_ptr<IConstantBuffer> IConstantBufferPtr;
typedef std::shared_ptr<ITexture> ITexturePtr;
typedef std::shared_ptr<ITexture1D> ITexture1DPtr;
typedef std::shared_ptr<ITexture2D> ITexture2DPtr;

// Shader Resource
typedef std::shared_ptr<IView> IViewPtr;

// Shader
typedef std::shared_ptr<IShader> IShaderPtr;
typedef std::shared_ptr<IInputFormat> IInputFormatPtr;

// Scene based event recivers
typedef std::shared_ptr<ISceneController> ISceneControllerPtr;
typedef std::shared_ptr<ISceneControllerFPS> ISceneControllerFPSPtr;

//----------------------------------------------------------------------------
// Rendering Implementations [API level]
//----------------------------------------------------------------------------
// Input format
class CInputElement;
class CInputFormat;

// Buffers [buffer, texture]
class CBuffer;
class CTexture;
class CTexture1D;
class CTexture2D;

// Shell classes
class CIndexBuffer;
class CVertexBuffer;

// Views
class CView;
class CShaderResource;
class CRenderTarget;
class CDepthStencil;

// Shaders (generic)
class CShader;
class CVertexShader;
class CPixelShader;
class CInputElement;

// Effect system 
class CVisualPass;
class CTechnique;
class CBaseEffect;
class CEffect;
class CVariable;

// States (generic)
class CGlobalState;
class CAlphaState;
class CCullState;
class CDepthState;
class COffsetState;
class CStencilState;
class CWireState;
class CZBufferState;

// Mesh class
class CMesh;
class CSubMesh;


//----------------------------------------------------------------------------
// Rendering Smart Pointers
//----------------------------------------------------------------------------
// Buffers [buffer, texture]
typedef std::shared_ptr<CBuffer> CBufferPtr;
typedef std::shared_ptr<CTexture> CTexturePtr;
typedef std::shared_ptr<CTexture1D> CTexture1DPtr;
typedef std::shared_ptr<CTexture2D> CTexture2DPtr;

// Shell classes
typedef std::shared_ptr<CIndexBuffer> CIndexBufferPtr;
typedef std::shared_ptr<CVertexBuffer> CVertexBufferPtr;

// Views
typedef std::shared_ptr<CView> CViewPtr;
typedef std::shared_ptr<CShaderResource> CShaderResourcePtr;
typedef std::shared_ptr<CRenderTarget> CRenderTargetPtr;
typedef std::shared_ptr<CDepthStencil> CDepthStencilPtr;

// Shaders (generic)
typedef std::shared_ptr<CShader> CShaderPtr;
typedef std::shared_ptr<CVertexShader> CVertexShaderPtr;
typedef std::shared_ptr<CPixelShader> CPixelShaderPtr;

// Effects (generic)
typedef std::shared_ptr<CBaseEffect> CBaseEffectPtr;
typedef std::shared_ptr<CEffect> CEffectPtr;

// States (generic)
typedef std::shared_ptr<CGlobalState> CGlobalStatePtr;
typedef std::shared_ptr<CAlphaState> CAlphaStatePtr;
typedef std::shared_ptr<CCullState> CCullStatePtr;
typedef std::shared_ptr<CDepthState> CDepthStatePtr;
typedef std::shared_ptr<COffsetState> COffsetStatePtr;
typedef std::shared_ptr<CStencilState> CStencilStatePtr;
typedef std::shared_ptr<CWireState> CWireStatePtr;
typedef std::shared_ptr<CZBufferState> CZBufferStatePtr;

// Mesh class
typedef std::shared_ptr<CMesh> CMeshPtr;
typedef std::shared_ptr<CSubMesh> CSubMeshPtr;
//----------------------------------------------------------------------------
// Aplication
//----------------------------------------------------------------------------
class IBaseApp;
class IEventData;
class IEventListener;
class IMouseController;
class IEventManager;
//----------------------------------------------------------------------------
// Win32
//----------------------------------------------------------------------------
class CWinEvent;
class CFramework;
class CTimer;
class CMouseController; 

// Event Manager
class CEventManager;
struct SKeyEvent;
struct SMouseEvent;
struct SWindowSizeEvent;
struct SWindowCreateEvent;
//----------------------------------------------------------------------------
// Pointer types
//----------------------------------------------------------------------------
// Interfaces
typedef std::shared_ptr<IEventData> IEventDataPtr;
typedef std::shared_ptr<IEventListener> IEventListenerPtr;
typedef std::shared_ptr<IMouseController> IMouseControllerPtr;
typedef std::shared_ptr<IEventManager> IEventManagerPtr;

// Application
typedef std::shared_ptr<IBaseApp> IBaseAppPtr;

// Framework
typedef std::shared_ptr<CFramework> CFrameworkPtr;
typedef std::shared_ptr<CTimer> CTimerPtr;
typedef std::shared_ptr<CMouseController> CMouseControllerPtr;

// Events
typedef std::shared_ptr<CEventManager> CEventManagerPtr;


//----------------------------------------------------------------------------
// Scenegraph
//----------------------------------------------------------------------------
class ISpatial;
class CSpatial;
class CRootNode;
class CDlodNode;
class CCameraNodeFPS;
class CVisualNode;
class CTriFan;
class CTriMesh;
class CTriStrip;
class CPolypoint;
class CPolyline;

// Scene node properties
class CCuller;
class CCamera;
class CFVFElement;
class CFVFormat;

// Controllers
class CCameraControllerFPS;

// Data Types
class CTransform;

// Geometry
class CGeometry;

// visitors
class CSpatialVistor;
class CTransformVisitor;

//----------------------------------------------------------------------------
// Pointer types :: Scenegraph
//----------------------------------------------------------------------------
typedef std::shared_ptr<ISpatial> ISpatialPtr;
typedef std::shared_ptr<CSpatial> CSpatialPtr;
typedef std::shared_ptr<CRootNode> CRootNodePtr;
typedef std::shared_ptr<CDlodNode> CDlodNodePtr;
typedef std::shared_ptr<CCameraNodeFPS> CCameraNodeFPSPtr;
typedef std::shared_ptr<CVisualNode> CVisualNodePtr;
typedef std::shared_ptr<CTriFan> CTriFanPtr;
typedef std::shared_ptr<CTriMesh> CTriMeshPtr;
typedef std::shared_ptr<CTriStrip> CTriStripPtr;
typedef std::shared_ptr<CPolypoint> CPolypointPtr;
typedef std::shared_ptr<CPolyline> CPolylinePtr;

// Scene node properties
typedef std::shared_ptr<CCuller> CCullerPtr;
typedef std::shared_ptr<CCamera> CCameraPtr;
typedef std::shared_ptr<CFVFormat> CFVFormatPtr;

// Controllers
typedef std::shared_ptr<CCameraControllerFPS> CCameraControllerFPSPtr;





_ENGINE_END



#endif