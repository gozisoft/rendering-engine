#ifndef __EFFECT_BASE_VARIABLE_H__
#define __EFFECT_BASE_VARIABLE_H__

#include "VariableProxy.h"
#include "ShaderTypes.h"

_ENGINE_BEGIN

namespace // Unamed namespace
{
	// GetClassType
	class CClassTypeVistor : public boost::static_visitor<Shader::ClassType>
	{
	public:
		typedef boost::static_visitor<Shader::ClassType> parent_type;
		typedef parent_type::result_type result_type;

		template <class T>
		result_type operator() (const T& pElement) const
		{
			return pElement->GetClassType();
		}
	};

	// GetVariablename
	class CVariableNameVisitor : public boost::static_visitor<const std::string&>
	{
	public:
		typedef boost::static_visitor<const std::string&> parent_type;
		typedef parent_type::result_type result_type;

		template <class T>
		result_type operator() (const T& pElement) const
		{
			return pElement->GetVariableName();
		}
	};
}

// Typedefs for proxy classes
typedef resource_proxy<CConstantBuffer*> CConstantBufferProxy;
typedef resource_proxy<CBufferView*> CBufferViewProxy;
typedef resource_proxy<CTextureView*> CTextureViewProxy;

// This is the base class for any form or shader variable.
// It uses the a C++ std style nameing convention for typedefs.
// This allows for inherant based typedef follow up declarations from child
// classes.
class CVariable
{
public:
	// Unique types (these I am very unsure of having in the global space)
	typedef boost::variant<CConstantBufferProxy*, CBufferViewProxy*, CTextureViewProxy*> shader_resource;

	// SType describes the type of variable. Can be of a number of different types.
	typedef boost::variant<CConstantElement*, CShaderTextureDesc*, CShaderBufferDesc*> type_desc;

	// Constructor and destructor
	CVariable(const shader_resource& shaderRes, const type_desc& tpyeDesc) : 
	m_resource(shaderRes), m_desc(tpyeDesc)
	{

	}

	virtual ~CVariable() 
	{

	}

	// Functions applicable to all desc types
	Shader::ClassType GetClassType() const
	{
		return boost::apply_visitor(CClassTypeVistor(), m_desc);
	}

	// Access the name of the variable
	const std::string& GetVariableName() const
	{
		return boost::apply_visitor(CVariableNameVisitor(), m_desc);
	}

protected:
    // For annotations/variables/variable members:
    // 1) If numeric, pointer to data (for variables: points into backing store,
    //      for annotations, points into reflection heap)
    // OR
    // 2) If object, pointer to the block. If object array, subsequent array elements are found in
    //      contiguous blocks; the Nth block is found by ((<SpecificBlockType> *) pBlock) + N
    //      (this is because variables that are arrays of objects have their blocks allocated contiguously)
    //
    // For structure members:
    //    Offset of this member (in bytes) from parent structure (structure members must be numeric/struct)
	shader_resource m_resource;
	type_desc m_desc;
};

_ENGINE_END

#endif

	//// Typedef shader_resource
	//typedef std::reference_wrapper<CConstantBuffer*> CConstantBufferRef;
	//typedef std::reference_wrapper<CTexture*> CTextureRef;

	//// Typedef type_desc
	//typedef std::reference_wrapper<CConstantFormat*> CConstantFormatRef;
	//typedef std::reference_wrapper<CConstantElement*> CConstantElementRef;
	//typedef std::reference_wrapper<CShaderTexture*> CShaderTextureRef;



