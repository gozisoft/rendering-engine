#ifndef __CSHADER_RESOURCE_VIEW_H__
#define __CSHADER_RESOURCE_VIEW_H__

#include "ShaderVariableDesc.h"

_ENGINE_BEGIN

// This will most likely be a texture
class CShaderTextureDesc : public CShaderDesc
{
	RTTI_DECL; // Run time type info

public:
	CShaderTextureDesc(const std::string& name, size_t numSamples, Shader::ShaderReturnType returnType,
		Shader::ShaderDimensions dimensions, size_t bindPoint=0, size_t bindCount=0);

	Shader::ShaderReturnType GetReturnType() const;
	Shader::ShaderDimensions GetShaderDimensions() const;
	size_t GetNumSamples() const;

protected:
	// Return type (if texture)
	Shader::ShaderReturnType m_returnType;	

	// Dimension (if texture)
	Shader::ShaderDimensions m_dimension;	

	// number of samples of the texture
	size_t m_numSamples;
};

class CShaderBufferDesc : public CShaderDesc
{
	RTTI_DECL; // Run time type info

public:
	CShaderBufferDesc(const std::string& name, size_t bindPoint=0, size_t bindCount=0);

};


#include "ShaderResourceDesc.inl"

_ENGINE_END

#endif