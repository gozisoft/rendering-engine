#include "frameafx.h"
#include "ShaderConstantDesc.h"
#include "BitHacks.h"
#include "Helper.h"

using namespace Engine;
using namespace Shader;

RTTI_IMPL_1(Engine, CConstantElement, CShaderDesc);
RTTI_IMPL_1(Engine, CConstantFormat, CShaderDesc);

CConstantElement::CConstantElement(const std::string& name, DataType dataType, VariableType variable,
	size_t rows, size_t columns, size_t size, size_t offset, void* pValue) :
CShaderDesc(name, CT_CBUFFER, 0, 0),
m_dataType(dataType),
m_variableType(variable),
m_rows(rows),
m_columns(columns),
m_size(size),
m_offset(offset),
m_pDefaultValue(pValue)
{

}

CConstantFormat::CConstantFormat(const std::string& bufferName, size_t paddedSize, size_t bindPoint,
	size_t bindCount)
		: 
CShaderDesc(bufferName, CT_CBUFFER, bindPoint, bindCount),
m_paddedSize(paddedSize),
m_size(0)
{

}

CConstantFormat::CConstantFormat(const ElementContainer& elements, const std::string& bufferName,
	size_t paddedSize, size_t bindPoint, size_t bindCount) 
	: 
CShaderDesc(bufferName, CT_CBUFFER, bindPoint, bindCount),
m_constElements(elements),
m_paddedSize(paddedSize),
m_size(0)
{

}

CConstantFormat::~CConstantFormat()
{
	for (auto i = m_constElements.begin(); i != m_constElements.end(); ++i)
	{
		SafeDelete(*i);
	}
	m_constElements.clear();
}

// **cannot use this function if the array has been pre allocated
const CConstantElement* CConstantFormat::PushElement(const std::string& variableName, DataType dataType,
	VariableType variable, size_t rows, size_t cols, size_t size, size_t offset, UInt8* pDefaultValue)
{
	// Create the constant element
	CConstantElement* pElemet = new CConstantElement(variableName, dataType,
		variable, rows,	cols, size, offset, pDefaultValue);

	// m_paddedSize ::
	// Number of bytes to advance between elements.
	// Typically a multiple of 16 for arrays, vectors, matrices.
	// For scalars and small vectors/matrices, this can be 4 or 8.
	//switch (classType)
	//{
	//case CT_MATRIX: // multiple of 16
	//case CT_VECTOR:	
	//	{
	//		size_t rowSize = rows * SizeOfType(variable);
	//		size_t paddedRow = Maths::AlignToPowerOf2(rowSize, 16U);
	//		size_t totalPadded = paddedRow * cols;
	//		m_paddedSize += totalPadded;
	//	} 
	//	break;
	//};

	m_constElements.push_back(pElemet);
	m_size += ( SizeOfType(variable) * rows * cols );
	return pElemet;
}

const CConstantElement* CConstantFormat::GetElementByName(const std::string& variableName) const
{
	auto itor = std::find_if(m_constElements.begin(), m_constElements.end(), [variableName](CConstantElement* pElement)
	{
		return pElement->GetVariableName() == variableName;
	} );

	assert ( itor != m_constElements.end() );
	return *itor;
}







//CConstantElementPtr CConstantFormat::PushElement(const std::string& variableName, Shader::ClassType classType, VariableType variable,
//	size_t rows, size_t cols, UInt8* pDefaultValue)
//{
//	
//	CConstantElementPtr pElemet = std::make_shared<CConstantElement>(variableName, classType, variable, rows, cols,
//		GetSize(), pDefaultValue);
//
//	// Increment the stride and push back the element on the array
//	size_t typeSize = SizeOfType(variable)*rows*cols;
//	size_t alignmentValue = 0;
//	switch (classType)
//	{
//	case CT_SCALAR:
//	case CT_VECTOR:
//		alignmentValue = 4U;
//		break;
//	case CT_MATRIX:
//		alignmentValue = 16U;
//		break;
//	};
//
//	m_paddedSize += Maths::AlignToPowerOf2(typeSize, alignmentValue);
//	m_size += typeSize;
//	m_constElements.push_back(pElemet);
//	return pElemet;
//}