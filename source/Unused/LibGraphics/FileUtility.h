#ifndef __CFILE_UTILITY_H__
#define __CFILE_UTILITY_H__

#include "Core.h"

_ENGINE_BEGIN

class CFileUtility
{
public:
	static String GetMediaFile(const String& file);

private:
	static String SearchFolders(const String& filename, Char* exeFolder, Char* exeName);
};

//--------------------------------------------------------------------------------------
// Tries to finds a media file by searching in common locations
//--------------------------------------------------------------------------------------
String FindMediaFile(const Char* filename, UInt pathSize = _MAX_PATH);
String FindMediaSearchTypicalDirs(const Char* leafName, const Char* exePath, const Char* exeName, UInt pathSize);
String FindMediaSearchParentDirs(const Char* startAt, const Char* leafName, UInt pathSize);


_ENGINE_END


#endif