#ifndef __CDISCREATE_LOD_NODE_H__
#define __CDISCREATE_LOD_NODE_H__

#include "RootNode.h"

_ENGINE_BEGIN

class CDlodNode : public CRootNode
{
public:
	// Constant used to determin in valid model
	static const UInt INACTIVE_CHILD = RSIZE_MAX; 

    // Construction and destruction.
    CDlodNode(UInt numLevelsOfDetail);
    ~CDlodNode();

	struct NodeMinMax
	{
		float m_localMinDist;
		float m_localMaxDist;
		float m_worldMinDist;
		float m_worldMaxDist;
	};
	typedef std::vector<NodeMinMax> NodeMinMaxArray;

	void SetModelDistance(UInt index, float minDist, float maxDist);
	void SelectLOD(const CCameraPtr& pCamera);

	// Used by culler to determine which spatial to select
	UInt GetActiveChildIndex() const;

	// Accessors for postional data
	const Point3f& GetLocalCenter() const;
	const Point3f& GetWorldCenter() const;

private:
	// This determines which mesh will be placed in the
	// root nodes child array.
	UInt m_activeChildIndex;

    // The point whose distance to the camera determines the correct child to
    // activate.
    Point3f m_localLodCenter;
    Point3f m_worldLodCenter;

	// Squared distances for each LOD interval.
	NodeMinMaxArray m_nodeMinMax;
};

#include "DLODNode.inl"

_ENGINE_END

#endif