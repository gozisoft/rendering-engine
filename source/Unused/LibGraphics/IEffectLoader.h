#ifndef __IEFFECT_LOADER_H__
#define __IEFFECT_LOADER_H__

#include "framefwd.h"

_ENGINE_BEGIN

class IEffectLoader
{
public:
	virtual ~IEffectLoader() {} 

	enum EffectType
	{
		ET_FX_XML
	};

	static IEffectLoaderPtr CreateEffectLoader(EffectType type = ET_FX_XML);

	virtual CBaseEffectPtr LoadEffectDisk(const std::string& headerName) = 0;
};


_ENGINE_END

#endif