#include "frameafx.h"
#include "Technique.h"

using namespace Engine;

CShaderBlock::CShaderBlock(const CShaderPtr& pShader) : 
m_pShader(pShader) 
{
}

CShaderBlock::~CShaderBlock()
{
	// Delete constants
	std::for_each( m_constants.begin(), m_constants.end(), boost::checked_delete<CConstantBuffer> );

	// Delete textures
	std::for_each( m_textures.begin(), m_textures.end(), boost::checked_delete<CTexture> );
}


//CVisualPass::CVisualPass() :
//m_pAlphaState(nullptr),
//m_pCullState(nullptr),
//m_pDepthState(nullptr),
//m_pOffsetState(nullptr),
//m_pStencilState(nullptr),
//m_pWireState(nullptr)
//{
//
//}
//
//CVisualPass::~CVisualPass()
//{
//	//SafeDelete(m_pAlphaState);
//	//SafeDelete(m_pCullState);
//	//SafeDelete(m_pDepthState);
//	//SafeDelete(m_pOffsetState);
//	//SafeDelete(m_pStencilState);
//	//SafeDelete(m_pWireState);
//}

//CSceneTechnique::CSceneTechnique(const CVisualPass* passes, size_t numPasses) :
//m_visualPasses(passes, passes + numPasses)
//{
//
//}
//
//CSceneTechnique::CSceneTechnique(const VisualPassArray& passes) : 
//m_visualPasses(passes)
//{
//
//}
//
//CSceneTechnique::~CSceneTechnique()
//{
//	namespace bl = boost;
//
//	for (auto itor = m_visualPasses.begin(); itor != m_visualPasses.end(); ++itor)
//	{
//		CVisualPass& pass = *itor;
//		bl::checked_delete(pass.m_pAlphaState);
//		bl::checked_delete(pass.m_pCullState);
//		bl::checked_delete(pass.m_pDepthState);
//		bl::checked_delete(pass.m_pOffsetState);
//		bl::checked_delete(pass.m_pStencilState);
//		bl::checked_delete(pass.m_pWireState);
//	}
//
//	m_visualPasses.clear();
//}
//


