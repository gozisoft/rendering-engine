#include "frameafx.h"
#include "FXCompiler.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include "BaseEffect.h"

using namespace Engine;
using boost::property_tree::iptree;
typedef iptree::value_type& valueType;

extern CShader* CompileHLSLShader(CShader::ShaderVersion version, CShader::ShaderType shaderType,
	std::string& name, std::string& program);

CXMLProcessor::CXMLProcessor(std::istream& inputStream)
{
	namespace pt = boost::property_tree;
	using pt::xml_parser::no_comments;
	using pt::xml_parser::no_concat_text;
	using pt::xml_parser::trim_whitespace;

	// Load the XML file into the property tree. If reading fails
	// (cannot open file, parse error), an exception is thrown.
	int flags = no_comments | trim_whitespace;
	read_xml(inputStream, m_tree, flags);

	// Go over the context children
	iptree& contextLevel = m_tree.get_child("Shader.Contexts");
	BOOST_FOREACH(valueType contexts, contextLevel)
	{
		// Go over the leaves of the contexts level [context[n]->pass[n]->compile(vertex, pixel, etc)]
		BuildContext(contexts.second);
	}
}

CXMLProcessor::~CXMLProcessor()
{
}

void CXMLProcessor::BuildContext(iptree &contextLevel)
{
	// Context attributes
	iptree& attributeLevel = contextLevel.get_child("<xmlattr>");

	// Context data to be obtained
	std::string& contexName = attributeLevel.get<std::string>("name");
	std::string& techVersion = attributeLevel.get<std::string>("version");

	// Determine the shader version
	CShaderVersionParser versionParser;
	CShader::ShaderVersion version = versionParser.PraseSVersion(techVersion);

	// Go to the pass level now and build an array of them
	iptree& passLevel = contextLevel.get_child("Pass");
	BOOST_FOREACH(valueType pass, passLevel)
	{
		BuildPass(pass.second, version);
	}
}

void CXMLProcessor::BuildPass(iptree &passLevel, CShader::ShaderVersion version)
{
	// The visual pass to be filled
	SVisualPass* visPass = new SVisualPass;
	
	// Now go over the pass leaves
	BOOST_FOREACH(valueType pass, passLevel)
	{
		if (pass.first == "Vertex")
		{
			BOOST_FOREACH( valueType vertex, pass.second.get_child("") )
			{
				if (vertex.first == "function")
				{
					std::string functionName = vertex.second.get<std::string>("");
					std::string shaderData = ReadShaderData(functionName);
					visPass->m_pVertexShader = CompileHLSLShader(version, CShader::ST_VERTEX,
						functionName, shaderData);
				}
			}
		}
	}
}

std::string CXMLProcessor::ReadShaderData(std::string& name)
{
	std::stringstream ss;
	ss << "Shader" << "." << name;

	// Get the name of the shader and load in the shader text
	iptree& codeLevel = m_tree.get_child( ss.str() );
	BOOST_FOREACH(valueType data, codeLevel)
	{
		// Load in the shader Text
		if (data.first == "HLSL") 
		{		
			std::string shaderProgram = data.second.get<std::string>("");
			return shaderProgram;
		}
	}

	return std::string();
}

