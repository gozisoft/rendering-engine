#include "frameafx.h"
#include "ConstantVariables.h"
#include "NumericVariables.h"

using namespace Engine;
using boost::checked_delete;

// default constructor
constant_object::constant_object(CConstantBuffer* pConstBuffer, CConstantFormat* pFormat) : 
CVariable(pConstBuffer, pFormat),
m_variables(pFormat->GetElementCount())
{
	// Iterate over the variables
	for (size_t i = 0; i < pFormat->GetElementCount(); ++i)
	{
		// Conceptual constness
		CConstantElement* pElement = 
			const_cast<CConstantElement*>( pFormat->GetElementByIndex(i) );

		// Create the correct variable based on data type and variable type
		switch ( pElement->GetDataType() )
		{
		case Shader::DT_SCALAR:
			switch ( pElement->GetVariableType() )
			{
			case Shader::VT_BOOL:
				m_variables[i] = new scalar_bool(pConstBuffer, pElement);
			case Shader::VT_INT:
				m_variables[i] = new scalar_int(pConstBuffer, pElement);
			case Shader::VT_UINT:
				m_variables[i] = new scalar_uint(pConstBuffer, pElement);
			case Shader::VT_FLOAT:
				m_variables[i] = new scalar_float(pConstBuffer, pElement);
				break;
			};
			break;

		case Shader::DT_VECTOR:
			switch ( pElement->GetVariableType() )
			{
			case Shader::VT_BOOL:
				m_variables[i] = new vector_bool(pConstBuffer, pElement);
				break;
			case Shader::VT_INT:						
				m_variables[i] = new vector_int(pConstBuffer, pElement);
				break;
			case Shader::VT_UINT:
				m_variables[i] = new vector_uint(pConstBuffer, pElement);
				break;
			case Shader::VT_FLOAT:
				m_variables[i] = new vector_float(pConstBuffer, pElement);
				break;
			};
			break;

		case Shader::DT_MATRIX:
			switch ( pElement->GetVariableType() )
			{
			case Shader::VT_BOOL:
				m_variables[i] = new matrix_bool(pConstBuffer, pElement);
				break;
			case Shader::VT_INT:						
				m_variables[i] = new matrix_int(pConstBuffer, pElement);
				break;
			case Shader::VT_UINT:
				m_variables[i] = new matrix_uint(pConstBuffer, pElement);
				break;
			case Shader::VT_FLOAT:
				m_variables[i] = new matrix_float(pConstBuffer, pElement);
				break;
			}
			break;
		}; // switch

	}
}

constant_object::~constant_object()
{
	std::for_each( m_variables.begin(), m_variables.end(),
		checked_delete<numeric_variable> );
}