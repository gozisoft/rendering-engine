#ifndef __CSPATIAL_H__
#define __CSPATIAL_H__

#include "ISpatial.h"
#include "Transform.h"
#include "Sphere.h"

_ENGINE_BEGIN

class CSpatial : public ISpatial, public std::enable_shared_from_this<CSpatial>
{
public:
	enum CullMode
	{
		// Determine visibility state by comparing the world bounding volume
		// to culling planes.
		CM_DYNAMIC,
		// Force the object to be culled.  If a Node is culled, its entire
		// subtree is culled.
		CM_ALWAYS,
		// Never cull the object.  If a Node is never culled, its entire
		// subtree is never culled.  To accomplish this, the first time such
		// a Node is encountered, the bNoCull parameter is set to 'true' in
		// the recursive chain GetVisibleSet/OnGetVisibleSet.
		CM_NEVER,
		NUM_CULL_MODES
	};

	// Virtual destructor
	virtual ~CSpatial();

	// Update from root function
	void UpdateFromRoot();

	// Ability to set the world transform
	void SetWorldTransform(const CTransform& transform);

	// Access to the world transform
	const CTransform& GetWorldTransform() const;

	// Access to the current cullmode
	CullMode GetCullMode() const;

	// Allows the user to set an item as visible or not
	void SetCullMode(CullMode mode);

	// Access the world worldbound (worldspace)
	const CSphere& GetWorldBound() const;

	// Access to the parent
	const CSpatialPtr& GetParent() const;

	// Ability to set the parent
	void SetParent(const CSpatialPtr& parent);

protected:
	// Protected constructor to prevent construction
	// of this class
	CSpatial();

	// Transformation data
	CTransform m_worldTransform;

	// Bounding volume
	CSphere m_worldBound;

	// Culling mode
	CullMode m_cullMode;

	// Pointer to parent
	CSpatialPtr m_parent;
};

#include "Spatial.inl"

_ENGINE_END

#endif