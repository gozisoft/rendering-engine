#ifndef FRAMEWORK_HEADERS_H
#define FRAMEWORK_HEADERS_H

// -------------------------------------------------------------------------------------------------------------
// Interfaces
// -------------------------------------------------------------------------------------------------------------

// Rendering
#include "IRenderer.h"
#include "IResource.h"
#include "IView.h"
#include "IInputFormat.h"
#include "IShader.h"

// Image
#include "IImage.h"
#include "IImageLoader.h"

// Scene graph
#include "ISpatial.h"


#endif