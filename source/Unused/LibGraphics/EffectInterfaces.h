#ifndef __EffECT_INTERFACES_H__
#define __EffECT_INTERFACES_H__

#include "framefwd.h"

_ENGINE_BEGIN

//class IEffectVariable
//{
//public:
//	virtual IEffectScalarVariable* AsScalar() = 0;
//	virtual IEffectVectorVariable* AsVector() = 0;
//	virtual IEffectMatrixVariable* AsMatrix() = 0;
//	virtual IEffectStringVariable* AsString() = 0;
//	virtual IEffectResourceVariable* AsResourceVariable() = 0;
//
//    virtual void SetRawValue(const void *pData, UInt offset, UInt count) = 0;
//    virtual void GetRawValue(void *pData, UInt offset, UInt count) = 0;
//};
//
//class IEffectScalarVariable : public IEffectVariable
//{
//public:
//	virtual IEffectScalarVariable* AsScalar() = 0;
//	virtual IEffectVectorVariable* AsVector() = 0;
//	virtual IEffectMatrixVariable* AsMatrix() = 0;
//	virtual IEffectStringVariable* AsString() = 0;
//	virtual IEffectResourceVariable* AsResourceVariable() = 0;
//
//    virtual void SetRawValue(const void *pData, UInt offset, UInt count) = 0;
//    virtual void GetRawValue(void *pData, UInt offset, UInt count) = 0;
//};
//
//class IEffectVectorVariable : public IEffectVariable
//{
//public:
//	virtual IEffectScalarVariable* AsScalar() = 0;
//	virtual IEffectVectorVariable* AsVector() = 0;
//	virtual IEffectMatrixVariable* AsMatrix() = 0;
//	virtual IEffectStringVariable* AsString() = 0;
//	virtual IEffectResourceVariable* AsResourceVariable() = 0;
//
//    virtual void SetRawValue(const void *pData, UInt offset, UInt count) = 0;
//    virtual void GetRawValue(void *pData, UInt offset, UInt count) = 0;
//
//	virtual void SetBoolVector(const bool *pData) = 0; 
//	virtual void GetBoolVector(bool *pData) = 0; 
//
//	virtual void SetIntVector(const int *pData) = 0;
//	virtual void GetIntVector(int *pData) = 0;
//
//	virtual void SetFloatVector(const float *pData) = 0;
//	virtual void GetFloatVector(float *pData) = 0;
//
//	virtual void SetBoolVectorArray(const bool *pData, UInt offset, UInt count) = 0;
//	virtual void GetBoolVectorArray(bool *pData, UInt offset, UInt count) = 0; 
//
//	virtual void SetIntVectorArray(const int *pData, UInt offset, UInt count) = 0;
//	virtual void GetIntVectorArray(int *pData, UInt offset, UInt count) = 0;
//
//	virtual void SetFloatVectorArray(const float *pData, UInt offset, UInt count) = 0;
//	virtual void GetFloatVectorArray(float *pData, UInt offset, UInt count) = 0;
//};
//
//class IEffectMatrixVariable : public IEffectVariable
//{
//public:
//
//	virtual IEffectScalarVariable* AsScalar() = 0;
//	virtual IEffectVectorVariable* AsVector() = 0;
//	virtual IEffectMatrixVariable* AsMatrix() = 0;
//	virtual IEffectStringVariable* AsString() = 0;
//	virtual IEffectResourceVariable* AsResourceVariable() = 0;
//
//    virtual void SetRawValue(const void *pData, UInt offset, UInt count) = 0;
//    virtual void GetRawValue(void *pData, UInt offset, UInt count) = 0;
//
//	virtual void SetMatrix(const float *pData) = 0;
//	virtual void GetMatrix(float *pData) = 0;
//
//	virtual void SetMatrixArray(const float *pData, UInt offset, UInt count) = 0;
//	virtual void GetMatrixArray(float *pData, UInt offset, UInt count) = 0;
//
//	virtual void SetMatrixTranspose(const float *pData) = 0;
//	virtual void GetMatrixTranspose(float *pData) = 0;
//};
//
//class IEffectResourceVariable : public IEffectVariable
//{
//public:
//	virtual IEffectScalarVariable* AsScalar() = 0;
//	virtual IEffectVectorVariable* AsVector() = 0;
//	virtual IEffectMatrixVariable* AsMatrix() = 0;
//	virtual IEffectStringVariable* AsString() = 0;
//	virtual IEffectResourceVariable* AsResourceVariable() = 0;
//
//    virtual void SetRawValue(const void *pData, UInt offset, UInt count) = 0;
//    virtual void GetRawValue(void *pData, UInt offset, UInt count) = 0;
//
//    virtual void SetResource(const ShaderResource& pResource) = 0;
//    virtual void GetResource(ShaderResource& pResource) = 0;
//
//};



_ENGINE_END

#endif