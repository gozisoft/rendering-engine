#ifndef __CTECHNIQUE_H__
#define __CTECHNIQUE_H__

#include "framefwd.h"

_ENGINE_BEGIN

class CVisualPassBuilder;

class CTechnique
{
public:
	// Tags
	struct random_tag { };
	struct name_tag { };

	// Typedefs
	typedef boost::multi_index::random_access< 
		boost::multi_index::tag<random_tag>
	> RandomIndex;

	typedef boost::multi_index::hashed_unique<
		boost::multi_index::tag<name_tag>,
		boost::multi_index::member<CTechnique, std::string, &CTechnique::name>
	> NameIndex;

	typedef boost::multi_index::multi_index_container <
		CTechnique,
		boost::multi_index::indexed_by <
		RandomIndex, 
		NameIndex
		> // index_by
	> Techniques;

	// Typedef list for easy access to multi_index
	typedef Techniques::index<random_tag>::type tech_random;
	typedef Techniques::index<name_tag>::type tech_by_name;

	// Add a single technique to the base effect [C style array method]
	void AddPass(const CVisualPass& pass);

	// Ass a group of passes
	void AddPasses(const CVisualPass** passes, UInt numPasses);

	// Get a pass by name
	const CVisualPass* GetPass(const std::string& name);

	// Get a pass by index
	const CVisualPass* GetPass(UInt index);

	// The number of techniques within this base effect
	UInt GetNumPasses() const;

protected:
	// Array of specified techniques ( might be better using std::set? )
	Techniques m_techniques;

};

#include "Technique.inl"

_ENGINE_END

#endif


//// Does not own these objects there is no deletor
//struct SVisualPass
//{
//	CVertexShader* m_pVertexShader;
//	CPixelShader* m_pPixelShader;
//	CAlphaState* m_pAlphaState;
//	CCullState* m_pCullState;
//	CDepthState* m_pDepthState;
//	COffsetState* m_pOffsetState;
//	CStencilState* m_pStencilState;
//	CWireState* m_pWireState;
//	CZBufferState* m_pZBufferState;
//};

//class CShaderBlock
//{
//public:
//	// Typedef for reference wrappers
//	typedef std::reference_wrapper<CConstantBufferPtr> CBufferRef;
//	typedef std::reference_wrapper<CViewPtr> CViewRef;
//
//	// Typedef for arrays
//	typedef std::vector<CBufferRef> CBuffers;
//	typedef std::vector<CViewRef> CViews;
//
//	// Constructor and destructor : owns pshader object
//	CShaderBlock(const CShaderPtr& pShader);
//	~CShaderBlock();
//
//	// Constant buffers
//	CBuffers m_constants;
//
//	// Buffer Views (Textures, buffers)
//	CViews m_views;
//
//	// Samplers
//
//	// Pointer to owning shader object
//	CShaderPtr m_pShader;
//};


//// Manages the lifetime of an array of CVisualPass's
//// The SceneTechnique is also a non copyable class to prevent
//// any non permitted deletions of the class during runtime.
//// The lifetime of the SceneTechnique is governed by the 
//// BaseEffect class.
//class CSceneTechnique
//{
//public:
//	typedef std::vector<CVisualPass> VisualPassArray;
//
//	CSceneTechnique(const CVisualPass* passes, size_t numPasses); 
//	CSceneTechnique(const VisualPassArray& passes);
//	~CSceneTechnique();
//
//	// Member access
//    size_t GetNumPasses() const;
//    const CVisualPass& GetPass(size_t passIndex) const;
//	CVisualPass& GetPass(size_t passIndex);
//
//	// VisualPass access
//	const CVertexShader* GetVertexShader(size_t index) const;
//	const CPixelShader* GetPixelShader(size_t index) const;
//	const CAlphaState* GetAlphaState(size_t index) const;
//	const CCullState* GetCullState(size_t index) const;
//	const CDepthState* GetDepthState(size_t index) const;
//	const COffsetState* GetOffsetState(size_t index) const;
//	const CStencilState* GetStencilState(size_t index) const;
//	const CWireState* GetWireState(size_t index) const;
//
//protected:
//	// An array of visual pass structures used to describe shaders
//	// and their assoiciated states
//	VisualPassArray m_visualPasses;
//
//private: 
//	// non copyable class
//	CSceneTechnique();
//	CSceneTechnique(const CSceneTechnique&);
//	CSceneTechnique& operator = (const CSceneTechnique&);
//
//};