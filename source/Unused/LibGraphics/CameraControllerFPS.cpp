#include "Frameafx.h"
#include "CameraControllerFPS.h"
#include "CameraNodeFPS.h"
#include "IMouseController.h"
#include "IEventReciever.h"
#include "StringFunctions.h"
#include "WinMouseEvent.h"
#include "WinKeyboardEvent.h"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#undef WIN32_LEAN_AND_MEAN
#include <xnamath.h>

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CCameraControllerFPS, "CCameraControllerFPS", "ISceneControllerFPS");

CCameraControllerFPS::CCameraControllerFPS(IMouseControllerPtr pMouse, float roateSpeed, float moveSpeed, SKeyMap* pKeyMapVector,
	UInt KeyMapSize, bool freeLook, bool centerMouse, bool invert) :
m_pMouse(pMouse),
m_yaw(0.0f),
m_pitch(0.0f),
m_maxVertAngle(85.0f),
m_rotateSpeed(roateSpeed),
m_moveSpeed(moveSpeed),
m_vertMovement(freeLook),
m_centerMouse(centerMouse),
m_mouseYDirection( invert ? -1.0f : 1.0f ),
m_firstUpdate(true)
{
	// zero the keys
	SetAllKeysUp();

	// zero the buttons
	SetAllButtonsUp();

	if (!pKeyMapVector || !KeyMapSize)
	{
		// create default key map
		m_keyMap.push_back( SKeyMap(KA_MOVE_FORWARD, KEY_UP) );
		m_keyMap.push_back( SKeyMap(KA_MOVE_BACKWARD, KEY_DOWN) );
		m_keyMap.push_back( SKeyMap(KA_STRAFE_LEFT, KEY_LEFT) );
		m_keyMap.push_back( SKeyMap(KA_STRAFE_RIGHT, KEY_RIGHT) );
	}
	else
	{
		// create custom 
		SetKeyMap(pKeyMapVector, KeyMapSize);
	}

}

CCameraControllerFPS::~CCameraControllerFPS()
{
	m_keyMap.clear();
}

// function moves the camera by given speed.
void CCameraControllerFPS::AnimateNode(CSpatial* pSpatial, double time, double deltaTime)
{
	UNUSED_PARAMETER(time);

	if (!pSpatial)
		return;

	// cast the spatial to a camera
	CCameraNodeFPS *pCameraNode = static_cast<CCameraNodeFPS*>(pSpatial);

	if (m_firstUpdate)
	{
		if (pCameraNode && m_pMouse)
		{
			m_pMouse->SetRelativePos( Point2f(0.5f, 0.5f) );
			m_lastCursorPos = m_pMouse->GetRelativeMousePos();
		}

		m_firstUpdate = false;
	}

	if (m_mouseButtons[0] == true)
	{
		// Get current position of mouse
		const Point2l& curMousePos = m_pMouse->GetMousePos();

		// Calc how far it's moved since last frame
		const Point2l& curMouseDelta = curMousePos - m_lastCursorPos;

		// Record current position for next time
		m_lastCursorPos = curMousePos;

		// Reset cursor position to the centre of the window.
		if (m_centerMouse)
		{
			m_pMouse->SetRelativePos( Point2f(0.5f, 0.5f) );
			m_lastCursorPos = m_pMouse->GetMousePos();
		}

		// Smooth the relative mouse data over a few frames so it isn't 
		// jerky when moving slowly at low frame rates.
		float percentOfNew = 0.5f;
		float percentOfOld = 1.0f - percentOfNew;
		m_mouseDelta = (m_mouseDelta * percentOfOld) + (curMouseDelta * percentOfNew);

		// Assign the rotational velocity of this frame		
		m_rotVelocity = m_mouseDelta * m_rotateSpeed;

		// Update the pitch & yaw angle based on mouse movement
		float yawDelta = m_rotVelocity[0];
		float pitchDelta = m_rotVelocity[1] * m_mouseYDirection;

		// Invert yaw if requested
		m_yaw += yawDelta;
		m_pitch += pitchDelta; 
		
        // Limit pitch to straight up or straight down
		m_pitch = std::max( -Const<float>::HALF_PI(), m_pitch );
        m_pitch = std::min( +Const<float>::HALF_PI(), m_pitch );
	}
	else
	{
		m_lastCursorPos =  m_pMouse->GetMousePos();
	}

	Vector3f constant_motion = cml::zero_3D();
	if (m_cursorkeys[KA_MOVE_FORWARD])
		constant_motion[2] += 1.0f;	

	if (m_cursorkeys[KA_MOVE_BACKWARD])
		constant_motion[2] -= 1.0f;

	if (m_cursorkeys[KA_STRAFE_RIGHT])
		constant_motion[0] += 1.0f;

	if (m_cursorkeys[KA_STRAFE_LEFT])
		constant_motion[0] -= 1.0f;

	// Test for zero case
	if ( !IsEqual( cml::length(constant_motion), 0.0f ) )
    {
		constant_motion.normalize();
		constant_motion *= m_moveSpeed;
	}

	float elapsedTime = static_cast<float>(deltaTime * 0.001);
	Vector3f positionDelta = constant_motion * elapsedTime;

	/////////////////////////////////////////////////////////////////////////////////////////
	// Rotation
	/////////////////////////////////////////////////////////////////////////////////////////

	// Make a rotation matrix based on the camera's yaw & pitch
	Matrix4f cameraRot;
//	cml::matrix_rotation_euler(cameraRot, 0.0f, m_pitch, m_yaw, cml::euler_order_zxy);
	cameraRot = *(Matrix4f*)&XMMatrixRotationRollPitchYaw(m_pitch, m_yaw, 0.0f);

	//XMVECTOR test = XMVectorSet(5.0f, 5.0f, 2.0f, 1.0f);
	//Vector3f testResult = *(Vector3f*)&XMVector3Transform( test, *(XMMATRIX*)&cameraRot );
	//Vector3f test1(5.0f, 5.0f, 2.0f);
	//Vector3f testResult1 = cml::transform_point(cameraRot,test1);

    // Transform vectors based on camera's rotation matrix
	//Vector3f worldY = cml::transform_vector(cameraRot, cml::axis_3D(1));
	//Vector3f worldZ = cml::transform_vector(cameraRot, cml::axis_3D(2));
	XMVECTOR tempY = XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f);
	XMVECTOR tempZ = XMVectorSet(0.0f, 0.0f, 1.0f, 1.0f);
	Vector3f worldY = *(Vector3f*)&XMVector3TransformCoord( tempY, *(XMMATRIX*)&cameraRot );
	Vector3f worldZ = *(Vector3f*)&XMVector3TransformCoord( tempZ, *(XMMATRIX*)&cameraRot );

	// Transform the position delta by the camera's rotation
	//Vector3f posWorldDelta = cml::transform_vector(cameraRot, positionDelta);
	XMVECTOR tempPosDelta = XMVectorSet(positionDelta[0], positionDelta[1], positionDelta[2], 1.0f);
	Vector3f posWorldDelta = *(Vector3f*)&XMVector3TransformCoord( tempPosDelta, *(XMMATRIX*)&cameraRot );

	/////////////////////////////////////////////////////////////////////////////////////////
	// Movement
	/////////////////////////////////////////////////////////////////////////////////////////

	// Transform the position delta by the camera's rotation 
	CTransform& transform = pCameraNode->GetLocalTransform();
	Vector3f position = transform.GetPostion();

	// Rotate the camera
	transform.SetRotation(cameraRot);

	// Move the eye position 
	position += posWorldDelta;

	// Set the local position
	transform.SetPosition(position);

	 // Update the lookAt position based on the eye position 
	Vector3f lookat = position + worldZ;
	pCameraNode->SetTarget(lookat);

}

bool CCameraControllerFPS::HandleEvent(const IEventData& event)
{
	if ( SKeyEvent::sk_EventType == event.GetEventType() )
	{
		const SKeyEvent& ed = static_cast<const SKeyEvent&>(event);
		SKeyInput keyInput = ed.m_keyInput;

		for (KeyMapVector::iterator i = m_keyMap.begin(); i != m_keyMap.end(); ++i)
		{
			if ( (*i).keycode == keyInput.key )
			{
				m_cursorkeys[(*i).action] = keyInput.pressedDown;
				return true; // event absorbed
			}
		}
	}
	else if ( SMouseEvent::sk_EventType == event.GetEventType() )
	{
		const SMouseEvent& ed = static_cast<const SMouseEvent&>(event);
		SMouseInput mouseInput = ed.m_mouseInput;

		//if (mouseInput.mouseEvent == MIE_MOUSE_MOVED)
		//{
		//	// Get current position of mouse
		//	m_curMousePos = m_pMouse->GetMousePos();
		//}

		m_mouseButtons[0] = mouseInput.isLeftPressed();
		m_mouseButtons[1] = mouseInput.isRightPressed();
		return true;
	}


	return false;
}

void CCameraControllerFPS::SetKeyMap(SKeyMap *pKeyMap, UInt size)
{
	// clear keymap
	m_keyMap.clear();

	// loop through keymap
	for (UInt32 i = 0; i < size; ++i)
	{
		// find the assigned action
		// and assign the approriate
		// keycode
		switch (pKeyMap[i].action)
		{
		case KA_MOVE_FORWARD:
			m_keyMap.push_back( SKeyMap(KA_MOVE_FORWARD, pKeyMap[i].keycode) );
			break;
		case KA_MOVE_BACKWARD:
			m_keyMap.push_back( SKeyMap(KA_MOVE_BACKWARD, pKeyMap[i].keycode) );
			break;
		case KA_STRAFE_LEFT:
			m_keyMap.push_back( SKeyMap(KA_STRAFE_LEFT, pKeyMap[i].keycode) );
			break;
		case KA_STRAFE_RIGHT:
			m_keyMap.push_back( SKeyMap(KA_STRAFE_RIGHT, pKeyMap[i].keycode) );
			break;
		};
	}
}


/*
Vector3f GetHorizontalAngle(const Vector3f& vector)
{
	Vector3f angle;

	const double x = vector[0];
	const double y = vector[1];
	const double z = vector[2];

	// Set the yaw (y axis)
	double tmp = std::atan2(x, z) * Const<double>::TO_DEG();
	angle[1] = tmp;

	if (angle[1] < 0)
		angle[1] += 360;
	if (angle[1] >= 360)
		angle[1] -= 360;

	// Set the pitch (x axis)
	double lookLengthOnXZ = std::sqrt(x*x + z*z);
	angle[0] = std::atan2(y, lookLengthOnXZ) * Const<double>::TO_DEG() - 90.0;

	if (angle[0] < 0)
		angle[0] += 360;
	if (angle[0] >= 360)
		angle[0] -= 360; 

	return angle;
}
*/


/*
// function moves the camera by given speed.
void CCameraControllerFPS::AnimateNode(CSpatial* pSpatial, double time, double deltaTime)
{
	UNUSED_PARAMETER(time);

	if (!pSpatial)
		return;

	// cast the spatial to a camera
	CCameraNodeFPS *pCameraNode = static_cast<CCameraNodeFPS*>(pSpatial);

	if (m_firstUpdate)
	{
		if (pCameraNode && m_pMouse)
		{
			m_pMouse->SetRelativePos( Point2f(0.5f, 0.5f) );
			m_lastCursorPos = m_pMouse->GetMousePos();
		}

		m_firstUpdate = false;
	}

	float fpitch = 0.0f;
	float fyaw = 0.0f;
	Point2f rotVelocity;
	if (m_mouseButtons[0] == true)
	{
		// Process mouse input
		{
			// Get current position of mouse
			Point2l curMousePos = m_pMouse->GetMousePos();

			// Calc how far it's moved since last frame
			Point2f curMouseDelta = curMousePos - m_lastCursorPos;

			// Record current position for next time
			m_lastCursorPos = curMousePos;

			// Reset cursor position to the centre of the window.
			if (m_centerMouse)
			{
				m_pMouse->SetRelativePos( Point2f(0.5f, 0.5f) );
				m_lastCursorPos = m_pMouse->GetMousePos();
			}

			// Smooth the relative mouse data over a few frames so it isn't 
			// jerky when moving slowly at low frame rates.
			float percentOfNew = 1.0f / 2.0f;
			float percentOfOld = 1.0f - percentOfNew;
			m_mouseDelta = (m_mouseDelta * percentOfOld) + (curMouseDelta * percentOfNew);

			// Assign the rotational velocity of this frame		
			rotVelocity[0] = m_mouseDelta[0] * m_rotateSpeed * m_mouseYDirection;
			rotVelocity[1] = m_mouseDelta[1] * m_rotateSpeed;

			// Update the pitch & yaw angle based on mouse movement
			fyaw += rotVelocity[0];
			fpitch += rotVelocity[1];
		}

		//OStringStream oss;
		//oss << fpitch * Const<float>::TO_DEG() << ieS(" ") << fyaw * Const<float>::TO_DEG() << std::endl;
		//OutputDebugString(oss.str().c_str());
	}

	// pitch is about local x axes
	// calculate around arbitrary x axes and rotate by current rotation
	Quaternionf pitch(cml::axis_3D(0), fpitch);

    // Get head rotation from the current object orientation transform
	Quaternionf head_rotation( pCameraNode->GetWorldTransform() );

	// Determine the new rotation
	Quaternionf new_rotation(pitch);
	new_rotation *= head_rotation;

	// get new look at vector
	Vector3f new_look_at = Rotate(head_rotation, cml::axis_3D(2));

	// get new look at flattened on the x-z plane
	Vector3f new_look_at_flatten(new_look_at);
	new_look_at_flatten[1] = 0.0f;
	new_look_at_flatten.Normalise();

	// check against current look at
	// get current look at vector
	Vector3f current_look_at = Rotate(head_rotation, cml::axis_3D(2));

	// get current look at flattened on the x-z plane
	Vector3f current_look_at_flatten(current_look_at);
	current_look_at_flatten[1] = 0.0f;
	current_look_at_flatten.Normalise();

	if ( IsEqual( Mag(new_look_at_flatten), 0.0f ) )
	{
		// new look at is straight up, don't apply pitch rotation
		new_rotation = head_rotation;
	}
	else
	{
		// angle between two should be zero (otherwise the pitch has changed and the look at has flipped)
		// cos of angle should be 1
		float cos_angle = cml::dot(current_look_at_flatten, new_look_at_flatten);
		if ((cos_angle < 0.0f) || ( IsEqual(cos_angle, 0.0f, 0.005f) ))
		{
			//look at has flipped, undo pitch rotation
			new_rotation = head_rotation;
		}
	}

	// no roll on camera so yaw is about arbitrary up axes
	Quaternionf yaw(cml::axis_3D(1), fyaw);
	new_rotation *= yaw; // combine your yaw and pitch here

	// Set the head rotation of the camera
	pCameraNode->SetHeadRotation(new_rotation);

	/////////////////////////////////////////////////////////////////////////////////////////
	// Movement
	/////////////////////////////////////////////////////////////////////////////////////////

	Vector3f constant_motion;
	if (m_cursorkeys[KA_MOVE_FORWARD])
		constant_motion[2] += 1.0f;	

	if (m_cursorkeys[KA_MOVE_BACKWARD])
		constant_motion[2] -= 1.0f;

	if (m_cursorkeys[KA_STRAFE_RIGHT])
		constant_motion[0] += 1.0f;

	if (m_cursorkeys[KA_STRAFE_LEFT])
		constant_motion[0] -= 1.0f;

	Vector3f movement;
    if ( !IsEqual(Mag(constant_motion), 0.0f) )
    {
        //constant motion in frame
        movement = constant_motion;
        movement.Normalise();
    }

	if ( Mag(movement) )
	{
        Vector3f position = Rotate(new_rotation, movement);
        position *= m_moveSpeed * (float)(deltaTime * 0.001);

		position += pCameraNode->GetPostion();

        pCameraNode->SetPosition(position);
	}

}
*/

/*
// function moves the camera by given speed.
void CCameraControllerFPS::AnimateNode(CSpatial* pSpatial, double time, double deltaTime)
{
	UNUSED_PARAMETER(time);

	if (!pSpatial)
		return;

	// cast the spatial to a camera
	CCameraNodeFPS *pCameraNode = static_cast<CCameraNodeFPS*>(pSpatial);

	if (m_firstUpdate)
	{
		if (pCameraNode && m_pMouse)
		{
			m_pMouse->SetRelativePos( Point2f(0.5f, 0.5f) );
			m_lastCursorPos = m_pMouse->GetMousePos();
		}

		m_firstUpdate = false;
	}

	// Get latest pos.
	D3DXVECTOR3 KeyboardDirection = D3DXVECTOR3( 0, 0, 0 );

	if (m_cursorkeys[KA_MOVE_FORWARD])
		KeyboardDirection.z += 1.0f;
	if (m_cursorkeys[KA_MOVE_BACKWARD])
		KeyboardDirection.z -= 1.0f;
	if (m_cursorkeys[KA_STRAFE_RIGHT])
		KeyboardDirection.x += 1.0f;
	if (m_cursorkeys[KA_STRAFE_LEFT])
		KeyboardDirection.x -= 1.0f;

	D3DXVec3Normalize(&KeyboardDirection, &KeyboardDirection);

    // Scale the acceleration vector
    KeyboardDirection *= m_moveSpeed;

    // Simple euler method to calculate position delta
	D3DXVECTOR3 vPosDelta = KeyboardDirection * (deltaTime * 0.001);

	Point2f mouseDelta;
	if (m_mouseButtons[0] == true) // Process mouse input
	{	
		// Get current position of mouse
		Point2l curMousePos = m_pMouse->GetMousePos();

		// Calc how far it's moved since last frame
		Point2f curMouseDelta = curMousePos - m_lastCursorPos;

		// Record current position for next time
		m_lastCursorPos = curMousePos;

		// Reset cursor position to the centre of the window.
		if (m_centerMouse)
		{
			m_pMouse->SetRelativePos( Point2f(0.5f, 0.5f) );
			m_lastCursorPos = m_pMouse->GetMousePos();
		}

		// Smooth the relative mouse data over a few frames so it isn't 
		// jerky when moving slowly at low frame rates.
		float fPercentOfNew = 1.0f / 2.0f;
		float fPercentOfOld = 1.0f - fPercentOfNew;
		mouseDelta = (m_mouseDelta * fPercentOfOld) + (curMouseDelta * fPercentOfNew);

		// Assign the rotational velocity of this frame
		m_rotVelocity = mouseDelta * m_rotateSpeed;

		// Update the pitch & yaw angle based on mouse movement
		float angleY = m_rotVelocity[1] * m_mouseYDirection;
		float angleX = m_rotVelocity[0];

		m_pitch += angleY;
		m_yaw += angleX;

		// Limit pitch to straight up or straight down
		m_pitch = Max( -Const<float>::HALF_PI(), m_pitch );
		m_pitch = Min( +Const<float>::HALF_PI(), m_pitch );
	}

    // Make a rotation matrix based on the camera's yaw & pitch
	D3DXMATRIX mCameraRot;
	D3DXMatrixRotationYawPitchRoll(&mCameraRot, m_yaw, m_pitch, 0);

    // Transform vectors based on camera's rotation matrix
    D3DXVECTOR3 vWorldUp, vWorldAhead;
    D3DXVECTOR3 vLocalUp = D3DXVECTOR3( 0, 1, 0 );
    D3DXVECTOR3 vLocalAhead = D3DXVECTOR3( 0, 0, 1 );
    D3DXVec3TransformCoord(&vWorldUp, &vLocalUp, &mCameraRot);
    D3DXVec3TransformCoord(&vWorldAhead, &vLocalAhead, &mCameraRot);

    D3DXVECTOR3 vPosDeltaWorld;
    if(!m_vertMovement)
    {
        // If restricting Y movement, do not include pitch
        // when transforming position delta vector.
        D3DXMatrixRotationYawPitchRoll( &mCameraRot, m_yaw, 0.0f, 0.0f );
    }
    D3DXVec3TransformCoord( &vPosDeltaWorld, &vPosDelta, &mCameraRot );

    // Transform the position delta by the camera's rotation
	D3DXVECTOR3 postion = *(D3DXVECTOR3*)&pCameraNode->GetPostion();
	D3DXVECTOR3 eye = postion + vPosDeltaWorld;

	// Set the local position for next frame update
	pCameraNode->SetPosition( (*(Vector3f*)&eye) );

    // Update the lookAt position based on the eye position 
    D3DXVECTOR3 vLookAt = eye + vWorldAhead;

	pCameraNode->SetTarget( (*(Vector3f*)&vLookAt) );
}
*/



/*
// function moves the camera by given speed.
void CCameraControllerFPS::AnimateNode(CSpatial* pSpatial, double time, double deltaTime)
{
	UNUSED_PARAMETER(time);

	if (!pSpatial)
		return;

	// cast the spatial to a camera
	CCameraNodeFPS *pCameraNode = static_cast<CCameraNodeFPS*>(pSpatial);

	if (m_firstUpdate)
	{
		if (pCameraNode && m_pMouse)
		{
			m_pMouse->SetRelativePos( Point2f(0.5f, 0.5f) );
			m_lastCursorPos = m_pMouse->GetRelativeMousePos();
		}

		m_firstUpdate = false;
	}

	float fpitch = 0.0f;
	float fyaw = 0.0f;
	Point2f rotVelocity;
	if (m_mouseButtons[0] == true)
	{
		// Process mouse input
		{
			// Get current position of mouse
			Point2f curMousePos = m_pMouse->GetRelativeMousePos();

			// Calc how far it's moved since last frame
			Point2f curMouseDelta = curMousePos - m_lastCursorPos;

			// Record current position for next time
			m_lastCursorPos = curMousePos;

			// Reset cursor position to the centre of the window.
			if (m_centerMouse)
			{
				m_pMouse->SetRelativePos( Point2f(0.5f, 0.5f) );
				m_lastCursorPos = m_pMouse->GetMousePos();
			}

			// Smooth the relative mouse data over a few frames so it isn't 
			// jerky when moving slowly at low frame rates.
			float percentOfNew = 1.0f / 2.0f;
			float percentOfOld = 1.0f - percentOfNew;
			m_mouseDelta = (m_mouseDelta * percentOfOld) + (curMouseDelta * percentOfNew);
	
			// Assign the rotational velocity of this frame		
			rotVelocity[0] = m_mouseDelta[0] * m_rotateSpeed * m_mouseYDirection;
			rotVelocity[1] = m_mouseDelta[1] * m_rotateSpeed;

			// Update the pitch & yaw angle based on mouse movement
			fyaw += rotVelocity[0];
			fpitch += rotVelocity[1];
		}
	}

    // Get head rotation from the current object orientation transform
	Quaternionf orientation( pCameraNode->GetWorldTransform() );

	// pitch is about local x axes
	// calculate around arbitrary x axes and rotate by current rotation
	if (fpitch != 0.0f)
	{
		Quaternionf pitch(cml::axis_3D(0), fpitch*Const<float>::TO_DEG() );
		orientation *= pitch;
	}

    // Rotate camera about the world y axis.
    // Note the order the quaternions are multiplied. That is important!
    if (fyaw != 0.0f)
    {
        Quaternionf yaw( cml::axis_3D(1), fyaw *Const<float>::TO_DEG() );
		orientation *= yaw;
    }

	orientation.Normalise();

	// Set the head rotation of the camera
	pCameraNode->SetHeadRotation(orientation);

	/////////////////////////////////////////////////////////////////////////////////////////
	// Movement
	/////////////////////////////////////////////////////////////////////////////////////////

	Vector3f constant_motion;
	if (m_cursorkeys[KA_MOVE_FORWARD])
		constant_motion[2] += 1.0f;	

	if (m_cursorkeys[KA_MOVE_BACKWARD])
		constant_motion[2] -= 1.0f;

	if (m_cursorkeys[KA_STRAFE_RIGHT])
		constant_motion[0] += 1.0f;

	if (m_cursorkeys[KA_STRAFE_LEFT])
		constant_motion[0] -= 1.0f;

	Vector3f movement;
    if ( !IsEqual(Mag(constant_motion), 0.0f) )
    {
        //constant motion in frame
        movement = constant_motion;
        movement.Normalise();
    }

	if ( Mag(movement) )
	{
        Vector3f position = Rotate(orientation, movement);
        position *= m_moveSpeed * (float)(deltaTime * 0.001);

		position += pCameraNode->GetPostion();

        pCameraNode->SetPosition(position);
	}

}
*/