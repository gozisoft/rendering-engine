#include "frameafx.h"
#include "Effect.h"
#include "BaseEffect.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include "EffectVarCreate.h"

using namespace Engine;

// Function for building global variables from shader
void CEffect::ProcessVariables(CShaderBlock* shaderBlock)
{
	// Shader params to be filled in
	const CShaderPtr& shader = shaderBlock->m_pShader;

	// Iterate over the constants first
	const std::vector<CConstantFormat*>& constants = shader->GetShaderData().constants;
	for (auto it = constants.begin(); it != constants.end(); ++it)
	{
		CConstantFormat* pConstFormat = *it;
		m_variablePool.push_back( CreateConstant(pConstFormat) );
	}

	// Iterate over the different variables and expose them as globals
	const std::vector<CShaderDesc*>& variableDesc = shader->GetShaderData().descriptors;
	for (auto itor = variableDesc.begin(); itor != variableDesc.end(); ++itor)
	{
		CShaderDesc* pVarDesc = *itor;
		m_variablePool.push_back( CreateVariable(pVarDesc) );
	}

	// Pushback the shader block
	m_shaderBlocks.push_back(shaderBlock);
}


CEffect::CEffect(const CBaseEffectPtr& effect, const std::string& techName) :
m_pEffect(effect),
m_tech( std::ref( m_pEffect->GetTechnique(techName) ) )
{
	const CTechnique& tech = m_tech;
	for (auto i = tech.passArray.begin(); i < tech.passArray.end(); ++i)
	{
		const SVisualPass* pass = (*i);

		// Get variables from vertex shader
		if (pass->m_pVertexShader)
			ProcessVariables(pass->m_pVertexShader);
	
		// Get variables from pixel shader
		if (pass->m_pPixelShader)
			ProcessVariables(pass->m_pPixelShader);
	}
}

CEffect::~CEffect()
{
	// Clear global variables
	m_variablePool.clear();
}









//	// Constant format and Shaderflag origin pair
//struct SConstFormatIDPair
//{
//	SConstFormatIDPair(const CConstantFormatPtr& pFormat, UInt32 flags) : m_pConstFormat(pFormat), m_flags(flags) { }

//	CConstantBufferPtr m_pCBuffer;
//	CConstantFormatPtr m_pConstFormat;
//	UInt32 m_flags;
//};

//// Tags
//struct format_id_pair { };
//struct format { };
//struct buffer { };

//// Typedefs
//typedef boost::multi_index::hashed_unique<
//	boost::multi_index::tag<format_id_pair>,
//	boost::multi_index::identity<SConstFormatIDPair>
//> FormatIDPairIndex; // unique SConstFormatIDPair's

//typedef boost::multi_index::hashed_unique<
//	boost::multi_index::tag<format>,
//	boost::multi_index::member<SConstFormatIDPair, CConstantFormatPtr, &SConstFormatIDPair::m_pConstFormat>
//> FormatIndex; // unique formats

//typedef boost::multi_index::hashed_unique<
//	boost::multi_index::tag<buffer>,
//	boost::multi_index::member<SConstFormatIDPair, CConstantBufferPtr, &SConstFormatIDPair::m_pCBuffer>
//> BufferPoolIndex; // unique buffer pools

//// The constant format is paired with a shader origin flags bitfield.
//// This tells the effect what shaders a constant is found within. A
//// constant buffer is shared between a vertex and a pixel shader. To
//// enforce resource sharing of the effect a constant used within the vertex
//// and shader programs will be using the same data pool (ie no need to manage
//// multiple wvp constants for different shaders).
//typedef boost::multi_index::multi_index_container <
//	SConstFormatIDPair,
//	boost::multi_index::indexed_by <
//	FormatIDPairIndex,
//	FormatIndex,
//	BufferPoolIndex
//	>
//> ConstantFormatContainer;


//void CEffect::ProcessVariables(const ShaderDescs& variableDesc, GlobalVariables& globalVars)
//{
//	// Iterate over the different variables and expose them as globals
//	// globalVars.reserve( globalVars.size() + variableDesc.size() );
//	for (auto itor = variableDesc.begin(); itor != variableDesc.end(); ++itor)
//	{
//		// Temp vairable desc
//		CShaderDesc* pVarDesc = (*itor);
//
//		// Create the correct variable based on class type
//		switch ( pVarDesc->GetClassType() )
//		{
//		case Shader::CT_CBUFFER:
//			{
//				// Cast the constant variable description pointer over to constant format type
//				// and get the number of elements within the format.
//				CConstantFormat* pConstFormat = polymorphic_downcast<CConstantFormat*>(pVarDesc);
//
//				// Create a constnat buffer and pass a pointer of it to the constant object.
//				CConstantBuffer* pConstBuffer = new CConstantBuffer( pConstFormat->GetPaddedSize() );
//
//				// Push the variable to the mapped global variable
//				globalVars.push_back( new constant_object(pConstBuffer, pConstFormat) );
//
//				// Push back the constant buffer to the effects member array
//				m_constBuffers.push_back(pConstBuffer);
//			}
//			break;
//
//		case Shader::CT_TBUFFER:
//			{
//
//			}
//			break;
//
//		case Shader::CT_SAMPLER:
//			{
//
//
//			}
//			break;
//
//		case Shader::CT_TEXTURE:
//			{
//				//pGlobal = new CShaderResourceVariable(pTextureDesc);		
//				//globalVars.push_back(pGlobal); // Push the variable to the 
//			}
//			break;
//		}; // switch
//	}
//}