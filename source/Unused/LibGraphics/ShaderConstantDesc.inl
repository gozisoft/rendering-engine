#ifndef __CCONSTANT_FORMAT_INL__
#define __CCONSTANT_FORMAT_INL__

// ----------------------------------------------------------------------------------------------
// CConstantElement
// ----------------------------------------------------------------------------------------------
inline Shader::DataType CConstantElement::GetDataType() const
{
	return m_dataType;
}

inline Shader::VariableType CConstantElement::GetVariableType() const
{
	return m_variableType;
}

inline size_t CConstantElement::GetRows() const
{
	return m_rows;
}

inline size_t CConstantElement::GetCols() const
{
	return m_columns;
}

inline size_t CConstantElement::GetSize() const
{
	return m_size;
}

inline size_t CConstantElement::GetOffset() const
{
	return m_offset;
}

inline void* CConstantElement::GetDefaultValue() const
{
	return m_pDefaultValue;
}

// ----------------------------------------------------------------------------------------------
// CConstantFormat
// ----------------------------------------------------------------------------------------------
inline const CConstantElement* CConstantFormat::GetElementByIndex(size_t index) const
{
	assert( index < GetElementCount() );
	return m_constElements[index];
}

inline size_t CConstantFormat::GetElementCount() const
{
	return m_constElements.size();
}

inline size_t CConstantFormat::GetPaddedSize() const
{
	return m_paddedSize;
}

inline size_t CConstantFormat::GetSize() const
{
	return m_size;
}



#endif


//// Operator overloads
//inline bool operator != (const CConstantFormat& format0, const CConstantFormat& format1)
//{
//	if ( format0.GetElements() != format1.GetElements() || 
//		 format0.GetFormatName() != format1.GetFormatName() )
//	{
//		return true;
//	}
//
//	return false;
//}
//
//inline bool operator == (const CConstantFormat& format0, const CConstantFormat& format1)
//{
//	return !(format0 != format1);
//}