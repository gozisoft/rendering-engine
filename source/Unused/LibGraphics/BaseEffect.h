#ifndef __CCONTEXT_H__
#define __CCONTEXT_H__

#include "Technique.h"

_ENGINE_BEGIN

// Effect class consits of a number of different effect techniques
class CBaseEffect
{
public:
	// Tags
	struct random_tag { };
	struct name_tag { };

	// Typedefs
	typedef boost::multi_index::random_access< 
		boost::multi_index::tag<random_tag>
	> RandomIndex;

	typedef boost::multi_index::hashed_unique<
		boost::multi_index::tag<name_tag>,
		boost::multi_index::member<CTechnique, std::string, &CTechnique::name>
	> NameIndex;

	typedef boost::multi_index::multi_index_container <
		CTechnique,
		boost::multi_index::indexed_by <
		RandomIndex, 
		NameIndex
		> // index_by
	> Techniques;

	// Typedef list for easy access to multi_index
	typedef Techniques::index<random_tag>::type tech_random;
	typedef Techniques::index<name_tag>::type tech_by_name;

	// Add a single technique to the base effect [C style array method]
	void AddTechnique(const std::string& techName, const SVisualPass** passes,
		size_t numPasses);

	// Add a single technique to the base effect [C++ std::vector method]
	void AddTechnique(const std::string& techName, const VisualPassArray& passes);

	// Add a single technique to the base effect [CTechnique method]
	void AddTechnique(const CTechnique& technique);

	// The number of techniques within this base effect
	size_t GetNumTechniques() const;

	// Access a member of TechniquesArray
	const CTechnique& GetTechnique(size_t techniqueIndex) const;

	// Non-const member of TechniquesArray
	CTechnique& GetTechnique(size_t techniqueIndex);

	// Lookup by the tech's name
	const CTechnique& GetTechnique(const std::string& techName) const;

	// Non-const Lookup by the tech's name
	CTechnique& GetTechnique(const std::string& techName);

protected:
	// Array of specified techniques ( might be better using std::set? )
	Techniques m_techniques;

};

#include "BaseEffect.inl"

_ENGINE_END

#endif
