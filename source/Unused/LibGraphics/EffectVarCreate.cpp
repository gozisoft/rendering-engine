#include "frameafx.h"
#include "EffectVarCreate.h"
#include "Effect.h"
#include "ShaderResource.h"
#include "ViewVariables.h"
#include "ConstantVariables.h"
#include "Helper.h"

_ENGINE_BEGIN

namespace 
{
	VarCreationPtr Lookup(const CRTTI& rtti)
	{
		static CreationMap creationMap = map_list_of<CRTTI, VarCreationPtr>
			(CShaderTextureDesc::rtti, CreateShaderTextureVar)
			(CShaderBufferDesc::rtti, CreateShaderBufferVar);

		auto it = creationMap.find(rtti);
		if ( it == creationMap.end() )
			return nullptr;

		return it->second;
	}

	CVariable* CreateSamplerVar(const CShaderDesc* desc)
	{
		return nullptr;
	}

	CVariable* CreateShaderTextureVar(const CShaderDesc* desc)
	{
		const CShaderTextureDesc* shaderDesc = DynamicCast<const CShaderTextureDesc*>(desc);
		if (shaderDesc != nullptr)
		{
			texture_variable* pView = new texture_variable( (CShaderTextureDesc*)shaderDesc );
			return pView;
		}

		return nullptr;
	}

	CVariable* CreateShaderBufferVar(const CShaderDesc* desc)
	{
		const CShaderBufferDesc* shaderDesc = DynamicCast<const CShaderBufferDesc*>(desc);
		if (shaderDesc != nullptr)
		{
			buffer_variable* pView = new buffer_variable( (CShaderBufferDesc*)shaderDesc );
			return pView;
		}

		return nullptr;
	}
}

CVariable* CreateVariable(const CShaderDesc* desc)
{
	VarCreationPtr phf = Lookup( desc->GetRtti() );
	return phf ? phf(desc) : nullptr;
}

CVariable* CreateConstant(const CConstantFormat* constFormat)
{
	// Create a constnat buffer and pass a pointer of it to the constant object.
	CConstantBuffer* pConstBuffer = new CConstantBuffer( constFormat->GetPaddedSize() );
	constant_object* constObject =  new constant_object( pConstBuffer, (CConstantFormat*)constFormat );
	return constObject;
}


_ENGINE_END