#ifndef __CCONSTANT_FORMAT_H__
#define __CCONSTANT_FORMAT_H__

#include "ShaderVariableDesc.h"

_ENGINE_BEGIN

// m_offset ::
// Number of bytes to advance between elements.
// Typically a multiple of 16 for arrays, vectors, matrices.
// For scalars and small vectors/matrices, this can be 4 or 8.    
class CConstantElement : public CShaderDesc
{
public:
	CConstantElement(const std::string& variableName, Shader::DataType dataType,
		Shader::VariableType variable, size_t rows, size_t columns, size_t size,
		size_t offset, void* pDefaultValue);

	// The class type: scalar, vector, matrix
	Shader::DataType GetDataType() const;

	// Returns the type of vertex, V_UNKNOWN = ~0, V_BYTE = 0, V_UBYTE,
	// VT_BOOL, VT_INT, V_UINT, VT_FLOAT, VT_DOUBLE.
	Shader::VariableType GetVariableType() const;

	// Number of rows of this element
	size_t GetRows() const;

	// Number of columns of this element
	size_t GetCols() const;

	// The size of this element in bytes
	size_t GetSize() const;

	// Get the offset of this element from the start of the buffer array
	size_t GetOffset() const;

	// Default value of the constant buffer
	void* GetDefaultValue() const;

	// Run time type info
	RTTI_DECL;

protected:
	Shader::DataType m_dataType;
	Shader::VariableType m_variableType;
	size_t m_rows;
	size_t m_columns;
	size_t m_size;
	size_t m_offset;
	void* m_pDefaultValue;	
};

// Constant format which can be used to describe the input layout of
// a shader. Uses string variable name types to allow for custom
// shader input signatures in HLSL.
class CConstantFormat : public CShaderDesc
{
public:
	// tags
	typedef std::vector<CConstantElement*> ElementContainer;

	// Constructor for delayed creation
	CConstantFormat(const std::string& bufferName, size_t paddedSize, size_t bindPoint=0,
		size_t bindCount=0);

	// Constructor for fixed creation
	CConstantFormat(const ElementContainer& elements, const std::string& bufferName,
		size_t paddedSize, size_t bindPoint=0, size_t bindCount=0);

	// Non virtual destrcutor
	~CConstantFormat();

	// Push back an element to the format
	const CConstantElement* PushElement(const std::string& variableName, Shader::DataType dataType,
		Shader::VariableType variable, size_t rows, size_t cols, size_t size, size_t offset,
		UInt8* pDefaultValue = nullptr);

	// Get element by its name
	const CConstantElement* GetElementByName(const std::string& variableName) const;

	// Get element by Index
	const CConstantElement* GetElementByIndex(size_t index) const;

	// Number of elements
	size_t GetElementCount() const;

	// Size of all the elements combined + Padding
	size_t GetPaddedSize() const;

	// Size of all the elements combined
	size_t GetSize() const;

	// Run time type info
	RTTI_DECL;

private:
	ElementContainer m_constElements;
	size_t m_paddedSize; // This size includes any padding
	size_t m_size;		 // This size is unpadded
};


#include "ShaderConstantDesc.inl"

_ENGINE_END

#endif