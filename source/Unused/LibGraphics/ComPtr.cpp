#include "frameafx.h"
#include "ComPtr.h"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#undef WIN32_LEAN_AND_MEAN

IUnknown* ComPtrAssign(IUnknown** pp, IUnknown* lp)
{
	if (pp == nullptr)
		return nullptr;
		
	if (lp != nullptr)
		lp->AddRef();

	if (*pp)
		(*pp)->Release();

	*pp = lp;
	return lp;
}