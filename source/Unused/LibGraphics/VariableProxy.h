#ifndef __VARIABLE_PROXIES_H__
#define __VARIABLE_PROXIES_H__

#include "framefwd.h"
#include "RTTI.h"

_ENGINE_BEGIN

class IResourceProxy;
class IShaderBlock;

class IResourceProxy
{
	RTTI_DECL;
public:
	virtual ~IResourceProxy() {} 
	virtual IResourceProxy* GetThis() const = 0;
};

// Observer class
class IShaderBlock
{
public:
	virtual ~IShaderBlock() { }
	virtual void NotifyDirty(IResourceProxy* proxy) = 0;
};

template < class Resource >
class resource_proxy : public IResourceProxy
{
	RTTI_DECL;
	
public:
	// public typedefs
	typedef resource_proxy<Resource> this_type;
	typedef Resource resource_type;
	typedef resource_type* resource_pointer;
	typedef resource_type& resource_reference;

	// constructor
	resource_proxy(const resource_type& resource) : 
	m_buffer(buffer),
	m_dirty(false)
	{

	}

	// copy constructor
	resource_proxy(const this_type& resource) : 
	m_buffer( resource.GetResource() ),
	m_dirty( resource.IsDirty() )
	{

	}

	// operators
	this_type& operator = (const this_type& other)
	{
		m_buffer = other.m_buffer;
		m_observers = other.m_observers;

		// infrom the obeservers of a change in resource
		NotifyDirty();
		return *this;
	}

	// add or remove obeserver objects
	size_t AddShaderBlock(IShaderBlock* block)
	{
		// Check for dupliaction
		auto it = std::find_if(m_observers.begin(), m_observers.end(), [block] (IShaderBlock* observer)
		{
			return observer == block;
		}

		if ( it != m_observers.end() )
		{
			m_observers.push_back(block);
			return m_observers.size();
		}
	}

	void RemoveShaderBlock(size_t index)
	{
		if ( index < m_observers.size() )
		{
			auto it = m_observers.begin() + index;
			m_observers.erase(it);
		}
	}

	void NotifyDirty()
	{
		// infrom the obeservers of a change in resource and set dirty flag
		m_dirty = true;		
		std::for_each( m_observers.begin(), m_observers.end(),
			std::bind1st(IShaderBlock::NotifyDirty, this) );
	}

	void Clean()
	{
		m_dirty = false;
	}

	// Accessor functions
	this_type* GetThis() const
	{
		return this;
	}

	const resource_type& GetResource() const
	{
		return m_buffer;
	}

	resource_type& GetResource() const
	{
		return const_cast<resource_type&>( static_cast<const this_type&>(*this).GetResource() );
	}

private:
	std::vector<IShaderBlock*> m_observers;
	resource_type m_buffer;
	bool m_dirty;
};

_ENGINE_END
	
#endif