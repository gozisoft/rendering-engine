#ifndef __EFFECT_NUMERIC_VARIABLE_H__
#define __EFFECT_NUMERIC_VARIABLE_H__

#include "Globals.h"
#include "ConstantVariables.h"
#include "ConstantBuffer.h"
#include "MathsFwd.h"

#define USING_COLUMN_MAJOR_SHADERS

_ENGINE_BEGIN

//////////////////////////////////////////////////////////////////////////
// numeric_variable - implements raw set/get functionality
//////////////////////////////////////////////////////////////////////////

// IMPORTANT NOTE: All of these numeric & object aspect classes MUST NOT
// add data members to the base variable classes.  Otherwise type sizes 
// will disagree between object & numeric variables and we cannot eaily 
// create arrays of global variables using SGlobalVariable

// Requires that IBaseInterface have CVariable's members, GetTotalUnpackedSize() and DirtyVariable()
class numeric_variable : public CVariable
{
public:
	// Default constructor
	numeric_variable(CConstantBuffer* pConstBuffer, CConstantElement* pElement) :
	CVariable(pConstBuffer, pElement) { 

	}

	const CConstantElement* GetDesc() const {
		return boost::get<CConstantElement*>(m_desc);
	}

	const CConstantBuffer* GetBuffer() const {
		return boost::get<CConstantBuffer*>(m_resource);
	}

protected:
	UInt8* GetOffsetData() {
		return const_cast<UInt8*>( static_cast<const numeric_variable&>(*this).GetOffsetData() );	
	}

	const UInt8* GetOffsetData() const {
		const UInt8* pData = GetBuffer()->GetData() + GetDesc()->GetOffset();
		return pData;	
	}
};


//////////////////////////////////////////////////////////////////////////
// IEffectScalarVariable (TFloatScalarVariable implementation)
//////////////////////////////////////////////////////////////////////////
template <typename T>
class scalar_variable : public numeric_variable
{
public:
	// Typedef typelist
	typedef numeric_variable base_type;
	typedef typename Type2EnumType<T>::type enum_type;
	typedef T value_type;
	typedef value_type* pointer;
	typedef const pointer const_pointer;
	typedef value_type& reference;
	typedef const reference const_reference;

	// default ctor
	scalar_variable(CConstantBuffer* pConstBuffer, CConstantElement* pElement);

	// Functions for set and get
	template < typename U >
	void SetScalar(const U& value);

	template < typename U >
	void GetScalar(U& value) const;

	template < typename U >
	void SetScalarArray(const U& pData, UInt offset, UInt count);

	template < typename U >
	void GetScalarArray(U& pData, UInt offset, UInt count) const;
};

//////////////////////////////////////////////////////////////////////////
// IEffectVectorVariable (vector_variable implementation)
//////////////////////////////////////////////////////////////////////////
template <typename T>
class vector_variable : public numeric_variable
{
public:
	// Typedef typelist
	typedef numeric_variable base_type;
	typedef T value_type;
	typedef value_type* pointer;
	typedef const pointer const_pointer;
	typedef value_type& reference;
	typedef const reference const_reference;

	// Default ctor
	vector_variable(CConstantBuffer* pConstBuffer, CConstantElement* pElement);

	template < typename U >
	void SetVector(const U& srcData);

	template < typename U >
	void SetVector(const U& srcData, size_t srcSize, size_t elements);

	template < typename U >
	void GetVector(U& dstData) const;   

	template < typename U >
	void GetVector(U& dstData, size_t dstSize, size_t elements) const;

	template < typename U >
	void SetVectorArray(const U& srcData, size_t offset, size_t count); 

	template < typename U >
	void GetVectorArray(U& dstData, size_t offset, size_t count) const;
};

//////////////////////////////////////////////////////////////////////////
// IEffectMatrixVariable (matrix_variable implementation)
//////////////////////////////////////////////////////////////////////////
template <typename T>
class matrix_variable : public numeric_variable
{
public:
	// Typedef typelist
	typedef numeric_variable base_type;
	typedef T value_type;
	typedef value_type* pointer;
	typedef const pointer const_pointer;
	typedef value_type& reference;
	typedef const reference const_reference;

	// Default ctor
	matrix_variable(CConstantBuffer* pConstBuffer, CConstantElement* pElement);

	template < typename U >
	void SetMatrix(const U& srcData);

	template < typename U >
	void GetMatrix(U& dstData);

	template < typename U >
	void SetMatrixArray(const U& data, UInt offset, UInt count);

	template < typename U >
	void GetMatrixArray(U& data, UInt offset, UInt count);

	template < typename U >
	void SetMatrixTranspose(const U& data);

	template < typename U >
	void GetMatrixTranspose(U& data);
};

//////////////////////////////////////////////////////////////////////////
// IEffectResourceVariable (matrix_variable implementation)
//////////////////////////////////////////////////////////////////////////
template < class T >
class TResourceVariable : public CVariable
{
public:
	TResourceVariable(const shader_resource& resource)
	{
		m_desc = resource;
	}

	void SetResource(const shader_resource& resource);
	void GetResource(shader_resource& resource);
};

// Shader Resource
class CShaderResourceVariable : public TResourceVariable< CVariable >
{
public:
	CShaderResourceVariable(const shader_resource& resource) : 
	  TResourceVariable(resource)
	  {

	  }
};

#if defined(USING_CMLSHADER) || defined(_USING_CMLSHADER)

//////////////////////////////////////////////////////////////////////////
// Helper functions
//////////////////////////////////////////////////////////////////////////
template< typename T >
T* AsScalar(CVariable* pVariable)
{
	if (pVariable->GetClassType() != Shader::CT_CBUFFER)
	{
		typedef scalar_variable<CVariable, T>* scalar_type;
		scalar_type scalarVar = static_cast<scalar_type>(pVariable);
		if (scalarVar->GetDesc()->GetVariableType() == 
			scalar_type::this_type)
		{
			T* temp_scalar = static_cast<T*>( scalarVar->GetOffsetData() );
			return temp_scalar;
		}
	}

	assert(false);
	return nullptr;
}

template< typename T >
std::shared_ptr< cml::vector< T, cml::external<> > > AsVector(CVariable* pVariable)
{
	if (pVariable->GetClassType() != Shader::CT_CBUFFER)
	{
		typedef vector_variable<T, ParentClass>* vector_type;
		vector_type vectorVar = static_cast<vector_type>(pVariable);
		if (vectorVar->GetDesc()->GetVariableType() == 
			vector_type::this_type)
		{
			// Typedef for the vector type using a shared_ptr.
			typedef std::shared_ptr< cml::vector< T, cml::external<> > > v_ptr;

			v_ptr temp_vector = std::make_shared<v_ptr>( (T*)vectorVar->GetOffsetData(),
				vectorVar->GetDesc()->GetCols() );

			return temp_vector;
		}
	}

	assert(false);
	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
// Typedefs for variables
//////////////////////////////////////////////////////////////////////////

// Scalers
typedef float scalar_float;
typedef bool scalar_bool;
typedef Int32 scalar_int;
typedef UInt32 scalar_uint;

// Vectors
typedef cml::vector< float, cml::external<> > vector_float;
typedef cml::vector< bool, cml::external<> > vector_bool;
typedef cml::vector< Int32, cml::external<> > vector_int;
typedef cml::vector< UInt32, cml::external<> > vector_uint;

// Matrix
typedef cml::matrix< float, cml::external<>, cml::col_basis, cml::col_major > matrix_float;
typedef cml::vector< bool, cml::external<>, cml::col_basis, cml::col_major > matrix_bool;
typedef cml::vector< Int32, cml::external<>, cml::col_basis, cml::col_major > matrix_int;
typedef cml::vector< UInt32, cml::external<>, cml::col_basis, cml::col_major > matrix_uint;

//////////////////////////////////////////////////////////////////////////
// Reference wrappers that can be used for local storage [better than a pointer]
//////////////////////////////////////////////////////////////////////////

// Scalers reference wrapped
typedef std::reference_wrapper<scalar_float> scalarRef_float;
typedef std::reference_wrapper<scalar_bool> scalarRef_bool;
typedef std::reference_wrapper<scalar_int> scalarRef_int;
typedef std::reference_wrapper<scalar_uint> scalarRef_uint;

// Vectors reference wrapped
typedef std::reference_wrapper<vector_float> vectorRef_float;
typedef std::reference_wrapper<vector_bool> vectorRef_bool;
typedef std::reference_wrapper<vector_int> vectorRef_int;
typedef std::reference_wrapper<vector_uint> vectorRef_uint;

// Matrix reference wrapped
typedef std::reference_wrapper<matrix_float> matrixRef_float;
typedef std::reference_wrapper<matrix_bool> matrixRef_bool;
typedef std::reference_wrapper<matrix_int> matrixRef_int;
typedef std::reference_wrapper<matrix_uint> matrixRef_uint;

//////////////////////////////////////////////////////////////////////////
#else
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// Helper functions
//////////////////////////////////////////////////////////////////////////
template <class ParentClass = CVariable, typename T>
scalar_variable<T, ParentClass>* AsScalar(CVariable* pVariable)
{
	if (pVariable->GetClassType() != Shader::CT_CBUFFER)
	{
		typedef scalar_variable<T, ParentClass>* scalar_type;
		scalar_type scalarVar = static_cast<scalar_type>(pVariable);
		if (scalarVar->GetDesc()->GetVariableType() == 
			scalar_type::this_type)
		{
			return scalarVar;
		}
	}

	assert(false);
	return nullptr;
}

template <class ParentClass = CVariable, typename T>
vector_variable<T, ParentClass>* AsVector(CVariable* pVariable)
{
	if (pVariable->GetClassType() != Shader::CT_CBUFFER)
	{
		typedef vector_variable<T, ParentClass>* vector_type;
		vector_type vectorVar = static_cast<vector_type>(pVariable);
		if (vectorVar->GetDesc()->GetVariableType() == 
			vector_type::this_type)
		{
			return vectorVar;
		}
	}

	assert(false);
	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
// Typedefs for variables
//////////////////////////////////////////////////////////////////////////

// Scalers
typedef scalar_variable<float>  scalar_float;
typedef scalar_variable<bool>   scalar_bool;
typedef scalar_variable<Int32>  scalar_int;
typedef scalar_variable<UInt32> scalar_uint;

// Vectors
typedef vector_variable<float>  vector_float;
typedef vector_variable<bool>   vector_bool;
typedef vector_variable<Int32>  vector_int;
typedef vector_variable<UInt32> vector_uint;

// Matrix
typedef matrix_variable<float>  matrix_float;
typedef matrix_variable<bool>   matrix_bool;
typedef matrix_variable<Int32>  matrix_int;
typedef matrix_variable<UInt32> matrix_uint;

//////////////////////////////////////////////////////////////////////////
// Reference wrappers that can be used for local storage [better than a pointer]
//////////////////////////////////////////////////////////////////////////

// Scalers reference wrapped
typedef std::reference_wrapper<scalar_float> scalarRef_float;
typedef std::reference_wrapper<scalar_bool> scalarRef_bool;
typedef std::reference_wrapper<scalar_int> scalarRef_int;
typedef std::reference_wrapper<scalar_uint> scalarRef_uint;

// Vectors reference wrapped
typedef std::reference_wrapper<vector_float> vectorRef_float;
typedef std::reference_wrapper<vector_bool> vectorRef_bool;
typedef std::reference_wrapper<vector_int> vectorRef_int;
typedef std::reference_wrapper<vector_uint> vectorRef_uint;

// Matrix reference wrapped
typedef std::reference_wrapper<matrix_float> matrixRef_float;
typedef std::reference_wrapper<matrix_bool> matrixRef_bool;
typedef std::reference_wrapper<matrix_int> matrixRef_int;
typedef std::reference_wrapper<matrix_uint> matrixRef_uint;

//////////////////////////////////////////////////////////////////////////
#endif // if defined(USING_CMLSHADER) || defined(_USING_CMLSHADER)
//////////////////////////////////////////////////////////////////////////

#include "NumericVariables.inl"

_ENGINE_END

#endif