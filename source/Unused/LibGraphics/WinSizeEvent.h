#ifndef WIN_SIZE_EVENT_H
#define WIN_SIZE_EVENT_H

#include "IEventReciever.h"
#include "MathsFwd.h"


namespace Engine
{


	struct SWindowSizeEvent : public IEventData
	{
		static const EventType sk_EventType;

		const EventType& GetEventType() const
		{
			return sk_EventType;	
		}

		virtual ~SWindowSizeEvent() {}

		explicit SWindowSizeEvent(const Point2l& clientSize, const Point2l& winSize) :
		m_clientSize(clientSize),
		m_winSize(winSize)
		{

		}

		void Serialise(std::ostream &out) const
		{
			UNUSED_PARAMETER(out);
		}

		void Serialise(CBufferIO & data) const
		{
			UNUSED_PARAMETER(data);
		}

		Point2l m_clientSize;
		Point2l m_winSize;

	private:
		DECLARE_HEAP;
	};




}

#endif