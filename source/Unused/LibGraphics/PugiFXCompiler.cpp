#include "frameafx.h"
#include "FXCompiler.h"
#include "pugixml.hpp"

// This will include the multi index capabability of boost
#include <boost/multi_index_container.hpp>

using namespace Engine;

void LoadEffect(std::istream& inputStream)
{
    pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load(inputStream);

	// First child of the xml should be the effect
	pugi::xml_node node = doc.first_child();

	{
		// Check the first node for syntax
		std::string syn_effect = node.name();

		// Transform to lower case
		std::transform(std::begin(syn_effect), std::end(syn_effect),
			std::begin(syn_effect), ::tolower);

		// This file does not contain the correct syntax
		if (syn_effect != "effect")
		{
			assert( false && ieS("Incorrect xml syntax") );
			return;
		}
	}

	for (auto it = node.begin(); it != node.end(); ++it)
	{
		std::string syn_level = (*it).name();

		// Transform to lower case
		std::transform(std::begin(syn_level), std::end(syn_level),
			std::begin(syn_level), ::tolower);

		if (syn_level == "technique")
		{


		}

	}
}




void LoadShader(std::istream& inputStream)
{
    pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load(inputStream);






}