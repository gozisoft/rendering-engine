#include "frameafx.h"
#include "TaskManagerTBB.h"

using namespace Engine;

CTaskManagerTBB::CTaskManagerTBB() : 
m_numThreads(0),
m_maxNumberOfThreads(0),
m_numberOfThreads(0),
m_targetNumberOfThreads(0)
{


}

CTaskManagerTBB::~CTaskManagerTBB()
{
	m_primaryThreadID = tbb::this_tbb_thread::get_id();

	m_numThreads = tbb::task_scheduler_init::default_num_threads();
	m_stallPoolSemaphore = CreateSemaphore(nullptr, 0, m_numThreads, nullptr);
	SynchronizeTask::m_hAllCallbacksInvokedEvent = CreateEvent( NULL, True, False, NULL );
}