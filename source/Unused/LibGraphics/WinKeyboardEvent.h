#ifndef WIN_KEYBOARD_EVENT_H
#define WIN_KEYBOARD_EVENT_H

#include "IEventReciever.h"
#include "Keycodes.h"
#include "BufferIO.h"


_ENGINE_BEGIN

// use of bit fields
struct SKeyInput 
{
	KeyCode key;
	bool pressedDown:1;	// if not true key is up
	bool shift:1;		// if true shift was also pressed
	bool ctrl:1;		// if true ctrl was also pressed

private:
	DECLARE_HEAP;
};

struct SKeyEvent : public IEventData
{
	static const EventType sk_EventType;

	const EventType& GetEventType() const 
	{
		return sk_EventType;	
	}

	virtual ~SKeyEvent() {}

	SKeyEvent(const SKeyInput& input)
	{
		m_keyInput.key = input.key;
		m_keyInput.pressedDown = input.pressedDown;
		m_keyInput.shift = input.shift;
		m_keyInput.ctrl = input.ctrl;
	}

	void Serialise(std::ostream& out) const
	{
		UNUSED_PARAMETER(out);
	}

	void Serialise(CBufferIO& data) const
	{
		UNUSED_PARAMETER(data);
	};

	SKeyInput m_keyInput;

private:
	DECLARE_HEAP;
};


_ENGINE_END

#endif