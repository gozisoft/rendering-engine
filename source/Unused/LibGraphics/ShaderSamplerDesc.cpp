#include "frameafx.h"
#include "ShaderSamplerDesc.h"

using namespace Engine;

const CRTTI CShaderSampler::rtti("Engine.CShaderSampler", &CShaderDesc::rtti);

CShaderSampler::CShaderSampler(const std::string& name,	size_t bindPoint, size_t bindCount) : 
CShaderDesc(name, Shader::CT_SAMPLER, bindPoint, bindCount),
m_lodBias(0.0f),
m_anisotropy(0.0f)
{

}