#include "frameafx.h"
#include "IEffectLoader.h"
#include "FXCompiler.h"

using namespace Engine;

IEffectLoaderPtr IEffectLoader::CreateEffectLoader(EffectType type)
{
	switch (type)
	{
	case ET_FX_XML:
		return std::make_shared<CFXCompiler>();
	default:
		assert(false);
		return nullptr;
	};
}

