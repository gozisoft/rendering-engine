#ifndef __CSHADER_RESOURCE_VIEW_INL__
#define __CSHADER_RESOURCE_VIEW_INL__

// ----------------------------------------------------------------------------------------------
// CShaderTexture
// ----------------------------------------------------------------------------------------------
inline Shader::ShaderReturnType CShaderTextureDesc::GetReturnType() const 
{
	return m_returnType;
}

inline Shader::ShaderDimensions CShaderTextureDesc::GetShaderDimensions() const
{
	return m_dimension;
}

inline size_t CShaderTextureDesc::GetNumSamples() const
{
	return m_numSamples;
}

#endif