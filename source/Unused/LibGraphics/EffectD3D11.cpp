#include "frameafx.h"
#include "EffectD3D11.h"
#include "RenderResourcesD3D11.h"
#include "ResourceD3D11.h"
#include "ShaderD3D11.h"
#include "ViewD3D11.h"

#include "Shader.h"
#include "Technique.h"
#include "ShaderConstantDesc.h"
#include "ConstantBuffer.h"

#include "NumericVariables.h"

_ENGINE_BEGIN

CShaderBlockD3D11::CShaderBlockD3D11(ID3D11DeviceChildPtr pShaderD3D, SD3DShaderVTable* vtable)
{


}

CShaderBlockD3D11::~CShaderBlockD3D11()
{

}

void CShaderBlockD3D11::NotifyDirty(IResourceProxy* proxy)
{



}

// Update constant buffer contents if necessary
void CheckAndUpdateCB(CConstantBuffer* pCB, ID3D11Buffer *pCBD3D, ID3D11DeviceContext *pContext)
{
	if (pCB->IsDirty() == true)
    {
        // CB out of date; rebuild it
		const void* data = static_cast<const void*>( pCB->GetData() );
		pContext->UpdateSubresource(pCBD3D, 0, nullptr, data, 0, 0);

		// Reset the dirty indicator
		pCB->SetDirty(false);
    }
}

CShaderBlockD3D11::CShaderBlockD3D11(ID3D11DeviceChildPtr pShaderD3D, SD3DShaderVTable* vtable) : 
pShaderD3D(pShaderD3D),
pVTable(vtable)
{

}

CShaderBlockD3D11::~CShaderBlockD3D11()
{

}

CEffectD3D11::CEffectD3D11(const CEffect* pEffect, EffectResTuple& eTuple, ID3D11DevicePtr device) :
m_shaderMap( std::get<0>(eTuple) ),
m_resources( std::get<1>(eTuple) ),
m_viewMap( std::get<2>(eTuple) )
{
	const CTechnique& tech = pEffect->GetTechnique();
	for (auto it = tech.passArray.begin(); it != tech.passArray.end(); ++it)
	{
		const SVisualPass* pass = (*it);

		if (pass->m_pVertexShader != nullptr)
			SortVariables(pass->m_pVertexShader);

		if (pass->m_pPixelShader != nullptr)
			SortVariables(pass->m_pPixelShader);
	}
}

CEffectD3D11::~CEffectD3D11()
{

}

void CEffectD3D11::AddVariable(CVariable* variable)
{
	if ( GetVariable( variable->GetVariableName() ) )
		m_variables.push_back(variable);
}

CVariable* CEffectD3D11::GetVariable(const std::string& name) const
{
	const variable_by_name& name_index = m_variables.get<name_tag>();
	auto itor = name_index.find(name);
	if ( itor != name_index.end() )
	{
		return (*itor);
	}
	
	assert( ieS("Failed to find the constant variable by name") );
	return nullptr;
}

CVariable* CEffectD3D11::GetVariable(size_t index) const
{
	return m_variables[index];
}

void CEffectD3D11::SortVariables(const CShader* shader, ID3D11DevicePtr device)
{
	// Add the shader to the D3DResources and obtain the D3D equivilant
	// shader.
	auto shaderIt = m_shaderMap.find(shader);
	if ( shaderIt != m_shaderMap.end() )
	{
		return;
	}

	// Create the API Buffer 
	SD3DShaderVTable* vtable = nullptr;
	ID3D11DeviceChildPtr pShaderD3D = CreateShader(shader, device, &vtable);

	// Create the D3D11 shader block
	CShaderBlockD3D11* pShaderBlockD3D = new CShaderBlockD3D11(pShaderD3D, vtable);

	// Go over constant buffers
	const std::vector<CConstantFormat*>& constants = shader->GetConstantDescs();
	for (auto i = constants.begin(); i != constants.end(); ++i)
	{
		const CConstantFormat* format = *i;
		auto it = m_proxies.find(format->GetVariableName());
		if ( it != m_proxies.end() )
		{
			CConstantBufferProxy* pCProxy = reinterpret_cast<CConstantBufferProxy*>( (*it).second );
			pCProxy->AddShaderBlock(pShaderBlockD3D);
			pShaderBlockD3D->AddVariable();
		}
		else
		{
			m_uniqueConstants.push_back( new CConstantBuffer( (*i)->GetPaddedSize() ) );
			CConstantBufferProxy* proxy = new CConstantBufferProxy( m_uniqueConstants.back() );

			ID3D11BufferPtr bufferD3D = CreateBufferD3D11(m_uniqueConstants.back() , device);

			// Create and add a new variable
			m_variables.push_back( new constant_object(cbuffer, (CConstantFormat*)format) );
		}
	}


	pShaderBlockD3D->CBDep.Reserve( constants.size() );
	for (size_t i = 0; i < pShaderBlockD3D->CBDep.Size(); ++i)
	{
		const CConstantFormat* format = constants[i];

		// Check if a variable has already been created
		const std::string varName = format->GetVariableName();
		auto constIt = m_uniqueConstants.find(varName);
		if ( constIt != m_uniqueConstants.end() )
		{
			auto resIt = m_resources.find( (*constIt).second );
			if ( resIt == m_resources.end() )
			{
				assert(false);
				return;
			}


		}
		else // Have to create a new API buffer and D3D Buffer
		{
			CConstantBuffer* cbuffer = new CConstantBuffer( format->GetPaddedSize() );
			ID3D11BufferPtr bufferD3D = CreateBufferD3D11(cbuffer, device);

			// Store a pointer to the unique constant
			auto newConstIt = m_uniqueConstants.insert( std::make_pair(varName, cbuffer) );

			// Map CBuffer to CBufferD3D (Resource managment)
			m_resources.insert( std::make_pair(cbuffer, bufferD3D) );

			// Create and add a new variable
			m_variables.push_back( new constant_object(cbuffer, (CConstantFormat*)format) );

			// Add the constantbuffer and d3dconstnatbuffer to the D3D11 shader block
			pShaderBlockD3D->CBDep.Push_Back(cbuffer, bufferD3D);
		}
	}

	// Go over the shader views
	const std::vector<CShaderDesc*>& shaderDescs = shader->GetShaderDescs();
	pShaderBlockD3D->ViewDeps.Reserve( shaderDescs.size() );
	for (size_t i = 0; i < pShaderBlockD3D->ViewDeps.Size(); ++i)
	{
		const CShaderDesc* desc = shaderDescs[i];

		// Check if a variable has already been created
		const std::string varName = desc->GetVariableName();
		auto viewIt = m_uniqueViews.find(varName);
		if ( viewIt != m_uniqueViews.end() )
		{
			auto resIt = m_viewMap.find( (*viewIt).second );
			if ( resIt == m_viewMap.end() )
			{
				assert(false);
				return;
			}

			// Add the resource view and d3d resource view to the D3D11 shader block
			pShaderBlockD3D->ViewDeps.Push_Back( (*viewIt).second, (*resIt).second );
		}
		else
		{
			m_uniqueViews.insert( std::make_pair(varName, nullptr) );

			// Add the resource view and d3d resource view to the D3D11 shader block
			pShaderBlockD3D->ViewDeps.Push_Back(pResView, pResViewD3D);

		}




	}

	// Map CShader to ID3D11DeviceChildPtr (Resource managment)
	m_shaderMap.insert( std::make_pair(pShader, pShaderD3D) );

	// Map CShaderblock to CShaderblockD3D
	m_shaderBlocks.insert( std::make_pair(shaderblock, pShaderBlockD3D) );
	
}

void CEffectD3D11::Enable(CEffect* pEffect, ID3D11DeviceContextPtr& context)
{
	const CTechnique& tech = pEffect->GetTechnique();
	for (auto it = tech.passArray.begin(); it != tech.passArray.end(); ++it)
	{
		const SVisualPass* pass = (*it);

		// Bind the vertex shader to the renderer and process its constants
		auto vertIt = m_shaderBlocks.find(pass->m_pVertexShader);
		if (vertIt != m_shaderBlocks.end())
		{
			ApplyShader(vertIt->second, context);
		}

		auto pixIt = m_shaderBlocks.find(pass->m_pPixelShader);
		if (pixIt != m_shaderBlocks.end())
 		{
			ApplyShader(pixIt->second, context);
		}
	}
}

void CEffectD3D11::ApplyShader(CShaderBlockD3D11* block, ID3D11DeviceContextPtr& context)
{
	// Now process and update D3D versions of the constant buffers
	for (size_t i = 0; i < block->CBDep.Size(); ++i)
	{
		CShaderBlockD3D11::ConstantBufferDependency::pair_type& CBPair = block->CBDep.At(i);
		CheckAndUpdateCB(CBPair.first, CBPair.second, context);
	}

	// Process and update the D3D versions of the resource views
	for (size_t i = 0; i < block->ResourceDeps.Size(); ++i)
	{
		CShaderBlockD3D11::ShaderResourceDependency::pair_type& RVPair = block->ResourceDeps.At(i);
		

	}

	// Enable the constant buffers
	( context->*(block->pVTable->pSetConstantBuffers) )(0, block->CBDep.Size(), &block->CBDep.pD3DRes[0] );

	// Enable the shader resource views
	( context->*(block->pVTable->pSetShaderResources) )(0, block->ResourceDeps.Size(), & block->ResourceDeps.pD3DRes[0]);

	// [FINAL] Enable the shader
	( context->*(block->pVTable->pSetShader) )( block->pShaderD3D, nullptr, 0 );
}

_ENGINE_END

//// Iterate over each constant buffer and create a corresponding 
//// d3d constant buffer.
//for (size_t i = 0; i < m_constBuffers.size(); ++i)
//{
//	// Access the original CBuffer*
//	const CBuffer* pBuffer = static_cast<const CBuffer*>( m_constBuffers[i] );
//
//	// Create the API Buffer using data from CBuffer*
//	CConstantBufferD3D11* pCBufferD3D = new CConstantBufferD3D11(pBuffer, pRes->m_pImmediateContext);
//
//	// ID3DBuffer [comptr]
//	const ID3D11BufferPtr& pD3DBuffer = pCBufferD3D->GetBuffer();
//
//	// Push the API buffer to the effects array of D3DBuffers
//	m_d3dBufferArray[i] = pD3DBuffer.m_Ptr;
//
//	// Push the API buffer to the buffer map and map D3DBuffer to CBuffer
//	CBufferMap.insert( make_pair(pBuffer, pD3DBuffer) );
//}