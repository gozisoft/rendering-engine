#ifndef __CEFFECT_INL__
#define __CEFFECT_INL__

inline CVariable* CEffect::GetVariableByIndex(size_t index) const
{
	return m_variablePool[index];
}

inline const CEffect::Variables& CEffect::GetVariables() const
{
	return m_variablePool;
}

inline const CTechnique& CEffect::GetTechnique() const
{
	return m_tech.get();
}



#endif