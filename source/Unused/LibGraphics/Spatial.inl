#ifndef __CSPATIAL_INL__
#define __CSPATIAL_INL__

// Ability to set the world transform
inline void CSpatial::SetWorldTransform(const CTransform& transform)
{
	m_worldTransform = transform;
}

// Access to the world transform
inline const CTransform& CSpatial::GetWorldTransform() const
{
	return m_worldTransform;
}

inline CSpatial::CullMode CSpatial::GetCullMode() const
{
	return m_cullMode;
}

inline void CSpatial::SetCullMode(CullMode mode)
{
	m_cullMode = mode;
}

inline const CSphere& CSpatial::GetWorldBound() const
{
	return m_worldBound;
}



#endif