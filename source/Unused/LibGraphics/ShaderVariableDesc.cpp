#include "frameafx.h"
#include "ShaderVariableDesc.h"

using namespace Engine;
using namespace Shader;

RTTI_ROOT_IMPL(Engine, CShaderDesc);

CShaderDesc::CShaderDesc(const std::string& name, ClassType classType, 
	size_t bindPoint, size_t bindCount) : 
m_resourceName(name),
m_classType(classType),
m_bindPoint(bindPoint),
m_bindCount(bindCount)
{

}

CShaderDesc::~CShaderDesc()
{
	
}
