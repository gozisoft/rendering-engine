#include "frameafx.h"
#include "winMouseEvent.h"
#include "StringFunctions.h"

using namespace Engine;

DEFINE_HEAP(SMouseInput, "SMouseInput");
DEFINE_HIERARCHICALHEAP(SMouseEvent, "SMouseEvent", "IEventData");

const SMouseEvent::EventType SMouseEvent::sk_EventType("SMouseEvent");