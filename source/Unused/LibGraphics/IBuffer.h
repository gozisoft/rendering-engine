#ifndef __IBUFFER_H__
#define __IBUFFER_H__



class IVertexBuffer
{
public:
	// Run time type declare
	RTTI_DECL;

	// destructor
	virtual ~IVertexBuffer() { /**/ }

	// The stride in bytes
	// Returns the stride which is calculated by m_channels * SizeOfType(type).
	// Also known as the size of each vertex { i.e. Vector3f + ColorRBGA = 12 + 16 = 28 stride } 
	virtual UInt GetStride() const = 0;

	// The number of elements within the Buffer ie 24 Vector3's
	virtual UInt GetNumVertices() const = 0;
};

class IIndexBuffer
{
public:
	// Run time type declare
	RTTI_DECL;

	// destructor
	virtual ~IIndexBuffer() { /**/ }

	// This function will update the min and max index values.
	// These values can be used within glDrawRangeElements.
	virtual void UpdateMinMax(IPipeline* pipeline) = 0;

	// Retrieve the number of elements
	virtual UInt GetNumIndices() const = 0;

	// The stride in bytes
	virtual UInt GetStride() const = 0;

	// Get the minimum range of the index buffer
	virtual UInt GetMinRange() const = 0;

	// Get the maximum range of the index buffer
	virtual UInt GetMaxRange() const = 0;
};

class IConstantBuffer
{
public:
	// Run time type declare
	RTTI_DECL;

	// Virtual destructor
	virtual ~IConstantBuffer() { /**/ }
};

#endif