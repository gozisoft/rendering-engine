#ifndef __EFFECT_NUMERIC_VARIABLE_INL__
#define __EFFECT_NUMERIC_VARIABLE_INL__

//////////////////////////////////////////////////////////////////////////
// IEffectScalarVariable (TFloatScalarVariable<T> implementation)
//////////////////////////////////////////////////////////////////////////
template <typename T>
scalar_variable<T>::scalar_variable(CConstantBufferPtr pConstBuffer, CConstantElement* pElement) : 
base_type(pConstBuffer, pElement)
{ 

}

template <typename T> template < typename U >
void scalar_variable<T>::SetScalar(const U& value)
{
	using Shader::CopyScalarValue;
	T* pData = reinterpret_cast<T*>( GetOffsetData() );
	CopyScalarValue(value, *pData);
}

template <typename T> template < typename U >
void scalar_variable<T>::GetScalar(U& value) const
{
	using Shader::CopyScalarValue;
	const T* pData = reinterpret_cast<const T*>( GetOffsetData() );	
	CopyScalarValue(*pData, value);
}
//-------------------------------------------------------------------------------------
template <typename T> template < typename U >
void scalar_variable<T>::SetScalarArray(const U& pData, UInt offset, UInt count)
{
	UNUSED_PARAMETER(pData);
	UNUSED_PARAMETER(offset);
	UNUSED_PARAMETER(count);
}

template <typename T> template < typename U >
void scalar_variable<T>::GetScalarArray(U& pData, UInt offset, UInt count)
{
	UNUSED_PARAMETER(pData);
	UNUSED_PARAMETER(offset);
	UNUSED_PARAMETER(count);
}

//////////////////////////////////////////////////////////////////////////
// IEffecTVectorVariable<ParentClass, BaseType> (vector_variable<ParentClass, BaseType> implementation)
//////////////////////////////////////////////////////////////////////////
template <typename T>
vector_variable<T>::vector_variable(CConstantBufferPtr pConstBuffer, CConstantElement* pElement) : 
base_type(pConstBuffer, pElement)
{ 

}

#if defined (USING_COLUMN_MAJOR_SHADERS) || defined (_USING_COLUMN_MAJOR_SHADERS)

template <typename T> template < typename U >
void vector_variable<T>::SetVector(const U& srcData)
{
	using std::addressof;
	using Shader::CopyDataWithTypeConversion;

	// Get a pointer to the offset constant buffer
	T* destData = reinterpret_cast<T*>( GetOffsetData() );

	// size of the destination in bytes and presumed source
	size_t bothSizes = GetDesc()->GetSize();

	// num of presumed elements within the source
	size_t elements = GetDesc()->GetCols();

	CopyDataWithTypeConversion( addressof(srcData),
		destData,
		bothSizes,  // size of the source in bytes
		bothSizes,  // size of the destination in bytes
		elements ); // number of elements from source to read
}

template <typename T> template < typename U >
void vector_variable<T>::SetVector(const U& srcData, size_t srcSize, size_t elements)
{
	using std::addressof;
	using Shader::CopyDataWithTypeConversion;

	// Make sure src has less or equal elements in vector
	assert( elements <= GetDesc()->GetCols() );

	// Get a pointer to the offset constant buffer
	T* destData = reinterpret_cast<T*>( GetOffsetData() );

	// size of the destination in bytes
	size_t destSize = GetDesc()->GetSize();

	CopyDataWithTypeConversion( addressof(srcData),
		destData,
		srcSize,	// size of the source in bytes
		destSize,	// size of the destination in bytes
		elements);	// number of elements from source to read
}

template <typename T> template < typename U >
void vector_variable<T>::GetVector(U& dstData) const
{
	using std::addressof;
	using Shader::CopyDataWithTypeConversion;

	// Get a pointer to the offset constant buffer
	const T* srcData = reinterpret_cast<const T*>( GetOffsetData() );

	// size of the destination in bytes and presumed source
	size_t bothSizes = GetDesc()->GetSize();

	// num of presumed elements within the source
	size_t elements = GetDesc()->GetCols();

	CopyDataWithTypeConversion( srcData,
		addressof(dstData),
		bothSizes, // size of the source in bytes
		bothSizes, // size of the destination in bytes
		elements); // number of elements from source to read
}

template <typename T> template < typename U >
void vector_variable<T>::GetVector(U& dstData, size_t dstSize, size_t elements) const
{
	using std::addressof;
	using Shader::CopyDataWithTypeConversion;

	// Make sure src has less or equal elements in vector
	assert( elements <= GetDesc()->GetCols() );

	// Get a pointer to the offset constant buffer
	const T* srcData = reinterpret_cast<const T*>( GetOffsetData() );

	// size of the destination in bytes
	size_t srcSize = GetDesc()->GetSize();

	// Copy data from buffer to the destination
	CopyDataWithTypeConversion(srcData,
		addressof(dstData),
		srcSize,	// size of the source in bytes
		dstSize,	// size of the destination in bytes
		elements);	// number of elements from source to read
}

//-------------------------------------------------------------------------------------
template <typename T> template < typename U >
void vector_variable<T>::SetVectorArray(const U& srcData, size_t offset, size_t count)
{
	UNUSED_PARAMETER(pData);
	UNUSED_PARAMETER(offset);
	UNUSED_PARAMETER(count);
}

template <typename T> template < typename U >
void vector_variable<T>::GetVectorArray(U& destData, size_t offset, size_t count) const
{
	UNUSED_PARAMETER(pData);
	UNUSED_PARAMETER(offset);
	UNUSED_PARAMETER(count);
}

#endif // #if defined (USING_COLUMN_MAJOR_SHADERS) || defined (_USING_COLUMN_MAJOR_SHADERS)


//////////////////////////////////////////////////////////////////////////
// IEffectMatrixVariable (matrix_variable implementation)
//////////////////////////////////////////////////////////////////////////
template <typename T>
matrix_variable<T>::matrix_variable(CConstantBufferPtr pConstBuffer, CConstantElement* pElement) : 
base_type(pConstBuffer, pElement)
{ 

}

#if defined (USING_COLUMN_MAJOR_SHADERS) || defined (_USING_COLUMN_MAJOR_SHADERS)

template <typename T> template < typename U >
void matrix_variable<T>::SetMatrix(const U& srcData)
{
	using std::addressof;
	using Shader::CopyDataWithTypeConversion;

	// Get a pointer to the offset constant buffer
	T* dstData = reinterpret_cast<T*>( GetOffsetData() );

	// size of the destination in bytes and presumed source
	size_t bothSizes = GetDesc()->GetSize();

	// number of elements within the matrix
	size_t elements = GetDesc()->GetRows() * GetDesc()->GetCols();

	// copy data over
	CopyDataWithTypeConversion( addressof(srcData),
		dstData,
		bothSizes,	// size of the source in bytes
		bothSizes,	// size of the destination in bytes
		elements);	// number of elements from source to read				
}

template <typename T> template < typename U >
void matrix_variable<T>::GetMatrix(U& dstData)
{
	UNUSED_PARAMETER(dstData);
}
//-------------------------------------------------------------------------------------
template <typename T> template < typename U >
void matrix_variable<T>::SetMatrixArray(const U& srcData, size_t offset, size_t count)
{
	UNUSED_PARAMETER(srcData);
	UNUSED_PARAMETER(offset);
	UNUSED_PARAMETER(count);
}

template <typename T> template < typename U >
void matrix_variable<T>::GetMatrixArray(U& dstData, size_t offset, size_t count)
{
	UNUSED_PARAMETER(dstData);
	UNUSED_PARAMETER(offset);
	UNUSED_PARAMETER(count);
}
//-------------------------------------------------------------------------------------
template <typename T> template < typename U >
void matrix_variable<T>::SetMatrixTranspose(const U& srcData)
{
	UNUSED_PARAMETER(srcData);
}

template <typename T> template < typename U >
void matrix_variable<T>::GetMatrixTranspose(U& dstData)
{
	UNUSED_PARAMETER(dstData);
}

#endif // #if defined (USING_COLUMN_MAJOR_SHADERS) || defined (_USING_COLUMN_MAJOR_SHADERS)

//////////////////////////////////////////////////////////////////////////
// IEffectResourceVariable (matrix_variable implementation)
//////////////////////////////////////////////////////////////////////////
template<class ParentClass>
void TResourceVariable<T>::SetResource(const ShaderResource& resource)
{
	m_resource = resource;
}

template<class ParentClass>
void TResourceVariable<T>::GetResource(ShaderResource& resource)
{
	// resource = m_memberData.m_shaderResource;
}

#endif
