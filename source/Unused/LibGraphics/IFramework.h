#ifndef __IFRAMEWORK_H__
#define __IFRAMEWORK_H__

#include "Core.h"

_ENGINE_BEGIN

class IFramework
{
public:
	virtual ~IFramework() { }

	virtual bool Initialise() = 0;
	virtual void Release() = 0;
	virtual int Run(void* hAccel) = 0;

};


_ENGINE_END

#endif