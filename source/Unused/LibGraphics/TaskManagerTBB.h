#ifndef __CTASK_MANAGER_TBB_H__
#define __CTASK_MANAGER_TBB_H__

#include <tbb/task.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/tbb_thread.h>

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#undef WIN32_LEAN_AND_MEAN

#include "framefwd.h"

class ISystemTask;

_ENGINE_BEGIN




class CTaskManagerTBB
{
public:
	CTaskManagerTBB();
	~CTaskManagerTBB();


private:
	typedef std::vector<ISystemTask> SystemTasks;
	typedef std::vector<tbb::task::affinity_id> AffinityIDsList;
	typedef std::shared_ptr<tbb::task_scheduler_init> task_scheduler_init_Ptr;
	typedef std::shared_ptr<tbb::task> task_ptr;
	typedef tbb::tbb_thread::id id;

	SystemTasks m_primaryThreadSystemTasks;
	SystemTasks m_tmpTasks;

	AffinityIDsList m_affinityIDs;

	id m_primaryThreadID;
	task_ptr m_pStallPoolParent;
	task_ptr m_pSystemTasksRoot;
	task_scheduler_init_Ptr m_pTbbScheduler;

	UInt32 m_numThreads;
	UInt32 m_maxNumberOfThreads;
	UInt32 m_numberOfThreads;
	UInt32 m_targetNumberOfThreads;

	HANDLE m_stallPoolSemaphore;

};

_ENGINE_END

#endif