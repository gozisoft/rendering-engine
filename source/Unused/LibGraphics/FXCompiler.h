#ifndef __CFX_COMPILER_H__
#define __CFX_COMPILER_H__

#include "IEffectLoader.h"
#include "ShaderTypes.h"
#include "Shader.h"
#include "Technique.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>


_ENGINE_BEGIN

class CShaderTypeParser
{
public:
	CShaderTypeParser()	{
		m_shaderTypeMap.insert( std::make_pair("Vertex", CShader::ST_VERTEX) );
		m_shaderTypeMap.insert( std::make_pair("Pixel", CShader::ST_PIXEL) );
		m_shaderTypeMap.insert( std::make_pair("Geometry", CShader::ST_GEOMETRY) );
	}

	~CShaderTypeParser() {
		m_shaderTypeMap.clear();
	}

	CShader::ShaderType PraseSType(const std::string& type) const {
		auto itor = m_shaderTypeMap.find(type);
		if (itor == m_shaderTypeMap.end() )
		{
			assert( false && ieS("Unknown shader type within effect xml") );
			return CShader::ST_UNKNOWN;
		}
		return itor->second;
	}

private:
	typedef std::map<std::string, CShader::ShaderType> ShaderTypeMap;
	ShaderTypeMap m_shaderTypeMap;
};

class CShaderVersionParser
{
public:
	CShaderVersionParser() {
		m_shaderVersionMap.insert( std::make_pair("3.0", CShader::SV_3_0) );
		m_shaderVersionMap.insert( std::make_pair("4.0", CShader::SV_4_0) );
		m_shaderVersionMap.insert( std::make_pair("4.1", CShader::SV_4_1) );
		m_shaderVersionMap.insert( std::make_pair("5.0", CShader::SV_5_0) );
	}

	~CShaderVersionParser()	{
		m_shaderVersionMap.clear();
	}

	CShader::ShaderVersion PraseSVersion(const std::string& version) const {
		auto itor = m_shaderVersionMap.find(version);
		if (itor == m_shaderVersionMap.end() )
		{
			assert( false && ieS("Unknown shader version within effect xml") );
			return CShader::SV_UNKNOWN;
		}
		return itor->second;
	}

private:
	typedef std::map<std::string, CShader::ShaderVersion> ShaderVersionMap;
	ShaderVersionMap m_shaderVersionMap;
};

class CXMLProcessor
{
public:
	typedef boost::property_tree::iptree iptree;
	friend class CFXCompiler;

	// Constructor to begin the process
	CXMLProcessor(std::istream& inputStream);
	~CXMLProcessor();
	
	// Functions for tree processing
	void BuildContext(iptree &contextLevel);
	void BuildPass(iptree &passLevel, CShader::ShaderVersion version);
	std::string ReadShaderData(std::string& name);

	// Mapping functions for translation
	CShader::ShaderVersion MapShaderVersion(const std::string& versionNum);
	CShader::ShaderType MapShaderType(const std::string& shaderType);

private:
	typedef std::map<std::string, CShader::ShaderType> ShaderTypeMap;
	typedef std::map<std::string, CShader::ShaderVersion> ShaderVersionMap;

	ShaderTypeMap m_shaderTypeMap;
	ShaderVersionMap m_shaderVersionMap;
	iptree m_tree;
};

class CFXCompiler : public IEffectLoader
{
public:	
	CBaseEffectPtr LoadEffect(std::istream& inputStream);
	CBaseEffectPtr LoadEffect(std::wistream& inputStream);
};


_ENGINE_END

#endif





//namespace Detail
//{
//	class CShaderData;
//	class CPassInfo;
//	class CContext;
//
//	typedef std::shared_ptr<CShaderData> CShaderDataPtr;
//	typedef std::shared_ptr<CPassInfo> CPassInfoPtr;
//	typedef std::shared_ptr<CContext> CContextPtr;
//	typedef std::vector<CPassInfoPtr> PassInfoArray;
//	typedef std::vector<CContextPtr> ContextArray;
//
//	class IMessage
//	{
//	public:
//		enum MessageType
//		{
//			MT_SHADERDATA,
//			MT_PASSINFO,
//			MT_CONTEXT
//		};
//	};
//
//	class CShaderData
//	{
//	public:
//		CShaderData() {}
//		CShaderData(std::string& st, std::string& fn) : 
//		shaderType(st), functionName(fn) 
//		{}
//
//		std::string shaderType;
//		std::string functionName;
//	};
//
//	class CPassInfo
//	{
//	public:
//		CPassInfo() {}
//		CPassInfo(CShaderDataPtr& vert, CShaderDataPtr& pixel) :
//		vertex(vert), pixel(pixel)
//		{}
//
//		size_t index;
//		CShaderDataPtr vertex;
//		CShaderDataPtr pixel;
//	};
//
//	class CContext
//	{
//	public:
//		CContext() {}
//		CContext(std::string& cn, std::string& tv, PassInfoArray ps) :
//		contexName(cn), techVersion(tv), passes(ps)
//		{}
//
//		std::string contexName;
//		std::string techVersion;
//		PassInfoArray passes;
//	};
//}
//
//namespace boost {
//namespace serialization {
//
//	template<class Archive>
//	void serialize(Archive & ar, Detail::CPassInfo & p, const unsigned int version)
//	{
//		ar & p.index;
//		ar & p.vertex;
//		ar & p.pixel;
//	}
//
//	template<class Archive>
//	void serialize(Archive & ar, Detail::CContext & c, const unsigned int version)
//	{
//		ar & c.contexName;
//		ar & c.techVersion;
//		ar & c.passes;
//	}
//
//	template<class Archive>
//	void serialize(Archive & ar, Detail::CShaderData & s, const unsigned int version)
//	{
//		ar & s.shaderType;
//		ar & s.functionName;
//		ar & s.passes;
//	}
//
//} // namespace serialization
//} // namespace boost
