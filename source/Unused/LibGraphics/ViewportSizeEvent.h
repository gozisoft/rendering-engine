#ifndef VIEWPORT_SIZE_EVENT_H
#define VIEWPORT_SIZE_EVENT_H

#include "IEventReciever.h"
#include "MathsFwd.h"

namespace Engine
{

	struct SViewportSizeEvent : public IEventData
	{
		static const EventType sk_EventType;

		const EventType& GetEventType() const
		{
			return sk_EventType;	
		}

		virtual ~SViewportSizeEvent() {}

		explicit SViewportSizeEvent(const Viewporti& viewport) :
		m_viewport(viewport)
		{

		}

		void Serialise(OStream &out) const
		{
			UNUSED_PARAMETER(out);
		}

		Viewporti m_viewport;

	private:
		DECLARE_HEAP;
	};


}


#endif