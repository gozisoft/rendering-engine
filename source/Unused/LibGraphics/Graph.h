#ifndef __GRAPH_H__
#define __GRAPH_H__

#include "framefwd.h"

_ENGINE_BEGIN

template < class Vertex, class Edge >
class vertex
{
public:
	// Typedef typelist
	typedef Vertex vertex_type;
	typedef vertex_type* vertex_pointer;
	typedef vertex_type& vertex_reference;
	typedef Edge Edge_type;
	typedef Edge_type* edge_pointer;
	typedef Edge_type& edge_reference;

	typedef std::vector<edge_pointer> edgeList;

	vertex() : m_index(0) {
	}

	~vertex() {
		m_edges.clear();
	}

	void AddEdge(edge_pointer edge)	{
		m_edges.push_back(edge);
	}

	edgeList& GetEdges() {
		return m_edges;
	}

	size_t GetIndex() const	{
		return m_index;
	}

	void SetIndex(size_t index)	{
		m_index = index;
	}

protected:
	edgeList m_edges;
	size_t m_index;

};

template < class Vertex, class Edge >
class graph
{
public:
	// Typedef typelist
	typedef Vertex vertex_type;
	typedef vertex_type* vertex_pointer;
	typedef vertex_type& vertex_reference;
	typedef Edge Edge_type;
	typedef Edge_type* edge_pointer;
	typedef Edge_type& edge_reference;

	typedef std::vector<vertex_pointer> vertexList;
	typedef std::vector<edge_pointer> edgeList;

	// Constructor and destructor
	graph();
	virtual ~graph();

	// Add a new vertex
	void AddVertex(vertex_pointer vert)
	{

	}

	// Create an edge between vertices
	bool CreateEdge(vertex_pointer tail, vertex_pointer head,
		bool twoway = true)
	{


	}

	// Access to the graph contents
	vertex_pointer GetVertex(size_t index) const
	{
		return m_verts[index];
	}

	edge_pointer GetEdge(size_t index) const
	{
		return m_edges[index];
	}

protected:
	vertexList m_verts;
	edgeList m_edges;

};




_ENGINE_END

#endif