#ifndef __ISPATIAL_H__
#define __ISPATIAL_H__

#include "framefwd.h"

_ENGINE_BEGIN

class ISpatial
{
public:
	virtual ~ISpatial() {}

	virtual void Update(double time, double deltaTime, bool initiator) = 0;

	virtual void UpdateWorldData(double time, double deltaTime) = 0;

	virtual void UpdateWorldBound() = 0;

	virtual void GetVisibleSet(CCuller& culler, bool noCull) = 0;

	virtual void UpdateFromRoot() = 0;
};

_ENGINE_END

#endif