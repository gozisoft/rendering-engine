#include "Frameafx.h"
#include "GeometryCreator.h"
#include "FVFormat.h"
#include "TriangleMesh.h"
#include "VertexBuffer.h"
#include "MathsFwd.h"

using namespace Engine;
using namespace Buffer;

CGeometry::CGeometry(CFVFormatPtr pVFormat, bool insideNormals, CBuffer::BufferUse usage) :
m_pVFormat(pVFormat), 
m_usage(usage),
m_insideNormals(insideNormals)
{
	m_hasNormals = m_pVFormat->IsEnabled(VS_NORMAL);
	m_hasTCoords = m_pVFormat->IsEnabled(VS_TEXTURE_COORDINATES);
	m_hasColours = m_pVFormat->IsEnabled(VS_COLOUR);
}

CTriMeshPtr CGeometry::Box(float xExtent, float yExtent, float zExtent)
{
	UInt numVertices = 24;
	UInt numTriangles = 12;
	UInt numIndices = 3*numTriangles;
	UInt stride = m_pVFormat->GetTotalStride();

	CVertexBufferPtr pVBuffer = nullptr; 

	if ( (m_hasColours == true) && (m_hasTCoords == false) )
	{


		VPosCol cubeFace[] = 
		{
			{ Vector3f(-xExtent, -yExtent, -zExtent), cml::white_rgba() },
			{ Vector3f(-xExtent, +yExtent, -zExtent), cml::black_rgba() },
			{ Vector3f(+xExtent, +yExtent, -zExtent), cml::red_rgba() },
			{ Vector3f(+xExtent, -yExtent, -zExtent), cml::green_rgba() },
			{ Vector3f(-xExtent, -yExtent, +zExtent), cml::blue_rgba() },
			{ Vector3f(-xExtent, +yExtent, +zExtent), cml::yellow_rgba() },
			{ Vector3f(+xExtent, +yExtent, +zExtent), cml::cyan_rgba() },
			{ Vector3f(+xExtent, -yExtent, +zExtent), cml::magenta_rgba() },
		};

		pVBuffer = std::make_shared<CVertexBuffer>(numVertices, stride, m_usage, cubeFace); 
	}
	else if ( (m_hasColours == false) && (m_hasTCoords == true) )
	{
		struct VPosTex
		{
			Vector3f pos;
			Vector2f tex; 
		};

		VPosTex cubeFace[] = 
		{
			{ Vector3f(-xExtent, -yExtent, -zExtent), Vector2f(0.0f, 0.0f) },
			{ Vector3f(-xExtent, +yExtent, -zExtent), Vector2f(0.0f, 1.0f) },
			{ Vector3f(+xExtent, +yExtent, -zExtent), Vector2f(1.0f, 1.0f) },
			{ Vector3f(+xExtent, -yExtent, -zExtent), Vector2f(1.0f, 0.0f) },
			{ Vector3f(-xExtent, -yExtent, +zExtent), Vector2f(1.0f, 0.0f) },
			{ Vector3f(-xExtent, +yExtent, +zExtent), Vector2f(1.0f, 1.0f) },
			{ Vector3f(+xExtent, +yExtent, +zExtent), Vector2f(0.0f, 1.0f) },
			{ Vector3f(+xExtent, -yExtent, +zExtent), Vector2f(0.0f, 0.0f) },
		};

		pVBuffer = std::make_shared<CVertexBuffer>(numVertices, stride, m_usage, cubeFace); 
	}

	UInt indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3,

		// back face
		4, 6, 5,
		4, 7, 6,

		// left face
		4, 5, 1,
		4, 1, 0,

		// right face
		3, 2, 6,
		3, 6, 7,

		// top face
		1, 5, 6,
		1, 6, 2,

		// bottom face
		4, 0, 3, 
		4, 3, 7
	};

	// Generate indices (outside view).
	CIndexBufferPtr pIBuffer = std::make_shared<CIndexBuffer>(numIndices, IT_UINT, indices);
	UInt* indices = reinterpret_cast<UInt*>( pIBuffer->GetData() );

	// return the mesh
	return std::make_shared<CTriMesh>(m_pVFormat, pVBuffer, pIBuffer);
}

CTriMeshPtr CGeometry::Triangle(float xExtent, float yExtent, float zExtent)
{
	UInt stride = m_pVFormat->GetTotalStride();
	CVertexBufferPtr pVBuffer = nullptr; 

	// positions
	if ( (m_hasColours == true) && (m_hasTCoords == false) )
	{
		struct VPosCol
		{
			Vector3f pos;
			Colour4f col; 
		};

		VPosCol triangle[] = 
		{
			{ Vector3f(-xExtent, -yExtent, zExtent), cml::red_rgba() },
			{ Vector3f(0.0f, yExtent, zExtent), cml::green_rgba() },
			{ Vector3f(xExtent, -yExtent, zExtent), cml::blue_rgba() }
		};

		pVBuffer = std::make_shared<CVertexBuffer>(3, stride, m_usage, triangle); 
	}
	else if ( (m_hasColours == false) && (m_hasTCoords == true) )
	{
		struct VPosTex
		{
			Vector3f pos;
			Vector2f tex; 
		};

		VPosTex triangle[] = 
		{
			{ Vector3f(-xExtent, -yExtent, zExtent),	Vector2f(0.0f, 0.0f) },
			{ Vector3f(0.0f, yExtent, zExtent),			Vector2f(0.5f, 1.0f) },
			{ Vector3f(xExtent, -yExtent, zExtent),		Vector2f(1.0f, 0.0f) }
		};

		pVBuffer = std::make_shared<CVertexBuffer>(3, stride, m_usage, triangle); 
	}

	// Create index buffer
	UInt indices[] = {	0, 1, 2, };
	CIndexBufferPtr pIBuffer = std::make_shared<CIndexBuffer>(3, IT_UINT, indices);

	// return the mesh
	return std::make_shared<CTriMesh>(m_pVFormat, pVBuffer, pIBuffer);
}

CTriMeshPtr CGeometry::Plane(float cellSpacing, UInt rows, UInt cols)
{
	UInt numVertices = rows * cols;
	UInt stride = m_pVFormat->GetTotalStride();

	float halfWidth = (cols - 1) * cellSpacing * 0.5f;
	float halfDepth = (rows - 1) * cellSpacing * 0.5f;

	// Texture coords
	float du = 1.0f / (cols - 1);
	float dv = 1.0f / (rows - 1);

	// Pointer to the vertex buffer
	CVertexBufferPtr pVBuffer = nullptr; 
	if ( (m_hasNormals == true) && (m_hasColours == true) )
	{
		struct VPNCVert
		{
			Vector3f pos;
			Vector3f normal;
			Colour4f col; 
		};

		// Generate vertices
		std::vector<VPNCVert> vertices(numVertices);	
		for (UInt i = 0; i < rows; ++i)
		{
			float z = halfDepth - i * cellSpacing;
			for (UInt j =0; j < cols; ++j)
			{
				float x = -halfWidth + j * cellSpacing;
				float y = 0;

				vertices[i * cols + j].pos = Vector3f(x, y, z);
				vertices[i * cols + j].normal = Vector3f(0.0f, 1.0f, 0.0f);
				vertices[i * cols + j].col = cml::blue_rgba();
			}
		}

		pVBuffer = std::make_shared<CVertexBuffer>(numVertices, stride, m_usage, &vertices[0]);
	}
	else if ( (m_hasNormals == true) && (m_hasTCoords == true) )
	{
		struct VPNTVert
		{
			Vector3f pos;
			Vector3f normal;
			Vector2f tex; 
		};

		// Generate vertices
		std::vector<VPNTVert> vertices(numVertices);
		for (UInt i = 0; i < rows; ++i)
		{
			float z = halfDepth - i * cellSpacing;
			for (UInt j =0; j < cols; ++j)
			{
				float x = -halfWidth + j * cellSpacing;
				float y = 0;

				vertices[i * cols + j].pos = Vector3f(x, y, z);
				vertices[i * cols + j].normal = Vector3f(0.0f, 1.0f, 0.0f);
				vertices[i * cols + j].tex = Vector2f(j * du, i*dv);
			}
		}

		pVBuffer = std::make_shared<CVertexBuffer>(numVertices, stride, m_usage, &vertices[0]);
	}

	// Generate indices (outside view).
	UInt numFaces = (rows-1) * (cols-1) * 2;
	CIndexBufferPtr pIBuffer = std::make_shared<CIndexBuffer>(numFaces * 3, IT_UINT);
	UInt* indices = reinterpret_cast<UInt*>( pIBuffer->GetData() );

	int k = 0;
	for(UInt i = 0; i < rows-1; ++i)
	{
		for(UInt j = 0; j < cols-1; ++j)
		{
			indices[k]   = i * cols + j;
			indices[k+1] = i * cols + j + 1;
			indices[k+2] = (i+1) *cols + j;

			indices[k+3] = (i+1) * cols + j;
			indices[k+4] = i * cols + j + 1;
			indices[k+5] = (i+1) * cols + j + 1;

			k += 6; // next quad
		}
	}

	// return the mesh
	return std::make_shared<CTriMesh>(m_pVFormat, pVBuffer, pIBuffer);
}

//CTriMeshPtr CGeometry::Disk(UInt32 shellSamples, UInt32 radialSamples, float radius)
//{
//	UInt32 rsm1 = radialSamples - 1, ssm1 = shellSamples - 1;
//	UInt32 numVertices = 1 + radialSamples*ssm1;
//	UInt32 numTriangles = radialSamples*(2*ssm1-1);
//	UInt32 numIndices = 3*numTriangles;
//	UInt stride = m_pVFormat->GetTotalStride();
//
//	// Create a vertex buffer.
//	CVertexBufferPtr pVBuffer( new CVertexBuffer(numVertices, stride, m_usage) );
//	CVertexBufferAccess vba(m_pVFormat, pVBuffer);
//
//	// Generate geometry.
//
//	// Center of disk.
//	vba.Position<Vector3f>(0) = Vector3f(0.0f, 0.0f, 0.0f);
//	if (m_hasNormals)
//	{
//		vba.Normal<Vector3f>(0) = Vector3f(0.0f, 0.0f, 1.0f);
//	}
//
//	/*
//	Vector2f tcoord(0.5f, 0.5f);
//	int unit;
//	for (unit = 0; unit < MAX_UNITS; ++unit)
//	{
//	if (mHasTCoords[unit])
//	{
//	vba.TCoord<Float2>(unit, 0) = tcoord;
//	}
//	}
//	*/
//
//	float invSSm1 = 1.0f/(float)ssm1;
//	float invRS = 1.0f/(float)radialSamples;
//	for (UInt32 r = 0; r < radialSamples; ++r)
//	{
//		float angle = Const<float>::TWO_PI() * invRS * r;
//		float cs = std::cos(angle);
//		float sn = std::sin(angle);
//		Vector3f radial(cs, sn, 0.0f);
//
//		for (UInt32 s = 1; s < shellSamples; ++s)
//		{
//			float fraction = invSSm1*s;  // in (0,R]
//			Vector3f fracRadial = fraction*radial;
//			int i = s + ssm1*r;
//
//			vba.Position<Vector3f>(i) = radius * fracRadial;
//			if (m_hasNormals)
//			{
//				vba.Normal<Vector3f>(i) = Vector3f(0.0f, 0.0f, 1.0f);
//			}
//
//			/*
//			tcoord[0] = 0.5f + 0.5f*fracRadial[0];
//			tcoord[1] = 0.5f + 0.5f*fracRadial[1];
//			for (unit = 0; unit < MAX_UNITS; ++unit)
//			{
//			if (mHasTCoords[unit])
//			{
//			vba.TCoord<Float2>(unit, i) = tcoord;
//			}
//			}
//			*/
//		}
//	}
//	// TransformData(vba);
//
//	// Generate indices.
//	CIndexBufferPtr pIBuffer ( new CIndexBuffer(numIndices, IT_UINT, m_usage) );
//	UInt32* indices = (UInt32*)pIBuffer->GetData();
//	for (UInt32 r0 = rsm1, r1 = 0, t = 0; r1 < radialSamples; r0 = r1++)
//	{
//		indices[0] = 0;
//		indices[1] = 1 + ssm1*r0;
//		indices[2] = 1 + ssm1*r1;
//		indices += 3;
//		++t;
//		for (UInt32 s = 1; s < ssm1; ++s, indices += 6)
//		{
//			int i00 = s + ssm1*r0;
//			int i01 = s + ssm1*r1;
//			int i10 = i00 + 1;
//			int i11 = i01 + 1;
//			indices[0] = i00;
//			indices[1] = i10;
//			indices[2] = i11;
//			indices[3] = i00;
//			indices[4] = i11;
//			indices[5] = i01;
//			t += 2;
//		}
//	}
//
//	return CTriMeshPtr( new CTriMesh(m_pVFormat, pVBuffer, pIBuffer) );
//}
//
//CTriMeshPtr CGeometry::Sphere (UInt32 zSamples, UInt32 radialSamples, float radius)
//{
//	UInt32 zsm1 = zSamples-1, zsm2 = zSamples-2, zsm3 = zSamples-3;
//	UInt32 rsp1 = radialSamples+1;
//	UInt32 numVertices = zsm2*rsp1 + 2;
//	UInt32 numTriangles = 2*zsm2*radialSamples;
//	UInt32 numIndices = 3*numTriangles;
//	UInt stride = m_pVFormat->GetTotalStride();
//
//	// Create a vertex buffer.
//	CVertexBufferPtr pVBuffer( new CVertexBuffer(numVertices, stride, m_usage) ); 
//	CVertexBufferAccess vba (m_pVFormat, pVBuffer);
//
//	// Generate geometry.
//	float invRS = 1.0f/(float)radialSamples;
//	float zFactor = 2.0f/(float)zsm1;
//	UInt32 r, z, zStart, i, unit;
//	Vector2f tcoord;
//
//	// Generate points on the unit circle to be used in computing the mesh
//	// points on a cylinder slice.
//	float* sn = new float[rsp1];
//	float* cs = new float[rsp1];
//	for (r = 0; r < radialSamples; ++r)
//	{
//		float angle = Const<float>::TWO_PI() * invRS * r;
//		cs[r] = std::cos(angle);
//		sn[r] = std::sin(angle);
//	}
//	sn[radialSamples] = sn[0];
//	cs[radialSamples] = cs[0];
//
//	// Generate the cylinder itself.
//	for (z = 1, i = 0; z < zsm1; ++z)
//	{
//		float zFraction = -1.0f + zFactor*z;  // in (-1,1)
//		float zValue = radius*zFraction;
//
//		// Compute center of slice.
//		Vector3f sliceCenter(0.0f, 0.0f, zValue);
//
//		// Compute radius of slice.
//		float sliceRadius = sqrt( Abs( Sqr(radius) - Sqr(zValue) ) );
//
//		// Compute slice vertices with duplication at endpoint.
//		Vector3f normal;
//		int save = i;
//		for (r = 0; r < radialSamples; ++r)
//		{
//			float radialFraction = r*invRS;  // in [0,1)
//			Vector3f radial(cs[r], sn[r], 0.0f);
//
//			vba.Position<Vector3f>(i) = sliceCenter + sliceRadius * radial;
//
//			if(m_hasColours)
//				vba.Colour<Colour4f>(0, i) = Colour4f( 0.25f, 0.75f, 0.10f, 1.0f );
//
//			if (m_hasNormals)
//			{
//				normal = vba.Position<Vector3f>(i);
//				normal.normalize();
//				if (m_insideNormals)
//				{
//					vba.Normal<Vector3f>(i) = -normal;
//				}
//				else
//				{
//					vba.Normal<Vector3f>(i) = normal;
//				}
//			}
//
//			/*     tcoord[0] = radialFraction;
//			tcoord[1] = 0.5f*(zFraction + 1.0f);
//			for (unit = 0; unit < MAX_UNITS; ++unit)
//			{
//			if (m_hasTCoords[unit])
//			{
//			vba.TCoord<Float2>(unit, i) = tcoord;
//			}
//			} */
//
//			++i;
//		}
//
//		vba.Position<Vector3f>(i) = vba.Position<Vector3f>(save);
//		if (m_hasNormals)
//		{
//			vba.Normal<Vector3f>(i) = vba.Normal<Vector3f>(save);
//		}
//
//		/*
//		tcoord[0] = 1.0f;
//		tcoord[1] = 0.5f*(zFraction + 1.0f);
//		for (unit = 0; unit < MAX_UNITS; ++unit)
//		{
//		if (mHasTCoords[unit])
//		{
//		vba.TCoord<Vector2f>(unit, i) = tcoord;
//		}
//		}
//		*/
//
//		++i;
//	}
//
//	// south pole
//	vba.Position<Vector3f>(i) = Vector3f(0.0f, 0.0f, -radius);
//
//	if(m_hasColours)
//		vba.Colour<Colour4f>(0, i) = Colour4f( 0.25f, 0.75f, 0.10f, 1.0f );
//
//	if (m_hasNormals)
//	{
//		if (m_insideNormals)
//		{
//			vba.Normal<Vector3f>(i) = Vector3f(0.0f, 0.0f, 1.0f);
//		}
//		else
//		{
//			vba.Normal<Vector3f>(i) = Vector3f(0.0f, 0.0f, -1.0f);
//		}
//	}
//
//	/*
//	tcoord = Float2(0.5f, 0.5f);
//	for (unit = 0; unit < MAX_UNITS; ++unit)
//	{
//	if (mHasTCoords[unit])
//	{
//	vba.TCoord<Float2>(unit, i) = tcoord;
//	}
//	}
//	*/
//
//	++i;
//
//	// north pole
//	vba.Position<Vector3f>(i) = Vector3f(0.0f, 0.0f, radius);
//
//	if(m_hasColours)
//		vba.Colour<Colour4f>(0, i) = Colour4f( 0.25f, 0.75f, 0.10f, 1.0f );
//
//	if (m_hasNormals)
//	{
//		if (m_insideNormals)
//		{
//			vba.Normal<Vector3f>(i) = Vector3f(0.0f, 0.0f, -1.0f);
//		}
//		else
//		{
//			vba.Normal<Vector3f>(i) = Vector3f(0.0f, 0.0f, 1.0f);
//		}
//	}
//
//	/*
//	tcoord = Float2(0.5f, 1.0f);
//	for (unit = 0; unit < MAX_UNITS; ++unit)
//	{
//	if (mHasTCoords[unit])
//	{
//	vba.TCoord<Float2>(unit, i) = tcoord;
//	}
//	}
//	*/
//
//	++i;
//
//	// TransformData(vba);
//
//	// -----------------------------------------------------------------------------------------
//	// Generate indices.
//	// -----------------------------------------------------------------------------------------
//	CIndexBufferPtr pIbuffer ( new CIndexBuffer(numIndices, IT_UINT, m_usage) );
//	UInt32* indices = (UInt32*)pIbuffer->GetData();
//	for (z = 0, zStart = 0; z < zsm3; ++z)
//	{
//		UInt32 i0 = zStart;
//		UInt32 i1 = i0 + 1;
//		zStart += rsp1;
//		UInt32 i2 = zStart;
//		UInt32 i3 = i2 + 1;
//		for (i = 0; i < radialSamples; ++i, indices += 6)
//		{
//			if (m_insideNormals)
//			{
//				indices[0] = i0++;
//				indices[1] = i2;
//				indices[2] = i1;
//				indices[3] = i1++;
//				indices[4] = i2++;
//				indices[5] = i3++;
//			}
//			else  // inside view
//			{
//				indices[0] = i0++;
//				indices[1] = i1;
//				indices[2] = i2;
//				indices[3] = i1++;
//				indices[4] = i3++;
//				indices[5] = i2++;
//			}
//		}
//	}
//
//	// south pole triangles
//	int numVerticesM2 = numVertices - 2;
//	for (i = 0; i < radialSamples; ++i, indices += 3)
//	{
//		if (m_insideNormals)
//		{
//			indices[0] = i;
//			indices[1] = i + 1;
//			indices[2] = numVerticesM2;
//		}
//		else
//		{
//			indices[0] = i;
//			indices[1] = numVerticesM2;
//			indices[2] = i + 1;
//		}
//	}
//
//	// north pole triangles
//	int numVerticesM1 = numVertices-1, offset = zsm3*rsp1;
//	for (i = 0; i < radialSamples; ++i, indices += 3)
//	{
//		if (m_insideNormals)
//		{
//			indices[0] = i + offset;
//			indices[1] = numVerticesM1;
//			indices[2] = i + 1 + offset;
//		}
//		else
//		{
//			indices[0] = i + offset;
//			indices[1] = i + 1 + offset;
//			indices[2] = numVerticesM1;
//		}
//	}
//
//	SafeDelete(cs);
//	SafeDelete(sn);
//
//	// The duplication of vertices at the seam cause the automatically
//	// generated bounding volume to be slightly off center.  Reset the bound
//	// to use the true information.
//	CTriMeshPtr pMesh ( new CTriMesh(m_pVFormat, pVBuffer, pIbuffer) );
//	pMesh->GetModelBound().m_center = cml::zero_3D();
//	pMesh->GetModelBound().m_radius = radius;
//	return pMesh;
//}
//
//CTriMeshPtr CGeometry::Torus (UInt32 circleSamples, UInt32 radialSamples, float outerRadius, float innerRadius)
//{
//	UInt32 numVertices = (circleSamples+1)*(radialSamples+1);
//	UInt32 numTriangles = 2*circleSamples*radialSamples;
//	UInt32 numIndices = 3*numTriangles;
//	UInt stride = m_pVFormat->GetTotalStride();
//
//	// Create a vertex buffer.
//	CVertexBufferPtr pVBuffer( new CVertexBuffer(numVertices, stride, m_usage) ); 
//	CVertexBufferAccess vba(m_pVFormat, pVBuffer);
//
//	// Generate geometry.
//	float invCS = 1.0f/(float)circleSamples;
//	float invRS = 1.0f/(float)radialSamples;
//	UInt32 c, r, i, unit;
//	Vector2f tcoord;
//
//	// Generate the cylinder itself.
//	for (c = 0, i = 0; c < circleSamples; ++c)
//	{
//		// Compute center point on torus circle at specified angle.
//		float circleFraction = c * invCS;  // in [0,1)
//		float theta = Const<float>::TWO_PI() * circleFraction;
//		float cosTheta = std::cos(theta);
//		float sinTheta = std::sin(theta);
//		Vector3f radial(cosTheta, sinTheta, 0.0f);
//		Vector3f torusMiddle = outerRadius*radial;
//
//		// Compute slice vertices with duplication at endpoint.
//		int save = i;
//		for (r = 0; r < radialSamples; ++r)
//		{
//			float radialFraction = r*invRS;  // in [0,1)
//			float phi = Const<float>::TWO_PI() * radialFraction;
//			float cosPhi = std::cos(phi);
//			float sinPhi = std::sin(phi);
//			Vector3f normal = cosPhi*radial + sinPhi*cml::axis_3D(2);
//			vba.Position<Vector3f>(i) = torusMiddle + innerRadius*normal;
//
//			if(m_hasColours)
//				vba.Colour<Colour4f>(0, i) = Colour4f( 0.55f, 0.0f, 0.10f, 1.0f );
//
//			if (m_hasNormals)
//			{
//				if (m_insideNormals)
//				{
//					vba.Normal<Vector3f>(i) = -normal;
//				}
//				else
//				{
//					vba.Normal<Vector3f>(i) = normal;
//				}
//			}
//
//			/*
//			tcoord = Vector2f(radialFraction, circleFraction);
//			for (unit = 0; unit < MAX_UNITS; ++unit)
//			{
//			if (mHasTCoords[unit])
//			{
//			vba.TCoord<Float2>(unit, i) = tcoord;
//			}
//			}
//			*/
//
//			++i;
//		}
//
//		vba.Position<Vector3f>(i) = vba.Position<Vector3f>(save);
//		if (m_hasNormals)
//		{
//			vba.Normal<Vector3f>(i) = vba.Normal<Vector3f>(save);
//		}
//
//		/*
//		tcoord = Float2(1.0f, circleFraction);
//		for (unit = 0; unit < MAX_UNITS; ++unit)
//		{
//		if (mHasTCoords[unit])
//		{
//		vba.TCoord<Float2>(unit, i) = tcoord;
//		}
//		}
//		*/
//
//		++i;
//	}
//
//	// Duplicate the cylinder ends to form a torus.
//	for (r = 0; r <= radialSamples; ++r, ++i)
//	{
//		vba.Position<Vector3f>(i) = vba.Position<Vector3f>(r);
//
//		if(m_hasColours)
//			vba.Colour<Colour4f>(0, i) = Colour4f( 0.55f, 0.0f, 0.10f, 1.0f );
//
//		if (m_hasNormals)
//		{
//			vba.Normal<Vector3f>(i) = vba.Normal<Vector3f>(r);
//		}
//
//		/*
//		for (unit = 0; unit < MAX_UNITS; ++unit)
//		{
//		if (mHasTCoords[unit])
//		{
//		vba.TCoord<Float2>(unit, i) =
//		Float2(vba.TCoord<Float2>(unit, r)[0], 1.0f);
//		}
//		}
//		*/
//	}
//
//	// TransformData(vba);
//
//	// Generate indices.
//	CIndexBufferPtr pIBuffer( new CIndexBuffer(numIndices, IT_UINT, m_usage) );
//	UInt32* indices = (UInt32*)pIBuffer->GetData();
//	UInt32 cStart = 0;
//	for (c = 0; c < circleSamples; ++c)
//	{
//		UInt32 i0 = cStart;
//		UInt32 i1 = i0 + 1;
//		cStart += radialSamples + 1;
//		UInt32 i2 = cStart;
//		UInt32 i3 = i2 + 1;
//		for (i = 0; i < radialSamples; ++i, indices += 6)
//		{
//			if (m_insideNormals)
//			{
//				indices[0] = i0++;
//				indices[1] = i1;
//				indices[2] = i2;
//				indices[3] = i1++;
//				indices[4] = i3++;
//				indices[5] = i2++;
//			}
//			else  // inside view
//			{
//				indices[0] = i0++;
//				indices[1] = i2;
//				indices[2] = i1;
//				indices[3] = i1++;
//				indices[4] = i2++;
//				indices[5] = i3++;
//			}
//		}
//	}
//
//	// The duplication of vertices at the seam cause the automatically
//	// generated bounding volume to be slightly off center.  Reset the bound
//	// to use the true information.
//	CTriMeshPtr pMesh ( new CTriMesh(m_pVFormat, pVBuffer, pIBuffer) );
//	pMesh->GetModelBound().m_center = cml::zero_3D();
//	pMesh->GetModelBound().m_radius = outerRadius;
//	return pMesh;
//}
//
//void CGeometry::ComputeBoundingSphere(const UInt8* pData, UInt numVertices, CSphere& sphere)
//{
//	const CVertexElementPtr& pElement = m_pVFormat->GetElementBySemantic(VS_POSITION);
//	if (!pElement)
//	{
//		assert( false && ieS("Requires position element to compute bounding sphere\n") );
//		return;
//	}
//
//	Buffer::VertexType type = pElement->GetVertexType();
//	if (type != VT_FLOAT3 && type != VT_FLOAT4)
//	{
//		assert( false && ieS("Can only be used on 3 or 4 float elements\n") );
//		return;
//	}
//
//	// Caclulate center of the sphere - average value of all the vertex positions.
//	sphere.m_center = cml::zero_3D();
//	for (UInt i = 0; i < numVertices; ++i)
//	{
//		const float *pos = reinterpret_cast<const float*>( ( pData + pElement->GetStride() )  + ( i * m_pVFormat->GetTotalStride() ) );
//		sphere.m_center[0] += pos[0];
//		sphere.m_center[1] += pos[1];
//		sphere.m_center[2] += pos[2];
//	}
//
//	// Same as [SumOfAllVerts / numVerts]
//	float invNumVerts = 1.0f / static_cast<float>(numVertices);
//	sphere.m_center *= invNumVerts;
//
//	// The radius is the largest distance from the center to the positions.
//	// Eqtn of a circle (x - ca)^2 + (y - cb)^2 + (z - cc)^2 = r^2
//	// where [ca,cb,cc] are the xyz coords of the center.
//	sphere.m_radius = 0.0f;
//	for (UInt i = 0; i < numVertices; ++i)
//	{
//		const float *pos = reinterpret_cast<const float*>( ( pData + pElement->GetStride() )  + ( i * m_pVFormat->GetTotalStride() ) );
//		float difference[3] = 
//		{
//			pos[0] - sphere.m_center[0],
//			pos[1] - sphere.m_center[1],
//			pos[2] - sphere.m_center[2]
//		};
//		float radiusSqrd = Sqr(difference[0]) + Sqr(difference[1]) + Sqr(difference[2]);
//
//		// now check if this radius is greator than the last radius
//		if (radiusSqrd > sphere.m_radius)
//		{
//			sphere.m_radius = radiusSqrd;
//		}
//	}
//
//	sphere.m_radius = sqrt(sphere.m_radius);
//}