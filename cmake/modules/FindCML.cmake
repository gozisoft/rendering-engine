# Look here https://cmake.org/Wiki/CMake:How_To_Find_Libraries
# and here https://github.com/SFML/SFML/wiki/Tutorial%3A-Build-your-SFML-project-with-CMake

# Find CML
#
# CML_INCLUDE_DIR
# CML_LIBRARY
# CML_FOUND

# define the list of search paths for headers and libraries
# CML home can be set in root CMakeLists
# CML can be set in OS enviorement variable
set(FIND_CML_PATHS ${CML_HOME} $ENV{CML_HOME})

# find the SFML include directory
find_path(CML_INCLUDE_DIR cml/cml.h
        HINTS ${FIND_CML_PATHS})

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBXML2_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(CML DEFAULT_MSG CML_INCLUDE_DIR)

mark_as_advanced(CML_INCLUDE_DIR)

# define the variable for include dirs
set(CML_INCLUDE_DIRS ${CML_INCLUDE_DIR})

