# Look here https://cmake.org/Wiki/CMake:How_To_Find_Libraries
# and here https://github.com/SFML/SFML/wiki/Tutorial%3A-Build-your-SFML-project-with-CMake

# Find WTL
#
# WTL_INCLUDE_DIR
# WTL_LIBRARY
# WTL_FOUND

# define the list of search paths for headers and libraries
# WTL home can be set in root CMakeLists
# WTL can be set in OS enviorement variable
set(FIND_WTL_PATHS
        ${WTL_HOME}
        $ENV{WTL_HOME}
        ${WTL_HOME}/include
        $ENV{WTL_HOME}/include)

# Set a list of the wtl header files to find.
set(WTL_HEADERS
        atlapp.h
        atlcrack.h
        atlctrls.h
        atlctrlw.h
        atlctrlx.h
        atlddx.h
        atldlgs.h
        atldwm.h
        atlfind.h
        atlframe.h
        atlgdi.h
        atlmisc.h
        atlprint.h
        atlres.h
        atlresce.h
        atlribbon.h
        atlscrl.h
        atlsplit.h
        atltheme.h
        atluser.h
        atlwince.h
        atlwinx.h)

# find the WTL include directory
find_path(WTL_INCLUDE_DIR ${WTL_HEADERS}
        PATHS ${FIND_WTL_PATHS})

include(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set WTL_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(WTL DEFAULT_MSG WTL_INCLUDE_DIR)

# define the variable for include dirs
set(WTL_INCLUDE_DIRS ${WTL_INCLUDE_DIR})

# If WTL is not found locally, download it
if (NOT WTL_FOUND)
    message("Package WTL not found - download instead")

    # Download and unpack googletest at configure time
    configure_file(${CMAKE_SOURCE_DIR}/cmake/configurations/CompileWTL.txt.in wtl-download/CMakeLists.txt)
    execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
            RESULT_VARIABLE result
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/wtl-download )
    if(result)
        message(FATAL_ERROR "CMake step for cml failed: ${result}")
    endif()
    execute_process(COMMAND ${CMAKE_COMMAND} --build .
            RESULT_VARIABLE result
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/wtl-download )
    if(result)
        message(FATAL_ERROR "Build step for cml failed: ${result}")
    endif()

    set(CML_INCLUDE_DIRS ${CMAKE_BINARY_DIR}/wtl-src)
endif ()