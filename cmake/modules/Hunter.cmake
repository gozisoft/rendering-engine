# !!! DO NOT PLACE HEADER GUARDS HERE !!!

# Load used modules
include(hunter_add_version)
include(hunter_download)
include(hunter_pick_scheme)

# List of versions here...
hunter_add_version(
        cml
        VERSION "2.0.0a"
        URL "https://github.com/rgozim/CML/archive/CML-2_0_0_a.tar.gz"
        SHA1 "464ab8455bcf9bad5726bf874742318c7eb02774"
)


# Probably more versions for real packages...

# Pick a download scheme
hunter_pick_scheme(DEFAULT url_sha1_cmake) # use scheme for cmake projects

# Download package.
# Two versions of library will be build by default:
#     * libexample_A.a
#     * libexample_Ad.a
hunter_download(PACKAGE_NAME cml)