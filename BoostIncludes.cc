
//// This will include the multi index capabability of boost
//#include <boost/multi_index_container.hpp>
//#include <boost/multi_index/mem_fun.hpp>
//#include <boost/multi_index/composite_key.hpp>
//#include <boost/multi_index/member.hpp>
//
//// These do have fwd declares, but I have no idea how to use them
//#include <boost/multi_index/ordered_index.hpp>
//#include <boost/multi_index/hashed_index.hpp>
//#include <boost/multi_index/sequenced_index.hpp>
//#include <boost/multi_index/random_access_index.hpp>

// The assign list of functionality
#include <boost/assign/list_of.hpp>

// The boost varient
#include <boost/variant.hpp>

// Boost signals library
#include <boost/signals.hpp>
#include <boost/signals2.h>

// Safe casting ability
#include <boost/cast.hpp>

// Safe deltetion
#include <boost/checked_delete.hpp>