**Rendering engine**

#### Getting Started

The project requires some external libraries.

The libraries are as follows:

- CMake
- Boost
- Configurable Maths Library (CML)
- (Windows only) Windows Template lanaguate (WTL)

**CMake**

1. Download cmake via https://cmake.org/download/

**CML**

1. My mirror of CML can be cloned at https://github.com/demianmnave/CML.git
2. (Optional) Add CML CML_HOME as environment variable, or to your $PATH if you're using OSx.

**WTL**

1. WTL can be downloaded at https://sourceforge.net/projects/wtl/
2. (Optional) Add WTL WTL_HOME as environment variable, or to your $PATH if you're using OSx.

**My recommend project structure**

* user / projects
* conan.io
* cml
* rendering-engine

#### Configure Build

Inside the rendering-engine folder you will find `CMakeLists.txt`. This is the root _cmake_ file of the project and any
subsequent _cmake_ files branch off of it in a tree like structure.

##### `rendering-engine/CMakeLists.txt`

There are 2 properties you will have `set` in the root `CMakeLists.txt` file. You will find these properties near the
bottom of the file.

1. `set(CML_HOME "/PATH_TO_WHERE_YOU_CLONCED_CML/cml")`
2. `set(BOOST_ROOT "/Development/sdk/boost_1_60_0")`
3. `set(BOOST_INCLUDEDIR "/Development/sdk/boost_1_60_0/boost")`

##### (Windows only) `rendering-engine/source/Executable/ExeWTLWindow/CMakeLists.txt`

1. `set(WTL_HOME "/PATH_TO_WHERE_YOU_EXTRACTED_WTL")`

#### Using Conan

1. `$ conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan`
2. `$ mkdir .conan`
3. `$ cd .conan`

###### OSX

1. `$ conan install .. -s compiler=apple-clang -s compiler.libcxx=libc++ -s compiler.version=9.0 --build=missing`

###### Windows

#### Building project files via cmake GUI

1. Launch cmake application
2. Set "Where is the source code" to point to the rendering-engine project dir
3. Set "Where to build the binaries" to a different dir than the source code dir
4. Click `Configure`
5. Click `Generate`

You can now open the project in your XCode, whatever you generated the project in.
